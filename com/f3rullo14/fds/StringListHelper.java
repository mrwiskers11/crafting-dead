package com.f3rullo14.fds;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.EnumChatFormatting;

public class StringListHelper {

   public static List a(List var0, int var1) {
      return a(a(var0), var1);
   }

   public static String a(List var0) {
      String var1 = "";

      for(int var2 = 0; var2 < var0.size(); ++var2) {
         var1 = var1 + " " + ((String)var0.get(var2)).trim();
      }

      return var1.trim();
   }

   public static ArrayList a(String var0, int var1) {
      ArrayList var2 = new ArrayList();
      String var3 = a(var0);
      String[] var4 = var3.split(" ");
      String var5 = "";
      int var6 = var1;
      FontRenderer var7 = Minecraft.getMinecraft().fontRenderer;

      for(int var8 = 0; var8 < var4.length; ++var8) {
         String var9 = var4[var8];
         if(var9.equals("[br]")) {
            if(var7.getStringWidth(var5) > 0) {
               var2.add(new String(var5));
            }

            var5 = "";
         } else if(!var9.equals("[nl]") && !var9.equals("++")) {
            if(var7.getStringWidth(var5) + var7.getStringWidth(var9) <= var6) {
               var5 = var5 + var9 + " ";
            } else {
               var2.add(new String(var5));
               var5 = var9 + " ";
            }
         } else {
            if(var7.getStringWidth(var5) > 0) {
               var2.add(new String(var5));
            }

            var2.add("");
            var5 = "";
         }
      }

      if(var7.getStringWidth(var5) > 0) {
         var2.add(var5);
      }

      return var2;
   }

   public static String a(String var0) {
      String var1 = "";
      boolean var2 = false;

      for(int var3 = 0; var3 < var0.length(); ++var3) {
         char var4 = var0.charAt(var3);
         if(var2) {
            var2 = false;
         } else {
            if(var4 == 38 && var3 != var0.length()) {
               char var5 = var0.charAt(var3 + 1);
               boolean var6 = false;
               EnumChatFormatting[] var7 = EnumChatFormatting.values();
               int var8 = var7.length;

               for(int var9 = 0; var9 < var8; ++var9) {
                  EnumChatFormatting var10 = var7[var9];
                  if(var10.func_96298_a() == var5) {
                     var1 = var1 + var10;
                     var2 = true;
                     var6 = true;
                     break;
                  }
               }

               if(var6) {
                  continue;
               }
            }

            var1 = var1 + var4;
         }
      }

      return var1;
   }
}
