package com.f3rullo14.fds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FFReader {

   public ArrayList a(File var1) {
      if(!var1.exists()) {
         var1.getParentFile().mkdirs();

         try {
            var1.createNewFile();
         } catch (IOException var7) {
            var7.printStackTrace();
         }
      }

      ArrayList var2 = new ArrayList();

      try {
         FileReader var3 = new FileReader(var1);
         BufferedReader var4 = new BufferedReader(var3);
         StringBuffer var5 = new StringBuffer();

         String var6;
         while((var6 = var4.readLine()) != null) {
            var5.append(var6);
            var5.append("\n");
            var2.add(var6.trim());
         }

         var3.close();
      } catch (IOException var8) {
         var8.printStackTrace();
      }

      return var2;
   }
}
