package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.f3rullo14.fds.tag.FDSArrayList;
import com.f3rullo14.fds.tag.FDSBoolean;
import com.f3rullo14.fds.tag.FDSInteger;
import com.f3rullo14.fds.tag.FDSString;
import com.f3rullo14.fds.tag.FDSTagCompound;

public abstract class FDSBase {

   private static final Class[] a = new Class[32];


   public abstract void a(DataOutput var1) throws IOException;

   public abstract void a(DataInput var1) throws IOException;

   public abstract String a();

   public abstract String toString();

   public abstract Object b();

   public abstract FDSBase c();

   public static void a(DataOutput var0, String var1) throws IOException {
      var0.writeUTF(var1);
   }

   public static String b(DataInput var0) throws IOException {
      return var0.readUTF();
   }

   public static Class a(byte var0) {
      return a[var0];
   }

   public static byte a(Class var0) {
      for(int var1 = 0; var1 < a.length; ++var1) {
         if(a[var1] == var0) {
            return (byte)var1;
         }
      }

      return (byte)-1;
   }

   static {
      a[1] = FDSInteger.class;
      a[2] = FDSString.class;
      a[3] = FDSTagCompound.class;
      a[4] = FDSBoolean.class;
      a[5] = FDSArrayList.class;
   }
}
