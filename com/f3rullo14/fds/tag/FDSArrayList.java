package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.f3rullo14.fds.tag.FDSBase;

public class FDSArrayList extends FDSBase {

   public String a;
   public ArrayList b = new ArrayList();


   public FDSArrayList() {}

   public FDSArrayList(String var1, ArrayList var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(DataOutput var1) throws IOException {
      a(var1, this.a);
      var1.writeInt(this.b.size());
      Iterator var2 = this.b.iterator();

      while(var2.hasNext()) {
         Object var3 = var2.next();
         FDSBase var4 = (FDSBase)var3;
         var1.writeByte(FDSBase.a(var4.getClass()));
         var4.a(var1);
      }

   }

   public void a(DataInput var1) throws IOException {
      this.a = b(var1);
      this.b.clear();
      int var2 = var1.readInt();

      for(int var3 = 0; var3 < var2; ++var3) {
         byte var4 = var1.readByte();

         try {
            FDSBase var5 = (FDSBase)FDSBase.a(var4).newInstance();
            var5.a(var1);
            this.b.add(var5);
         } catch (Exception var6) {
            var6.printStackTrace();
         }
      }

   }

   public String a() {
      return this.a;
   }

   public String toString() {
      return "[" + this.a + ":LIST:" + this.b + "]";
   }

   public Object b() {
      return this.b;
   }

   public FDSBase c() {
      return new FDSArrayList(this.a, new ArrayList(this.b));
   }
}
