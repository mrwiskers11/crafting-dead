package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.f3rullo14.fds.tag.FDSArrayList;
import com.f3rullo14.fds.tag.FDSBase;
import com.f3rullo14.fds.tag.FDSBoolean;
import com.f3rullo14.fds.tag.FDSInteger;
import com.f3rullo14.fds.tag.FDSString;

public class FDSTagCompound extends FDSBase {

   private String a;
   private Map b = new HashMap();


   public FDSTagCompound() {}

   public FDSTagCompound(String var1) {
      this.a = var1;
   }

   public FDSTagCompound a(String var1) {
      this.a = var1;
      return this;
   }

   public int d() {
      return this.b.size();
   }

   public boolean b(String var1) {
      return this.b.containsKey(var1);
   }

   public FDSBase c(String var1) {
      return this.b.containsKey(var1)?(FDSBase)this.b.get(var1):null;
   }

   public void a(String var1, FDSTagCompound var2) {
      this.b.put(var1, var2);
   }

   public FDSTagCompound d(String var1) {
      return this.b.containsKey(var1)?(FDSTagCompound)this.b.get(var1):null;
   }

   public void a(String var1, int var2) {
      FDSInteger var3 = new FDSInteger(var1, var2);
      this.b.put(var1, var3);
   }

   public int e(String var1) {
      return this.b.containsKey(var1)?((FDSInteger)this.b.get(var1)).b:0;
   }

   public void a(String var1, String var2) {
      FDSString var3 = new FDSString(var1, var2);
      this.b.put(var1, var3);
   }

   public String f(String var1) {
      return this.b.containsKey(var1)?((FDSString)this.b.get(var1)).b:null;
   }

   public void a(String var1, boolean var2) {
      FDSBoolean var3 = new FDSBoolean(var1, var2);
      this.b.put(var1, var3);
   }

   public boolean g(String var1) {
      return (this.b.containsKey(var1)?Boolean.valueOf(((FDSBoolean)this.b.get(var1)).b):null).booleanValue();
   }

   public void a(String var1, ArrayList var2) {
      ArrayList var3 = new ArrayList();

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         var3.add(new FDSInteger("" + var4, ((Integer)var2.get(var4)).intValue()));
      }

      FDSArrayList var5 = new FDSArrayList(var1, var3);
      this.b.put(var1, var5);
   }

   public ArrayList h(String var1) {
      FDSArrayList var2 = this.b.containsKey(var1)?(FDSArrayList)this.b.get(var1):null;
      ArrayList var3 = new ArrayList();
      if(var2 != null) {
         for(int var4 = 0; var4 < var2.b.size(); ++var4) {
            var3.add(Integer.valueOf(((FDSInteger)var2.b.get(var4)).b));
         }
      }

      return var3;
   }

   public void b(String var1, ArrayList var2) {
      ArrayList var3 = new ArrayList();

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         var3.add(new FDSString("" + var4, (String)var2.get(var4)));
      }

      FDSArrayList var5 = new FDSArrayList(var1, var3);
      this.b.put(var1, var5);
   }

   public ArrayList i(String var1) {
      FDSArrayList var2 = this.b.containsKey(var1)?(FDSArrayList)this.b.get(var1):null;
      ArrayList var3 = new ArrayList();
      if(var2 != null) {
         for(int var4 = 0; var4 < var2.b.size(); ++var4) {
            var3.add(((FDSString)var2.b.get(var4)).b);
         }
      }

      return var3;
   }

   public void c(String var1, ArrayList var2) {
      ArrayList var3 = new ArrayList();

      for(int var4 = 0; var4 < var2.size(); ++var4) {
         var3.add(new FDSBoolean("" + var4, ((Boolean)var2.get(var4)).booleanValue()));
      }

      FDSArrayList var5 = new FDSArrayList(var1, var3);
      this.b.put(var1, var5);
   }

   public ArrayList j(String var1) {
      FDSArrayList var2 = this.b.containsKey(var1)?(FDSArrayList)this.b.get(var1):null;
      ArrayList var3 = new ArrayList();
      if(var2 != null) {
         for(int var4 = 0; var4 < var2.b.size(); ++var4) {
            var3.add(Boolean.valueOf(((FDSBoolean)var2.b.get(var4)).b));
         }
      }

      return var3;
   }

   public void a(DataOutput var1) throws IOException {
      Iterator var2 = this.b.keySet().iterator();
      a(var1, this.a);

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         FDSBase var4 = (FDSBase)this.b.get(var3);
         var1.writeByte(FDSBase.a(var4.getClass()));
         var4.a(var1);
      }

      var1.writeByte(0);
   }

   public void a(DataInput var1) throws IOException {
      this.b.clear();
      this.a = b(var1);

      byte var2;
      while((var2 = var1.readByte()) != 0) {
         try {
            FDSBase var3 = (FDSBase)FDSBase.a(var2).newInstance();
            var3.a(var1);
            this.b.put(var3.a(), var3);
         } catch (Exception var4) {
            System.out.println("[FDS] Bad Byte, skipping...");
            var4.printStackTrace();
         }
      }

   }

   public FDSTagCompound e() {
      FDSTagCompound var1 = new FDSTagCompound(this.a());
      ArrayList var2 = new ArrayList(this.b.values());
      Iterator var3 = var2.iterator();

      while(var3.hasNext()) {
         FDSBase var4 = (FDSBase)var3.next();
         var1.b.put(var4.a(), var4.c());
      }

      return var1;
   }

   public String a() {
      return this.a;
   }

   public String toString() {
      return "[" + this.a + ":COMPOUND:" + this.b + "]";
   }

   public Object b() {
      return this;
   }

   // $FF: synthetic method
   public FDSBase c() {
      return this.e();
   }
}
