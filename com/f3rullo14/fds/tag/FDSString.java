package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.f3rullo14.fds.tag.FDSBase;

public class FDSString extends FDSBase {

   public String a;
   public String b;


   public FDSString() {}

   public FDSString(String var1, String var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(DataOutput var1) throws IOException {
      a(var1, this.a);
      a(var1, this.b != null && this.b.length() != 0?this.b:"-");
   }

   public void a(DataInput var1) throws IOException {
      this.a = b(var1);
      this.b = b(var1);
      if(this.b.equals("-")) {
         this.b = "";
      }

   }

   public String a() {
      return this.a;
   }

   public String toString() {
      return "[" + this.a + ":STR:" + this.b + "]";
   }

   public Object b() {
      return this.b;
   }

   public FDSBase c() {
      return new FDSString(this.a, this.b);
   }
}
