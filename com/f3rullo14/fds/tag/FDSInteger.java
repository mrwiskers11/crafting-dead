package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.f3rullo14.fds.tag.FDSBase;

public class FDSInteger extends FDSBase {

   public String a;
   public int b;


   public FDSInteger() {}

   public FDSInteger(String var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(DataOutput var1) throws IOException {
      a(var1, this.a);
      a(var1, "" + this.b);
   }

   public void a(DataInput var1) throws IOException {
      this.a = b(var1);
      String var2 = b(var1);
      this.b = Integer.parseInt(var2);
   }

   public String a() {
      return this.a;
   }

   public String toString() {
      return "[" + this.a + ":INT:" + this.b + "]";
   }

   public Object b() {
      return Integer.valueOf(this.b);
   }

   public FDSBase c() {
      return new FDSInteger(this.a, this.b);
   }
}
