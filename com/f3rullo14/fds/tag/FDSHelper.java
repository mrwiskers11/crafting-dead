package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.f3rullo14.fds.tag.FDSTagCompound;

public class FDSHelper {

   private static final boolean a = false;


   public static void a(String var0, FDSTagCompound var1) {
      if(!var0.endsWith(".FDS")) {
         var0 = var0 + ".FDS";
      }

      a(new File(var0), var1);
   }

   public static void a(File var0, FDSTagCompound var1) {
      if(var0.exists()) {
         var0.delete();
      }

      try {
         var0.createNewFile();
      } catch (IOException var7) {
         var7.printStackTrace();
      }

      try {
         b("Saving Compound \'" + var1.a() + "\'...");
         FileOutputStream var2 = new FileOutputStream(var0);
         DataOutputStream var3 = new DataOutputStream(var2);

         try {
            var1.a((DataOutput)var3);
         } catch (IOException var5) {
            var5.printStackTrace();
         }

         var3.close();
         var2.close();
         b("Saved Compound \'" + var1.a() + "\'");
      } catch (Exception var6) {
         var6.printStackTrace();
      }

   }

   public static FDSTagCompound a(String var0) {
      if(!var0.endsWith(".FDS")) {
         var0 = var0 + ".FDS";
      }

      return a(new File(var0));
   }

   public static FDSTagCompound a(File var0) {
      try {
         if(var0.exists()) {
            b("Loading Compound...");
            FileInputStream var1 = new FileInputStream(var0);
            DataInputStream var2 = new DataInputStream(var1);
            FDSTagCompound var3 = new FDSTagCompound();
            var3.a((DataInput)var2);
            var2.close();
            var1.close();
            b("Loaded Compound \'" + var3.a() + "\'...");
            return var3;
         }
      } catch (Exception var4) {
         b("Failed to Load a Compound!");
      }

      return new FDSTagCompound();
   }

   private static void b(String var0) {}
}
