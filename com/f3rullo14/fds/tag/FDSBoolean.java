package com.f3rullo14.fds.tag;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.f3rullo14.fds.tag.FDSBase;

public class FDSBoolean extends FDSBase {

   public String a;
   public boolean b;


   public FDSBoolean() {}

   public FDSBoolean(String var1, boolean var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a(DataOutput var1) throws IOException {
      a(var1, this.a);
      a(var1, this.b?"true":"false");
   }

   public void a(DataInput var1) throws IOException {
      this.a = b(var1);
      String var2 = b(var1);
      this.b = var2.equals("true");
   }

   public String a() {
      return this.a;
   }

   public String toString() {
      return "[" + this.a + ":BOOL:" + this.b + "]";
   }

   public Object b() {
      return Boolean.valueOf(this.b);
   }

   public FDSBase c() {
      return new FDSBoolean(this.a, this.b);
   }
}
