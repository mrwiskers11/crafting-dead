package com.f3rullo14.fds;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

public class FDSCompound {

   private File a;
   private ArrayList b = new ArrayList();
   private String c = "&&";


   public FDSCompound(String var1) {
      this.a = new File(var1);
   }

   public FDSCompound(File var1) {
      this.a = var1;
   }

   public void a() {
      File var1 = new File(this.a.getAbsolutePath());
      if(var1.exists()) {
         this.b.clear();
         this.b = this.a(var1);
      }

   }

   public void b() {
      this.b.clear();
   }

   public void c() {
      String var1 = this.a.getAbsolutePath();
      File var2 = new File(var1);
      File var3 = new File(this.a.getParentFile().getPath());

      try {
         if(!var3.exists()) {
            var3.mkdirs();
         }

         if(var2.exists()) {
            var2.delete();
         }

         File var4 = new File(var1);
         if(!var4.exists()) {
            var4.createNewFile();
         }

         FileWriter var5 = new FileWriter(var1, true);
         BufferedWriter var6 = new BufferedWriter(var5);
         if(this.b.size() > 0) {
            for(int var7 = 0; var7 < this.b.size(); ++var7) {
               var6.write((String)this.b.get(var7));
               if(var7 != this.b.size() - 1) {
                  var6.newLine();
               }
            }
         }

         var6.close();
      } catch (MalformedURLException var8) {
         var8.printStackTrace();
      } catch (IOException var9) {
         var9.printStackTrace();
      }

   }

   public boolean a(String var1) {
      if(this.b.size() > 0) {
         for(int var2 = 0; var2 < this.b.size(); ++var2) {
            String var3 = (String)this.b.get(var2);
            String[] var4 = var3.split("&&");
            if(var4[0].equals(var1)) {
               return true;
            }
         }
      }

      return false;
   }

   public int b(String var1) {
      if(this.b.size() > 0) {
         for(int var2 = 0; var2 < this.b.size(); ++var2) {
            String var3 = (String)this.b.get(var2);
            String[] var4 = var3.split("&&");
            if(var4.length == 2 && var1.equals(var4[0])) {
               return Integer.parseInt(var4[1]);
            }
         }
      }

      return 0;
   }

   public void a(String var1, int var2) {
      this.b(var1, "" + var2);
   }

   public boolean c(String var1) {
      if(this.b.size() > 0) {
         for(int var2 = 0; var2 < this.b.size(); ++var2) {
            String var3 = (String)this.b.get(var2);
            String[] var4 = var3.split(this.c);
            if(var4.length == 2 && var1.equals(var4[0])) {
               return var4[1].equals("true");
            }
         }
      }

      return false;
   }

   public void a(String var1, boolean var2) {
      this.b(var1, "" + var2);
   }

   public String d(String var1) {
      if(this.b.size() > 0) {
         for(int var2 = 0; var2 < this.b.size(); ++var2) {
            String var3 = (String)this.b.get(var2);
            String[] var4 = var3.split(this.c);
            if(var4.length == 2 && var1.equals(var4[0])) {
               return var4[1];
            }
         }
      }

      return "";
   }

   public void a(String var1, String var2) {
      this.b(var1, var2);
   }

   private void b(String var1, String var2) {
      this.b.add(var1 + this.c + var2);
   }

   private ArrayList a(File var1) {
      ArrayList var2 = new ArrayList();

      try {
         BufferedReader var3 = new BufferedReader(new FileReader(var1));
         Throwable var4 = null;

         try {
            StringBuffer var5 = new StringBuffer();

            String var6;
            while((var6 = var3.readLine()) != null) {
               var5.append(var6);
               var5.append("\n");
               var2.add(var6);
            }
         } catch (Throwable var15) {
            var4 = var15;
            throw var15;
         } finally {
            if(var3 != null) {
               if(var4 != null) {
                  try {
                     var3.close();
                  } catch (Throwable var14) {
                     var4.addSuppressed(var14);
                  }
               } else {
                  var3.close();
               }
            }

         }
      } catch (IOException var17) {
         var17.printStackTrace();
      }

      return var2;
   }
}
