package com.f3rullo14.cloud.client;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpConnection;
import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.client.ConnectThread;
import com.f3rullo14.cloud.client.IClientCloudConnection;
import com.f3rullo14.cloud.packet.PacketClientHeartBeat;
import com.f3rullo14.cloud.packet.PacketClientLogin;
import com.f3rullo14.cloud.packet.PacketClientLogout;
import com.f3rullo14.cloud.packet.PacketCloudDisconnected;

import java.io.DataOutputStream;
import java.net.Socket;

public class G_ClientTcpConnectionHandler extends G_ITcpConnectionHandler {

   private boolean a;
   private String b;
   private int c;
   private G_TcpConnection d;
   private volatile int e = 600;
   private IClientCloudConnection f;
   private int g = 0;
   private boolean h = false;
   private int i = 0;
   private static boolean j = false;


   public G_ClientTcpConnectionHandler(IClientCloudConnection var1) {
      this.f = var1;
      this.b = var1.e();
      this.c = var1.f();
      this.d();
   }

   public void c() {
      ConnectThread var1 = new ConnectThread(this);
      var1.start();
   }

   public void d() {
      if(!this.g() && !j) {
         j = true;

         try {
            if(this.f.g()) {
               Socket var1 = new Socket(this.b, this.c);
               System.out.println("[G_ClientTcpConnectionHandler] Connecting...");
               this.d = new G_TcpConnection(var1, this.a(), this);
               DataOutputStream var2 = new DataOutputStream(var1.getOutputStream());
               var2.writeUTF(this.f.d());
               var2.writeUTF(this.f.b());
               var2.writeUTF(this.f.c());
               System.out.println("[G_ClientTcpConnectionHandler] Connection Established to cloud!");
               this.h = false;
            }
         } catch (Exception var3) {
            System.out.println("[G_ClientTcpConnectionHandler] Failed to Establish Connection to the cloud!");
         }

         j = false;
      }

   }

   public void e() {
      if(this.g()) {
         if(!this.h) {
            System.out.println("[G_ClientTcpConnectionHandler] Sending Login Information");
            PacketClientLogin var1 = new PacketClientLogin();
            var1.e = this.a();
            this.f.a(var1.f);
            this.c((G_TcpPacket)var1);
            this.h = true;
         }

         if(this.g++ > 20) {
            this.c((G_TcpPacket)(new PacketClientHeartBeat()));
            this.g = 0;
         }

         this.f();
      } else {
         this.h = false;
         if(this.f.h() && this.i++ > 600) {
            this.f.i();
            this.i = 0;
         }
      }

   }

   public void f() {
      if(this.e <= 0) {
         this.c("No message from cloud.");
      }

      if(!this.a && this.d != null) {
         this.d.b();
      }

      if(this.d != null) {
         this.d.a();
      }

   }

   public void c(G_TcpPacket var1) {
      if(!this.a && this.d != null) {
         this.d(var1);
      }

   }

   private void d(G_TcpPacket var1) {
      this.d.a(var1);
   }

   public void b(String var1) {
      this.a = true;
      if(this.d != null) {
         this.d.a((G_TcpPacket)(new PacketClientLogout()));
         this.d.a();
         this.d.a("disconnect.closed");
         this.d = null;
      }

   }

   private void c(String var1) {
      this.a = true;
      if(this.d != null) {
         this.d(var1);
         this.d.a();
         this.d.a("disconnect.closed");
         this.d = null;
      }

   }

   public boolean g() {
      return G_TcpConnection.i(this.d);
   }

   public boolean a(G_TcpPacket var1) {
      return true;
   }

   public void b(G_TcpPacket var1) {
      if(var1 instanceof PacketCloudDisconnected) {
         this.c(((PacketCloudDisconnected)var1).c());
      } else {
         this.f.a(var1);
         this.e = 600;
      }
   }

   private void d(String var1) {
      this.f.a(var1);
   }

   public String a() {
      return this.f.b();
   }

   public void a(String var1) {
      if(!this.a) {
         this.a = true;
         System.out.println("[G_ClientTcpConnectionHandler] Error Message");
         System.out.println("[G_ClientTcpConnectionHandler] " + var1);
      }

   }

   public String b() {
      return this.f.c();
   }

}
