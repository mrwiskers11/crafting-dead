package com.f3rullo14.cloud.client;

import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.fds.tag.FDSTagCompound;

public interface IClientCloudConnection {

   String b();

   String c();

   String d();

   String e();

   int f();

   boolean g();

   void a(FDSTagCompound var1);

   void a(String var1);

   void a(G_TcpPacket var1);

   void a(boolean var1);

   boolean h();

   void i();
}
