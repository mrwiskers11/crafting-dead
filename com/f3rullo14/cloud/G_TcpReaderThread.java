package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_TcpConnection;

class G_TcpReaderThread extends Thread {

   final G_TcpConnection a;


   G_TcpReaderThread(G_TcpConnection var1, String var2) {
      super(var2);
      this.a = var1;
   }

   public void run() {
      G_TcpConnection.a.getAndIncrement();

      try {
         while(G_TcpConnection.a(this.a) && !G_TcpConnection.b(this.a)) {
            if(!G_TcpConnection.c(this.a)) {
               try {
                  sleep(2L);
               } catch (InterruptedException var5) {
                  ;
               }
            }
         }
      } finally {
         G_TcpConnection.a.getAndDecrement();
      }

   }
}
