package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_TcpConnection;
import java.io.IOException;

class G_TcpWriterThread extends Thread {

   final G_TcpConnection a;


   G_TcpWriterThread(G_TcpConnection var1, String var2) {
      super(var2);
      this.a = var1;
   }

   public void run() {
      G_TcpConnection.b.getAndIncrement();

      try {
         while(G_TcpConnection.a(this.a)) {
            boolean var1;
            for(var1 = false; G_TcpConnection.d(this.a); var1 = true) {
               ;
            }

            try {
               if(var1 && G_TcpConnection.e(this.a) != null) {
                  G_TcpConnection.e(this.a).flush();
               }
            } catch (IOException var8) {
               if(!G_TcpConnection.f(this.a)) {
                  G_TcpConnection.a(this.a, var8);
               }

               var8.printStackTrace();
            }

            try {
               sleep(2L);
            } catch (InterruptedException var7) {
               ;
            }
         }
      } finally {
         G_TcpConnection.b.getAndDecrement();
      }

   }
}
