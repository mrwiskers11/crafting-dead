package com.f3rullo14.cloud.packet;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import com.f3rullo14.fds.tag.FDSTagCompound;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketClientLogin extends G_TcpPacketCustomPayload {

   public String e;
   public FDSTagCompound f = new FDSTagCompound("login");


   public void a(DataOutputStream var1) throws IOException {
      if(G_TcpPacket.b) {
         var1.writeUTF(this.e);
         if(this.f != null) {
            this.f.a((DataOutput)var1);
         }
      }

   }

   public void a(DataInputStream var1) throws IOException {
      if(!G_TcpPacket.b) {
         this.e = var1.readUTF();
         this.f.a((DataInput)var1);
      }

   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {}
}
