package com.f3rullo14.cloud.packet;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketCloudDisconnected extends G_TcpPacketCustomPayload {

   private String e = "Disconnected.";


   public PacketCloudDisconnected() {}

   public PacketCloudDisconnected(String var1) {
      this.e = var1;
   }

   public void a(DataOutputStream var1) throws IOException {
      var1.writeUTF(this.e);
   }

   public void a(DataInputStream var1) throws IOException {
      this.e = var1.readUTF();
   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {}

   public String c() {
      return this.e;
   }
}
