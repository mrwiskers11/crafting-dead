package com.f3rullo14.cloud.example;

import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.client.G_ClientTcpConnectionHandler;
import com.f3rullo14.cloud.client.IClientCloudConnection;
import com.f3rullo14.fds.tag.FDSTagCompound;

public class Client implements IClientCloudConnection {

   private boolean a = true;
   private G_ClientTcpConnectionHandler b = new G_ClientTcpConnectionHandler(this);


   public void a() {
      this.b.e();
   }

   public void j() {
      this.b.b("Logging Out");
   }

   public String b() {
      return "test";
   }

   public String e() {
      return "localhost";
   }

   public int f() {
      return 1009;
   }

   public void a(FDSTagCompound var1) {}

   public void a(String var1) {
      System.out.println(var1);
   }

   public boolean h() {
      return true;
   }

   public void a(boolean var1) {}

   public void a(G_TcpPacket var1) {}

   public static void main(String[] var0) {
      System.out.println("[Client] Starting...");
      Client var1 = new Client();
      System.out.println("[Client] Running Updates to the Program...");

      while(var1.a) {
         var1.a();

         try {
            Thread.sleep(50L);
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         }
      }

      System.out.println("[Client] Shutting Down...");
      var1.j();
      System.exit(0);
   }

   public void i() {}

   public String d() {
      return "user";
   }

   public String c() {
      return "1.0.0";
   }

   public boolean g() {
      return true;
   }
}
