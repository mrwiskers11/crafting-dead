package com.f3rullo14.cloud.example;

import com.f3rullo14.cloud.example.CloudPacketHandler;
import com.f3rullo14.cloud.server.G_CloudManager;
import com.f3rullo14.cloud.server.G_CloudTcpConnectionHandler;
import com.f3rullo14.cloud.server.ICloud;
import com.f3rullo14.cloud.server.ICloudPacketHandler;

import java.util.ArrayList;

public class Cloud implements ICloud {

   public boolean a = true;
   private G_CloudManager b = new G_CloudManager(this);


   public Cloud() {
      this.b.c().a((ICloudPacketHandler)(new CloudPacketHandler()));
   }

   public void a() {
      this.b.a();
   }

   public void b() {
      this.b.b();
   }

   public String c() {
      return "Cloud9";
   }

   public String d() {
      return "1.0.0";
   }

   public int e() {
      return 1009;
   }

   public void a(G_CloudTcpConnectionHandler var1) {
      this.b.b("User: " + var1.a() + " connected!");
   }

   public void b(G_CloudTcpConnectionHandler var1) {}

   public void a(String var1) {}

   public static void main(String[] var0) {
      System.out.println("[Cloud] Starting...");
      Cloud var1 = new Cloud();
      System.out.println("[Cloud] Running Updates to the Program...");

      while(var1.a) {
         var1.a();

         try {
            Thread.sleep(50L);
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         }
      }

      System.out.println("[Cloud] Shutting Down...");
      var1.b();
      System.exit(0);
   }

   public boolean c(G_CloudTcpConnectionHandler var1) {
      return true;
   }

   public void a(ArrayList var1) {}
}
