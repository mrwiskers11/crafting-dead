package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_TcpConnection;

class G_TcpMasterThread extends Thread {

   final G_TcpConnection a;


   G_TcpMasterThread(G_TcpConnection var1) {
      this.a = var1;
   }

   public void run() {
      try {
         Thread.sleep(5000L);
         if(this.a != null) {
            if(G_TcpConnection.g(this.a) != null && G_TcpConnection.g(this.a).isAlive()) {
               try {
                  G_TcpConnection.g(this.a).stop();
               } catch (Throwable var3) {
                  ;
               }
            }

            if(G_TcpConnection.h(this.a) != null && G_TcpConnection.h(this.a).isAlive()) {
               try {
                  G_TcpConnection.h(this.a).stop();
               } catch (Throwable var2) {
                  ;
               }
            }
         }
      } catch (InterruptedException var4) {
         var4.printStackTrace();
      } catch (Exception var5) {
         ;
      }

   }
}
