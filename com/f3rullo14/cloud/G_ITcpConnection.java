package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacket;
import java.net.SocketAddress;

public interface G_ITcpConnection {

   void a(G_ITcpConnectionHandler var1);

   void a(G_TcpPacket var1);

   void a();

   void b();

   SocketAddress c();

   void d();

   int e();

   void a(String var1);

   void f();
}
