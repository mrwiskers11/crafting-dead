package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_TcpPacket;

public abstract class G_ITcpConnectionHandler {

   public abstract boolean a(G_TcpPacket var1);

   public abstract void b(G_TcpPacket var1);

   public abstract String a();

   public abstract String b();

   public abstract void a(String var1);

   public abstract void c(G_TcpPacket var1);
}
