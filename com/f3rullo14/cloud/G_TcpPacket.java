package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.packet.PacketClientHeartBeat;
import com.f3rullo14.cloud.packet.PacketClientLogin;
import com.f3rullo14.cloud.packet.PacketClientLogout;
import com.f3rullo14.cloud.packet.PacketCloudDisconnected;
import com.f3rullo14.cloud.packet.PacketCloudHeartBeat;
import com.f3rullo14.cloud.util.IntHashMap;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

public abstract class G_TcpPacket {

   public static IntHashMap a = new IntHashMap();
   private static Map c = new HashMap();
   public static boolean b = true;
   private static final boolean d = false;


   public static void a(int var0, Class var1) {
      if(a.b(var0)) {
         System.out.println("[G_TcpPacket] PACKET ERROR: Overriding Packet ID [" + var0 + "] with a new packet!");
      }

      a.a(var0, var1);
      c.put(var1, Integer.valueOf(var0));
   }

   public static G_TcpPacket a(DataInput var0, Socket var1) throws IOException {
      G_TcpPacket var2 = null;

      try {
         int var3 = var0.readUnsignedByte();
         if(!a.b(var3)) {
            throw new IOException("[G_TcpPacket] Bad packet id " + var3);
         }

         var2 = a(var3);
         if(var2 == null) {
            throw new IOException("[G_TcpPacket] Bad packet id " + var3);
         }

         var2.b(var0);
      } catch (EOFException var5) {
         return null;
      } catch (SocketTimeoutException var6) {
         return null;
      } catch (SocketException var7) {
         return null;
      } catch (Exception var8) {
         return null;
      }

      var1.setSoTimeout(30000);
      return var2;
   }

   public static void a(DataOutput var0, String var1) throws IOException {
      if(var1.length() > 32767) {
         throw new IOException("String too big");
      } else {
         var0.writeShort(var1.length());
         var0.writeChars(var1);
      }
   }

   public static String a(DataInput var0) throws IOException {
      short var1 = var0.readShort();
      StringBuilder var2 = new StringBuilder();

      for(int var3 = 0; var3 < var1; ++var3) {
         var2.append(var0.readChar());
      }

      return var2.toString();
   }

   public static G_TcpPacket a(int var0) {
      try {
         if(!a.b(var0)) {
            return null;
         }

         Class var1 = (Class)a.a(var0);
         return (G_TcpPacket)var1.newInstance();
      } catch (InstantiationException var2) {
         var2.printStackTrace();
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
      }

      return null;
   }

   public static void a(G_TcpPacket var0, DataOutput var1) throws IOException {
      var1.write(var0.a());
      var0.a(var1);
   }

   public final int a() {
      return ((Integer)c.get(this.getClass())).intValue();
   }

   public abstract void a(DataOutput var1) throws IOException;

   public abstract void b(DataInput var1) throws IOException;

   public abstract void b(G_ITcpConnectionHandler var1);

   public abstract int b();

   static {
      a(2, PacketClientLogin.class);
      a(3, PacketClientHeartBeat.class);
      a(4, PacketClientLogout.class);
      a(5, PacketCloudHeartBeat.class);
      a(6, PacketCloudDisconnected.class);
   }
}
