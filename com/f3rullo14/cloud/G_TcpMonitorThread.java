package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_TcpConnection;

class G_TcpMonitorThread extends Thread {

   final G_TcpConnection a;


   G_TcpMonitorThread(G_TcpConnection var1) {
      this.a = var1;
   }

   public void run() {
      try {
         Thread.sleep(2000L);
         if(G_TcpConnection.a(this.a)) {
            G_TcpConnection.h(this.a).interrupt();
            this.a.a("disconnect.closed");
         }
      } catch (Exception var2) {
         var2.printStackTrace();
      }

   }
}
