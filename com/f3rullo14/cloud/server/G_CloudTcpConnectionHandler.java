package com.f3rullo14.cloud.server;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpConnection;
import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.packet.PacketClientHeartBeat;
import com.f3rullo14.cloud.packet.PacketClientLogin;
import com.f3rullo14.cloud.packet.PacketClientLogout;
import com.f3rullo14.cloud.packet.PacketCloudDisconnected;
import com.f3rullo14.cloud.packet.PacketCloudHeartBeat;
import com.f3rullo14.cloud.server.G_CloudNetworkManager;
import com.f3rullo14.cloud.server.ICloudPacketHandler;

import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;

public class G_CloudTcpConnectionHandler extends G_ITcpConnectionHandler {

   public G_TcpConnection a;
   private String i = "[connecting]";
   private String j = "1.0.0";
   public boolean b = false;
   public int c = 0;
   public boolean d = false;
   public int e = 0;
   public int f = 0;
   public String g;
   public G_CloudNetworkManager h;


   public G_CloudTcpConnectionHandler(String var1, Socket var2, String var3, String var4) throws IOException {
      this.g = var1;
      this.i = var3;
      this.j = var4;
      this.a = new G_TcpConnection(var2, "CloudClientConnection", this);
      this.c = 0;
   }

   public void c() {
      if(!this.d) {
         if(this.b) {
            this.d();
            if(this.e++ >= 600) {
               this.c("Client Pulsing Stopped.");
               this.e = 0;
            }
         } else if(this.c++ >= 8000) {
            this.c("Login timed out.");
         } else {
            this.d();
         }
      }

   }

   public void d() {
      if(this.a != null) {
         this.a.b();
         this.a.a();
      }

   }

   public void b(String var1) {
      System.out.println("[G_CloudTcpConnectionHandler] Kicking Connection: " + var1);
      this.a.a((G_TcpPacket)(new PacketCloudDisconnected(var1)));
      this.a.a();
      this.c(var1);
   }

   private void c(String var1) {
      try {
         if(this.a != null) {
            this.a.d();
            this.a.a("Disconnecting Player");
         }

         this.d = true;
      } catch (Exception var3) {
         var3.printStackTrace();
      }

   }

   public boolean a(G_TcpPacket var1) {
      if(!this.b) {
         System.out.println("[G_CloudTcpConnectionHandler] Waiting for login packet...");
         return var1 instanceof PacketClientLogin;
      } else {
         return true;
      }
   }

   public void b(G_TcpPacket var1) {
      this.e = 0;
      if(!this.b) {
         if(var1 instanceof PacketClientLogin) {
            this.a((PacketClientLogin)var1);
         }
      } else {
         if(var1 instanceof PacketClientHeartBeat) {
            this.e();
            return;
         }

         if(var1 instanceof PacketClientLogout) {
            this.c("Logout");
            return;
         }

         if(this.h.b.size() > 0) {
            Iterator var2 = this.h.b.iterator();

            while(var2.hasNext()) {
               ICloudPacketHandler var3 = (ICloudPacketHandler)var2.next();
               var3.a(this, var1);
            }
         }
      }

   }

   public void e() {
      this.c((G_TcpPacket)(new PacketCloudHeartBeat()));
   }

   public void a(PacketClientLogin var1) {
      if(this.i.equals(var1.e) && this.i != null && this.i.length() > 0 && this.d(this.i)) {
         this.b = true;
         System.out.println("[G_CloudTcpConnectionHandler] " + this.i + " successfully connected!");

         try {
            this.h.a(this);
         } catch (Exception var3) {
            System.out.println("FAILED TO HANDLE METHOD \'onUserLoginSuccessful\'");
            var3.printStackTrace();
         }
      } else {
         this.c("[G_CloudTcpConnectionHandler] Failed to varify connection username.");
      }

   }

   private boolean d(String var1) {
      if(var1.contains(" ")) {
         return false;
      } else {
         for(int var2 = 0; var2 < var1.length(); ++var2) {
            if(!Character.isLetter(var1.charAt(var2)) && !Character.isDigit(var1.charAt(var2)) && !this.a(var1.charAt(var2))) {
               return false;
            }
         }

         return true;
      }
   }

   private boolean a(char var1) {
      char[] var2 = new char[]{'_', '-'};

      for(int var3 = 0; var3 < var2.length; ++var3) {
         if(var1 == var2[var3]) {
            return true;
         }
      }

      return false;
   }

   public void a(String var1) {
      if(!this.d) {
         this.d = true;
         if(var1.equals("disconnect.endOfStream")) {
            System.out.println("[G_CloudTcpConnectionHandler] " + this.i + "\'s stream closed.");
            return;
         }

         System.out.println("[G_CloudTcpConnectionHandler] " + var1);
      }

   }

   public String a() {
      return this.i;
   }

   public void c(G_TcpPacket var1) {
      this.a.a(var1);
   }

   public String b() {
      return this.j;
   }
}
