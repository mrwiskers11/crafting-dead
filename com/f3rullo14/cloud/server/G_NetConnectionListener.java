package com.f3rullo14.cloud.server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.f3rullo14.cloud.server.G_CloudNetworkManager;
import com.f3rullo14.cloud.server.G_CloudTcpConnectionHandler;

public class G_NetConnectionListener extends Thread {

   private ServerSocket b = null;
   public G_CloudNetworkManager a;
   private final List c = Collections.synchronizedList(new ArrayList());
   private int d = 1490;
   private boolean e = true;
   private boolean f = true;


   public G_NetConnectionListener(int var1, G_CloudNetworkManager var2) {
      this.d = var1;
      this.a = var2;
   }

   public void a() {
      List var1 = this.c;
      List var2 = this.c;
      synchronized(this.c) {
         for(int var3 = 0; var3 < this.c.size(); ++var3) {
            G_CloudTcpConnectionHandler var4 = (G_CloudTcpConnectionHandler)this.c.get(var3);
            if(var4 != null && var4.a != null) {
               this.a.a(var4.g, var4);
               this.c.remove(var3);
            } else {
               System.out.println("[G_NetConnectionListener] Error! Invalid player connection hanlder trying to be initalized!");
            }
         }

      }
   }

   public void run() {
      System.out.println("[G_NetConnectionListener] Now Accepting Pending Connections...");

      while(this.e) {
         this.c();
         this.b();

         try {
            Socket var1 = this.b.accept();
            DataInputStream var2 = new DataInputStream(var1.getInputStream());
            String var3 = var2.readUTF();
            String var4 = var2.readUTF();
            String var5 = var2.readUTF();
            G_CloudTcpConnectionHandler var6 = new G_CloudTcpConnectionHandler(var3, var1, var4, var5);
            System.out.println("[G_NetConnectionListener] (1/2) Potential connection established.");
            boolean var7 = false;

            try {
               G_CloudNetworkManager var8 = this.a;
               synchronized(this.a) {
                  if(!this.a.d().e().c(var6)) {
                     var7 = true;
                  }
               }
            } catch (Exception var12) {
               System.out.println("[G_NetConnectionListener] FAILED HANDLE METHOD \'onConnectionReached\'");
            }

            if(var7) {
               try {
                  System.out.println("[G_NetConnectionListener] (2/2) Discarding Connection...");
                  var6.b("Discarding");
                  var1.close();
               } catch (Exception var10) {
                  ;
               }
            } else {
               System.out.println("[G_NetConnectionListener] (2/2) Adding potential connection to main thread.");
               this.a(var6);
            }
         } catch (IOException var13) {
            var13.printStackTrace();
         } catch (Exception var14) {
            var14.printStackTrace();
         }
      }

      System.err.println("[WARNING] [G_NetConnectionListener] Closing Connection Listener Thread!");
   }

   public void a(G_CloudTcpConnectionHandler var1) {
      if(var1 != null) {
         List var2 = this.c;
         List var3 = this.c;
         synchronized(this.c) {
            this.c.add(var1);
         }
      }

   }

   public void b() {
      try {
         int var1 = this.d;
         if(var1 < 10000 && var1 != 0 && this.b == null) {
            this.b = new ServerSocket(var1);
         }
      } catch (Exception var2) {
         var2.printStackTrace();
      }

   }

   public void c() {
      try {
         sleep(5L);
      } catch (InterruptedException var2) {
         var2.printStackTrace();
      }

   }
}
