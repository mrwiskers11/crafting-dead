package com.f3rullo14.cloud.server;

import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.server.G_CloudNetworkManager;
import com.f3rullo14.cloud.server.ICloud;
import com.f3rullo14.cloud.server.gui.ConnectionListComponent;
import com.f3rullo14.cloud.server.gui.LogAgent;
import com.f3rullo14.cloud.server.gui.ServerGui;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class G_CloudManager {

   public String a = "";
   public String b = "";
   private G_CloudNetworkManager f;
   private int g;
   public int c = 0;
   private int h = 0;
   public ConnectionListComponent d;
   private LogAgent i;
   public boolean e = false;
   private String j;
   private ICloud k;


   public G_CloudManager(ICloud var1) {
      this.k = var1;
      this.a = var1.c();
      this.b = var1.d();
      this.g = var1.e();
      this.h();
      this.i();
      this.g();
      this.f();
      G_TcpPacket.b = false;
   }

   public void a() {
      if(this.h++ > 20) {
         this.c = this.f.e();
         this.h = 0;
      }

      this.d.a();
      this.f.a();
   }

   public void b() {
      this.f.b();
      this.e = true;
   }

   public void a(String var1) {
      if(var1.startsWith("/")) {
         this.e().a(var1);
      }

   }

   public ArrayList a(ArrayList var1) {
      var1.add(this.a);
      var1.add(this.b);
      var1.add("");

      for(int var2 = 0; var2 < this.f.c().size(); ++var2) {
         String var3 = (String)this.f.c().get(var2);
         String var4 = "" + this.f.a(var3).size();
         var1.add(var3 + ": " + var4);
      }

      return var1;
   }

   public void a(ConnectionListComponent var1) {
      this.d = var1;
   }

   public G_CloudNetworkManager c() {
      return this.f;
   }

   public LogAgent d() {
      return this.i;
   }

   public ICloud e() {
      return this.k;
   }

   public void b(String var1) {
      this.d().a(var1);
   }

   public void f() {
      this.d().a("Starting Cloud [" + this.a + "]...");
      this.d().a("Version " + this.b);
      this.d().a("Running Java Version: " + System.getProperty("java.version"));
      this.d().a("Starting Network Manager...");
      this.f = new G_CloudNetworkManager(this, this.g);
      this.d().a("Network Manager Loaded!");
   }

   public void g() {
      ServerGui.a(this);
   }

   public void h() {
      this.j = System.getenv("AppData");
      this.j = this.j + "/.ferullogaming/Cloud/CraftingDead/";
      File var1 = new File(this.j);
      if(!var1.exists()) {
         var1.mkdirs();
      }

   }

   public void i() {
      SimpleDateFormat var1 = new SimpleDateFormat("yyyy-MM-dd");
      Date var2 = new Date();
      String var3 = var1.format(var2);
      int var4 = 0;
      File var5 = new File(this.j + "serverlog-" + var3 + "-" + var4 + ".txt");
      boolean var6 = true;

      while(var6) {
         ++var4;
         var5 = new File(this.j + "serverlog-" + var3 + "-" + var4 + ".txt");
         if(!var5.exists()) {
            var6 = false;
         }
      }

      try {
         var5.createNewFile();
      } catch (IOException var8) {
         var8.printStackTrace();
      }

      this.i = new LogAgent("CCCloud", (String)null, var5.getAbsolutePath());
      this.i.a();
   }
}
