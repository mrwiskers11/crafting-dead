package com.f3rullo14.cloud.server;

import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.server.G_CloudManager;
import com.f3rullo14.cloud.server.G_CloudTcpConnectionHandler;
import com.f3rullo14.cloud.server.G_NetConnectionListener;
import com.f3rullo14.cloud.server.ICloudPacketHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class G_CloudNetworkManager {

   public G_NetConnectionListener a;
   private volatile HashMap c = new HashMap();
   public ArrayList b = new ArrayList();
   private G_CloudManager d;


   public G_CloudNetworkManager(G_CloudManager var1, int var2) {
      this.d = var1;
      this.c = new HashMap();
      this.a = new G_NetConnectionListener(var2, this);
      this.a.start();
   }

   public void a() {
      this.a.a();
      Iterator var1 = this.c.keySet().iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         ArrayList var3 = (ArrayList)this.c.get(var2);

         for(int var4 = 0; var4 < var3.size(); ++var4) {
            G_CloudTcpConnectionHandler var5 = (G_CloudTcpConnectionHandler)var3.get(var4);

            try {
               var5.c();
            } catch (Exception var7) {
               var5.b("Failed to process a packet.");
               var7.printStackTrace();
            }

            if(var5.d) {
               this.d.e().b(var5);
               var3.remove(var4);
            }
         }
      }

   }

   public void b() {
      Iterator var1 = this.c.keySet().iterator();

      while(var1.hasNext()) {
         String var2 = (String)var1.next();
         ArrayList var3 = (ArrayList)this.c.get(var2);

         for(int var4 = 0; var4 < var3.size(); ++var4) {
            G_CloudTcpConnectionHandler var5 = (G_CloudTcpConnectionHandler)var3.get(var4);
            var5.b("Cloud Restarting");
            if(var5.d) {
               this.d.e().b(var5);
               var3.remove(var4);
            }

            var5.a.a();
         }
      }

   }

   public void a(G_CloudTcpConnectionHandler var1) {
      this.d.e().a(var1);
   }

   public boolean a(String var1, String var2) {
      return this.b(var1, var2) != null;
   }

   public G_CloudTcpConnectionHandler b(String var1, String var2) {
      ArrayList var3 = this.a(var1);

      for(int var4 = 0; var4 < var3.size(); ++var4) {
         if(var3.get(var4) != null && ((G_CloudTcpConnectionHandler)var3.get(var4)).a() != null && ((G_CloudTcpConnectionHandler)var3.get(var4)).a().equals(var2)) {
            return (G_CloudTcpConnectionHandler)var3.get(var4);
         }
      }

      return null;
   }

   public void a(String var1, G_CloudTcpConnectionHandler var2) {
      ArrayList var3 = this.a(var1);
      if(!var3.contains(var2)) {
         var2.h = this;
         var3.add(var2);
      }

   }

   public void a(String var1, String var2, G_TcpPacket var3) {
      G_CloudTcpConnectionHandler var4 = this.b(var1, var2);
      if(var4 != null && var3 != null) {
         var4.c(var3);
      }

   }

   public ArrayList a(String var1) {
      if(!this.c.containsKey(var1)) {
         this.c.put(var1, new ArrayList());
      }

      return (ArrayList)this.c.get(var1);
   }

   public ArrayList c() {
      return new ArrayList(this.c.keySet());
   }

   public void a(ICloudPacketHandler var1) {
      this.b.add(var1);
   }

   public G_CloudManager d() {
      return this.d;
   }

   public int e() {
      int var1 = 0;

      String var3;
      for(Iterator var2 = this.c.keySet().iterator(); var2.hasNext(); var1 += ((ArrayList)this.c.get(var3)).size()) {
         var3 = (String)var2.next();
      }

      return var1;
   }

   public static void b(String var0) {
      System.out.println("[CNM] " + var0);
   }
}
