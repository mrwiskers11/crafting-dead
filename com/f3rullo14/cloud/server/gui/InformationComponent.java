package com.f3rullo14.cloud.server.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.Timer;

import com.f3rullo14.cloud.server.G_CloudManager;
import com.f3rullo14.cloud.server.gui.InformationComponentINNER1;

public class InformationComponent extends JComponent {

   private static final DecimalFormat a = new DecimalFormat("########0.000");
   private int[] b = new int[256];
   private int c;
   private ArrayList d = new ArrayList();
   private ArrayList e = new ArrayList();
   private final G_CloudManager f;


   public InformationComponent(G_CloudManager var1) {
      this.f = var1;
      this.setPreferredSize(new Dimension(456, 246));
      this.setMinimumSize(new Dimension(456, 246));
      this.setMaximumSize(new Dimension(456, 246));
      (new Timer(100, new InformationComponentINNER1(this))).start();
      this.setBackground(Color.BLACK);
   }

   private void a() {
      this.d.clear();
      this.e.clear();
      System.gc();
      Runtime var1 = Runtime.getRuntime();
      NumberFormat var2 = NumberFormat.getInstance();
      double var3 = (double)var1.maxMemory() / 1024.0D / 1024.0D;
      double var5 = (double)var1.totalMemory() / 1024.0D / 1024.0D;
      double var7 = (double)var1.freeMemory() / 1024.0D / 1024.0D;
      double var9 = var5 + var3 + 500.0D;
      Set var11 = Thread.getAllStackTraces().keySet();
      this.d.addAll(this.f.a(new ArrayList()));
      this.e.add("Alloc Mem Usage: " + var2.format((long)(var5 - var7)) + "mb (" + var2.format((var5 - var7) * 100.0D / var5) + "%)");
      this.e.add("Alloc Mem Free: " + var2.format((long)var7) + "mb");
      this.e.add("Alloc Mem Total: " + this.a(var5 / 1024.0D, 1) + "gb");
      this.e.add("Total Mem Max: " + this.a(var9 / 1024.0D, 1) + "gb");
      this.e.add("Total Mem Free: " + this.a((var7 + (var3 - var5) + 500.0D) / 1024.0D, 1) + "gb");
      this.e.add("Threads Open: " + var11.size());
      this.repaint();
   }

   public void paint(Graphics var1) {
      var1.setColor(new Color(16777215));
      var1.fillRect(0, 0, 456, 246);

      int var2;
      for(var2 = 0; var2 < 256; ++var2) {
         int var3 = this.b[var2 + this.c & 255];
         var1.setColor(new Color(var3 + 28 << 16));
         var1.fillRect(var2, 100 - var3, 1, var3);
      }

      var1.setColor(Color.BLACK);

      String var4;
      for(var2 = 0; var2 < this.d.size(); ++var2) {
         var4 = (String)this.d.get(var2);
         if(var4 != null) {
            var1.drawString(var4, 10, 14 + var2 * 16);
         }
      }

      for(var2 = 0; var2 < this.e.size(); ++var2) {
         var4 = (String)this.e.get(var2);
         if(var4 != null) {
            var1.drawString(var4, 228, 14 + var2 * 16);
         }
      }

   }

   public double a(double var1, int var3) {
      if(var3 < 0) {
         throw new IllegalArgumentException();
      } else {
         long var4 = (long)Math.pow(10.0D, (double)var3);
         var1 *= (double)var4;
         long var6 = Math.round(var1);
         return (double)var6 / (double)var4;
      }
   }

   static void a(InformationComponent var0) {
      var0.a();
   }

}
