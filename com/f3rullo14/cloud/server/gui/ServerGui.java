package com.f3rullo14.cloud.server.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import com.f3rullo14.cloud.server.G_CloudManager;
import com.f3rullo14.cloud.server.gui.ConnectionListComponent;
import com.f3rullo14.cloud.server.gui.InformationComponent;
import com.f3rullo14.cloud.server.gui.ServerGuiINNER1;
import com.f3rullo14.cloud.server.gui.ServerGuiINNER2;
import com.f3rullo14.cloud.server.gui.ServerGuiINNER3;
import com.f3rullo14.cloud.server.gui.TextAreaLogHandler;

public class ServerGui extends JComponent {

   private static boolean a;
   private G_CloudManager b;


   public static void a(G_CloudManager var0) {
      try {
         UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      } catch (Exception var3) {
         ;
      }

      ServerGui var1 = new ServerGui(var0);
      a = true;
      JFrame var2 = new JFrame(var0.a);
      var2.add(var1);
      var2.pack();
      var2.setLocationRelativeTo((Component)null);
      var2.setVisible(true);
      var2.addWindowListener(new ServerGuiINNER1(var0));
   }

   public ServerGui(G_CloudManager var1) {
      this.b = var1;
      this.setPreferredSize(new Dimension(1000, 550));
      this.setLayout(new BorderLayout());

      try {
         this.add(this.c(), "Center");
         this.add(this.a(), "West");
      } catch (Exception var3) {
         var3.printStackTrace();
      }

   }

   private JComponent a() {
      JPanel var1 = new JPanel(new BorderLayout());
      var1.add(new InformationComponent(this.b), "North");
      var1.add(this.b(), "Center");
      var1.setBorder(new TitledBorder(new EtchedBorder(), "Information"));
      return var1;
   }

   private JComponent b() {
      ConnectionListComponent var1 = new ConnectionListComponent(this.b);
      JScrollPane var2 = new JScrollPane(var1, 22, 30);
      var2.setBorder(new TitledBorder(new EtchedBorder(), "Players"));
      return var2;
   }

   private JComponent c() {
      JPanel var1 = new JPanel(new BorderLayout());
      JTextArea var2 = new JTextArea();
      this.b.d().b().addHandler(new TextAreaLogHandler(var2));
      JScrollPane var3 = new JScrollPane(var2, 22, 30);
      var2.setEditable(false);
      JTextField var4 = new JTextField();
      var4.addActionListener(new ServerGuiINNER2(this, var4));
      var2.addFocusListener(new ServerGuiINNER3(this));
      var1.add(var3, "Center");
      var1.add(var4, "South");
      var1.setBorder(new TitledBorder(new EtchedBorder(), "Console"));
      return var1;
   }

   static G_CloudManager a(ServerGui var0) {
      return var0.b;
   }
}
