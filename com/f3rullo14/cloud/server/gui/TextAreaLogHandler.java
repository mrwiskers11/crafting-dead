package com.f3rullo14.cloud.server.gui;

import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import javax.swing.JTextArea;

import com.f3rullo14.cloud.server.gui.TextAreaLogHandlerINNER1;

public class TextAreaLogHandler extends Handler {

   private int[] b = new int[1024];
   private int c;
   Formatter a = new TextAreaLogHandlerINNER1(this);
   private JTextArea d;


   public TextAreaLogHandler(JTextArea var1) {
      this.setFormatter(this.a);
      this.d = var1;
   }

   public void close() {}

   public void flush() {}

   public void publish(LogRecord var1) {
      int var2 = this.d.getDocument().getLength();
      this.d.append(this.a.format(var1));
      this.d.setCaretPosition(this.d.getDocument().getLength());
      int var3 = this.d.getDocument().getLength() - var2;
      if(this.b[this.c] != 0) {
         this.d.replaceRange("", 0, this.b[this.c]);
      }

      this.b[this.c] = var3;
      this.c = (this.c + 1) % 1024;
   }
}
