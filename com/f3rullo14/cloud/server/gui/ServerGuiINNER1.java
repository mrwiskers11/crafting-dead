package com.f3rullo14.cloud.server.gui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.f3rullo14.cloud.server.G_CloudManager;

final class ServerGuiINNER1 extends WindowAdapter {

   final G_CloudManager a;


   ServerGuiINNER1(G_CloudManager var1) {
      this.a = var1;
   }

   public void windowClosing(WindowEvent var1) {
      this.a.b();

      while(!this.a.e) {
         try {
            Thread.sleep(100L);
         } catch (InterruptedException var3) {
            var3.printStackTrace();
         }
      }

      System.exit(0);
   }
}
