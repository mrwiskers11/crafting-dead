package com.f3rullo14.cloud.server.gui;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.f3rullo14.cloud.server.gui.LogFormatter;

public class LogAgent {

   private final Logger a;
   private final String b;
   private final String c;
   private final String d;


   public LogAgent(String var1, String var2, String var3) {
      this.a = Logger.getLogger(var1);
      this.c = var1;
      this.d = var2;
      this.b = var3;
   }

   public void a() {
      LogFormatter var1 = new LogFormatter(this);

      try {
         this.a.setUseParentHandlers(false);
         FileHandler var2 = new FileHandler(this.b, true);
         var2.setFormatter(var1);
         this.a.addHandler(var2);
      } catch (Exception var3) {
         this.a.log(Level.WARNING, "Failed to log " + this.c + " to " + this.b, var3);
      }

   }

   public void a(String var1) {
      this.a.log(Level.INFO, var1);
   }

   public Logger b() {
      return this.a;
   }

   public void b(String var1) {
      this.a.log(Level.WARNING, var1);
   }

   public void a(String var1, Object ... var2) {
      this.a.log(Level.WARNING, var1, var2);
   }

   public void a(String var1, Throwable var2) {
      this.a.log(Level.WARNING, var1, var2);
   }

   public void c(String var1) {
      this.a.log(Level.SEVERE, var1);
   }

   public void b(String var1, Throwable var2) {
      this.a.log(Level.SEVERE, var1, var2);
   }

   public void d(String var1) {
      this.a.log(Level.FINE, var1);
   }

   static String a(LogAgent var0) {
      return "[CDCloud]";
   }
}
