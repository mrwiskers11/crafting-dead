package com.f3rullo14.cloud.server.gui;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import com.f3rullo14.cloud.server.gui.TextAreaLogHandler;

class TextAreaLogHandlerINNER1 extends Formatter {

   final TextAreaLogHandler a;


   TextAreaLogHandlerINNER1(TextAreaLogHandler var1) {
      this.a = var1;
   }

   public String format(LogRecord var1) {
      StringBuilder var2 = new StringBuilder();
      var2.append(" [" + var1.getLevel().getName() + "] ");
      var2.append(this.formatMessage(var1));
      var2.append('\n');
      Throwable var3 = var1.getThrown();
      if(var3 != null) {
         StringWriter var4 = new StringWriter();
         var3.printStackTrace(new PrintWriter(var4));
         var2.append(var4.toString());
      }

      return var2.toString();
   }
}
