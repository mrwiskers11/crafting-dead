package com.f3rullo14.cloud.server.gui;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JList;

import com.f3rullo14.cloud.server.G_CloudManager;
import com.f3rullo14.cloud.server.G_CloudTcpConnectionHandler;

public class ConnectionListComponent extends JList {

   private G_CloudManager a;
   private int b;


   public ConnectionListComponent(G_CloudManager var1) {
      this.a = var1;
      var1.a(this);
   }

   public void a() {
      if(this.b++ % 20 == 0) {
         Vector var1 = new Vector();
         ArrayList var2 = this.a.c().a("player");

         for(int var3 = 0; var3 < var2.size(); ++var3) {
            try {
               var1.add(((G_CloudTcpConnectionHandler)var2.get(var3)).a());
            } catch (Exception var5) {
               ;
            }
         }

         this.setListData(var1);
      }

   }
}
