package com.f3rullo14.cloud.server.gui;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import com.f3rullo14.cloud.server.gui.LogAgent;

class LogFormatter extends Formatter {

   private SimpleDateFormat b;
   final LogAgent a;


   public LogFormatter(LogAgent var1) {
      this.a = var1;
      this.b = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   }

   public String format(LogRecord var1) {
      StringBuilder var2 = new StringBuilder();
      var2.append(this.b.format(Long.valueOf(var1.getMillis())));
      if(LogAgent.a(this.a) != null) {
         var2.append(LogAgent.a(this.a));
      }

      var2.append(" [").append(var1.getLevel().getName()).append("] ");
      var2.append(this.formatMessage(var1));
      Throwable var3 = var1.getThrown();
      if(var3 != null) {
         StringWriter var4 = new StringWriter();
         var3.printStackTrace(new PrintWriter(var4));
         var2.append(var4.toString());
      }

      System.out.println("" + var2.toString());
      return var2.toString() + "\n";
   }
}
