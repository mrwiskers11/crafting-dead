package com.f3rullo14.cloud.server.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

import com.f3rullo14.cloud.server.gui.ServerGui;

class ServerGuiINNER2 implements ActionListener {

   final JTextField a;
   final ServerGui b;


   ServerGuiINNER2(ServerGui var1, JTextField var2) {
      this.b = var1;
      this.a = var2;
   }

   public void actionPerformed(ActionEvent var1) {
      String var2 = this.a.getText().trim();
      if(var2.length() > 0) {
         ServerGui.a(this.b).a(var2);
      }

      this.a.setText("");
   }
}
