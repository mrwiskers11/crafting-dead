package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_ITcpConnection;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpMasterThread;
import com.f3rullo14.cloud.G_TcpMonitorThread;
import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.G_TcpReaderThread;
import com.f3rullo14.cloud.G_TcpWriterThread;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class G_TcpConnection implements G_ITcpConnection {

   public static AtomicInteger a = new AtomicInteger();
   public static AtomicInteger b = new AtomicInteger();
   public final String c;
   private final Object g;
   private Socket h;
   private final SocketAddress i;
   private volatile DataInputStream j;
   private volatile DataOutputStream k;
   private volatile boolean l;
   private volatile boolean m;
   private Queue n;
   private List o;
   private G_ITcpConnectionHandler p;
   private boolean q = false;
   private boolean r;
   private Thread s;
   private Thread t;
   private String u;
   private int v;
   private int w;
   public static int[] d = new int[256];
   public static int[] e = new int[256];
   public int f;


   public G_TcpConnection(Socket var1, String var2, G_ITcpConnectionHandler var3) throws IOException {
      this.c = var2;
      this.g = new Object();
      this.l = true;
      this.n = new ConcurrentLinkedQueue();
      this.o = Collections.synchronizedList(new ArrayList());
      this.u = "";
      this.h = var1;
      this.i = var1.getRemoteSocketAddress();
      this.p = var3;

      try {
         var1.setSoTimeout(30000);
         var1.setTrafficClass(25);
      } catch (SocketException var5) {
         System.err.println(var5.getMessage());
      }

      this.j = new DataInputStream(var1.getInputStream());
      this.k = new DataOutputStream(new BufferedOutputStream(var1.getOutputStream(), 5120));
      this.t = new G_TcpReaderThread(this, var2 + " read thread");
      this.s = new G_TcpWriterThread(this, var2 + " write thread");
      this.t.start();
      this.s.start();
      this.q = true;
   }

   public void a(G_ITcpConnectionHandler var1) {
      this.p = var1;
   }

   public void a(G_TcpPacket var1) {
      if(!this.r) {
         Object var2 = this.g;
         Object var3 = this.g;
         synchronized(this.g) {
            this.w += var1.b() + 1;
            this.o.add(var1);
         }
      }

   }

   private boolean g() {
      boolean var1 = false;

      try {
         if(this.f == 0) {
            G_TcpPacket var2 = this.h();
            if(var2 != null) {
               G_TcpPacket.a(var2, (DataOutput)this.k);
               int[] var4 = e;
               int var3 = var2.a();
               var4[var3] = var2.b() + 1;
               var1 = true;
            }
         }

         return var1;
      } catch (Exception var5) {
         if(!this.m) {
            this.a(var5);
         }

         return false;
      }
   }

   private G_TcpPacket h() {
      G_TcpPacket var1 = null;
      List var2 = this.o;
      Object var3 = this.g;
      Object var4 = this.g;
      synchronized(this.g) {
         while(!var2.isEmpty() && var1 == null) {
            var1 = (G_TcpPacket)var2.remove(0);
            this.w -= var1.b() + 1;
         }

         return var1;
      }
   }

   public void a() {
      if(this.t != null) {
         this.t.interrupt();
      }

      if(this.s != null) {
         this.s.interrupt();
      }

   }

   private boolean i() {
      boolean var1 = false;

      try {
         G_TcpPacket var2 = G_TcpPacket.a((DataInput)this.j, this.h);
         if(var2 != null) {
            int[] var3 = d;
            int var4 = var2.a();
            var3[var4] = var2.b() + 1;
            if(!this.r) {
               this.n.add(var2);
            }

            var1 = true;
         } else {
            this.a("disconnect.endOfStream");
         }

         return var1;
      } catch (Exception var5) {
         if(!this.m) {
            this.a(var5);
         }

         return false;
      }
   }

   public void b() {
      if(this.w > 2097152) {
         this.a("disconnect.overflow");
      }

      if(this.n.isEmpty()) {
         if(this.v++ >= 1200) {
            this.a("disconnect.timeout");
         }
      } else {
         this.v = 0;
      }

      int var1 = 1000;

      while(var1-- >= 0) {
         G_TcpPacket var2 = (G_TcpPacket)this.n.poll();
         if(var2 != null) {
            var2.b(this.p);
            this.p.b(var2);
         }
      }

      this.a();
      if(this.m && this.n.isEmpty()) {
         this.p.a(this.u);
      }

   }

   public SocketAddress c() {
      return this.i;
   }

   public void d() {
      if(!this.r) {
         this.a();
         this.r = true;
         this.t.interrupt();
         (new G_TcpMonitorThread(this)).start();
      }

   }

   public int e() {
      return 0;
   }

   private void a(Exception var1) {
      var1.printStackTrace();
      this.a("disconnect.genericReason");
   }

   public void a(String var1) {
      if(this.l) {
         this.q = false;
         this.m = true;
         this.u = var1;
         this.l = false;
         (new G_TcpMasterThread(this)).start();

         try {
            this.j.close();
         } catch (Throwable var5) {
            ;
         }

         try {
            this.k.close();
         } catch (Throwable var4) {
            ;
         }

         try {
            this.h.close();
         } catch (Throwable var3) {
            ;
         }

         this.j = null;
         this.k = null;
         this.h = null;
      }

   }

   public void f() {
      this.a();
      this.s = null;
      this.t = null;
   }

   static boolean a(G_TcpConnection var0) {
      return var0.l;
   }

   static boolean b(G_TcpConnection var0) {
      return var0.r;
   }

   static boolean c(G_TcpConnection var0) {
      return var0.i();
   }

   static boolean d(G_TcpConnection var0) {
      return var0.g();
   }

   static DataOutputStream e(G_TcpConnection var0) {
      return var0.k;
   }

   static boolean f(G_TcpConnection var0) {
      return var0.m;
   }

   static void a(G_TcpConnection var0, Exception var1) {
      var0.a(var1);
   }

   static Thread g(G_TcpConnection var0) {
      return var0.t;
   }

   static Thread h(G_TcpConnection var0) {
      return var0.s;
   }

   public static boolean i(G_TcpConnection var0) {
      return var0 == null?false:var0.q;
   }

}
