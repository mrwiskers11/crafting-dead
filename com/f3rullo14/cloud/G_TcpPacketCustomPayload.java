package com.f3rullo14.cloud;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacket;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class G_TcpPacketCustomPayload extends G_TcpPacket {

   public int c;
   public byte[] d;


   public void a(DataOutput var1) {
      try {
         ByteArrayOutputStream var2 = new ByteArrayOutputStream();
         DataOutputStream var3 = new DataOutputStream(var2);
         this.a(var3);
         this.d = var2.toByteArray();
         this.c = this.d.length;
         var3.close();
         var2.close();
         var1.writeShort(this.c);
         if(this.d != null) {
            var1.write(this.d);
         }
      } catch (IOException var4) {
         var4.printStackTrace();
      }

   }

   public void b(DataInput var1) {
      try {
         this.c = var1.readShort();
         if(this.c > 0) {
            this.d = new byte[this.c];
            var1.readFully(this.d);
         }

         if(this.d != null) {
            DataInputStream var2 = new DataInputStream(new ByteArrayInputStream(this.d));
            this.a(var2);
         }
      } catch (IOException var3) {
         var3.printStackTrace();
      }

   }

   public void b(G_ITcpConnectionHandler var1) {
      try {
         if(var1.a((G_TcpPacket)this)) {
            this.a(var1);
         }
      } catch (IOException var3) {
         var3.printStackTrace();
      }

   }

   public int b() {
      return this.d == null?0:this.d.length + 2;
   }

   public abstract void a(DataOutputStream var1) throws IOException;

   public abstract void a(DataInputStream var1) throws IOException;

   public abstract void a(G_ITcpConnectionHandler var1) throws IOException;
}
