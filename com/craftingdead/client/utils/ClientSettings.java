package com.craftingdead.client.utils;

import com.craftingdead.CraftingDead;
import com.f3rullo14.fds.tag.FDSHelper;
import com.f3rullo14.fds.tag.FDSTagCompound;

import java.io.File;

public class ClientSettings {

   public int a = 2;
   public boolean b = true;
   public boolean c = true;
   public boolean d = false;
   public boolean e = true;
   public boolean f = false;
   public boolean g = true;
   public boolean h = true;
   public boolean i = true;
   public boolean j = true;
   private File k;


   public ClientSettings(File var1) {
      this.k = new File(var1, "modsettings");
      this.a();
   }

   public void a() {
      new FDSTagCompound("modsettings");
      FDSTagCompound var1 = FDSHelper.a(this.k).a("modsettings");
      this.a = var1.b("particles")?var1.e("particles"):0;
      this.b = var1.b("noti")?var1.g("noti"):true;
      this.c = var1.b("bodytext")?var1.g("bodytext"):true;
      this.d = var1.b("displayloot")?var1.g("displayloot"):false;
      this.e = var1.b("music")?var1.g("music"):true;
      this.f = var1.b("hitbox")?var1.g("hitbox"):false;
      this.g = var1.b("piles")?var1.g("piles"):true;
      this.h = var1.b("blood")?var1.g("blood"):true;
      this.i = var1.b("karmaSkins")?var1.g("karmaSkins"):true;
      this.j = var1.b("customTextures")?var1.g("customTextures"):true;
      CraftingDead.d.info("Loaded settings");
   }

   public void b() {
      FDSTagCompound var1 = new FDSTagCompound("modsettings");
      var1.a("particles", this.a);
      var1.a("noti", this.b);
      var1.a("bodytext", this.c);
      var1.a("displayloot", this.d);
      var1.a("music", this.e);
      var1.a("hitbox", this.f);
      var1.a("piles", this.g);
      var1.a("blood", this.h);
      var1.a("karmaSkins", this.i);
      var1.a("customTextures", this.j);
      FDSHelper.a(this.k, var1);
      CraftingDead.d.info("Saved settings");
   }
}
