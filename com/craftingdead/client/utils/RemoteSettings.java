package com.craftingdead.client.utils;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.gui.gui.server.ServerRegion;
import com.craftingdead.utils.FileHelper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.awt.Component;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import javax.swing.JOptionPane;

public class RemoteSettings {

   private static final JsonParser a = new JsonParser();
   private File b;
   private String c;
   private boolean d;
   private String e;
   private String f;
   private URL g;
   private URL h;
   private Map i = new HashMap();
   private URI j;
   private URI k;
   private URI l;
   private URI m;
   private Map n = new HashMap();


   public RemoteSettings(File var1) throws Throwable {
      this.b = new File(var1, "craftingdead.json");
      if(!this.b.exists()) {
         JOptionPane.showMessageDialog((Component)null, "Cannot continue loading without " + this.b.getAbsolutePath(), "Error", 0);
         System.exit(0);
      }

      this.a();

      try {
         this.b();
      } catch (URISyntaxException var3) {
         CraftingDead.d.log(Level.WARNING, "An error occurred while loading settings", var3);
      }

   }

   public boolean a() throws Throwable {
      try {
         JsonObject var1 = (JsonObject)a.parse(new FileReader(this.b));
         URL var2 = new URL(var1.get("remote").getAsString());
         String var3 = null;
         InputStream var4 = FileHelper.a(var2);
         Throwable var5 = null;

         try {
            var3 = FileHelper.a(var4);
         } catch (Throwable var15) {
            var5 = var15;
            throw var15;
         } finally {
            if(var4 != null) {
               if(var5 != null) {
                  try {
                     var4.close();
                  } catch (Throwable var14) {
                     var5.addSuppressed(var14);
                  }
               } else {
                  var4.close();
               }
            }

         }

         return FileHelper.a(this.b, var2, var3);
      } catch (IOException var17) {
         CraftingDead.d.log(Level.WARNING, "Could not update settings", var17);
         return false;
      }
   }

   public void b() throws IOException, URISyntaxException {
      JsonObject var1 = (JsonObject)a.parse(new FileReader(this.b));
      this.c = var1.get("network-address").getAsString();
      this.d = var1.get("display-presents").getAsBoolean();
      JsonObject var2 = var1.get("resource-pack").getAsJsonObject();
      this.g = new URL(var2.get("url").getAsString());
      this.e = var2.get("hash").getAsString();
      JsonObject var3 = var1.get("team").getAsJsonObject();
      this.h = new URL(var3.get("url").getAsString());
      this.f = var3.get("hash").getAsString();
      Iterator var4 = var1.get("servers").getAsJsonObject().entrySet().iterator();

      Entry var5;
      while(var4.hasNext()) {
         var5 = (Entry)var4.next();

         try {
            ServerRegion var6 = ServerRegion.valueOf((String)var5.getKey());
            ArrayList var7 = new ArrayList();
            Iterator var8 = ((JsonElement)var5.getValue()).getAsJsonArray().iterator();

            while(var8.hasNext()) {
               Object var9 = var8.next();

               try {
                  var7.add(((JsonElement)var9).getAsString());
               } catch (Exception var12) {
                  CraftingDead.d.log(Level.WARNING, "An error occurred while iterating over servers in " + var6.name(), var12);
               }
            }

            this.i.put(var6, var7);
         } catch (Exception var13) {
            CraftingDead.d.log(Level.WARNING, "An error occurred while iterating over server regions", var13);
         }
      }

      this.j = new URI(var1.get("website").getAsString());
      this.k = new URI(var1.get("store").getAsString());
      this.l = new URI(var1.get("maps").getAsString());
      this.m = new URI(var1.get("discord").getAsString());
      var4 = var1.get("capes").getAsJsonObject().entrySet().iterator();

      while(var4.hasNext()) {
         var5 = (Entry)var4.next();

         try {
            String var14 = (String)var5.getKey();
            String var15 = ((JsonElement)var5.getValue()).getAsString();
            this.n.put(var14, var15);
         } catch (Exception var11) {
            CraftingDead.d.log(Level.WARNING, "An error occurred while iterating over capes", var11);
         }
      }

   }

   public String c() {
      return this.c;
   }

   public boolean d() {
      return this.d;
   }

   public URL e() {
      return this.g;
   }

   public String f() {
      return this.e;
   }

   public URL g() {
      return this.h;
   }

   public String h() {
      return this.f;
   }

   public Map i() {
      return new HashMap(this.i);
   }

   public URI j() {
      return this.j;
   }

   public URI k() {
      return this.k;
   }

   public URI l() {
      return this.l;
   }

   public URI m() {
      return this.m;
   }

   public Map n() {
      return new HashMap(this.n);
   }

}
