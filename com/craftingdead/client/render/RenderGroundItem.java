package com.craftingdead.client.render;

import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.item.gun.ItemGun;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.Icon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import org.lwjgl.opengl.GL11;

public class RenderGroundItem extends Render {

   private Minecraft a = FMLClientHandler.instance().getClient();


   public void a(EntityGroundItem var1, double var2, double var4, double var6, float var8, float var9) {
      ItemStack var10 = var1.b();
      if(var10 != null) {
         IItemRenderer var11;
         if(var10.getItem() instanceof ItemGun) {
            GL11.glPushMatrix();
            var11 = MinecraftForgeClient.getItemRenderer(var10, ItemRenderType.ENTITY);
            ((RenderGun)var11).a(var1, var10, var2, var4, var6);
            GL11.glPopMatrix();
         } else {
            var11 = MinecraftForgeClient.getItemRenderer(var10, ItemRenderType.ENTITY);
            if(var11 != null) {
               GL11.glPushMatrix();
               GL11.glTranslatef((float)var2, (float)(var4 + 0.05000000074505806D), (float)var6);
               var11.renderItem(ItemRenderType.ENTITY, var10, new Object[]{Integer.valueOf(0), var1});
               GL11.glPopMatrix();
               return;
            }

            GL11.glPushMatrix();
            GL11.glTranslatef((float)var2, (float)(var4 + 0.05000000074505806D), (float)var6);
            float var12 = 0.75F;
            float var13 = 0.2F;
            GL11.glScalef(var12 - var13, var12 + 0.5F, var12 - var13);
            GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(var1.a, 0.0F, 0.0F, 1.0F);
            this.a(var1, var10.getIconIndex(), 1, 1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glPopMatrix();
         }

         String var14 = "" + var10.getDisplayName();
         if(var10.stackSize > 1) {
            var14 = var14 + " (" + var10.stackSize + ")";
         }

         if(this.a.objectMouseOver != null && this.a.objectMouseOver.typeOfHit == EnumMovingObjectType.ENTITY) {
            Entity var15 = this.a.objectMouseOver.entityHit;
            if(var15 == var1) {
               CDRenderHelper.a(var14, var2, var4 + 0.25D, var6, super.renderManager.playerViewY, super.renderManager.playerViewX);
            }
         }
      }

   }

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.a((EntityGroundItem)var1, var2, var4, var6, var8, var9);
   }

   protected ResourceLocation getEntityTexture(Entity var1) {
      return new ResourceLocation("craftingdead:textures/models/grenadeFlash.png");
   }

   private void a(EntityGroundItem var1, Icon var2, int var3, float var4, float var5, float var6, float var7) {
      Tessellator var8 = Tessellator.instance;
      if(var2 == null) {
         TextureManager var9 = Minecraft.getMinecraft().getTextureManager();
         ResourceLocation var10 = var9.getResourceLocation(var1.b().getItemSpriteNumber());
         var2 = ((TextureMap)var9.getTexture(var10)).getAtlasSprite("missingno");
      }

      float var24 = ((Icon)var2).getMinU();
      float var25 = ((Icon)var2).getMaxU();
      float var11 = ((Icon)var2).getMinV();
      float var12 = ((Icon)var2).getMaxV();
      float var13 = 0.5F;
      float var14 = 0.25F;
      GL11.glPushMatrix();
      float var16 = 0.0625F;
      float var15 = 0.021875F;
      ItemStack var17 = var1.b();
      byte var18 = 1;
      GL11.glTranslatef(-var13, -var14, -((var16 + var15) * (float)var18 / 2.0F));

      for(int var19 = 0; var19 < var18; ++var19) {
         GL11.glTranslatef(0.0F, 0.0F, var16 + var15);
         if(var17.getItemSpriteNumber() == 0) {
            super.renderManager.renderEngine.bindTexture(TextureMap.locationBlocksTexture);
         } else {
            super.renderManager.renderEngine.bindTexture(TextureMap.locationItemsTexture);
         }

         GL11.glColor4f(var5, var6, var7, 1.0F);
         ItemRenderer.renderItemIn2D(var8, var25, var11, var24, var12, ((Icon)var2).getIconWidth(), ((Icon)var2).getIconHeight(), var16);
         if(var17 != null && var17.getItem().hasEffect(var17, 0)) {
            GL11.glDepthFunc(514);
            GL11.glDisable(2896);
            ResourceLocation var20 = new ResourceLocation("textures/misc/enchanted_item_glint.png");
            super.renderManager.renderEngine.bindTexture(var20);
            GL11.glEnable(3042);
            GL11.glBlendFunc(768, 1);
            float var21 = 0.76F;
            GL11.glColor4f(0.5F * var21, 0.25F * var21, 0.8F * var21, 1.0F);
            GL11.glMatrixMode(5890);
            GL11.glPushMatrix();
            float var22 = 0.125F;
            GL11.glScalef(var22, var22, var22);
            float var23 = (float)(Minecraft.getSystemTime() % 3000L) / 3000.0F * 8.0F;
            GL11.glTranslatef(var23, 0.0F, 0.0F);
            GL11.glRotatef(-50.0F, 0.0F, 0.0F, 1.0F);
            ItemRenderer.renderItemIn2D(var8, 0.0F, 0.0F, 1.0F, 1.0F, 255, 255, var16);
            GL11.glPopMatrix();
            GL11.glPushMatrix();
            GL11.glScalef(var22, var22, var22);
            var23 = (float)(Minecraft.getSystemTime() % 4873L) / 4873.0F * 8.0F;
            GL11.glTranslatef(-var23, 0.0F, 0.0F);
            GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
            ItemRenderer.renderItemIn2D(var8, 0.0F, 0.0F, 1.0F, 1.0F, 255, 255, var16);
            GL11.glPopMatrix();
            GL11.glMatrixMode(5888);
            GL11.glDisable(3042);
            GL11.glEnable(2896);
            GL11.glDepthFunc(515);
         }
      }

      GL11.glPopMatrix();
   }
}
