package com.craftingdead.client.render;

import com.craftingdead.client.model.ModelFlamethrower;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderFlamethrower extends RenderGun {

   protected void a(Entity var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.05F);
      GL11.glRotatef(365.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(30.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-1.0F, 0.15F, -0.03F);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      double var3 = 0.95D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.8F, -0.2F, 0.3F);
      double var3 = 0.85D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {}

   protected void b(Entity var1, ItemStack var2) {}

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(630.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.9D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {}

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {}

   protected ModelBase a() {
      return new ModelFlamethrower();
   }

   protected String b() {
      return "flamethrower";
   }
}
