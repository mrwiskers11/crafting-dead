package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatArmyHelmet;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatArmyhelmet extends RenderHat {

   public RenderHatArmyhelmet(String var1) {
      super.a = new ModelHatArmyHelmet();
      super.b = "hatModel_armyhelmet";
      super.b = var1;
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(-0.25D, -3.0D, -0.25D);
      this.a();
      this.a(var1);
   }
}
