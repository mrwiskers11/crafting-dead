package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatBunny;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatBunny extends RenderHat {

   public RenderHatBunny() {
      super.a = new ModelHatBunny();
      super.b = "bunny";
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(0.0D, -3.0D, -1.0D);
      this.a();
      this.a(var1);
   }
}
