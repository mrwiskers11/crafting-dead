package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatScubaMask;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatScubaMask extends RenderHat {

   public RenderHatScubaMask() {
      super.a = new ModelHatScubaMask();
      super.b = "scubamask";
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotated(-90.0D, 0.0D, 1.0D, 0.0D);
      GL11.glTranslated(-2.8D, -4.4D, -2.6D);
      GL11.glBlendFunc(770, 771);
      this.a();
      this.a(var1);
   }
}
