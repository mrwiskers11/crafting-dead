package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatKnight;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatKnight extends RenderHat {

   public RenderHatKnight() {
      super.a = new ModelHatKnight();
      super.b = "knight";
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslated(0.0D, -2.5D, 0.0D);
      this.a();
      this.a(var1);
   }
}
