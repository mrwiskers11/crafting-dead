package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatNVGoggles;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatNVGoggles extends RenderHat {

   public RenderHatNVGoggles() {
      super.a = new ModelHatNVGoggles();
      super.b = "nvgoggles";
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(0.0D, -5.0D, 0.0D);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      this.a();
      this.a(var1);
   }
}
