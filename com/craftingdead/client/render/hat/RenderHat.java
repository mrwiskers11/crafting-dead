package com.craftingdead.client.render.hat;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public abstract class RenderHat {

   public ModelBase a;
   public String b;


   public abstract void a(EntityPlayer var1, ItemStack var2);

   public void a() {
      Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/hats/hat_" + this.b + ".png"));
   }

   public void a(EntityPlayer var1) {
      this.a.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.625F);
   }
}
