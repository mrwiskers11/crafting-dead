package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatBallisticHelmet;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatBallisticHelmet extends RenderHat {

   public RenderHatBallisticHelmet(String var1) {
      super.a = new ModelHatBallisticHelmet();
      super.b = "ballistic_" + var1;
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.06D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(0.0D, -11.0D, 1.0D);
      this.a();
      this.a(var1);
   }
}
