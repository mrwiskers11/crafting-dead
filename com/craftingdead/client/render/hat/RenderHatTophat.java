package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatTophat;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatTophat extends RenderHat {

   public RenderHatTophat() {
      super.a = new ModelHatTophat();
      super.b = "tophat";
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(0.0D, -7.0D, 0.0D);
      this.a();
      this.a(var1);
   }
}
