package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatGasmask;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatScuba extends RenderHat {

   public RenderHatScuba() {
      super.a = new ModelHatGasmask();
      super.b = "scubamask";
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(-2.8D, -5.1D, -3.0D);
      this.a();
      this.a(var1);
   }
}
