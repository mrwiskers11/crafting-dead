package com.craftingdead.client.render.hat;

import com.craftingdead.client.model.hat.ModelHatPayday;
import com.craftingdead.client.render.hat.RenderHat;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderHatPayday extends RenderHat {

   public RenderHatPayday(String var1) {
      super.a = new ModelHatPayday();
      super.b = var1;
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(0.0D, -2.0D, -3.0D);
      this.a();
      this.a(var1);
   }
}
