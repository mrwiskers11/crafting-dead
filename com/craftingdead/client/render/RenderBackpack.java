package com.craftingdead.client.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public abstract class RenderBackpack {

   protected ModelBase a;
   protected String b;


   public RenderBackpack(String var1, ModelBase var2) {
      this.b = var1;
      this.a = var2;
   }

   public abstract void a(EntityPlayer var1, ItemStack var2);

   public String a() {
      return this.b;
   }
}
