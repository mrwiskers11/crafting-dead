package com.craftingdead.client.render;

import com.craftingdead.block.BlockLoot;
import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBarrier1;
import com.craftingdead.block.tileentity.TileEntityBarrier2;
import com.craftingdead.block.tileentity.TileEntityBarrier3;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.block.tileentity.TileEntityCampfire;
import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.block.tileentity.TileEntityLoot;
import com.craftingdead.block.tileentity.TileEntityMilitarySign;
import com.craftingdead.block.tileentity.TileEntityRoadBlock;
import com.craftingdead.block.tileentity.TileEntitySandBarrier;
import com.craftingdead.block.tileentity.TileEntityShelfEmpty;
import com.craftingdead.block.tileentity.TileEntityShelfLoot;
import com.craftingdead.block.tileentity.TileEntityStopSign;
import com.craftingdead.block.tileentity.TileEntityTrafficCone;
import com.craftingdead.block.tileentity.TileEntityTrafficPole;
import com.craftingdead.block.tileentity.TileEntityTrashCan;
import com.craftingdead.block.tileentity.TileEntityVendingMachine;
import com.craftingdead.block.tileentity.TileEntityWaterPump;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.world.IBlockAccess;

public class RenderBlockHandler implements ISimpleBlockRenderingHandler {

   public void renderInventoryBlock(Block var1, int var2, int var3, RenderBlocks var4) {
      if(var3 == BlockManager.a) {
         TileEntityLoot var5 = new TileEntityLoot(((BlockLoot)var1).e());
         var5.a = true;
         TileEntityRenderer.instance.renderTileEntityAt(var5, 0.0D, 0.0D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.b) {
         TileEntityBaseCenter var8 = new TileEntityBaseCenter();
         TileEntityRenderer.instance.renderTileEntityAt(var8, 0.0D, 0.0D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.c) {
         TileEntityCampfire var9 = new TileEntityCampfire();
         TileEntityRenderer.instance.renderTileEntityAt(var9, 0.0D, 0.0D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.d) {
         TileEntitySandBarrier var10 = new TileEntitySandBarrier();
         TileEntityRenderer.instance.renderTileEntityAt(var10, 0.0D, -0.05D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.e) {
         TileEntityWaterPump var11 = new TileEntityWaterPump();
         TileEntityRenderer.instance.renderTileEntityAt(var11, 0.0D, -0.2D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.h) {
         TileEntityBarrier1 var12 = (new TileEntityBarrier1()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var12, 0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.i) {
         TileEntityBarrier2 var13 = (new TileEntityBarrier2()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var13, 0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.j) {
         TileEntityBarrier3 var14 = (new TileEntityBarrier3()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var14, 0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.k) {
         TileEntityTrafficPole var15 = (new TileEntityTrafficPole()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var15, 0.0D, -0.3D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.l) {
         TileEntityTrafficCone var16 = (new TileEntityTrafficCone()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var16, 0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.m) {
         TileEntityStopSign var17 = (new TileEntityStopSign()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var17, 0.0D, -0.6D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.o) {
         TileEntityTrashCan var18 = (new TileEntityTrashCan()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var18, -0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.p) {
         TileEntityVendingMachine var19 = (new TileEntityVendingMachine()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var19, -0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.n) {
         TileEntityRoadBlock var20 = (new TileEntityRoadBlock()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var20, -0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.q) {
         TileEntityShelfEmpty var21 = (new TileEntityShelfEmpty()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var21, -0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.r) {
         TileEntityShelfLoot var22 = (new TileEntityShelfLoot()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var22, -0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.s) {
         TileEntityForge var23 = (new TileEntityForge()).a();
         TileEntityRenderer.instance.renderTileEntityAt(var23, -0.0D, -0.1D, 0.0D, 0.0F);
      }

      if(var3 == BlockManager.g) {
         TileEntityMilitarySign var24 = (new TileEntityMilitarySign()).a();
         if(var24 != null) {
            try {
               TileEntityRenderer.instance.renderTileEntityAt(var24, 0.0D, 0.0D, 0.0D, 0.0F);
            } catch (Exception var7) {
               var7.printStackTrace();
            }
         }
      }

   }

   public boolean renderWorldBlock(IBlockAccess var1, int var2, int var3, int var4, Block var5, int var6, RenderBlocks var7) {
      return true;
   }

   public boolean shouldRender3DInInventory() {
      return true;
   }

   public int getRenderId() {
      return 0;
   }
}
