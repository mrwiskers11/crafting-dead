package com.craftingdead.client.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderTacticalVest {

   protected ModelBase a;
   protected String b;


   public RenderTacticalVest(String var1, ModelBase var2) {
      this.b = var1;
      this.a = var2;
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 1.05D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(-0.0D, -0.01D, -0.12D);
      GL11.glRotated(180.0D, 1.0D, 0.0D, 0.0D);
      GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      GL11.glEnable(2884);
      this.a.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glDisable(2884);
   }

   public String a() {
      return this.b;
   }
}
