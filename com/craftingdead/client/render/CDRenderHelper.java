package com.craftingdead.client.render;

import com.craftingdead.client.render.IRenderOnGUI;
import com.craftingdead.mojang.MojangAPI;
import java.awt.Color;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureObject;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import org.lwjgl.opengl.GL11;

public class CDRenderHelper {

   private static final ResourceLocation d = new ResourceLocation("craftingdead", "textures/gui/avatar.png");
   public static int a = 16777215;
   public static int b = 16777215;
   private static ResourceLocation e = new ResourceLocation("craftingdead", "textures/gui/icon.png");
   public static ResourceLocation c = new ResourceLocation("craftingdead", "textures/gui/emerald.png");
   private static RenderPlayer f;
   private static float g = 0.0F;


   public static void a(String var0) {
      a = (int)Long.parseLong(var0.replaceFirst("0x", ""), 16);
   }

   public static int a() {
      return a;
   }

   public static void a(int var0) {
      Color var1 = Color.decode("" + var0);
      float var2 = (float)var1.getRed() / 255.0F;
      float var3 = (float)var1.getGreen() / 255.0F;
      float var4 = (float)var1.getBlue() / 255.0F;
      GL11.glColor3f(var2, var3, var4);
   }

   public static void b(String var0) {
      Color var1 = Color.decode("" + var0);
      float var2 = (float)var1.getRed() / 255.0F;
      float var3 = (float)var1.getGreen() / 255.0F;
      float var4 = (float)var1.getBlue() / 255.0F;
      GL11.glColor3f(var2, var3, var4);
   }

   public static void b() {
      Color var0 = Color.decode("" + a);
      float var1 = (float)var0.getRed() / 255.0F;
      float var2 = (float)var0.getGreen() / 255.0F;
      float var3 = (float)var0.getBlue() / 255.0F;
      GL11.glColor3f(var1, var2, var3);
   }

   public static void c() {
      Color var0 = Color.decode("" + b);
      float var1 = (float)var0.getRed() / 255.0F;
      float var2 = (float)var0.getGreen() / 255.0F;
      float var3 = (float)var0.getBlue() / 255.0F;
      GL11.glColor4f(var1, var2, var3, 0.9F);
   }

   public static void a(double var0, double var2, int var4, int var5, int var6, double var7) {
      int var9 = var4 * var6;
      int var10 = var5 * var6;
      GL11.glPushMatrix();
      GL11.glEnable(3042);
      float var11 = 1.0F;
      float var12 = 1.0F;
      float var13 = 1.0F;
      GL11.glColor4f(var11, var12, var13, 0.9F);
      GL11.glScaled(var7, var7, var7);
      GL11.glTranslated(var0 * (1.0D / var7), var2 * (1.0D / var7), 0.0D);
      Minecraft.getMinecraft().getTextureManager().bindTexture(e);
      a(var6 / 2, var6 / 2, var9, var10, var6, var6);
      GL11.glDisable(3042);
      GL11.glPopMatrix();
   }

   private static void a(int var0, int var1, int var2, int var3, int var4, int var5) {
      float var6 = 0.00390625F;
      float var7 = 0.00390625F;
      Tessellator var8 = Tessellator.instance;
      var8.startDrawingQuads();
      var8.addVertexWithUV((double)(var0 + 0), (double)(var1 + var5), (double)g, (double)((float)(var2 + 0) * var6), (double)((float)(var3 + var5) * var7));
      var8.addVertexWithUV((double)(var0 + var4), (double)(var1 + var5), (double)g, (double)((float)(var2 + var4) * var6), (double)((float)(var3 + var5) * var7));
      var8.addVertexWithUV((double)(var0 + var4), (double)(var1 + 0), (double)g, (double)((float)(var2 + var4) * var6), (double)((float)(var3 + 0) * var7));
      var8.addVertexWithUV((double)(var0 + 0), (double)(var1 + 0), (double)g, (double)((float)(var2 + 0) * var6), (double)((float)(var3 + 0) * var7));
      var8.draw();
   }

   public static void a(ItemStack var0, int var1, int var2) {
      GL11.glPushMatrix();
      RenderItem var3 = new RenderItem();
      Minecraft var4 = Minecraft.getMinecraft();
      var3.renderItemAndEffectIntoGUI(var4.fontRenderer, var4.getTextureManager(), var0, var1, var2);
      GL11.glDisable(2896);
      GL11.glPopMatrix();
   }

   public static void a(ItemStack var0, double var1, double var3) {
      Tessellator var5 = Tessellator.instance;
      Object var6 = var0.getIconIndex();
      if(var6 == null) {
         TextureManager var7 = Minecraft.getMinecraft().getTextureManager();
         ResourceLocation var8 = var7.getResourceLocation(var0.getItemSpriteNumber());
         var6 = ((TextureMap)var7.getTexture(var8)).getAtlasSprite("missingno");
      }

      float var17 = ((Icon)var6).getMinU();
      float var18 = ((Icon)var6).getMaxU();
      float var9 = ((Icon)var6).getMinV();
      float var10 = ((Icon)var6).getMaxV();
      float var11 = 0.5F;
      float var12 = 0.25F;
      float var13 = 0.021875F;
      GL11.glPushMatrix();
      float var14 = 0.0625F;
      byte var15 = 1;
      GL11.glTranslated(var1, var3, 0.0D);
      GL11.glTranslatef(-var11, -var12, -((var14 + var13) * (float)var15 / 2.0F));

      for(int var16 = 0; var16 < var15; ++var16) {
         GL11.glTranslatef(0.0F, 0.0F, var14 + var13);
         if(var0.getItemSpriteNumber() == 0) {
            Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
         } else {
            Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.locationItemsTexture);
         }

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         ItemRenderer.renderItemIn2D(var5, var18, var9, var17, var10, ((Icon)var6).getIconWidth(), ((Icon)var6).getIconHeight(), var14);
      }

      GL11.glPopMatrix();
   }

   public static void b(ItemStack var0, double var1, double var3) {
      IItemRenderer var5 = MinecraftForgeClient.getItemRenderer(var0, ItemRenderType.EQUIPPED);
      if(var5 != null && var5 instanceof IRenderOnGUI) {
         ((IRenderOnGUI)var5).b(var0, var1, var3);
      } else {
         GL11.glPushMatrix();
         GL11.glTranslated(var1 - 12.0D, var3 - 19.0D, 0.0D);
         double var6 = 1.5D;
         GL11.glScaled(var6, var6, var6);
         a(var0, 0, 0);
         GL11.glPopMatrix();
      }

   }

   public static void a(ItemStack var0, double var1, double var3, double var5) {
      IItemRenderer var7 = MinecraftForgeClient.getItemRenderer(var0, ItemRenderType.EQUIPPED);
      if(var7 != null && var7 instanceof IRenderOnGUI) {
         GL11.glPushMatrix();
         GL11.glTranslated(var1, var3, 0.0D);
         GL11.glScaled(var5, var5, var5);
         ((IRenderOnGUI)var7).b(var0, 0.0D, 0.0D);
         GL11.glPopMatrix();
      } else {
         GL11.glPushMatrix();
         GL11.glTranslated(var1 - 12.0D, var3 - 18.0D, 0.0D);
         double var8 = 1.5D;
         GL11.glScaled(var8, var8, var8);
         a(var0, 0, 0);
         GL11.glPopMatrix();
      }

   }

   public static void b(ItemStack var0, double var1, double var3, double var5) {
      IItemRenderer var7 = MinecraftForgeClient.getItemRenderer(var0, ItemRenderType.EQUIPPED);
      if(var7 != null && var7 instanceof IRenderOnGUI) {
         ((IRenderOnGUI)var7).a(var0, var1, var3, var5);
      } else {
         GL11.glPushMatrix();
         GL11.glTranslated(var1 - 60.0D, var3 - 45.0D, 0.0D);
         double var8 = 7.0D;
         GL11.glScaled(var8, var8, var8);
         a(var0, 0, 0);
         GL11.glPopMatrix();
      }

   }

   public static void c(ItemStack var0, double var1, double var3) {
      IItemRenderer var5 = MinecraftForgeClient.getItemRenderer(var0, ItemRenderType.EQUIPPED);
      if(var5 != null && var5 instanceof IRenderOnGUI) {
         ((IRenderOnGUI)var5).a(var0, var1, var3);
      } else {
         GL11.glPushMatrix();
         GL11.glTranslated(var1 - 95.0D, var3 - 55.0D, 0.0D);
         double var6 = 7.0D;
         GL11.glScaled(var6, var6, var6);
         a(var0, 0, 0);
         GL11.glPopMatrix();
      }

   }

   public static void c(ItemStack var0, double var1, double var3, double var5) {
      GL11.glPushMatrix();
      GL11.glTranslated(var1, var3, 0.0D);
      GL11.glScaled(var5, var5, var5);
      a(var0, 0, 0);
      GL11.glPopMatrix();
   }

   public static void a(double var0, double var2, double var4, double var6, int var8, float var9) {
      b(var0 - 1.0D, var2 - 1.0D, var4 + 2.0D, var6 + 2.0D, 0, 0.2F);
      b(var0, var2, var4, var6, var8, var9);
   }

   public static void b(double var0, double var2, double var4, double var6, int var8, float var9) {
      float var10 = (float)(var8 >> 16 & 255) / 255.0F;
      float var11 = (float)(var8 >> 8 & 255) / 255.0F;
      float var12 = (float)(var8 & 255) / 255.0F;
      a(var0, var2, var0 + var4, var2 + var6, var10, var11, var12, var9);
   }

   public static void a(double var0, double var2, double var4, double var6, float var8, float var9, float var10, float var11) {
      Tessellator var12 = new Tessellator();
      GL11.glPushMatrix();
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      GL11.glDisable(3553);
      GL11.glColor4f(var8, var9, var10, var11);
      var12.startDrawingQuads();
      var12.addVertex(var0, var6, 0.0D);
      var12.addVertex(var4, var6, 0.0D);
      var12.addVertex(var4, var2, 0.0D);
      var12.addVertex(var0, var2, 0.0D);
      var12.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
      GL11.glColor3f(1.0F, 1.0F, 1.0F);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glPopMatrix();
   }

   public static void a(String var0, int var1, int var2, double var3) {
      GL11.glPushMatrix();
      GL11.glTranslated((double)var1, (double)var2, 0.0D);
      GL11.glScaled(var3, var3, var3);
      a(var0, 0, 0);
      GL11.glPopMatrix();
   }

   public static void a(String var0, int var1, int var2) {
      a(var0, var1, var2, true);
   }

   public static void a(String var0, int var1, int var2, boolean var3) {
      Minecraft var4 = Minecraft.getMinecraft();
      var4.fontRenderer.drawString(var0, var1, var2, a, var3);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public static void b(String var0, int var1, int var2) {
      b(var0, var1, var2, true);
   }

   public static void b(String var0, int var1, int var2, boolean var3) {
      Minecraft var4 = Minecraft.getMinecraft();
      var4.fontRenderer.drawString(var0, var1 - var4.fontRenderer.getStringWidth(var0) / 2, var2, a, var3);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public static void c(String var0, int var1, int var2) {
      a(var0, var1, var2, true, a);
   }

   public static void a(String var0, int var1, int var2, boolean var3, int var4) {
      Minecraft var5 = Minecraft.getMinecraft();
      var5.fontRenderer.drawString(var0, var1 - var5.fontRenderer.getStringWidth(var0), var2, var4, var3);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public static void a(String var0, int var1, int var2, double var3, int var5) {
      GL11.glPushMatrix();
      GL11.glTranslated((double)var1, (double)var2, 0.0D);
      GL11.glScaled(var3, var3, var3);
      a(var0, 0, 0, var5);
      GL11.glPopMatrix();
   }

   public static void a(String var0, int var1, int var2, int var3) {
      Minecraft var4 = Minecraft.getMinecraft();
      var4.fontRenderer.drawString(var0, var1, var2, var3, true);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public static void b(String var0, int var1, int var2, int var3) {
      Minecraft var4 = Minecraft.getMinecraft();
      var4.fontRenderer.drawString(var0, var1 - var4.fontRenderer.getStringWidth(var0) / 2, var2, var3, true);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public static void b(String var0, int var1, int var2, double var3) {
      Minecraft var5 = Minecraft.getMinecraft();
      double var6 = (double)(var5.fontRenderer.getStringWidth(var0) / 2) * var3;
      GL11.glPushMatrix();
      GL11.glTranslated((double)var1 - var6, (double)var2, 0.0D);
      GL11.glScaled(var3, var3, var3);
      a(var0, 0, 0);
      GL11.glPopMatrix();
   }

   public static void a(double var0, double var2, ResourceLocation var4, double var5, double var7, float var9) {
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      GL11.glDisable(3008);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, var9);
      Minecraft.getMinecraft().renderEngine.bindTexture(var4);
      Tessellator var10 = new Tessellator();
      var10.startDrawingQuads();
      var10.addVertexWithUV(var0, var2 + var7, 0.0D, 0.0D, 1.0D);
      var10.addVertexWithUV(var0 + var5, var2 + var7, 0.0D, 1.0D, 1.0D);
      var10.addVertexWithUV(var0 + var5, var2, 0.0D, 1.0D, 0.0D);
      var10.addVertexWithUV(var0, var2, 0.0D, 0.0D, 0.0D);
      var10.draw();
      GL11.glEnable(3008);
      GL11.glDisable(3042);
   }

   public static ResourceLocation a(UUID var0) {
      ResourceLocation var1 = new ResourceLocation("craftingdead", "textures/avatars/" + var0 + ".png");
      TextureManager var2 = Minecraft.getMinecraft().getTextureManager();
      TextureObject var3 = var2.getTexture(var1);
      if(var3 == null) {
         ThreadDownloadImageData var4 = new ThreadDownloadImageData(MojangAPI.PlayerResource.AVATAR_URL.getUrl(var0), d, (IImageBuffer)null);
         var2.loadTexture(var1, (TextureObject)var4);
      }

      return var1;
   }

   public static void a(String var0, double var1, double var3, double var5, float var7, double var8) {
      Minecraft var10 = Minecraft.getMinecraft();
      EntityClientPlayerMP var11 = var10.thePlayer;
      GL11.glPushMatrix();
      FontRenderer var12 = var10.fontRenderer;
      float var13 = 0.016F;
      double var14 = var11.lastTickPosX + (var11.posX - var11.lastTickPosX) * (double)var7;
      double var16 = var11.lastTickPosY + (var11.posY - var11.lastTickPosY) * (double)var7;
      double var18 = var11.lastTickPosZ + (var11.posZ - var11.lastTickPosZ) * (double)var7;
      GL11.glTranslated(var1, var3, var5);
      GL11.glTranslated(-var14, -var16, -var18);
      GL11.glNormal3f(0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-var11.rotationYaw, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(var11.rotationPitch, 1.0F, 0.0F, 0.0F);
      GL11.glScalef(-var13, -var13, var13);
      GL11.glScaled(var8, var8, var8);
      GL11.glDisable(2896);
      GL11.glDepthMask(false);
      GL11.glDisable(2929);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      byte var20 = 0;
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      var12.drawString(var0, -var12.getStringWidth(var0) / 2, var20, 553648127);
      GL11.glDepthMask(true);
      var12.drawString(var0, -var12.getStringWidth(var0) / 2, var20, -1);
      GL11.glEnable(2896);
      GL11.glDisable(3042);
      GL11.glEnable(2929);
      GL11.glPopMatrix();
   }

   public static void a(int var0, int var1, int var2, float var3) {
      a((double)var0, (double)var1, (double)var2, 255.0F, 255.0F, 255.0F, 1.0D, var3);
   }

   public static void a(double var0, double var2, double var4, float var6, float var7, float var8, double var9, float var11) {
      EntityClientPlayerMP var12 = Minecraft.getMinecraft().thePlayer;
      double var13 = var0 + var9 / 2.0D;
      double var15 = var2 + var9 / 2.0D;
      double var17 = var4 + var9 / 2.0D;
      double var19 = -(var9 / 2.0D);
      double var21 = var9 / 2.0D;
      AxisAlignedBB var23 = AxisAlignedBB.getAABBPool().getAABB(var13 + var19, var15 + var19, var17 + var19, var13 + var21, var15 + var21, var17 + var21);
      GL11.glPushMatrix();
      GL11.glDisable(3008);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      GL11.glColor4f(var6, var7, var8, 0.4F);
      GL11.glLineWidth(5.0F);
      GL11.glDisable(3553);
      GL11.glDepthMask(false);
      double var24 = var12.lastTickPosX + (var12.posX - var12.lastTickPosX) * (double)var11;
      double var26 = var12.lastTickPosY + (var12.posY - var12.lastTickPosY) * (double)var11;
      double var28 = var12.lastTickPosZ + (var12.posZ - var12.lastTickPosZ) * (double)var11;
      a(var23.getOffsetBoundingBox(-var24, -var26, -var28));
      GL11.glDepthMask(true);
      GL11.glEnable(3553);
      GL11.glDisable(3042);
      GL11.glEnable(3008);
      GL11.glPopMatrix();
   }

   public static void a(AxisAlignedBB var0) {
      Tessellator var1 = Tessellator.instance;
      var1.startDrawing(3);
      var1.addVertex(var0.minX, var0.minY, var0.minZ);
      var1.addVertex(var0.maxX, var0.minY, var0.minZ);
      var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
      var1.addVertex(var0.minX, var0.minY, var0.maxZ);
      var1.addVertex(var0.minX, var0.minY, var0.minZ);
      var1.draw();
      var1.startDrawing(3);
      var1.addVertex(var0.minX, var0.maxY, var0.minZ);
      var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
      var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
      var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
      var1.addVertex(var0.minX, var0.maxY, var0.minZ);
      var1.draw();
      var1.startDrawing(1);
      var1.addVertex(var0.minX, var0.minY, var0.minZ);
      var1.addVertex(var0.minX, var0.maxY, var0.minZ);
      var1.addVertex(var0.maxX, var0.minY, var0.minZ);
      var1.addVertex(var0.maxX, var0.maxY, var0.minZ);
      var1.addVertex(var0.maxX, var0.minY, var0.maxZ);
      var1.addVertex(var0.maxX, var0.maxY, var0.maxZ);
      var1.addVertex(var0.minX, var0.minY, var0.maxZ);
      var1.addVertex(var0.minX, var0.maxY, var0.maxZ);
      var1.draw();
   }

   public static void a(EntityPlayer var0, int var1, int var2, float var3, float var4) {
      RenderManager.instance.livingPlayer = var0;
      RenderManager.instance.renderEngine = Minecraft.getMinecraft().renderEngine;
      if(f == null) {
         f = new RenderPlayer();
         f.setRenderManager(RenderManager.instance);
      } else {
         GL11.glEnable(2903);
         GL11.glPushMatrix();
         GL11.glTranslatef((float)var1, (float)var2, 50.0F);
         GL11.glScalef(-var3, var3, var3);
         GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
         GL11.glEnable(2884);
         GL11.glEnable(3008);
         GL11.glEnable(2929);
         GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
         RenderHelper.enableStandardItemLighting();
         GL11.glRotatef(-110.0F + var4, 0.0F, 1.0F, 0.0F);
         GL11.glTranslatef(0.0F, var0.yOffset, 0.0F);
         RenderManager.instance.playerViewY = 180.0F;
         f.doRender(var0, 0.0D, 0.0D, 0.0D, 0.0F, 0.625F);
         GL11.glPopMatrix();
         RenderHelper.disableStandardItemLighting();
         GL11.glDisable('\u803a');
         OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
         GL11.glDisable(3553);
         OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
      }
   }

   public static void a(String var0, double var1, double var3, double var5, float var7, float var8) {
      GL11.glPushMatrix();
      FontRenderer var9 = Minecraft.getMinecraft().fontRenderer;
      float var10 = 0.017F;
      GL11.glTranslatef((float)var1 + 0.0F, (float)var3 + 0.5F, (float)var5);
      GL11.glNormal3f(0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-var7, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(var8, 1.0F, 0.0F, 0.0F);
      GL11.glScalef(-var10, -var10, var10);
      GL11.glDisable(2896);
      GL11.glDepthMask(false);
      GL11.glDisable(2929);
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      Tessellator var11 = Tessellator.instance;
      byte var12 = 0;
      GL11.glDisable(3553);
      var11.startDrawingQuads();
      int var13 = var9.getStringWidth(var0) / 2;
      var11.setColorRGBA_F(0.0F, 0.0F, 0.0F, 0.25F);
      var11.addVertex((double)(-var13 - 1), (double)(-1 + var12), 0.0D);
      var11.addVertex((double)(-var13 - 1), (double)(8 + var12), 0.0D);
      var11.addVertex((double)(var13 + 1), (double)(8 + var12), 0.0D);
      var11.addVertex((double)(var13 + 1), (double)(-1 + var12), 0.0D);
      var11.draw();
      GL11.glEnable(3553);
      var9.drawString(var0, -var9.getStringWidth(var0) / 2, var12, 553648127);
      GL11.glEnable(2929);
      GL11.glDepthMask(true);
      var9.drawString(var0, -var9.getStringWidth(var0) / 2, var12, -1);
      GL11.glEnable(2896);
      GL11.glDisable(3042);
      GL11.glPopMatrix();
   }

}
