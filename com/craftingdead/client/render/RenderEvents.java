package com.craftingdead.client.render;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.bullet.BulletCollisionBox;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.client.render.RenderTickHandler;
import com.craftingdead.client.render.gun.IRenderPartialTick;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.cloud.cdplayer.EnumKarmaType;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.item.blueprint.Blueprint;
import com.craftingdead.item.blueprint.ItemBlueprint;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MovingObjectPosition;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.event.RenderPlayerEvent.Pre;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.PlayerEvent.NameFormat;

public class RenderEvents {

   private Minecraft b = Minecraft.getMinecraft();
   private ArrayList c = new ArrayList();
   public ArrayList a = new ArrayList();


   public RenderEvents() {
      MinecraftForge.EVENT_BUS.register(this);
   }

   @ForgeSubscribe
   public void a(NameFormat var1) {
      if(Minecraft.getMinecraft() != null && Minecraft.getMinecraft().thePlayer != null) {
         CDPlayer var2 = PlayerDataHandler.a(Minecraft.getMinecraft().getSession().getUsername());
         CDPlayer var3 = PlayerDataHandler.a(var1.username);
         var1.displayname = var1.username;
         if(var3.g != null && var3.g.length() > 0) {
            var1.displayname = "[" + var3.g + "] " + var1.username;
         }

         if(FactionManager.c().a(var2.b(), var1.username)) {
            var1.displayname = EnumChatFormatting.AQUA + var1.displayname;
         }
      }

   }

   @ForgeSubscribe
   public void a(Pre var1) {
      if(Minecraft.getMinecraft() != null && Minecraft.getMinecraft().thePlayer != null && var1.entity instanceof EntityPlayer) {
         EntityClientPlayerMP var2 = Minecraft.getMinecraft().thePlayer;
         PlayerData var3 = PlayerDataHandler.b(var2.username);
         RendererLivingEntity.NAME_TAG_RANGE = 64.0F;
         RendererLivingEntity.NAME_TAG_RANGE_SNEAK = 32.0F;
         EntityPlayer var4 = (EntityPlayer)var1.entity;
         if(!var3.C) {
            RendererLivingEntity.NAME_TAG_RANGE = 0.0F;
            RendererLivingEntity.NAME_TAG_RANGE_SNEAK = 0.0F;
            if(Minecraft.getMinecraft().objectMouseOver != null && Minecraft.getMinecraft().objectMouseOver.entityHit != null && Minecraft.getMinecraft().objectMouseOver.entityHit instanceof EntityPlayer) {
               EntityPlayer var5 = (EntityPlayer)Minecraft.getMinecraft().objectMouseOver.entityHit;
               if(var5.username.toLowerCase().equals(var4.username.toLowerCase())) {
                  RendererLivingEntity.NAME_TAG_RANGE = 64.0F;
                  RendererLivingEntity.NAME_TAG_RANGE_SNEAK = 32.0F;
               }
            }
         }

         if(FactionManager.c().e(var4.username)) {
            RendererLivingEntity.NAME_TAG_RANGE = 64.0F;
            RendererLivingEntity.NAME_TAG_RANGE_SNEAK = 32.0F;
         }

         if(RendererLivingEntity.NAME_TAG_RANGE > 0.0F && var4.getDistanceToEntity(var2) <= RendererLivingEntity.NAME_TAG_RANGE) {
            EnumKarmaType var14 = PlayerDataHandler.a(var4.username).a();
            if(var14 != null) {
               double var6 = var4.lastTickPosX + (var4.posX - var4.lastTickPosX) * (double)var1.partialRenderTick;
               double var8 = var4.lastTickPosY + (var4.posY - var4.lastTickPosY) * (double)var1.partialRenderTick;
               double var10 = var4.lastTickPosZ + (var4.posZ - var4.lastTickPosZ) * (double)var1.partialRenderTick;
               double var12 = 2.53D;
               if(var4.isSneaking()) {
                  var12 = 2.15D;
               }

               CDRenderHelper.a(var14.l, var6, var8 + var12, var10, var1.partialRenderTick, 1.2D);
            }
         }
      }

   }

   @ForgeSubscribe
   public void a(RenderWorldLastEvent var1) {
      EntityClientPlayerMP var2 = this.b.thePlayer;
      PlayerData var3 = PlayerDataHandler.a((EntityPlayer)var2);
      if(var2 != null) {
         if(var3.c) {
            Minecraft.getMinecraft().entityRenderer.renderHand(var1.partialTicks, 0);
         }

         if(RenderTickHandler.c) {
            for(int var4 = 0; var4 < this.c.size(); ++var4) {
               BulletCollisionBox var5 = (BulletCollisionBox)this.c.get(var4);
               CDRenderHelper.a(var5.a - 0.125D, var5.b - 0.125D, var5.c - 0.125D, 255.0F, 255.0F, 255.0F, 0.25D, var1.partialTicks);
            }
         }

         ItemStack var12 = var2.getCurrentEquippedItem();
         if(var12 != null) {
            IItemRenderer var13 = MinecraftForgeClient.getItemRenderer(var12, ItemRenderType.EQUIPPED_FIRST_PERSON);
            if(var13 != null && var13 instanceof IRenderPartialTick) {
               ((IRenderPartialTick)var13).a(var1.partialTicks);
            }

            if(var12.getItem() instanceof ItemBlueprint) {
               MovingObjectPosition var6 = var2.rayTrace(5.0D, 1.0F);
               if(var6 != null && var6.typeOfHit == EnumMovingObjectType.TILE) {
                  int var7 = var6.blockX;
                  int var8 = var6.blockY;
                  int var9 = var6.blockZ;
                  Blueprint var10 = ((ItemBlueprint)var12.getItem()).d();
                  if(var10.a() && !ItemBlueprint.b(var2.worldObj, var2, var7, var8, var9)) {
                     return;
                  }

                  boolean var11 = var10.a(var2, var12, (double)var7, (double)var8, (double)var9);
                  var10.a(var2, var12, var7, var8, var9, var11, var1.partialTicks);
               }
            }
         }
      }

   }

   public void a(BulletCollisionBox var1) {
      if(this.c.size() > 10) {
         this.c.remove(0);
      }

      this.c.add(var1);
   }

   public static void b(BulletCollisionBox var0) {
      if(RenderTickHandler.c) {
         CraftingDead.l().d().e.a().a(var0);
      }

   }
}
