package com.craftingdead.client.render;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.model.ModelHandcuffs;
import com.craftingdead.client.render.RenderBackpack;
import com.craftingdead.client.render.RenderFuelTanks;
import com.craftingdead.client.render.RenderTacticalVest;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.client.render.hat.RenderHat;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.cloud.cdplayer.EnumKarmaType;
import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemClothing;
import com.craftingdead.item.ItemFuelTankBackpack;
import com.craftingdead.item.ItemHat;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.ItemTacticalVest;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.Map;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import org.lwjgl.opengl.GL11;

public class RenderPlayerEvents {

   public boolean renderCDCape = false;
   public String a = "cd1";
   private ModelHandcuffs b = new ModelHandcuffs();


   public static RenderPlayerEvents instance() {
      return CraftingDead.l().d().e.b();
   }

   public void onPreRender(AbstractClientPlayer var1, ModelBiped var2, ModelBiped var3, ModelBiped var4, RenderPlayer var5) {
      PlayerData var6 = PlayerDataHandler.a((EntityPlayer)var1);
      if(var6.c) {
         var2.aimedBow = var3.aimedBow = var4.aimedBow = true;
      }

      if(var6.Q) {
         var2.isRestrained = var3.isRestrained = var4.isRestrained = true;
      }

      if(var1.getCurrentEquippedItem() != null && var1.getCurrentEquippedItem().getItem().itemID == ItemManager.N.itemID) {
         var2.isMinigunHeld = var3.isMinigunHeld = var4.isMinigunHeld = true;
      }

   }

   public void onPostRender(AbstractClientPlayer var1, ModelBiped var2, ModelBiped var3, ModelBiped var4, RenderPlayer var5) {
      var2.aimedBow = var3.aimedBow = var4.aimedBow = false;
      var2.isRestrained = var3.isRestrained = var4.isRestrained = false;
      var2.isMinigunHeld = var3.isMinigunHeld = var4.isMinigunHeld = false;
   }

   public void onRenderSpecials(AbstractClientPlayer var1, ModelBiped var2, ModelBiped var3, ModelBiped var4, RenderPlayer var5) {
      PlayerData var6 = PlayerDataHandler.a((EntityPlayer)var1);
      InventoryCDA var7 = var6.d();
      this.renderCDCape = true;
      ItemStack var8;
      if(var7.a("gun") != null) {
         var8 = var7.a("gun");
         IItemRenderer var9 = MinecraftForgeClient.getItemRenderer(var8, ItemRenderType.EQUIPPED);
         GL11.glPushMatrix();
         if(var1.isSneaking()) {
            GL11.glRotatef(30.0F, 1.0F, 0.0F, 0.0F);
            GL11.glTranslatef(0.0F, 0.1F, -0.02F);
         }

         if(var1 != null && var9 != null && var9 instanceof RenderGun) {
            ((RenderGun)var9).d((EntityPlayer)var1, var8);
         }

         GL11.glPopMatrix();
         this.renderCDCape = false;
      }

      if(var7.a("melee") != null) {
         var8 = var7.a("melee");
         GL11.glPushMatrix();
         if(var1.isSneaking()) {
            GL11.glRotatef(30.0F, 1.0F, 0.0F, 0.0F);
            GL11.glTranslatef(0.0F, 0.1F, -0.02F);
         }

         GL11.glTranslatef(0.25F, 1.4F, 0.18F);
         GL11.glRotatef(-45.0F, 0.0F, 0.0F, 1.0F);
         double var11 = 1.5D;
         GL11.glScaled(var11, var11, var11);
         this.renderDroppedItem(var8);
         GL11.glPopMatrix();
      }

      if(var7.a("backpack") != null) {
         var8 = var7.a("backpack");
         if(var8.getItem() instanceof ItemBackpack) {
            ItemBackpack var12 = (ItemBackpack)var8.getItem();
            RenderBackpack var10 = var12.d();
            Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/backpacks/" + var10.a() + ".png"));
            GL11.glPushMatrix();
            if(var1.isSneaking()) {
               GL11.glRotatef(30.0F, 1.0F, 0.0F, 0.0F);
               GL11.glTranslatef(0.0F, 0.0F, -0.02F);
            }

            var10.a(var1, var8);
            GL11.glPopMatrix();
            this.renderCDCape = false;
         }

         if(var8.getItem() instanceof ItemFuelTankBackpack) {
            ItemFuelTankBackpack var14 = (ItemFuelTankBackpack)var8.getItem();
            RenderFuelTanks var17 = var14.d();
            Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/backpacks/" + var17.a() + ".png"));
            GL11.glPushMatrix();
            if(var1.isSneaking()) {
               GL11.glRotatef(30.0F, 1.0F, 0.0F, 0.0F);
               GL11.glTranslatef(0.0F, 0.0F, -0.02F);
            }

            var17.a(var1, var8);
            GL11.glPopMatrix();
         }
      }

      if(var7.a("vest") != null) {
         var8 = var7.a("vest");
         ItemTacticalVest var16 = (ItemTacticalVest)var8.getItem();
         RenderTacticalVest var18 = var16.d();
         Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/tacticalvests/" + var18.a() + ".png"));
         GL11.glPushMatrix();
         if(var1.isSneaking()) {
            GL11.glRotatef(30.0F, 1.0F, 0.0F, 0.0F);
            GL11.glTranslatef(0.0F, 0.0F, -0.02F);
         }

         var18.a(var1, var8);
         GL11.glPopMatrix();
      }

      if(var7.a("hat") != null) {
         var8 = var7.a("hat");
         GL11.glPushMatrix();
         var4.bipedHead.postRender(0.0625F);
         RenderHat var19 = ((ItemHat)var8.getItem()).g();
         if(var19 != null) {
            GL11.glPushMatrix();
            var19.a(var1, var8);
            GL11.glPopMatrix();
         }

         GL11.glPopMatrix();
      }

      if(var6.Q) {
         Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/handcuff.png"));
         GL11.glPushMatrix();
         float var13 = 1.4F;
         GL11.glScalef(var13, var13, var13);
         GL11.glPushMatrix();
         var4.bipedRightArm.postRender(0.0625F);
         this.b.k = true;
         GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
         GL11.glTranslated(-0.32D, -0.06D, -0.4D);
         this.b.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
         GL11.glPushMatrix();
         var4.bipedLeftArm.postRender(0.0625F);
         this.b.j = true;
         GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
         GL11.glTranslated(0.07D, -0.06D, -0.4D);
         this.b.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
         GL11.glPushMatrix();
         this.b.l = true;
         GL11.glTranslated(-0.22D, 0.34D, 0.14D);
         GL11.glScalef(1.3F, 1.0F, 1.0F);
         this.b.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
         GL11.glPopMatrix();
      }

      Map var15 = CraftingDead.l().f().n();
      if(Minecraft.getMinecraft().theWorld != null && var15.containsKey(var6.a)) {
         this.renderCDCape = true;
         this.a = (String)var15.get(var6.a);
      } else {
         this.renderCDCape = false;
      }

   }

   public void renderPlayerModel(RenderPlayer var1, ModelBase var2, EntityLivingBase var3, float var4, float var5, float var6, float var7, float var8, float var9) {
      EntityPlayer var10 = (EntityPlayer)var3;
      if(!var3.isInvisible()) {
         var2.render(var3, var4, var5, var6, var7, var8, var9);
      } else if(!var3.isInvisibleToPlayer(Minecraft.getMinecraft().thePlayer)) {
         GL11.glPushMatrix();
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.15F);
         GL11.glDepthMask(false);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glAlphaFunc(516, 0.003921569F);
         var2.render(var3, var4, var5, var6, var7, var8, var9);
         GL11.glDisable(3042);
         GL11.glAlphaFunc(516, 0.1F);
         GL11.glPopMatrix();
         GL11.glDepthMask(true);
      } else {
         var2.setRotationAngles(var4, var5, var6, var7, var8, var9, var3);
      }

      PlayerData var11 = PlayerDataHandler.a(var10);
      CDPlayer var12 = PlayerDataHandler.a(var10.username);
      EnumKarmaType var13 = var12.a();
      if(var12 != null && var13 != null && CraftingDead.l().e().i) {
         ResourceLocation var14 = new ResourceLocation("craftingdead", "textures/models/clothing/clothing_karma_bandit.png");
         if(var13.k) {
            var14 = new ResourceLocation("craftingdead", "textures/models/clothing/clothing_karma_hero.png");
         }

         Minecraft.getMinecraft().renderEngine.bindTexture(var14);
         GL11.glDepthMask(false);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glAlphaFunc(516, 0.003921569F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         var2.render(var3, var4, var5, var6, var7, var8, var9);
         GL11.glDisable(3042);
         GL11.glAlphaFunc(516, 0.1F);
         GL11.glDepthMask(true);
      }

      if(var11.d().a("clothing") != null) {
         ItemStack var17 = var11.d().a("clothing");
         String var15 = ItemClothing.a(var17);
         if(var15 != null) {
            ResourceLocation var16 = new ResourceLocation("craftingdead", "textures/models/clothing/" + var15 + ".png");
            Minecraft.getMinecraft().renderEngine.bindTexture(var16);
            GL11.glDepthMask(false);
            GL11.glEnable(3042);
            GL11.glBlendFunc(770, 771);
            GL11.glAlphaFunc(516, 0.003921569F);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            var2.render(var3, var4, var5, var6, var7, var8, var9);
            GL11.glDisable(3042);
            GL11.glAlphaFunc(516, 0.1F);
            GL11.glDepthMask(true);
         }
      }

   }

   public void onPostFirstPersonRender(EntityPlayer var1, RenderPlayer var2, ModelBiped var3) {
      PlayerData var4 = PlayerDataHandler.a(var1);
      if(var4.d().a("clothing") != null) {
         ItemStack var5 = var4.d().a("clothing");
         String var6 = ItemClothing.a(var5);
         if(var6 != null) {
            ResourceLocation var7 = new ResourceLocation("craftingdead", "textures/models/clothing/" + var6 + ".png");
            Minecraft.getMinecraft().renderEngine.bindTexture(var7);
            GL11.glEnable(3042);
            GL11.glBlendFunc(770, 771);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            var3.onGround = 0.0F;
            var3.setRotationAngles(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F, var1);
            var3.bipedRightArm.render(0.0625F);
            GL11.glDisable(3042);
         }
      }

   }

   private void renderDroppedItem(ItemStack var1) {
      Tessellator var2 = Tessellator.instance;
      TextureManager var3 = Minecraft.getMinecraft().getTextureManager();
      Object var4 = var1.getItem().getIcon(var1, 0);
      if(var4 == null) {
         ResourceLocation var5 = var3.getResourceLocation(var1.getItemSpriteNumber());
         var4 = ((TextureMap)var3.getTexture(var5)).getAtlasSprite("missingno");
      }

      float var12 = ((Icon)var4).getMinU();
      float var6 = ((Icon)var4).getMaxU();
      float var7 = ((Icon)var4).getMinV();
      float var8 = ((Icon)var4).getMaxV();
      if(var1 != null) {
         if(var1.getItemSpriteNumber() == 0) {
            var3.bindTexture(TextureMap.locationBlocksTexture);
         } else {
            var3.bindTexture(TextureMap.locationItemsTexture);
         }

         GL11.glPushMatrix();
         float var9 = 0.0625F;
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         double var10 = 0.6D;
         GL11.glScaled(var10, var10, var10);
         GL11.glTranslatef(0.3F, -1.3F, 0.0F);
         ItemRenderer.renderItemIn2D(var2, var6, var7, var12, var8, ((Icon)var4).getIconWidth(), ((Icon)var4).getIconHeight(), var9);
         GL11.glPopMatrix();
      }

   }
}
