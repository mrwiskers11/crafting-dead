package com.craftingdead.client.render;

import net.minecraft.item.ItemStack;

public interface IRenderOnGUI {

   void a(ItemStack var1, double var2, double var4);

   void b(ItemStack var1, double var2, double var4);

   void a(ItemStack var1, double var2, double var4, double var6);
}
