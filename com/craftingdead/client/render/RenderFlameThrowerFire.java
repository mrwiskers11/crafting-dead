package com.craftingdead.client.render;

import com.craftingdead.entity.EntityFlameThrowerFire;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderFlameThrowerFire extends Render {

   public void a(EntityFlameThrowerFire var1, double var2, double var4, double var6, float var8, float var9) {}

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.a((EntityFlameThrowerFire)var1, var2, var4, var6, var8, var9);
   }

   protected ResourceLocation getEntityTexture(Entity var1) {
      return null;
   }
}
