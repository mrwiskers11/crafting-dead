package com.craftingdead.client.render;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.model.ModelCrate;

public class RenderLootcrate implements IItemRenderer {

   private ResourceLocation a;
   private ModelCrate b = new ModelCrate();


   public RenderLootcrate(String var1) {
      this.a = new ResourceLocation("craftingdead:textures/models/crates/" + var1 + ".png");
   }

   public boolean handleRenderType(ItemStack var1, ItemRenderType var2) {
      return var2 != ItemRenderType.FIRST_PERSON_MAP;
   }

   public boolean shouldUseRenderHelper(ItemRenderType var1, ItemStack var2, ItemRendererHelper var3) {
      return true;
   }

   public void renderItem(ItemRenderType var1, ItemStack var2, Object ... var3) {
      Minecraft var4 = Minecraft.getMinecraft();
      var4.getTextureManager().bindTexture(this.a);
      if(var1 == ItemRenderType.EQUIPPED) {
         EntityPlayer var5 = (EntityPlayer)var3[1];
         GL11.glPushMatrix();
         double var6 = 1.6D;
         GL11.glScaled(var6, var6, var6);
         GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
         GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
         GL11.glTranslated(0.1D, -0.5D, -0.2D);
         this.b.render(var5, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
      } else {
         double var8;
         if(var1 == ItemRenderType.INVENTORY) {
            GL11.glPushMatrix();
            var8 = 1.6D;
            GL11.glScaled(var8, var8, var8);
            GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
            GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
            GL11.glTranslated(0.0D, -0.2D, 0.0D);
            this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
         } else if(var1 == ItemRenderType.EQUIPPED_FIRST_PERSON) {
            GL11.glPushMatrix();
            var8 = 1.6D;
            GL11.glScaled(var8, var8, var8);
            GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
            GL11.glRotated(300.0D, 0.0D, 1.0D, 0.0D);
            GL11.glTranslated(0.1D, -0.8D, 0.1D);
            this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
         } else if(var1 == ItemRenderType.ENTITY) {
            GL11.glPushMatrix();
            var8 = 0.8D;
            GL11.glScaled(var8, var8, var8);
            GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
            GL11.glTranslated(-0.1D, -0.38D, -0.1D);
            this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
         }
      }

   }

   public static RenderLootcrate a(ItemStack var0) {
      return (RenderLootcrate)MinecraftForgeClient.getItemRenderer(var0, (ItemRenderType)null);
   }

   public void a(ItemStack var1, double var2, double var4, double var6, boolean var8, double var9) {
      Minecraft var11 = Minecraft.getMinecraft();
      var11.getTextureManager().bindTexture(this.a);
      GL11.glPushMatrix();
      GL11.glDisable(2884);
      GL11.glEnable(2929);
      GL11.glTranslated(var2 + var6 / 2.0D, var4 + var6 / 2.0D, 0.0D);
      GL11.glScaled(var6, -var6, var6);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
      GL11.glTranslated(0.5D, -0.6D, 0.0D);
      GL11.glRotated(15.0D, 1.0D, 0.0D, 0.0D);
      GL11.glRotated(-35.0D, 0.0D, -1.0D, 0.0D);
      double var12 = 1.5D;
      GL11.glScaled(var12, var12, var12);
      GL11.glRotated(-var9, 0.0D, 1.0D, 0.0D);
      this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glDisable(2929);
      GL11.glEnable(2884);
      GL11.glPopMatrix();
   }
}
