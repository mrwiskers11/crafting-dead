package com.craftingdead.client.render;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.ClientNotification;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.client.render.RenderEvents;
import com.craftingdead.client.render.RenderHUD;
import com.craftingdead.client.render.RenderPlayerEvents;
import com.craftingdead.item.ItemBinoculars;
import com.craftingdead.item.ItemFlameThrower;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.EnumFireMode;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemAmmo;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.relauncher.Side;
import java.text.DecimalFormat;
import java.util.ArrayList;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.particle.EntityBreakingFX;
import net.minecraft.client.particle.EntityCloudFX;
import net.minecraft.client.particle.EntityDiggingFX;
import net.minecraft.client.particle.EntityExplodeFX;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.particle.EntityFlameFX;
import net.minecraft.client.particle.EntityHugeExplodeFX;
import net.minecraft.client.particle.EntityLargeExplodeFX;
import net.minecraft.client.particle.EntitySmokeFX;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.client.GuiIngameForge;
import org.lwjgl.opengl.GL11;

public class RenderTickHandler {

   private Minecraft k = Minecraft.getMinecraft();
   private World l;
   private ResourceLocation m = new ResourceLocation("craftingdead", "textures/gui/icon.png");
   private RenderPlayerEvents n = new RenderPlayerEvents();
   public boolean a = true;
   public static final boolean b = false;
   public static boolean c = false;
   public static final double d = 128.0D;
   public static final double e = 128.0D;
   private boolean o = false;
   private boolean p = false;
   public boolean f = false;
   public float g = 1.0F;
   private int q = 0;
   public int h = 0;
   private RenderEvents r = new RenderEvents();
   private RenderHUD s = new RenderHUD();
   public boolean i = true;
   public int[] j = new int[21];
   private ResourceLocation t = new ResourceLocation("craftingdead", "textures/misc/blood1.png");
   private ResourceLocation u = new ResourceLocation("craftingdead", "textures/misc/blood2.png");
   private float v = 0.0F;


   public void a(Minecraft var1, float var2) {
      c = CraftingDead.l().e().f;
      ScaledResolution var3 = new ScaledResolution(var1.gameSettings, var1.displayWidth, var1.displayHeight);
      int var4 = var3.getScaledWidth();
      int var5 = var3.getScaledHeight();
      int var6 = var4 / 2;
      int var7 = var5 / 2;
      boolean var8 = true;
      this.a(var1);
      EntityClientPlayerMP var9 = var1.thePlayer;
      PlayerData var10 = PlayerDataHandler.a((EntityPlayer)var9);
      this.l = var1.theWorld;
      if(var9 != null && !var9.isDead) {
         if(var1.currentScreen == null && var1.inGameHasFocus) {
            this.a(var10, (double)var4, (double)var5);
            if(var10.Q) {
               int var11 = 200 - var10.R;
               CDRenderHelper.b(EnumChatFormatting.WHITE + "" + var11 + "/" + 200, var6 + 1, var7 + 10);
               CDRenderHelper.c(new ItemStack(ItemManager.fe), (double)var6 - 19.5D, (double)(var7 - 30), 2.5D);
            }
         }

         ItemStack var20 = var9.getCurrentEquippedItem();
         boolean var12 = true;
         boolean var13 = false;
         boolean var14 = true;
         this.f = false;
         this.g = 0.35F;
         int var17;
         if(!var9.capabilities.isCreativeMode && (var1.currentScreen == null || var1.currentScreen instanceof GuiChat)) {
            int var15 = var5 - 53;
            if(!var9.isInsideOfMaterial(Material.water) && !var10.c) {
               int var16 = (int)(20.0D * var10.e().d());

               for(var17 = 0; var17 < 20; ++var17) {
                  if(var16 <= 5) {
                     var15 -= this.j[var17];
                  }

                  if(var17 % 2 == 0) {
                     this.a(var1, (double)(var6 + 77 - 8 * var17 + 8 * var17 / 2), (double)var15, 0, 32, 9, 1.0D);
                  }

                  if(var17 < var16) {
                     if(var16 == 0) {
                        continue;
                     }

                     if(var17 == var16 - 1 && var16 % 2 != 0) {
                        this.a(var1, (double)(var6 + 77 - 8 * var17 + 8 * var17 / 2), (double)var15, 18, 32, 9, 1.0D);
                        continue;
                     }

                     if(var17 % 2 == 0) {
                        this.a(var1, (double)(var6 + 77 - 8 * var17 + 8 * var17 / 2), (double)var15, 9, 32, 9, 1.0D);
                        continue;
                     }
                  }

                  if(var16 <= 5) {
                     var15 += this.j[var17];
                  }
               }
            }
         }

         ResourceLocation var26;
         float var27;
         if(var9 != null && var9.getHealth() < 20.0F && CraftingDead.l().e().h) {
            var27 = 1.0F - var9.getHealth() / 20.0F;
            var26 = this.t;
            if(var9.getHealth() <= 19.0F) {
               if(var9.getHealth() <= 6.0F) {
                  var26 = this.u;
               }

               CDRenderHelper.a(0.0D, 0.0D, var26, (double)var4, (double)var5, var27);
            }
         }

         PlayerData var31;
         ItemStack var35;
         if(var20 != null) {
            if(var20.getItem() instanceof ItemGun) {
               this.a = true;
               ItemGun var29 = (ItemGun)var20.getItem();
               if(this.o != var10.c) {
                  if(var10.c) {
                     this.q = var1.gameSettings.thirdPersonView;
                     if(var1.gameSettings.thirdPersonView != 0) {
                        var1.gameSettings.thirdPersonView = 0;
                     }
                  } else {
                     var1.gameSettings.thirdPersonView = this.q;
                  }

                  this.o = var10.c;
               }

               if(var10.c) {
                  if(var1.currentScreen != null) {
                     var10.c = false;
                     return;
                  }

                  float var28 = var29.cI;
                  GunAttachment var30 = var29.d(var20, 0);
                  this.a = false;
                  var12 = false;
                  if(var30 != null) {
                     var28 = var30.d;
                  }

                  if(var30 != null && var30.u != null) {
                     this.f = true;
                     var13 = true;
                     if(!this.p) {
                        var1.sndManager.playSoundFX("craftingdead:zoomin", 1.0F, 1.0F);
                        this.p = true;
                     }

                     GL11.glDisable(2929);
                     GL11.glEnable(3042);
                     GL11.glBlendFunc(770, 771);
                     GL11.glDisable(3008);
                     GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                     var1.renderEngine.bindTexture(var30.u);
                     Tessellator var18 = Tessellator.instance;
                     var18.startDrawingQuads();
                     var18.addVertexWithUV((double)(var4 / 2 - 2 * var5), (double)var5, -90.0D, 0.0D, 1.0D);
                     var18.addVertexWithUV((double)(var4 / 2 + 2 * var5), (double)var5, -90.0D, 1.0D, 1.0D);
                     var18.addVertexWithUV((double)(var4 / 2 + 2 * var5), 0.0D, -90.0D, 1.0D, 0.0D);
                     var18.addVertexWithUV((double)(var4 / 2 - 2 * var5), 0.0D, -90.0D, 0.0D, 0.0D);
                     var18.draw();
                     GL11.glEnable(3008);
                     GL11.glDisable(3042);
                     GL11.glEnable(3008);
                  }

                  if(var28 > 1.0F) {
                     this.b(var1, var28);
                     var14 = false;
                  }
               } else {
                  this.p = false;
               }

               if(!var29.cM) {
                  this.a = false;
                  var12 = false;
               }

               if(this.a && var1.currentScreen == null) {
                  var12 = false;
                  double var32 = (double)(var10.e * 5.0F);
                  GL11.glPushMatrix();
                  double var33 = 0.5D;
                  this.a(var1, (double)(var6 - 8), (double)(var7 - 8), 32, 0, 16, var33);
                  this.a(var1, (double)var6 - (14.0D + var32), (double)(var7 - 8), 16, 0, 16, var33);
                  this.a(var1, (double)var6 - (2.0D - var32), (double)(var7 - 8), 16, 0, 16, var33);
                  this.a(var1, (double)(var6 - 8), (double)var7 - (14.0D + var32), 0, 0, 16, var33);
                  this.a(var1, (double)(var6 - 8), (double)var7 - (2.0D - var32), 0, 0, 16, var33);
                  GL11.glPopMatrix();
               }

               if(!var13 && var1.currentScreen == null) {
                  var35 = var29.w(var20);
                  if(var35 != null) {
                     var17 = var29.r(var20);
                     int var36 = ((ItemAmmo)var29.w(var20).getItem()).d;
                     CDRenderHelper.a(EnumChatFormatting.WHITE + "" + var17 + "/" + var36, var6 - 75, var5 - 60);
                     CDRenderHelper.a(var35, var6 - 92, var5 - 65);
                  }

                  if(this.h > 0) {
                     CDRenderHelper.a(EnumChatFormatting.WHITE + "Firemode " + var29.e().toString(), var6 - 88, var5 - 72);
                  }
               }
            } else if(var20.getItem() instanceof ItemBinoculars) {
               this.a = true;
               if(this.o != var10.d) {
                  if(var10.d) {
                     this.q = var1.gameSettings.thirdPersonView;
                     if(var1.gameSettings.thirdPersonView != 0) {
                        var1.gameSettings.thirdPersonView = 0;
                     }
                  } else {
                     var1.gameSettings.thirdPersonView = this.q;
                  }

                  this.o = var10.d;
               }

               if(var10.d) {
                  if(var1.currentScreen != null) {
                     var10.d = false;
                     return;
                  }

                  var27 = 14.0F;
                  this.a = false;
                  var12 = false;
                  if(!this.p) {
                     var1.sndManager.playSoundFX("craftingdead:zoomin", 1.0F, 1.0F);
                     this.p = true;
                  }

                  var26 = new ResourceLocation("craftingdead:textures/misc/sights/binoscope.png");
                  GL11.glPushMatrix();
                  GL11.glDisable(2929);
                  GL11.glDepthMask(false);
                  GL11.glScaled(1.0D, 1.0D, 0.0D);
                  GL11.glBlendFunc(770, 771);
                  GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                  var1.renderEngine.bindTexture(var26);
                  Tessellator var34 = Tessellator.instance;
                  var34.startDrawingQuads();
                  var34.addVertexWithUV((double)(var4 / 2 - 2 * var5), (double)var5, -90.0D, 0.0D, 1.0D);
                  var34.addVertexWithUV((double)(var4 / 2 + 2 * var5), (double)var5, -90.0D, 1.0D, 1.0D);
                  var34.addVertexWithUV((double)(var4 / 2 + 2 * var5), 0.0D, -90.0D, 1.0D, 0.0D);
                  var34.addVertexWithUV((double)(var4 / 2 - 2 * var5), 0.0D, -90.0D, 0.0D, 0.0D);
                  var34.draw();
                  GL11.glDepthMask(true);
                  GL11.glEnable(2929);
                  GL11.glEnable(3008);
                  GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                  GL11.glPopMatrix();
                  if(var27 > 1.0F) {
                     this.b(var1, var27);
                     var14 = false;
                  }
               } else {
                  this.p = false;
               }
            }

            if(var20.getItem() instanceof ItemFlameThrower && var1.currentScreen == null) {
               var31 = PlayerDataHandler.b();
               if(var31.d() != null) {
                  CDRenderHelper.a(EnumChatFormatting.WHITE + "" + var31.P, var6 - 75, var5 - 60);
                  CDRenderHelper.a(new ItemStack(ItemManager.fb), var6 - 92, var5 - 65);
               }
            }
         }

         var31 = PlayerDataHandler.b();
         var35 = var31.d().a("hat");
         if(var35 != null && var1.currentScreen == null) {
            if(var9.isInWater() && var35.getItem() == ItemManager.gd) {
               CDRenderHelper.c(new ItemStack(var35.getItem()), (double)(var4 - 50), (double)(var7 / 10), 3.0D);
            }

            if(var31.l && var35.getItem() == ItemManager.fA) {
               CDRenderHelper.c(new ItemStack(var35.getItem()), (double)(var4 - 50), (double)(var7 / 10), 3.0D);
            }
         }

         if(!var13) {
            this.s.a(var1, var2);
         }

         if((var20 == null || !(var20.getItem() instanceof ItemGun)) && this.o) {
            var1.gameSettings.thirdPersonView = this.q;
            this.o = false;
         }

         if(var14) {
            this.b(var1, 1.0F);
         }

         GuiIngameForge.renderCrosshairs = var12;
         this.b(var10, (double)var4, (double)var5);
      }

      if(var1.currentScreen != null && var1.currentScreen instanceof GuiIngameMenu && !var1.isSingleplayer()) {
         PlayerData var21 = PlayerDataHandler.b();
         if(var21.b()) {
            String var23 = EnumChatFormatting.RED + "" + 5 + "s" + EnumChatFormatting.WHITE;
            CDRenderHelper.b("You are in combat! You must wait " + var23 + " to logout!", var4 / 2, 50);
            if(var21 != null) {
               DecimalFormat var24 = new DecimalFormat("0.00");
               String var25 = EnumChatFormatting.RED + "" + var24.format((double)var21.K / 20.0D) + "s" + EnumChatFormatting.WHITE;
               CDRenderHelper.b("Time remaining: " + var25, var4 / 2, 60);
            }
         }
      }

      ClientNotification var22 = CraftingDead.l().d().o;
      if(var22 != null && var8) {
         var22.a(var1);
      }

   }

   public void a(Minecraft var1) {
      if(var1.theWorld != null) {
         ArrayList var2 = (ArrayList)var1.theWorld.getLoadedEntityList();

         for(int var3 = 0; var3 < var2.size(); ++var3) {
            Entity var4 = (Entity)var2.get(var3);
            if(var4 instanceof EntityPlayer) {
               var4.renderDistanceWeight = 2.0D;
            }
         }

      }
   }

   public void a(PlayerData var1, double var2, double var4) {
      if(var1.l) {
         CDRenderHelper.b(0.0D, 0.0D, (double)((int)var2), (double)((int)var4), '\uff00', 0.15F);
      }

   }

   public void b(PlayerData var1, double var2, double var4) {
      float var6 = (float)var1.w * 1.0F / 6.0F;
      if(var6 >= 0.1F) {
         CDRenderHelper.b(0.0D, 0.0D, (double)((int)var2), (double)((int)var4), 16777215, var6);
      }

   }

   public void a(Minecraft var1, double var2, double var4, int var6, int var7, int var8, double var9) {
      var1.getTextureManager().bindTexture(this.m);
      GL11.glPushMatrix();
      GL11.glEnable(3042);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.9F);
      GL11.glScaled(var9, var9, var9);
      GL11.glTranslated(var2 * (1.0D / var9), var4 * (1.0D / var9), 0.0D);
      this.a(var8 / 2, var8 / 2, var6, var7, var8, var8);
      GL11.glDisable(3042);
      GL11.glPopMatrix();
   }

   private void a(int var1, int var2, int var3, int var4, int var5, int var6) {
      float var7 = 0.00390625F;
      float var8 = 0.00390625F;
      Tessellator var9 = Tessellator.instance;
      var9.startDrawingQuads();
      var9.addVertexWithUV((double)(var1 + 0), (double)(var2 + var6), (double)this.v, (double)((float)(var3 + 0) * var7), (double)((float)(var4 + var6) * var8));
      var9.addVertexWithUV((double)(var1 + var5), (double)(var2 + var6), (double)this.v, (double)((float)(var3 + var5) * var7), (double)((float)(var4 + var6) * var8));
      var9.addVertexWithUV((double)(var1 + var5), (double)(var2 + 0), (double)this.v, (double)((float)(var3 + var5) * var7), (double)((float)(var4 + 0) * var8));
      var9.addVertexWithUV((double)(var1 + 0), (double)(var2 + 0), (double)this.v, (double)((float)(var3 + 0) * var7), (double)((float)(var4 + 0) * var8));
      var9.draw();
   }

   public void b(Minecraft var1, float var2) {
      try {
         ObfuscationReflectionHelper.setPrivateValue(EntityRenderer.class, var1.entityRenderer, Double.valueOf((double)var2), new String[]{"cameraZoom", "Y", "field_78503_V"});
      } catch (Exception var4) {
         throw new RuntimeException(var4);
      }
   }

   public static void a(String var0, double var1, double var3, double var5, double var7, double var9, double var11) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT && d() != null && d().l != null) {
         d().b(var0, var1, var3, var5, var7, var9, var11);
      }

   }

   private EntityFX b(String var1, double var2, double var4, double var6, double var8, double var10, double var12) {
      if(this.k != null && this.k.renderViewEntity != null && this.k.effectRenderer != null) {
         double var14 = this.k.renderViewEntity.posX - var2;
         double var16 = this.k.renderViewEntity.posY - var4;
         double var18 = this.k.renderViewEntity.posZ - var6;
         Object var20 = null;
         if(var1.equals("hugeexplosion")) {
            this.k.effectRenderer.addEffect((EntityFX) (var20 = new EntityHugeExplodeFX(this.l, var2, var4, var6, var8, var10, var12)));
         } else if(var1.equals("largeexplode")) {
            this.k.effectRenderer.addEffect((EntityFX) (var20 = new EntityLargeExplodeFX(this.k.renderEngine, this.l, var2, var4, var6, var8, var10, var12)));
         }

         if(var20 != null) {
            return (EntityFX)var20;
         } else {
            double var21 = 128.0D;
            if(var14 * var14 + var16 * var16 + var18 * var18 > var21 * var21) {
               return null;
            } else {
               if(var1.equals("smoke")) {
                  var20 = new EntitySmokeFX(this.l, var2, var4, var6, var8, var10, var12);
               } else if(var1.equals("explode")) {
                  var20 = new EntityExplodeFX(this.l, var2, var4, var6, var8, var10, var12);
               } else if(var1.equals("flame")) {
                  var20 = new EntityFlameFX(this.l, var2, var4, var6, var8, var10, var12);
               } else if(var1.equals("largesmoke")) {
                  var20 = new EntitySmokeFX(this.l, var2, var4, var6, var8, var10, var12, 2.5F);
               } else if(var1.equals("cloud")) {
                  var20 = new EntityCloudFX(this.l, var2, var4, var6, var8, var10, var12);
               } else {
                  int var23;
                  String[] var24;
                  int var25;
                  if(var1.startsWith("iconcrack_")) {
                     var24 = var1.split("_", 3);
                     var23 = Integer.parseInt(var24[1]);
                     if(var24.length > 2) {
                        var25 = Integer.parseInt(var24[2]);
                        var20 = new EntityBreakingFX(this.l, var2, var4, var6, var8, var10, var12, Item.itemsList[var23], var25);
                     } else {
                        var20 = new EntityBreakingFX(this.l, var2, var4, var6, var8, var10, var12, Item.itemsList[var23], 0);
                     }
                  } else if(var1.startsWith("tilecrack_")) {
                     var24 = var1.split("_", 3);
                     var23 = Integer.parseInt(var24[1]);
                     var25 = Integer.parseInt(var24[2]);
                     var20 = (new EntityDiggingFX(this.l, var2, var4, var6, var8, var10, var12, Block.blocksList[var23], var25)).applyRenderColor(var25);
                  }
               }

               if(var20 != null) {
                  this.k.effectRenderer.addEffect((EntityFX)var20);
               }

               return (EntityFX)var20;
            }
         }
      } else {
         return null;
      }
   }

   public void a(EnumFireMode var1) {
      this.h = 60;
   }

   public RenderEvents a() {
      return this.r;
   }

   public RenderPlayerEvents b() {
      return this.n;
   }

   public RenderHUD c() {
      return this.s;
   }

   public static RenderTickHandler d() {
      return CraftingDead.l().d().e;
   }

}
