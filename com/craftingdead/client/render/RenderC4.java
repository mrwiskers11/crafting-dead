package com.craftingdead.client.render;

import com.craftingdead.client.model.ModelC4;
import com.craftingdead.entity.EntityGrenade;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderC4 extends Render {

   public ModelBase a = new ModelC4();


   public void a(EntityGrenade var1, double var2, double var4, double var6, float var8, float var9) {
      super.renderManager.renderEngine.bindTexture(this.getEntityTexture(var1));
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2, (float)var4 - 0.03F, (float)var6);
      double var10 = 0.04D;
      GL11.glScaled(var10, var10, var10);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      if(var1.motionX != 0.0D || var1.motionY != 0.0D) {
         GL11.glRotatef(var1.f * 35.0F, 1.0F, 0.0F, 1.0F);
      }

      this.a.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.625F);
      GL11.glPopMatrix();
   }

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.a((EntityGrenade)var1, var2, var4, var6, var8, var9);
   }

   protected ResourceLocation getEntityTexture(Entity var1) {
      return new ResourceLocation("craftingdead:textures/models/c4model.png");
   }
}
