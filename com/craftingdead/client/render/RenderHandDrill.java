package com.craftingdead.client.render;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.model.ModelHandDrill;

public class RenderHandDrill implements IItemRenderer {

   private ResourceLocation a = new ResourceLocation("craftingdead:textures/models/handdrill.png");
   private ModelHandDrill b = new ModelHandDrill();


   public boolean handleRenderType(ItemStack var1, ItemRenderType var2) {
      return var2 != ItemRenderType.FIRST_PERSON_MAP;
   }

   public boolean shouldUseRenderHelper(ItemRenderType var1, ItemStack var2, ItemRendererHelper var3) {
      return true;
   }

   public void renderItem(ItemRenderType var1, ItemStack var2, Object ... var3) {
      Minecraft var4 = Minecraft.getMinecraft();
      var4.getTextureManager().bindTexture(this.a);
      if(var1 == ItemRenderType.EQUIPPED) {
         EntityPlayer var5 = (EntityPlayer)var3[1];
         GL11.glPushMatrix();
         double var6 = 1.8D;
         GL11.glScaled(var6, var6, var6);
         GL11.glRotated(210.0D, 1.0D, 0.6D, 1.0D);
         GL11.glRotated(296.0D, 1.0D, 0.9D, 0.0D);
         GL11.glTranslated(0.6D, -0.3D, 0.0D);
         this.b.render(var5, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
      } else {
         double var8;
         if(var1 == ItemRenderType.INVENTORY) {
            GL11.glPushMatrix();
            var8 = 2.0D;
            GL11.glScaled(var8, var8, var8);
            GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
            GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
            GL11.glTranslated(-0.2D, 0.1D, 0.0D);
            this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
         } else if(var1 == ItemRenderType.EQUIPPED_FIRST_PERSON) {
            GL11.glPushMatrix();
            var8 = 1.6D;
            GL11.glScaled(var8, var8, var8);
            GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
            GL11.glRotated(300.0D, 0.0D, 1.0D, 0.0D);
            GL11.glTranslated(0.2D, -0.7D, 0.0D);
            this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
         } else if(var1 == ItemRenderType.ENTITY) {
            GL11.glPushMatrix();
            var8 = 0.8D;
            GL11.glScaled(var8, var8, var8);
            GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
            GL11.glTranslated(-0.1D, -0.38D, -0.1D);
            this.b.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glPopMatrix();
         }
      }

   }
}
