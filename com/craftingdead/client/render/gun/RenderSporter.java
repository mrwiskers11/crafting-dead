package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelM4A1IS1;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.model.gun.ModelSporter22;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderSporter extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.7F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.5F, 0.55F, 0.0F);
      double var3 = 1.0D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.1F;
      super.e = 0.0F;
      super.f = -2.1F;
      super.g = 1.2F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.4F, -0.2F, 0.2F);
      double var3 = 0.75D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-24.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.1F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.1F, -0.677F, 0.9515F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.03F, 0.0F);
      }

      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-0.7F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.9D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.5F, 0.15F, 0.16F);
      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(0.9F, -0.7F, -0.145F);
      var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-2.3F, 1.24F, -0.025F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m4a1modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(1.625F, -0.091F, 0.104F);
      double var1 = 0.25D;
      GL11.glScaled(var1 + 0.75D, var1, var1);
      GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/g18modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.h) {
         GL11.glTranslated(0.85D, -1.7D, 0.55D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.i) {
         GL11.glTranslated(0.85D, -1.7D, 0.55D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.e) {
         GL11.glTranslated(1.0D, -0.95D, 0.545D);
         var4 = 0.65D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.f) {
         GL11.glDisable(2884);
         GL11.glTranslated(1.25D, -0.93D, 0.81D);
         var4 = 0.45D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.l) {
         GL11.glTranslated(22.0D, -0.55D, 1.5D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.j) {
         GL11.glTranslated(9.5D, 1.2D, 1.35D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.k) {
         GL11.glTranslated(7.5D, 1.2D, 0.4D);
         var4 = 0.85D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.01F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, 0.0F, 0.0F);
   }

   protected ModelBase a() {
      this.i = new ModelM4A1IS1();
      this.j = new ModelPistolIS2();
      return new ModelSporter22();
   }

   protected String b() {
      return "sporter22model";
   }
}
