package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelAKMIS1;
import com.craftingdead.client.model.gun.ModelAKMIS2;
import com.craftingdead.client.model.gun.ModelRPK;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderRPK extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.75F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(0.2F, 0.6F, 0.0F);
      double var3 = 0.8D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.0F;
      super.e = 0.1F;
      super.f = -1.8F;
      super.g = 1.2F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.35F, 0.5F);
      double var3 = 0.5D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-2.0F, 1.0F, 0.0F, 0.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.002F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.0F, -0.13F, 0.92F);
      double var3 = 0.9D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-1.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(0.25F, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(1.25F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.3F, -0.2F, 0.005F);
      GL11.glTranslatef(0.0F, 0.0F, 0.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.85D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.5D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.5F, 0.4F, 0.25F);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(0.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.51D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-0.2F, -0.4F, 0.12F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/rpkmodelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(1.7F, -0.25F, 0.07F);
      double var1 = 0.25D;
      GL11.glScaled(var1, var1, var1);
      GL11.glDisable(2884);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/rpkmodelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.e) {
         GL11.glTranslated(-5.1D, -2.0D, 0.43D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      } else {
         if(var3 == GunAttachment.l) {
            GL11.glTranslated(23.8D, -0.16D, 1.4D);
            var4 = 0.8D;
            GL11.glScaled(var4, var4, var4);
         }

         if(var3 == GunAttachment.f) {
            GL11.glDisable(2884);
            GL11.glTranslated(-5.0D, -1.945D, 0.775D);
            var4 = 0.5D;
            GL11.glScaled(var4, var4, var4);
         }

      }
   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.01F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.9F, 0.0F, 0.6F);
   }

   protected ModelBase a() {
      this.i = new ModelAKMIS1();
      this.j = new ModelAKMIS2();
      return new ModelRPK();
   }

   protected String b() {
      return "rpkmodel";
   }
}
