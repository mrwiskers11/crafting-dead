package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelAK47;
import com.craftingdead.client.model.gun.ModelAKMIS1;
import com.craftingdead.client.model.gun.ModelAKMIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderAK47 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.9F, -0.7F, 0.4F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(0.3F, 0.6F, 0.0F);
      double var3 = 0.35D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.1F;
      super.e = -0.1F;
      super.f = -2.19F;
      super.g = 1.2F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-30.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.8F, -0.14F, 0.1F);
      double var3 = 0.35D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.002F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.48F, 1.0F);
      double var3 = 0.15D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-1.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(0.25F, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(1.25F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.3F, -0.2F, 0.005F);
      GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      if(ItemGun.H().d(var2, 0) != null) {
         GunAttachment var5 = ItemGun.H().d(var2, 0);
         if(var5 == GunAttachment.e) {
            GL11.glTranslatef(0.0F, 0.015F, 0.0F);
         }

         if(var5 == GunAttachment.f) {
            GL11.glTranslatef(0.0F, 0.02F, 0.0F);
         }
      }

   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.3D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.18D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(0.0F, 0.8F, 0.9F);
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.h) {
         GL11.glTranslatef(-28.0F, -9.0F, -0.35F);
         var4 = 2.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.i) {
         GL11.glTranslatef(-28.0F, -9.0F, -0.35F);
         var4 = 2.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.e) {
         GL11.glTranslatef(-30.0F, -5.4F, -0.35F);
         var4 = 2.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.f) {
         GL11.glDisable(2884);
         GL11.glTranslatef(-27.0F, -5.0F, 0.3F);
         var4 = 1.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.l) {
         GL11.glTranslatef(30.0F, -0.2F, 2.0F);
         var4 = 1.9D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.j) {
         GL11.glTranslatef(-3.0F, 3.2F, 2.0F);
         var4 = 3.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.k) {
         GL11.glTranslatef(-15.0F, 3.7F, -2.0F);
         var4 = 3.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.g) {
         GL11.glTranslated(-23.0D, -8.3D, 1.0D);
         var4 = 0.4D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.23F, -0.3F);
      } else {
         GL11.glTranslatef(-0.1F, 0.3F, 0.04F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(4.0F, 0.8F, 2.5F);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(0.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 1.1D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-3.2F, -0.48F, 0.02F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/akmmodelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.7F, -0.6F, 0.01F);
      double var1 = 0.8D;
      GL11.glScaled(var1, var1, var1);
      GL11.glDisable(2884);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/akmmodelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected ModelBase a() {
      this.i = new ModelAKMIS1();
      this.j = new ModelAKMIS2();
      return new ModelAK47();
   }

   protected String b() {
      return "ak47model";
   }
}
