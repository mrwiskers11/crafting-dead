package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelM9;
import com.craftingdead.client.model.gun.ModelPistolIS1;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderM9 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.05F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(78.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.0F, -0.28F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      double var3 = 1.1D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.6F;
      super.e = -0.25F;
      super.f = -1.9F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.3F, 0.35F);
      double var3 = 0.7D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-25.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.2F, -0.695F, 0.972F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.02F, 0.0F);
      }

      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(0.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-0.3F, -0.15F, -0.25F);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/g18modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.6F, -0.05F, 0.07F);
      double var1 = 0.25D;
      GL11.glScaled(var1, var1, var1);
      GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/g18modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 1.0D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.55D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(1.1F, -0.2F, -0.58F);
   }

   protected void d(Entity var1, ItemStack var2) {
      if(var2.itemID == ItemManager.ad.itemID) {
         GL11.glRotatef(5.0F, 0.0F, 0.0F, 1.0F);
      }

   }

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      if(var3 == GunAttachment.l) {
         GL11.glTranslatef(12.0F, -0.3F, 1.2F);
         double var4 = 0.9D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      GL11.glTranslatef(0.02F, 0.04F, -0.12F);
      if(var3) {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      } else {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      }

   }

   protected ModelBase a() {
      this.i = new ModelPistolIS1();
      this.j = new ModelPistolIS2();
      return new ModelM9();
   }

   protected String b() {
      return "m9model";
   }
}
