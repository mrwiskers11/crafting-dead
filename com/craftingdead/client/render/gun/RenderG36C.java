package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelG36C;
import com.craftingdead.client.model.gun.ModelM4A1IS1;
import com.craftingdead.client.model.gun.ModelM4A1IS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderG36C extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.8F, 0.4F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.2F, 0.55F, 0.0F);
      double var3 = 1.2D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.5F;
      super.e = -0.25F;
      super.f = -1.9F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-33.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.4F, -0.26F, 0.3F);
      double var3 = 1.1D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.2F, -0.34F, 0.95F);
      double var3 = 1.1D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-0.4F, 1.0F, 0.0F, 0.0F);
      GL11.glTranslatef(-0.6F, 0.0F, 0.0F);
      GL11.glTranslatef(0.0F, 0.033F, 0.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 1.3D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.7D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.6F, 0.32F, 0.28F);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(0.3F, -0.21F, 0.22F);
      var1 = 0.75D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-0.5F, -0.04F, -0.4F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m4a1modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.63F, -0.15F, 0.041F);
      double var1 = 0.2D;
      GL11.glScaled(var1, var1, var1);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m4a1modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {
      if(var2.itemID == ItemManager.R.itemID) {
         GL11.glTranslated(0.8D, -1.5D, 0.2D);
         double var3 = 1.2D;
         GL11.glScaled(var3, var3, var3);
      }

   }

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.e) {
         GL11.glTranslated(-1.0D, -1.4D, 0.17D);
         var4 = 0.75D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.f) {
         GL11.glTranslated(-0.3D, -1.63D, 0.55D);
         var4 = 0.3D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.j) {
         GL11.glTranslated(9.0D, 0.8D, 1.04D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.g) {
         GL11.glTranslated(1.2D, -2.2D, 0.65D);
         var4 = 0.09D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.0F, 0.19F, 0.0F);
      }

   }

   protected ModelBase a() {
      this.i = new ModelM4A1IS1();
      this.j = new ModelM4A1IS2();
      return new ModelG36C();
   }

   protected String b() {
      return "g36cmodel";
   }
}
