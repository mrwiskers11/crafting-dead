package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelMP5A5;
import com.craftingdead.client.model.gun.ModelPistolIS1;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderMP5A5 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, 0.7F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(78.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.0F, -0.0F, 0.55F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(0.15F, -0.05F, 0.0F);
      double var3 = 0.46D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.43F;
      super.e = -0.3F;
      super.f = -1.85F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-32.0F, 0.0F, 0.0F, 0.5F);
      GL11.glRotatef(-2.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.24F, -0.27F, 0.25F);
      double var3 = 0.33D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(3.6F, 0.35F, 0.95F);
      double var3 = 1.0D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-0.4F, 1.0F, 0.0F, 0.0F);
      GL11.glTranslatef(-0.6F, 0.0F, 0.0F);
      GL11.glTranslatef(0.0F, 0.033F, 0.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 1.3D;
      GL11.glScaled(var1, var1, var1);
      var1 = 0.48D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(2.9F, -0.6F, -0.1F);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/g18modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.5F, -0.4F, 0.025F);
      double var1 = 1.0D;
      GL11.glScaled(var1, var1 + 0.5D, var1);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.4D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.29D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.3F, 0.7F, 0.5F);
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.l) {
         GL11.glTranslatef(16.0F, -0.4F, 2.0F);
         var4 = 1.5D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.e) {
         GL11.glTranslatef(-23.0F, -3.6F, -0.4F);
         var4 = 1.7D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.f) {
         GL11.glDisable(2884);
         GL11.glTranslatef(-16.0F, -3.6F, 0.3F);
         var4 = 1.2D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.02F, -0.1F, -0.2F);
      } else {
         GL11.glRotated(9.0D, 1.0D, 0.0D, 0.0D);
         GL11.glTranslatef(0.04F, 0.18F, -0.15F);
      }

   }

   public void c() {
      GL11.glRotatef(-40.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.7F, 0.0F, 1.8F);
   }

   protected ModelBase a() {
      this.i = new ModelPistolIS1();
      this.j = new ModelPistolIS2();
      return new ModelMP5A5();
   }

   protected String b() {
      return "mp5a5model";
   }
}
