package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelScarhIS2;
import com.craftingdead.client.model.gun.ModelTrenchGun;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTrenchGun extends RenderGun {

   private ModelBase i;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.75F, 0.4F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.2F, 0.55F, 0.0F);
      double var3 = 0.7D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.1F;
      super.e = 0.1F;
      super.f = -1.8F;
      super.g = 1.2F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.7F, -0.2F, 0.15F);
      double var3 = 0.5D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-24.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.1F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.0F, -0.69F, 0.972F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.017F, 0.0F);
      }

      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-0.7F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(0.0F, 0.0F, 0.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.7D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.45D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.7F, 0.18F, 0.4F);
   }

   private void f() {}

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(1.6F, -0.11F, 0.0315F);
      double var1 = 0.49D;
      GL11.glScaled(var1, var1, var1);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/scarhmodelis2.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      if(var3 == GunAttachment.j) {
         GL11.glTranslated(7.0D, 3.3D, 1.0D);
         double var4 = 1.0D;
         GL11.glScaled(var4, var4, var4);
      }
   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.01F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.6F, 0.0F, 0.6F);
   }

   protected ModelBase a() {
      this.i = new ModelScarhIS2();
      return new ModelTrenchGun();
   }

   protected String b() {
      return "trenchgunmodel";
   }
}
