package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelM1911;
import com.craftingdead.client.model.gun.ModelPistolIS1;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderM1911 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.05F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(78.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.0F, -0.28F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      double var3 = 0.75D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.5F;
      super.e = -0.2F;
      super.f = -1.85F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.3F, 0.35F);
      double var3 = 0.7D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-25.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.1F, -0.701F, 0.972F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.03F, 0.0F);
      }

      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-0.2F, -0.1F, -0.25F);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.75F, -0.04F, 0.07F);
      double var1 = 0.25D;
      GL11.glScaled(var1, var1, var1);
      GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.7D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.45D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(1.7F, -0.3F, -0.7F);
   }

   protected void d(Entity var1, ItemStack var2) {
      if(var2.itemID == ItemManager.ab.itemID) {
         GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(-8.0F, 0.0F, 0.0F, 1.0F);
         GL11.glTranslatef(-1.8F, 1.0F, -1.25F);
         double var3 = 1.0D;
         GL11.glScaled(var3, var3, var3);
      }

   }

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.l) {
         GL11.glTranslatef(14.3F, 0.0F, 1.25F);
         var4 = 1.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.e) {
         GL11.glTranslated(0.1D, -0.5D, 0.155D);
         var4 = 0.75D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      GL11.glTranslatef(0.02F, 0.04F, -0.12F);
      if(var3) {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      } else {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      }

   }

   protected ModelBase a() {
      this.i = new ModelPistolIS1();
      this.j = new ModelPistolIS2();
      return new ModelM1911();
   }

   protected String b() {
      return "m1911model";
   }
}
