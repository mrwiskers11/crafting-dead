package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelAS50;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.model.gun.ModelScarhIS1;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderAS50 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.75F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.4F, 0.55F, 0.0F);
      double var3 = 1.2D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.3F;
      super.e = -0.1F;
      super.f = -1.8F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.3F, -0.2F, 0.15F);
      double var3 = 0.9D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-24.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.1F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.1F, -0.676F, 0.9705F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.017F, 0.0F);
      }

      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-0.7F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 1.25D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.75D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.7F, 0.18F, 0.25F);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(1.4F, -0.53F, -0.083F);
      var1 = 0.75D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-2.0F, 0.6F, -0.12F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/scarhmodelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(1.05F, -0.09F, 0.057F);
      double var1 = 0.25D;
      GL11.glScaled(var1, var1 + 0.5D, var1);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.h) {
         GL11.glTranslated(2.0D, -1.5D, 0.26D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      } else if(var3 == GunAttachment.i) {
         GL11.glTranslated(2.0D, -1.5D, 0.26D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      } else if(var3 == GunAttachment.e) {
         GL11.glTranslated(0.4D, -0.77D, 0.26D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      } else if(var3 == GunAttachment.f) {
         GL11.glDisable(2884);
         GL11.glTranslated(1.5D, -0.78D, 0.51D);
         var4 = 0.4D;
         GL11.glScaled(var4, var4, var4);
      } else if(var3 == GunAttachment.j) {
         GL11.glTranslated(9.0D, 1.4D, 0.88D);
         var4 = 0.55D;
         GL11.glScaled(var4, var4, var4);
      } else if(var3 == GunAttachment.k) {
         GL11.glTranslated(9.0D, 1.5D, 0.2D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      }
   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.01F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, 0.0F, -0.1F);
   }

   protected ModelBase a() {
      this.i = new ModelScarhIS1();
      this.j = new ModelPistolIS2();
      return new ModelAS50();
   }

   protected String b() {
      return "as50model";
   }
}
