package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelScarh;
import com.craftingdead.client.model.gun.ModelScarhIS1;
import com.craftingdead.client.model.gun.ModelScarhIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderScarh extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.7F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.45F, 0.5F, 0.0F);
      double var3 = 1.2D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.1F;
      super.e = 0.0F;
      super.f = -1.8F;
      super.g = 1.2F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.3F, -0.3F, 0.4F);
      double var3 = 0.8D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-2.0F, 1.0F, 0.0F, 0.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.0F, -0.22F, 0.94F);
      double var3 = 1.0D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-1.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(0.18F, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(1.2F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.8F, -0.17F, 0.01F);
      GL11.glTranslatef(0.0F, 0.0F, 0.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 1.2D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.72D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.7F, 0.2F, 0.2F);
   }

   protected void d(Entity var1, ItemStack var2) {
      double var3;
      if(var2.itemID == ItemManager.R.itemID) {
         GL11.glTranslated(3.0D, 1.5D, 0.05D);
         var3 = 0.9D;
         GL11.glScaled(var3, var3, var3);
      }

      if(var2.itemID == ItemManager.S.itemID) {
         GL11.glTranslated(3.0D, 2.5D, 0.05D);
         var3 = 0.9D;
         GL11.glScaled(var3, var3, var3);
      }

      if(var2.itemID == ItemManager.T.itemID) {
         GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
         GL11.glTranslated(-1.24D, 2.4D, 2.7D);
         var3 = 0.65D;
         GL11.glScaled(var3, var3, var3);
         GL11.glRotated(5.0D, 1.0D, 0.0D, 0.0D);
      }

      if(var2.itemID == ItemManager.U.itemID) {
         GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
         GL11.glTranslated(-1.8D, 3.2D, 3.0D);
         var3 = 0.95D;
         GL11.glScaled(var3, var3, var3);
      }

   }

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.h) {
         GL11.glTranslated(1.0D, -1.8D, 0.25D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.i) {
         GL11.glTranslated(1.0D, -1.8D, 0.25D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.e) {
         GL11.glTranslated(0.5D, -1.02D, 0.26D);
         var4 = 0.6D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.f) {
         GL11.glDisable(2884);
         GL11.glTranslated(1.5D, -1.035D, 0.508D);
         var4 = 0.4D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.l) {
         GL11.glTranslated(18.3D, 0.1D, 1.15D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.j) {
         GL11.glTranslated(9.0D, 1.3D, 1.05D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.k) {
         GL11.glTranslated(8.0D, 1.3D, 0.15D);
         var4 = 0.8D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.g) {
         GL11.glTranslated(2.0D, -1.8D, 0.65D);
         var4 = 0.1D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.03F, 0.15F, -0.05F);
      }

   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-0.2F, -0.15F, -0.187F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/scarhmodelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(1.1255F, -0.19F, 0.0318F);
      double var1 = 0.495D;
      GL11.glScaled(var1, var1, var1);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/scarhmodelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.7F, 0.0F, 0.2F);
   }

   protected ModelBase a() {
      this.i = new ModelScarhIS1();
      this.j = new ModelScarhIS2();
      return new ModelScarh();
   }

   protected String b() {
      return "scarhmodel";
   }
}
