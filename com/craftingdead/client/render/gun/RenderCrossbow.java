package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelCrossbow;
import com.craftingdead.client.model.gun.ModelScarhIS1;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderCrossbow extends RenderGun {

   private ModelBase i;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(1.0F, -0.6F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.4F, 0.6F, 0.0F);
      double var3 = 0.5D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.4F, -0.2F, 0.15F);
      double var3 = 0.4D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-24.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.1F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.66F, 0.92F);
      double var3 = 0.5D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-0.7F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.45D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(220.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.35D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.6F, 0.2F, -0.9F);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.9D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(3.0F, -0.525F, -0.2F);
      var1 = 0.75D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-2.5F, 0.6F, -0.061F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/scarhmodelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      if(var3 == GunAttachment.f) {
         GL11.glTranslated(-11.0D, -1.1D, 1.632D);
         double var4 = 0.5D;
         GL11.glScaled(var4, var4, var4);
      }
   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.01F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.7F, 0.0F, 0.2F);
   }

   protected ModelBase a() {
      this.i = new ModelScarhIS1();
      return new ModelCrossbow();
   }

   protected String b() {
      return "crossbowmodel";
   }
}
