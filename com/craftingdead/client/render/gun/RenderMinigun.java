package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelMinigun;
import com.craftingdead.client.model.gun.ModelMinigunBarrel;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderMinigun extends RenderGun {

   private ModelBase i;
   private float j = 0.0F;
   private float k = 0.0F;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.75F, 0.5F);
      GL11.glRotatef(-55.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.8F, -0.1F, 0.0F);
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.3F, 0.3F, 0.2F);
      double var3 = 1.4D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.2F;
      super.e = -0.43F;
      super.f = -1.8F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.1F, 0.5F, -0.1F);
      double var3 = 1.3D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-24.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.1F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.1F, -0.665F, 0.952F);
      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-0.7F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      this.e(var1, var2);
   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(0.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 1.5D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glTranslatef(0.0F, -0.2F, 0.0F);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.8D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-0.8F, 0.2F, 0.4F);
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {}

   public void e(Entity var1, ItemStack var2) {
      ItemGun var3 = (ItemGun)var2.getItem();
      this.j = var3.n(var2);
      this.k = var3.o(var2);
      GL11.glPushMatrix();
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/minigunmodel.png"));
      float var4 = 0.155F;
      float var5 = 0.035F;
      GL11.glTranslated(0.0D, (double)var4, (double)(-var5));
      float var6 = this.k + (this.j - this.k) * super.h;
      GL11.glRotated((double)var6, 1.0D, 0.0D, 0.0D);
      GL11.glTranslated(0.0D, (double)(-var4), (double)var5);
      this.i.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.01F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.3F, -0.3F, 0.2F);
   }

   protected ModelBase a() {
      this.i = new ModelMinigunBarrel();
      return new ModelMinigun();
   }

   protected String b() {
      return "minigunmodel";
   }
}
