package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelMK48;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.model.gun.ModelScarhIS1;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderMK48Mod1 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(77.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.6F, 0.4F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.4F, 0.55F, 0.0F);
      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.5F;
      super.e = -0.08F;
      super.f = -1.85F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-40.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-3.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.6F, -0.3F, 0.3F);
      double var3 = 0.28D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(3.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-24.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.1F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.1F, -0.61F, 1.0F);
      double var3 = 0.2D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(-0.7F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.36D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-1.0F, 0.7F, 0.7F);
   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 1.4D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(2.0F, -0.76F, 0.01F);
      var1 = 0.7D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-2.4F, 0.6F, -0.13F);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/scarhmodelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(1.63F, -0.6F, 0.04F);
      double var1 = 0.8D;
      GL11.glScaled(var1, var1 + 0.5D, var1);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.e) {
         GL11.glTranslated(-9.0D, -4.7D, -0.7D);
         var4 = 2.0D;
         GL11.glScaled(var4, var4, var4);
      } else if(var3 == GunAttachment.k) {
         GL11.glTranslated(8.0D, 2.2D, 0.3D);
         var4 = 2.0D;
         GL11.glScaled(var4, var4, var4);
      } else {
         if(var3 == GunAttachment.l) {
            GL11.glTranslated(33.0D, -2.0D, 1.3D);
            var4 = 1.8D;
            GL11.glScaled(var4, var4, var4);
         }

         if(var3 == GunAttachment.j) {
            GL11.glTranslated(12.0D, 2.19D, 1.5D);
            var4 = 2.0D;
            GL11.glScaled(var4, var4, var4);
         }

         if(var3 == GunAttachment.g) {
            GL11.glTranslated(-4.0D, -7.0D, 0.65D);
            var4 = 0.3D;
            GL11.glScaled(var4, var4, var4);
         }

      }
   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.1F, -0.15F, -0.3F);
      } else {
         GL11.glTranslatef(0.09F, 0.15F, -0.1F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(2.0F, 0.0F, 1.0F);
   }

   protected ModelBase a() {
      this.i = new ModelScarhIS1();
      this.j = new ModelPistolIS2();
      return new ModelMK48();
   }

   protected String b() {
      return "mk48mod1model";
   }
}
