package com.craftingdead.client.render.gun;

import com.craftingdead.client.ClientTickHandler;
import com.craftingdead.client.animation.AnimationManager;
import com.craftingdead.client.animation.GunAnimation;
import com.craftingdead.client.animation.gun.GunAnimationReload;
import com.craftingdead.client.model.gun.ModelMuzzleFlash;
import com.craftingdead.client.render.gun.IRenderPartialTick;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.GunPaint;
import com.craftingdead.item.gun.ItemAmmo;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.player.PlayerDataHandler;

import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.IItemRenderer.ItemRendererHelper;
import org.lwjgl.opengl.GL11;

public abstract class RenderGun implements IRenderPartialTick, IItemRenderer {

   public Minecraft a = Minecraft.getMinecraft();
   public ModelBase b = null;
   public ModelBase c;
   public float d = 0.4F;
   public float e = 0.2F;
   public float f = -1.8F;
   public float g = 2.0F;
   public float h;


   public boolean handleRenderType(ItemStack var1, ItemRenderType var2) {
      return var2 == ItemRenderType.EQUIPPED || var2 == ItemRenderType.ENTITY || var2 == ItemRenderType.EQUIPPED_FIRST_PERSON;
   }

   public boolean shouldUseRenderHelper(ItemRenderType var1, ItemStack var2, ItemRendererHelper var3) {
      return false;
   }

   public void a(float var1) {
      this.h = var1;
   }

   public void renderItem(ItemRenderType var1, ItemStack var2, Object ... var3) {
      this.f();
      this.c = new ModelMuzzleFlash();
      if(var2 != null) {
         Entity var4 = (Entity)var3[1];
         GL11.glPushMatrix();
         String var5 = this.a(var2);
         GunPaint var6 = ItemGun.H().F(var2);
         if(var1 == ItemRenderType.EQUIPPED_FIRST_PERSON) {
            if(PlayerDataHandler.b(((EntityPlayer)var4).username).c && ItemGun.H().d(var2, 0) != null && ItemGun.H().d(var2, 0).u != null) {
               GL11.glPopMatrix();
               return;
            }

            if(!PlayerDataHandler.b(((EntityPlayer)var4).username).c && ClientTickHandler.i == 3.0D) {
               OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240.0F, 240.0F);
               GL11.glEnable(3042);
            }

            if(!PlayerDataHandler.b(((EntityPlayer)var4).username).c) {
               Entity var7 = (Entity)var3[1];
               EntityClientPlayerMP var8 = this.a.thePlayer;
               GunAnimation var9;
               if(var1 == ItemRenderType.EQUIPPED_FIRST_PERSON && !var8.isSprinting()) {
                  GL11.glPushMatrix();
                  var9 = AnimationManager.d().c();
                  if(var9 != null) {
                     var9.a(var2, this.h, true);
                  }

                  GL11.glEnable('\u803a');
                  this.a.getTextureManager().bindTexture(var8.getLocationSkin());
                  GL11.glTranslatef(1.0F, 1.0F, 0.0F);
                  GL11.glRotatef(125.0F, 0.0F, 0.0F, 1.0F);
                  GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
                  GL11.glScalef(1.0F, 1.0F, 1.0F);
                  GL11.glTranslatef(0.19F, -1.4F, 0.5F);
                  this.a((EntityPlayer)var7, var2, true);
                  Render var10 = RenderManager.instance.getEntityRenderObject(this.a.thePlayer);
                  RenderPlayer var11 = (RenderPlayer)var10;
                  float var12 = 1.0F;
                  GL11.glScalef(var12, var12, var12);
                  var11.renderFirstPersonArm(this.a.thePlayer);
                  GL11.glPopMatrix();
                  GL11.glPushMatrix();
                  if(var9 != null) {
                     var9.a(var2, this.h, false);
                  }

                  GL11.glEnable('\u803a');
                  this.a.getTextureManager().bindTexture(var8.getLocationSkin());
                  GL11.glTranslatef(1.05F, 1.0F, 0.0F);
                  GL11.glRotatef(120.0F, 0.0F, 0.0F, 1.0F);
                  GL11.glRotatef(130.0F, 1.0F, 0.0F, 0.0F);
                  GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
                  GL11.glScalef(1.0F, 1.0F, 1.0F);
                  GL11.glTranslatef(0.1F, -1.6F, -0.12F);
                  this.a((EntityPlayer)var7, var2, false);
                  GL11.glScalef(var12, var12 + 0.2F, var12);
                  var11.renderFirstPersonArm(this.a.thePlayer);
                  GL11.glPopMatrix();
               }

               this.a((EntityPlayer)var4, var2);
               if(var8.isSprinting()) {
                  this.c();
               }

               var9 = AnimationManager.d().c();
               if(var9 != null) {
                  var9.a(var2, this.h);
               }

               if(var6 != null && var6 == GunPaint.k) {
                  int[] var14 = ItemGun.H().G(var2);
                  GL11.glColor4f((float)var14[0] / 255.0F, (float)var14[1] / 255.0F, (float)var14[2] / 255.0F, 1.0F);
               }

               this.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/guns/" + var5 + ".png"));
               this.b.render(var4, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
               this.b(var4, var2);
               GL11.glPushMatrix();
               if(var9 != null && var9 instanceof GunAnimationReload) {
                  ((GunAnimationReload)var9).b(var2, this.h);
               }

               this.f(var4, var2);
               GL11.glPopMatrix();
               this.e(var4, var2);
               GL11.glPopMatrix();
               Random var15 = new Random();
               if(ClientTickHandler.i == 3.0D) {
                  GL11.glPushMatrix();
                  GL11.glPushMatrix();
                  GL11.glPushMatrix();
                  GL11.glRotated(-85.0D, -0.08D, 1.0D, 0.0D);
                  GL11.glRotated(30.0D, 1.0D, 0.0D, 0.0D);
                  GL11.glScaled((double)this.g, (double)this.g, (double)this.g);
                  if(ItemGun.H().d(var2, 2) != null) {
                     this.f = (float)((double)this.f - 0.4D);
                  }

                  GL11.glTranslated((double)this.d, (double)this.e, (double)this.f);
                  GL11.glEnable('\u803a');
                  GL11.glDisable(3008);
                  GL11.glDisable(2896);
                  GL11.glEnable(3042);
                  GL11.glDepthMask(false);
                  GL11.glColor4f(222.0F, 666.0F, 444.0F, 111.0F);
                  this.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead", "textures/models/guns/flashes/flash" + Integer.toString(var15.nextInt(3) + 1) + ".png"));
                  this.c.render(this.a.thePlayer, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
                  GL11.glPopMatrix();
                  GL11.glPopMatrix();
                  GL11.glPopMatrix();
               }

               return;
            }

            this.b((EntityPlayer)var4, var2);
         } else if(var1 == ItemRenderType.EQUIPPED) {
            this.a(var4, var2);
         } else if(var1 == ItemRenderType.ENTITY) {
            this.c(var4, var2);
         }

         if(var6 != null && var6 == GunPaint.k) {
            int[] var13 = ItemGun.H().G(var2);
            GL11.glColor4f((float)var13[0] / 255.0F, (float)var13[1] / 255.0F, (float)var13[2] / 255.0F, 1.0F);
         }

         this.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/" + var5 + ".png"));
         this.b.render(var4, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         this.b(var4, var2);
         this.f(var4, var2);
         this.e(var4, var2);
         GL11.glPopMatrix();
      }

   }

   public void a(Entity var1, ItemStack var2, double var3, double var5, double var7) {
      this.f();
      GL11.glPushMatrix();
      String var9 = this.a(var2);
      GunPaint var10 = ItemGun.H().F(var2);
      if(var10 != null && var10 == GunPaint.k) {
         int[] var11 = ItemGun.H().G(var2);
         GL11.glColor4f((float)var11[0] / 255.0F, (float)var11[1] / 255.0F, (float)var11[2] / 255.0F, 1.0F);
      }

      this.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/" + var9 + ".png"));
      GL11.glTranslated(var3, var5 + 0.1D, var7);
      double var13 = 0.4D;
      GL11.glScaled(var13, var13, var13);
      GL11.glRotatef(0.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(((EntityGroundItem)var1).a, 0.0F, 1.0F, 0.0F);
      this.c(var1, var2);
      this.b.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      this.b(var1, var2);
      this.f(var1, var2);
      this.e(var1, var2);
      GL11.glDisable(2884);
      GL11.glPopMatrix();
   }

   public void d(EntityPlayer var1, ItemStack var2) {
      this.f();
      GL11.glPushMatrix();
      String var3 = this.a(var2);
      GunPaint var4 = ItemGun.H().F(var2);
      if(var4 != null && var4 == GunPaint.k) {
         int[] var5 = ItemGun.H().G(var2);
         GL11.glColor4f((float)var5[0] / 255.0F, (float)var5[1] / 255.0F, (float)var5[2] / 255.0F, 1.0F);
      }

      this.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/" + var3 + ".png"));
      this.c(var1, var2);
      this.b.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      this.b((Entity)var1, var2);
      this.f(var1, var2);
      this.e(var1, var2);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glDisable(2884);
      GL11.glPopMatrix();
   }

   private void e(Entity var1, ItemStack var2) {
      if(ItemGun.H().E(var2)) {
         for(int var3 = 0; var3 < 3; ++var3) {
            GunAttachment var4 = ItemGun.H().d(var2, var3);
            if(var4 != null) {
               GL11.glPushMatrix();
               this.a.getTextureManager().bindTexture(var4.s);
               double var5 = 0.1D;
               GL11.glScaled(var5, var5, var5);
               this.a(var1, var2, var4);
               var4.r.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.625F);
               GL11.glEnable(2884);
               GL11.glPopMatrix();
            }
         }
      }

   }

   private void f(Entity var1, ItemStack var2) {
      if(ItemGun.H().w(var2) != null) {
         String var3 = this.a(var2);
         GL11.glPushMatrix();
         ItemAmmo var4 = (ItemAmmo)ItemGun.H().w(var2).getItem();
         if(var4.cB != null) {
            String var5 = var4.cC.equals("guntexture")?var3:var4.cC;
            if(var3.equals("ak47model_vulcan")) {
               var5 = "ak47model_vulcan";
            }

            this.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/" + var5 + ".png"));
            double var6 = 0.1D;
            GL11.glScaled(var6, var6, var6);
            this.d(var1, ItemGun.H().w(var2));
            var4.cB.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.625F);
         }

         GL11.glPopMatrix();
      }

   }

   private void f() {
      if(this.b == null) {
         this.b = this.a();
      }

   }

   private String a(ItemStack var1) {
      GunPaint var2 = ItemGun.H().F(var1);
      return var2 != null?(var2 == GunPaint.k?this.b():this.b() + "_" + var2.a()):this.b();
   }

   public float d() {
      return 0.07F;
   }

   public float e() {
      return 0.03F;
   }

   protected abstract void a(Entity var1, ItemStack var2);

   protected abstract void a(EntityPlayer var1, ItemStack var2);

   protected abstract void b(EntityPlayer var1, ItemStack var2);

   protected abstract void b(Entity var1, ItemStack var2);

   protected abstract void c(Entity var1, ItemStack var2);

   protected abstract void c(EntityPlayer var1, ItemStack var2);

   protected abstract void d(Entity var1, ItemStack var2);

   protected abstract void a(Entity var1, ItemStack var2, GunAttachment var3);

   protected abstract ModelBase a();

   protected abstract String b();

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      } else {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.7F, 0.0F, 0.2F);
   }
}
