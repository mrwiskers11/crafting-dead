package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelFiveSeven;
import com.craftingdead.client.model.gun.ModelPistolIS1;
import com.craftingdead.client.model.gun.ModelPistolIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderFiveSeven extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.05F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(78.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.4F, -0.22F, 0.45F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      double var3 = 0.3D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.5F;
      super.e = -0.3F;
      super.f = -1.7F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.9F, -0.27F, 0.4F);
      double var3 = 0.2D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-25.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.1F, -0.7F, 1.003F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.03F, 0.0F);
      }

      double var3 = 0.1D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 1.6D;
      GL11.glScaled(var1, var1, var1);
      var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(2.0F, -0.25F, -0.09F);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.7F, -0.25F, 0.099F);
      double var1 = 0.9D;
      GL11.glScaled(var1, var1, var1);
      GL11.glRotated(90.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.25D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.15D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(6.0F, -1.0F, -2.0F);
   }

   protected void d(Entity var1, ItemStack var2) {
      if(var2.itemID == ItemManager.aE.itemID) {
         GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(-8.0F, 0.0F, 0.0F, 1.0F);
         GL11.glTranslatef(24.0F, 1.0F, -2.0F);
         double var3 = 0.9D;
         GL11.glScaled(var3, var3, var3);
      }

   }

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      if(var3 == GunAttachment.l) {
         GL11.glTranslatef(19.0F, -0.3F, 1.8F);
         double var4 = 1.7D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      GL11.glTranslatef(0.0F, 0.02F, -0.12F);
      if(var3) {
         GL11.glTranslatef(0.0F, 0.0F, 0.0F);
      } else {
         GL11.glTranslatef(0.0F, 0.04F, 0.0F);
      }

   }

   protected ModelBase a() {
      this.i = new ModelPistolIS1();
      this.j = new ModelPistolIS2();
      return new ModelFiveSeven();
   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(3.0F, 0.0F, 2.0F);
   }

   protected String b() {
      return "fivesevenmodel";
   }
}
