package com.craftingdead.client.render.gun;

import com.craftingdead.client.model.gun.ModelP90;
import com.craftingdead.client.model.gun.ModelPistolIS1;
import com.craftingdead.client.model.gun.ModelScarhIS2;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderP90 extends RenderGun {

   private ModelBase i;
   private ModelBase j;


   protected void a(Entity var1, ItemStack var2) {
      GL11.glTranslatef(0.0F, 0.0F, -0.05F);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-15.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(78.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.0F, -0.28F, 0.35F);
      GL11.glRotatef(15.0F, 0.0F, 0.0F, 1.0F);
      double var3 = 0.75D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void a(EntityPlayer var1, ItemStack var2) {
      super.d = 0.5F;
      super.e = -0.2F;
      super.f = -1.95F;
      super.g = 2.0F;
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-35.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(-2.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.5F, -0.25F, 0.1F);
      double var3 = 0.9D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void b(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-25.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(5.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(-0.1F, -0.58F, 0.9525F);
      if(ItemGun.H().d(var2, 0) != null) {
         GL11.glTranslatef(0.0F, 0.03F, 0.0F);
      }

      double var3 = 0.6D;
      GL11.glScaled(var3, var3, var3);
      GL11.glRotatef(0.0F, 0.0F, 0.0F, 1.0F);
   }

   protected void b(Entity var1, ItemStack var2) {
      if(ItemGun.H().d(var2, 0) == null) {
         this.f();
         this.g();
      }

   }

   private void f() {
      GL11.glPushMatrix();
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1 + 0.2D, var1);
      var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glTranslatef(-2.2F, -0.65F, -0.38F);
      GL11.glRotated(180.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/m1911modelis1.png"));
      this.i.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   private void g() {
      GL11.glPushMatrix();
      GL11.glTranslatef(0.85F, -0.28F, 0.064F);
      double var1 = 0.5D;
      GL11.glScaled(var1, var1, var1);
      GL11.glRotated(0.0D, 0.0D, 1.0D, 0.0D);
      super.a.getTextureManager().bindTexture(new ResourceLocation("craftingdead:textures/models/guns/p90modelis2.png"));
      this.j.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   protected void c(Entity var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      double var3 = 0.9D;
      GL11.glScaled(var3, var3, var3);
   }

   protected void c(EntityPlayer var1, ItemStack var2) {
      GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
      double var3 = 0.45D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslatef(-1.0F, 0.4F, 0.4F);
   }

   protected void d(Entity var1, ItemStack var2) {}

   protected void a(Entity var1, ItemStack var2, GunAttachment var3) {
      double var4;
      if(var3 == GunAttachment.l) {
         GL11.glTranslatef(16.3F, 0.0F, 1.6F);
         var4 = 1.0D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.e) {
         GL11.glTranslated(4.25D, -2.5D, 0.482D);
         var4 = 0.75D;
         GL11.glScaled(var4, var4, var4);
      }

      if(var3 == GunAttachment.f) {
         GL11.glDisable(2884);
         GL11.glTranslated(6.0D, -2.45D, 0.78D);
         var4 = 0.55D;
         GL11.glScaled(var4, var4, var4);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, boolean var3) {
      if(var3) {
         GL11.glTranslatef(-0.01F, -0.0F, -0.3F);
      } else {
         GL11.glRotated(15.0D, 1.0D, 0.0D, 0.0D);
         GL11.glTranslatef(0.05F, 0.15F, -0.2F);
      }

   }

   public void c() {
      GL11.glRotatef(-70.0F, 0.0F, 1.0F, 0.0F);
      GL11.glTranslatef(0.7F, 0.0F, 0.2F);
   }

   protected ModelBase a() {
      this.i = new ModelPistolIS1();
      this.j = new ModelScarhIS2();
      return new ModelP90();
   }

   protected String b() {
      return "p90model";
   }
}
