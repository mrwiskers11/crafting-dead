package com.craftingdead.client.render;

import com.craftingdead.entity.EntityCDZombie;

import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderCDZombie extends RenderBiped {

   public static ResourceLocation k;


   public RenderCDZombie(ModelBiped var1, float var2) {
      super(var1, var2);
   }

   protected ResourceLocation getEntityTexture(Entity var1) {
      if(var1 instanceof EntityCDZombie) {
         k = new ResourceLocation("craftingdead:textures/mobs/zombie" + ((EntityCDZombie)var1).b() + ".png");
      }

      return k;
   }
}
