package com.craftingdead.client.render;

import com.craftingdead.CraftingDead;
import com.craftingdead.entity.EntityCorpse;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderCorpse extends Render {

   private ModelBiped a = new ModelBiped();


   public void a(EntityCorpse var1, double var2, double var4, double var6, float var8, float var9) {
      String var10 = var1.h();
      ResourceLocation var11 = AbstractClientPlayer.locationStevePng;
      if(CraftingDead.l().e().c && var10 != null && var10.length() > 0) {
         var11 = AbstractClientPlayer.getLocationSkin(var10);
         AbstractClientPlayer.getDownloadImageSkin(var11, var10);
      }

      this.bindTexture(var11);
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2, (float)var4 + 0.2F, (float)var6 - 0.4F);
      double var12 = 0.1D;
      GL11.glScaled(var12, var12, var12);
      GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
      float var14 = 0.625F;
      int var15 = var1.i();
      this.a.setRotationAngles(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, var1);
      this.a.bipedHead.render(var14);
      this.a.bipedBody.render(var14);
      if(var15 < 4) {
         this.a.bipedRightArm.render(var14);
      }

      if(var15 < 3) {
         this.a.bipedLeftArm.render(var14);
      }

      if(var15 < 2) {
         this.a.bipedRightLeg.render(var14);
      }

      if(var15 < 1) {
         this.a.bipedLeftLeg.render(var14);
      }

      this.a.bipedHeadwear.render(var14);
      GL11.glPopMatrix();
   }

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.a((EntityCorpse)var1, var2, var4, var6, var8, var9);
   }

   protected ResourceLocation getEntityTexture(Entity var1) {
      return new ResourceLocation("craftingdead:textures/models/defaultcorpse.png");
   }
}
