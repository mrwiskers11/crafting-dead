package com.craftingdead.client.render;

import com.craftingdead.client.model.ModelSupplyDrop;
import com.craftingdead.entity.EntitySupplyDrop;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderSupplyDrop extends Render {

   private ModelSupplyDrop a = new ModelSupplyDrop();


   public void a(EntitySupplyDrop var1, double var2, double var4, double var6, float var8, float var9) {
      super.renderManager.renderEngine.bindTexture(this.getEntityTexture(var1));
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2, (float)var4 + 1.5F, (float)var6);
      double var10 = 0.1D;
      GL11.glScaled(var10, var10, var10);
      GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
      this.a.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.625F);
      GL11.glPopMatrix();
   }

   public void doRender(Entity var1, double var2, double var4, double var6, float var8, float var9) {
      this.a((EntitySupplyDrop)var1, var2, var4, var6, var8, var9);
   }

   protected ResourceLocation getEntityTexture(Entity var1) {
      return new ResourceLocation("craftingdead:textures/models/supplydrop.png");
   }
}
