package com.craftingdead.client.render;

import com.craftingdead.client.render.RenderBackpack;
import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

public class RenderBackpackSmall extends RenderBackpack {

   public RenderBackpackSmall(String var1, ModelBase var2) {
      super(var1, var2);
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      double var3 = 1.0D;
      GL11.glScaled(var3, var3, var3);
      GL11.glTranslated(-0.03D, 0.22D, 0.3D);
      GL11.glRotated(180.0D, 1.0D, 0.0D, 0.0D);
      GL11.glRotated(180.0D, 0.0D, 0.0D, 1.0D);
      super.a.render(var1, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
   }
}
