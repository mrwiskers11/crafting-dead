package com.craftingdead.client.render.tileentity;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.tileentity.TileEntityLoot;
import com.craftingdead.client.model.ModelLoot;
import com.craftingdead.client.model.ModelLootPresent;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTileEntityLoot extends TileEntitySpecialRenderer {

   private ModelLoot a = new ModelLoot();
   private ModelLootPresent c = new ModelLootPresent();


   public void a(TileEntityLoot var1, double var2, double var4, double var6, float var8) {
      if(CraftingDead.l().f().d()) {
         this.bindTexture(new ResourceLocation("craftingdead", "textures/models/loot/presents/" + var1.b() + ".png"));
         GL11.glPushMatrix();
         GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
         GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
         GL11.glScalef(1.0F, -1.0F, -1.0F);
         this.c.a(0.0625F);
         GL11.glPopMatrix();
      } else if(var1.a() != null && !var1.a && !CraftingDead.l().e().g) {
         this.a(var1, var2, var4, var6, var8, var1.a());
      } else {
         this.bindTexture(new ResourceLocation("craftingdead:textures/models/loot/" + var1.b() + ".png"));
         GL11.glPushMatrix();
         GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
         GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
         GL11.glScalef(1.0F, -1.0F, -1.0F);
         this.a.a(0.0625F);
         GL11.glPopMatrix();
      }

   }

   public void a(TileEntityLoot var1, double var2, double var4, double var6, float var8, ItemStack var9) {
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2, (float)var4 + 0.05F, (float)var6);
      double var10 = 1.0D;
      GL11.glScaled(var10, var10, var10);
      GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
      this.a(var9, 0.0F, 0.0F, 0.0F);
      GL11.glPopMatrix();
   }

   private void a(ItemStack var1, float var2, float var3, float var4) {
      Tessellator var5 = Tessellator.instance;
      Object var6 = var1.getItem().getIcon(var1, 0);
      if(var6 == null) {
         TextureManager var7 = Minecraft.getMinecraft().getTextureManager();
         ResourceLocation var8 = var7.getResourceLocation(var1.getItemSpriteNumber());
         var6 = ((TextureMap)var7.getTexture(var8)).getAtlasSprite("missingno");
      }

      float var14 = ((Icon)var6).getMinU();
      float var15 = ((Icon)var6).getMaxU();
      float var9 = ((Icon)var6).getMinV();
      float var10 = ((Icon)var6).getMaxV();
      if(var1 != null) {
         if(var1.getItemSpriteNumber() == 0) {
            this.bindTexture(TextureMap.locationBlocksTexture);
         } else {
            this.bindTexture(TextureMap.locationItemsTexture);
         }

         GL11.glPushMatrix();
         float var11 = 0.0625F;
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         double var12 = 0.6D;
         GL11.glScaled(var12, var12, var12);
         GL11.glTranslatef(0.3F, -1.3F, 0.0F);
         ItemRenderer.renderItemIn2D(var5, var15, var9, var14, var10, ((Icon)var6).getIconWidth(), ((Icon)var6).getIconHeight(), var11);
         GL11.glPopMatrix();
      }

   }

   public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) {
      this.a((TileEntityLoot)var1, var2, var4, var6, var8);
   }
}
