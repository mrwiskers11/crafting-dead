package com.craftingdead.client.render.tileentity;

import com.craftingdead.block.model.ModelMilitarySign;
import com.craftingdead.block.tileentity.TileEntityMilitarySign;
import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderTileEntitySign extends TileEntitySpecialRenderer {

   private ModelMilitarySign a = new ModelMilitarySign();


   public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) {
      TileEntityMilitarySign var9 = (TileEntityMilitarySign)var1;
      if(var9 != null) {
         int var10 = var9.a;
         String[] var11 = var9.c;
         this.bindTexture(new ResourceLocation("craftingdead:textures/models/blocks/sign_back.png"));
         GL11.glPushMatrix();
         GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
         if(var9.b) {
            GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
         }

         GL11.glRotatef((float)(var10 * -90 + 180), 0.0F, 1.0F, 0.0F);
         GL11.glScalef(1.0F, -1.0F, -1.0F);
         this.a.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
         GL11.glPopMatrix();
         if(!var9.b) {
            this.bindTexture(new ResourceLocation("craftingdead:textures/models/blocks/sign_color.png"));
            GL11.glPushMatrix();
            GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
            GL11.glRotatef((float)(var10 * -90 + 180), 0.0F, 1.0F, 0.0F);
            GL11.glScalef(1.0F, -1.0F, -1.0F);
            GL11.glDisable(2896);
            CDRenderHelper.b(var9.d);
            this.a.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
            GL11.glEnable(2896);
            GL11.glPopMatrix();
         }

         if(!var9.b && var11 != null && var11.length > 0) {
            GL11.glPushMatrix();
            FontRenderer var12 = this.getFontRenderer();
            float var13 = 0.016F;
            GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
            GL11.glNormal3f(0.0F, 1.0F, 0.0F);
            if(var10 == 0) {
               GL11.glTranslated(0.0D, 0.0D, -0.26D);
            }

            if(var10 == 1) {
               GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
               GL11.glTranslated(0.0D, 0.0D, -0.26D);
            }

            if(var10 == 2) {
               GL11.glRotatef(-180.0F, 0.0F, 1.0F, 0.0F);
               GL11.glTranslated(0.0D, 0.0D, -0.26D);
            }

            if(var10 == 3) {
               GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
               GL11.glTranslated(0.0D, 0.0D, -0.26D);
            }

            GL11.glScalef(-var13, -var13, var13);
            GL11.glDisable(2896);
            GL11.glEnable(3042);
            GL11.glBlendFunc(770, 771);
            byte var14 = 0;
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

            for(int var15 = 0; var15 < var11.length; ++var15) {
               String var16 = var11[var15];
               GL11.glDepthMask(false);
               var12.drawString(var16, -var12.getStringWidth(var16) / 2, var14 + var15 * 10, 553648127);
               GL11.glDepthMask(true);
               var12.drawString(var16, -var12.getStringWidth(var16) / 2, var14 + var15 * 10, -1);
            }

            GL11.glEnable(2896);
            GL11.glDisable(3042);
            GL11.glPopMatrix();
         }

      }
   }
}
