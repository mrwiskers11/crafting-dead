package com.craftingdead.client.render.tileentity;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import com.craftingdead.block.model.ModelGunRack;
import com.craftingdead.block.tileentity.TileEntityGunRack;

public class RenderTileEntityGunRack extends TileEntitySpecialRenderer {

   private ModelBase a = new ModelGunRack();


   public void a(TileEntityGunRack var1, double var2, double var4, double var6, float var8) {
      this.bindTexture(new ResourceLocation("craftingdead:textures/models/blocks/gunrack.png"));
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2 + 0.55F, (float)var4 + 1.35F, (float)var6 + 0.47F);
      GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
      if(var1.a == 0) {
         GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
      } else if(var1.a == 1) {
         GL11.glTranslatef(-0.08F, 0.0F, -0.025F);
      } else if(var1.a == 2) {
         GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
         GL11.glTranslatef(-0.11F, 0.0F, 0.05F);
      } else if(var1.a == 3) {
         GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
         GL11.glTranslatef(1.0F, 0.0F, 0.08F);
      }

      double var9;
      if(var1.b) {
         GL11.glTranslatef(-0.1F, -0.1F, 0.0F);
         GL11.glRotatef(180.0F, 0.0F, 2.0F, 0.0F);
         var9 = 3.0D;
         GL11.glScaled(var9, var9, var9);
      }

      GL11.glScalef(1.0F, -1.0F, -1.0F);
      var9 = 0.9D;
      GL11.glScaled(var9, var9, var9);
      this.a.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }

   public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) {
      this.a((TileEntityGunRack)var1, var2, var4, var6, var8);
   }
}
