package com.craftingdead.client.render.tileentity;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import com.craftingdead.block.model.ModelTrafficCone;
import com.craftingdead.block.tileentity.TileEntityTrafficCone;

public class RenderTileEntityTrafficCone extends TileEntitySpecialRenderer {

   private ModelTrafficCone a = new ModelTrafficCone();


   public void a(TileEntityTrafficCone var1, double var2, double var4, double var6, float var8) {
      this.bindTexture(new ResourceLocation("craftingdead:textures/models/blocks/trafficcone.png"));
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
      int var10 = var1.a;
      GL11.glRotatef((float)(var10 * -90 + 180), 0.0F, 1.0F, 0.0F);
      GL11.glScalef(1.0F, -1.0F, -1.0F);
      this.a.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
      if(var1.b) {
         GL11.glTranslatef(-0.1F, -0.75F, 0.0F);
         GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
         double var11 = 0.5D;
         GL11.glScaled(var11, var11, var11);
      }

   }

   public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) {
      this.a((TileEntityTrafficCone)var1, var2, var4, var6, var8);
   }
}
