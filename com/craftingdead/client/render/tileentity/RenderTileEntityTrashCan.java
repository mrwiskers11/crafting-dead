package com.craftingdead.client.render.tileentity;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import com.craftingdead.block.model.ModelTrash;
import com.craftingdead.block.tileentity.TileEntityTrashCan;

public class RenderTileEntityTrashCan extends TileEntitySpecialRenderer {

   private ModelBase a = new ModelTrash();


   public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) {
      this.bindTexture(new ResourceLocation("craftingdead:textures/models/blocks/trash.png"));
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var2 + 0.5F, (float)var4 + 1.5F, (float)var6 + 0.5F);
      TileEntityTrashCan var9 = (TileEntityTrashCan)var1;
      int var10 = var9.a;
      GL11.glRotatef((float)(var10 * -90 + 90), 0.0F, 1.0F, 0.0F);
      GL11.glScalef(1.0F, -1.0F, -1.0F);
      this.a.render((Entity)null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
      GL11.glPopMatrix();
   }
}
