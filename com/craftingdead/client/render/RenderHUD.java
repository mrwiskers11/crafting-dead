package com.craftingdead.client.render;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.item.IItemMovementPenalty;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.item.potions.PotionManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public class RenderHUD {

   public ArrayList a = new ArrayList();
   public int b = 100000;


   public void a(EntityPlayer var1) {
      if(this.b++ > 10) {
         this.b(var1);
         this.b = 0;
      }

   }

   public void b(EntityPlayer var1) {
      PlayerData var2 = PlayerDataHandler.b(var1.username);
      CDPlayer var3 = PlayerDataHandler.a(var1.username);
      this.a.clear();
      this.a.add(EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + "" + var1.username);
      if(FactionManager.c().b()) {
         this.a.add(EnumChatFormatting.BOLD + "Faction: " + FactionManager.c().a().c);
      }

      this.a.add("");
      this.a.add(EnumChatFormatting.ITALIC + "Karma " + var3.e);
      this.a.add(EnumChatFormatting.ITALIC + "" + var2.h + " Player(s) Killed");
      this.a.add(EnumChatFormatting.ITALIC + "" + var2.i + " Zombie(s) Killed");
      this.a.add(EnumChatFormatting.ITALIC + "" + var2.j + " Day(s) Survived");
      double var4 = 0.0D;
      ItemStack var6 = var2.d().a("gun");
      ItemStack var7 = var1.getCurrentEquippedItem();
      if(var6 != null) {
         ItemGun var8 = (ItemGun)var6.getItem();
         var4 += var8.c(var6) / 2.0D;
      }

      if(var7 != null && var7.getItem() instanceof IItemMovementPenalty) {
         IItemMovementPenalty var9 = (IItemMovementPenalty)var7.getItem();
         var4 += var9.c(var7);
      }

      if(var4 > 0.0D) {
         this.a.add(EnumChatFormatting.ITALIC + "" + (int)(100.0D - 100.0D * var4) + "% Movement Speed");
      }

      if(var1.isPotionActive(PotionManager.b) || var1.isPotionActive(PotionManager.a) || var1.isPotionActive(PotionManager.c) || var1.isPotionActive(PotionManager.d)) {
         this.a.add("");
      }

      if(var1.isPotionActive(PotionManager.b)) {
         this.a.add(EnumChatFormatting.DARK_RED + "" + EnumChatFormatting.BOLD + "Bleeding");
      }

      if(var1.isPotionActive(PotionManager.a)) {
         this.a.add(EnumChatFormatting.GREEN + "" + EnumChatFormatting.BOLD + "RBI Infection");
      }

      if(var1.isPotionActive(PotionManager.c)) {
         this.a.add(EnumChatFormatting.WHITE + "" + EnumChatFormatting.BOLD + "Broken Leg");
      }

      if(var1.isPotionActive(PotionManager.d)) {
         this.a.add(EnumChatFormatting.BLUE + "" + EnumChatFormatting.BOLD + "Adrenaline");
      }

   }

   public void a(Minecraft var1, float var2) {
      if(CraftingDead.l().d().e.i) {
         ScaledResolution var3 = new ScaledResolution(var1.gameSettings, var1.displayWidth, var1.displayHeight);
         int var4 = var3.getScaledWidth();
         int var5 = var3.getScaledHeight();
         EntityClientPlayerMP var6 = var1.thePlayer;
         int var7 = this.a.size() * 10 + 5;
         byte var8 = 125;
         int var9 = var4 - (var8 + 2);
         int var10 = var5 / 2 - var7 / 2;
         if(var6 != null && (var1.currentScreen == null || var1.currentScreen instanceof GuiChat)) {
            CDRenderHelper.a((double)var9, (double)var10, (double)var8, (double)var7, 0, 0.5F);

            for(int var11 = 0; var11 < this.a.size(); ++var11) {
               String var12 = (String)this.a.get(var11);
               CDRenderHelper.a(var12, var9 + 4, var10 + 4 + var11 * 10);
            }
         }

      }
   }
}
