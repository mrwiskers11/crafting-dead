package com.craftingdead.client;

import com.craftingdead.CraftingDead;
import com.craftingdead.item.ItemBinoculars;
import com.craftingdead.item.gun.ItemGun;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ClientGunTicker extends Thread {

   public Minecraft a;
   public boolean b = true;
   public int c = 0;


   public ClientGunTicker(Minecraft var1) {
      this.a = var1;
      CraftingDead.d.info("Creating Gun Thread");
   }

   public void run() {
      while(this.b) {
         try {
            if(this.a != null) {
               EntityClientPlayerMP var1 = this.a.thePlayer;
               if(var1 != null && this.a.theWorld != null && FMLCommonHandler.instance().getSide() == Side.CLIENT) {
                  ItemStack var2 = var1.getCurrentEquippedItem();
                  if(var2 != null && this.c <= 0) {
                     if(var2.getItem() instanceof ItemGun) {
                        ItemGun var3 = (ItemGun)var2.getItem();
                        var3.a((World)this.a.theWorld, (EntityPlayer)var1, var2);
                     } else if(var2.getItem() instanceof ItemBinoculars) {
                        ItemBinoculars var6 = (ItemBinoculars)var2.getItem();
                        var6.a(this.a.theWorld, var1, var2);
                     }
                  }
               }
            }
         } catch (Exception var5) {
            var5.printStackTrace();
         }

         try {
            Thread.sleep(25L);
         } catch (InterruptedException var4) {
            ;
         }
      }

   }

   public void a() {
      this.b = false;
   }
}
