package com.craftingdead.client;

import java.util.Calendar;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;

public class TickChecker {

   private int a = 0;
   private int b = 0;
   private int c = 0;


   public void a(Minecraft var1) {
      if(var1.theWorld != null) {
         int var2 = Calendar.getInstance().get(13);
         ++this.a;
         if(this.b != var2) {
            if(this.a > 25 && var1.inGameHasFocus && var1.currentScreen == null) {
               ++this.c;
            }

            this.a = 0;
            this.b = var2;
         }
      }

      if(this.c > 40 && var1.inGameHasFocus && var1.currentScreen == null) {
         var1.crashed(new CrashReport("Tick rate out of sync! Do not alter Minecraft\'s tick rate!", new Throwable()));
      }

   }
}
