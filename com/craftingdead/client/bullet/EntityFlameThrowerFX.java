package com.craftingdead.client.bullet;

import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFlameFX;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityFlameThrowerFX extends EntityFlameFX {

   private float a;
   private float aB;
   private float aC;


   public EntityFlameThrowerFX(World var1, double var2, double var4, double var6, double var8, double var10, double var12) {
      super(var1, var2, var4, var6, var8, var10, var12);
   }

   public EntityFlameThrowerFX(EntityPlayer var1) {
      super(var1.worldObj, var1.posX, var1.posY + 1.4D, var1.posZ, 0.0D, 0.0D, 0.0D);
      if(var1.username.equals(Minecraft.getMinecraft().thePlayer.username)) {
         this.setPosition(super.posX, super.posY - 1.5D, super.posZ);
      }

      this.setLocationAndAngles(var1.posX, var1.posY + (double)var1.getEyeHeight(), var1.posZ, var1.rotationYaw, var1.rotationPitch);
      super.posX -= (double)(MathHelper.cos(super.rotationYaw / 180.0F * 3.1415927F) * 0.16F);
      super.posY -= 0.3D;
      super.posZ -= (double)(MathHelper.sin(super.rotationYaw / 180.0F * 3.1415927F) * 0.16F);
      this.setPosition(super.posX, super.posY, super.posZ);
      Random var2 = new Random();
      float var3 = (1.0F + (0.5F - var2.nextFloat())) * 1.2F;
      float var4 = var1.rotationYaw + (0.5F - var2.nextFloat()) * 4.0F;
      float var5 = var1.rotationPitch + (0.5F - var2.nextFloat()) * 4.0F;
      double var6 = (double)(-MathHelper.sin(var4 * 3.141593F / 180.0F));
      double var8 = (double)MathHelper.cos(var4 * 3.141593F / 180.0F);
      super.motionX = (double)var3 * var6 * (double)MathHelper.cos(var5 / 180.0F * 3.141593F);
      super.motionY = (double)(-var3 * MathHelper.sin(var5 / 180.0F * 3.141593F));
      super.motionZ = (double)var3 * var8 * (double)MathHelper.cos(var5 / 180.0F * 3.141593F);
      super.particleMaxAge = 20;
      this.a = 0.03F + var2.nextFloat() / 50.0F;
      super.noClip = false;
      this.aB = (0.5F - var2.nextFloat()) / 35.0F;
      this.aC = (0.5F - var2.nextFloat()) / 35.0F;
   }

   public void onUpdate() {
      super.prevPosX = super.posX;
      super.prevPosY = super.posY;
      super.prevPosZ = super.posZ;
      if(super.particleAge++ >= super.particleMaxAge) {
         this.setDead();
      }

      this.moveEntity(super.motionX, super.motionY, super.motionZ);
      super.motionX *= 0.9800000190734863D;
      super.motionY *= 0.9800000190734863D;
      super.motionZ *= 0.9800000190734863D;
      super.motionY -= (double)this.a;
      super.motionX += (double)this.aB;
      super.motionZ += (double)this.aC;
      this.aB *= 1.01F;
      this.aC *= 1.01F;
      if(super.onGround) {
         super.motionX *= 0.099999988079071D;
         super.motionZ *= 0.099999988079071D;
         super.motionY = 0.0D;
      }

   }

   public int getBrightnessForRender(float var1) {
      return 15728880;
   }

   public float getBrightness(float var1) {
      return 1.0F;
   }
}
