package com.craftingdead.client.animation;

import com.craftingdead.CraftingDead;
import java.lang.reflect.Field;
import java.util.Random;
import java.util.logging.Level;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundManager;
import net.minecraft.client.audio.SoundPoolEntry;
import net.minecraft.client.settings.GameSettings;
import paulscode.sound.SoundSystem;

public class AmbienceHandler {

   private SoundManager b;
   private GameSettings c;
   private float d;
   private int e = 4800;
   private int f = 6000;
   public float a = 0.45F;
   private SoundSystem g;


   public AmbienceHandler() {
      Minecraft var1 = Minecraft.getMinecraft();
      this.c = var1.gameSettings;
      this.b = var1.sndManager;
      this.d = var1.gameSettings.musicVolume;
      this.c();
   }

   public void a() {
      try {
         this.b.stopAllSounds();
         this.g = null;
      } catch (Exception var2) {
         ;
      }

   }

   public void b() {
      if(this.g != null && this.b != null) {
         if(!CraftingDead.l().e().e) {
            if(this.g.playing("BgMusic2")) {
               this.g.stop("BgMusic2");
            }

            return;
         }

         if(this.g.playing("BgMusic")) {
            this.g.stop("BgMusic");
         }

         if(this.e++ >= this.f) {
            this.c();
            if(!this.g.playing("BgMusic2")) {
               try {
                  Random var1 = new Random();
                  String var2 = "ambient_cd_" + (var1.nextInt(6) + 1);
                  SoundPoolEntry var3 = new SoundPoolEntry(var2 + ".ogg", this.getClass().getResource("/assets/craftingdead/sound/" + var2 + ".ogg"));
                  this.g.backgroundMusic("BgMusic2", var3.getSoundUrl(), var3.getSoundName(), false);
                  this.g.setVolume("BgMusic2", this.c.musicVolume * this.a);
                  this.g.play("BgMusic2");
               } catch (Exception var4) {
                  CraftingDead.d.log(Level.WARNING, "Ambient sound handler has failed", var4);
               }
            }

            this.e = 0;
         }

         if(this.d != this.c.musicVolume && this.g.playing("BgMusic2")) {
            if(this.c.musicVolume <= 0.0F) {
               this.g.stop("BgMusic2");
            } else {
               this.g.setVolume("BgMusic2", this.c.musicVolume * this.a);
            }
         }

         this.d = this.c.musicVolume;
      }

   }

   private void c() {
      try {
         Field[] var1 = this.b.getClass().getDeclaredFields();
         var1[1].setAccessible(true);
         this.g = (SoundSystem)var1[1].get(this.b);
      } catch (Exception var2) {
         ;
      }

   }
}
