package com.craftingdead.client.animation.gun.reloadf;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.gun.GunAnimationReload;

public class GunAnimationReloadMinigun extends GunAnimationReload {

   private float k = 0.0F;
   private float l = 0.0F;
   private float m = 55.0F;
   private float n = -1.0F;
   private float o = 0.0F;
   private boolean p = true;
   public boolean j = false;


   public GunAnimationReloadMinigun() {
      this.j = true;
      this.n = 0.0F;
   }

   public GunAnimationReloadMinigun(boolean var1) {
      this.j = var1;
      this.n = this.j?0.0F:1.0F;
   }

   public void b(boolean var1) {
      this.j = var1;
      this.n = this.j?0.0F:1.0F;
   }

   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      if(super.a == 15) {
         this.p = false;
      }

      this.l = this.k;
      this.o = this.n;
      if(super.a < 15 || super.a >= 22) {
         float var4 = 3.0F;
         float var5 = 0.1F;
         if(this.j) {
            this.n -= var5;
         } else {
            this.n -= var5;
            if(this.n < 0.0F) {
               this.n = 0.0F;
            }
         }

         if(this.p) {
            this.k += var4;
         } else {
            this.k -= var4;
         }

         if(this.k > this.m) {
            this.k = this.m;
         }

         if(this.k < 0.0F) {
            this.k = 0.0F;
         }

      }
   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.l + (this.k - this.l) * var2;
      GL11.glRotatef(-var3, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(-var3, 1.0F, 0.0F, 0.0F);
      GL11.glTranslated((double)(var3 / 100.0F), 0.0D, 0.0D);
   }

   public void b(ItemStack var1, float var2) {
      float var3;
      if(!this.j) {
         var3 = this.o + (this.n - this.o) * var2;
         GL11.glTranslatef(0.0F, -var3, 0.0F);
      } else {
         var3 = this.o + (this.n - this.o) * var2;
         GL11.glTranslatef(0.0F, -var3, 0.0F);
      }

   }

   public void a(ItemStack var1) {}

   public float a() {
      return 40.0F;
   }
}
