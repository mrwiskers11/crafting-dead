package com.craftingdead.client.animation.gun.reloadb;

import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.gun.GunAnimationReload;

public class GunAnimationReloadMAC10 extends GunAnimationReload {

   protected float j = 0.3F;


   public void a(ItemStack var1, float var2, boolean var3) {
      if((float)super.a < this.a() / 2.0F) {
         float var4;
         if(var3) {
            var4 = super.c + (super.b - super.c) * var2;
            GL11.glRotatef(var4 * 0.2F, 1.0F, 0.0F, 1.0F);
         } else {
            var4 = super.f + (super.e - super.f) * var2;
            GL11.glTranslatef(-var4 * 1.0F, var4 * 0.5F, var4);
            float var5 = super.c + (super.b - super.c) * var2;
            GL11.glRotatef(var5 * 0.2F, 0.0F, 0.0F, 1.0F);
         }
      }

      if(!super.i && !var3 && (float)super.a >= this.a() / 2.0F + 5.0F) {
         this.j = (float)((double)this.j - 0.01D);
         GL11.glTranslatef(this.j - 0.55F, this.j - 0.05F, 0.1F);
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = super.c + (super.b - super.c) * var2;
      GL11.glRotatef(var3, 2.2F, -0.5F, -1.0F);
      GL11.glRotatef(-var3 * 0.25F, 0.0F, 0.0F, 1.0F);
   }

   public void b(ItemStack var1, float var2) {
      float var3;
      if(super.i) {
         var3 = super.f + (super.e - super.f) * var2;
         GL11.glTranslatef(0.0F, -var3, 0.0F);
      } else {
         var3 = super.f + (super.e - super.f) * var2;
         GL11.glTranslatef(var3, -var3, 0.0F);
      }

   }

   public float a() {
      return 40.0F;
   }
}
