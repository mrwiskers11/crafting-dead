package com.craftingdead.client.animation.gun.reloadb;

import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.gun.GunAnimationReload;

public class GunAnimationReloadVector extends GunAnimationReload {

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4;
      if(var3) {
         var4 = super.c + (super.b - super.c) * var2;
         GL11.glRotatef(var4 * 0.2F, 1.0F, 0.0F, 1.0F);
      } else {
         var4 = super.f + (super.e - super.f) * var2;
         GL11.glTranslatef(-var4 * 1.0F, var4 * 0.5F, var4);
         float var5 = super.c + (super.b - super.c) * var2;
         GL11.glRotatef(var5 * 0.2F, 0.0F, 0.0F, 1.0F);
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = super.c + (super.b - super.c) * var2;
      GL11.glRotatef(var3, 2.0F, 0.0F, -1.0F);
   }
}
