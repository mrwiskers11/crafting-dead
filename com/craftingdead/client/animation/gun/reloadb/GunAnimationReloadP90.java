package com.craftingdead.client.animation.gun.reloadb;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.gun.GunAnimationReload;

public class GunAnimationReloadP90 extends GunAnimationReload {

   private float k = 0.0F;
   private float l = 0.0F;
   private float m = -30.0F;
   private float n = 1.4F;
   private float o = 0.0F;
   private boolean p = true;
   public boolean j = false;


   public GunAnimationReloadP90() {}

   public GunAnimationReloadP90(boolean var1) {
      this.j = var1;
      System.out.println(var1);
   }

   public void b(boolean var1) {
      this.j = var1;
      System.out.println(var1);
      if(var1) {
         this.n = 0.0F;
      }

   }

   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      if(super.a == 15) {
         this.p = false;
      }

      this.l = this.k;
      this.o = this.n;
      float var4 = 10.0F;
      float var5 = 0.12F;
      if(this.j) {
         this.n += var5;
         if(this.n > 5.0F) {
            this.n = 5.0F;
         }
      } else {
         this.n -= var5;
         if(this.n < 0.0F) {
            this.n = 0.0F;
         }
      }

      if(this.p) {
         this.k -= var4;
      } else {
         this.k += var4;
      }

      if(this.k < this.m) {
         this.k = this.m;
      }

      if(this.k > 0.0F) {
         this.k = 0.0F;
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.l + (this.k - this.l) * var2;
      GL11.glRotatef(var3, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(var3, 1.0F, 0.0F, 0.0F);
      GL11.glRotatef(-var3 * 0.25F, 0.0F, 0.0F, 1.0F);
   }

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4;
      if(var3) {
         var4 = this.l + (this.k - this.l) * var2;
         GL11.glRotatef(var4 * 0.4F, 0.0F, 0.0F, 1.0F);
      } else {
         var4 = this.l + (this.k - this.l) * var2;
         float var5 = this.o + (this.n - this.o) * var2;
         GL11.glRotatef(-var4 * 1.4F, 0.0F, 0.0F, 1.0F);
         GL11.glTranslatef(-var5 * 0.8F, 0.0F, 0.0F);
      }

   }

   public void b(ItemStack var1, float var2) {
      float var3 = this.o + (this.n - this.o) * var2;
      GL11.glTranslatef(-var3, -var3 * 0.5F, 0.0F);
   }

   public void a(ItemStack var1) {}

   public float a() {
      return 20.0F;
   }
}
