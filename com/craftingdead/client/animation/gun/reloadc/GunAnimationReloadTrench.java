package com.craftingdead.client.animation.gun.reloadc;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.gun.GunAnimationReload;

public class GunAnimationReloadTrench extends GunAnimationReload {

   protected float j = 0.0F;
   protected float k = 0.0F;
   protected float l = 55.0F;
   protected float m = -1.0F;
   protected float n = 0.0F;
   protected float o = 0.3F;
   protected float p = 0.4F;
   protected boolean q = true;
   public boolean r = false;


   public GunAnimationReloadTrench() {
      this.r = true;
      this.m = 0.0F;
   }

   public GunAnimationReloadTrench(boolean var1) {
      this.r = var1;
      this.m = this.r?0.0F:-1.0F;
   }

   public void b(boolean var1) {
      this.r = var1;
      this.m = this.r?0.0F:-1.0F;
   }

   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      if(super.a == 20) {
         this.q = false;
      }

      this.k = this.j;
      this.n = this.m;
      float var4 = 10.0F;
      float var5 = 0.1F;
      if(this.r) {
         this.m -= var5;
      } else {
         this.m += var5;
         if(this.m > 0.0F) {
            this.m = 0.0F;
         }
      }

      if(this.q) {
         this.j += var4;
      } else {
         this.j -= var4;
      }

      if(this.j > this.l) {
         this.j = this.l;
      }

      if(this.j < 0.0F) {
         this.j = 0.0F;
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.k + (this.j - this.k) * var2;
      GL11.glRotatef(-var3, 6.0F, 0.0F, 1.0F);
      GL11.glRotatef(var3, 1.0F, 0.0F, 0.0F);
   }

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4;
      float var5;
      if(!this.r && !var3 && (float)super.a >= this.a() / 2.0F) {
         var4 = (float)super.a - this.a() / 2.0F;
         var5 = this.p - 0.35F;
         float var6 = this.p - 0.3F;
         if(var4 < 12.0F) {
            if(var4 >= 5.0F) {
               this.p = (float)((double)this.p - 0.015D);
            } else {
               this.p = (float)((double)this.p + 0.015D);
            }
         }

         GL11.glTranslatef(var5, var6, 0.1F);
      } else if(var3) {
         var4 = this.k + (this.j - this.k) * var2;
         GL11.glRotatef(var4 * 0.2F, 1.0F, 0.0F, 1.0F);
      } else {
         var4 = this.n + (this.m - this.n) * var2;
         GL11.glTranslatef(-var4 * 1.0F, var4 * 0.5F, var4);
         var5 = this.k + (this.j - this.k) * var2;
         GL11.glRotatef(var5 * 0.2F, 0.0F, 0.0F, 1.0F);
      }

   }

   public void b(ItemStack var1, float var2) {
      float var3;
      if(this.r) {
         var3 = this.n + (this.m - this.n) * var2;
         GL11.glTranslatef(0.0F, -var3, 0.0F);
      } else {
         var3 = this.n + (this.m - this.n) * var2;
         GL11.glTranslatef(var3, -var3, 0.0F);
      }

   }

   public void a(ItemStack var1) {}

   public float a() {
      return 40.0F;
   }
}
