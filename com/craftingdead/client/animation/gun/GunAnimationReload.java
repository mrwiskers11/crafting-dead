package com.craftingdead.client.animation.gun;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.GunAnimation;

public class GunAnimationReload extends GunAnimation {

   protected float b = 0.0F;
   protected float c = 0.0F;
   protected float d = 55.0F;
   protected float e = -1.0F;
   protected float f = 0.0F;
   protected float g = 0.3F;
   protected boolean h = true;
   public boolean i = false;


   public GunAnimationReload() {
      this.i = true;
      this.e = 0.0F;
   }

   public GunAnimationReload(boolean var1) {
      this.i = var1;
      this.e = this.i?0.0F:-1.0F;
   }

   public void b(boolean var1) {
      this.i = var1;
      this.e = this.i?0.0F:-1.0F;
   }

   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      if(super.a == 20) {
         this.h = false;
      }

      this.c = this.b;
      this.f = this.e;
      float var4 = 10.0F;
      float var5 = 0.1F;
      if(this.i) {
         this.e -= var5;
      } else {
         this.e += var5;
         if(this.e > 0.0F) {
            this.e = 0.0F;
         }
      }

      if(this.h) {
         this.b += var4;
      } else {
         this.b -= var4;
      }

      if(this.b > this.d) {
         this.b = this.d;
      }

      if(this.b < 0.0F) {
         this.b = 0.0F;
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.c + (this.b - this.c) * var2;
      GL11.glRotatef(-var3, 4.0F, 0.0F, 1.0F);
      GL11.glRotatef(var3, 1.0F, 0.0F, 0.0F);
   }

   public void b(ItemStack var1, float var2) {
      float var3;
      if(this.i) {
         var3 = this.f + (this.e - this.f) * var2;
         GL11.glTranslatef(0.0F, -var3, 0.0F);
      } else {
         var3 = this.f + (this.e - this.f) * var2;
         GL11.glTranslatef(var3, -var3, 0.0F);
      }

   }

   public void a(ItemStack var1) {}

   public float a() {
      return 30.0F;
   }

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4;
      if(var3) {
         var4 = this.c + (this.b - this.c) * var2;
         GL11.glRotatef(-var4 * 0.4F, 0.0F, 0.0F, 1.0F);
      } else {
         var4 = this.f + (this.e - this.f) * var2;
         GL11.glTranslatef(-var4, var4, var4);
      }

   }
}
