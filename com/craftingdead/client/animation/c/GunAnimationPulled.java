package com.craftingdead.client.animation.c;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.GunAnimation;

public class GunAnimationPulled extends GunAnimation {

   private float b = -180.0F;
   private float c = 0.0F;


   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      this.c = this.b;
      this.b += 35.0F;
      if(this.b > 0.0F) {
         this.b = 0.0F;
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.c + (this.b - this.c) * var2;
      GL11.glRotatef(var3, 0.0F, 1.0F, 0.0F);
   }

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4 = this.c + (this.b - this.c) * var2;
      GL11.glRotatef(-var4 * 0.3F, 0.0F, 1.0F, 0.0F);
   }

   public void a(ItemStack var1) {}

   public float a() {
      return 12.0F;
   }
}
