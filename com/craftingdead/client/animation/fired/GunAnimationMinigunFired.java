package com.craftingdead.client.animation.fired;

import com.craftingdead.client.animation.GunAnimation;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class GunAnimationMinigunFired extends GunAnimation {

   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {}

   public void a(ItemStack var1, float var2) {}

   public void b(ItemStack var1, float var2) {}

   public void a(ItemStack var1) {}

   public float a() {
      return 0.0F;
   }

   public void a(ItemStack var1, float var2, boolean var3) {}
}
