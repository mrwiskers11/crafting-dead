package com.craftingdead.client.animation.fired;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.GunAnimation;

public class GunAnimationPistolFired extends GunAnimation {

   private float b = 0.0F;
   private float c = 0.0F;
   private float d = 10.0F;
   private float e = 0.0F;
   private float f = 0.0F;
   private float g = 0.5F;
   private boolean h = true;


   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      this.c = this.b;
      this.f = this.e;
      float var4 = 60.0F;
      float var5 = 0.3F;
      if(super.a == 2) {
         this.h = false;
      }

      if(this.h) {
         this.b += var4;
         this.e += var5;
      } else {
         this.b -= var4;
         this.e -= var5;
      }

      if(this.e > this.g) {
         this.e = this.g;
      }

      if(this.e < 0.0F) {
         this.e = 0.0F;
      }

      if(this.b > this.d) {
         this.b = this.d;
      }

      if(this.b < 0.0F) {
         this.b = 0.0F;
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.f + (this.e - this.f) * var2;
      GL11.glTranslatef(-var3, 0.0F, 0.0F);
      float var4 = this.c + (this.b - this.c) * var2;
      GL11.glRotatef(-var4, 0.0F, 0.0F, 1.0F);
   }

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4 = this.f + (this.e - this.f) * var2;
      GL11.glTranslatef(-var4, 0.0F, 0.0F);
      float var5 = this.c + (this.b - this.c) * var2;
      GL11.glRotatef(-var5, 0.0F, 0.0F, 1.0F);
   }

   public void b(ItemStack var1, float var2) {}

   public void a(ItemStack var1) {}

   public float a() {
      return 4.0F;
   }
}
