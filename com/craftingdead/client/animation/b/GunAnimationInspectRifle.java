package com.craftingdead.client.animation.b;

import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.b.GunAnimationInspect;

public class GunAnimationInspectRifle extends GunAnimationInspect {

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4;
      float var5;
      if(var3) {
         var4 = super.f + (super.e - super.f) * var2;
         GL11.glTranslatef(var4 * 0.4F, var4 * 0.4F, -var4 * 0.6F);
         var5 = super.c + (super.b - super.c) * var2;
         GL11.glRotatef(-var5 * 0.1F, 0.0F, 0.0F, 1.0F);
      } else {
         var4 = super.f + (super.e - super.f) * var2;
         GL11.glTranslatef(-var4, 0.0F, -var4);
         var5 = super.c + (super.b - super.c) * var2;
         GL11.glRotatef(var5, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(-var5 / 2.0F, 1.0F, 0.0F, 0.0F);
      }

   }
}
