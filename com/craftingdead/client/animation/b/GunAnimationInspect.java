package com.craftingdead.client.animation.b;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.animation.GunAnimation;

public class GunAnimationInspect extends GunAnimation {

   protected float b = 0.0F;
   protected float c = 0.0F;
   protected float d = 55.0F;
   protected float e = 0.0F;
   protected float f = 0.0F;
   protected float g = 0.6F;
   protected boolean h = true;


   public void a(Minecraft var1, EntityPlayer var2, ItemStack var3) {
      if((double)super.a > (double)this.a() * 0.8D) {
         this.h = false;
      }

      this.c = this.b;
      this.f = this.e;
      float var4 = 15.0F;
      float var5 = 0.15F;
      if(this.h) {
         this.e += var5;
         if(this.e > this.g) {
            this.e = this.g;
         }

         this.b += var4;
      } else {
         this.b -= var4;
         this.e -= var5;
         if(this.e < 0.0F) {
            this.e = 0.0F;
         }
      }

      if(this.b > this.d) {
         this.b = this.d;
      }

      if(this.b < 0.0F) {
         this.b = 0.0F;
      }

   }

   public void a(ItemStack var1, float var2) {
      float var3 = this.f + (this.e - this.f) * var2;
      GL11.glTranslatef(var3, 0.0F, var3);
      float var4 = this.c + (this.b - this.c) * var2;
      GL11.glRotatef(-var4, 0.0F, 1.0F, 0.0F);
      GL11.glRotatef(var4 / 3.0F, 1.0F, 0.0F, 0.0F);
   }

   public void a(ItemStack var1, float var2, boolean var3) {
      float var4;
      float var5;
      if(var3) {
         var4 = this.f + (this.e - this.f) * var2;
         GL11.glTranslatef(var4 * 0.95F, var4 * 0.4F, 0.0F);
         GL11.glTranslatef(0.0F, 0.04F, 0.1F);
         var5 = this.c + (this.b - this.c) * var2;
         GL11.glRotatef(-var5 * 0.1F, 0.0F, 0.0F, 1.0F);
      } else {
         var4 = this.f + (this.e - this.f) * var2;
         GL11.glTranslatef(-var4, 0.0F, var4);
         var5 = this.c + (this.b - this.c) * var2;
         GL11.glRotatef(var5, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(-var5 / 2.0F, 1.0F, 0.0F, 0.0F);
      }

   }

   public void a(ItemStack var1) {}

   public float a() {
      return 100.0F;
   }
}
