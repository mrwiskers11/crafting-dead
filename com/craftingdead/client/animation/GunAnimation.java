package com.craftingdead.client.animation;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public abstract class GunAnimation {

   public int a;


   public abstract void a(Minecraft var1, EntityPlayer var2, ItemStack var3);

   public abstract void a(ItemStack var1, float var2);

   public abstract void a(ItemStack var1, float var2, boolean var3);

   public boolean a(boolean var1) {
      return true;
   }

   public abstract void a(ItemStack var1);

   public abstract float a();
}
