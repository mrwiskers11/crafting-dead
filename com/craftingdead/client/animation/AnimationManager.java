package com.craftingdead.client.animation;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.animation.GunAnimation;

import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

public class AnimationManager {

   private GunAnimation a;
   private GunAnimation b;
   private ItemStack c = null;
   private ItemStack d = null;


   public void a(Minecraft var1) {
      if(var1.thePlayer != null) {
         this.c = this.d;
         ItemStack var2 = var1.thePlayer.getCurrentEquippedItem();
         this.d = var2;
         if(this.c != null && this.d != null && this.d.itemID != this.c.itemID) {
            this.a();
            return;
         }

         if(this.b != null) {
            this.a = this.b;
            this.b = null;
         }

         if(this.a != null && this.d == null) {
            this.a();
            return;
         }

         if(this.d != null && this.a != null) {
            this.a.a(var1, var1.thePlayer, var2);
            if(this.a != null) {
               ++this.a.a;
               if((float)this.a.a > this.a.a()) {
                  this.a.a(var2);
                  this.a = null;
               }
            }
         }
      }

   }

   public void a(GunAnimation var1) {
      this.b = var1;
   }

   public void a() {
      if(this.a != null) {
         this.a.a((ItemStack)null);
         this.a = null;
      }

   }

   public void b() {
      this.a();
      this.b = null;
   }

   public GunAnimation c() {
      return this.a;
   }

   public static AnimationManager d() {
      return CraftingDead.l().d().f;
   }
}
