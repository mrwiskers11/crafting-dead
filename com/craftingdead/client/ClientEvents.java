package com.craftingdead.client;

import com.craftingdead.client.ClientProxy;
import com.craftingdead.client.bullet.BulletCollisionBox;
import com.craftingdead.client.bullet.EntityFlameThrowerFX;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.GuiHome;
import com.craftingdead.client.gui.gui.GuiPauseMenu;
import com.craftingdead.client.gui.gui.GuiSplashScreen;
import com.craftingdead.client.render.RenderEvents;
import com.craftingdead.entity.EntityManager;
import com.craftingdead.event.EnumBulletCollision;
import com.craftingdead.event.EventBulletCollision;
import com.craftingdead.network.packets.CDAPacketACLPaused;
import com.craftingdead.network.packets.CDAPacketOpenGUI;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.RenderLivingEvent.Specials.Pre;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.Event.Result;

public class ClientEvents {

   private ClientProxy a;


   public ClientEvents(ClientProxy var1) {
      this.a = var1;
   }

   @ForgeSubscribe
   public void a(Pre var1) {
      EntityLivingBase var2 = var1.entity;
      if(var2 instanceof EntityPlayer) {
         EntityPlayer var3 = (EntityPlayer)var2;
         if(var3.username.startsWith("NPC_")) {
            var1.setCanceled(true);
            var1.setResult(Result.DENY);
         }
      }

   }

   @ForgeSubscribe
   public void a(GuiOpenEvent var1) {
      if(ClientProxy.a && !(var1.gui instanceof GuiSplashScreen)) {
         var1.setCanceled(true);
         Minecraft.getMinecraft().displayGuiScreen(new GuiSplashScreen());
      }

      if(!ClientProxy.a && var1.gui instanceof GuiMainMenu) {
         var1.setCanceled(true);
         Minecraft.getMinecraft().displayGuiScreen(new GuiHome());
      }

      EntityClientPlayerMP var2 = Minecraft.getMinecraft().thePlayer;
      if(var2 != null) {
         PlayerData var3 = PlayerDataHandler.b();
         if(var1.gui instanceof GuiIngameMenu) {
            var1.gui = new GuiPauseMenu();
            if(!Minecraft.getMinecraft().isSingleplayer() && var3.b()) {
               PacketDispatcher.sendPacketToServer(CDAPacketACLPaused.b());
               var3.K = 100;
            }
         }

         if(Minecraft.getMinecraft().playerController.isNotCreative() && var1.gui instanceof GuiInventory) {
            var1.setCanceled(true);
            PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.a));
         }
      }

      if(var1.gui instanceof GuiCDScreen) {
         ((GuiCDScreen)var1.gui).a = this.a;
      }

   }

   public void a(ItemStack var1, World var2, EntityPlayer var3) {
      Random var4 = new Random();
      int var5 = var4.nextInt(25);

      for(int var6 = 0; var6 < var5; ++var6) {
         Minecraft var7 = Minecraft.getMinecraft();
         var7.effectRenderer.addEffect(new EntityFlameThrowerFX(var3));
      }

   }

   public void a(EntityLivingBase var1, boolean var2, Vec3 var3) {
      double var4 = var3.xCoord;
      double var6 = var3.yCoord;
      double var8 = var3.zCoord;
      Random var10 = new Random();
      EventBulletCollision var11 = new EventBulletCollision(var1 instanceof EntityPlayer?EnumBulletCollision.b:EnumBulletCollision.c, (EntityPlayer)null, (ItemStack)null, var1.worldObj);
      var11.a(var1);
      if(!MinecraftForge.EVENT_BUS.post(var11)) {
         byte var12;
         int var13;
         if(!var1.isDead) {
            var12 = 20;

            for(var13 = 0; var13 < var12; ++var13) {
               double var14 = var10.nextDouble();
               double var16 = var14 * (double)(0.5F - var10.nextFloat());
               double var18 = var14 * (double)(0.5F - var10.nextFloat());
               double var20 = var14 * (double)(0.5F - var10.nextFloat());
               EntityManager.a("tilecrack_152_0", var4 + var16, var6 + (double)var1.getEyeHeight() + var18, var8 + var20, 0.0D, 0.0D, 0.0D);
            }
         }

         if(var1 instanceof EntityPlayer && var2) {
            var12 = 20;

            for(var13 = 0; var13 < var12; ++var13) {
               EntityManager.a("tilecrack_42_0", var1.posX, var1.posY + 1.75D, var1.posZ, 0.0D, 0.0D, 0.0D);
            }
         }

      }
   }

   public void a(World var1, double var2, double var4, double var6, int var8, int var9, Vec3 var10) {
      Minecraft var11 = Minecraft.getMinecraft();
      byte var12 = 15;
      double var13 = var10.xCoord;
      double var15 = var10.yCoord;
      double var17 = var10.zCoord;
      EventBulletCollision var19 = new EventBulletCollision(EnumBulletCollision.a, (EntityPlayer)null, (ItemStack)null, var1);
      var19.a((int)var2, (int)var4, (int)var6);
      BulletCollisionBox var20 = new BulletCollisionBox(var13, var15, var17);
      RenderEvents.b(var20);
      if(!MinecraftForge.EVENT_BUS.post(var19)) {
         if(var11.gameSettings.fancyGraphics) {
            var12 = 30;
         }

         int var21 = var1.getBlockMetadata((int)var2, (int)var4, (int)var6);

         for(int var22 = 0; var22 < var12; ++var22) {
            EntityManager.a("tilecrack_" + var8 + "_" + var21, var13, var15, var17, 0.0D, 0.0D, 0.0D);
         }

         if(var11.thePlayer.getDistance(var2, var4, var6) < 18.0D) {
            Block var25 = Block.blocksList[var11.theWorld.getBlockId((int)var2, (int)var4, (int)var6)];
            if(var25 != null) {
               Material var23 = var25.blockMaterial;
               String var24 = "dirt";
               if(var23 == Material.wood) {
                  var24 = "wood";
               }

               if(var23 == Material.grass) {
                  var24 = "dirt";
               }

               if(var23 == Material.rock) {
                  var24 = "stone";
               }

               if(var23 == Material.glass || var23 == Material.ice) {
                  var24 = "glass";
               }

               if(var25 == Block.blockIron || var25 == Block.blockGold || var25 == Block.blockDiamond || var25 == Block.blockEmerald || var25 == Block.furnaceIdle || var25 == Block.furnaceBurning || var25 == Block.anvil) {
                  var24 = "metal";
               }

               var11.sndManager.playSound("craftingdead:bulletimpact_" + var24, (float)var2, (float)var4, (float)var6, 0.5F, 1.0F);
            }
         }

      }
   }
}
