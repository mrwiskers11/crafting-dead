package com.craftingdead.client.model.attachment;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelAttachmentEOTECH extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;


   public ModelAttachmentEOTECH() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 5, 50);
      this.a.addBox(0.0F, 0.0F, 0.0F, 8, 24, 1);
      this.a.setRotationPoint(-7.0F, 0.0F, -9.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 30, 50);
      this.b.addBox(0.0F, 0.0F, 0.0F, 8, 24, 1);
      this.b.setRotationPoint(-7.0F, 0.0F, 7.0F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 5, 80);
      this.c.addBox(0.0F, 0.0F, 0.0F, 25, 10, 15);
      this.c.setRotationPoint(-8.0F, 13.25F, -8.0F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 5, 30);
      this.d.addBox(0.0F, 0.0F, 0.0F, 5, 13, 1);
      this.d.setRotationPoint(-3.7F, 1.7F, -9.0F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, -0.3490659F);
      this.e = new ModelRenderer(this, 25, 30);
      this.e.addBox(0.0F, 0.0F, 0.0F, 5, 13, 1);
      this.e.setRotationPoint(-3.7F, 1.7F, 7.0F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, -0.3490659F);
      this.f = new ModelRenderer(this, 5, 110);
      this.f.addBox(0.0F, 0.0F, 0.0F, 8, 1, 15);
      this.f.setRotationPoint(-7.0F, 0.0F, -8.0F);
      this.f.setTextureSize(256, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 42, 30);
      this.g.addBox(0.0F, 0.0F, 0.0F, 5, 12, 1);
      this.g.setRotationPoint(0.5F, 12.2F, -9.0F);
      this.g.setTextureSize(256, 128);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 60, 30);
      this.h.addBox(0.0F, 0.0F, 0.0F, 5, 12, 1);
      this.h.setRotationPoint(0.5F, 12.2F, 7.0F);
      this.h.setTextureSize(256, 128);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 100, 30);
      this.i.addBox(0.0F, 0.0F, 0.0F, 2, 10, 4);
      this.i.setRotationPoint(-9.0F, 14.5F, -5.9F);
      this.i.setTextureSize(256, 128);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 60, 50);
      this.j.addBox(0.0F, 0.0F, 0.0F, 14, 2, 13);
      this.j.setRotationPoint(1.0F, 11.9F, -7.0F);
      this.j.setTextureSize(256, 128);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 80, 30);
      this.k.addBox(0.0F, 0.0F, 0.0F, 2, 10, 4);
      this.k.setRotationPoint(-9.0F, 14.5F, 1.4F);
      this.k.setTextureSize(256, 128);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 100, 70);
      this.l.addBox(0.0F, 0.0F, 0.0F, 24, 2, 12);
      this.l.setRotationPoint(-8.0F, 22.0F, -6.0F);
      this.l.setTextureSize(256, 128);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 100, 87);
      this.m.addBox(0.0F, 0.0F, 0.0F, 5, 2, 15);
      this.m.setRotationPoint(-5.0F, 11.9F, -8.0F);
      this.m.setTextureSize(256, 128);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 200, 5);
      this.n.addBox(0.0F, 0.0F, 0.0F, 1, 13, 15);
      this.n.setRotationPoint(-4.0F, 1.0F, -8.0F);
      this.n.setTextureSize(256, 128);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
      this.o = new ModelRenderer(this, 30, 5);
      this.o.addBox(0.0F, 0.0F, 0.0F, 4, 4, 18);
      this.o.setRotationPoint(-4.3F, 14.2F, -9.5F);
      this.o.setTextureSize(256, 128);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, 0.0F);
      this.v = new ModelRenderer(this, 2, 5);
      this.v.addBox(0.0F, 0.0F, 0.0F, 8, 3, 3);
      this.v.setRotationPoint(-9.0F, 3.0F, 8.0F);
      this.v.setTextureSize(256, 128);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.v.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
