package com.craftingdead.client.model.attachment;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;

public class ModelAttachmentBipod extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;


   public ModelAttachmentBipod() {
      super.textureWidth = 64;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 6, 1, 3);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 7);
      this.b.addBox(0.0F, 0.0F, 0.0F, 6, 2, 2);
      this.b.setRotationPoint(2.0F, 1.0F, -0.5F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0349066F, 0.0523599F);
      this.c = new ModelRenderer(this, 0, 7);
      this.c.addBox(0.0F, 0.0F, -2.0F, 6, 2, 2);
      this.c.setRotationPoint(2.0F, 1.0F, 3.5F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, 0.0F, -0.0349066F, 0.0523599F);
      this.d = new ModelRenderer(this, 0, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 2, 1, 2);
      this.d.setRotationPoint(0.0F, 1.0F, 0.5F);
      this.d.setTextureSize(64, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 0, 12);
      this.e.addBox(6.0F, 0.5F, 0.0F, 6, 1, 1);
      this.e.setRotationPoint(2.0F, 1.0F, 0.0F);
      this.e.setTextureSize(64, 32);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0349066F, 0.0523599F);
      this.f = new ModelRenderer(this, 0, 15);
      this.f.addBox(12.0F, 0.0F, 0.0F, 2, 2, 2);
      this.f.setRotationPoint(2.0F, 1.0F, -0.5F);
      this.f.setTextureSize(64, 32);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0349066F, 0.0523599F);
      this.g = new ModelRenderer(this, 0, 12);
      this.g.addBox(6.0F, 0.5F, -1.5F, 6, 1, 1);
      this.g.setRotationPoint(2.0F, 1.0F, 3.5F);
      this.g.setTextureSize(64, 32);
      this.g.mirror = true;
      this.a(this.g, 0.0F, -0.0349066F, 0.0523599F);
      this.h = new ModelRenderer(this, 0, 15);
      this.h.addBox(12.0F, 0.0F, -2.0F, 2, 2, 2);
      this.h.setRotationPoint(2.0F, 1.0F, 3.5F);
      this.h.setTextureSize(64, 32);
      this.h.mirror = true;
      this.a(this.h, 0.0F, -0.0349066F, 0.0523599F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      double var8 = 0.7D;
      GL11.glScaled(var8, var8, var8);
      GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
