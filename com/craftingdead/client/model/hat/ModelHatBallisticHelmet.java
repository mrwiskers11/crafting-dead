package com.craftingdead.client.model.hat;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelHatBallisticHelmet extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;


   public ModelHatBallisticHelmet() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 9, 35);
      this.a.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.a.setRotationPoint(8.0F, 5.0F, -5.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 89, 35);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 7, 3);
      this.b.setRotationPoint(6.0F, 8.0F, -11.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 1, 35);
      this.c.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.c.setRotationPoint(-9.0F, 5.0F, -5.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 1, 41);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 14, 4);
      this.d.setRotationPoint(-9.0F, 5.0F, -9.0F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 14, 41);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 14, 4);
      this.e.setRotationPoint(8.0F, 5.0F, -9.0F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 55, 35);
      this.f.addBox(0.0F, 0.0F, 0.0F, 14, 4, 1);
      this.f.setRotationPoint(-7.0F, 15.0F, -9.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 27, 44);
      this.g.addBox(0.0F, 0.0F, 0.0F, 1, 14, 1);
      this.g.setRotationPoint(7.0F, 5.0F, -9.0F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 20, 35);
      this.h.addBox(0.0F, 0.0F, 0.0F, 14, 3, 1);
      this.h.setRotationPoint(-7.0F, 5.0F, -9.0F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 34, 44);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 14, 1);
      this.i.setRotationPoint(-8.0F, 5.0F, -9.0F);
      this.i.setTextureSize(128, 64);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 43, 50);
      this.j.addBox(0.0F, 0.0F, 0.0F, 12, 1, 3);
      this.j.setRotationPoint(-6.0F, 14.0F, -11.0F);
      this.j.setTextureSize(128, 64);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 77, 43);
      this.k.addBox(0.0F, 0.0F, 0.0F, 1, 7, 3);
      this.k.setRotationPoint(-7.0F, 8.0F, -11.0F);
      this.k.setTextureSize(128, 64);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 89, 48);
      this.l.addBox(0.0F, 0.0F, 0.0F, 12, 5, 0);
      this.l.setRotationPoint(-6.0F, 9.0F, -10.0F);
      this.l.setTextureSize(128, 64);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 1, 1);
      this.m.addBox(0.0F, 0.0F, 0.0F, 16, 16, 16);
      this.m.setRotationPoint(-8.0F, 1.0F, -8.0F);
      this.m.setTextureSize(128, 64);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 43, 43);
      this.n.addBox(0.0F, 0.0F, 0.0F, 12, 1, 3);
      this.n.setRotationPoint(-6.0F, 8.0F, -11.0F);
      this.n.setTextureSize(128, 64);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
