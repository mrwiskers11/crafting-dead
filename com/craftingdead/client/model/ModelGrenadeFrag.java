package com.craftingdead.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelGrenadeFrag extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;


   public ModelGrenadeFrag() {
      super.textureWidth = 64;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 4, 4, 4);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 3, 3);
      this.b.setRotationPoint(-0.5F, 0.5F, 0.5F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 0, 0);
      this.c.addBox(0.0F, 0.0F, 0.0F, 3, 3, 1);
      this.c.setRotationPoint(0.5F, 0.5F, -0.5F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 0, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 3, 3);
      this.d.setRotationPoint(3.5F, 0.5F, 0.5F);
      this.d.setTextureSize(64, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 0, 0);
      this.e.addBox(0.0F, 0.0F, 0.0F, 3, 3, 1);
      this.e.setRotationPoint(0.5F, 0.5F, 3.5F);
      this.e.setTextureSize(64, 32);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 0, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 3, 1, 3);
      this.f.setRotationPoint(0.5F, 3.5F, 0.5F);
      this.f.setTextureSize(64, 32);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 11, 10);
      this.g.addBox(0.0F, 0.0F, 0.0F, 3, 1, 3);
      this.g.setRotationPoint(0.5F, -1.0F, 0.5F);
      this.g.setTextureSize(64, 32);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 0, 10);
      this.h.addBox(0.0F, 0.0F, 0.0F, 2, 2, 2);
      this.h.setRotationPoint(1.0F, -3.0F, 1.0F);
      this.h.setTextureSize(64, 32);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 0, 15);
      this.i.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.i.setRotationPoint(3.0F, -3.0F, 1.5F);
      this.i.setTextureSize(64, 32);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.6457718F);
      this.j = new ModelRenderer(this, 0, 18);
      this.j.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1);
      this.j.setRotationPoint(4.5F, -1.9F, 1.5F);
      this.j.setTextureSize(64, 32);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 1.047198F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
