package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelDeagle extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;
   ModelRenderer w;
   ModelRenderer x;
   ModelRenderer y;
   ModelRenderer z;
   ModelRenderer A;
   ModelRenderer B;


   public ModelDeagle() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 48, 20);
      this.a.addBox(0.0F, 0.0F, 0.0F, 14, 3, 2);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 74, 35);
      this.b.addBox(-3.0F, -0.5F, 0.0F, 3, 6, 2);
      this.b.setRotationPoint(4.0F, 3.0F, 0.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.2094395F);
      this.c = new ModelRenderer(this, 81, 29);
      this.c.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.c.setRotationPoint(0.0F, 3.0F, 0.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, -0.9075712F);
      this.d = new ModelRenderer(this, 72, 26);
      this.d.addBox(0.0F, 0.0F, 0.0F, 2, 1, 2);
      this.d.setRotationPoint(3.5F, 4.0F, 0.0F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 65, 26);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.e.setRotationPoint(5.5F, 3.0F, 0.0F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 41, 20);
      this.f.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.f.setRotationPoint(14.0F, 0.0F, 0.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 41, 25);
      this.g.addBox(-1.0F, 0.0F, 0.0F, 1, 1, 2);
      this.g.setRotationPoint(15.0F, 2.0F, 0.0F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.7853982F);
      this.h = new ModelRenderer(this, 41, 25);
      this.h.addBox(0.0F, -1.0F, 0.0F, 1, 1, 2);
      this.h.setRotationPoint(14.0F, 3.0F, 0.0F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, -0.7853982F);
      this.i = new ModelRenderer(this, 64, 16);
      this.i.addBox(0.0F, 0.0F, 0.0F, 6, 1, 2);
      this.i.setRotationPoint(0.0F, -0.3F, 0.0F);
      this.i.setTextureSize(128, 64);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 41, 17);
      this.j.addBox(0.0F, 0.0F, 0.0F, 9, 1, 1);
      this.j.setRotationPoint(6.0F, -0.3F, 0.5F);
      this.j.setTextureSize(128, 64);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 36, 20);
      this.k.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.k.setRotationPoint(14.2F, 0.5F, 0.5F);
      this.k.setTextureSize(128, 64);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 81, 20);
      this.l.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.l.setRotationPoint(-0.5F, 0.0F, 0.0F);
      this.l.setTextureSize(128, 64);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.122173F);
      this.m = new ModelRenderer(this, 88, 20);
      this.m.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.m.setRotationPoint(-1.0F, 0.0F, 0.5F);
      this.m.setTextureSize(128, 64);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.3839724F);
      this.n = new ModelRenderer(this, 76, 13);
      this.n.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.n.setRotationPoint(1.0F, -0.7F, 0.5F);
      this.n.setTextureSize(128, 64);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
      this.o = new ModelRenderer(this, 67, 40);
      this.o.addBox(-0.5F, 4.5F, 0.0F, 1, 1, 2);
      this.o.setRotationPoint(4.0F, 3.0F, 0.0F);
      this.o.setTextureSize(128, 64);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, 0.2094395F);
      this.v = new ModelRenderer(this, 72, 30);
      this.v.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.v.setRotationPoint(3.5F, 2.5F, 0.5F);
      this.v.setTextureSize(128, 64);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, 0.0F);
      this.w = new ModelRenderer(this, 81, 25);
      this.w.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.w.setRotationPoint(-1.0F, 2.0F, 0.0F);
      this.w.setTextureSize(128, 64);
      this.w.mirror = true;
      this.a(this.w, 0.0F, 0.0F, 0.0F);
      this.x = new ModelRenderer(this, 85, 35);
      this.x.addBox(-2.5F, -0.5F, 0.0F, 2, 6, 1);
      this.x.setRotationPoint(4.0F, 3.0F, 1.2F);
      this.x.setTextureSize(128, 64);
      this.x.mirror = true;
      this.a(this.x, 0.0F, 0.0F, 0.2094395F);
      this.y = new ModelRenderer(this, 85, 35);
      this.y.addBox(-2.5F, -0.5F, 0.0F, 2, 6, 1);
      this.y.setRotationPoint(4.0F, 3.0F, -0.2F);
      this.y.setTextureSize(128, 64);
      this.y.mirror = true;
      this.a(this.y, 0.0F, 0.0F, 0.2094395F);
      this.z = new ModelRenderer(this, 41, 14);
      this.z.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.z.setRotationPoint(13.5F, -0.7F, 0.5F);
      this.z.setTextureSize(128, 64);
      this.z.mirror = true;
      this.a(this.z, 0.0F, 0.0F, 0.0F);
      this.A = new ModelRenderer(this, 48, 26);
      this.A.addBox(0.0F, 0.0F, 0.0F, 7, 1, 1);
      this.A.setRotationPoint(6.0F, 2.2F, 0.5F);
      this.A.setTextureSize(128, 64);
      this.A.mirror = true;
      this.a(this.A, 0.0F, 0.0F, 0.0F);
      this.B = new ModelRenderer(this, 69, 13);
      this.B.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.B.setRotationPoint(2.0F, -0.5F, 0.5F);
      this.B.setTextureSize(128, 64);
      this.B.mirror = true;
      this.a(this.B, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.v.render(var7);
      this.w.render(var7);
      this.x.render(var7);
      this.y.render(var7);
      this.z.render(var7);
      this.A.render(var7);
      this.B.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
