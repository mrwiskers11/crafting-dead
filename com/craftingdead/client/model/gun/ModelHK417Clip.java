package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelHK417Clip extends ModelBase {

   ModelRenderer a;


   public ModelHK417Clip() {
      super.textureWidth = 512;
      super.textureHeight = 256;
      this.a = new ModelRenderer(this, 97, 65);
      this.a.addBox(0.0F, 0.0F, 0.0F, 3, 4, 2);
      this.a.setRotationPoint(-2.6F, 4.3F, -0.5F);
      this.a.setTextureSize(512, 256);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, -0.0872665F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
