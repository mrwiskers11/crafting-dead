package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelACRClip extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;


   public ModelACRClip() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 148, 84);
      this.a.addBox(0.4666667F, 0.0F, 0.0F, 4, 3, 3);
      this.a.setRotationPoint(1.0F, 1.5F, 0.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, -0.1487195F);
      this.b = new ModelRenderer(this, 149, 93);
      this.b.addBox(0.3F, 3.0F, 0.0F, 4, 3, 3);
      this.b.setRotationPoint(1.0F, 1.5F, 0.0F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.2230767F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void b(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
   }
}
