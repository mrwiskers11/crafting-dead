package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMuzzleFlash extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;


   public ModelMuzzleFlash() {
      super.textureWidth = 64;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 1, 1);
      this.a.addBox(0.0F, 0.0F, 0.0F, 8, 8, 0);
      this.a.setRotationPoint(-4.0F, -4.0F, 0.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 9, 1);
      this.b.addBox(-4.0F, 0.0F, 0.0F, 8, 0, 15);
      this.b.setRotationPoint(0.0F, 0.0F, -15.0F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.7853982F);
      this.c = new ModelRenderer(this, 1, 17);
      this.c.addBox(-4.0F, 0.0F, 0.0F, 8, 0, 15);
      this.c.setRotationPoint(0.0F, 0.0F, -15.0F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, -2.373648F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void b(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.setRotationAngles(var7, var2, var3, var4, var5, var6, var1);
   }
}
