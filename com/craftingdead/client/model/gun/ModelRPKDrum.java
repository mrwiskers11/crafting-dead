package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelRPKDrum extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;


   public ModelRPKDrum() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1);
      this.a.setRotationPoint(-3.0F, 1.2F, 1.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 4, 4, 4);
      this.b.setRotationPoint(-4.5F, 3.666667F, 2.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 0, 0);
      this.c.addBox(0.0F, 0.0F, 0.0F, 4, 4, 4);
      this.c.setRotationPoint(-4.5F, 3.7F, -3.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 0, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 2, 1, 3);
      this.d.setRotationPoint(-3.5F, 3.233333F, 2.5F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 0, 0);
      this.e.addBox(0.0F, 0.0F, 0.0F, 2, 1, 3);
      this.e.setRotationPoint(-3.466667F, 7.2F, 2.5F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 0, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 2, 1, 3);
      this.f.setRotationPoint(-3.5F, 3.2F, -2.5F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 0, 0);
      this.g.addBox(0.0F, 0.0F, 0.0F, 2, 1, 3);
      this.g.setRotationPoint(-3.5F, 7.2F, -2.5F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.a(var2, var3, var4, var5, var6, var7);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void a(float var1, float var2, float var3, float var4, float var5, float var6) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, (Entity)null);
   }
}
