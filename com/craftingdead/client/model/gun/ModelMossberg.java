package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMossberg extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;
   ModelRenderer w;
   ModelRenderer x;
   ModelRenderer y;
   ModelRenderer z;
   ModelRenderer A;
   ModelRenderer B;
   ModelRenderer C;


   public ModelMossberg() {
      super.textureWidth = 128;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 33, 22);
      this.a.addBox(0.0F, 0.0F, 0.0F, 2, 3, 2);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(128, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.9294653F);
      this.b = new ModelRenderer(this, 41, 17);
      this.b.addBox(0.0F, 0.0F, 0.0F, 2, 2, 1);
      this.b.setRotationPoint(0.0F, 0.0F, 0.1F);
      this.b.setTextureSize(128, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.3717861F);
      this.c = new ModelRenderer(this, 51, 17);
      this.c.addBox(0.0F, 0.0F, 0.0F, 7, 2, 2);
      this.c.setRotationPoint(1.8F, -0.7F, 0.0F);
      this.c.setTextureSize(128, 32);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 70, 17);
      this.d.addBox(0.0F, 0.0F, 0.0F, 16, 1, 1);
      this.d.setRotationPoint(8.533334F, -0.7F, 0.5F);
      this.d.setTextureSize(128, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 70, 20);
      this.e.addBox(0.0F, 0.0F, 0.0F, 9, 1, 1);
      this.e.setRotationPoint(8.533334F, 0.3333333F, 0.5F);
      this.e.setTextureSize(128, 32);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 1, 14);
      this.f.addBox(0.0F, -1.0F, 0.0F, 8, 2, 2);
      this.f.setRotationPoint(-10.0F, 1.333333F, 0.0F);
      this.f.setTextureSize(128, 32);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, -0.0371786F);
      this.g = new ModelRenderer(this, 0, 25);
      this.g.addBox(-1.0F, 0.2666667F, 0.0F, 9, 1, 2);
      this.g.setRotationPoint(-9.0F, 4.0F, 0.0F);
      this.g.setTextureSize(128, 32);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, -0.3717861F);
      this.h = new ModelRenderer(this, 0, 19);
      this.h.addBox(0.0F, -1.0F, 0.0F, 1, 3, 2);
      this.h.setRotationPoint(-9.9F, 3.0F, 0.0F);
      this.h.setTextureSize(128, 32);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, -0.0371786F);
      this.i = new ModelRenderer(this, 34, 18);
      this.i.addBox(1.0F, 0.0F, 0.0F, 1, 1, 2);
      this.i.setRotationPoint(-3.0F, 0.0F, 0.0F);
      this.i.setTextureSize(128, 32);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.5576792F);
      this.j = new ModelRenderer(this, 24, 21);
      this.j.addBox(0.0F, 0.0F, 0.0F, 2, 1, 2);
      this.j.setRotationPoint(-2.0F, 0.0F, 0.0F);
      this.j.setTextureSize(128, 32);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.5379539F);
      this.k = new ModelRenderer(this, 24, 17);
      this.k.addBox(0.0F, 0.5F, 0.0F, 2, 1, 2);
      this.k.setRotationPoint(-1.6F, 0.0F, 0.0F);
      this.k.setTextureSize(128, 32);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 0, 10);
      this.l.addBox(0.0F, 0.0F, 0.0F, 6, 1, 2);
      this.l.setRotationPoint(-9.0F, 2.0F, 0.0F);
      this.l.setTextureSize(128, 32);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 7, 20);
      this.m.addBox(-0.7333333F, -0.2F, 0.0F, 4, 2, 2);
      this.m.setRotationPoint(-9.0F, 2.466667F, 0.0F);
      this.m.setTextureSize(128, 32);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, -0.1115358F);
      this.n = new ModelRenderer(this, 24, 25);
      this.n.addBox(-0.8F, 0.0F, 0.0F, 2, 1, 2);
      this.n.setRotationPoint(-2.0F, 2.0F, 0.0F);
      this.n.setTextureSize(128, 32);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.3717861F);
      this.o = new ModelRenderer(this, 0, 6);
      this.o.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.o.setRotationPoint(-3.0F, 2.0F, 0.0F);
      this.o.setTextureSize(128, 32);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, 0.0F);
      this.v = new ModelRenderer(this, 44, 22);
      this.v.addBox(0.0F, 0.0F, 0.0F, 0, 1, 1);
      this.v.setRotationPoint(3.0F, 1.0F, 0.5F);
      this.v.setTextureSize(128, 32);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, -0.0743572F);
      this.w = new ModelRenderer(this, 47, 25);
      this.w.addBox(0.0F, 0.0F, 0.0F, 1, 0, 1);
      this.w.setRotationPoint(3.0F, 2.0F, 0.5F);
      this.w.setTextureSize(128, 32);
      this.w.mirror = true;
      this.a(this.w, 0.0F, 0.0F, 0.1115358F);
      this.x = new ModelRenderer(this, 53, 21);
      this.x.addBox(0.0F, 0.3F, 0.0F, 0, 2, 1);
      this.x.setRotationPoint(4.933333F, 0.06666667F, 0.5F);
      this.x.setTextureSize(128, 32);
      this.x.mirror = true;
      this.a(this.x, 0.0F, 0.0F, 0.4833219F);
      this.y = new ModelRenderer(this, 60, 23);
      this.y.addBox(0.0F, 0.0F, 0.0F, 6, 1, 2);
      this.y.setRotationPoint(10.0F, 0.0F, 0.0F);
      this.y.setTextureSize(128, 32);
      this.y.mirror = true;
      this.a(this.y, 0.0F, 0.0F, 0.0F);
      this.z = new ModelRenderer(this, 77, 23);
      this.z.addBox(0.0F, 0.0F, 0.0F, 6, 1, 2);
      this.z.setRotationPoint(10.0F, 0.6F, 0.0F);
      this.z.setTextureSize(128, 32);
      this.z.mirror = true;
      this.a(this.z, 0.0F, 0.0F, 0.0F);
      this.A = new ModelRenderer(this, 55, 14);
      this.A.addBox(0.0F, 0.0F, 0.0F, 7, 1, 1);
      this.A.setRotationPoint(2.0F, -0.9F, 0.5F);
      this.A.setTextureSize(128, 32);
      this.A.mirror = true;
      this.a(this.A, 0.0F, 0.0F, 0.0F);
      this.B = new ModelRenderer(this, 8, 6);
      this.B.addBox(0.0F, 0.0F, 0.0F, 6, 1, 2);
      this.B.setRotationPoint(-10.0F, 0.3F, 0.0F);
      this.B.setTextureSize(128, 32);
      this.B.mirror = true;
      this.a(this.B, 0.0F, 0.0F, -0.0371786F);
      this.C = new ModelRenderer(this, 41, 17);
      this.C.addBox(0.0F, 0.0F, 0.9F, 2, 2, 1);
      this.C.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.C.setTextureSize(128, 32);
      this.C.mirror = true;
      this.a(this.C, 0.0F, 0.0F, -0.3717861F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.v.render(var7);
      this.w.render(var7);
      this.x.render(var7);
      this.y.render(var7);
      this.z.render(var7);
      this.A.render(var7);
      this.B.render(var7);
      this.C.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
