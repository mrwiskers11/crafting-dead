package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelAS50Clip extends ModelBase {

   ModelRenderer a;


   public ModelAS50Clip() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 45, 41);
      this.a.addBox(-4.0F, 0.0F, 0.0F, 4, 3, 1);
      this.a.setRotationPoint(9.5F, 3.0F, 0.5F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
