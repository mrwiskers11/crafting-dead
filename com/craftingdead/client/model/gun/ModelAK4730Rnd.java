package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelAK4730Rnd extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;


   public ModelAK4730Rnd() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 0, 115);
      this.a.addBox(0.0F, 0.0F, 0.0F, 14, 8, 5);
      this.a.setRotationPoint(-42.0F, 6.0F, -1.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, -0.0523599F);
      this.b = new ModelRenderer(this, 45, 115);
      this.b.addBox(0.0F, 0.0F, 0.0F, 14, 6, 5);
      this.b.setRotationPoint(-41.55F, 14.0F, -1.0F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.1745329F);
      this.c = new ModelRenderer(this, 90, 115);
      this.c.addBox(0.0F, 0.0F, 0.0F, 14, 7, 5);
      this.c.setRotationPoint(-40.5F, 19.95F, -1.0F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, -0.296706F);
      this.d = new ModelRenderer(this, 135, 115);
      this.d.addBox(0.0F, 0.0F, 0.0F, 14, 5, 5);
      this.d.setRotationPoint(-38.45F, 26.5F, -1.0F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, -0.418879F);
      this.e = new ModelRenderer(this, 180, 110);
      this.e.addBox(0.0F, 0.0F, 0.0F, 14, 10, 5);
      this.e.setRotationPoint(-36.4F, 31.05F, -1.0F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0174533F, 0.0F, -0.6283185F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
