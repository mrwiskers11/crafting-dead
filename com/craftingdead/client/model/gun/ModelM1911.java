package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelM1911 extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;
   ModelRenderer w;


   public ModelM1911() {
      super.textureWidth = 64;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 22, 12);
      this.a.addBox(0.0F, 0.0F, 0.0F, 10, 2, 2);
      this.a.setRotationPoint(0.0F, 1.0F, 0.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 36, 17);
      this.b.addBox(0.0F, 0.0F, 0.0F, 3, 6, 2);
      this.b.setRotationPoint(0.0F, 2.0F, 0.0F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.1570796F);
      this.c = new ModelRenderer(this, 27, 17);
      this.c.addBox(0.0F, 0.0F, 0.0F, 3, 1, 1);
      this.c.setRotationPoint(2.0F, 4.0F, 0.5F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 22, 17);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.d.setRotationPoint(5.0F, 3.0F, 0.5F);
      this.d.setTextureSize(64, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 6, 13);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.e.setRotationPoint(12.5F, 0.5F, 0.5F);
      this.e.setTextureSize(64, 32);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 13, 17);
      this.f.addBox(0.0F, 0.0F, 0.0F, 3, 1, 1);
      this.f.setRotationPoint(10.0F, 1.8F, 0.5F);
      this.f.setTextureSize(64, 32);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 36, 8);
      this.g.addBox(0.0F, 0.0F, 0.0F, 3, 1, 2);
      this.g.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.g.setTextureSize(64, 32);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 3, 8);
      this.h.addBox(0.0F, 0.0F, 0.0F, 8, 1, 2);
      this.h.setRotationPoint(5.0F, 0.0F, 0.0F);
      this.h.setTextureSize(64, 32);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 11, 13);
      this.i.addBox(0.0F, 0.0F, 0.0F, 3, 1, 2);
      this.i.setRotationPoint(10.0F, 1.0F, 0.0F);
      this.i.setTextureSize(64, 32);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 27, 5);
      this.j.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.j.setRotationPoint(3.0F, 0.0F, 0.5F);
      this.j.setTextureSize(64, 32);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 47, 13);
      this.k.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.k.setRotationPoint(-1.0F, 1.0F, 0.0F);
      this.k.setTextureSize(64, 32);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 34, 5);
      this.l.addBox(0.0F, 0.0F, 0.0F, 3, 1, 1);
      this.l.setRotationPoint(0.0F, -0.2F, 0.5F);
      this.l.setTextureSize(64, 32);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 47, 10);
      this.m.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.m.setRotationPoint(-0.5F, 0.0F, 0.5F);
      this.m.setTextureSize(64, 32);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 6, 5);
      this.n.addBox(0.0F, 0.0F, 0.0F, 8, 1, 1);
      this.n.setRotationPoint(5.0F, -0.2F, 0.5F);
      this.n.setTextureSize(64, 32);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
      this.o = new ModelRenderer(this, 47, 18);
      this.o.addBox(0.5F, 0.0F, 0.0F, 2, 6, 1);
      this.o.setRotationPoint(0.0F, 2.0F, 1.2F);
      this.o.setTextureSize(64, 32);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, 0.1570796F);
      this.v = new ModelRenderer(this, 54, 18);
      this.v.addBox(0.5F, 0.0F, 0.0F, 2, 6, 1);
      this.v.setRotationPoint(0.0F, 2.0F, -0.2F);
      this.v.setTextureSize(64, 32);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, 0.1570796F);
      this.w = new ModelRenderer(this, 26, 8);
      this.w.addBox(0.0F, 0.0F, 0.0F, 2, 1, 2);
      this.w.setRotationPoint(3.0F, 0.5F, 0.0F);
      this.w.setTextureSize(64, 32);
      this.w.mirror = true;
      this.a(this.w, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.v.render(var7);
      this.w.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
