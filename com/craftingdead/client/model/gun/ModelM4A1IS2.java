package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelM4A1IS2 extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;


   public ModelM4A1IS2() {
      super.textureWidth = 64;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 1, 14);
      this.a.addBox(0.0F, 0.0F, 0.0F, 4, 1, 4);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 8, 9);
      this.b.addBox(0.0F, -3.0F, 0.0F, 4, 3, 1);
      this.b.setRotationPoint(0.0F, 1.0F, 0.0F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.1745329F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 8, 9);
      this.c.addBox(0.0F, -2.0F, -1.0F, 4, 3, 1);
      this.c.setRotationPoint(0.0F, 0.0F, 4.0F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, -0.1745329F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 1, 8);
      this.d.addBox(0.0F, -3.0F, 0.0F, 2, 4, 1);
      this.d.setRotationPoint(1.0F, 0.0F, 1.5F);
      this.d.setTextureSize(64, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
