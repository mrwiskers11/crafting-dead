package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelAWPClip extends ModelBase {

   ModelRenderer a;


   public ModelAWPClip() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 36, 44);
      this.a.addBox(0.0F, 0.0F, 0.0F, 4, 2, 2);
      this.a.setRotationPoint(3.5F, 3.0F, 0.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
