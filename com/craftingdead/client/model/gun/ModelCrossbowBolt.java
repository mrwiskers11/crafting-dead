package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelCrossbowBolt extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;


   public ModelCrossbowBolt() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 36, 1, 1);
      this.a.setRotationPoint(-6.6F, -1.0F, 2.5F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 13);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1);
      this.b.setRotationPoint(29.4F, -1.5F, 2.5F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 1, 18);
      this.c.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.c.setRotationPoint(29.4F, -1.0F, 2.0F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 1, 12);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.d.setRotationPoint(30.0F, -1.0F, 2.5F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
