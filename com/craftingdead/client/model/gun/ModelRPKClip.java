package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelRPKClip extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;


   public ModelRPKClip() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 44, 31);
      this.a.addBox(0.0F, 0.0F, 0.0F, 4, 4, 3);
      this.a.setRotationPoint(-6.0F, 2.0F, 0.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, -0.1745329F);
      this.b = new ModelRenderer(this, 59, 31);
      this.b.addBox(0.0F, 0.0F, 0.0F, 4, 4, 3);
      this.b.setRotationPoint(-5.3F, 5.9F, 0.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.3490659F);
      this.c = new ModelRenderer(this, 74, 31);
      this.c.addBox(0.0F, 0.0F, 0.0F, 4, 4, 3);
      this.c.setRotationPoint(-4.0F, 9.5F, 0.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, -0.5235988F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
