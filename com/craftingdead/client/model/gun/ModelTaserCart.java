package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTaserCart extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;


   public ModelTaserCart() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 100, 30);
      this.a.addBox(0.0F, 0.0F, 0.0F, 1, 3, 3);
      this.a.setRotationPoint(0.25F, 0.0F, 0.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 90, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 1, 3);
      this.b.setRotationPoint(-0.2F, -0.4F, 0.0F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 80, 0);
      this.c.addBox(0.0F, 0.0F, 0.0F, 3, 3, 1);
      this.c.setRotationPoint(-2.2F, -0.1F, -0.5F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, -0.0698132F, 0.0349066F);
      this.d = new ModelRenderer(this, 100, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 3, 3, 1);
      this.d.setRotationPoint(-2.2F, -0.1F, 2.5F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0698132F, 0.0349066F);
      this.e = new ModelRenderer(this, 110, 0);
      this.e.addBox(0.0F, 0.0F, 0.0F, 3, 1, 3);
      this.e.setRotationPoint(-2.0F, -1.0F, 0.0F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.1919862F);
      this.f = new ModelRenderer(this, 65, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 3, 1, 3);
      this.f.setRotationPoint(-2.25F, 2.45F, 0.0F);
      this.f.setTextureSize(256, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, -0.0872665F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
