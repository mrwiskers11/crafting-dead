package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelScarh extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;
   ModelRenderer w;


   public ModelScarh() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 40, 25);
      this.a.addBox(0.0F, 0.0F, 0.0F, 17, 2, 2);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 66, 30);
      this.b.addBox(0.0F, 0.0F, 0.0F, 4, 2, 2);
      this.b.setRotationPoint(0.0F, 1.5F, 0.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 53, 30);
      this.c.addBox(0.0F, 0.0F, 0.0F, 4, 3, 2);
      this.c.setRotationPoint(4.0F, 2.0F, 0.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 70, 35);
      this.d.addBox(-2.0F, 0.0F, 0.0F, 2, 4, 2);
      this.d.setRotationPoint(2.0F, 3.5F, 0.0F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.3141593F);
      this.e = new ModelRenderer(this, 59, 36);
      this.e.addBox(0.0F, 0.0F, 0.0F, 3, 0, 2);
      this.e.setRotationPoint(1.0F, 5.0F, 0.0F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 79, 25);
      this.f.addBox(0.0F, 0.0F, 0.0F, 1, 3, 2);
      this.f.setRotationPoint(-1.0F, 0.0F, 0.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 42, 22);
      this.g.addBox(0.0F, 0.0F, 0.0F, 17, 1, 1);
      this.g.setRotationPoint(0.0F, -1.0F, 0.5F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 34, 30);
      this.h.addBox(0.0F, 0.0F, 0.0F, 8, 1, 1);
      this.h.setRotationPoint(8.0F, 1.5F, 0.5F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 27, 25);
      this.i.addBox(0.0F, 0.0F, 0.0F, 5, 1, 1);
      this.i.setRotationPoint(17.0F, 0.5F, 0.5F);
      this.i.setTextureSize(128, 64);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 35, 22);
      this.j.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.j.setRotationPoint(17.0F, -0.75F, 0.5F);
      this.j.setTextureSize(128, 64);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 30, 21);
      this.k.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1);
      this.k.setRotationPoint(18.0F, -1.5F, 0.5F);
      this.k.setTextureSize(128, 64);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 86, 25);
      this.l.addBox(0.0F, 0.0F, 0.0F, 6, 2, 1);
      this.l.setRotationPoint(-7.0F, 0.5F, 0.5F);
      this.l.setTextureSize(128, 64);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 106, 25);
      this.m.addBox(0.0F, 0.0F, 0.0F, 1, 5, 1);
      this.m.setRotationPoint(-8.0F, 0.5F, 0.5F);
      this.m.setTextureSize(128, 64);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 101, 25);
      this.n.addBox(-1.0F, -4.0F, 0.0F, 1, 4, 1);
      this.n.setRotationPoint(-7.0F, 5.5F, 0.5F);
      this.n.setTextureSize(128, 64);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.2094395F);
      this.o = new ModelRenderer(this, 79, 21);
      this.o.addBox(0.0F, 0.0F, 0.0F, 5, 1, 2);
      this.o.setRotationPoint(-4.9F, 0.0F, 0.0F);
      this.o.setTextureSize(128, 64);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, -0.2007129F);
      this.v = new ModelRenderer(this, 94, 20);
      this.v.addBox(0.0F, 0.0F, 0.0F, 4, 2, 2);
      this.v.setRotationPoint(-4.9F, 0.0F, 0.0F);
      this.v.setTextureSize(128, 64);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, 0.0F);
      this.w = new ModelRenderer(this, 111, 25);
      this.w.addBox(0.0F, 0.0F, 0.0F, 1, 6, 2);
      this.w.setRotationPoint(-9.0F, 0.0F, 0.0F);
      this.w.setTextureSize(128, 64);
      this.w.mirror = true;
      this.a(this.w, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.v.render(var7);
      this.w.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
