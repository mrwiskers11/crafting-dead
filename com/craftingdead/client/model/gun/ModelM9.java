package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelM9 extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;
   ModelRenderer w;


   public ModelM9() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 50, 30);
      this.a.addBox(0.0F, 0.0F, 0.0F, 6, 2, 2);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 37, 30);
      this.b.addBox(0.0F, 0.0F, 0.0F, 4, 1, 2);
      this.b.setRotationPoint(6.0F, 0.0F, 0.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 39, 34);
      this.c.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1);
      this.c.setRotationPoint(6.0F, 1.0F, 0.5F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 55, 27);
      this.d.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1);
      this.d.setRotationPoint(0.5F, -0.5F, 0.5F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 67, 31);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.e.setRotationPoint(-0.5F, 1.5F, 0.0F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 58, 35);
      this.f.addBox(-2.0F, 0.0F, 0.0F, 2, 5, 2);
      this.f.setRotationPoint(2.5F, 2.0F, 0.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.122173F);
      this.g = new ModelRenderer(this, 67, 28);
      this.g.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.g.setRotationPoint(-0.2F, -0.2F, 0.5F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.4712389F);
      this.h = new ModelRenderer(this, 42, 40);
      this.h.addBox(0.0F, 0.0F, 0.0F, 2, 1, 2);
      this.h.setRotationPoint(2.5F, 3.0F, 0.0F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 35, 40);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.i.setRotationPoint(4.5F, 2.0F, 0.0F);
      this.i.setTextureSize(128, 64);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 32, 30);
      this.j.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.j.setRotationPoint(9.5F, -0.3F, 0.5F);
      this.j.setTextureSize(128, 64);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 42, 27);
      this.k.addBox(0.0F, 0.0F, 0.0F, 5, 1, 1);
      this.k.setRotationPoint(4.0F, -0.3F, 0.5F);
      this.k.setTextureSize(128, 64);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 37, 27);
      this.l.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.l.setRotationPoint(9.0F, -0.5F, 0.5F);
      this.l.setTextureSize(128, 64);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 51, 35);
      this.m.addBox(-0.5F, 0.0F, 0.0F, 1, 5, 2);
      this.m.setRotationPoint(2.5F, 2.0F, 0.0F);
      this.m.setTextureSize(128, 64);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.122173F);
      this.n = new ModelRenderer(this, 46, 37);
      this.n.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.n.setRotationPoint(3.0F, 1.5F, 0.5F);
      this.n.setTextureSize(128, 64);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
      this.v = new ModelRenderer(this, 67, 35);
      this.v.addBox(-1.75F, 0.0F, 0.0F, 2, 5, 1);
      this.v.setRotationPoint(2.5F, 2.0F, 1.2F);
      this.v.setTextureSize(128, 64);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, 0.122173F);
      this.w = new ModelRenderer(this, 67, 35);
      this.w.addBox(-1.75F, 0.0F, 0.0F, 2, 5, 1);
      this.w.setRotationPoint(2.5F, 2.0F, -0.2F);
      this.w.setTextureSize(128, 64);
      this.w.mirror = true;
      this.a(this.w, 0.0F, 0.0F, 0.122173F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.v.render(var7);
      this.w.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
