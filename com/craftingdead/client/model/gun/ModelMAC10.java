package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;

public class ModelMAC10 extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;
   ModelRenderer w;
   ModelRenderer x;
   ModelRenderer y;


   public ModelMAC10() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 40, 30);
      this.a.addBox(0.0F, 0.0F, 0.0F, 13, 3, 3);
      this.a.setRotationPoint(0.0F, 0.0F, 0.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 55, 37);
      this.b.addBox(0.0F, 0.0F, 0.0F, 3, 5, 2);
      this.b.setRotationPoint(3.0F, 3.0F, 0.5F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 55, 45);
      this.c.addBox(0.0F, 0.0F, 0.0F, 2, 6, 1);
      this.c.setRotationPoint(4.0F, 8.0F, 1.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 48, 42);
      this.d.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.d.setRotationPoint(6.0F, 4.5F, 1.0F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 45, 37);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1);
      this.e.setRotationPoint(8.0F, 2.5F, 1.0F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 50, 37);
      this.f.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.f.setRotationPoint(6.5F, 3.0F, 1.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 26, 30);
      this.g.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.g.setRotationPoint(13.0F, 1.0F, 1.0F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 40, 37);
      this.h.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.h.setRotationPoint(12.0F, 3.0F, 1.0F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 66, 37);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.i.setRotationPoint(2.5F, 3.0F, 0.5F);
      this.i.setTextureSize(128, 64);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 73, 36);
      this.j.addBox(0.0F, 0.0F, 0.0F, 1, 1, 3);
      this.j.setRotationPoint(-1.0F, 2.5F, 0.0F);
      this.j.setTextureSize(128, 64);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 73, 30);
      this.k.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1);
      this.k.setRotationPoint(-1.5F, -1.0F, 0.0F);
      this.k.setTextureSize(128, 64);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 73, 30);
      this.l.addBox(0.0F, 0.0F, 0.0F, 1, 4, 1);
      this.l.setRotationPoint(-1.5F, -1.0F, 2.0F);
      this.l.setTextureSize(128, 64);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 73, 27);
      this.m.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1);
      this.m.setRotationPoint(-1.0F, -1.5F, 0.0F);
      this.m.setTextureSize(128, 64);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0872665F);
      this.n = new ModelRenderer(this, 73, 27);
      this.n.addBox(0.0F, 0.0F, 0.0F, 4, 1, 1);
      this.n.setRotationPoint(-1.0F, -1.5F, 2.0F);
      this.n.setTextureSize(128, 64);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0872665F);
      this.o = new ModelRenderer(this, 68, 27);
      this.o.addBox(3.0F, 0.0F, 0.0F, 1, 1, 1);
      this.o.setRotationPoint(-1.0F, -1.5F, 1.0F);
      this.o.setTextureSize(128, 64);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, 0.0872665F);
      this.v = new ModelRenderer(this, 40, 23);
      this.v.addBox(0.0F, 0.0F, 0.0F, 1, 1, 2);
      this.v.setRotationPoint(12.0F, -0.5F, 0.5F);
      this.v.setTextureSize(128, 64);
      this.v.mirror = true;
      this.a(this.v, 0.0F, 0.0F, 0.0F);
      this.w = new ModelRenderer(this, 47, 24);
      this.w.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.w.setRotationPoint(10.0F, -0.5F, 1.0F);
      this.w.setTextureSize(128, 64);
      this.w.mirror = true;
      this.a(this.w, 0.0F, 0.0F, 0.0F);
      this.x = new ModelRenderer(this, 40, 27);
      this.x.addBox(0.0F, 0.0F, 0.0F, 11, 1, 1);
      this.x.setRotationPoint(1.0F, -0.2F, 1.0F);
      this.x.setTextureSize(128, 64);
      this.x.mirror = true;
      this.a(this.x, 0.0F, 0.0F, 0.0F);
      this.y = new ModelRenderer(this, 33, 30);
      this.y.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.y.setRotationPoint(12.5F, 0.5F, 0.5F);
      this.y.setTextureSize(128, 64);
      this.y.mirror = true;
      this.a(this.y, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      GL11.glPushMatrix();
      GL11.glRotatef(-100.0F, 0.0F, 0.0F, 1.0F);
      GL11.glTranslatef(-0.125F, -0.22F, 0.0F);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      GL11.glPopMatrix();
      this.v.render(var7);
      this.w.render(var7);
      this.x.render(var7);
      this.y.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
