package com.craftingdead.client.model.gun;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMP5A5Clip extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;


   public ModelMP5A5Clip() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 110, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 6, 9, 3);
      this.a.setRotationPoint(-19.6F, 6.0F, -0.5F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, -0.122173F);
      this.b = new ModelRenderer(this, 135, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 6, 7, 3);
      this.b.setRotationPoint(-18.5F, 14.9F, -0.5F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.2617994F);
      this.c = new ModelRenderer(this, 160, 0);
      this.c.addBox(0.0F, 0.0F, 0.0F, 6, 10, 3);
      this.c.setRotationPoint(-16.7F, 21.65F, -0.5F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, -0.5235988F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
