package com.craftingdead.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelHandDrill extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;


   public ModelHandDrill() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 55, 44);
      this.a.addBox(0.0F, 0.0F, 0.0F, 5, 2, 3);
      this.a.setRotationPoint(0.0F, 3.0F, 0.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 56, 51);
      this.b.addBox(0.0F, 0.0F, 0.0F, 4, 1, 3);
      this.b.setRotationPoint(0.5333334F, 2.733333F, 0.0F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 58, 30);
      this.c.addBox(-0.4666667F, -2.266667F, 0.0F, 2, 6, 2);
      this.c.setRotationPoint(2.0F, 0.0F, 0.5F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, -0.1487144F);
      this.d = new ModelRenderer(this, 53, 21);
      this.d.addBox(0.0F, 0.0F, 0.0F, 7, 2, 3);
      this.d.setRotationPoint(-2.0F, -4.0F, 0.0F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 34, 33);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 1, 3);
      this.e.setRotationPoint(4.7F, -3.0F, 0.0F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, -0.2602503F);
      this.f = new ModelRenderer(this, 33, 10);
      this.f.addBox(0.0F, 0.0F, 0.0F, 1, 1, 3);
      this.f.setRotationPoint(5.0F, -4.0F, 0.0F);
      this.f.setTextureSize(256, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.2602503F);
      this.g = new ModelRenderer(this, 35, 25);
      this.g.addBox(0.0F, 0.0F, 0.0F, 1, 1, 3);
      this.g.setRotationPoint(5.1F, -3.3F, 0.0F);
      this.g.setTextureSize(256, 128);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 35, 18);
      this.h.addBox(0.0F, 0.0F, 0.0F, 1, 1, 3);
      this.h.setRotationPoint(5.1F, -3.7F, 0.0F);
      this.h.setTextureSize(256, 128);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 75, 21);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 2, 2);
      this.i.setRotationPoint(-3.0F, -4.0F, 0.5F);
      this.i.setTextureSize(256, 128);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 83, 22);
      this.j.addBox(-1.0F, 0.0F, 0.0F, 4, 1, 1);
      this.j.setRotationPoint(-6.0F, -3.866667F, 1.5F);
      this.j.setTextureSize(256, 128);
      this.j.mirror = true;
      this.a(this.j, -0.8179294F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 55, 11);
      this.k.addBox(0.0F, 0.0F, 0.0F, 5, 1, 3);
      this.k.setRotationPoint(-1.0F, -4.2F, 0.0F);
      this.k.setTextureSize(256, 128);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 45, 38);
      this.l.addBox(-0.2F, 0.0F, 0.0F, 2, 1, 1);
      this.l.setRotationPoint(3.0F, -2.133333F, 1.0F);
      this.l.setTextureSize(256, 128);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, -0.4833219F);
      this.m = new ModelRenderer(this, 69, 34);
      this.m.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.m.setRotationPoint(0.6F, -1.4F, 1.0F);
      this.m.setTextureSize(256, 128);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 56, 58);
      this.n.addBox(0.0F, 0.0F, 0.0F, 4, 1, 3);
      this.n.setRotationPoint(0.5333334F, 4.1F, 0.0F);
      this.n.setTextureSize(256, 128);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
      this.o = new ModelRenderer(this, 72, 28);
      this.o.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1);
      this.o.setRotationPoint(-0.4F, -2.7F, 1.0F);
      this.o.setTextureSize(256, 128);
      this.o.mirror = true;
      this.a(this.o, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.h.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
