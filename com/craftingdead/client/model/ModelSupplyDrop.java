package com.craftingdead.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelSupplyDrop extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;


   public ModelSupplyDrop() {
      super.textureWidth = 256;
      super.textureHeight = 256;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 32, 4, 32);
      this.a.setRotationPoint(-16.0F, 20.0F, -16.0F);
      this.a.setTextureSize(256, 256);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 38);
      this.b.addBox(0.0F, 0.0F, 0.0F, 30, 18, 15);
      this.b.setRotationPoint(-15.0F, 2.0F, 0.0F);
      this.b.setTextureSize(256, 256);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 0, 73);
      this.c.addBox(0.0F, 0.0F, 0.0F, 14, 14, 14);
      this.c.setRotationPoint(0.0F, 6.0F, -15.0F);
      this.c.setTextureSize(256, 256);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 0, 105);
      this.d.addBox(0.0F, 0.0F, 0.0F, 11, 6, 6);
      this.d.setRotationPoint(-13.0F, 14.0F, -15.0F);
      this.d.setTextureSize(256, 256);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 0, 119);
      this.e.addBox(0.0F, 0.0F, 0.0F, 11, 6, 6);
      this.e.setRotationPoint(-13.0F, 14.0F, -7.0F);
      this.e.setTextureSize(256, 256);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 0, 133);
      this.f.addBox(0.0F, 0.0F, 0.0F, 40, 30, 40);
      this.f.setRotationPoint(-20.0F, -50.0F, -20.0F);
      this.f.setTextureSize(256, 256);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      if(var1.fallDistance > 0.0F && !var1.onGround) {
         this.f.render(var7);
      }

   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
