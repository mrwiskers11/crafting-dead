package com.craftingdead.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelCrate extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;


   public ModelCrate() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 12, 7, 8);
      this.a.setRotationPoint(-6.0F, 0.0F, -4.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 7, 17);
      this.b.addBox(0.0F, 0.0F, 0.0F, 2, 7, 1);
      this.b.setRotationPoint(3.0F, 0.0F, -4.2F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 0, 17);
      this.c.addBox(0.0F, 0.0F, 0.0F, 2, 7, 1);
      this.c.setRotationPoint(-5.0F, 0.0F, -4.2F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 41, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 11, 1, 7);
      this.d.setRotationPoint(-5.5F, -0.2F, -3.533333F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 14, 17);
      this.e.addBox(0.0F, 0.0F, 0.0F, 12, 1, 1);
      this.e.setRotationPoint(-6.0F, 0.5F, -4.2F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 0, 27);
      this.f.addBox(0.0F, 0.0F, 0.0F, 1, 1, 8);
      this.f.setRotationPoint(5.2F, 0.5F, -4.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 0, 27);
      this.g.addBox(0.0F, 0.0F, 0.0F, 1, 1, 8);
      this.g.setRotationPoint(-6.2F, 0.5F, -4.0F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 19, 27);
      this.h.addBox(0.0F, 0.0F, 0.0F, 12, 1, 1);
      this.h.setRotationPoint(-6.0F, 0.5F, 3.2F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
