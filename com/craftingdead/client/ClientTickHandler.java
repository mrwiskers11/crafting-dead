package com.craftingdead.client;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.ClientNotification;
import com.craftingdead.client.ClientProxy;
import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.client.TickChecker;
import com.craftingdead.client.animation.AnimationManager;
import com.craftingdead.client.animation.GunAnimation;
import com.craftingdead.client.animation.c.GunAnimationPulled;
import com.craftingdead.client.bullet.BulletTracer;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.client.gui.gui.minimap.GuiMap;
import com.craftingdead.client.gui.gui.minimap.GuiMapMarkerDialog;
import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.map.Marker;
import com.craftingdead.client.render.RenderTickHandler;
import com.craftingdead.item.IItemMovementPenalty;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.blueprint.ItemBlueprint;
import com.craftingdead.item.gun.EnumFireMode;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.mojang.MojangAPI;
import com.craftingdead.network.packets.CDAPacketACLUnpaused;
import com.craftingdead.network.packets.CDAPacketOpenGUI;
import com.craftingdead.network.packets.CDAPacketPlayerDataToServer;
import com.craftingdead.network.packets.CDAPacketSwitchItem;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.network.PacketDispatcher;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.crash.CrashReport;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.input.Keyboard;

public class ClientTickHandler implements ITickHandler {

   public float a = 0.0F;
   public float b;
   public float c;
   public float d;
   private Minecraft p = Minecraft.getMinecraft();
   public RenderTickHandler e = new RenderTickHandler();
   public AnimationManager f = new AnimationManager();
   public float g;
   public int h = 0;
   public static double i;
   public static final float j = 0.6F;
   public static final float k = 0.7F;
   public static final float l = 0.2F;
   public static final float m = 0.65F;
   private int q = 0;
   private int r = 0;
   private int s = 0;
   private int t = 0;
   private TickChecker u = new TickChecker();
   private GuiScreen v = null;
   public ArrayList n = new ArrayList();
   public ClientNotification o = null;


   public void tickStart(EnumSet var1, Object ... var2) {}

   public void tickEnd(EnumSet var1, Object ... var2) {
      try {
         Minecraft var3 = Minecraft.getMinecraft();
         if(var1.equals(EnumSet.of(TickType.RENDER))) {
            float var4 = ((Float)var2[0]).floatValue();
            this.e.a(var3, var4);
            CraftingDead.l().k().o();
         } else if(var1.equals(EnumSet.of(TickType.CLIENT))) {
            MojangAPI.downloadPendingUUIDs();
            GuiScreen var6 = Minecraft.getMinecraft().currentScreen;
            if(this.v != var6) {
               if(var6 == null) {
                  PacketDispatcher.sendPacketToServer(CDAPacketACLUnpaused.b());
               }

               this.v = var6;
            }

            if(var6 != null) {
               this.a(var3, var6);
            } else {
               this.b(var3);
               this.u.a(var3);
            }

            this.a(var3);
         } else if(var1.equals(EnumSet.of(TickType.PLAYER))) {
            this.a((EntityPlayer)var2[0]);
         }
      } catch (Exception var5) {
         if(Minecraft.getMinecraft().theWorld != null && !Minecraft.getMinecraft().isSingleplayer()) {
            PacketDispatcher.sendPacketToServer(CDAPacketACLUnpaused.b());
         }

         Minecraft.getMinecraft().crashed(new CrashReport(">>> Crafting Dead Crash <<<", var5));
      }

   }

   private void a(EntityPlayer var1) {
      if(var1 != null && !var1.isDead) {
         CraftingDead.f.c().e().a(var1);
      }

   }

   private void a(Minecraft var1) {
      CraftingDead.l().h().a();
      if(i >= 2.9D) {
         i -= 0.5D;
      }

      if(i <= 0.0D) {
         i = 0.0D;
      }

      this.f.a(var1);
      PlayerData var2;
      if(var1.theWorld != null) {
         CraftingDead.l().j().a();
         var2 = PlayerDataHandler.b();
         if(var2.N > 0) {
            --var2.N;
         }

         if(var2.K > 0) {
            --var2.K;
         }
      } else {
         var2 = PlayerDataHandler.b();
         var2.N = 0;
         var2.K = 0;
      }

      if(this.e.h > 0) {
         --this.e.h;
      }

      if(this.e.a().a.size() > 0) {
         for(int var6 = 0; var6 < this.e.a().a.size(); ++var6) {
            if(((BulletTracer)this.e.a().a.get(var6)).c-- <= 0) {
               this.e.a().a.remove(var6);
            }
         }
      }

      if(var1.thePlayer != null) {
         var2 = PlayerDataHandler.b(var1.thePlayer.username);
         this.a(var2);
         int var3 = (int)(20.0D * var2.e().d());
         if(this.r++ > 0) {
            for(int var4 = 0; var4 < this.e.j.length; ++var4) {
               this.e.j[var4] = 0;
            }

            if(var3 <= 5) {
               Random var7 = new Random();

               for(int var5 = 0; var5 < this.e.j.length; ++var5) {
                  this.e.j[var5] = var7.nextInt(2);
               }
            }

            this.r = 0;
         }

         if(this.s > 0) {
            --this.s;
         }

         if(this.t > 0) {
            --this.t;
         }
      }

      if(!ClientProxy.a) {
         if(this.o == null) {
            if(this.n.size() > 0) {
               this.o = (ClientNotification)this.n.get(0);
               this.n.remove(0);
            }
         } else {
            this.o.a();
            if(this.o.c <= 0.0D) {
               this.o = null;
            }
         }
      }

   }

   private void b(Minecraft var1) {
      if(var1.theWorld != null && var1.thePlayer != null) {
         EntityClientPlayerMP var2 = var1.thePlayer;
         WorldClient var3 = var1.theWorld;
         ItemStack var4 = var2.getCurrentEquippedItem();
         PlayerData var5 = PlayerDataHandler.b(var2.username);
         this.e.c().a(var2);
         if(var5.w > 0.0D) {
            var5.w -= 0.10000000149011612D;
         }

         if(var5.c && (var4 == null || !(var4.getItem() instanceof ItemGun))) {
            var5.c = false;
         }

         if(!var2.isSprinting() && Keyboard.isKeyDown(KeyBindingManager.c.keyCode)) {
            var2.setSprinting(true);
         }

         if(KeyBindingManager.i.isPressed()) {
            this.e.i = !this.e.i;
         }

         if(KeyBindingManager.f.isPressed() && var5.d().a("backpack") != null) {
            PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.c));
         }

         if(KeyBindingManager.g.isPressed() && var5.d().a("vest") != null) {
            PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.d));
         }

         if(KeyBindingManager.e.isPressed() && this.t == 0) {
            PacketDispatcher.sendPacketToServer(CDAPacketSwitchItem.a(false));
            this.t = 16;
         }

         if(KeyBindingManager.d.isPressed() && this.s == 0) {
            PacketDispatcher.sendPacketToServer(CDAPacketSwitchItem.a(true));
            this.s = 20;
         }

         MapManager var6 = CraftingDead.l().k();
         if(this.p.currentScreen == null && var6.z) {
            if(KeyBindingManager.l.isPressed()) {
               var6.U.a(1);
            } else if(KeyBindingManager.j.isPressed()) {
               var1.displayGuiScreen(new GuiMap(var6));
            } else if(KeyBindingManager.k.isPressed()) {
               String var7 = var6.V.a();
               if(var7.equals("none")) {
                  var7 = "group";
               }

               this.p.displayGuiScreen(new GuiMapMarkerDialog((GuiScreen)null, var6.V, "", var7, var6.G, var6.H, var6.I, var6.K));
            } else if(KeyBindingManager.m.isPressed()) {
               var6.V.d();
               var6.V.c();
               this.p.thePlayer.addChatMessage("group " + var6.V.a() + " selected");
            } else if(KeyBindingManager.n.isPressed()) {
               Marker var15 = var6.V.a(var6.G, var6.I, var6.J);
               if(var15 != null) {
                  var6.a(var15);
               }
            } else if(KeyBindingManager.o.isPressed()) {
               var6.U.g.b(-1);
            } else if(KeyBindingManager.p.isPressed()) {
               var6.U.g.b(1);
            }
         }

         if(var3.isRemote && var4 != null && var2.isUsingItem() && var4.getItem() instanceof ItemBlueprint) {
            ((ItemBlueprint)var4.getItem()).d().a(var1.theWorld, var1.thePlayer, var4, var1.thePlayer.getItemInUseDuration());
         }

         if(this.h != var2.inventory.currentItem) {
            if(var4 != null && var4.getItem() instanceof ItemGun) {
               AnimationManager.d().b();
               AnimationManager.d().a((GunAnimation)(new GunAnimationPulled()));
            }

            this.h = var2.inventory.currentItem;
         }

         if(this.q++ > 2) {
            PacketDispatcher.sendPacketToServer(CDAPacketPlayerDataToServer.b());
            this.q = 0;
         }

         if(var4 != null && var4.getItem() instanceof IItemMovementPenalty) {
            IItemMovementPenalty var16 = (IItemMovementPenalty)var4.getItem();
            double var8 = var16.c(var4);
            double var10 = 1.0D - var8;
            if(var8 > 0.0D && var16.a(var2, var4)) {
               if(var2.onGround) {
                  var2.motionX *= var10;
                  var2.motionZ *= var10;
               } else {
                  var2.motionX *= 1.0D - var8 / 3.0D;
                  var2.motionZ *= 1.0D - var8 / 3.0D;
               }
            }
         }

         if(var5 != null && var5.d().a("gun") != null) {
            ItemStack var18 = var5.d().a("gun");
            if(var18.getItem() instanceof IItemMovementPenalty) {
               IItemMovementPenalty var17 = (IItemMovementPenalty)var18.getItem();
               double var9 = var17.c(var18);
               double var11 = 1.0D - var9 / 3.0D;
               double var13 = 1.0D - var9 / 5.0D;
               if(var9 > 0.0D && var17.a(var2, var18)) {
                  if(var2.onGround) {
                     var2.motionX *= var11;
                     var2.motionZ *= var11;
                  } else {
                     var2.motionX *= var13;
                     var2.motionZ *= var13;
                  }
               }
            }
         }

         this.a();
         this.b();
      }

   }

   public void a(PlayerData var1) {
      ItemStack var2 = var1.d().a("hat");
      if(var2 != null && var2.itemID == ItemManager.fA.itemID) {
         if(KeyBindingManager.h.isPressed()) {
            var1.l = !var1.l;
         }
      } else {
         var1.l = false;
      }

      float var3 = 3.5F;
      if(this.p.currentScreen == null && this.p.inGameHasFocus) {
         if(var1.l) {
            if(this.p.gameSettings.gammaSetting < var3) {
               this.g = this.p.gameSettings.gammaSetting;
               this.p.gameSettings.gammaSetting = var3;
            }
         } else {
            if(this.p.gameSettings.gammaSetting == var3) {
               this.p.gameSettings.gammaSetting = this.g;
            }

            if(this.p.gameSettings.gammaSetting > 1.0F) {
               this.p.gameSettings.gammaSetting = 0.1F;
            }
         }

      } else {
         if(this.p.gameSettings.gammaSetting == var3) {
            this.p.gameSettings.gammaSetting = this.g;
         }

      }
   }

   public void a() {
      EntityClientPlayerMP var1 = Minecraft.getMinecraft().thePlayer;
      PlayerData var2 = PlayerDataHandler.a((EntityPlayer)var1);
      if(var1.getCurrentEquippedItem() != null && var1.getCurrentEquippedItem().getItem() instanceof ItemGun) {
         ItemGun var3 = (ItemGun)var1.getCurrentEquippedItem().getItem();
         var2.f = var3.G();
      } else {
         var2.f = 0.1F;
      }

      if(var2.e < var2.f) {
         var2.e = var2.f;
      }

      if(var1.isSprinting()) {
         var2.a(0.6F);
      } else if(!var1.onGround) {
         var2.a(0.7F);
      }

      if(var2.e > var2.f) {
         if(var1.isSneaking()) {
            var2.e -= 0.65F;
         } else {
            var2.e -= 0.2F;
         }

         if(var2.e < var2.f) {
            var2.e = var2.f;
         }
      }

   }

   public void b() {
      float var1 = 0.15F;
      Random var2 = new Random();
      if(this.a > 0.0F) {
         float var3 = this.a + (0.5F - var2.nextFloat());
         switch(var2.nextInt(3)) {
         case 0:
            this.b += var3;
            break;
         case 1:
            this.d += var3;
            break;
         case 2:
            this.c += var3;
         }

         this.b += var3 * 0.5F;
         this.a = 0.0F;
      }

      if(this.b > 0.0F) {
         this.b *= var1;
      }

      this.p.thePlayer.rotationPitch -= this.b;
      if(this.c > 0.0F) {
         this.c *= var1;
      }

      this.p.thePlayer.rotationYaw += this.c;
      if(this.d > 0.0F) {
         this.d *= var1;
      }

      this.p.thePlayer.rotationYaw -= this.d;
   }

   public boolean c() {
      for(int var1 = 0; var1 < this.p.thePlayer.inventory.mainInventory.length; ++var1) {
         if(this.p.thePlayer.inventory.mainInventory[var1] != null) {
            return true;
         }
      }

      return false;
   }

   private void a(Minecraft var1, GuiScreen var2) {
      if(var2 instanceof GuiContainerCreative) {
         GuiContainerCreative var3 = (GuiContainerCreative)var2;
         if(var3.getCurrentTabIndex() == 11) {
            try {
               Field var4 = null;

               try {
                  var4 = GuiContainerCreative.class.getDeclaredField("selectedTabIndex");
               } catch (Exception var9) {
                  ;
               }

               if(var4 == null) {
                  try {
                     var4 = GuiContainerCreative.class.getDeclaredField("field_74241_p");
                  } catch (Exception var8) {
                     ;
                  }
               }

               var4.setAccessible(true);

               try {
                  var4.setInt((Object)null, 0);
                  PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.a));
               } catch (IllegalArgumentException var6) {
                  var6.printStackTrace();
               } catch (IllegalAccessException var7) {
                  var7.printStackTrace();
               }
            } catch (SecurityException var10) {
               var10.printStackTrace();
            }
         }
      }

   }

   public void a(float var1) {
      Minecraft var2 = Minecraft.getMinecraft();
      ItemStack var3 = var2.thePlayer.inventory.getCurrentItem();
      if(var2.thePlayer.isSneaking()) {
         var1 *= 0.8F;
      }

      if(var3 != null && var3.getItem() instanceof ItemGun) {
         this.a = var1;
      }

   }

   public void a(EnumFireMode var1) {
      this.e.a(var1);
   }

   public EnumSet ticks() {
      return EnumSet.of(TickType.RENDER, TickType.CLIENT, TickType.PLAYER);
   }

   public String getLabel() {
      return "cdaclienttick";
   }

   public void a(ClientNotification var1) {
      if(this.n != null && var1 != null) {
         this.n.add(var1);
      }

   }
}
