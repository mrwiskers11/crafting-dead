package com.craftingdead.client;

import com.craftingdead.CraftingDead;
import com.craftingdead.IProxy;
import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBarrier1;
import com.craftingdead.block.tileentity.TileEntityBarrier2;
import com.craftingdead.block.tileentity.TileEntityBarrier3;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.block.tileentity.TileEntityCampfire;
import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.block.tileentity.TileEntityGunRack;
import com.craftingdead.block.tileentity.TileEntityLoot;
import com.craftingdead.block.tileentity.TileEntityMilitarySign;
import com.craftingdead.block.tileentity.TileEntityRoadBlock;
import com.craftingdead.block.tileentity.TileEntitySandBarrier;
import com.craftingdead.block.tileentity.TileEntityShelfEmpty;
import com.craftingdead.block.tileentity.TileEntityShelfLoot;
import com.craftingdead.block.tileentity.TileEntityStopSign;
import com.craftingdead.block.tileentity.TileEntityTrafficCone;
import com.craftingdead.block.tileentity.TileEntityTrafficPole;
import com.craftingdead.block.tileentity.TileEntityTrashCan;
import com.craftingdead.block.tileentity.TileEntityVendingMachine;
import com.craftingdead.block.tileentity.TileEntityWaterPump;
import com.craftingdead.client.ClientEvents;
import com.craftingdead.client.ClientGunTicker;
import com.craftingdead.client.ClientTickHandler;
import com.craftingdead.client.FakePlayerManager;
import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.client.SoundHandler;
import com.craftingdead.client.gui.widget.team.TeamMember;
import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.MapConnectionHandler;
import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.api.MwAPI;
import com.craftingdead.client.mapwriter.overlay.OverlayGrid;
import com.craftingdead.client.mapwriter.overlay.OverlaySlime;
import com.craftingdead.client.render.RenderBlockHandler;
import com.craftingdead.client.render.RenderC4;
import com.craftingdead.client.render.RenderCDZombie;
import com.craftingdead.client.render.RenderCorpse;
import com.craftingdead.client.render.RenderEntityHeadHitbox;
import com.craftingdead.client.render.RenderFlameThrowerFire;
import com.craftingdead.client.render.RenderFlamethrower;
import com.craftingdead.client.render.RenderGrenade;
import com.craftingdead.client.render.RenderGrenadeFlash;
import com.craftingdead.client.render.RenderGrenadeGas;
import com.craftingdead.client.render.RenderGrenadeSmoke;
import com.craftingdead.client.render.RenderGroundItem;
import com.craftingdead.client.render.RenderHandDrill;
import com.craftingdead.client.render.RenderLootcrate;
import com.craftingdead.client.render.RenderSupplyDrop;
import com.craftingdead.client.render.tileentity.RenderTileEntityBarrier1;
import com.craftingdead.client.render.tileentity.RenderTileEntityBarrier2;
import com.craftingdead.client.render.tileentity.RenderTileEntityBarrier3;
import com.craftingdead.client.render.tileentity.RenderTileEntityBaseCenter;
import com.craftingdead.client.render.tileentity.RenderTileEntityCampfire;
import com.craftingdead.client.render.tileentity.RenderTileEntityForge;
import com.craftingdead.client.render.tileentity.RenderTileEntityGunRack;
import com.craftingdead.client.render.tileentity.RenderTileEntityLoot;
import com.craftingdead.client.render.tileentity.RenderTileEntityRoadBlock;
import com.craftingdead.client.render.tileentity.RenderTileEntitySandBarrier;
import com.craftingdead.client.render.tileentity.RenderTileEntityShelfEmpty;
import com.craftingdead.client.render.tileentity.RenderTileEntityShelfLoot;
import com.craftingdead.client.render.tileentity.RenderTileEntitySign;
import com.craftingdead.client.render.tileentity.RenderTileEntityStopSign;
import com.craftingdead.client.render.tileentity.RenderTileEntityTrafficCone;
import com.craftingdead.client.render.tileentity.RenderTileEntityTrafficPole;
import com.craftingdead.client.render.tileentity.RenderTileEntityTrashCan;
import com.craftingdead.client.render.tileentity.RenderTileEntityVendingMachine;
import com.craftingdead.client.render.tileentity.RenderTitleEntityWaterPump;
import com.craftingdead.client.utils.ClientSettings;
import com.craftingdead.client.utils.RemoteSettings;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.entity.EntityC4;
import com.craftingdead.entity.EntityCDZombie;
import com.craftingdead.entity.EntityCorpse;
import com.craftingdead.entity.EntityFlameThrowerFire;
import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.entity.EntityGrenadeDecoy;
import com.craftingdead.entity.EntityGrenadeFire;
import com.craftingdead.entity.EntityGrenadeFlash;
import com.craftingdead.entity.EntityGrenadeGas;
import com.craftingdead.entity.EntityGrenadePipeBomb;
import com.craftingdead.entity.EntityGrenadeSmoke;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.entity.EntityPlayerHead;
import com.craftingdead.entity.EntitySupplyDrop;
import com.craftingdead.item.ItemManager;
import com.craftingdead.mojang.MojangAPI;
import com.craftingdead.utils.FileHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.KeyBindingRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLDummyContainer;
import cpw.mods.fml.common.InjectedModContainer;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.MCPDummyContainer;
import cpw.mods.fml.common.MinecraftDummyContainer;
import cpw.mods.fml.common.ModContainer;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.resources.ResourcePackRepositoryEntry;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.ForgeDummyContainer;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.opengl.Display;

public class ClientProxy implements IProxy {

   private static final JsonParser b = new JsonParser();
   private static final ImmutableList c = (new Builder()).add(MinecraftDummyContainer.class).add(FMLDummyContainer.class).add(ForgeDummyContainer.class).add(MCPDummyContainer.class).build();
   private static final boolean d = true;
   private Minecraft e;
   private SoundHandler f;
   private ClientEvents g;
   private ClientGunTicker h;
   private ClientTickHandler i;
   private CMClient j;
   private ClientSettings k;
   private RemoteSettings l;
   private FakePlayerManager m;
   public static boolean a = true;
   private MapManager n;
   private List o = new ArrayList();
   private File p;
   private File q;
   private List r = new ArrayList();


   public void a(FMLPreInitializationEvent var1) {
      this.e = Minecraft.getMinecraft();
      Display.setTitle("Crafting Dead Origins 1.2.5");

      try {
         Display.setIcon(new ByteBuffer[]{a(this.e.getResourceManager().getResource(new ResourceLocation("craftingdead", "textures/gui/icon_16x16.png")).getInputStream()), a(this.e.getResourceManager().getResource(new ResourceLocation("craftingdead", "textures/gui/icon_32x32.png")).getInputStream())});
      } catch (IOException var3) {
         CraftingDead.d.log(Level.WARNING, "Failed to set display icon", var3);
      }

      this.e.gameSettings.guiScale = 3;
      this.k = new ClientSettings(CraftingDead.f.k());
      try {
		this.l = new RemoteSettings(CraftingDead.f.k());
	} catch (Throwable e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
      this.i = new ClientTickHandler();
      TickRegistry.registerTickHandler(this.i, Side.CLIENT);
      this.m = new FakePlayerManager();
      this.h = new ClientGunTicker(this.e);
      this.h.start();
      this.j = new CMClient(this.l.c());
      KeyBindingRegistry.registerKeyBinding(new KeyBindingManager());
      this.g = new ClientEvents(this);
      MinecraftForge.EVENT_BUS.register(this.g);
      this.p = new File(this.e.getResourcePackRepository().getDirResourcepacks(), "craftingdead.zip");
      try {
		FileHelper.a(this.p, this.l.e(), this.l.f());
	} catch (NoSuchAlgorithmException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
      this.n();
      this.q = new File(CraftingDead.f.k(), "team.json");
      this.o();
      MojangAPI.getUUID(this.e.getSession().getUsername(), true);
   }

   public void a(FMLInitializationEvent var1) {
      this.n = new MapManager(new MapConfig(new File(CraftingDead.f.k(), "map.config")));
      MinecraftForge.EVENT_BUS.register(this.n);
      NetworkRegistry.instance().registerConnectionHandler(new MapConnectionHandler(this.n));
   }

   public void a(FMLPostInitializationEvent var1) {
      this.f = new SoundHandler();
      MinecraftForge.EVENT_BUS.register(this.f);
      BlockManager.a = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.b = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.c = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.d = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.e = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.f = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.g = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.h = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.i = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.j = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.k = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.l = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.m = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.o = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.p = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.n = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.r = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.q = RenderingRegistry.getNextAvailableRenderId();
      BlockManager.s = RenderingRegistry.getNextAvailableRenderId();
      RenderBlockHandler var2 = new RenderBlockHandler();
      RenderingRegistry.registerBlockHandler(BlockManager.a, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.b, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.c, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.d, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.e, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.f, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.g, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.h, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.i, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.j, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.k, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.l, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.m, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.o, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.p, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.n, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.q, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.r, var2);
      RenderingRegistry.registerBlockHandler(BlockManager.s, var2);
      MinecraftForgeClient.registerItemRenderer(ItemManager.eW.itemID, new RenderLootcrate("alpha"));
      MinecraftForgeClient.registerItemRenderer(ItemManager.eX.itemID, new RenderLootcrate("bravo"));
      MinecraftForgeClient.registerItemRenderer(ItemManager.eY.itemID, new RenderLootcrate("charlie"));
      MinecraftForgeClient.registerItemRenderer(ItemManager.fc.itemID, new RenderFlamethrower());
      MinecraftForgeClient.registerItemRenderer(ItemManager.fn.itemID, new RenderHandDrill());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityLoot.class, new RenderTileEntityLoot());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBaseCenter.class, new RenderTileEntityBaseCenter());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCampfire.class, new RenderTileEntityCampfire());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySandBarrier.class, new RenderTileEntitySandBarrier());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWaterPump.class, new RenderTitleEntityWaterPump());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGunRack.class, new RenderTileEntityGunRack());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMilitarySign.class, new RenderTileEntitySign());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBarrier1.class, new RenderTileEntityBarrier1());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBarrier2.class, new RenderTileEntityBarrier2());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBarrier3.class, new RenderTileEntityBarrier3());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrafficPole.class, new RenderTileEntityTrafficPole());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrafficCone.class, new RenderTileEntityTrafficCone());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityStopSign.class, new RenderTileEntityStopSign());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrashCan.class, new RenderTileEntityTrashCan());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityVendingMachine.class, new RenderTileEntityVendingMachine());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRoadBlock.class, new RenderTileEntityRoadBlock());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityShelfEmpty.class, new RenderTileEntityShelfEmpty());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityShelfLoot.class, new RenderTileEntityShelfLoot());
      ClientRegistry.bindTileEntitySpecialRenderer(TileEntityForge.class, new RenderTileEntityForge());
      RenderingRegistry.registerEntityRenderingHandler(EntityCDZombie.class, new RenderCDZombie(new ModelBiped(), 0.4F));
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenade.class, new RenderGrenade());
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenadeFlash.class, new RenderGrenadeFlash());
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenadeDecoy.class, new RenderGrenadeFlash("grenadedecoy"));
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenadeSmoke.class, new RenderGrenadeSmoke());
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenadeGas.class, new RenderGrenadeGas());
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenadeFire.class, new RenderGrenadeSmoke("grenadefire"));
      RenderingRegistry.registerEntityRenderingHandler(EntityGrenadePipeBomb.class, new RenderGrenadeSmoke("grenadepipe"));
      RenderingRegistry.registerEntityRenderingHandler(EntityC4.class, new RenderC4());
      RenderingRegistry.registerEntityRenderingHandler(EntityCorpse.class, new RenderCorpse());
      RenderingRegistry.registerEntityRenderingHandler(EntityPlayerHead.class, new RenderEntityHeadHitbox());
      RenderingRegistry.registerEntityRenderingHandler(EntityGroundItem.class, new RenderGroundItem());
      RenderingRegistry.registerEntityRenderingHandler(EntityFlameThrowerFire.class, new RenderFlameThrowerFire());
      RenderingRegistry.registerEntityRenderingHandler(EntitySupplyDrop.class, new RenderSupplyDrop());
      MwAPI.a("Slime", new OverlaySlime());
      MwAPI.a("Grid", new OverlayGrid());
      List var3 = this.b();
      if(var3.size() > 0) {
         String var4 = "Game contains one or more third party mods\n" + var3.toString();
         CraftingDead.d.warning(var4);
         JOptionPane.showMessageDialog((Component)null, var4, "Error", 0);
         System.exit(0);
         //TODO: Remove mod detection with an empty if block
      }

   }

   public void a() {
      this.f.b().a();
      this.k.b();
      if(this.h != null) {
         this.h.a();
         this.h.interrupt();
         this.h = null;
      }

   }

   private void o() {
      try {
		FileHelper.a(this.q, this.l.g(), this.l.h());
	} catch (NoSuchAlgorithmException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

      try {
         JsonArray var1 = (JsonArray)b.parse(new FileReader(this.q));
         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            Object var3 = var2.next();

            try {
               JsonObject var4 = (JsonObject)var3;
               String var5 = var4.get("name").getAsString();
               TeamMember.a var6 = TeamMember.a.valueOf(var4.get("rank").getAsString());
               String var7 = var4.get("bio").getAsString();
               this.r.add(new TeamMember(var5, var6, var7));
            } catch (Exception var8) {
               CraftingDead.d.log(Level.WARNING, "An error exists with one of the elements in the team file", var8);
            }
         }
      } catch (IOException var9) {
         CraftingDead.d.log(Level.WARNING, "Could not read team file", var9);
      }

   }

   public List b() {
      ArrayList var1 = new ArrayList();
      Iterator var2 = Loader.instance().getModList().iterator();

      while(var2.hasNext()) {
         ModContainer var3 = (ModContainer)var2.next();
         if(var3 instanceof InjectedModContainer) {
            InjectedModContainer var4 = (InjectedModContainer)var3;
            var3 = var4.wrappedContainer;
         }

         if(!c.contains(var3.getClass()) && !var3.getModId().equals("craftingdead")) {
            CraftingDead.d.warning("Found third party mod container: " + var3.getName());
            var1.add(var3.getModId());
         }
      }

      return var1;
   }

   public boolean c() {
      return this.h().j().g();
   }

   public ClientTickHandler d() {
      return this.i;
   }

   public ClientSettings e() {
      return this.k;
   }

   public RemoteSettings f() {
      return this.l;
   }

   public FakePlayerManager g() {
      return this.m;
   }

   public CMClient h() {
      return this.j;
   }

   public ClientEvents i() {
      return this.g;
   }

   public SoundHandler j() {
      return this.f;
   }

   public MapManager k() {
      return this.n;
   }

   public List l() {
      return this.o;
   }

   public List m() {
      return this.r;
   }

   private static ByteBuffer a(InputStream var0) throws IOException {
      BufferedImage var1 = ImageIO.read(var0);
      int[] var2 = var1.getRGB(0, 0, var1.getWidth(), var1.getHeight(), (int[])null, 0, var1.getWidth());
      ByteBuffer var3 = ByteBuffer.allocate(4 * var2.length);
      int[] var4 = var2;
      int var5 = var2.length;

      for(int var6 = 0; var6 < var5; ++var6) {
         int var7 = var4[var6];
         var3.putInt(var7 << 8 | var7 >> 24 & 255);
      }

      var3.flip();
      return var3;
   }

   public void n() {
      this.e.getResourcePackRepository().updateRepositoryEntriesAll();
      if(this.k.j) {
         if(!this.a(this.p.getName())) {
            this.a("Default");
         }
      } else {
         this.a("Default");
      }

   }

   private boolean a(String var1) {
      try {
         List var2 = this.e.getResourcePackRepository().getRepositoryEntriesAll();
         if(var1.equalsIgnoreCase("Default")) {
            this.e.getResourcePackRepository().setRepositoryEntries(new ResourcePackRepositoryEntry[0]);
            this.e.gameSettings.skin = "Default";
         } else {
            int var3 = 0;

            for(Iterator var4 = var2.iterator(); var4.hasNext(); ++var3) {
               ResourcePackRepositoryEntry var5 = (ResourcePackRepositoryEntry)var4.next();
               if(var5.getResourcePackName().equalsIgnoreCase(var1)) {
                  break;
               }
            }

            this.e.getResourcePackRepository().setRepositoryEntries(new ResourcePackRepositoryEntry[]{(ResourcePackRepositoryEntry)var2.get(var3)});
            this.e.gameSettings.skin = ((ResourcePackRepositoryEntry)var2.get(var3)).getResourcePackName();
         }

         this.e.refreshResources();
         this.e.gameSettings.saveOptions();
         return true;
      } catch (Exception var6) {
         CraftingDead.d.log(Level.WARNING, "Could not enable resource pack: " + var1, var6);
         return false;
      }
   }

}
