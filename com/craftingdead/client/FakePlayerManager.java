package com.craftingdead.client;

import com.craftingdead.client.fake.FakeCDPlayerFactory;
import com.craftingdead.event.EventGUIPlayerGenerated;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;

public class FakePlayerManager {

   private EntityPlayer a;
   private FakeCDPlayerFactory b;


   public void a() {
      this.b = null;
      this.a = null;
   }

   public EntityPlayer b() {
      if(this.b == null) {
         this.b = new FakeCDPlayerFactory();
      }

      if(this.a == null) {
         this.a = this.b.a();
         EventGUIPlayerGenerated var1 = new EventGUIPlayerGenerated(this.a);
         MinecraftForge.EVENT_BUS.post(var1);
      }

      return this.a;
   }
}
