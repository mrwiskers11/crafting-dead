package com.craftingdead.client;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.opengl.GL11;

public class ClientNotification {

   public String[] a;
   public String b;
   public double c = 100.0D;
   private double d = -35.0D;
   private double e = 1.75D;


   public ClientNotification(String var1) {
      this.a = this.c(var1);
      this.b = "Ferullo Gaming";
   }

   public ClientNotification(String var1, String var2) {
      this.a = new String[2];
      this.a[0] = var1;
      this.a[1] = var2;
      this.b = "Ferullo Gaming";
   }

   public ClientNotification a(String var1) {
      this.b = var1;
      return this;
   }

   public void a() {
      if(this.d < 0.0D) {
         this.d += this.e;
      }

      if(this.d > 0.0D) {
         this.d = 0.0D;
      }

      if(this.c > 0.0D) {
         --this.c;
      }

   }

   public void a(Minecraft var1) {
      short var2 = 160;
      CDRenderHelper.a(0.0D, (double)((int)this.d), (double)var2, 30.0D, 2500134, 1.0F);
      CDRenderHelper.a(0.0D, (double)((int)this.d + 30), (double)var2, 10.0D, 4802889, 1.0F);
      CDRenderHelper.a(EnumChatFormatting.WHITE + this.a[0].trim(), 4, (int)this.d + 4);
      CDRenderHelper.a(EnumChatFormatting.WHITE + this.a[1].trim(), 4, (int)this.d + 14);
      CDRenderHelper.a(EnumChatFormatting.YELLOW + this.b, 4, (int)this.d + 32, 0.75D);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   private String[] c(String var1) {
      int var2 = Minecraft.getMinecraft().fontRenderer.getStringWidth(var1);
      String[] var3 = new String[]{"", ""};
      byte var4 = 0;
      short var5 = 160;
      if(var2 > var5) {
         String[] var6 = var1.split(" ");

         for(int var7 = 0; var7 < var6.length; ++var7) {
            String var8 = var3[var4] + " " + var6[var7];
            if(Minecraft.getMinecraft().fontRenderer.getStringWidth(var8) < var5) {
               var3[var4] = var3[var4] + " " + var6[var7];
            } else if(var4 != 1) {
               var4 = 1;
               var3[var4] = var3[var4] + " " + var6[var7];
            }
         }
      } else {
         var3[0] = var1;
      }

      return var3;
   }

   public static void b(String var0) {
      ClientNotification var1 = new ClientNotification(var0);
      CraftingDead.l().d().n.add(var1);
   }
}
