package com.craftingdead.client.fake;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.world.World;

public class FakeCDPlayer extends AbstractClientPlayer {

   public FakeCDPlayer(World var1, String var2) {
      super(var1, var2);
   }

   public void b(String var1) {}

   public boolean canCommandSenderUseCommand(int var1, String var2) {
      return false;
   }

   public ChunkCoordinates getPlayerCoordinates() {
      return new ChunkCoordinates(0, 0, 0);
   }

   public void sendChatToPlayer(ChatMessageComponent var1) {}
}
