package com.craftingdead.client.fake;

import com.craftingdead.client.fake.FakeCDWorld;

import net.minecraft.world.EnumGameType;

public final class FakeCDWorldFactory {

   private static final FakeCDWorld a = new FakeCDWorld(EnumGameType.SURVIVAL);


   public static FakeCDWorld a() {
      return a;
   }

}
