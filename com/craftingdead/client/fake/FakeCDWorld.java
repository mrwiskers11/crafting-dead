package com.craftingdead.client.fake;

import net.minecraft.profiler.*;
import net.minecraft.logging.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.storage.*;
import net.minecraft.world.*;
import net.minecraft.village.*;
import net.minecraft.block.*;
import net.minecraft.entity.*;
import net.minecraft.world.chunk.*;
import net.minecraft.world.chunk.storage.IChunkLoader;

import java.io.File;
import java.util.*;
import java.util.logging.Logger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.*;

public class FakeCDWorld extends World {
    private int[][][] a;
    private int[][][] b;
    private int c = 3;
    private boolean H = false;
    private final List<Entity> J = new ArrayList(5);
    private static final Profiler K = new Profiler();
    private static final ILogAgent L = new b();

    public FakeCDWorld(EnumGameType enumGameType) {
        super((ISaveHandler)new d(), "pseudo", (WorldProvider)new c(), new WorldSettings(0L, enumGameType, false, false, WorldType.DEFAULT), K, L);
        this.b(this.a());
        this.chunkProvider = new a(this);
        this.villageCollectionObj = new VillageCollection((World)this);
        this.provider.registerWorld((World)this);
        this.chunkProvider = this.createChunkProvider();
        this.calculateInitialSkylight();
        this.provider.calculateInitialWeather();
    }

    public int[] a() {
        int[] arrn = new int[27];
        for (int i = 0; i < arrn.length; ++i) {
            arrn[i] = i < 18 ? 0 : (i < 9 ? Block.grass.blockID : Block.dirt.blockID);
        }
        return arrn;
    }

    public /* varargs */ int[] a(int ... arrn) {
        int[] arrn2 = new int[(int)Math.pow((double)arrn.length, (double)3.0)];
        int n = -1;
        for (int n2 : arrn) {
            for (int i = 0; i < arrn.length; ++i) {
                for (int j = 0; j < arrn.length; ++j) {
                    arrn2[n++] = arrn[n2];
                }
            }
        }
        return arrn2;
    }

    public void b(boolean bl) {
        this.H = bl;
    }

    public void a(int[] arrn, int[] arrn2) {
        try {
            if (arrn.length != arrn2.length) {
                throw new Throwable("Id and meta arrays must be equivalent in length");
            }
            for (int i = 0; i < arrn.length; ++i) {
                if (arrn[i] < 0 || i >= Block.blocksList.length) {
                    throw new Throwable("All Id values must be within range of 0 to " + Block.blocksList.length);
                }
                if (arrn2[i] <= 16 && i >= 0) continue;
                throw new Throwable("All meta values must be within the range of 0 to 15");
            }
            double d2 = Math.cbrt((double)arrn.length);
            int n = this.c;
            this.c = (int)d2;
            if (d2 != (double)this.c && (d2 != (double)this.c || this.c == 0)) {
                this.c = n;
                throw new Throwable("Block Return arrays must be a round, cubic this.factor");
            }
            int n2 = 0;
            this.a = new int[this.c][this.c][this.c];
            this.b = new int[this.c][this.c][this.c];
            for (int i = 0; i < this.c; ++i) {
                for (int j = 0; j < this.c; ++j) {
                    for (int k = 0; k < this.c; ++k) {
                        this.a[i][k][j] = arrn[n2];
                        this.b[i][k][j] = arrn2[n2];
                        ++n2;
                    }
                }
            }
        }
        catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void b(int[] arrn) {
        int[] arrn2 = new int[arrn.length];
        for (int i = 0; i < arrn.length; ++i) {
            arrn2[0] = 0;
        }
        this.a(arrn, arrn2);
    }

    public void d() {
        this.J.clear();
    }

    public List<Entity> e() {
        List<Entity> list = this.J;
        this.d();
        return list;
    }

    public Entity i() {
        Entity entity = null;
        if (!this.J.isEmpty()) {
            entity = (Entity)this.J.get(0);
            this.J.remove(0);
        }
        return entity;
    }

    public void tickBlocksAndAmbiance() {
        for (int i = 0; i < this.c; ++i) {
            for (int j = 0; j < this.c; ++j) {
                for (int k = 0; k < this.c; ++k) {
                    Block.blocksList[this.a[k][i][j]].updateTick((World)this, i, k, j, this.rand);
                }
            }
        }
    }

    public boolean setBlock(int n, int n2, int n3, int n4) {
        if (this.H) {
            this.a[n2 >= this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c] = n4;
        }
        return this.H;
    }

    public boolean setBlock(int n, int n2, int n3, int n4, int n5, int n6) {
        if (this.H) {
            this.a[n2 > this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c] = n4;
            this.b[n2 > this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c] = n5;
        }
        return this.H;
    }

    public boolean setBlockMetadataWithNotify(int n, int n2, int n3, int n4, int n5) {
        if (this.H) {
            this.b[n2 > this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c] = n4;
        }
        return this.H;
    }

    public void tick() {
        this.tickBlocksAndAmbiance();
        this.tickUpdates(true);
        this.updateWeather();
        this.updateEntities();
    }

    public boolean setBlockToAir(int n, int n2, int n3) {
        if (this.H) {
            this.a[n2 > this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c] = 0;
        }
        return this.H;
    }

    public boolean blockExists(int n, int n2, int n3) {
        return this.getBlockId(n, n2, n3) != 0;
    }

    public int getBlockId(int n, int n2, int n3) {
        return this.a[n2 > this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c];
    }

    public int getBlockMetadata(int n, int n2, int n3) {
        return this.a[n2 > this.c ? this.c : (n2 < 0 ? 0 : n2)][n % this.c][n3 % this.c];
    }

    protected IChunkProvider createChunkProvider() {
        return new a(this);
    }

    public Entity getEntityByID(int var1) {
        Iterator var2 = super.loadedEntityList.iterator();

        Entity var3;
        do {
           if(!var2.hasNext()) {
              return null;
           }

           var3 = (Entity)var2.next();
        } while(var3.entityId != var1);

        return var3;
     }


    public boolean spawnEntityInWorld(Entity entity) {
        this.J.add(entity);
        return super.spawnEntityInWorld(entity);
    }

    public ChunkCoordinates getSpawnPoint() {
        return new ChunkCoordinates(0, 0, 0);
    }
    
    static final class a implements IChunkProvider
    {
        private final FakeCDWorld a;
        
        public a(final FakeCDWorld a) {
            this.a = a;
        }
        
        public boolean chunkExists(final int n, final int n2) {
            return true;
        }
        
        public Chunk provideChunk(final int n, final int n2) {
            return new Chunk((World)this.a, 0, 0);
        }
        
        public Chunk loadChunk(final int n, final int n2) {
            return new Chunk((World)this.a, 0, 0);
        }
        
        public void populate(final IChunkProvider chunkProvider, final int n, final int n2) {
        }
        
        public boolean saveChunks(final boolean b, final IProgressUpdate progressUpdate) {
            return false;
        }
        
        public boolean unloadQueuedChunks() {
            return true;
        }
        
        public boolean canSave() {
            return false;
        }
        
        public String makeString() {
            return "fake world";
        }
        
        public List getPossibleCreatures(final EnumCreatureType enumCreatureType, final int n, final int n2, final int n3) {
            return null;
        }
        
        public ChunkPosition findClosestStructure(final World world, final String s, final int n, final int n2, final int n3) {
            return new ChunkPosition(0, 0, 0);
        }
        
        public int getLoadedChunkCount() {
            return 1;
        }
        
        public void recreateStructures(final int n, final int n2) {
        }
        
        public void saveExtraData() {
        }
    }
    
    static final class b implements ILogAgent
    {
        public void logInfo(final String s) {
        }
        
        @SideOnly(Side.SERVER)
        public Logger func_120013_a() {
            return FMLCommonHandler.instance().getFMLLogger();
        }
        
        public void logWarning(final String s) {
        }
        
        public void logWarningFormatted(final String s, final Object... array) {
        }
        
        public void logWarningException(final String s, final Throwable t) {
        }
        
        public void logSevere(final String s) {
        }
        
        public void logSevereException(final String s, final Throwable t) {
        }
        
        @SideOnly(Side.CLIENT)
        public void logFine(final String s) {
        }
    }

    static final class c extends WorldProvider
    {
        public String getDimensionName() {
            return "pseudo";
        }
    }

    static final class d implements ISaveHandler
    {
        public WorldInfo loadWorldInfo() {
            return null;
        }
        
        public void checkSessionLock() {
        }
        
        public IChunkLoader getChunkLoader(final WorldProvider worldProvider) {
            return null;
        }
        
		public void saveWorldInfoWithPlayer(WorldInfo worldinfo, NBTTagCompound nbttagcompound) {
        }
        
        public void saveWorldInfo(final WorldInfo worldInfo) {
        }
        
        public IPlayerFileData getSaveHandler() {
            return null;
        }
        
        public void flush() {
        }
        
        public File getMapFileFromName(final String s) {
            return null;
        }
        
        public String getWorldDirectoryName() {
            return "";
        }
    }
    

class innerClass {
}

 

}
