package com.craftingdead.client.fake;

import com.craftingdead.client.fake.FakeCDPlayer;
import com.craftingdead.client.fake.FakeCDWorldFactory;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class FakeCDPlayerFactory {

   private String a = Minecraft.getMinecraft().getSession().getUsername();
   private World b = FakeCDWorldFactory.a();
   private FakeCDPlayer c;


   public FakeCDPlayerFactory() {
      this.c = new FakeCDPlayer(this.b, this.a);
   }

   public EntityPlayer a() {
      return this.c;
   }

   public void a(EntityPlayer var1) {
      this.c = (FakeCDPlayer)var1;
   }
}
