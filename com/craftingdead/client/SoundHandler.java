package com.craftingdead.client;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.animation.AmbienceHandler;
import com.craftingdead.item.gun.ItemGun;

import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.client.audio.SoundManager;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class SoundHandler {

   public static ArrayList a;
   private AmbienceHandler b = new AmbienceHandler();


   public void a() {
      this.b.b();
   }

   public AmbienceHandler b() {
      return this.b;
   }

   public void a(String var1) {
      a.add(var1);
   }

   @ForgeSubscribe
   public void a(SoundLoadEvent var1) {
      a = new ArrayList();
      this.a("gunEmpty");
      this.a("gunFiremode");
      this.a("gunPulled");
      this.a("zoomin");
      this.a("flame");
      this.a("flamestart");
      this.a("flameout");
      this.a("ambient_cd_1");
      this.a("ambient_cd_2");
      this.a("ambient_cd_3");
      this.a("ambient_cd_4");
      this.a("ambient_cd_5");
      this.a("ambient_cd_6");
      this.a("bulletimpact_dirt");
      this.a("bulletimpact_flesh");
      this.a("bulletimpact_flesh_vest");
      this.a("bulletimpact_glass");
      this.a("bulletimpact_metal");
      this.a("bulletimpact_metal2");
      this.a("bulletimpact_stone");
      this.a("bulletimpact_water");
      this.a("bulletimpact_wood");
      this.a("pipebomb_beep");
      this.a("backpack");
      this.a("backpackclose");
      this.a("whistlingone");
      this.a("whistlingtwo");
      this.a("whistlingthree");
      this.a("tradersigh");
      this.a("buy");
      this.a("minigunbarrel");
      this.a("drill");

      for(int var2 = 0; var2 < Item.itemsList.length; ++var2) {
         if(Item.itemsList[var2] != null && Item.itemsList[var2] instanceof ItemGun) {
            ItemGun var3 = (ItemGun)Item.itemsList[var2];
            if(var3.cD != null && var3.cD.length() > 0) {
               this.a(var3.cD.trim().toLowerCase());
            }

            if(var3.cE != null && var3.cE.length() > 0) {
               this.a(var3.cE.trim().toLowerCase());
            }

            if(var3.cF != null && var3.cF.length() > 0) {
               this.a(var3.cF.trim().toLowerCase());
            }
         }
      }

      SoundManager var8 = var1.manager;
      boolean var9 = true;
      Iterator var4 = a.iterator();

      while(var4.hasNext()) {
         String var5 = (String)var4.next();

         try {
            var8.addSound("craftingdead:" + var5 + ".ogg");
         } catch (Exception var7) {
            CraftingDead.d.warning("Failed to load sound: " + var5);
            var9 = false;
         }
      }

      if(!var9) {
         CraftingDead.d.warning("Sound failed to load!");
      }

   }
}
