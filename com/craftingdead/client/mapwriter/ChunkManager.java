package com.craftingdead.client.mapwriter;

import java.util.Iterator;
import java.util.Map.Entry;

import com.craftingdead.client.mapwriter.CircularHashMap;
import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.region.MwChunk;
import com.craftingdead.client.mapwriter.tasks.SaveChunkTask;
import com.craftingdead.client.mapwriter.tasks.UpdateSurfaceChunksTask;

import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;

public class ChunkManager {

   public MapManager a;
   private boolean b = false;
   private CircularHashMap c = new CircularHashMap();
   private static final int d = 1;
   private static final int e = 2;


   public ChunkManager(MapManager var1) {
      this.a = var1;
   }

   public synchronized void a() {
      this.b = true;
      this.b();
      this.c.a();
   }

   public static MwChunk a(Chunk var0) {
      byte[][] var1 = new byte[16][];
      byte[][] var2 = new byte[16][];
      byte[][] var3 = new byte[16][];
      byte[][] var4 = new byte[16][];
      ExtendedBlockStorage[] var5 = var0.getBlockStorageArray();
      if(var5 != null) {
         ExtendedBlockStorage[] var6 = var5;
         int var7 = var5.length;

         for(int var8 = 0; var8 < var7; ++var8) {
            ExtendedBlockStorage var9 = var6[var8];
            if(var9 != null) {
               int var10 = var9.getYLocation() >> 4 & 15;
               var2[var10] = var9.getBlockLSBArray();
               var1[var10] = var9.getBlockMSBArray() != null?var9.getBlockMSBArray().data:null;
               var3[var10] = var9.getMetadataArray() != null?var9.getMetadataArray().data:null;
               var4[var10] = var9.getBlocklightArray() != null?var9.getBlocklightArray().data:null;
            }
         }
      }

      return new MwChunk(var0.xPosition, var0.zPosition, var0.worldObj.provider.dimensionId, var1, var2, var3, var4, var0.getBiomeArray());
   }

   public synchronized void b(Chunk var1) {
      if(!this.b && var1 != null) {
         this.c.a(var1, Integer.valueOf(0));
      }

   }

   public synchronized void c(Chunk var1) {
      if(!this.b && var1 != null) {
         int var2 = ((Integer)this.c.c(var1)).intValue();
         if((var2 & 2) != 0) {
            this.d(var1);
         }

         this.c.a(var1);
      }

   }

   public synchronized void b() {
      Iterator var1 = this.c.e().iterator();

      while(var1.hasNext()) {
         Entry var2 = (Entry)var1.next();
         int var3 = ((Integer)var2.getValue()).intValue();
         if((var3 & 2) != 0) {
            this.d((Chunk)var2.getKey());
         }
      }

   }

   public void c() {
      int var1 = (this.a.G >> 4) - 1;
      int var2 = (this.a.I >> 4) - 1;
      MwChunk[] var3 = new MwChunk[9];

      for(int var4 = 0; var4 < 3; ++var4) {
         for(int var5 = 0; var5 < 3; ++var5) {
            Chunk var6 = this.a.a.theWorld.getChunkFromChunkCoords(var1 + var5, var2 + var4);
            if(!var6.isEmpty()) {
               var3[var4 * 3 + var5] = a(var6);
            }
         }
      }

   }

   public void d() {
      int var1 = Math.min(this.c.b(), this.a.t);
      MwChunk[] var2 = new MwChunk[var1];

      for(int var3 = 0; var3 < var1; ++var3) {
         Entry var4 = this.c.g();
         if(var4 != null) {
            Chunk var5 = (Chunk)var4.getKey();
            int var6 = ((Integer)var4.getValue()).intValue();
            if(MapUtil.a(this.a.G, this.a.I, var5) <= this.a.o) {
               var6 |= 3;
            } else {
               var6 &= -2;
            }

            var4.setValue(Integer.valueOf(var6));
            if((var6 & 1) != 0) {
               var2[var3] = a(var5);
            } else {
               var2[var3] = null;
            }
         }
      }

      this.a.T.a(new UpdateSurfaceChunksTask(this.a, var2));
   }

   public void e() {
      if(!this.b) {
         if((this.a.B & 15) == 0) {
            this.c();
         } else {
            this.d();
         }
      }

   }

   public void a(MwChunk[] var1) {
      this.a.T.a(new UpdateSurfaceChunksTask(this.a, var1));
   }

   private void d(Chunk var1) {
      if((this.a.A && this.a.x || !this.a.A && this.a.w) && !var1.isEmpty()) {
         this.a.T.a(new SaveChunkTask(a(var1), this.a.X));
      }

   }
}
