package com.craftingdead.client.mapwriter.region;

import com.craftingdead.client.mapwriter.region.BlockColours;
import com.craftingdead.client.mapwriter.region.IChunk;

public class ChunkRender {

   public static final byte a = 0;
   public static final byte b = 1;
   public static final byte c = 2;
   public static final double d = 0.35D;
   public static final double e = 0.35D;
   public static final double f = 0.7D;
   public static final double g = 1.4D;


   public static double a(int var0, int var1, int var2) {
      int var3 = 0;
      int var4 = 0;
      if(var1 > 0 && var1 < 255) {
         var4 += var0 - var1;
         ++var3;
      }

      if(var2 > 0 && var2 < 255) {
         var4 += var0 - var2;
         ++var3;
      }

      double var5 = 0.0D;
      if(var3 > 0) {
         var5 = (double)var4 / ((double)var3 * 255.0D);
      }

      return var5 >= 0.0D?Math.pow(var5, 0.35D) * 0.7D:-Math.pow(-var5, 0.35D) * 1.4D;
   }

   public static int a(BlockColours var0, IChunk var1, int var2, int var3, int var4, int var5, int var6) {
      double var7 = 1.0D;
      double var9 = 0.0D;
      double var11 = 0.0D;

      double var13;
      int var17;
      double var20;
      for(var13 = 0.0D; var3 > 0; --var3) {
         int var15 = var1.a(var2, var3, var4);
         int var16 = var0.a(var15);
         var17 = var16 >> 24 & 255;
         if(var17 > 0) {
            int var18 = var1.a(var2, var4);
            int var19 = var0.c(var15, var18);
            var20 = (double)var17 / 255.0D;
            double var22 = (double)(var16 >> 16 & 255) / 255.0D;
            double var24 = (double)(var16 >> 8 & 255) / 255.0D;
            double var26 = (double)(var16 >> 0 & 255) / 255.0D;
            double var28 = (double)(var19 >> 16 & 255) / 255.0D;
            double var30 = (double)(var19 >> 8 & 255) / 255.0D;
            double var32 = (double)(var19 >> 0 & 255) / 255.0D;
            var9 += var7 * var20 * var22 * var28;
            var11 += var7 * var20 * var24 * var30;
            var13 += var7 * var20 * var26 * var32;
            var7 *= 1.0D - var20;
         }

         if(var17 == 255) {
            break;
         }
      }

      double var34 = a(var3, var5, var6);
      var17 = var1.b(var2, var3 + 1, var4);
      double var35 = (double)var17 / 15.0D;
      var20 = (var34 + 1.0D) * var35;
      var9 = Math.min(Math.max(0.0D, var9 * var20), 1.0D);
      var11 = Math.min(Math.max(0.0D, var11 * var20), 1.0D);
      var13 = Math.min(Math.max(0.0D, var13 * var20), 1.0D);
      return (var3 & 255) << 24 | ((int)(var9 * 255.0D) & 255) << 16 | ((int)(var11 * 255.0D) & 255) << 8 | (int)(var13 * 255.0D) & 255;
   }

   static int a(int[] var0, int var1, int var2) {
      return var1 >= var2?var0[var1 - var2] >> 24 & 255:-1;
   }

   static int b(int[] var0, int var1, int var2) {
      return (var1 & var2 - 1) >= 1?var0[var1 - 1] >> 24 & 255:-1;
   }

   public static void a(BlockColours var0, IChunk var1, int[] var2, int var3, int var4, boolean var5) {
      int var6 = var1.a();

      for(int var7 = 0; var7 < 16; ++var7) {
         for(int var8 = 0; var8 < 16; ++var8) {
            int var9 = var6;
            int var10;
            if(var5) {
               while(var9 >= 0) {
                  var10 = var1.a(var8, var9, var7);
                  int var11 = var0.a(var10) >> 24 & 255;
                  if(var11 != 255) {
                     break;
                  }

                  --var9;
               }
            }

            var10 = var3 + var7 * var4 + var8;
            var2[var10] = a(var0, var1, var8, var9, var7, b(var2, var10, var4), a(var2, var10, var4));
         }
      }

   }

   public static void a(BlockColours var0, IChunk var1, int[] var2, int var3, int var4, int var5, byte[] var6) {
      var5 = Math.min(Math.max(0, var5), 255);

      for(int var7 = 0; var7 < 16; ++var7) {
         for(int var8 = 0; var8 < 16; ++var8) {
            if(var6 == null || var6[var7 * 16 + var8] == 1) {
               int var9 = var5;

               int var10;
               for(var10 = var5; var10 < var1.a(); ++var10) {
                  int var11 = var1.a(var8, var10, var7);
                  int var12 = var0.a(var11) >> 24 & 255;
                  if(var12 == 255) {
                     break;
                  }

                  if(var12 > 0) {
                     var9 = var10;
                  }
               }

               var10 = var3 + var7 * var4 + var8;
               var2[var10] = a(var0, var1, var8, var9, var7, b(var2, var10, var4), a(var2, var10, var4));
            }
         }
      }

   }
}
