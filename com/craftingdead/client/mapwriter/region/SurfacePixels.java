package com.craftingdead.client.mapwriter.region;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import javax.imageio.ImageIO;

import com.craftingdead.client.mapwriter.region.ChunkRender;
import com.craftingdead.client.mapwriter.region.MwChunk;
import com.craftingdead.client.mapwriter.region.Region;
import com.craftingdead.client.mapwriter.region.RegionManager;

public class SurfacePixels {

   protected Region a;
   protected File b;
   protected int[] c = null;
   protected boolean d = false;
   protected int e = 0;


   public SurfacePixels(Region var1, File var2) {
      this.a = var1;
      this.b = var2;
   }

   public void a() {
      if(this.c != null) {
         Arrays.fill(this.c, 0);
      }

   }

   public void b() {
      if(this.e > 0) {
         this.e();
      }

      this.c = null;
   }

   private void e() {
      if(this.c != null) {
         a(this.b, this.c, 512, 512);
         this.d = false;
      }

      this.e = 0;
   }

   private void f() {
      if(!this.d) {
         this.c = a(this.b, 512, 512);
         if(this.c != null) {
            for(int var1 = 0; var1 < this.c.length; ++var1) {
               int var2 = this.c[var1];
               if(var2 == -16777216) {
                  this.c[var1] = 0;
               }
            }
         } else {
            this.d = true;
         }

         this.e = 0;
      }

   }

   public int[] c() {
      if(this.c == null) {
         this.f();
      }

      return this.c;
   }

   public int[] d() {
      this.c();
      if(this.c == null) {
         this.c = new int[262144];
         this.a();
      }

      return this.c;
   }

   public void a(MwChunk var1) {
      int var2 = var1.b << 4;
      int var3 = var1.c << 4;
      int var4 = this.a.a(var2, var3);
      int[] var5 = this.d();
      ChunkRender.a(this.a.a.c, var1, var5, var4, 512, var1.d == -1);
      this.a.d(var2, var3, 16, 16);
      ++this.e;
   }

   public static int a(int[] var0, int var1, int var2) {
      int var3 = var0[var1];
      int var4 = var0[var1 + 1];
      int var5 = var0[var1 + var2];
      int var6 = var0[var1 + var2 + 1];
      int var7 = (var3 >> 16 & 255) + (var4 >> 16 & 255) + (var5 >> 16 & 255) + (var6 >> 16 & 255);
      var7 >>= 2;
      int var8 = (var3 >> 8 & 255) + (var4 >> 8 & 255) + (var5 >> 8 & 255) + (var6 >> 8 & 255);
      var8 >>= 2;
      int var9 = (var3 & 255) + (var4 & 255) + (var5 & 255) + (var6 & 255);
      var9 >>= 2;
      return -16777216 | (var7 & 255) << 16 | (var8 & 255) << 8 | var9 & 255;
   }

   public void a(int[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      int[] var8 = this.d();

      for(int var9 = 0; var9 < var7; ++var9) {
         for(int var10 = 0; var10 < var6; ++var10) {
            int var11 = (var3 + var9 * 2 << 9) + var2 + var10 * 2;
            int var12 = a(var1, var11, 512);
            var8[(var5 + var9 << 9) + var4 + var10] = var12;
         }
      }

      ++this.e;
   }

   public static void a(File var0, int[] var1, int var2, int var3) {
      BufferedImage var4 = new BufferedImage(var2, var3, 2);
      var4.setRGB(0, 0, var2, var3, var1, 0, var2);

      try {
         ImageIO.write(var4, "png", var0);
      } catch (IOException var6) {
         RegionManager.c("saveImage: error: could not write image to %s", new Object[]{var0});
      }

   }

   public static int[] a(File var0, int var1, int var2) {
      BufferedImage var3 = null;

      try {
         var3 = ImageIO.read(var0);
      } catch (IOException var5) {
         var3 = null;
      }

      int[] var4 = null;
      if(var3 != null) {
         if(var3.getWidth() == var1 && var3.getHeight() == var2) {
            var4 = new int[var1 * var2];
            var3.getRGB(0, 0, var1, var2, var4, 0, var1);
         } else {
            RegionManager.b("loadImage: image \'%s\' does not match expected dimensions (got %dx%d expected %dx%d)", new Object[]{var0, Integer.valueOf(var3.getWidth()), Integer.valueOf(var3.getHeight()), Integer.valueOf(var1), Integer.valueOf(var2)});
         }
      }

      return var4;
   }
}
