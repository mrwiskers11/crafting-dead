package com.craftingdead.client.mapwriter.region;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import com.craftingdead.client.mapwriter.region.RegionManager;

public class RegionFile {

   private final File a;
   private int b = 0;
   private RandomAccessFile c = null;
   private RegionFile.b[] d = new RegionFile.b[4096];
   private int[] e = new int[4096];
   private List f = null;


   public RegionFile(File var1) {
      this.a = var1;
   }

   public String toString() {
      return String.format("%s", new Object[]{this.a});
   }

   public boolean a() {
      return this.a.isFile();
   }

   public boolean b() {
      return this.c != null;
   }

   private void a(RegionFile.b var1, boolean var2) {
      int var3 = var1.a + var1.b;
      int var4 = var3 + 1 - this.f.size();

      int var5;
      for(var5 = 0; var5 < var4; ++var5) {
         this.f.add(Boolean.valueOf(false));
      }

      for(var5 = var1.a; var5 < var3; ++var5) {
         if(var2 && ((Boolean)this.f.get(var5)).booleanValue()) {
            RegionManager.c("sector %d already filled, possible chunk overlap", new Object[]{Integer.valueOf(var5)});
         }

         this.f.set(var5, Boolean.valueOf(var2));
      }

   }

   private boolean a(RegionFile.b var1) {
      int var2 = Math.min(var1.a + var1.b, this.f.size());
      boolean var3 = false;

      for(int var4 = var1.a; var4 < var2; ++var4) {
         if(((Boolean)this.f.get(var4)).booleanValue()) {
            var3 = true;
         }
      }

      return var3;
   }

   private RegionFile.b a(int var1) {
      int var2 = 0;
      int var3 = 0;
      int var4 = 0;
      int var5 = Integer.MAX_VALUE;

      int var6;
      for(var6 = 2; var6 < this.f.size(); ++var6) {
         if(((Boolean)this.f.get(var6)).booleanValue()) {
            if(var3 >= var1 && var3 < var5) {
               var5 = var3;
               var4 = var2;
               if(var3 == var1) {
                  break;
               }
            }

            var3 = 0;
         } else {
            if(var3 == 0) {
               var2 = var6;
            }

            ++var3;
         }
      }

      if(var4 <= 0) {
         var4 = var6;
      }

      return new RegionFile.b(var4, var1);
   }

   public void c() {
      int var1 = 0;
      int var2 = 0;

      for(int var3 = 2; var3 < this.f.size(); ++var3) {
         if(((Boolean)this.f.get(var3)).booleanValue()) {
            ++var2;
         } else {
            ++var1;
         }
      }

      RegionManager.a("Region File %s: filled sectors = %d, free sectors = %d", new Object[]{this, Integer.valueOf(var2), Integer.valueOf(var1)});
      String var5 = "";

      int var4;
      for(var4 = 0; var4 < this.f.size(); ++var4) {
         if((var4 & 31) == 0) {
            var5 = String.format("%04x:", new Object[]{Integer.valueOf(var4)});
         }

         var5 = var5 + (((Boolean)this.f.get(var4)).booleanValue()?'1':'0');
         if((var4 & 31) == 31) {
            RegionManager.a("%s", new Object[]{var5});
         }
      }

      if((var4 & 31) != 31) {
         RegionManager.a("%s", new Object[]{var5});
      }

   }

   private RegionFile.b c(int var1, int var2) {
      return this.d[(var2 & 31) << 5 | var1 & 31];
   }

   private void a(int var1, int var2, RegionFile.b var3) throws IOException {
      int var4 = (var2 & 31) << 5 | var1 & 31;
      this.c.seek((long)(var4 * 4));
      if(var3 != null && var3.b > 0) {
         this.c.writeInt(var3.a());
      } else {
         this.c.writeInt(0);
      }

      this.d[var4] = var3;
   }

   public boolean d() {
      File var1 = this.a.getParentFile();
      if(var1.exists()) {
         if(!var1.isDirectory()) {
            RegionManager.c("path %s exists and is not a directory", new Object[]{var1});
            return true;
         }
      } else if(!var1.mkdirs()) {
         RegionManager.c("could not create directory %s", new Object[]{var1});
         return true;
      }

      try {
         this.c = new RandomAccessFile(this.a, "rw");
         this.c.seek(0L);
         this.b = (int)((this.c.length() + 4095L) / 4096L);
         this.f = new ArrayList();
         Arrays.fill(this.d, (Object)null);
         Arrays.fill(this.e, 0);
         int var2;
         if(this.b < 3) {
            for(var2 = 0; var2 < 2048; ++var2) {
               this.c.writeInt(0);
            }
         } else {
            for(var2 = 0; var2 < 1024; ++var2) {
               RegionFile.b var3 = new RegionFile.b(this.c.readInt());
               if(var3.b > 0) {
                  if(!this.a(var3)) {
                     this.d[var2] = var3;
                     this.a(var3, true);
                  } else {
                     RegionManager.c("chunk %d overlaps another chunk, file may be corrupt", new Object[]{Integer.valueOf(var2)});
                  }
               }
            }

            for(var2 = 0; var2 < 1024; ++var2) {
               this.e[var2] = this.c.readInt();
            }
         }
      } catch (Exception var4) {
         this.c = null;
         RegionManager.c("exception when opening region file \'%s\': %s", new Object[]{this.a, var4});
      }

      return this.c == null;
   }

   public void e() {
      if(this.c != null) {
         try {
            this.c.close();
         } catch (IOException var2) {
            ;
         }
      }

   }

   public DataInputStream a(int var1, int var2) {
      DataInputStream var3 = null;
      if(this.c != null) {
         RegionFile.b var4 = this.c(var1, var2);
         if(var4 != null && var4.b > 0) {
            int var5 = var4.a * 4096;

            try {
               this.c.seek((long)var5);
               int var6 = this.c.readInt();
               byte var7 = this.c.readByte();
               if(var6 > 1 && var6 + 4 < var4.b * 4096 && var7 == 2) {
                  byte[] var8 = new byte[var6 - 1];
                  this.c.read(var8);
                  var3 = new DataInputStream(new BufferedInputStream(new InflaterInputStream(new ByteArrayInputStream(var8))));
               } else {
                  RegionManager.c("data length (%d) or version (%d) invalid for chunk (%d, %d)", new Object[]{Integer.valueOf(var6), Byte.valueOf(var7), Integer.valueOf(var1), Integer.valueOf(var2)});
               }
            } catch (Exception var9) {
               RegionManager.c("exception while reading chunk (%d, %d): %s", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2), var9});
               var3 = null;
            }
         }
      }

      return var3;
   }

   public DataOutputStream b(int var1, int var2) {
      return new DataOutputStream(new DeflaterOutputStream(new RegionFile.a(this, var1, var2)));
   }

   private void a(RegionFile.b var1, byte[] var2, int var3) throws IOException {
      this.c.seek((long)var1.a * 4096L);
      this.c.writeInt(var3 + 1);
      this.c.writeByte(2);
      this.c.write(var2, 0, var3);
      int var4 = var1.a + var1.b;
      if(var4 + 1 > this.b) {
         this.b = var4 + 1;
      }

   }

   private boolean a(int var1, int var2, byte[] var3, int var4) {
      if(var4 <= 0) {
         RegionManager.b("not writing chunk (%d, %d) with length %d", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2), Integer.valueOf(var4)});
         return true;
      } else {
         RegionFile.b var5 = this.c(var1, var2);
         if(var5 != null) {
            this.a(var5, false);
         }

         int var6 = (var4 + 5 + 4095) / 4096;
         RegionFile.b var7;
         if(var5 != null && var6 <= var5.b) {
            var7 = new RegionFile.b(var5.a, var6);
         } else {
            var7 = this.a(var6);
         }

         this.a(var7, true);
         boolean var8 = true;

         try {
            this.a(var7, var3, var4);
            this.a(var1, var2, var7);
            var8 = false;
         } catch (IOException var10) {
            RegionManager.c("could not write chunk (%d, %d) to region file: %s", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2), var10});
         }

         return var8;
      }
   }

   private class a extends ByteArrayOutputStream {

      private final int b;
      private final int c;
      private final RegionFile d;


      public a(RegionFile var2, int var3, int var4) {
         super(8096);
         this.d = var2;
         this.b = var3;
         this.c = var4;
      }

      public void close() {
         this.d.a(this.b, this.c, this.buf, this.count);
      }
   }

   private class b {

      final int a;
      final int b;


      b(int var2, int var3) {
         this.a = var2;
         this.b = var3;
      }

      b(int var2) {
         this(var2 >> 8 & 16777215, var2 & 255);
      }

      int a() {
         return this.a << 8 | this.b & 255;
      }
   }
}
