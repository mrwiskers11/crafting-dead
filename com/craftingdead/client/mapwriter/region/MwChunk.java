package com.craftingdead.client.mapwriter.region;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.craftingdead.client.mapwriter.region.IChunk;
import com.craftingdead.client.mapwriter.region.Nbt;
import com.craftingdead.client.mapwriter.region.RegionFile;
import com.craftingdead.client.mapwriter.region.RegionFileCache;
import com.craftingdead.client.mapwriter.region.RegionManager;

public class MwChunk implements IChunk {

   public static final int a = 16;
   public final int b;
   public final int c;
   public final int d;
   public final byte[][] e;
   public final byte[][] f;
   public final byte[][] g;
   public final byte[][] h;
   public final byte[] i;
   public final int j;


   public MwChunk(int var1, int var2, int var3, byte[][] var4, byte[][] var5, byte[][] var6, byte[][] var7, byte[] var8) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
      this.g = var6;
      this.i = var8;
      this.h = var7;
      int var9 = 0;

      for(int var10 = 0; var10 < 16; ++var10) {
         if(var5[var10] != null) {
            var9 = (var10 << 4) + 15;
         }
      }

      this.j = var9;
   }

   public String toString() {
      return String.format("(%d, %d) dim%d", new Object[]{Integer.valueOf(this.b), Integer.valueOf(this.c), Integer.valueOf(this.d)});
   }

   public static MwChunk a(int var0, int var1, int var2, RegionFileCache var3) {
      byte[] var4 = null;
      byte[][] var5 = new byte[16][];
      byte[][] var6 = new byte[16][];
      byte[][] var7 = new byte[16][];
      byte[][] var8 = new byte[16][];
      DataInputStream var9 = null;
      RegionFile var10 = var3.c(var0 << 4, var1 << 4, var2);
      if(!var10.b() && var10.a()) {
         var10.d();
      }

      if(var10.b()) {
         var9 = var10.a(var0 & 31, var1 & 31);
      }

      if(var9 != null) {
         try {
            Nbt var11 = Nbt.a(var9);
            Nbt var12 = var11.a("Level");
            int var13 = var12.a("xPos").e();
            int var14 = var12.a("zPos").e();
            if(var13 != var0 || var14 != var1) {
               RegionManager.b("chunk (%d, %d) has NBT coords (%d, %d)", new Object[]{Integer.valueOf(var0), Integer.valueOf(var1), Integer.valueOf(var13), Integer.valueOf(var14)});
            }

            Nbt var15 = var12.a("Sections");

            for(int var16 = 0; var16 < var15.b(); ++var16) {
               Nbt var17 = var15.a(var16);
               if(!var17.a()) {
                  byte var18 = var17.a("Y").c();
                  var6[var18 & 15] = var17.a("Blocks").i();
                  var5[var18 & 15] = var17.a("Add").i();
                  var7[var18 & 15] = var17.a("Data").i();
               }
            }

            var4 = var12.a("Biomes").i();
         } catch (IOException var27) {
            RegionManager.c("%s: could not read chunk (%d, %d) from region file\n", new Object[]{var27, Integer.valueOf(var0), Integer.valueOf(var1)});
         } finally {
            try {
               var9.close();
            } catch (IOException var26) {
               RegionManager.c("MwChunk.read: %s while closing input stream", new Object[]{var26});
            }

         }
      }

      return new MwChunk(var0, var1, var2, var5, var6, var7, var8, var4);
   }

   public boolean b() {
      return this.j <= 0;
   }

   public int a(int var1, int var2) {
      return this.i != null?this.i[(var2 & 15) << 4 | var1 & 15] & 255:0;
   }

   public int b(int var1, int var2, int var3) {
      return 15;
   }

   public int a() {
      return this.j;
   }

   public int a(int var1, int var2, int var3) {
      int var4 = var2 >> 4 & 15;
      int var5 = (var2 & 15) << 8 | (var3 & 15) << 4 | var1 & 15;
      byte var6 = this.f != null && this.f[var4] != null?this.f[var4][var5]:0;
      byte var7 = this.e != null && this.e[var4] != null?this.e[var4][var5 >> 1]:0;
      byte var8 = this.g != null && this.g[var4] != null?this.g[var4][var5 >> 1]:0;
      return (var5 & 1) == 1?(var7 & 240) << 8 | (var6 & 255) << 4 | (var8 & 240) >> 4:(var7 & 15) << 12 | (var6 & 255) << 4 | var8 & 15;
   }

   public Nbt c() {
      Nbt var1 = new Nbt((byte)9, "Sections", (Object)null);

      Nbt var3;
      for(int var2 = 0; var2 < 16; ++var2) {
         var3 = new Nbt((byte)10, "", (Object)null);
         var3.a(new Nbt((byte)1, "Y", Byte.valueOf((byte)var2)));
         if(this.f != null && this.f[var2] != null) {
            var3.a(new Nbt((byte)7, "Blocks", this.f[var2]));
         }

         if(this.e != null && this.e[var2] != null) {
            var3.a(new Nbt((byte)7, "Add", this.e[var2]));
         }

         if(this.g != null && this.g[var2] != null) {
            var3.a(new Nbt((byte)7, "Data", this.g[var2]));
         }

         var1.a(var3);
      }

      Nbt var4 = new Nbt((byte)10, "Level", (Object)null);
      var4.a(new Nbt((byte)3, "xPos", Integer.valueOf(this.b)));
      var4.a(new Nbt((byte)3, "zPos", Integer.valueOf(this.c)));
      var4.a(var1);
      if(this.i != null) {
         var4.a(new Nbt((byte)7, "Biomes", this.i));
      }

      var3 = new Nbt((byte)10, "", (Object)null);
      var3.a(var4);
      return var3;
   }

   public synchronized boolean a(RegionFileCache var1) {
      boolean var2 = false;
      RegionFile var3 = var1.c(this.b << 4, this.c << 4, this.d);
      if(!var3.b()) {
         var2 = var3.d();
      }

      if(!var2) {
         DataOutputStream var4 = var3.b(this.b & 31, this.c & 31);
         if(var4 != null) {
            Nbt var5 = this.c();

            try {
               var5.a(var4);
            } catch (IOException var15) {
               RegionManager.c("%s: could not write chunk (%d, %d) to region file", new Object[]{var15, Integer.valueOf(this.b), Integer.valueOf(this.c)});
               var2 = true;
            } finally {
               try {
                  var4.close();
               } catch (IOException var14) {
                  RegionManager.c("%s while closing chunk data output stream", new Object[]{var14});
               }

            }
         } else {
            RegionManager.c("error: could not get output stream for chunk (%d, %d)", new Object[]{Integer.valueOf(this.b), Integer.valueOf(this.c)});
         }
      } else {
         RegionManager.c("error: could not open region file for chunk (%d, %d)", new Object[]{Integer.valueOf(this.b), Integer.valueOf(this.c)});
      }

      return var2;
   }
}
