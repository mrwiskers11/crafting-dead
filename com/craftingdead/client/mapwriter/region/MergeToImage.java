package com.craftingdead.client.mapwriter.region;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import com.craftingdead.client.mapwriter.region.Region;
import com.craftingdead.client.mapwriter.region.RegionManager;

public class MergeToImage {

   public static final int a = 8192;
   public static final int b = 8192;


   public static BufferedImage a(RegionManager var0, int var1, int var2, int var3, int var4, int var5) {
      BufferedImage var6 = new BufferedImage(var3, var4, 1);

      for(int var7 = 0; var7 < var4; var7 += 512) {
         for(int var8 = 0; var8 < var3; var8 += 512) {
            Region var9 = var0.a(var1 + var8, var2 + var7, 0, var5);
            int[] var10 = var9.k.c();
            if(var10 != null) {
               var6.setRGB(var8, var7, 512, 512, var10, 0, 512);
            }
         }
      }

      return var6;
   }

   public static boolean a(BufferedImage var0, File var1) {
      boolean var2 = true;

      try {
         ImageIO.write(var0, "png", var1);
         var2 = false;
      } catch (IOException var4) {
         ;
      }

      return var2;
   }

   public static int a(RegionManager var0, int var1, int var2, int var3, int var4, int var5, File var6, String var7) {
      var3 = var3 + 512 - 1 & -512;
      var4 = var4 + 512 - 1 & -512;
      var3 = Math.max(512, var3);
      var4 = Math.max(512, var4);
      int var8 = var1 - var3 / 2;
      int var9 = var2 - var4 / 2;
      var8 = Math.round((float)var8 / 512.0F) * 512;
      var9 = Math.round((float)var9 / 512.0F) * 512;
      int var10 = var8 + var3;
      int var11 = var9 + var4;
      RegionManager.a("merging area starting at (%d,%d), %dx%d blocks", new Object[]{Integer.valueOf(var8), Integer.valueOf(var9), Integer.valueOf(var3), Integer.valueOf(var4)});
      int var12 = 0;
      int var13 = 0;

      for(int var14 = var9; var14 < var11; var14 += 8192) {
         int var15 = Math.min(var11 - var14, 8192);
         int var16 = 0;

         for(int var17 = var8; var17 < var10; var17 += 8192) {
            int var18 = Math.min(var10 - var17, 8192);
            String var19 = String.format("%s.%d.%d.png", new Object[]{var7, Integer.valueOf(var16), Integer.valueOf(var12)});
            File var20 = new File(var6, var19);
            RegionManager.a("merging regions to image %s", new Object[]{var20});
            BufferedImage var21 = a(var0, var17, var14, var18, var15, var5);
            a(var21, var20);
            ++var16;
            ++var13;
         }

         ++var12;
      }

      return var13;
   }
}
