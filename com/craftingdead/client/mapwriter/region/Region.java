package com.craftingdead.client.mapwriter.region;

import java.io.File;

import com.craftingdead.client.mapwriter.region.MwChunk;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.region.SurfacePixels;

public class Region {

   public RegionManager a;
   public static final int b = 9;
   public static final int c = 512;
   public static final int d = -512;
   public final int e;
   public final int f;
   public final int g;
   public final int h;
   public final Long i;
   public final int j;
   public SurfacePixels k;


   public Region(RegionManager var1, int var2, int var3, int var4, int var5) {
      this.a = var1;
      this.h = Math.min(Math.max(0, var4), var1.e);
      this.g = var5;
      this.j = 512 << var4;
      this.e = var2 & -this.j;
      this.f = var3 & -this.j;
      this.i = b(this.e, this.f, this.h, this.g);
      File var6 = this.c();
      this.k = new SurfacePixels(this, var6);
   }

   public void a() {
      this.k.b();
   }

   public void b() {
      this.k.a();
   }

   public String toString() {
      return String.format("(%d,%d) z%d dim%d", new Object[]{Integer.valueOf(this.e), Integer.valueOf(this.f), Integer.valueOf(this.h), Integer.valueOf(this.g)});
   }

   private static File a(File var0, int var1) {
      if(var1 != 0) {
         var0 = new File(var0, "DIM" + var1);
      }

      return var0;
   }

   public File c() {
      File var1 = a(this.a.b, this.g);
      File var2 = new File(var1, "z" + this.h);
      var2.mkdirs();
      String var3 = String.format("%d.%d.png", new Object[]{Integer.valueOf(this.e >> 9 + this.h), Integer.valueOf(this.f >> 9 + this.h)});
      return new File(var2, var3);
   }

   public boolean a(int var1, int var2, int var3, int var4) {
      var1 &= -this.j;
      var2 &= -this.j;
      return this.e == var1 && this.f == var2 && this.h == var3 && this.g == var4;
   }

   public boolean equals(Object var1) {
      boolean var2 = false;
      if(var1 != null && var1 instanceof Region) {
         Region var3 = (Region)var1;
         var2 = this.a(var3.e, var3.f, var3.h, var3.g);
      }

      return var2;
   }

   public static Long b(int var0, int var1, int var2, int var3) {
      var0 = var0 >> 9 + var2 & '\uffff';
      var1 = var1 >> 9 + var2 & '\uffff';
      var2 &= 255;
      var3 &= 255;
      return Long.valueOf((long)var3 << 40 | (long)var2 << 32 | (long)var1 << 16 | (long)var0);
   }

   public int a(int var1, int var2) {
      return ((var2 >> this.h & 511) << 9) + (var1 >> this.h & 511);
   }

   public int[] d() {
      return this.k.c();
   }

   public boolean a(int var1, int var2, int var3, int var4, int var5) {
      return var1 >= this.e && var2 >= this.f && var1 + var3 <= this.e + this.j && var2 + var4 <= this.f + this.j && var5 == this.g;
   }

   public Region c(int var1, int var2, int var3, int var4) {
      int[] var5 = this.k.c();
      Region var6 = null;
      if(var5 != null) {
         int var7 = this.h + 1;
         if(var7 <= this.a.e) {
            var6 = this.a.a(var1, var2, var7, this.g);
            int var8 = Math.max(1, var3 >> var6.h);
            int var9 = Math.max(1, var4 >> var6.h);
            int var10 = var1 >> this.h & 511 & -2;
            int var11 = var2 >> this.h & 511 & -2;
            int var12 = var1 >> var6.h & 511;
            int var13 = var2 >> var6.h & 511;
            var6.k.a(var5, var10, var11, var12, var13, var8, var9);
         }
      }

      return var6;
   }

   public void d(int var1, int var2, int var3, int var4) {
      for(Region var5 = this; var5 != null; var5 = var5.c(var1, var2, var3, var4)) {
         ;
      }

   }

   public void e() {
      this.d(this.e, this.f, this.j, this.j);
   }

   public void a(MwChunk var1) {
      if(this.h == 0) {
         this.k.a(var1);
      }

   }
}
