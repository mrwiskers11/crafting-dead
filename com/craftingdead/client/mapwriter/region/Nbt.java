package com.craftingdead.client.mapwriter.region;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Nbt {

   public static final byte a = 0;
   public static final byte b = 1;
   public static final byte c = 2;
   public static final byte d = 3;
   public static final byte e = 4;
   public static final byte f = 5;
   public static final byte g = 6;
   public static final byte h = 7;
   public static final byte i = 8;
   public static final byte j = 9;
   public static final byte k = 10;
   public static final byte l = 11;
   public static final byte m = -1;
   public static Nbt n = new Nbt((byte)-1, "", (Object)null);
   public byte o;
   public String p;
   private Object q;


   public Nbt(byte var1, String var2, Object var3) {
      this.o = var1;
      this.p = var2;
      this.q = var3;
   }

   public boolean a() {
      return this.o == -1;
   }

   public void a(Nbt var1) {
      if(this.o == 9) {
         if(this.q == null) {
            this.q = new ArrayList();
         }

         List var2 = (List)this.q;
         var2.add(var1);
      }

      if(this.o == 10) {
         if(this.q == null) {
            this.q = new HashMap();
         }

         Map var3 = (Map)this.q;
         var3.put(var1.p, var1);
      }

   }

   public Nbt a(int var1) {
      Nbt var2 = null;
      if(this.o == 9 && this.q != null) {
         List var3 = (List)this.q;
         if(var1 >= 0 && var1 < var3.size()) {
            var2 = (Nbt)var3.get(var1);
         }
      }

      return var2 != null?var2:n;
   }

   public Nbt a(String var1) {
      Nbt var2 = null;
      if(this.o == 10 && this.q != null) {
         Map var3 = (Map)this.q;
         var2 = (Nbt)var3.get(var1);
      }

      return var2 != null?var2:n;
   }

   public int b() {
      int var1 = 0;
      if(this.o == 9 && this.q != null) {
         List var2 = (List)this.q;
         var1 = var2.size();
      }

      return var1;
   }

   public static Nbt a(DataInputStream var0) throws IOException {
      String var2 = "";
      byte var1 = var0.readByte();
      if(var1 != 0) {
         var2 = var0.readUTF();
      }

      return a(var0, var1, var2);
   }

   public static Nbt a(DataInputStream var0, byte var1, String var2) throws IOException {
      Nbt var3 = null;
      Nbt var11;
      switch(var1) {
      case 0:
         break;
      case 1:
         var3 = new Nbt(var1, var2, new Byte(var0.readByte()));
         break;
      case 2:
         var3 = new Nbt(var1, var2, new Short(var0.readShort()));
         break;
      case 3:
         var3 = new Nbt(var1, var2, new Integer(var0.readInt()));
         break;
      case 4:
         var3 = new Nbt(var1, var2, new Long(var0.readLong()));
         break;
      case 5:
         var3 = new Nbt(var1, var2, new Float(var0.readFloat()));
         break;
      case 6:
         var3 = new Nbt(var1, var2, new Double(var0.readDouble()));
         break;
      case 7:
         int var4 = var0.readInt();
         byte[] var5 = null;
         if(var4 > 0) {
            var5 = new byte[var4];
            var0.readFully(var5);
         }

         var3 = new Nbt(var1, var2, var5);
         break;
      case 8:
         var3 = new Nbt(var1, var2, var0.readUTF());
         break;
      case 9:
         byte var12 = var0.readByte();
         int var9 = var0.readInt();
         var3 = new Nbt(var1, var2, (Object)null);

         for(int var13 = 0; var13 < var9; ++var13) {
            var11 = a(var0, var12, "");
            var3.a(var11);
         }

         return var3 != null?var3:n;
      case 10:
         var3 = new Nbt(var1, var2, (Object)null);
         boolean var10 = false;

         while(!var10) {
            var11 = a(var0);
            if(var11.a()) {
               var10 = true;
            } else {
               var3.a(var11);
            }
         }

         return var3 != null?var3:n;
      case 11:
         int var6 = var0.readInt();
         int[] var7 = null;
         if(var6 > 0) {
            var7 = new int[var6];

            for(int var8 = 0; var8 < var6; ++var8) {
               var7[var8] = var0.readInt();
            }
         }

         var3 = new Nbt(var1, var2, var7);
         break;
      default:
         System.out.format("error: encountered unknown tag id\n", new Object[0]);
      }

      return var3 != null?var3:n;
   }

   public byte c() {
      return this.o == 1 && this.q != null?((Byte)this.q).byteValue():0;
   }

   public short d() {
      return this.o == 2 && this.q != null?((Short)this.q).shortValue():0;
   }

   public int e() {
      return this.o == 3 && this.q != null?((Integer)this.q).intValue():0;
   }

   public long f() {
      return this.o == 4 && this.q != null?((Long)this.q).longValue():0L;
   }

   public float g() {
      return this.o == 5 && this.q != null?((Float)this.q).floatValue():0.0F;
   }

   public double h() {
      return this.o == 6 && this.q != null?((Double)this.q).doubleValue():0.0D;
   }

   public byte[] i() {
      return this.o == 7 && this.q != null?(byte[])((byte[])this.q):null;
   }

   public int[] j() {
      return this.o == 11 && this.q != null?(int[])((int[])this.q):null;
   }

   public String k() {
      return this.o == 8 && this.q != null?(String)this.q:null;
   }

   public void a(DataOutputStream var1) throws IOException {
      var1.writeByte(this.o);
      if(this.o != 0) {
         var1.writeUTF(this.p);
         this.b(var1);
      }

   }

   public void b(DataOutputStream var1) throws IOException {
      int var4;
      switch(this.o) {
      case 0:
         break;
      case 1:
         var1.writeByte(this.c());
         break;
      case 2:
         var1.writeShort(this.d());
         break;
      case 3:
         var1.writeInt(this.e());
         break;
      case 4:
         var1.writeLong(this.f());
         break;
      case 5:
         var1.writeFloat(this.g());
         break;
      case 6:
         var1.writeDouble(this.h());
         break;
      case 7:
         byte[] var2 = this.i();
         if(var2 != null) {
            var1.writeInt(var2.length);
            var1.write(var2);
         } else {
            var1.writeInt(0);
         }
         break;
      case 8:
         var1.writeUTF(this.k());
         break;
      case 9:
         var4 = this.b();
         if(this.b() > 0) {
            var1.writeByte(this.a(0).o);
            var1.writeInt(var4);

            for(int var8 = 0; var8 < var4; ++var8) {
               this.a(var8).b(var1);
            }

            return;
         } else {
            var1.writeByte(1);
            var1.writeInt(0);
            break;
         }
      case 10:
         Map var5 = (Map)this.q;
         Iterator var6 = var5.values().iterator();

         while(var6.hasNext()) {
            Nbt var7 = (Nbt)var6.next();
            if(var7 != null) {
               var7.a(var1);
            }
         }

         var1.writeByte(0);
         break;
      case 11:
         int[] var3 = this.j();
         if(var3 != null) {
            var1.writeInt(var3.length);

            for(var4 = 0; var4 < var3.length; ++var4) {
               var1.writeInt(var3[var4]);
            }

            return;
         } else {
            var1.writeInt(0);
            break;
         }
      default:
         System.out.format("error: encountered unknown tag id\n", new Object[0]);
      }

   }

}
