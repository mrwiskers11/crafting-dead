package com.craftingdead.client.mapwriter.region;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.mapwriter.region.BlockColours;
import com.craftingdead.client.mapwriter.region.MwChunk;
import com.craftingdead.client.mapwriter.region.Region;
import com.craftingdead.client.mapwriter.region.RegionFileCache;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class RegionManager {

   private final RegionManager.a g;
   public final File a;
   public final File b;
   public BlockColours c;
   public final RegionFileCache d;
   public int e;
   public int f;


   public static void a(String var0, Object ... var1) {
      CraftingDead.d.info(String.format(var0, var1));
   }

   public static void b(String var0, Object ... var1) {
      CraftingDead.d.warning(String.format(var0, var1));
   }

   public static void c(String var0, Object ... var1) {
      CraftingDead.d.severe(String.format(var0, var1));
   }

   public RegionManager(File var1, File var2, BlockColours var3, int var4, int var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.g = new RegionManager.a();
      this.d = new RegionFileCache(var1);
      this.f = var4;
      this.e = var5;
   }

   public void a() {
      Iterator var1 = this.g.values().iterator();

      while(var1.hasNext()) {
         Region var2 = (Region)var1.next();
         if(var2 != null) {
            var2.a();
         }
      }

      this.g.clear();
      this.d.a();
   }

   private static int a(Map var0, String var1) {
      int var2 = 1;
      if(var0.containsKey(var1)) {
         var2 = ((Integer)var0.get(var1)).intValue() + 1;
      }

      var0.put(var1, Integer.valueOf(var2));
      return var2;
   }

   public void b() {
      a("loaded region listing:", new Object[0]);
      HashMap var1 = new HashMap();
      Iterator var2 = this.g.values().iterator();

      while(var2.hasNext()) {
         Region var3 = (Region)var2.next();
         a("  %s", new Object[]{var3});
         a((Map)var1, String.format("dim%d", new Object[]{Integer.valueOf(var3.g)}));
         a((Map)var1, String.format("zoom%d", new Object[]{Integer.valueOf(var3.h)}));
         a((Map)var1, "total");
      }

      a("loaded region stats:", new Object[0]);
      var2 = var1.entrySet().iterator();

      while(var2.hasNext()) {
         Entry var4 = (Entry)var2.next();
         a("  %s: %d", new Object[]{var4.getKey(), var4.getValue()});
      }

   }

   public Region a(int var1, int var2, int var3, int var4) {
      Region var5 = (Region)this.g.get(Region.b(var1, var2, var3, var4));
      if(var5 == null) {
         var5 = new Region(this, var1, var2, var3, var4);
         this.g.put(var5.i, var5);
      }

      return var5;
   }

   public void a(MwChunk var1) {
      Region var2 = this.a(var1.b << 4, var1.c << 4, 0, var1.d);
      var2.a(var1);
   }

   public void a(int var1, int var2, int var3, int var4, int var5) {
      var1 &= -512;
      var2 &= -512;
      var3 = var3 + 512 & -512;
      var4 = var4 + 512 & -512;
      a("rebuilding regions from (%d, %d) to (%d, %d)", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2), Integer.valueOf(var1 + var3), Integer.valueOf(var2 + var4)});

      for(int var6 = var1; var6 < var1 + var3; var6 += 512) {
         for(int var7 = var2; var7 < var2 + var4; var7 += 512) {
            Region var8 = this.a(var6, var7, 0, var5);
            if(this.d.b(var6, var7, var5)) {
               var8.b();

               for(int var9 = 0; var9 < 32; ++var9) {
                  for(int var10 = 0; var10 < 32; ++var10) {
                     MwChunk var11 = MwChunk.a((var8.e >> 4) + var10, (var8.f >> 4) + var9, var8.g, this.d);
                     var8.a(var11);
                  }
               }
            }

            var8.e();
         }
      }

   }

   class a extends LinkedHashMap {

      private static final long b = 1L;
      private static final int c = 64;


      public a() {
         super(128, 0.5F, true);
      }

      protected boolean removeEldestEntry(Entry var1) {
         boolean var2 = false;
         if(this.size() > 64) {
            Region var3 = (Region)var1.getValue();
            var3.a();
            var2 = true;
         }

         return var2;
      }
   }
}
