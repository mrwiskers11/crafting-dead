package com.craftingdead.client.mapwriter.region;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.craftingdead.client.mapwriter.region.RegionFile;

public class RegionFileCache {

   private RegionFileCache.a a = new RegionFileCache.a();
   private File b;


   public RegionFileCache(File var1) {
      this.b = var1;
   }

   public void a() {
      Iterator var1 = this.a.values().iterator();

      while(var1.hasNext()) {
         RegionFile var2 = (RegionFile)var1.next();
         var2.e();
      }

      this.a.clear();
   }

   public File a(int var1, int var2, int var3) {
      File var4 = this.b;
      if(var3 != 0) {
         var4 = new File(var4, "DIM" + var3);
      }

      var4 = new File(var4, "region");
      String var5 = String.format("r.%d.%d.mca", new Object[]{Integer.valueOf(var1 >> 9), Integer.valueOf(var2 >> 9)});
      return new File(var4, var5);
   }

   public boolean b(int var1, int var2, int var3) {
      File var4 = this.a(var1, var2, var3);
      return var4.isFile();
   }

   public RegionFile c(int var1, int var2, int var3) {
      File var4 = this.a(var1, var2, var3);
      String var5 = var4.toString();
      RegionFile var6 = (RegionFile)this.a.get(var5);
      if(var6 == null) {
         var6 = new RegionFile(var4);
         this.a.put(var5, var6);
      }

      return var6;
   }

   class a extends LinkedHashMap {

      private static final long c = 1L;
      static final int a = 8;


      public a() {
         super(16, 0.5F, true);
      }

      protected boolean removeEldestEntry(Entry var1) {
         boolean var2 = false;
         if(this.size() > 8) {
            RegionFile var3 = (RegionFile)var1.getValue();
            var3.e();
            var2 = true;
         }

         return var2;
      }
   }
}
