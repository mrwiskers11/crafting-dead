package com.craftingdead.client.mapwriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class CircularHashMap {

   private Map a = new HashMap();
   private CircularHashMap.a b = null;
   private CircularHashMap.a c = null;


   public Object a(Object var1, Object var2) {
      CircularHashMap.a var3 = (CircularHashMap.a)this.a.get(var1);
      if(var3 == null) {
         var3 = new CircularHashMap.a(var1, var2);
         this.a.put(var1, var3);
         if(this.b == null) {
            var3.d = var3;
            var3.e = var3;
         } else {
            var3.d = this.b.d;
            var3.e = this.b;
            this.b.d.e = var3;
            this.b.d = var3;
         }

         if(this.c == null) {
            this.c = var3;
         }

         this.b = var3;
      } else {
         var3.c = var2;
      }

      return var2;
   }

   public Object a(Object var1) {
      CircularHashMap.a var2 = (CircularHashMap.a)this.a.get(var1);
      Object var3 = null;
      if(var2 != null) {
         if(this.b == var2) {
            this.b = var2.d;
            if(this.b == var2) {
               this.b = null;
            }
         }

         if(this.c == var2) {
            this.c = var2.d;
            if(this.c == var2) {
               this.c = null;
            }
         }

         var2.e.d = var2.d;
         var2.d.e = var2.e;
         var2.d = null;
         var2.e = null;
         var3 = var2.c;
         this.a.remove(var1);
      }

      return var3;
   }

   public void a() {
      Iterator var1 = this.a.values().iterator();

      while(var1.hasNext()) {
         CircularHashMap.a var2 = (CircularHashMap.a)var1.next();
         var2.d = null;
         var2.e = null;
      }

      this.a.clear();
      this.b = null;
      this.c = null;
   }

   public boolean b(Object var1) {
      return this.a.containsKey(var1);
   }

   public int b() {
      return this.a.size();
   }

   public Set c() {
      return this.a.keySet();
   }

   public Collection d() {
      ArrayList var1 = new ArrayList();
      Iterator var2 = this.a.values().iterator();

      while(var2.hasNext()) {
         CircularHashMap.a var3 = (CircularHashMap.a)var2.next();
         var1.add(var3.c);
      }

      return var1;
   }

   public Collection e() {
      return new ArrayList(this.a.values());
   }

   public Object c(Object var1) {
      CircularHashMap.a var2 = (CircularHashMap.a)this.a.get(var1);
      return var2 != null?var2.c:null;
   }

   public boolean f() {
      return this.a.isEmpty();
   }

   public Entry g() {
      if(this.c != null) {
         this.c = this.c.d;
      }

      return this.c;
   }

   public Entry h() {
      if(this.c != null) {
         this.c = this.c.e;
      }

      return this.c;
   }

   public void i() {
      this.c = this.b != null?this.b.d:null;
   }

   public boolean d(Object var1) {
      CircularHashMap.a var2 = (CircularHashMap.a)this.a.get(var1);
      if(var2 != null) {
         this.c = var2;
      }

      return var2 != null;
   }

   public class a implements Entry {

      private final Object b;
      private Object c;
      private CircularHashMap.a d;
      private CircularHashMap.a e;


      a(Object var2, Object var3) {
         this.b = var2;
         this.c = var3;
         this.d = this;
         this.e = this;
      }

      public Object getKey() {
         return this.b;
      }

      public Object getValue() {
         return this.c;
      }

      public Object setValue(Object var1) {
         Object var2 = this.c;
         this.c = var1;
         return var2;
      }
   }
}
