package com.craftingdead.client.mapwriter;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.tasks.Task;

public class BackgroundExecutor {

   private ExecutorService b = Executors.newSingleThreadExecutor();
   private LinkedList c = new LinkedList();
   public boolean a = false;


   public boolean a(Task var1) {
      if(!this.a) {
         Future var2 = this.b.submit(var1);
         var1.a(var2);
         this.c.add(var1);
      } else {
         MapUtil.e("MwExecutor.addTask: error: cannot add task to closed executor", new Object[0]);
      }

      return this.a;
   }

   public boolean a() {
      boolean var1 = false;
      Task var2 = (Task)this.c.poll();
      if(var2 != null) {
         if(var2.c()) {
            var2.d();
            var2.a();
            var1 = true;
         } else {
            this.c.push(var2);
         }
      }

      return !var1;
   }

   public boolean a(int var1, int var2) {
      while(this.c.size() > 0 && var1 > 0) {
         if(this.a()) {
            try {
               Thread.sleep((long)var2);
            } catch (Exception var4) {
               ;
            }

            --var1;
         }
      }

      return var1 <= 0;
   }

   public int b() {
      return this.c.size();
   }

   public boolean c() {
      boolean var1 = true;

      try {
         this.b.shutdown();
         this.a(50, 5);
         var1 = !this.b.awaitTermination(10L, TimeUnit.SECONDS);
         var1 = false;
      } catch (InterruptedException var3) {
         MapUtil.e("error: IO task was interrupted during shutdown", new Object[0]);
         var3.printStackTrace();
      }

      this.a = true;
      return var1;
   }
}
