package com.craftingdead.client.mapwriter;

import java.io.File;
import java.util.List;

import com.craftingdead.client.mapwriter.MapUtil;

import net.minecraftforge.common.Configuration;

public class MapConfig extends Configuration {

   public MapConfig(File var1) {
      super(var1, true);
   }

   public boolean a(String var1, String var2, boolean var3) {
      return this.get(var1, var2, var3?1:0).getInt() != 0;
   }

   public void b(String var1, String var2, boolean var3) {
      this.get(var1, var2, var3).set(var3?1:0);
   }

   public int a(String var1, String var2, int var3, int var4, int var5) {
      int var6 = this.get(var1, var2, var3).getInt();
      return Math.min(Math.max(var4, var6), var5);
   }

   public void a(String var1, String var2, int var3) {
      this.get(var1, var2, var3).set(var3);
   }

   public long a(String var1, String var2) {
      long var3 = -1L;
      if(this.hasKey(var1, var2)) {
         try {
            String var5 = this.get(var1, var2, "").getString();
            if(var5.length() > 0) {
               var3 = Long.parseLong(var5, 16);
               var3 &= 4294967295L;
            }
         } catch (NumberFormatException var6) {
            MapUtil.e("error: could not read colour from config file %s:%s", new Object[]{var1, var2});
            var3 = -1L;
         }
      }

      return var3;
   }

   public int b(String var1, String var2, int var3) {
      long var4 = this.a(var1, var2);
      if(var4 >= 0L) {
         var3 = (int)(var4 & 4294967295L);
      }

      return var3;
   }

   public int c(String var1, String var2, int var3) {
      long var4 = this.a(var1, var2);
      if(var4 >= 0L) {
         var3 = (int)(var4 & 4294967295L);
      } else {
         this.d(var1, var2, var3);
      }

      return var3;
   }

   public void d(String var1, String var2, int var3) {
      this.get(var1, var2, "00000000").set(String.format("%08x", new Object[]{Integer.valueOf(var3)}));
   }

   public void a(String var1, String var2, int var3, String var4) {
      this.get(var1, var2, "00000000", var4).set(String.format("%08x", new Object[]{Integer.valueOf(var3)}));
   }

   public String b(String var1, String var2) {
      String var3 = "";
      if(this.hasKey(var1, var2)) {
         var3 = this.get(var1, var2, var3).getString().trim();
         int var4 = var3.indexOf(32);
         if(var4 >= 0) {
            var3 = var3.substring(0, var4);
         }
      }

      return var3;
   }

   public void a(String var1, String var2, String var3, String var4) {
      if(var4 != null && var4.length() > 0) {
         var3 = var3 + " # " + var4;
      }

      this.get(var1, var2, var3).set(var3);
   }

   public void a(String var1, String var2, List var3) {
      int var4 = var3.size();
      int[] var5 = new int[var4];

      for(int var6 = 0; var6 < var4; ++var6) {
         var5[var6] = ((Integer)var3.get(var6)).intValue();
      }

      Object var9 = null;

      int[] var10;
      try {
         var10 = this.get(var1, var2, var5).getIntList();
      } catch (Exception var8) {
         var8.printStackTrace();
         var10 = null;
      }

      if(var10 != null) {
         var5 = var10;
      }

      var3.clear();

      for(int var7 = 0; var7 < var5.length; ++var7) {
         var3.add(Integer.valueOf(var5[var7]));
      }

   }

   public void b(String var1, String var2, List var3) {
      int var4 = var3.size();
      String[] var5 = new String[var4];

      for(int var6 = 0; var6 < var4; ++var6) {
         var5[var6] = ((Integer)var3.get(var6)).toString();
      }

      try {
         this.get(var1, var2, var5).set(var5);
      } catch (Exception var7) {
         var7.printStackTrace();
      }

   }
}
