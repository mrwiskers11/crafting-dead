package com.craftingdead.client.mapwriter.overlay;

import java.util.ArrayList;
import java.util.Random;

import com.craftingdead.client.mapwriter.api.IMwChunkOverlay;
import com.craftingdead.client.mapwriter.api.IMwDataProvider;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;
import com.craftingdead.client.mapwriter.overlay.OverlaySlime;
import com.craftingdead.client.mapwriter.overlay.OverlaySlime.a;

import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;

import java.awt.Point;


public class OverlaySlime implements IMwDataProvider {
   public ArrayList a(int var1, double var2, double var4, double var6, double var8, double var10, double var12) {
      if(Minecraft.getMinecraft().thePlayer.getEntityWorld().provider.dimensionId != var1) {
         return new ArrayList();
      } else {
         int var14 = (MathHelper.ceiling_double_int(var6) >> 4) - 1;
         int var15 = (MathHelper.ceiling_double_int(var8) >> 4) - 1;
         int var16 = (MathHelper.ceiling_double_int(var10) >> 4) + 1;
         int var17 = (MathHelper.ceiling_double_int(var12) >> 4) + 1;
         int var18 = (MathHelper.ceiling_double_int(var2) >> 4) + 1;
         int var19 = (MathHelper.ceiling_double_int(var4) >> 4) + 1;
         int var20 = Math.max(var14, var18 - 100);
         int var21 = Math.min(var16, var18 + 100);
         int var22 = Math.max(var15, var19 - 100);
         int var23 = Math.min(var17, var19 + 100);
         ArrayList var24 = new ArrayList();

         for(int var25 = var20; var25 <= var21; ++var25) {
            for(int var26 = var22; var26 <= var23; ++var26) {
               Random var27 = new Random(Minecraft.getMinecraft().theWorld.getSeed() + (long)(var25 * var25 * 4987142) + (long)(var25 * 5947611) + (long)(var26 * var26) * 4392871L + (long)(var26 * 389711) ^ 987234911L);
               if(var27.nextInt(10) == 0) {
                  var24.add(new a(this, var25, var26));
               }
            }
         }

         return var24;
      }
   }

   public String a(int var1, int var2, int var3, int var4) {
      return "";
   }

   public void a(int var1, int var2, int var3, MapView var4) {
   }

   public void a(int var1, MapView var2) {
   }

   public void a(double var1, double var3, MapView var5) {
   }

   public void b(int var1, MapView var2) {
   }

   public void a(MapView var1) {
   }

   public void b(MapView var1) {
   }

   public void a(MapView var1, MapMode var2) {
   }

   public boolean b(MapView var1, MapMode var2) {
      return false;
   }
   
   class a implements IMwChunkOverlay {
	   Point a;
	   final OverlaySlime b;

	   public a(OverlaySlime var1, int var2, int var3) {
	      this.b = var1;
	      this.a = new Point(var2, var3);
	   }

	   public Point a() {
	      return this.a;
	   }

	   public int b() {
	      return 1342242560;
	   }

	   public float c() {
	      return 1.0F;
	   }

	   public boolean d() {
	      return true;
	   }

	   public float e() {
	      return 0.5F;
	   }

	   public int f() {
	      return -16777216;
	   }
	}
}
