package com.craftingdead.client.mapwriter;

import net.minecraft.util.*;

import com.craftingdead.client.mapwriter.region.*;
import com.craftingdead.client.mapwriter.region.BlockColours.BlockType;

import net.minecraft.block.*;
import net.minecraft.world.biome.*;
import net.minecraft.client.*;
import net.minecraft.client.renderer.texture.*;

public class BlockColourGen
{
    private static int a(final Icon icon, final Texture texture) {
        final int round = Math.round(texture.a * Math.min(icon.getMinU(), icon.getMaxU()));
        final int round2 = Math.round(texture.b * Math.min(icon.getMinV(), icon.getMaxV()));
        final int round3 = Math.round(texture.a * Math.abs(icon.getMaxU() - icon.getMinU()));
        final int round4 = Math.round(texture.b * Math.abs(icon.getMaxV() - icon.getMinV()));
        final int[] array = new int[round3 * round4];
        texture.a(round, round2, round3, round4, array, 0, round3);
        return Render.a(array);
    }
    
    private static int a(final BlockColours blockColours, final int n, int b) {
        final Block block = Block.blocksList[n >> 4];
		BlockType blockType = blockColours.b(n);
        switch (blockType) {
            case OPAQUE: {
                b |= 0xFF000000;
            }
            case NORMAL: {
                try {
                    final int renderColor = block.getRenderColor(n & 0xF);
                    if (renderColor != 16777215) {
                        b = Render.b(b, 0xFF000000 | renderColor);
                    }
                }
                catch (RuntimeException ex) {}
                break;
            }
            case LEAVES: {
                b |= 0xFF000000;
                break;
            }
        }
        return b;
    }
    
    private static void b(final BlockColours blockColours) {
        for (int i = 0; i < BiomeGenBase.biomeList.length; ++i) {
            if (BiomeGenBase.biomeList[i] != null) {
                blockColours.d(i, BiomeGenBase.biomeList[i].getWaterColorMultiplier() & 0xFFFFFF);
                blockColours.e(i, BiomeGenBase.biomeList[i].getBiomeGrassColor() & 0xFFFFFF);
                blockColours.f(i, BiomeGenBase.biomeList[i].getBiomeFoliageColor() & 0xFFFFFF);
            }
            else {
                blockColours.d(i, 16777215);
                blockColours.e(i, 16777215);
                blockColours.f(i, 16777215);
            }
        }
    }
    
    public static void a(final BlockColours blockColours) {
        MapUtil.e("generating block map colours from textures", new Object[0]);
        final int glTextureId = Minecraft.getMinecraft().renderEngine.getTexture(TextureMap.locationBlocksTexture).getGlTextureId();
        if (glTextureId == 0) {
            MapUtil.e("error: could get terrain texture ID", new Object[0]);
            return;
        }
        final Texture texture = new Texture(glTextureId);
        double n = 0.0;
        double n2 = 0.0;
        double n3 = 0.0;
        double n4 = 0.0;
        int n5 = 0;
        int n6 = 0;
        int n7 = 0;
        int n8 = 0;
        for (int i = 0; i < Block.blocksList.length; ++i) {
            for (int j = 0; j < 16; ++j) {
                final int n9 = (i & 0xFFF) << 4 | (j & 0xF);
                final Block block = Block.blocksList[i];
                int n10 = 0;
                if (block != null) {
                    Icon icon = null;
                    try {
                        icon = block.getIcon(1, j);
                    }
                    catch (Exception ex) {
                        ++n6;
                    }
                    if (icon != null) {
                        final double n11 = icon.getMinU();
                        final double n12 = icon.getMaxU();
                        final double n13 = icon.getMinV();
                        final double n14 = icon.getMaxV();
                        if (n11 == n && n12 == n2 && n13 == n3 && n14 == n4) {
                            n10 = n5;
                            ++n8;
                        }
                        else {
                            n10 = a(icon, texture);
                            n = n11;
                            n2 = n12;
                            n3 = n13;
                            n4 = n14;
                            n5 = n10;
                            ++n7;
                        }
                    }
                    n10 = a(blockColours, n9, n10);
                }
                blockColours.a(n9, n10);
            }
        }
        MapUtil.e("processed %d block textures, %d skipped, %d exceptions", new Object[] { n7, n8, n6 });
        b(blockColours);
    }
}
