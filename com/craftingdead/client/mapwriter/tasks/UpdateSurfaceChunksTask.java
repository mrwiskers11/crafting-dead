package com.craftingdead.client.mapwriter.tasks;

import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.map.MapTexture;
import com.craftingdead.client.mapwriter.region.MwChunk;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.Task;

public class UpdateSurfaceChunksTask extends Task {

   MwChunk[] a;
   RegionManager b;
   MapTexture c;


   public UpdateSurfaceChunksTask(MapManager var1, MwChunk[] var2) {
      this.c = var1.S;
      this.b = var1.X;
      this.a = var2;
   }

   public void run() {
      MwChunk[] var1 = this.a;
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         MwChunk var4 = var1[var3];
         if(var4 != null) {
            this.b.a(var4);
            this.c.a(this.b, var4.b << 4, var4.c << 4, 16, 16, var4.d);
         }
      }

   }

   public void a() {}
}
