package com.craftingdead.client.mapwriter.tasks;

import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.region.BlockColours;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.Task;

public class RebuildRegionsTask extends Task {

   final RegionManager a;
   final BlockColours b;
   final int c;
   final int d;
   final int e;
   final int f;
   final int g;
   String h = "";


   public RebuildRegionsTask(MapManager var1, int var2, int var3, int var4, int var5, int var6) {
      this.a = var1.X;
      this.b = var1.W;
      this.c = var2;
      this.d = var3;
      this.e = var4;
      this.f = var5;
      this.g = var6;
   }

   public void run() {
      this.a.c = this.b;
      this.a.a(this.c, this.d, this.e, this.f, this.g);
   }

   public void a() {
      MapUtil.b("rebuild task complete");
   }
}
