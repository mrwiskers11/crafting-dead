package com.craftingdead.client.mapwriter.tasks;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public abstract class Task implements Runnable {

   private Future a = null;


   public abstract void a();

   public abstract void run();

   public final Future b() {
      return this.a;
   }

   public final void a(Future var1) {
      this.a = var1;
   }

   public final boolean c() {
      return this.a != null?this.a.isDone():false;
   }

   public final void d() {
      if(this.a != null) {
         try {
            this.a.get();
         } catch (ExecutionException var3) {
            Throwable var2 = var3.getCause();
            var2.printStackTrace();
         } catch (Exception var4) {
            var4.printStackTrace();
         }
      }

   }
}
