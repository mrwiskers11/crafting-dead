package com.craftingdead.client.mapwriter.tasks;

import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.Task;

public class CloseRegionManagerTask extends Task {

   private final RegionManager a;


   public CloseRegionManagerTask(RegionManager var1) {
      this.a = var1;
   }

   public void run() {
      this.a.a();
   }

   public void a() {}
}
