package com.craftingdead.client.mapwriter.tasks;

import com.craftingdead.client.mapwriter.region.MwChunk;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.Task;

public class SaveChunkTask extends Task {

   private final MwChunk a;
   private final RegionManager b;


   public SaveChunkTask(MwChunk var1, RegionManager var2) {
      this.a = var1;
      this.b = var2;
   }

   public void run() {
      this.a.a(this.b.d);
   }

   public void a() {}
}
