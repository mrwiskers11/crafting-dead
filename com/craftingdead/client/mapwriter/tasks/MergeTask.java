package com.craftingdead.client.mapwriter.tasks;

import java.io.File;

import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.region.MergeToImage;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.Task;

public class MergeTask extends Task {

   final RegionManager a;
   final File b;
   final String c;
   final int d;
   final int e;
   final int f;
   final int g;
   final int h;
   String i = "";


   public MergeTask(RegionManager var1, int var2, int var3, int var4, int var5, int var6, File var7, String var8) {
      this.a = var1;
      this.d = var2;
      this.e = var3;
      this.f = var4;
      this.g = var5;
      this.h = var6;
      this.b = var7;
      this.c = var8;
   }

   public void run() {
      int var1 = MergeToImage.a(this.a, this.d, this.e, this.f, this.g, this.h, this.b, this.c);
      if(var1 > 0) {
         this.i = String.format("successfully wrote merged images to %s/%s.*.*.png", new Object[]{this.b, this.c});
      } else {
         this.i = String.format("merge error: could not write images to %s", new Object[]{this.b});
      }

   }

   public void a() {
      MapUtil.b(this.i);
   }
}
