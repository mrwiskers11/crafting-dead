package com.craftingdead.client.mapwriter.map;

import com.craftingdead.client.mapwriter.map.MapTexture;
import com.craftingdead.client.mapwriter.map.MapViewRequest;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.Task;

public class MapUpdateViewTask extends Task {

   final MapViewRequest a;
   RegionManager b;
   MapTexture c;


   public MapUpdateViewTask(MapTexture var1, RegionManager var2, MapViewRequest var3) {
      this.c = var1;
      this.b = var2;
      this.a = var3;
   }

   public void run() {
      this.c.a(this.b, this.a);
   }

   public void a() {
      this.c.a(this.a);
   }
}
