package com.craftingdead.client.mapwriter.map;

import java.util.ArrayList;
import java.util.List;

import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.map.MapRenderer;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.mapmode.LargeMapMode;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;
import com.craftingdead.client.mapwriter.map.mapmode.SmallMapMode;

public class MiniMap {

   private MapManager k;
   public static final String a = "smallMap";
   public static final String b = "largeMap";
   public static final String c = "undergroundMap";
   public MapMode d;
   public MapMode e;
   public MapMode f;
   public MapView g;
   public MapRenderer h;
   public MapRenderer i;
   private List l;
   private MapRenderer m = null;
   public int j = 0;


   public MiniMap(MapManager var1) {
      this.k = var1;
      this.j = var1.c.a("options", "overlayModeIndex", this.j, 0, 1000);
      int var2 = var1.c.a("options", "overlayZoomLevel", 0, var1.m, var1.l);
      this.g = new MapView(var1);
      this.g.a(var2);
      this.d = new SmallMapMode(this.k.c);
      this.h = new MapRenderer(var1, this.d, this.g);
      this.e = new LargeMapMode(this.k.c);
      this.i = new MapRenderer(var1, this.e, this.g);
      this.l = new ArrayList();
      if(this.d.j) {
         this.l.add(this.h);
      }

      if(this.e.j) {
         this.l.add(this.i);
      }

      this.l.add((Object)null);
      this.a(0);
      this.m = (MapRenderer)this.l.get(this.j);
   }

   public void a() {
      this.l.clear();
      this.m = null;
      this.d.c();
      this.e.c();
      this.k.c.a("options", "overlayModeIndex", this.j);
      this.k.c.a("options", "overlayZoomLevel", this.g.e());
   }

   public MapRenderer a(int var1) {
      int var2 = this.l.size();
      this.j = (this.j + var2 + var1) % var2;
      this.m = (MapRenderer)this.l.get(this.j);
      return this.m;
   }

   public void b() {
      boolean var1 = this.d.f();
      this.e.a(var1);
   }

   public void c() {
      if(this.m != null) {
         this.m.a();
      }

   }
}
