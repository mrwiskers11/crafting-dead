package com.craftingdead.client.mapwriter.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.craftingdead.client.mapwriter.BackgroundExecutor;
import com.craftingdead.client.mapwriter.Texture;
import com.craftingdead.client.mapwriter.map.MapUpdateViewTask;
import com.craftingdead.client.mapwriter.map.MapViewRequest;
import com.craftingdead.client.mapwriter.region.Region;
import com.craftingdead.client.mapwriter.region.RegionManager;

public class MapTexture extends Texture {

   public int c;
   public int d;
   private MapViewRequest e = null;
   private MapViewRequest f = null;
   private Region[] g;
   private List h = new ArrayList();


   public MapTexture(int var1, boolean var2) {
      super(var1, var1, 0, 9729, 9729, 10497);
      this.a(var2);
      this.c = var1 >> 9;
      this.d = var1;
      this.g = new Region[this.c * this.c];
   }

   public void a(MapViewRequest var1, BackgroundExecutor var2, RegionManager var3) {
      if(this.f == null || !this.f.a(var1)) {
         this.f = var1;
         var2.a(new MapUpdateViewTask(this, var3, var1));
      }

   }

   public void d() {
      List var1 = this.h;
      synchronized(this.h) {
         Iterator var2 = this.h.iterator();

         while(var2.hasNext()) {
            MapTexture.a var3 = (MapTexture.a)var2.next();
            this.a(var3.a, var3.b, var3.c, var3.d);
         }

         this.h.clear();
      }
   }

   public void a(MapViewRequest var1) {
      this.e = var1;
   }

   public boolean b(MapViewRequest var1) {
      return this.e != null && this.e.b(var1);
   }

   public synchronized void c(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7) {
      int var8 = var2 * super.a + var1;

      for(int var9 = 0; var9 < var4; ++var9) {
         this.a(var8 + var9 * super.a);
         int var10 = var6 + var9 * var7;

         for(int var11 = 0; var11 < var3; ++var11) {
            int var12 = var5[var10 + var11];
            if(var12 != 0) {
               var12 |= -16777216;
            }

            this.b(var12);
         }
      }

   }

   public void b(int var1, int var2, int var3, int var4) {
      List var5 = this.h;
      synchronized(this.h) {
         this.h.add(new MapTexture.a(var1, var2, var3, var4));
      }
   }

   public void a(Region var1, int var2, int var3, int var4, int var5) {
      int var6 = var2 >> var1.h & super.a - 1;
      int var7 = var3 >> var1.h & super.b - 1;
      int var8 = var4 >> var1.h;
      int var9 = var5 >> var1.h;
      var8 = Math.min(var8, super.a - var6);
      var9 = Math.min(var9, super.b - var9);
      int[] var10 = var1.d();
      if(var10 != null) {
         this.c(var6, var7, var8, var9, var10, var1.a(var2, var3), 512);
      } else {
         this.a(var6, var7, var8, var9, 0);
      }

      this.b(var6, var7, var8, var9);
   }

   public int c(int var1, int var2, int var3) {
      var1 = var1 >> 9 + var3 & this.c - 1;
      var2 = var2 >> 9 + var3 & this.c - 1;
      return var2 * this.c + var1;
   }

   public boolean a(RegionManager var1, int var2, int var3, int var4, int var5) {
      boolean var6 = false;
      int var7 = this.c(var2, var3, var4);
      Region var8 = this.g[var7];
      if(var8 == null || !var8.a(var2, var3, var4, var5)) {
         Region var9 = var1.a(var2, var3, var4, var5);
         this.g[var7] = var9;
         this.a(var9, var9.e, var9.f, var9.j, var9.j);
         var6 = true;
      }

      return var6;
   }

   public int a(RegionManager var1, MapViewRequest var2) {
      int var3 = 512 << var2.e;
      int var4 = 0;

      for(int var5 = var2.c; var5 <= var2.d; var5 += var3) {
         for(int var6 = var2.a; var6 <= var2.b; var6 += var3) {
            if(this.a(var1, var6, var5, var2.e, var2.f)) {
               ++var4;
            }
         }
      }

      return var4;
   }

   public void a(RegionManager var1, int var2, int var3, int var4, int var5, int var6) {
      for(int var7 = 0; var7 < this.g.length; ++var7) {
         Region var8 = this.g[var7];
         if(var8 != null && var8.a(var2, var3, var4, var5, var6)) {
            this.a(var8, var2, var3, var4, var5);
         }
      }

   }

   private class a {

      final int a;
      final int b;
      final int c;
      final int d;


      a(int var2, int var3, int var4, int var5) {
         this.a = var2;
         this.b = var3;
         this.c = var4;
         this.d = var5;
      }
   }
}
