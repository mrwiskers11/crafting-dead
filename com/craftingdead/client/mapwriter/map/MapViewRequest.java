package com.craftingdead.client.mapwriter.map;

import com.craftingdead.client.mapwriter.map.MapView;

public class MapViewRequest {

   public final int a;
   public final int b;
   public final int c;
   public final int d;
   public final int e;
   public final int f;


   public MapViewRequest(MapView var1) {
      this.e = var1.f();
      int var2 = 512 << this.e;
      this.a = (int)var1.h() & -var2;
      this.c = (int)var1.j() & -var2;
      this.b = (int)var1.i() & -var2;
      this.d = (int)var1.k() & -var2;
      this.f = var1.g();
   }

   public boolean a(MapViewRequest var1) {
      return var1 != null && var1.e == this.e && var1.f == this.f && var1.a == this.a && var1.b == this.b && var1.c == this.c && var1.d == this.d;
   }

   public boolean b(MapViewRequest var1) {
      return var1 != null && var1.e == this.e && var1.f == this.f;
   }
}
