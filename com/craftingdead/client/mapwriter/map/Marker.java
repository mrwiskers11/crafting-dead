package com.craftingdead.client.mapwriter.map;

import java.awt.geom.Point2D.Double;

import com.craftingdead.client.mapwriter.Render;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class Marker {

   public final String a;
   public final String b;
   public int c;
   public int d;
   public int e;
   public int f;
   public int g;
   public Double h = new Double(0.0D, 0.0D);
   private static int[] i = new int[]{16711680, '\uff00', 255, 16776960, 16711935, '\uffff', 16744448, 8388863};
   private static int j = 0;


   public Marker(String var1, String var2, int var3, int var4, int var5, int var6, int var7) {
      this.a = var1;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
      this.g = var7;
      this.b = var2;
   }

   public String a() {
      return String.format("%s %s (%d, %d, %d) %d %06x", new Object[]{this.a, this.b, Integer.valueOf(this.c), Integer.valueOf(this.d), Integer.valueOf(this.e), Integer.valueOf(this.f), Integer.valueOf(this.g & 16777215)});
   }

   public static int b() {
      return -16777216 | i[j];
   }

   public void c() {
      j = (j + 1) % i.length;
      this.g = b();
   }

   public void d() {
      j = (j + i.length - 1) % i.length;
      this.g = b();
   }

   public void a(MapMode var1, MapView var2, int var3) {
      double var4 = var2.e(this.f);
      Double var6 = var1.b(var2, (double)this.c * var4, (double)this.e * var4);
      this.h.setLocation(var6.x + (double)var1.b, var6.y + (double)var1.c);
      double var7 = (double)var1.p;
      double var9 = (double)var1.p / 2.0D;
      Render.a(var3);
      Render.c(var6.x - var9, var6.y - var9, var7, var7);
      Render.a(this.g);
      Render.c(var6.x - var9 + 0.5D, var6.y - var9 + 0.5D, var7 - 1.0D, var7 - 1.0D);
   }

   public boolean equals(Object var1) {
      if(this == var1) {
         return true;
      } else if(!(var1 instanceof Marker)) {
         return false;
      } else {
         Marker var2 = (Marker)var1;
         return this.a == var2.a && this.b == var2.b && this.c == var2.c && this.d == var2.d && this.e == var2.e && this.f == var2.f;
      }
   }

}
