package com.craftingdead.client.mapwriter.map;

import java.util.List;

import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.api.MwAPI;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class MapView {

   private int a = 0;
   private int b = 0;
   private int c = 2048;
   private double d = 0.0D;
   private double e = 0.0D;
   private int f = 0;
   private int g = 0;
   private int h = 1;
   private int i = 1;
   private double j = 1.0D;
   private double k = 1.0D;
   private int l;
   private int m;


   public MapView(MapManager var1) {
      this.l = var1.m;
      this.m = var1.l;
      this.a(0);
      this.a(var1.D, var1.E);
   }

   public void a(double var1, double var3) {
      this.d = var1;
      this.e = var3;
      if(MwAPI.b() != null) {
         MwAPI.b().a(var1, var3, this);
      }

   }

   public double a() {
      return this.d;
   }

   public double b() {
      return this.e;
   }

   public double c() {
      return this.j;
   }

   public double d() {
      return this.k;
   }

   public void b(double var1, double var3) {
      this.a(this.d + var1 * this.j, this.e + var3 * this.k);
   }

   public int a(int var1) {
      int var2 = this.a;
      this.a = Math.min(Math.max(this.l, var1), this.m);
      if(var2 != this.a) {
         this.m();
      }

      return this.a;
   }

   private void m() {
      if(this.a >= 0) {
         this.j = (double)(this.h << this.a);
         this.k = (double)(this.i << this.a);
      } else {
         this.j = (double)(this.h >> -this.a);
         this.k = (double)(this.i >> -this.a);
      }

      if(MwAPI.b() != null) {
         MwAPI.b().b(this.e(), this);
      }

   }

   public int b(int var1) {
      return this.a(this.a + var1);
   }

   public int e() {
      return this.a;
   }

   public int f() {
      return Math.max(0, this.a);
   }

   public void a(int var1, double var2, double var4) {
      int var6 = this.a;
      var1 = this.a(var1);
      double var7 = Math.pow(2.0D, (double)(var1 - var6));
      this.a(var2 - (var2 - this.d) * var7, var4 - (var4 - this.e) * var7);
   }

   public void c(int var1) {
      double var2 = 1.0D;
      if(var1 != this.b) {
         if(this.b != -1 && var1 == -1) {
            var2 = 0.125D;
         } else if(this.b == -1 && var1 != -1) {
            var2 = 8.0D;
         }

         this.b = var1;
         this.a(this.d * var2, this.e * var2);
      }

      if(MwAPI.b() != null) {
         MwAPI.b().a(this.b, this);
      }

   }

   public void d(int var1) {
      byte var2 = 0;
      if(this.b != -1 && var1 == -1) {
         var2 = -3;
      } else if(this.b == -1 && var1 != -1) {
         var2 = 3;
      }

      this.a(this.e() + var2);
      this.c(var1);
   }

   public void a(List var1, int var2) {
      int var3 = var1.indexOf(Integer.valueOf(this.b));
      var3 = Math.max(0, var3);
      int var4 = var1.size();
      int var5 = ((Integer)var1.get((var3 + var4 + var2) % var4)).intValue();
      this.d(var5);
   }

   public int g() {
      return this.b;
   }

   public void a(int var1, int var2) {
      if(this.f != var1 || this.g != var2) {
         this.f = var1;
         this.g = var2;
         this.n();
      }

   }

   public void a(MapMode var1) {
      this.a(var1.h, var1.i);
   }

   public double h() {
      return this.d - this.j / 2.0D;
   }

   public double i() {
      return this.d + this.j / 2.0D;
   }

   public double j() {
      return this.e - this.k / 2.0D;
   }

   public double k() {
      return this.e + this.k / 2.0D;
   }

   public double e(int var1) {
      double var2;
      if(this.b != -1 && var1 == -1) {
         var2 = 8.0D;
      } else if(this.b == -1 && var1 != -1) {
         var2 = 0.125D;
      } else {
         var2 = 1.0D;
      }

      return var2;
   }

   public void a(double var1, double var3, int var5) {
      double var6 = this.e(var5);
      this.a(var1 * var6, var3 * var6);
   }

   public void f(int var1) {
      if(this.c != var1) {
         this.c = var1;
         this.n();
      }

   }

   private void n() {
      int var1 = this.f;
      int var2 = this.g;

      for(int var3 = this.c / 2; var1 > var3 || var2 > var3; var2 /= 2) {
         var1 /= 2;
      }

      this.h = var1;
      this.i = var2;
      this.m();
   }

   public int l() {
      return this.f / this.h;
   }

   public boolean a(double var1, double var3, boolean var5) {
      boolean var6;
      if(!var5) {
         var6 = var1 > this.h() || var1 < this.i() || var3 > this.j() || var3 < this.k();
      } else {
         double var7 = var1 - this.d;
         double var9 = var3 - this.e;
         double var11 = this.d() / 2.0D;
         var6 = var7 * var7 + var9 * var9 < var11 * var11;
      }

      return var6;
   }
}
