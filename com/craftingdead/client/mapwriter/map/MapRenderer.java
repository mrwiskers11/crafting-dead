package com.craftingdead.client.mapwriter.map;

import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.Render;
import com.craftingdead.client.mapwriter.api.IMwChunkOverlay;
import com.craftingdead.client.mapwriter.api.IMwDataProvider;
import com.craftingdead.client.mapwriter.api.MwAPI;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.MapViewRequest;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class MapRenderer {

   private MapManager b;
   private MapMode c;
   private MapView d;
   public Double a = new Double(0.0D, 0.0D);
   private ResourceLocation e = new ResourceLocation("craftingdead", "textures/gui/background.png");
   private ResourceLocation f = new ResourceLocation("craftingdead", "textures/gui/border_round.png");
   private ResourceLocation g = new ResourceLocation("craftingdead", "textures/gui/border_square.png");
   private ResourceLocation h = new ResourceLocation("craftingdead", "textures/gui/arrow_player.png");
   private ResourceLocation i = new ResourceLocation("craftingdead", "textures/gui/arrow_north.png");


   public MapRenderer(MapManager var1, MapMode var2, MapView var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   private void b() {
      int var1 = Math.max(0, this.d.e());
      double var2 = (double)this.b.q;
      double var4 = (double)(1 << var1);
      double var6;
      double var8;
      double var10;
      double var12;
      if(!this.c.l && this.b.p && this.d.e() >= 0) {
         var6 = (double)Math.round(this.d.h() / var4) / var2 % 1.0D;
         var8 = (double)Math.round(this.d.j() / var4) / var2 % 1.0D;
         var10 = (double)Math.round(this.d.c() / var4) / var2;
         var12 = (double)Math.round(this.d.d() / var4) / var2;
      } else {
         double var14 = var2 * var4;
         var6 = this.d.h() / var14 % 1.0D;
         var8 = this.d.j() / var14 % 1.0D;
         var10 = this.d.c() / var14;
         var12 = this.d.d() / var14;
      }

      GL11.glPushMatrix();
      if(this.c.k) {
         GL11.glRotated(this.b.L, 0.0D, 0.0D, 1.0D);
      }

      if(this.c.l) {
         Render.b(0.0D, 0.0D, (double)this.c.g / 2.0D);
      }

      MapViewRequest var25 = new MapViewRequest(this.d);
      this.b.S.a(var25, this.b.T, this.b.X);
      if(this.b.y > 0) {
         double var15 = 0.0D;
         double var17 = 1.0D;
         double var19 = 0.0D;
         double var21 = 1.0D;
         if(this.b.y == 2) {
            double var23 = var2 / 256.0D;
            var15 = var6 * var23;
            var17 = (var6 + var10) * var23;
            var19 = var8 * var23;
            var21 = (var8 + var12) * var23;
         }

         this.b.a.renderEngine.bindTexture(this.e);
         Render.a(16777215, this.c.r);
         Render.a((double)this.c.d, (double)this.c.e, (double)this.c.f, (double)this.c.g, var15, var19, var17, var21);
      } else {
         Render.a(0, this.c.r);
         Render.c((double)this.c.d, (double)this.c.e, (double)this.c.f, (double)this.c.g);
      }

      if(this.b.S.b(var25)) {
         this.b.S.b();
         Render.a(16777215, this.c.r);
         Render.a((double)this.c.d, (double)this.c.e, (double)this.c.f, (double)this.c.g, var6, var8, var6 + var10, var8 + var12);
      }

      if(this.c.l) {
         Render.f();
      }

      GL11.glPopMatrix();
   }

   private void c() {
      if(this.c.l) {
         this.b.a.renderEngine.bindTexture(this.f);
      } else {
         this.b.a.renderEngine.bindTexture(this.g);
      }

      Render.a(-1);
      Render.a((double)this.c.d / 0.75D, (double)this.c.e / 0.75D, (double)this.c.f / 0.75D, (double)this.c.g / 0.75D, 0.0D, 0.0D, 1.0D, 1.0D);
   }

   private void d() {
      GL11.glPushMatrix();
      double var1 = this.d.e(this.b.K);
      Double var3 = this.c.b(this.d, this.b.D * var1, this.b.E * var1);
      this.a.setLocation(var3.x + (double)this.c.b, var3.y + (double)this.c.c);
      GL11.glTranslated(var3.x, var3.y, 0.0D);
      if(!this.c.k) {
         GL11.glRotated(-this.b.L, 0.0D, 0.0D, 1.0D);
      }

      double var4 = (double)this.c.o;
      Render.a(-1);
      this.b.a.renderEngine.bindTexture(this.h);
      Render.a(-var4, -var4, var4 * 2.0D, var4 * 2.0D, 0.0D, 0.0D, 1.0D, 1.0D);
      GL11.glPopMatrix();
   }

   private void e() {
      GL11.glPushMatrix();
      if(this.c.k) {
         GL11.glRotated(this.b.L, 0.0D, 0.0D, 1.0D);
      }

      this.b.V.a(this.c, this.d);
      if(this.b.Z.d) {
         this.b.Z.a(this.c, this.d);
      }

      if(this.c.k) {
         double var1 = (double)this.c.g / 2.0D;
         double var3 = (double)this.c.o;
         Render.a(-1);
         this.b.a.renderEngine.bindTexture(this.i);
         Render.a(-var3, -var1 - var3 * 2.0D, var3 * 2.0D, var3 * 2.0D, 0.0D, 0.0D, 1.0D, 1.0D);
      }

      GL11.glPopMatrix();
      this.d();
   }

   private void f() {
      if(this.c.m) {
         GL11.glPushMatrix();
         GL11.glTranslatef((float)this.c.x, (float)this.c.y, 0.0F);
         if(this.b.h != 2) {
            GL11.glScalef(0.5F, 0.5F, 1.0F);
         }

         if(this.b.h > 0) {
            Render.b(0, 0, this.c.z, "%d, %d, %d", new Object[]{Integer.valueOf(this.b.G), Integer.valueOf(this.b.H), Integer.valueOf(this.b.I)});
         }

         GL11.glPopMatrix();
      }

   }

   private IMwDataProvider g() {
      IMwDataProvider var1 = MwAPI.b();
      if(var1 != null) {
         ArrayList var2 = var1.a(this.d.g(), this.d.a(), this.d.b(), this.d.h(), this.d.j(), this.d.i(), this.d.k());
         if(var2 != null) {
            Iterator var3 = var2.iterator();

            while(var3.hasNext()) {
               IMwChunkOverlay var4 = (IMwChunkOverlay)var3.next();
               a(this.c, this.d, var4);
            }
         }
      }

      return var1;
   }

   public void a() {
      this.c.d();
      this.d.a(this.c);
      this.d.f(this.b.q);
      GL11.glPushMatrix();
      GL11.glLoadIdentity();
      GL11.glTranslated((double)this.c.b, (double)this.c.c, -2000.0D);
      this.b();
      if(this.c.n > 0) {
         this.c();
      }

      this.e();
      IMwDataProvider var1 = this.g();
      this.f();
      GL11.glEnable(2929);
      GL11.glPopMatrix();
      if(var1 != null) {
         GL11.glPushMatrix();
         var1.a(this.d, this.c);
         GL11.glPopMatrix();
      }

   }

   private static void a(MapMode var0, MapView var1, IMwChunkOverlay var2) {
      int var3 = var2.a().x;
      int var4 = var2.a().y;
      float var5 = var2.c();
      Double var6 = var0.a(var1, (double)(var3 << 4), (double)(var4 << 4));
      Double var7 = var0.a(var1, (double)(var3 + 1 << 4), (double)(var4 + 1 << 4));
      var6.x = Math.max((double)var0.d, var6.x);
      var6.x = Math.min((double)(var0.d + var0.f), var6.x);
      var6.y = Math.max((double)var0.e, var6.y);
      var6.y = Math.min((double)(var0.e + var0.g), var6.y);
      var7.x = Math.max((double)var0.d, var7.x);
      var7.x = Math.min((double)(var0.d + var0.f), var7.x);
      var7.y = Math.max((double)var0.e, var7.y);
      var7.y = Math.min((double)(var0.e + var0.g), var7.y);
      double var8 = (var7.x - var6.x) * (double)var5;
      double var10 = (var7.y - var6.y) * (double)var5;
      double var12 = (var7.x - var6.x - var8) / 2.0D;
      double var14 = (var7.y - var6.y - var10) / 2.0D;
      if(var2.d()) {
         Render.a(var2.f());
         Render.a(var6.x + 1.0D, var6.y + 1.0D, var7.x - var6.x - 1.0D, var7.y - var6.y - 1.0D, (double)var2.e());
      }

      Render.a(var2.b());
      Render.c(var6.x + var12 + 1.0D, var6.y + var14 + 1.0D, var8 - 1.0D, var10 - 1.0D);
   }
}
