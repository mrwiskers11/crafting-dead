package com.craftingdead.client.mapwriter.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.Marker;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class MarkerManager {

   public List a = new ArrayList();
   public List b = new ArrayList();
   public List c = new ArrayList();
   private String e = "none";
   public Marker d = null;


   public void a(MapConfig var1, String var2) {
      this.a.clear();
      if(var1.hasCategory(var2)) {
         int var3 = var1.get(var2, "markerCount", 0).getInt();
         this.e = var1.get(var2, "visibleGroup", "").getString();
         if(var3 > 0) {
            for(int var4 = 0; var4 < var3; ++var4) {
               String var5 = "marker" + var4;
               String var6 = var1.get(var2, var5, "").getString();
               Marker var7 = this.b(var6);
               if(var7 != null) {
                  this.b(var7);
               } else {
                  MapUtil.e("error: could not load " + var5 + " from config file", new Object[0]);
               }
            }
         }
      }

      this.c();
   }

   public void b(MapConfig var1, String var2) {
      var1.get(var2, "markerCount", 0).set(this.a.size());
      var1.get(var2, "visibleGroup", "").set(this.e);
      int var3 = 0;

      for(Iterator var4 = this.a.iterator(); var4.hasNext(); ++var3) {
         Marker var5 = (Marker)var4.next();
         String var6 = "marker" + var3;
         String var7 = this.a(var5);
         var1.get(var2, var6, "").set(var7);
      }

   }

   public void a(String var1) {
      if(var1 != null) {
         this.e = MapUtil.a(var1);
      } else {
         this.e = "none";
      }

   }

   public String a() {
      return this.e;
   }

   public void b() {
      this.a.clear();
      this.b.clear();
      this.c.clear();
      this.e = "none";
   }

   public String a(Marker var1) {
      return String.format("%s %d %d %d %d %06x %s", new Object[]{var1.a, Integer.valueOf(var1.c), Integer.valueOf(var1.d), Integer.valueOf(var1.e), Integer.valueOf(var1.f), Integer.valueOf(var1.g & 16777215), var1.b});
   }

   public Marker b(String var1) {
      String[] var2 = var1.split(" ");
      Marker var3 = null;
      if(var2.length != 6 && var2.length != 7) {
         MapUtil.e("Marker.stringToMarker: incorrect number of parameters %d, need 6 or 7", new Object[]{Integer.valueOf(var2.length)});
      } else {
         try {
            int var4 = Integer.parseInt(var2[1]);
            int var5 = Integer.parseInt(var2[2]);
            int var6 = Integer.parseInt(var2[3]);
            int var7 = 4;
            int var8 = 0;
            if(var2.length == 7) {
               var8 = Integer.parseInt(var2[4]);
               ++var7;
            }

            int var9 = -16777216 | Integer.parseInt(var2[var7++], 16);
            var3 = new Marker(var2[0], var2[var7++], var4, var5, var6, var8, var9);
         } catch (NumberFormatException var10) {
            var3 = null;
         }
      }

      return var3;
   }

   public void b(Marker var1) {
      this.a.add(var1);
   }

   public void a(String var1, String var2, int var3, int var4, int var5, int var6, int var7) {
      var1 = MapUtil.a(var1);
      var2 = MapUtil.a(var2);
      this.b(new Marker(var1, var2, var3, var4, var5, var6, var7));
   }

   public boolean c(Marker var1) {
      return this.a.remove(var1);
   }

   public boolean a(String var1, String var2) {
      Marker var3 = null;
      Iterator var4 = this.a.iterator();

      while(var4.hasNext()) {
         Marker var5 = (Marker)var4.next();
         if((var1 == null || var5.a.equals(var1)) && (var2 == null || var5.b.equals(var2))) {
            var3 = var5;
            break;
         }
      }

      return this.c(var3);
   }

   public void c() {
      this.c.clear();
      this.b.clear();
      this.b.add("none");
      this.b.add("all");
      Iterator var1 = this.a.iterator();

      while(var1.hasNext()) {
         Marker var2 = (Marker)var1.next();
         if(var2.b.equals(this.e) || this.e.equals("all")) {
            this.c.add(var2);
         }

         if(!this.b.contains(var2.b)) {
            this.b.add(var2.b);
         }
      }

      if(!this.b.contains(this.e)) {
         this.e = "none";
      }

   }

   public void a(int var1) {
      if(this.b.size() > 0) {
         int var2 = this.b.indexOf(this.e);
         int var3 = this.b.size();
         if(var2 != -1) {
            var2 = (var2 + var3 + var1) % var3;
         } else {
            var2 = 0;
         }

         this.e = (String)this.b.get(var2);
      } else {
         this.e = "none";
         this.b.add("none");
      }

   }

   public void d() {
      this.a(1);
   }

   public int c(String var1) {
      int var2 = 0;
      if(var1.equals("all")) {
         var2 = this.a.size();
      } else {
         Iterator var3 = this.a.iterator();

         while(var3.hasNext()) {
            Marker var4 = (Marker)var3.next();
            if(var4.b.equals(var1)) {
               ++var2;
            }
         }
      }

      return var2;
   }

   public void e() {
      if(this.c.size() > 0) {
         int var1 = 0;
         if(this.d != null) {
            var1 = this.c.indexOf(this.d);
            if(var1 == -1) {
               var1 = 0;
            }
         }

         var1 = (var1 + 1) % this.c.size();
         this.d = (Marker)this.c.get(var1);
      } else {
         this.d = null;
      }

   }

   public Marker a(int var1, int var2, int var3) {
      int var4 = var3 * var3;
      Marker var5 = null;
      Iterator var6 = this.c.iterator();

      while(var6.hasNext()) {
         Marker var7 = (Marker)var6.next();
         int var8 = var1 - var7.c;
         int var9 = var2 - var7.e;
         int var10 = var8 * var8 + var9 * var9;
         if(var10 < var4) {
            var5 = var7;
            var4 = var10;
         }
      }

      return var5;
   }

   public Marker a(int var1, int var2, double var3) {
      int var5 = 100000000;
      Marker var6 = null;
      Iterator var7 = this.c.iterator();

      while(var7.hasNext()) {
         Marker var8 = (Marker)var7.next();
         int var9 = var8.c - var1;
         int var10 = var8.e - var2;
         int var11 = var9 * var9 + var10 * var10;
         double var12 = Math.atan2((double)var10, (double)var9);
         if(Math.cos(var3 - var12) > 0.8D && var11 < var5 && var11 > 4) {
            var6 = var8;
            var5 = var11;
         }
      }

      return var6;
   }

   public void a(MapMode var1, MapView var2) {
      Iterator var3 = this.c.iterator();

      while(var3.hasNext()) {
         Marker var4 = (Marker)var3.next();
         var4.a(var1, var2, -16777216);
      }

      if(this.d != null) {
         this.d.a(var1, var2, -1);
      }

   }
}
