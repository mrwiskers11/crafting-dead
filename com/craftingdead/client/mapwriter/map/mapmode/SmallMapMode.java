package com.craftingdead.client.mapwriter.map.mapmode;

import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class SmallMapMode extends MapMode {

   public SmallMapMode(MapConfig var1) {
      super(var1, "smallMap");
      super.w = 30;
      super.s = 10;
      super.t = -1;
      super.u = -1;
      super.v = 10;
      super.o = 4;
      super.p = 3;
      super.m = true;
      this.a();
   }
}
