package com.craftingdead.client.mapwriter.map.mapmode;

import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class LargeMapMode extends MapMode {

   public LargeMapMode(MapConfig var1) {
      super(var1, "largeMap");
      super.w = -1;
      super.s = 10;
      super.t = 40;
      super.u = 40;
      super.v = 40;
      super.o = 5;
      super.p = 5;
      super.m = true;
      this.a();
   }
}
