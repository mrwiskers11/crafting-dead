package com.craftingdead.client.mapwriter.map.mapmode;

import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class FullScreenMapMode extends MapMode {

   public FullScreenMapMode(MapConfig var1) {
      super(var1, "fullScreenMap");
      super.w = -1;
      super.s = 0;
      super.t = 0;
      super.u = 0;
      super.v = 0;
      super.n = 0;
      super.o = 5;
      super.p = 5;
      super.r = 100;
      super.k = false;
      super.l = false;
      super.m = false;
      this.a();
   }
}
