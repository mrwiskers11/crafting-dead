package com.craftingdead.client.mapwriter.map.mapmode;

import java.awt.Point;
import java.awt.geom.Point2D.Double;

import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.map.MapView;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

public class MapMode {

   private final MapConfig A;
   public final String a;
   private int B = 320;
   private int C = 240;
   private double D = 1.0D;
   public int b = 0;
   public int c = 0;
   public int d = -25;
   public int e = -25;
   public int f = 50;
   public int g = 50;
   public int h = 50;
   public int i = 50;
   public boolean j = true;
   public boolean k = true;
   public boolean l = true;
   public boolean m = false;
   public int n = 1;
   public int o = 5;
   public int p = 5;
   public int q = 3;
   public int r = 100;
   public int s = 0;
   public int t = 0;
   public int u = 0;
   public int v = 0;
   public int w = -1;
   public int x = 0;
   public int y = 0;
   public int z = -1;


   public MapMode(MapConfig var1, String var2) {
      this.A = var1;
      this.a = var2;
   }

   public void a() {
      this.j = this.A.a(this.a, "enabled", this.j);
      this.o = this.A.a(this.a, "playerArrowSize", this.o, 1, 20);
      this.p = this.A.a(this.a, "markerSize", this.p, 1, 20);
      this.r = this.A.a(this.a, "alphaPercent", this.r, 0, 100);
      this.w = this.A.a(this.a, "heightPercent", this.w, 0, 100);
      this.s = this.A.a(this.a, "marginTop", this.s, -1, 320);
      this.t = this.A.a(this.a, "marginBottom", this.t, -1, 320);
      this.u = this.A.a(this.a, "marginLeft", this.u, -1, 320);
      this.v = this.A.a(this.a, "marginRight", this.v, -1, 320);
      this.k = this.A.a(this.a, "rotate", this.k);
      this.l = this.A.a(this.a, "circular", this.l);
      this.m = this.A.a(this.a, "coordsEnabled", this.m);
      this.n = this.A.a(this.a, "borderMode", this.n, 0, 1);
      this.q = Math.max(1, this.p - 1);
   }

   public void b() {
      this.A.b(this.a, "enabled", this.j);
      this.A.a(this.a, "heightPercent", this.w);
      this.A.a(this.a, "marginTop", this.s);
      this.A.a(this.a, "marginBottom", this.t);
      this.A.a(this.a, "marginLeft", this.u);
      this.A.a(this.a, "marginRight", this.v);
      this.A.b(this.a, "rotate", this.k);
      this.A.b(this.a, "circular", this.l);
   }

   public void c() {
      this.b();
   }

   public void a(int var1, int var2, int var3, int var4, double var5) {
      if(var3 != this.B || var4 != this.C || var5 != this.D) {
         this.B = var3;
         this.C = var4;
         this.D = var5;
         this.g();
      }

   }

   public void d() {
      Minecraft var1 = Minecraft.getMinecraft();
      ScaledResolution var2 = new ScaledResolution(var1.gameSettings, var1.displayWidth, var1.displayHeight);
      this.a(var1.displayWidth, var1.displayHeight, var2.getScaledWidth(), var2.getScaledHeight(), (double)var2.getScaleFactor());
   }

   public void a(int var1, int var2, int var3, int var4) {
      this.s = var1;
      this.t = var2;
      this.u = var3;
      this.v = var4;
      this.g();
   }

   public void a(int var1) {
      this.w = var1;
      this.g();
   }

   public void e() {
      int var1 = this.w / 5 + 1;
      if(var1 > 12) {
         var1 = 1;
      }

      this.a(var1 * 5);
   }

   private void g() {
      int var1 = this.C * this.w / 100;
      int var2;
      if(this.u >= 0 && this.v >= 0) {
         var2 = this.u;
         this.f = this.B - this.u - this.v;
      } else if(this.u >= 0) {
         var2 = this.u;
         this.f = var1;
      } else if(this.v >= 0) {
         var2 = this.B - var1 - this.v;
         this.f = var1;
      } else {
         var2 = (this.B - var1) / 2;
         this.f = var1;
      }

      int var3;
      if(this.s >= 0 && this.t >= 0) {
         var3 = this.s;
         this.g = this.C - this.s - this.t;
      } else if(this.s >= 0) {
         var3 = this.s;
         this.g = var1;
      } else if(this.t >= 0) {
         var3 = this.C - var1 - this.t;
         this.g = var1;
      } else {
         var3 = (this.C - var1) / 2;
         this.g = var1;
      }

      this.f &= -2;
      this.g &= -2;
      this.b = var2 + (this.f >> 1);
      this.c = var3 + (this.g >> 1);
      if(this.l) {
         this.f = this.g;
      }

      this.d = -(this.f >> 1);
      this.e = -(this.g >> 1);
      this.h = (int)Math.round((double)this.f * this.D);
      this.i = (int)Math.round((double)this.g * this.D);
      this.x = 0;
      this.y = (this.g >> 1) + 4;
   }

   public void a(boolean var1) {
      this.k = var1;
      this.l = var1;
      this.g();
   }

   public boolean f() {
      this.a(!this.k);
      return this.k;
   }

   public Point a(MapView var1, int var2, int var3) {
      double var4 = (double)(var2 - this.b) / (double)this.f;
      double var6 = (double)(var3 - this.c) / (double)this.g;
      int var8 = (int)Math.floor(var1.a() + var4 * var1.c());
      int var9 = (int)Math.floor(var1.b() + var6 * var1.d());
      return new Point(var8, var9);
   }

   public Double a(MapView var1, double var2, double var4) {
      double var6 = (var2 - var1.a()) / var1.c();
      double var8 = (var4 - var1.b()) / var1.d();
      return new Double((double)this.f * var6, (double)this.g * var8);
   }

   public Double b(MapView var1, double var2, double var4) {
      double var6 = (var2 - var1.a()) / var1.c();
      double var8 = (var4 - var1.b()) / var1.d();
      double var10 = 0.49D;
      if(!this.l) {
         if(var6 < -var10) {
            var8 = -var10 * var8 / var6;
            var6 = -var10;
         }

         if(var6 > var10) {
            var8 = var10 * var8 / var6;
            var6 = var10;
         }

         if(var8 < -var10) {
            var6 = -var10 * var6 / var8;
            var8 = -var10;
         }

         if(var8 > var10) {
            var6 = var10 * var6 / var8;
            var8 = var10;
         }

         if(var6 < -var10) {
            var8 = -var10 * var8 / var6;
            var6 = -var10;
         }

         if(var6 > var10) {
            var8 = var10 * var8 / var6;
            var6 = var10;
         }
      } else {
         double var12 = var6 * var6 + var8 * var8;
         if(var12 > var10 * var10) {
            double var14 = Math.atan2(var8, var6);
            var6 = var10 * Math.cos(var14);
            var8 = var10 * Math.sin(var14);
         }
      }

      return new Double((double)this.f * var6, (double)this.g * var8);
   }
}
