package com.craftingdead.client.mapwriter.map;

import java.awt.geom.Point2D.Double;
import java.util.Iterator;
import java.util.LinkedList;

import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.Render;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.Trail;
import com.craftingdead.client.mapwriter.map.Trail.a;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public class Trail {
   private MapManager g;
   public LinkedList a = new LinkedList();
   public int b = 30;
   public String c;
   public boolean d;
   public long e = 0L;
   public long f = 5000L;

   public Trail(MapManager var1, String var2) {
      this.g = var1;
      this.c = var2;
      this.d = this.g.c.a("options", this.c + "TrailEnabled", false);
      this.b = this.g.c.a("options", this.c + "TrailMaxLength", this.b, 1, 200);
      this.f = (long)this.g.c.a("options", this.c + "TrailMarkerIntervalMillis", (int)this.f, 100, 360000);
   }

   public void a() {
      this.g.c.b("options", this.c + "TrailEnabled", this.d);
      this.g.c.a("options", this.c + "TrailMaxLength", this.b);
      this.g.c.a("options", this.c + "TrailMarkerIntervalMillis", (int)this.f);
      this.a.clear();
   }

   public void b() {
      long var1 = System.currentTimeMillis();
      if(var1 - this.e > this.f) {
         this.e = var1;
         this.a(this.g.D, this.g.F, this.g.E, this.g.J);
      }

   }

   public void a(double var1, double var3, double var5, double var7) {
      this.a.add(new a(this, var1, var3, var5, var7));

      while(this.a.size() > this.b) {
         this.a.poll();
      }

      int var9 = this.b - this.a.size();

      for(Iterator var10 = this.a.iterator(); var10.hasNext(); ++var9) {
         a var11 = (a)var10.next();
         var11.e = var9 * 100 / this.b;
      }

   }

   public void a(MapMode var1, MapView var2) {
      Iterator var3 = this.a.iterator();

      while(var3.hasNext()) {
         a var4 = (a)var3.next();
         var4.a(var1, var2);
      }

   }
   
   class a {
	   double a;
	   double b;
	   double c;
	   double d;
	   int e;
	   static final int f = -16777216;
	   static final int g = -16711681;
	   final Trail h;

	   public a(Trail var1, double var2, double var4, double var6, double var8) {
	      this.h = var1;
	      this.a(var2, var4, var6, var8);
	   }

	   public void a(double var1, double var3, double var5, double var7) {
	      this.a = var1;
	      this.b = var3;
	      this.c = var5;
	      this.d = var7;
	      this.e = 100;
	   }

	   public void a(MapMode var1, MapView var2) {
	      if(var2.a(this.a, this.c, var1.l)) {
	         Double var3 = var1.a(var2, this.a, this.c);
	         Render.a(-16777216, this.e);
	         Render.b(var3.x, var3.y, this.d, (double)var1.q);
	         Render.a(-16711681, this.e);
	         Render.b(var3.x, var3.y, this.d, (double)var1.q - 1.0D);
	      }

	   }
	}

}
