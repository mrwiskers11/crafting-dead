package com.craftingdead.client.mapwriter;

import com.craftingdead.CraftingDead;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.world.chunk.Chunk;

public class MapUtil {

   public static final Pattern a = Pattern.compile("[^a-zA-Z0-9_]");


   public static void a(String var0, Object ... var1) {
      CraftingDead.d.info(String.format(var0, var1));
   }

   public static void b(String var0, Object ... var1) {
      CraftingDead.d.warning(String.format(var0, var1));
   }

   public static void c(String var0, Object ... var1) {
      CraftingDead.d.severe(String.format(var0, var1));
   }

   public static void d(String var0, Object ... var1) {
      CraftingDead.d.finest(String.format(var0, var1));
   }

   public static void e(String var0, Object ... var1) {
      a(String.format(var0, var1), new Object[0]);
   }

   public static String a(String var0) {
      var0 = var0.replace('.', '_');
      var0 = var0.replace('-', '_');
      var0 = var0.replace(' ', '_');
      var0 = var0.replace('/', '_');
      var0 = var0.replace('\\', '_');
      return a.matcher(var0).replaceAll("");
   }

   public static File a(File var0, String var1, String var2) {
      int var3 = 0;
      File var4;
      if(var0 != null) {
         var4 = new File(var0, var1 + "." + var2);
      } else {
         var4 = new File(var1 + "." + var2);
      }

      for(; var4.exists() && var3 < 1000; ++var3) {
         if(var0 != null) {
            var4 = new File(var0, var1 + "." + var3 + "." + var2);
         } else {
            var4 = new File(var1 + "." + var3 + "." + var2);
         }
      }

      return var3 < 1000?var4:null;
   }

   public static void b(String var0) {
      EntityClientPlayerMP var1 = Minecraft.getMinecraft().thePlayer;
      if(var1 != null) {
         var1.addChatMessage(var0);
      }

      e("%s", new Object[]{var0});
   }

   public static File a(File var0, int var1) {
      File var2;
      if(var1 != 0) {
         var2 = new File(var0, "DIM" + var1);
      } else {
         var2 = var0;
      }

      return var2;
   }

   public static IntBuffer a(int var0) {
      return ByteBuffer.allocateDirect(var0 * 4).order(ByteOrder.nativeOrder()).asIntBuffer();
   }

   public static int b(int var0) {
      --var0;
      var0 |= var0 >> 1;
      var0 |= var0 >> 2;
      var0 |= var0 >> 4;
      var0 |= var0 >> 8;
      var0 |= var0 >> 16;
      return var0 + 1;
   }

   public static String a() {
      SimpleDateFormat var0 = new SimpleDateFormat("yyyyMMdd_HHmm");
      return var0.format(new Date());
   }

   public static int a(int var0, int var1, Chunk var2) {
      int var3 = (var2.xPosition << 4) + 8 - var0;
      int var4 = (var2.zPosition << 4) + 8 - var1;
      return var3 * var3 + var4 * var4;
   }

}
