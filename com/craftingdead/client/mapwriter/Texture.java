package com.craftingdead.client.mapwriter;

import java.nio.IntBuffer;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.Render;

public class Texture {

   private int c;
   public final int a;
   public final int b;
   private final IntBuffer d;


   public Texture(int var1, int var2, int var3, int var4, int var5, int var6) {
      this.c = GL11.glGenTextures();
      this.a = var1;
      this.b = var2;
      this.d = MapUtil.a(var1 * var2);
      this.a(0, 0, var1, var2, var3);
      this.d.position(0);
      this.b();
      GL11.glTexImage2D(3553, 0, '\u8058', var1, var2, 0, '\u80e1', 5121, this.d);
      this.b(var4, var5, var6);
   }

   public Texture(int var1, int var2, int var3) {
      this(var1, var2, var3, 9729, 9728, '\u812f');
   }

   public Texture(int var1) {
      this.c = var1;
      this.b();
      this.a = Render.b();
      this.b = Render.c();
      this.d = MapUtil.a(this.a * this.b);
      this.d();
      MapUtil.e("created new MwTexture from GL texture id %d (%dx%d) (%d pixels)", new Object[]{Integer.valueOf(this.c), Integer.valueOf(this.a), Integer.valueOf(this.b), Integer.valueOf(this.d.limit())});
   }

   public synchronized void a() {
      if(this.c != 0) {
         try {
            GL11.glDeleteTextures(this.c);
         } catch (NullPointerException var2) {
            MapUtil.e("MwTexture.close: null pointer exception (texture %d)", new Object[]{Integer.valueOf(this.c)});
         }

         this.c = 0;
      }

   }

   public void a(int var1) {
      this.d.position(var1);
   }

   public void b(int var1) {
      this.d.put(var1);
   }

   public synchronized void a(int var1, int var2, int var3, int var4, int var5) {
      int var6 = var2 * this.a + var1;

      for(int var7 = 0; var7 < var4; ++var7) {
         this.d.position(var6 + var7 * this.a);

         for(int var8 = 0; var8 < var3; ++var8) {
            this.d.put(var5);
         }
      }

   }

   public synchronized void a(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7) {
      int var8 = var2 * this.a + var1;

      for(int var9 = 0; var9 < var4; ++var9) {
         this.d.position(var8 + var9 * this.a);
         this.d.get(var5, var6 + var9 * var7, var3);
      }

   }

   public synchronized void b(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7) {
      int var8 = var2 * this.a + var1;

      for(int var9 = 0; var9 < var4; ++var9) {
         this.d.position(var8 + var9 * this.a);
         this.d.put(var5, var6 + var9 * var7, var3);
      }

   }

   public synchronized void a(int var1, int var2, int var3) {
      this.d.put(var2 * this.a + var1, var3);
   }

   public synchronized int a(int var1, int var2) {
      return this.d.get(var2 * this.a + var1);
   }

   public void b() {
      GL11.glBindTexture(3553, this.c);
   }

   public void b(int var1, int var2, int var3) {
      this.b();
      GL11.glTexParameteri(3553, 10242, var3);
      GL11.glTexParameteri(3553, 10243, var3);
      GL11.glTexParameteri(3553, 10241, var1);
      GL11.glTexParameteri(3553, 10240, var2);
   }

   public void a(boolean var1) {
      this.b();
      if(var1) {
         GL11.glTexParameteri(3553, 10241, 9729);
         GL11.glTexParameteri(3553, 10240, 9729);
      } else {
         GL11.glTexParameteri(3553, 10241, 9728);
         GL11.glTexParameteri(3553, 10240, 9728);
      }

   }

   public synchronized void a(int var1, int var2, int var3, int var4) {
      try {
         this.b();
         GL11.glPixelStorei(3314, this.a);
         this.d.position(var2 * this.a + var1);
         GL11.glTexSubImage2D(3553, 0, var1, var2, var3, var4, '\u80e1', 5121, this.d);
         GL11.glPixelStorei(3314, 0);
      } catch (NullPointerException var6) {
         MapUtil.e("MwTexture.updatePixels: null pointer exception (texture %d)", new Object[]{Integer.valueOf(this.c)});
      }

   }

   public synchronized void c() {
      this.b();
      this.d.position(0);
      GL11.glTexImage2D(3553, 0, '\u8058', this.a, this.b, 0, '\u80e1', 5121, this.d);
   }

   private synchronized void d() {
      try {
         this.b();
         this.d.clear();
         GL11.glGetTexImage(3553, 0, '\u80e1', 5121, this.d);
         this.d.limit(this.a * this.b);
      } catch (NullPointerException var2) {
         MapUtil.e("MwTexture.getPixels: null pointer exception (texture %d)", new Object[]{Integer.valueOf(this.c)});
      }

   }
}
