package com.craftingdead.client.mapwriter;

import com.craftingdead.client.mapwriter.MapManager;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.IConnectionHandler;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.NetLoginHandler;
import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet1Login;
import net.minecraft.server.MinecraftServer;

public class MapConnectionHandler implements IConnectionHandler {

   MapManager a;


   public MapConnectionHandler(MapManager var1) {
      this.a = var1;
   }

   public void playerLoggedIn(Player var1, NetHandler var2, INetworkManager var3) {}

   public String connectionReceived(NetLoginHandler var1, INetworkManager var2) {
      return null;
   }

   public void connectionOpened(NetHandler var1, String var2, int var3, INetworkManager var4) {
      Side var5 = FMLCommonHandler.instance().getEffectiveSide();
      if(var5 == Side.CLIENT) {
         this.a.a(var2, var3);
      }

   }

   public void connectionOpened(NetHandler var1, MinecraftServer var2, INetworkManager var3) {
      Side var4 = FMLCommonHandler.instance().getEffectiveSide();
      if(var4 == Side.CLIENT) {
         this.a.m();
      }

   }

   public void clientLoggedIn(NetHandler var1, INetworkManager var2, Packet1Login var3) {
      Side var4 = FMLCommonHandler.instance().getEffectiveSide();
      if(var4 == Side.CLIENT) {
         this.a.a(var3);
      }

   }

   public void connectionClosed(INetworkManager var1) {
      Side var2 = FMLCommonHandler.instance().getEffectiveSide();
      if(var2 == Side.CLIENT) {
         this.a.n();
      }

   }
}
