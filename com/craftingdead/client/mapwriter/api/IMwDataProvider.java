package com.craftingdead.client.mapwriter.api;

import java.util.ArrayList;

import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;

public interface IMwDataProvider {

   ArrayList a(int var1, double var2, double var4, double var6, double var8, double var10, double var12);

   String a(int var1, int var2, int var3, int var4);

   void a(int var1, int var2, int var3, MapView var4);

   void a(int var1, MapView var2);

   void a(double var1, double var3, MapView var5);

   void b(int var1, MapView var2);

   void a(MapView var1);

   void b(MapView var1);

   void a(MapView var1, MapMode var2);

   boolean b(MapView var1, MapMode var2);
}
