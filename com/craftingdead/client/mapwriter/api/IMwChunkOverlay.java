package com.craftingdead.client.mapwriter.api;

import java.awt.Point;

public interface IMwChunkOverlay {

   Point a();

   int b();

   float c();

   boolean d();

   float e();

   int f();
}
