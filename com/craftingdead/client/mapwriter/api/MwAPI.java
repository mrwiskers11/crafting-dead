package com.craftingdead.client.mapwriter.api;

import com.craftingdead.client.mapwriter.api.IMwDataProvider;
import com.google.common.collect.HashBiMap;
import java.util.ArrayList;
import java.util.Collection;

public class MwAPI {

   private static HashBiMap a = HashBiMap.create();
   private static IMwDataProvider b = null;
   private static ArrayList c = new ArrayList();


   public static void a(String var0, IMwDataProvider var1) {
      a.put(var0, var1);
      c.add(var0);
   }

   public static Collection a() {
      return a.values();
   }

   public static IMwDataProvider a(String var0) {
      return (IMwDataProvider)a.get(var0);
   }

   public static String a(IMwDataProvider var0) {
      return (String)a.inverse().get(var0);
   }

   public static IMwDataProvider b() {
      return b;
   }

   public static String c() {
      return b != null?a(b):"None";
   }

   public static IMwDataProvider b(String var0) {
      b = (IMwDataProvider)a.get(var0);
      return b;
   }

   public static IMwDataProvider b(IMwDataProvider var0) {
      b = var0;
      return b;
   }

   public static IMwDataProvider d() {
      if(b != null) {
         int var0 = c.indexOf(c());
         if(var0 + 1 >= c.size()) {
            b = null;
         } else {
            String var1 = (String)c.get(var0 + 1);
            b = a(var1);
         }
      } else if(c.size() > 0) {
         b = a((String)c.get(0));
      }

      return b;
   }

   public static IMwDataProvider e() {
      if(b != null) {
         int var0 = c.indexOf(c());
         if(var0 - 1 < 0) {
            b = null;
         } else {
            String var1 = (String)c.get(var0 - 1);
            b = a(var1);
         }
      } else if(c.size() > 0) {
         b = a((String)c.get(c.size() - 1));
      }

      return b;
   }

}
