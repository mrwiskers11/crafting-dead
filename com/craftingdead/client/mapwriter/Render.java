package com.craftingdead.client.mapwriter;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;

import com.craftingdead.client.mapwriter.MapUtil;

public class Render {

   public static double a = 0.0D;
   public static final double b = 30.0D;


   public static void a(int var0, int var1) {
      a((var1 * 255 / 100 & 255) << 24 | var0 & 16777215);
   }

   public static void a(int var0) {
      GL11.glColor4f((float)(var0 >> 16 & 255) / 255.0F, (float)(var0 >> 8 & 255) / 255.0F, (float)(var0 & 255) / 255.0F, (float)(var0 >> 24 & 255) / 255.0F);
   }

   public static void a() {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
   }

   public static int b(int var0, int var1) {
      float var2 = (float)(var0 >> 24 & 255);
      float var3 = (float)(var0 >> 16 & 255);
      float var4 = (float)(var0 >> 8 & 255);
      float var5 = (float)(var0 >> 0 & 255);
      float var6 = (float)(var1 >> 24 & 255);
      float var7 = (float)(var1 >> 16 & 255);
      float var8 = (float)(var1 >> 8 & 255);
      float var9 = (float)(var1 >> 0 & 255);
      int var10 = (int)(var3 * var7 / 255.0F) & 255;
      int var11 = (int)(var4 * var8 / 255.0F) & 255;
      int var12 = (int)(var5 * var9 / 255.0F) & 255;
      int var13 = (int)(var2 * var6 / 255.0F) & 255;
      return var13 << 24 | var10 << 16 | var11 << 8 | var12;
   }

   public static int a(int[] var0, int var1, int var2) {
      int var3 = var0[var1];
      int var4 = var0[var1 + 1];
      int var5 = var0[var1 + var2];
      int var6 = var0[var1 + var2 + 1];
      int var7 = (var3 >> 16 & 255) + (var4 >> 16 & 255) + (var5 >> 16 & 255) + (var6 >> 16 & 255);
      var7 >>= 2;
      int var8 = (var3 >> 8 & 255) + (var4 >> 8 & 255) + (var5 >> 8 & 255) + (var6 >> 8 & 255);
      var8 >>= 2;
      int var9 = (var3 & 255) + (var4 & 255) + (var5 & 255) + (var6 & 255);
      var9 >>= 2;
      return -16777216 | (var7 & 255) << 16 | (var8 & 255) << 8 | var9 & 255;
   }

   public static int a(int[] var0) {
      int var1 = 0;
      double var2 = 0.0D;
      double var4 = 0.0D;
      double var6 = 0.0D;
      double var8 = 0.0D;
      int[] var10 = var0;
      int var11 = var0.length;

      for(int var12 = 0; var12 < var11; ++var12) {
         int var13 = var10[var12];
         double var14 = (double)(var13 >> 24 & 255);
         double var16 = (double)(var13 >> 16 & 255);
         double var18 = (double)(var13 >> 8 & 255);
         double var20 = (double)(var13 >> 0 & 255);
         var2 += var14;
         var4 += var16 * var14 / 255.0D;
         var6 += var18 * var14 / 255.0D;
         var8 += var20 * var14 / 255.0D;
         ++var1;
      }

      var4 = var4 * 255.0D / var2;
      var6 = var6 * 255.0D / var2;
      var8 = var8 * 255.0D / var2;
      var2 /= (double)var1;
      return ((int)var2 & 255) << 24 | ((int)var4 & 255) << 16 | ((int)var6 & 255) << 8 | (int)var8 & 255;
   }

   public static int c(int var0, int var1) {
      int var2 = var0 >> 16 & 255;
      int var3 = var0 >> 8 & 255;
      int var4 = var0 >> 0 & 255;
      var2 = Math.min(Math.max(0, var2 + var1), 255);
      var3 = Math.min(Math.max(0, var3 + var1), 255);
      var4 = Math.min(Math.max(0, var4 + var1), 255);
      return var0 & -16777216 | var2 << 16 | var3 << 8 | var4;
   }

   public static int b() {
      return GL11.glGetTexLevelParameteri(3553, 0, 4096);
   }

   public static int c() {
      return GL11.glGetTexLevelParameteri(3553, 0, 4097);
   }

   public static int d() {
      return GL11.glGetInteger('\u8069');
   }

   public static void b(int var0) {
      int var1 = b();
      int var2 = c();
      int var3 = GL11.glGetTexLevelParameteri(3553, 0, '\u8071');
      int var4 = GL11.glGetTexLevelParameteri(3553, 0, 4099);
      MapUtil.e("texture %d parameters: width=%d, height=%d, depth=%d, format=%08x", new Object[]{Integer.valueOf(var0), Integer.valueOf(var1), Integer.valueOf(var2), Integer.valueOf(var3), Integer.valueOf(var4)});
   }

   public static int e() {
      return GL11.glGetInteger(3379);
   }

   public static void a(double var0, double var2, double var4, double var6) {
      a(var0, var2, var4, var6, 0.0D, 0.0D, 1.0D, 1.0D);
   }

   public static void a(double var0, double var2, double var4, double var6, double var8, double var10, double var12, double var14) {
      try {
         GL11.glEnable(3553);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         Tessellator var16 = Tessellator.instance;
         var16.startDrawingQuads();
         var16.addVertexWithUV(var0 + var4, var2, a, var12, var10);
         var16.addVertexWithUV(var0, var2, a, var8, var10);
         var16.addVertexWithUV(var0, var2 + var6, a, var8, var14);
         var16.addVertexWithUV(var0 + var4, var2 + var6, a, var12, var14);
         var16.draw();
         GL11.glDisable(3042);
      } catch (NullPointerException var17) {
         MapUtil.e("MwRender.drawTexturedRect: null pointer exception", new Object[0]);
      }

   }

   public static void b(double var0, double var2, double var4, double var6) {
      double var8 = 2.356194490192345D;
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      GL11.glBlendFunc(770, 771);
      Tessellator var10 = Tessellator.instance;
      var10.startDrawing(6);
      var10.addVertex(var0 + var6 * Math.cos(var4), var2 + var6 * Math.sin(var4), a);
      var10.addVertex(var0 + var6 * 0.5D * Math.cos(var4 - var8), var2 + var6 * 0.5D * Math.sin(var4 - var8), a);
      var10.addVertex(var0 + var6 * 0.3D * Math.cos(var4 + 3.141592653589793D), var2 + var6 * 0.3D * Math.sin(var4 + 3.141592653589793D), a);
      var10.addVertex(var0 + var6 * 0.5D * Math.cos(var4 + var8), var2 + var6 * 0.5D * Math.sin(var4 + var8), a);
      var10.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   public static void a(double var0, double var2, double var4, double var6, double var8, double var10) {
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      GL11.glBlendFunc(770, 771);
      Tessellator var12 = Tessellator.instance;
      var12.startDrawing(4);
      var12.addVertex(var0, var2, a);
      var12.addVertex(var4, var6, a);
      var12.addVertex(var8, var10, a);
      var12.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   public static void c(double var0, double var2, double var4, double var6) {
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      GL11.glBlendFunc(770, 771);
      Tessellator var8 = Tessellator.instance;
      var8.startDrawingQuads();
      var8.addVertex(var0 + var4, var2, a);
      var8.addVertex(var0, var2, a);
      var8.addVertex(var0, var2 + var6, a);
      var8.addVertex(var0 + var4, var2 + var6, a);
      var8.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   public static void a(double var0, double var2, double var4) {
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      GL11.glBlendFunc(770, 771);
      Tessellator var6 = Tessellator.instance;
      var6.startDrawing(6);
      var6.addVertex(var0, var2, a);
      double var7 = 6.283185307179586D;
      double var9 = var7 / 30.0D;

      for(double var11 = -var9; var11 < var7; var11 += var9) {
         var6.addVertex(var0 + var4 * Math.cos(-var11), var2 + var4 * Math.sin(-var11), a);
      }

      var6.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   public static void d(double var0, double var2, double var4, double var6) {
      GL11.glEnable(3042);
      GL11.glDisable(3553);
      GL11.glBlendFunc(770, 771);
      Tessellator var8 = Tessellator.instance;
      var8.startDrawing(5);
      double var9 = 6.283185307179586D;
      double var11 = var9 / 30.0D;
      double var13 = var4 + var6;

      for(double var15 = -var11; var15 < var9; var15 += var11) {
         var8.addVertex(var0 + var4 * Math.cos(-var15), var2 + var4 * Math.sin(-var15), a);
         var8.addVertex(var0 + var13 * Math.cos(-var15), var2 + var13 * Math.sin(-var15), a);
      }

      var8.draw();
      GL11.glEnable(3553);
      GL11.glDisable(3042);
   }

   public static void a(double var0, double var2, double var4, double var6, double var8) {
      c(var0 - var8, var2 - var8, var4 + var8 + var8, var8);
      c(var0 - var8, var2 + var6, var4 + var8 + var8, var8);
      c(var0 - var8, var2, var8, var6);
      c(var0 + var4, var2, var8, var6);
   }

   public static void a(int var0, int var1, int var2, String var3, Object ... var4) {
      Minecraft var5 = Minecraft.getMinecraft();
      FontRenderer var6 = var5.fontRenderer;
      String var7 = String.format(var3, var4);
      var6.drawStringWithShadow(var7, var0, var1, var2);
   }

   public static void b(int var0, int var1, int var2, String var3, Object ... var4) {
      Minecraft var5 = Minecraft.getMinecraft();
      FontRenderer var6 = var5.fontRenderer;
      String var7 = String.format(var3, var4);
      int var8 = var6.getStringWidth(var7);
      var6.drawStringWithShadow(var7, var0 - var8 / 2, var1, var2);
   }

   public static void b(double var0, double var2, double var4) {
      GL11.glEnable(2929);
      GL11.glColorMask(false, false, false, false);
      GL11.glDepthMask(true);
      GL11.glDepthFunc(519);
      a(-1);
      a = 1000.0D;
      a(var0, var2, var4);
      a = 0.0D;
      GL11.glColorMask(true, true, true, true);
      GL11.glDepthMask(false);
      GL11.glDepthFunc(516);
   }

   public static void f() {
      GL11.glDepthMask(true);
      GL11.glDepthFunc(515);
      GL11.glDisable(2929);
   }

}
