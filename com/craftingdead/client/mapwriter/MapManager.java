package com.craftingdead.client.mapwriter;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.craftingdead.client.mapwriter.BackgroundExecutor;
import com.craftingdead.client.mapwriter.BlockColourGen;
import com.craftingdead.client.mapwriter.ChunkManager;
import com.craftingdead.client.mapwriter.MapConfig;
import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.Render;
import com.craftingdead.client.mapwriter.map.MapTexture;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.Marker;
import com.craftingdead.client.mapwriter.map.MarkerManager;
import com.craftingdead.client.mapwriter.map.MiniMap;
import com.craftingdead.client.mapwriter.map.Trail;
import com.craftingdead.client.mapwriter.region.BlockColours;
import com.craftingdead.client.mapwriter.region.RegionManager;
import com.craftingdead.client.mapwriter.tasks.CloseRegionManagerTask;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGameOver;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.network.packet.Packet1Login;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.ChunkEvent.Unload;
import net.minecraftforge.event.world.WorldEvent.Load;

public class MapManager {

   public Minecraft a = null;
   public String b = "default";
   private String ab = "default";
   private int ac = 0;
   public MapConfig c;
   public MapConfig d = null;
   private final File ad;
   private final File ae;
   public File e = null;
   public File f = null;
   public boolean g = true;
   public int h = 0;
   public boolean i = true;
   public String j = "tp";
   public int k = 80;
   public int l = 5;
   public int m = -5;
   public boolean n = false;
   public int o = 16384;
   public boolean p = true;
   public int q = 2048;
   public int r = 2048;
   public int s = 3;
   public int t = 5;
   public boolean u = true;
   public String v = "";
   public boolean w = true;
   public boolean x = true;
   public int y = 0;
   private boolean af = false;
   public boolean z = false;
   public boolean A = false;
   public int B = 0;
   public List C = new ArrayList();
   public double D = 0.0D;
   public double E = 0.0D;
   public double F = 0.0D;
   public int G = 0;
   public int H = 0;
   public int I = 0;
   public double J = 0.0D;
   public int K = 0;
   public double L = 0.0D;
   public static final String M = "world";
   public static final String N = "markers";
   public static final String O = "options";
   public static final String P = "mapwriter.cfg";
   public static final String Q = "MapWriterBlockColours.txt";
   public static final String R = "MapWriterBlockColourOverrides.txt";
   public MapTexture S = null;
   public BackgroundExecutor T = null;
   public MiniMap U = null;
   public MarkerManager V = null;
   public BlockColours W = null;
   public RegionManager X = null;
   public ChunkManager Y = null;
   public Trail Z = null;
   public static MapManager aa;


   public MapManager(MapConfig var1) {
      this.a = Minecraft.getMinecraft();
      this.c = var1;
      this.ae = new File(this.a.mcDataDir, "saves");
      this.ad = new File(this.a.mcDataDir, "config");
      this.z = false;
      aa = this;
   }

   public String a() {
      String var1;
      if(this.A) {
         if(this.u) {
            var1 = String.format("%s_%d", new Object[]{this.ab, Integer.valueOf(this.ac)});
         } else {
            var1 = String.format("%s", new Object[]{this.ab});
         }
      } else {
         IntegratedServer var2 = this.a.getIntegratedServer();
         var1 = var2 != null?var2.getFolderName():"sp_world";
      }

      var1 = MapUtil.a(var1);
      if(var1 == "") {
         var1 = "default";
      }

      return var1;
   }

   public void b() {
      this.c.load();
      this.g = this.c.a("options", "linearTextureScaling", this.g);
      this.n = this.c.a("options", "useSavedBlockColours", this.n);
      this.i = this.c.a("options", "teleportEnabled", this.i);
      this.j = this.c.get("options", "teleportCommand", this.j).getString();
      this.h = this.c.a("options", "coordsMode", this.h, 0, 2);
      this.o = this.c.a("options", "maxChunkSaveDistSq", this.o, 1, 65536);
      this.p = this.c.a("options", "mapPixelSnapEnabled", this.p);
      this.s = this.c.a("options", "maxDeathMarkers", this.s, 0, 1000);
      this.t = this.c.a("options", "chunksPerTick", this.t, 1, 500);
      this.v = this.c.get("options", "saveDirOverride", this.v).getString();
      this.u = this.c.a("options", "portNumberInWorldNameEnabled", this.u);
      this.w = this.c.a("options", "regionFileOutputEnabledSP", this.w);
      this.x = this.c.a("options", "regionFileOutputEnabledMP", this.x);
      this.y = this.c.a("options", "backgroundTextureMode", this.y, 0, 1);
      this.l = this.c.a("options", "zoomOutLevels", this.l, 1, 256);
      this.m = -this.c.a("options", "zoomInLevels", -this.m, 1, 256);
      this.r = this.c.a("options", "textureSize", this.r, 1024, 8192);
      this.f();
   }

   public void c() {
      File var1 = new File(this.e, "mapwriter.cfg");
      this.d = new MapConfig(var1);
      this.d.load();
      this.C.clear();
      this.d.a("world", "dimensionList", this.C);
      this.a(0);
      this.h();
   }

   public void d() {
      this.c.b("options", "linearTextureScaling", this.g);
      this.c.b("options", "useSavedBlockColours", this.n);
      this.c.a("options", "textureSize", this.r);
      this.c.a("options", "coordsMode", this.h);
      this.c.a("options", "maxChunkSaveDistSq", this.o);
      this.c.b("options", "mapPixelSnapEnabled", this.p);
      this.c.a("options", "maxDeathMarkers", this.s);
      this.c.a("options", "chunksPerTick", this.t);
      this.c.a("options", "backgroundTextureMode", this.y);
      this.c.save();
   }

   public void e() {
      this.d.b("world", "dimensionList", this.C);
      this.d.save();
   }

   public void f() {
      if(this.r != this.q) {
         int var1 = Render.e();

         int var2;
         for(var2 = 1024; var2 <= var1 && var2 <= this.r; var2 *= 2) {
            ;
         }

         var2 /= 2;
         MapUtil.e("GL reported max texture size = %d", new Object[]{Integer.valueOf(var1)});
         MapUtil.e("texture size from config = %d", new Object[]{Integer.valueOf(this.r)});
         MapUtil.e("setting map texture size to = %d", new Object[]{Integer.valueOf(var2)});
         this.q = var2;
         if(this.z) {
            this.k();
         }
      }

   }

   public void g() {
      this.D = this.a.thePlayer.posX;
      this.F = this.a.thePlayer.posY;
      this.E = this.a.thePlayer.posZ;
      this.G = (int)Math.floor(this.D);
      this.H = (int)Math.floor(this.F);
      this.I = (int)Math.floor(this.E);
      this.J = Math.toRadians((double)this.a.thePlayer.rotationYaw) + 1.5707963267948966D;
      this.L = (double)(-this.a.thePlayer.rotationYaw + 180.0F);
   }

   public void a(int var1) {
      int var2 = this.C.indexOf(Integer.valueOf(var1));
      if(var2 < 0) {
         this.C.add(Integer.valueOf(var1));
      }

   }

   public void h() {
      ArrayList var1 = new ArrayList(this.C);
      this.C.clear();
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         int var3 = ((Integer)var2.next()).intValue();
         this.a(var3);
      }

   }

   public void i() {
      this.V.d();
      this.V.c();
      this.a.thePlayer.addChatMessage("group " + this.V.a() + " selected");
   }

   public void a(int var1, int var2, int var3) {
      if(this.i) {
         this.a.thePlayer.sendChatMessage(String.format("/%s %d %d %d", new Object[]{this.j, Integer.valueOf(var1), Integer.valueOf(var2), Integer.valueOf(var3)}));
      } else {
         MapUtil.b("teleportation is disabled in mapwriter.cfg");
      }

   }

   public void a(MapView var1, int var2, int var3, int var4) {
      double var5 = var1.e(this.K);
      this.a((int)((double)var2 / var5), var3, (int)((double)var4 / var5));
   }

   public void a(Marker var1) {
      if(var1.f == this.K) {
         this.a(var1.c, var1.d, var1.e);
      } else {
         MapUtil.b("cannot teleport to marker in different dimension");
      }

   }

   public void a(BlockColours var1) {
      File var2 = new File(this.ad, "MapWriterBlockColourOverrides.txt");
      if(var2.isFile()) {
         MapUtil.a("loading block colour overrides file %s", new Object[]{var2});
         var1.a(var2);
      } else {
         MapUtil.a("recreating block colour overrides file %s", new Object[]{var2});
         BlockColours.c(var2);
         if(var2.isFile()) {
            var1.a(var2);
         } else {
            MapUtil.c("could not load block colour overrides from file %s", new Object[]{var2});
         }
      }

   }

   public void b(BlockColours var1) {
      File var2 = new File(this.ad, "MapWriterBlockColours.txt");
      MapUtil.a("saving block colours to \'%s\'", new Object[]{var2});
      var1.b(var2);
   }

   public void j() {
      BlockColours var1 = new BlockColours();
      File var2 = new File(this.ad, "MapWriterBlockColours.txt");
      if(this.n && var2.isFile()) {
         MapUtil.a("loading block colours from %s", new Object[]{var2});
         var1.a(var2);
         this.a(var1);
      } else {
         MapUtil.a("generating block colours", new Object[0]);
         this.a(var1);
         BlockColourGen.a(var1);
         this.a(var1);
         this.b(var1);
      }

      this.W = var1;
   }

   public void k() {
      this.T.a(new CloseRegionManagerTask(this.X));
      this.T.c();
      MapTexture var1 = this.S;
      MapTexture var2 = new MapTexture(this.q, this.g);
      this.S = var2;
      if(var1 != null) {
         var1.a();
      }

      this.T = new BackgroundExecutor();
      this.X = new RegionManager(this.e, this.f, this.W, this.m, this.l);
   }

   public void b(int var1) {
      this.h = Math.min(Math.max(0, var1), 2);
   }

   public int l() {
      this.b((this.h + 1) % 3);
      return this.h;
   }

   public void m() {
      MapUtil.e("connection opened to integrated server", new Object[0]);
      this.A = false;
   }

   public void a(String var1, int var2) {
      MapUtil.e("connection opened to remote server: %s %d", new Object[]{var1, Integer.valueOf(var2)});
      this.ab = var1;
      this.ac = var2;
      this.A = true;
   }

   public void a(Packet1Login var1) {
      MapUtil.e("onClientLoggedIn: dimension = %d", new Object[]{Integer.valueOf(var1.dimension)});
      this.b();
      this.b = this.a();
      File var2 = this.ae;
      if(this.v.length() > 0) {
         File var3 = new File(this.v);
         if(var3.isDirectory()) {
            var2 = var3;
         } else {
            MapUtil.e("error: no such directory %s", new Object[]{this.v});
         }
      }

      if(this.A) {
         this.e = new File(new File(var2, "mapwriter_mp_worlds"), this.b);
      } else {
         this.e = new File(new File(var2, "mapwriter_sp_worlds"), this.b);
      }

      this.c();
      this.f = new File(this.e, "images");
      if(!this.f.exists()) {
         this.f.mkdirs();
      }

      if(!this.f.isDirectory()) {
         MapUtil.e("Mapwriter: ERROR: could not create images directory \'%s\'", new Object[]{this.f.getPath()});
      }

      this.B = 0;
      this.af = false;
      this.V = new MarkerManager();
      this.V.a(this.d, "markers");
      this.Z = new Trail(this, "player");
      this.T = new BackgroundExecutor();
      this.S = new MapTexture(this.q, this.g);
      this.j();
      this.X = new RegionManager(this.e, this.f, this.W, this.m, this.l);
      this.U = new MiniMap(this);
      this.U.g.c(var1.dimension);
      this.Y = new ChunkManager(this);
      this.z = true;
   }

   @ForgeSubscribe
   public void a(Load var1) {
      Side var2 = FMLCommonHandler.instance().getEffectiveSide();
      if(var2 == Side.CLIENT) {
         this.K = var1.world.provider.dimensionId;
         if(this.z) {
            this.a(this.K);
            this.U.g.c(this.K);
         }
      }

   }

   public void n() {
      MapUtil.e("connection closed", new Object[0]);
      if(this.z) {
         this.z = false;
         this.Y.a();
         this.Y = null;
         this.T.a(new CloseRegionManagerTask(this.X));
         this.X = null;
         MapUtil.e("waiting for %d tasks to finish...", new Object[]{Integer.valueOf(this.T.b())});
         if(this.T.c()) {
            MapUtil.e("error: timeout waiting for tasks to finish", new Object[0]);
         }

         MapUtil.e("done", new Object[0]);
         this.Z.a();
         this.V.b(this.d, "markers");
         this.V.b();
         this.U.a();
         this.U = null;
         this.S.a();
         this.e();
         this.d();
         this.B = 0;
      }

   }

   public void o() {
      if(this.z && this.a.thePlayer != null) {
         this.g();
         if(this.a.currentScreen instanceof GuiGameOver) {
            if(!this.af) {
               this.p();
               this.af = true;
            }
         } else {
            this.af = false;
            if(this.a.currentScreen == null) {
               this.U.g.a(this.D, this.E, this.K);
               this.U.c();
            }
         }

         for(int var1 = 50; !this.T.a() && var1 > 0; --var1) {
            ;
         }

         this.Y.e();
         this.S.d();
         this.Z.b();
         ++this.B;
      }

   }

   @ForgeSubscribe
   public void a(net.minecraftforge.event.world.ChunkEvent.Load var1) {
      Side var2 = FMLCommonHandler.instance().getEffectiveSide();
      if(var2 == Side.CLIENT && this.z && var1.getChunk() != null && var1.getChunk().worldObj instanceof WorldClient) {
         this.Y.b(var1.getChunk());
      }

   }

   @ForgeSubscribe
   public void a(Unload var1) {
      Side var2 = FMLCommonHandler.instance().getEffectiveSide();
      if(var2 == Side.CLIENT && this.z && var1.getChunk() != null && var1.getChunk().worldObj instanceof WorldClient) {
         this.Y.c(var1.getChunk());
      }

   }

   public void p() {
      if(this.z && this.s > 0) {
         this.g();
         int var1 = this.V.c("playerDeaths") - this.s + 1;

         for(int var2 = 0; var2 < var1; ++var2) {
            this.V.a((String)null, "playerDeaths");
         }

         this.V.a(MapUtil.a(), "playerDeaths", this.G, this.H, this.I, this.K, -65536);
         this.V.a("playerDeaths");
         this.V.c();
      }

   }
}
