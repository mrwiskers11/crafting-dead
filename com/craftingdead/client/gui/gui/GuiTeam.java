package com.craftingdead.client.gui.gui;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.GuiHome;
import com.craftingdead.client.gui.widget.team.GuiWidgetTeam;

import net.minecraft.client.gui.GuiButton;

public class GuiTeam extends GuiCDScreen {

   public void initGui() {
      super.initGui();
      super.a();
      int var1 = super.width / 2 - 173;
      this.a((new GuiWidgetTeam(1, var1 + 1, 47, 350, 250, this)).a(super.a.m()));
   }

   public void actionPerformed(GuiButton var1) {
      super.actionPerformed(var1);
      if(var1.id == 1) {
         super.mc.displayGuiScreen(new GuiHome());
      }

   }

   public void updateScreen() {
      super.updateScreen();
   }

   public void drawScreen(int var1, int var2, float var3) {
      super.drawScreen(var1, var2, var3);
   }
}
