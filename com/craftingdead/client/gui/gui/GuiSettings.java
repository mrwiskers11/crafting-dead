package com.craftingdead.client.gui.gui;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownParticles;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownSettingsPerformance;
import com.craftingdead.client.gui.modules.GuiCDButton;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.gui.GuiScreen;

public class GuiSettings extends GuiCDScreen {

   private GuiScreen c;


   public GuiSettings(GuiScreen var1) {
      this.c = var1;
   }

   public void initGui() {
      super.initGui();
      super.a();
      int var1 = super.height / 2;
      byte var2 = 120;
      int var3 = super.width / 2;
      int var4 = var1 - 120;
      int var5 = super.a.e().a;
      String var6 = var5 == 0?"Low":(var5 == 1?"Medium":(var5 == 2?"High":"Ultra"));
      String var7 = super.a.e().b?"On":"Off";
      String var8 = super.a.e().c?"On":"Off";
      String var9 = super.a.e().d?"On":"Off";
      String var10 = super.a.e().e?"On":"Off";
      String var11 = super.a.e().f?"On":"Off";
      String var12 = super.a.e().g?"On":"Off";
      String var13 = super.a.e().h?"On":"Off";
      String var14 = super.a.e().i?"On":"Off";
      String var15 = super.a.e().j?"Enabled":"Disabled";
      super.buttonList.add(new GuiCDButton(10, var3 - var2 - 2, var4 + 30, var2, 20, "Particles (" + var6 + ")"));
      super.buttonList.add(new GuiCDButton(11, var3 - var2 - 2, var4 + 54, var2, 20, "Notifications (" + var7 + ")"));
      super.buttonList.add(new GuiCDButton(12, var3 - var2 - 2, var4 + 78, var2, 20, "Corpse Skins (" + var8 + ")"));
      super.buttonList.add(new GuiCDButton(13, var3 - var2 - 2, var4 + 102, var2, 20, "Auto Performance"));
      super.buttonList.add(new GuiCDButton(14, var3 - var2 - 2, var4 + 126, var2, 20, "Loot Piles (" + var12 + ")"));
      super.buttonList.add(new GuiCDButton(20, var3 + 2, var4 + 30, var2, 20, "Controls (MC)"));
      super.buttonList.add(new GuiCDButton(21, var3 + 2, var4 + 54, var2, 20, "Loot Spawn %s (" + var9 + ")"));
      super.buttonList.add(new GuiCDButton(22, var3 + 2, var4 + 78, var2, 20, "Ambience (" + var10 + ")"));
      super.buttonList.add(new GuiCDButton(23, var3 + 2, var4 + 102, var2, 20, "Bullet Hitboxes (" + var11 + ")"));
      super.buttonList.add(new GuiCDButton(24, var3 + 2, var4 + 126, var2, 20, "Blood Splatter (" + var13 + ")"));
      super.buttonList.add(new GuiCDButton(25, var3 - var2 - 2, var4 + 150, var2, 20, "Karma Skins (" + var14 + ")"));
      super.buttonList.add(new GuiCDButton(26, var3 + 2, var4 + 150, var2, 20, "Textures (" + var15 + ")"));
   }

   public void actionPerformed(GuiButton var1) {
      super.actionPerformed(var1);
      int var2 = super.width / 12;
      int var3 = super.height / 2;
      int var4 = var2 * 7;
      byte var5 = 120;
      int var6 = var3 - 90;
      if(var1.id == 1) {
         super.mc.displayGuiScreen(this.c);
      }

      switch(var1.id) {
      case 10:
         super.mc.displayGuiScreen(new GuiDropDownParticles(this, var4 - var5 - 2, var6 + 51, var5, 20));
         break;
      case 11:
         super.a.e().b = !super.a.e().b;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 12:
         super.a.e().c = !super.a.e().c;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 13:
         super.mc.displayGuiScreen(new GuiDropDownSettingsPerformance(this, var4 - var5 - 2, var6 + 123, var5, 20));
         break;
      case 14:
         super.a.e().g = !super.a.e().g;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      default:
         break;
      case 20:
         super.mc.displayGuiScreen(new GuiControls(this, super.mc.gameSettings));
         break;
      case 21:
         super.a.e().d = !super.a.e().d;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 22:
         super.a.e().e = !super.a.e().e;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 23:
         super.a.e().f = !super.a.e().f;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 24:
         super.a.e().h = !super.a.e().h;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 25:
         super.a.e().i = !super.a.e().i;
         super.mc.displayGuiScreen(new GuiSettings(this.c));
         break;
      case 26:
         super.a.e().j = !super.a.e().j;
         super.a.n();
         super.mc.displayGuiScreen(new GuiSettings(this.c));
      }

      super.a.e().b();
   }
}
