package com.craftingdead.client.gui.gui.dropdown;

import com.craftingdead.client.gui.gui.GuiSettings;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;

import cpw.mods.fml.client.GuiModList;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownSettings extends GuiDropDownMenu {

   public GuiDropDownSettings(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Crafting Dead");
      this.a("Forge");
      this.a("Minecraft");
   }

   public void d(int var1) {
      if(var1 == 1) {
         super.mc.displayGuiScreen(new GuiSettings(this));
      }

      if(var1 == 2) {
         super.mc.displayGuiScreen(new GuiModList(this));
      }

      if(var1 == 3) {
         super.mc.displayGuiScreen(new GuiOptions(this, super.mc.gameSettings));
      }

   }
}
