package com.craftingdead.client.gui.gui.dropdown;

import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;
import com.craftingdead.client.gui.gui.server.GuiServerList;
import com.craftingdead.client.gui.gui.server.ServerRegion;

import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSelectWorld;

public class GuiDropDownPlay extends GuiDropDownMenu {

   public GuiDropDownPlay(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Single player");
      this.a("Mod Servers");
      this.a("Other Servers");
   }

   public void d(int var1) {
      switch(var1) {
      case 1:
         super.mc.displayGuiScreen(new GuiSelectWorld(this));
         break;
      case 2:
         super.mc.displayGuiScreen(new GuiServerList(ServerRegion.a));
         break;
      case 3:
         super.mc.displayGuiScreen(new GuiMultiplayer(this));
      }

   }
}
