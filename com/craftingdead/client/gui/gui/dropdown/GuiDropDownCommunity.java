package com.craftingdead.client.gui.gui.dropdown;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.GuiTeam;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownCommunity extends GuiDropDownMenu {

   public GuiDropDownCommunity(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Discord");
      this.a("Website");
      this.a("The Team");
   }

   public void d(int var1) {
      if(var1 == 1) {
         GuiCDScreen.a(super.a.f().m());
      }

      if(var1 == 2) {
         GuiCDScreen.a(super.a.f().j());
      }

      if(var1 == 3) {
         super.mc.displayGuiScreen(new GuiTeam());
      }

   }
}
