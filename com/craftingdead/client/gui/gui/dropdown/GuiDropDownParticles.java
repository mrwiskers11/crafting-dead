package com.craftingdead.client.gui.gui.dropdown;

import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownParticles extends GuiDropDownMenu {

   public GuiDropDownParticles(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Low");
      this.a("Medium");
      this.a("High");
      this.a("Ultra");
   }

   public void d(int var1) {
      super.a.e().a = var1 - 1;
   }
}
