package com.craftingdead.client.gui.gui.dropdown;

import java.util.ArrayList;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.modules.GuiCDButton;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public abstract class GuiDropDownMenu extends GuiCDScreen {

   protected GuiScreen c;
   private boolean t = false;
   private boolean u = false;
   protected ArrayList d = new ArrayList();
   protected ArrayList e = new ArrayList();
   protected int p;
   protected int q;
   protected int r = 85;
   protected int s = 20;


   public GuiDropDownMenu(GuiScreen var1, int var2, int var3, int var4, int var5) {
      this.c = var1;
      this.p = var2;
      this.q = var3;
      this.r = var4;
      this.s = var5;
      this.d.clear();
   }

   public GuiDropDownMenu a(String var1) {
      this.d.add(var1);
      this.e.add(Boolean.valueOf(true));
      return this;
   }

   public GuiDropDownMenu a(String var1, boolean var2) {
      this.d.add(var1);
      this.e.add(Boolean.valueOf(var2));
      return this;
   }

   public void initGui() {
      if(this.d.size() <= 0) {
         super.mc.displayGuiScreen(this.c);
      }

      for(int var1 = 0; var1 < this.d.size(); ++var1) {
         int var2 = var1 * (this.s + 1);
         GuiCDButton var3 = new GuiCDButton(var1 + 1, this.p, this.q + var2, this.r, this.s, (String)this.d.get(var1));
         boolean var4 = ((Boolean)this.e.get(var1)).booleanValue();
         var3.enabled = var4;
         var3.b(true);
         super.buttonList.add(var3);
      }

      if(this.u) {
         super.mc.displayGuiScreen(this.c);
      }

      this.u = true;
   }

   public void actionPerformed(GuiButton var1) {
      this.d(var1.id);
   }

   public abstract void d(int var1);

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
      if(!this.t && super.mc.currentScreen == this) {
         super.mc.displayGuiScreen(this.c);
      }

   }

   public void drawScreen(int var1, int var2, float var3) {
      for(int var4 = 0; var4 < super.buttonList.size(); ++var4) {
         GuiButton var5 = (GuiButton)super.buttonList.get(var4);
         var5.drawButton(super.mc, var1, var2);
      }

   }
}
