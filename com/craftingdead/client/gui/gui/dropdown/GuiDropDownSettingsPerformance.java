package com.craftingdead.client.gui.gui.dropdown;

import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownSettingsPerformance extends GuiDropDownMenu {

   public GuiDropDownSettingsPerformance(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Max");
      this.a("Fair");
      this.a("Optimize");
   }

   public void d(int var1) {
      if(var1 == 1) {
         super.mc.gameSettings.renderDistance = 0;
         super.mc.gameSettings.fancyGraphics = true;
         super.mc.gameSettings.limitFramerate = 0;
         super.mc.gameSettings.clouds = true;
         super.mc.gameSettings.enableVsync = false;
         super.mc.gameSettings.particleSetting = 0;
         super.a.e().a = 3;
         super.a.e().c = true;
         super.a.e().f = false;
      }

      if(var1 == 2) {
         super.mc.gameSettings.renderDistance = 1;
         super.mc.gameSettings.fancyGraphics = false;
         super.mc.gameSettings.limitFramerate = 0;
         super.mc.gameSettings.clouds = true;
         super.mc.gameSettings.enableVsync = false;
         super.mc.gameSettings.particleSetting = 1;
         super.a.e().a = 2;
         super.a.e().c = true;
         super.a.e().f = false;
      }

      if(var1 == 3) {
         super.mc.gameSettings.renderDistance = 1;
         super.mc.gameSettings.fancyGraphics = false;
         super.mc.gameSettings.limitFramerate = 0;
         super.mc.gameSettings.clouds = false;
         super.mc.gameSettings.enableVsync = true;
         super.mc.gameSettings.particleSetting = 2;
         super.a.e().a = 0;
         super.a.e().c = false;
         super.a.e().f = false;
      }

   }
}
