package com.craftingdead.client.gui.gui.blueprint;

import com.craftingdead.block.BlockBaseCenter;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.blueprint.GuiBlueprintRecipesSlot;
import com.craftingdead.client.gui.gui.blueprint.base.GuiPromptBaseMemberAdd;
import com.craftingdead.client.gui.gui.blueprint.base.GuiPromptBaseMemberRemove;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.blueprint.BlueprintRecipe;
import com.craftingdead.network.packets.CDAPacketBaseRemoval;
import com.craftingdead.network.packets.CDAPacketBlueprintCraft;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiBlueprintRecipes extends GuiCDScreen {

   private ResourceLocation s;
   protected int c;
   protected int d;
   protected int e;
   protected int p;
   private int t;
   public ArrayList q;
   public boolean r;


   public GuiBlueprintRecipes(boolean var1) {
      this.s = new ResourceLocation("craftingdead", "textures/gui/blueprintrecipe.png");
      this.c = 176;
      this.d = 146;
      this.t = 0;
      this.q = new ArrayList();
      this.r = false;
      this.r = var1;
   }

   public GuiBlueprintRecipes() {
      this(false);
   }

   public void initGui() {
      this.e = (super.width - this.c) / 2;
      this.p = (super.height - this.d) / 2;
      this.q = new ArrayList();

      for(int var1 = 0; var1 < ItemManager.b().size(); ++var1) {
         this.q.add(new GuiBlueprintRecipesSlot((BlueprintRecipe)ItemManager.b().get(var1)));
      }

      GuiCDButton var3;
      if(this.r) {
         var3 = new GuiCDButton(10, this.e + 134, this.p + 17, 35, 19, "BREAK");
         var3.t = 3355443;
         super.buttonList.add(var3);
         var3 = new GuiCDButton(11, this.e + 178, this.p + 1, 50, 19, "MEMBERS");
         var3.t = 3355443;
         super.buttonList.add(var3);
         var3 = new GuiCDButton(12, this.e + 178, this.p + 22, 50, 19, "ADD");
         var3.t = 3355443;
         super.buttonList.add(var3);
         var3 = new GuiCDButton(13, this.e + 178, this.p + 43, 50, 19, "REMOVE");
         var3.t = 3355443;
         super.buttonList.add(var3);
      }

      var3 = new GuiCDButton(1, this.e + 134, this.p + 41, 35, 18, " UP");
      var3.t = 3355443;
      super.buttonList.add(var3);

      for(int var2 = 0; var2 < 3; ++var2) {
         var3 = new GuiCDButton(3 + var2, this.e + 98, this.p + 42 + var2 * 34, 30, 10, "Craft");
         var3.r = false;
         var3.s = false;
         var3.t = 3355443;
         super.buttonList.add(var3);
      }

      var3 = new GuiCDButton(2, this.e + 134, this.p + 122, 35, 18, "DOWN");
      var3.t = 3355443;
      super.buttonList.add(var3);
   }

   public void actionPerformed(GuiButton var1) {
      if(var1.id == 10) {
         GuiYesNo var2 = new GuiYesNo(this, "Do you want to remove your base?", "If you click yes, all base material will be removed!", 0);
         super.mc.displayGuiScreen(var2);
      }

      TileEntityBaseCenter var3;
      EntityClientPlayerMP var6;
      if(var1.id == 11) {
         var6 = super.mc.thePlayer;
         var3 = BlockBaseCenter.j(var6.worldObj, (int)var6.posX, (int)var6.posY, (int)var6.posZ);
         if(var3 != null && var3.a(var6.username)) {
            var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+ ====================="));
            var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+ Base Members"));
            var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+"));
            var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+ " + var3.f()));
            Iterator var4 = var3.d().iterator();

            while(var4.hasNext()) {
               String var5 = (String)var4.next();
               var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+ " + var5));
            }

            var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+"));
            var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "+ ====================="));
         }
      }

      if(var1.id == 12) {
         var6 = super.mc.thePlayer;
         var3 = BlockBaseCenter.j(var6.worldObj, (int)var6.posX, (int)var6.posY, (int)var6.posZ);
         if(var3 != null && var3.f().equals(var6.username)) {
            GuiPromptBaseMemberAdd var9 = new GuiPromptBaseMemberAdd(this);
            super.mc.displayGuiScreen(var9);
         }
      }

      if(var1.id == 13) {
         var6 = super.mc.thePlayer;
         var3 = BlockBaseCenter.j(var6.worldObj, (int)var6.posX, (int)var6.posY, (int)var6.posZ);
         if(var3 != null && var3.f().equals(var6.username)) {
            GuiPromptBaseMemberRemove var10 = new GuiPromptBaseMemberRemove(this);
            super.mc.displayGuiScreen(var10);
         }
      }

      if(var1.id == 1 && this.t > 0) {
         --this.t;
      }

      if(var1.id == 2 && this.t < this.q.size() - 3) {
         ++this.t;
      }

      for(int var7 = 0; var7 < 3; ++var7) {
         if(var1.id == 3 + var7) {
            GuiBlueprintRecipesSlot var8 = (GuiBlueprintRecipesSlot)this.q.get(var7 + this.t);
            BlueprintRecipe var11 = var8.a;
            PacketDispatcher.sendPacketToServer(CDAPacketBlueprintCraft.a(var11));
         }
      }

   }

   public void confirmClicked(boolean var1, int var2) {
      if(var1) {
         PacketDispatcher.sendPacketToServer(CDAPacketBaseRemoval.b());
         super.mc.displayGuiScreen((GuiScreen)null);
      } else {
         super.mc.displayGuiScreen(this);
      }

   }

   protected void keyTyped(char var1, int var2) {
      super.keyTyped(var1, var2);
      if(var2 == 1 || var2 == super.mc.gameSettings.keyBindInventory.keyCode) {
         super.mc.thePlayer.closeScreen();
      }

   }

   public boolean doesGuiPauseGame() {
      return false;
   }

   public void drawScreen(int var1, int var2, float var3) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      super.mc.renderEngine.bindTexture(this.s);
      int var4 = this.e;
      int var5 = this.p;
      this.drawTexturedModalRect(var4, var5, 0, 0, this.c, this.d);
      CDRenderHelper.a(EnumChatFormatting.DARK_GRAY + "Blueprint Recipes", var4 + 6, var5 + 5);
      CDRenderHelper.b(EnumChatFormatting.WHITE + "Welcome Back " + super.mc.thePlayer.username, super.width / 2, super.height / 2 - 100);
      int var6 = var4 + 5;
      CDRenderHelper.b((double)(var4 + 5), (double)(var5 + 15), 125.0D, 23.0D, 5592405, 1.0F);
      CDRenderHelper.a(EnumChatFormatting.WHITE + "Click the craft button", var4 + 7, var5 + 17);
      CDRenderHelper.a(EnumChatFormatting.WHITE + "if you have the material!", var4 + 7, var5 + 27);
      CDRenderHelper.b((double)(var4 + 149), (double)(var5 + 61), 5.0D, 59.0D, 5592405, 1.0F);
      int var7 = this.q.size() > 3?53 / (this.q.size() - 3) * this.t:0;
      CDRenderHelper.b((double)(var4 + 149 - 8), (double)(var5 + 62 + var7), 20.0D, 5.0D, 8355711, 1.0F);

      for(int var8 = 0; var8 < 3; ++var8) {
         int var9 = var5 + 40 + var8 * 34;
         GuiBlueprintRecipesSlot var10 = (GuiBlueprintRecipesSlot)this.q.get(var8 + this.t);
         var10.a(var6, var9);
      }

      super.drawScreen(var1, var2, var3);
   }
}
