package com.craftingdead.client.gui.gui.blueprint;

import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.blueprint.BlueprintRecipe;
import com.craftingdead.item.blueprint.BlueprintRecipeInput;

import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiBlueprintRecipesSlot {

   public BlueprintRecipe a;


   public GuiBlueprintRecipesSlot(BlueprintRecipe var1) {
      this.a = var1;
   }

   public void a(int var1, int var2) {
      Minecraft var3 = Minecraft.getMinecraft();
      var3.renderEngine.bindTexture(new ResourceLocation("craftingdead", "textures/gui/blueprintrecipe.png"));
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      if(this.a != null) {
         this.a(var1, var2, 0, 146, 200, 33);
         boolean var4 = this.a.a(var3.thePlayer);
         String var5 = this.a.a().getDisplayName().replace("Blue Prints: ", "");
         var5 = var4?EnumChatFormatting.GREEN + var5:EnumChatFormatting.RED + var5;
         CDRenderHelper.a(EnumChatFormatting.WHITE + "" + var5, var1 + 3, var2 + 3);
         CDRenderHelper.a(this.a.a(), var1 + 3, var2 + 14);
         if(!var4) {
            CDRenderHelper.b((double)(var1 + 3), (double)(var2 + 14), 16.0D, 16.0D, 16711680, 0.2F);
         }

         ArrayList var6 = new ArrayList();
         var6.add(new BlueprintRecipeInput(ItemManager.ew.itemID, 1));
         var6.addAll(this.a.c);

         for(int var7 = 0; var7 < var6.size(); ++var7) {
            int var8 = var7 * 18;
            ItemStack var9 = new ItemStack(((BlueprintRecipeInput)var6.get(var7)).a, 1, 0);
            GL11.glDisable(2929);
            CDRenderHelper.a(var9, var1 + 52 + var8, var2 + 14);
            GL11.glEnable(2929);
            if(((BlueprintRecipeInput)var6.get(var7)).b != 1) {
               CDRenderHelper.b(EnumChatFormatting.DARK_GRAY + "" + ((BlueprintRecipeInput)var6.get(var7)).b, var1 + 66 + var8, var2 + 26);
               CDRenderHelper.b(EnumChatFormatting.WHITE + "" + ((BlueprintRecipeInput)var6.get(var7)).b, var1 + 65 + var8, var2 + 25);
            }
         }
      }

   }

   public void a(int var1, int var2, int var3, int var4, int var5, int var6) {
      float var7 = 0.0F;
      float var8 = 0.00390625F;
      float var9 = 0.00390625F;
      Tessellator var10 = Tessellator.instance;
      var10.startDrawingQuads();
      var10.addVertexWithUV((double)(var1 + 0), (double)(var2 + var6), (double)var7, (double)((float)(var3 + 0) * var8), (double)((float)(var4 + var6) * var9));
      var10.addVertexWithUV((double)(var1 + var5), (double)(var2 + var6), (double)var7, (double)((float)(var3 + var5) * var8), (double)((float)(var4 + var6) * var9));
      var10.addVertexWithUV((double)(var1 + var5), (double)(var2 + 0), (double)var7, (double)((float)(var3 + var5) * var8), (double)((float)(var4 + 0) * var9));
      var10.addVertexWithUV((double)(var1 + 0), (double)(var2 + 0), (double)var7, (double)((float)(var3 + 0) * var8), (double)((float)(var4 + 0) * var9));
      var10.draw();
   }
}
