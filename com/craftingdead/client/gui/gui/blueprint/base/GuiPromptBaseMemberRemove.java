package com.craftingdead.client.gui.gui.blueprint.base;

import com.craftingdead.client.gui.gui.GuiTextPrompt;
import com.craftingdead.network.packets.CDAPacketBaseMemberRemove;

import cpw.mods.fml.common.network.PacketDispatcher;
import net.minecraft.client.gui.GuiScreen;

public class GuiPromptBaseMemberRemove extends GuiTextPrompt {

   public GuiPromptBaseMemberRemove(GuiScreen var1) {
      super(var1);
      this.a(new String[]{"Enter the username you want", "to remove from your base access list.", "Case Sensitive"});
      super.p = 16;
   }

   public void j() {
      String var1 = this.k();
      PacketDispatcher.sendPacketToServer(CDAPacketBaseMemberRemove.a(var1));
      super.mc.displayGuiScreen(super.c);
   }
}
