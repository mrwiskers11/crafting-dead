package com.craftingdead.client.gui.gui.blueprint.base;

import com.craftingdead.client.gui.gui.GuiTextPrompt;
import com.craftingdead.network.packets.CDAPacketBaseMemberAdd;

import cpw.mods.fml.common.network.PacketDispatcher;
import net.minecraft.client.gui.GuiScreen;

public class GuiPromptBaseMemberAdd extends GuiTextPrompt {

   public GuiPromptBaseMemberAdd(GuiScreen var1) {
      super(var1);
      this.a(new String[]{"Enter a Username you wish", "to grant access to your base.", "Case Sensitive"});
      super.p = 16;
   }

   public void j() {
      String var1 = this.k();
      PacketDispatcher.sendPacketToServer(CDAPacketBaseMemberAdd.a(var1));
      super.mc.displayGuiScreen(super.c);
   }
}
