package com.craftingdead.client.gui.gui;

import com.craftingdead.client.ClientProxy;
import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.GuiHome;
import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

public class GuiSplashScreen extends GuiCDScreen {

   private float c = 0.0F;
   private float d = 0.0F;
   private float e = 0.0F;
   private int p = 0;
   private int q = 0;
   private int r = 0;


   public void updateScreen() {
      if(this.c < 1.0F) {
         this.c += 0.05F;
      } else if(this.q++ >= 10) {
         if(this.d < 1.0F) {
            this.d += 0.05F;
         } else if(this.p++ >= 10) {
            if(this.e < 1.0F) {
               this.e += 0.05F;
            } else {
               if(this.r++ >= 30) {
                  ClientProxy.a = false;
                  Minecraft.getMinecraft().displayGuiScreen(new GuiHome());
               }

            }
         }
      }
   }

   protected void keyTyped(char var1, int var2) {}

   protected void mouseClicked(int var1, int var2, int var3) {}

   public void drawScreen(int var1, int var2, float var3) {
      CDRenderHelper.b(0.0D, 0.0D, (double)super.width, (double)super.height, 0, 1.0F);
      ResourceLocation var4 = new ResourceLocation("craftingdead", "textures/gui/loading_background.png");
      CDRenderHelper.a(0.0D, 0.0D, var4, (double)super.width, (double)super.height, this.c);
      short var5 = 400;
      short var6 = 180;
      CDRenderHelper.a((double)(super.width / 2 - var5 / 2), (double)(super.height / 2 - var6 / 2), new ResourceLocation("craftingdead", "textures/gui/logodark.png"), (double)var5, (double)var6, this.d);
      var5 = 300;
      var6 = 160;
      CDRenderHelper.a((double)(super.width / 2 - var5 / 2), (double)(super.height / 2 - var6 / 2), new ResourceLocation("craftingdead", "textures/gui/loading_title.png"), (double)var5, (double)var6, this.e);
   }
}
