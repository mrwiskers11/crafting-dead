package com.craftingdead.client.gui.gui.server;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.server.ServerRegion;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.gui.widget.server.CDServerData;
import com.craftingdead.client.gui.widget.server.GuiWidgetServers;
import com.craftingdead.client.render.CDRenderHelper;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerAddress;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet254ServerPing;
import net.minecraft.util.ChatAllowedCharacters;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;

public class GuiServerList extends GuiCDScreen {

   public ServerRegion c;


   public GuiServerList(ServerRegion var1) {
      this.c = var1;
   }

   public void initGui() {
      super.initGui();
      super.a();
      int var1 = super.width / 2 - 173;
      GuiCDButton var2 = (new GuiCDButton(50, var1 + 1, 211, 100, 20, EnumChatFormatting.BOLD + "Join Server")).b(true);
      super.buttonList.add(var2);
      var2 = (new GuiCDButton(51, var1 + 251, 211, 100, 20, EnumChatFormatting.BOLD + "Refresh")).b(true);
      super.buttonList.add(var2);
      var2 = (new GuiCDButton(53, var1 + 1, 25, 100, 20, EnumChatFormatting.BOLD + "Network Store")).b(true);
      super.buttonList.add(var2);
      var2 = (new GuiCDButton(54, var1 + 251, 25, 100, 20, EnumChatFormatting.BOLD + "Server Maps")).b(true);
      super.buttonList.add(var2);
      this.a(new GuiWidgetServers(1, var1 + 1, 47, 350, 162, this, (List)super.a.f().i().get(this.c)));
   }

   public void actionPerformed(GuiButton var1) {
      super.actionPerformed(var1);
      if(var1.id == 50) {
         GuiWidgetServers var2 = (GuiWidgetServers)this.a(1);
         if(var2.i() != null) {
            CDServerData var3 = var2.i().a;
            super.mc.displayGuiScreen(new GuiConnecting(this, super.mc, var3));
         }
      }

      if(var1.id == 51) {
         GuiServerList var4 = new GuiServerList(this.c);
         super.mc.displayGuiScreen(var4);
      }

      if(var1.id == 53) {
         a(super.a.f().k());
      }

      if(var1.id == 54) {
         a(super.a.f().l());
      }

   }

   public void drawScreen(int var1, int var2, float var3) {
      super.drawScreen(var1, var2, var3);
      if(this.a(1) instanceof GuiWidgetServers) {
         GuiWidgetServers var4 = (GuiWidgetServers)this.a(1);
         if(var4.i.size() <= 0) {
            CDRenderHelper.b(EnumChatFormatting.BOLD + "Couldn\'t obtain the server list.", super.width / 2, 70);
            CDRenderHelper.b(EnumChatFormatting.RED + "Cloud connection is required.", super.width / 2, 80);
         }
      }

   }

   public static void a(CDServerData var0) throws IOException {
      ServerAddress var1 = ServerAddress.func_78860_a(var0.serverIP);
      Socket var2 = null;
      DataInputStream var3 = null;
      DataOutputStream var4 = null;

      try {
         var2 = new Socket();
         var2.setSoTimeout(3000);
         var2.setTcpNoDelay(true);
         var2.setTrafficClass(18);
         var2.connect(new InetSocketAddress(var1.getIP(), var1.getPort()), 3000);
         var3 = new DataInputStream(var2.getInputStream());
         var4 = new DataOutputStream(var2.getOutputStream());
         Packet254ServerPing var5 = new Packet254ServerPing(78, var1.getIP(), var1.getPort());
         var4.writeByte(var5.getPacketId());
         var5.writePacketData(var4);
         if(var3.read() != 255) {
            throw new IOException("Bad message");
         }

         String var6 = Packet.readString(var3, 256);
         char[] var7 = var6.toCharArray();

         int var8;
         for(var8 = 0; var8 < var7.length; ++var8) {
            if(var7[var8] != 167 && var7[var8] != 0 && ChatAllowedCharacters.allowedCharacters.indexOf(var7[var8]) < 0) {
               var7[var8] = 63;
            }
         }

         var6 = new String(var7);
         int var9;
         String[] var10;
         String[] var11;
         if(var6.startsWith("§") && var6.length() > 1) {
            var10 = var6.substring(1).split(" ");
            if(MathHelper.parseIntWithDefault(var10[0], 0) == 1) {
               var0.gameVersion = var10[2];
               var8 = MathHelper.parseIntWithDefault(var10[4], 0);
               var9 = MathHelper.parseIntWithDefault(var10[5], 0);
               if(var8 >= 0 && var9 >= 0) {
                  var0.l = var8 + "/" + var9;
               }

               try {
                  var11 = var10[3].split("&&");
                  if(var11.length == 2) {
                     var0.k = "Origins " + var11[0];
                     var0.j = var11[1];
                  }
               } catch (Exception var31) {
                  CraftingDead.d.warning("Failed to read MOTD");
               }

               var0.i = 2;
            }
         } else {
            var10 = var6.substring(1).split(" ");
            var0.gameVersion = "1.6.4";
            var8 = MathHelper.parseIntWithDefault(var10[1], 0);
            var9 = MathHelper.parseIntWithDefault(var10[2], 0);
            if(var8 >= 0 && var9 >= 0) {
               var0.l = var8 + "/" + var9;
            }

            try {
               var11 = var10[0].split("&&");
               if(var11.length == 2) {
                  var0.k = "Origins " + var11[0];
                  var0.j = var11[1];
               }
            } catch (Exception var30) {
               CraftingDead.d.warning("Failed to read MOTD");
            }

            var0.i = 2;
         }
      } catch (ConnectException var32) {
         var0.i = 1;
      } finally {
         try {
            if(var3 != null) {
               var3.close();
            }
         } catch (Throwable var29) {
            ;
         }

         try {
            if(var4 != null) {
               var4.close();
            }
         } catch (Throwable var28) {
            ;
         }

         try {
            if(var2 != null) {
               var2.close();
            }
         } catch (Throwable var27) {
            ;
         }

      }

   }
}
