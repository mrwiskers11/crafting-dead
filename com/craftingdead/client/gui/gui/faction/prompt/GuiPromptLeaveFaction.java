package com.craftingdead.client.gui.gui.faction.prompt;

import com.craftingdead.client.gui.gui.GuiYesNoPrompt;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.faction.packet.PacketFactionCommand;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;

public class GuiPromptLeaveFaction extends GuiYesNoPrompt {

   public GuiPromptLeaveFaction(int var1, GuiScreen var2) {
      super(var1, var2);
      this.a(new String[]{EnumChatFormatting.BOLD + "Are You Sure?", EnumChatFormatting.WHITE + "If You are the Leader", EnumChatFormatting.WHITE + "The Faction Will be Disbanded"});
   }

   public void actionPerformed(GuiButton var1) {
      if(var1.id == 11) {
         CMClient.b(new PacketFactionCommand("DELETE"));
      }

      if(super.mc.currentScreen == this) {
         super.mc.displayGuiScreen(super.c);
      }

   }
}
