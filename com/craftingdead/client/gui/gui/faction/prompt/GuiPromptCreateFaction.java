package com.craftingdead.client.gui.gui.faction.prompt;

import com.craftingdead.client.gui.gui.GuiTextPrompt;
import com.craftingdead.client.gui.gui.faction.GuiFactions;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;

public class GuiPromptCreateFaction extends GuiTextPrompt {

   public GuiPromptCreateFaction(GuiScreen var1) {
      super(var1);
      super.p = 5;
      this.a(new String[]{EnumChatFormatting.BOLD + "Create Faction", EnumChatFormatting.WHITE + "Enter a Faction Tag:", EnumChatFormatting.WHITE + "- " + super.p + " Character Limit", EnumChatFormatting.WHITE + "- Case Insensitive"});
   }

   public void j() {
      String var1 = this.k();
      if(!var1.matches("^[a-zA-Z0-9_.-]*$")) {
         super.w = "Name contains bad characters";
      } else if(var1.length() < 3) {
         super.w = "Name is too small";
      } else {
         CDPlayer var2 = PlayerDataHandler.a(super.mc.getSession().getUsername());
         FactionManager.c().a(var2, var1);
         super.mc.displayGuiScreen(new GuiFactions(this));
      }
   }
}
