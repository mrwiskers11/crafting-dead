package com.craftingdead.client.gui.gui.faction.prompt;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.gui.gui.GuiTextPrompt;
import com.craftingdead.client.gui.gui.faction.GuiFactions;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.faction.packet.PacketFactionCommand;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;

public class GuiPromptFactionMember extends GuiTextPrompt {

   private String x;


   public GuiPromptFactionMember(GuiScreen var1, String var2, String var3) {
      super(var1);
      this.x = var2;
      this.a(new String[]{EnumChatFormatting.BOLD + "" + var3, EnumChatFormatting.WHITE + "Enter Username.", EnumChatFormatting.WHITE + "", EnumChatFormatting.WHITE + "Case Sensitive"});
   }

   public void j() {
      String var1 = this.k();
      if(FactionManager.c().b()) {
         String var2 = "UPDATE MEMBER " + this.x + " " + var1.trim();
         CraftingDead.d.info("[Faction] Sent command. " + var2);
         CMClient.b(new PacketFactionCommand(var2));
      }

      super.mc.displayGuiScreen(new GuiFactions(this));
   }
}
