package com.craftingdead.client.gui.gui.faction.prompt;

import com.craftingdead.client.gui.gui.GuiYesNoPrompt;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.faction.packet.PacketFactionCommand;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;

public class GuiPromptJoinFaction extends GuiYesNoPrompt {

   public GuiPromptJoinFaction(int var1, GuiScreen var2, String var3) {
      super(var1, var2);
      this.a(new String[]{EnumChatFormatting.BOLD + "Are You Sure?", EnumChatFormatting.WHITE + "You will be joining the faction: ", EnumChatFormatting.GREEN + EnumChatFormatting.BOLD.toString() + var3});
   }

   public void actionPerformed(GuiButton var1) {
      if(var1.id == 11) {
         CMClient.b(new PacketFactionCommand("ACCEPT"));
      }

      if(super.mc.currentScreen == this) {
         super.mc.displayGuiScreen(super.c);
      }

   }
}
