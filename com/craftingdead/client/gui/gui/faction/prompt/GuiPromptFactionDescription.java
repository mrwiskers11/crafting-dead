package com.craftingdead.client.gui.gui.faction.prompt;

import com.craftingdead.client.gui.gui.GuiTextPrompt;
import com.craftingdead.client.gui.gui.faction.GuiFactions;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.faction.packet.PacketFactionCommand;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;

public class GuiPromptFactionDescription extends GuiTextPrompt {

   public GuiPromptFactionDescription(GuiScreen var1) {
      super(var1);
      super.p = 40;
      this.a(new String[]{EnumChatFormatting.BOLD + "Enter a new description!", EnumChatFormatting.WHITE + "", EnumChatFormatting.WHITE + "40 Characters Max", EnumChatFormatting.WHITE + ""});
   }

   public void j() {
      String var1 = this.k();
      if(FactionManager.c().b()) {
         String var2 = "UPDATE DESC " + var1.trim();
         CMClient.b(new PacketFactionCommand(var2));
      }

      super.mc.displayGuiScreen(new GuiFactions(this));
   }
}
