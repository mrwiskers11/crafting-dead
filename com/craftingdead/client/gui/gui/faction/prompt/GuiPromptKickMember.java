package com.craftingdead.client.gui.gui.faction.prompt;

import com.craftingdead.client.ClientNotification;
import com.craftingdead.client.gui.gui.GuiTextPrompt;
import com.craftingdead.client.gui.gui.faction.GuiFactions;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;

public class GuiPromptKickMember extends GuiTextPrompt {

   public GuiPromptKickMember(GuiScreen var1) {
      super(var1);
      super.p = 16;
      this.a(new String[]{EnumChatFormatting.BOLD + "Kick Member", EnumChatFormatting.WHITE + "- Enter Username", EnumChatFormatting.WHITE + "- Case Insensitive"});
   }

   public void j() {
      String var1 = this.k();
      ClientNotification var2 = (new ClientNotification(var1 + " has been removed from your Faction!")).a("Faction Notification");
      super.a.d().a(var2);
      super.mc.displayGuiScreen(new GuiFactions(this));
   }
}
