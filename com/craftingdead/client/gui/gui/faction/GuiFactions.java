package com.craftingdead.client.gui.gui.faction;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.GuiHome;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownSettings;
import com.craftingdead.client.gui.gui.faction.dropdown.GuiDropDownFactionMembers;
import com.craftingdead.client.gui.gui.faction.dropdown.GuiDropDownFactionOptions;
import com.craftingdead.client.gui.gui.faction.dropdown.GuiDropDownFactionPerms;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.gui.widget.GuiWidgetText;
import com.craftingdead.client.gui.widget.list.GuiWidgetList;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.faction.Faction;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.mojang.MojangAPI;
import com.craftingdead.player.PlayerDataHandler;

import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

public class GuiFactions extends GuiCDScreen {

   private static final ResourceLocation c = new ResourceLocation("craftingdead", "textures/gui/home.png");
   private static final ResourceLocation d = new ResourceLocation("craftingdead", "textures/gui/settings.png");
   private static final ResourceLocation e = new ResourceLocation("craftingdead", "textures/gui/power.png");
   private GuiScreen p;
   private boolean q = false;


   public GuiFactions(GuiScreen var1) {
      this.p = var1;
   }

   public void initGui() {
      super.initGui();
      this.q = FactionManager.c().b();
      int var1 = super.width / 12;
      int var2 = super.width / 2 - 173;
      if(FactionManager.c().b()) {
         Faction var3 = FactionManager.c().a();
         byte var4 = 1;
         byte var5 = 85;
         super.buttonList.add(new GuiCDButton(1, var2 + 1, var4, 30, 20, c));
         super.buttonList.add(new GuiCDButton(2, var2 + 35 + 3 * var5, var4, 30, 20, d));
         super.buttonList.add(new GuiCDButton(3, var2 + 66 + 3 * var5, var4, 30, 20, e));
         super.buttonList.add((new GuiCDButton(10, var2 + 32, var4, var5, 20, EnumChatFormatting.BOLD + "Members")).b(true).a('\u9922'));
         super.buttonList.add((new GuiCDButton(11, var2 + 33 + var5, var4, var5, 20, EnumChatFormatting.BOLD + "Permissions")).b(true).a('\u9922'));
         super.buttonList.add((new GuiCDButton(12, var2 + 34 + 2 * var5, var4, var5, 20, EnumChatFormatting.BOLD + "Options")).b(true).a('\u9922'));
         String var6 = var3.c;
         String var7 = var3.h;
         this.a((new GuiWidgetText(1, var1 * 6 - 115, 55, 247, 14, this)).a(new String[]{EnumChatFormatting.RED + "" + var6}).d().a(16777215));
         this.a((new GuiWidgetText(2, var1 * 6 - 115, 70, 247, 14, this)).a(new String[]{EnumChatFormatting.WHITE + var7}).d().a(3355443));
         this.a((new GuiWidgetText(3, var1 * 6 - 115, 100, 118, 13, this)).a(new String[]{EnumChatFormatting.WHITE + "Members"}).d().a(0));
         this.a((new GuiWidgetText(4, var1 * 6 + 7, 100, 144, 13, this)).a(new String[]{EnumChatFormatting.WHITE + "Faction Stats"}).a(0));
         ArrayList var8 = new ArrayList();
         Iterator var9 = FactionManager.c().a().f.iterator();

         while(var9.hasNext()) {
            String var10 = (String)var9.next();
            if(var3.b(MojangAPI.getUUID(var10, true).toString())) {
               var8.add(EnumChatFormatting.GOLD + "[Owner] " + var10);
            } else if(var3.a(var10)) {
               var8.add(EnumChatFormatting.RED + "[Admin] " + var10);
            } else {
               var8.add("[Member] " + var10);
            }
         }

         this.a((new GuiWidgetList(2, var1 * 6 - 115, 115, 120, 130, this)).b(var8));
         ArrayList var14 = new ArrayList();
         int var15 = var3.i;
         int var11 = var3.j;
         int var12 = var3.k;
         int var13 = var3.l;
         var14.add("Total Player Kills: " + var15);
         var14.add("Total Zombie Kills: " + var11);
         var14.add("Total Deaths: " + var12);
         var14.add(" ");
         var14.add("Total Average Karma: " + var13);
         this.a((new GuiWidgetList(3, var1 * 6 + 7, 115, 144, 60, this)).b(var14));
      } else {
         super.mc.displayGuiScreen(new GuiHome());
      }

   }

   public void actionPerformed(GuiButton var1) {
      super.actionPerformed(var1);
      byte var2 = 85;
      int var3 = super.width / 2 - 173;
      CDPlayer var4 = PlayerDataHandler.a(super.mc.getSession().getUsername());
      if(var1.id == 1) {
         super.mc.displayGuiScreen(new GuiHome());
      }

      if(var1.id == 2) {
         GuiDropDownSettings var5 = new GuiDropDownSettings(this, var3 + 35 + 255, 23, 85, 20);
         super.mc.displayGuiScreen(var5);
      }

      if(var1.id == 3) {
         super.mc.shutdown();
      }

      if(FactionManager.c().b()) {
         Faction var8 = FactionManager.c().a();
         boolean var6 = var8.b(MojangAPI.getUUID(var4.b(), false).toString());
         boolean var7 = var8.a(var4.b());
         if(var1.id == 10 && (var7 || var6)) {
            super.mc.displayGuiScreen(new GuiDropDownFactionMembers(this, var3 + 32, 22, var2, 20));
         }

         if(var1.id == 11 && var6) {
            super.mc.displayGuiScreen(new GuiDropDownFactionPerms(this, var3 + 33 + var2, 22, var2, 20));
         }

         if(var1.id == 12) {
            super.mc.displayGuiScreen(new GuiDropDownFactionOptions(this, var3 + 34 + 2 * var2, 22, var2, 20, var6));
         }
      }

   }

   public void updateScreen() {
      super.updateScreen();
   }

   public void drawScreen(int var1, int var2, float var3) {
      if(!super.a.c()) {
         super.mc.displayGuiScreen(new GuiHome());
      } else {
         super.drawScreen(var1, var2, var3);
         int var4 = super.width / 12;
         int var5 = super.height / 2;
         if(FactionManager.c().b()) {
            if(!this.q) {
               super.mc.displayGuiScreen(new GuiFactions(this.p));
            }
         } else {
            CDRenderHelper.b(EnumChatFormatting.RED + "Faction Menu", var4 * 7, var5 / 4, 1.5D);
            if(this.q) {
               super.mc.displayGuiScreen(new GuiFactions(this.p));
            }
         }

         this.q = FactionManager.c().b();
      }
   }

}
