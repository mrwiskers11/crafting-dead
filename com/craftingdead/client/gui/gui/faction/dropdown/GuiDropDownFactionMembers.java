package com.craftingdead.client.gui.gui.faction.dropdown;

import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;
import com.craftingdead.client.gui.gui.faction.prompt.GuiPromptFactionMember;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownFactionMembers extends GuiDropDownMenu {

   public GuiDropDownFactionMembers(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Invite");
      this.a("Kick");
   }

   public void d(int var1) {
      if(var1 == 1) {
         super.mc.displayGuiScreen(new GuiPromptFactionMember(super.c, "INVITE", "Invite to Faction"));
      }

      if(var1 == 2) {
         super.mc.displayGuiScreen(new GuiPromptFactionMember(super.c, "REMOVE", "Remove from Faction"));
      }

   }
}
