package com.craftingdead.client.gui.gui.faction.dropdown;

import com.craftingdead.client.ClientNotification;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;
import com.craftingdead.client.gui.gui.faction.prompt.GuiPromptCreateFaction;
import com.craftingdead.client.gui.gui.faction.prompt.GuiPromptJoinFaction;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownFactions extends GuiDropDownMenu {

   private String t;


   public GuiDropDownFactions(GuiScreen var1, int var2, int var3, int var4, int var5, String var6) {
      super(var1, var2, var3, var4, var5);
      this.t = var6;
      this.a("Create");
      if(this.t != null && this.t.length() > 0) {
         this.a("Join");
      }

   }

   public void d(int var1) {
      if(var1 == 1) {
         super.mc.displayGuiScreen(new GuiPromptCreateFaction(this));
      }

      if(var1 == 2) {
         if(this.t != null && this.t.length() > 0) {
            super.mc.displayGuiScreen(new GuiPromptJoinFaction(1, this, this.t));
         } else {
            ClientNotification var2 = (new ClientNotification("You do not have any faction invites!")).a("Faction Notification");
            super.a.d().a(var2);
         }
      }

   }
}
