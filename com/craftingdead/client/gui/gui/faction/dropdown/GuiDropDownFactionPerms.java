package com.craftingdead.client.gui.gui.faction.dropdown;

import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;
import com.craftingdead.client.gui.gui.faction.prompt.GuiPromptFactionMember;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownFactionPerms extends GuiDropDownMenu {

   public GuiDropDownFactionPerms(GuiScreen var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4, var5);
      this.a("Promote");
      this.a("Demote");
   }

   public void d(int var1) {
      if(var1 == 1) {
         super.mc.displayGuiScreen(new GuiPromptFactionMember(super.c, "PROMOTE", "Promote to Admin"));
      }

      if(var1 == 2) {
         super.mc.displayGuiScreen(new GuiPromptFactionMember(super.c, "DEMOTE", "Demote from Admin"));
      }

   }
}
