package com.craftingdead.client.gui.gui.faction.dropdown;

import com.craftingdead.client.gui.gui.dropdown.GuiDropDownMenu;
import com.craftingdead.client.gui.gui.faction.prompt.GuiPromptFactionDescription;
import com.craftingdead.client.gui.gui.faction.prompt.GuiPromptLeaveFaction;

import net.minecraft.client.gui.GuiScreen;

public class GuiDropDownFactionOptions extends GuiDropDownMenu {

   private boolean t;


   public GuiDropDownFactionOptions(GuiScreen var1, int var2, int var3, int var4, int var5, boolean var6) {
      super(var1, var2, var3, var4, var5);
      this.t = var6;
      if(var6) {
         this.a("Description");
         this.a("Disband");
      } else {
         this.a("Leave");
      }

   }

   public void d(int var1) {
      if(this.t) {
         if(var1 == 1) {
            super.mc.displayGuiScreen(new GuiPromptFactionDescription(super.c));
         }

         if(var1 == 2) {
            super.mc.displayGuiScreen(new GuiPromptLeaveFaction(1, this));
         }
      } else if(var1 == 1) {
         super.mc.displayGuiScreen(new GuiPromptLeaveFaction(1, this));
      }

   }
}
