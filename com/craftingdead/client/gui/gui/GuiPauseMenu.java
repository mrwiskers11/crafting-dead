package com.craftingdead.client.gui.gui;

import com.craftingdead.client.gui.gui.GuiSettings;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraft.util.EnumChatFormatting;

public class GuiPauseMenu extends GuiIngameMenu {

   public void initGui() {
      super.initGui();
      super.buttonList.add(new GuiButton(8, super.width / 2 - 100, super.height / 4 + 56, EnumChatFormatting.RED + EnumChatFormatting.BOLD.toString() + "Crafting Dead Options"));
   }

   public void actionPerformed(GuiButton var1) {
      super.actionPerformed(var1);
      if(var1.id == 8) {
         super.mc.displayGuiScreen(new GuiSettings(this));
      }

   }
}
