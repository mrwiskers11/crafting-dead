package com.craftingdead.client.gui.gui.minimap;

import com.craftingdead.client.mapwriter.MapManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

public class GuiMapOptionSlot extends GuiSlot {

   private Minecraft b;
   private MapManager h;
   private int i = 0;
   private int j = 0;
   private int k = 0;
   private static final String[] l = new String[]{"unchanged", "top right", "top left", "bottom right", "bottom left"};
   private static final String[] m = new String[]{"disabled", "small", "large"};
   private static final String[] n = new String[]{"none", "static", "panning"};
   private GuiButton[] o = new GuiButton[12];
   static final ResourceLocation a = new ResourceLocation("textures/gui/widgets.png");


   public void c(int var1) {
      switch(var1) {
      case 0:
         this.o[var1].displayString = "Draw coords: " + m[this.h.h];
         break;
      case 1:
         this.o[var1].displayString = "Circular mode: " + this.h.U.d.l;
         break;
      case 2:
         this.o[var1].displayString = "Texture size: " + this.h.r;
         break;
      case 3:
         this.o[var1].displayString = "Texture scaling: " + (this.h.g?"linear":"nearest");
         break;
      case 4:
         this.o[var1].displayString = "Trail markers: " + this.h.Z.d;
         break;
      case 5:
         this.o[var1].displayString = "Map colours: " + (this.h.n?"frozen":"auto");
         break;
      case 6:
         this.o[var1].displayString = "Max draw distance: " + Math.round(Math.sqrt((double)this.h.o));
         break;
      case 7:
         this.o[var1].displayString = "Mini map size: " + this.h.U.d.w;
         break;
      case 8:
         this.o[var1].displayString = "Mini map position: " + l[this.k];
         break;
      case 9:
         this.o[var1].displayString = "Map pixel snapping: " + (this.h.p?"enabled":"disabled");
         break;
      case 10:
         this.o[var1].displayString = "Max death markers: " + this.h.s;
         break;
      case 11:
         this.o[var1].displayString = "Background mode: " + n[this.h.y];
      }

   }

   public GuiMapOptionSlot(GuiScreen var1, Minecraft var2, MapManager var3) {
      super(var2, var1.width, var1.height, 16, var1.height - 32, 25);
      this.h = var3;
      this.b = var2;

      for(int var4 = 0; var4 < this.o.length; ++var4) {
         this.o[var4] = new GuiButton(300 + var4, 0, 0, "");
         this.c(var4);
      }

   }

   protected boolean a(char var1, int var2) {
      return false;
   }

   protected int getSize() {
      return this.o.length;
   }

   protected void elementClicked(int var1, boolean var2) {
      switch(var1) {
      case 0:
         this.h.l();
         break;
      case 1:
         this.h.U.b();
         break;
      case 2:
         this.h.r *= 2;
         if(this.h.r > 4096) {
            this.h.r = 1024;
         }
         break;
      case 3:
         this.h.g = !this.h.g;
         this.h.S.a(this.h.g);
         break;
      case 4:
         this.h.Z.d = !this.h.Z.d;
         break;
      case 5:
         this.h.n = !this.h.n;
         this.h.j();
         break;
      case 6:
         int var3 = Math.round((float)Math.sqrt((double)this.h.o));
         var3 += 32;
         if(var3 > 256) {
            var3 = 64;
         }

         this.h.o = var3 * var3;
         break;
      case 7:
         this.h.U.d.e();
         break;
      case 8:
         ++this.k;
         if(this.k >= l.length) {
            this.k = 1;
         }

         switch(this.k) {
         case 1:
            this.h.U.d.a(10, -1, -1, 10);
            break;
         case 2:
            this.h.U.d.a(10, -1, 10, -1);
            break;
         case 3:
            this.h.U.d.a(-1, 40, -1, 10);
            break;
         case 4:
            this.h.U.d.a(-1, 40, 10, -1);
         }
      case 9:
         this.h.p = !this.h.p;
         break;
      case 10:
         ++this.h.s;
         if(this.h.s > 10) {
            this.h.s = 0;
         }
         break;
      case 11:
         this.h.y = (this.h.y + 1) % 3;
      }

      this.c(var1);
   }

   public void drawScreen(int var1, int var2, float var3) {
      this.i = var1;
      this.j = var2;
      super.drawScreen(var1, var2, var3);
   }

   protected boolean isSelected(int var1) {
      return false;
   }

   protected void drawBackground() {}

   protected void drawSlot(int var1, int var2, int var3, int var4, Tessellator var5) {
      GuiButton var6 = this.o[var1];
      var6.xPosition = var2;
      var6.yPosition = var3;
      var6.drawButton(this.b, this.i, this.j);
   }

}
