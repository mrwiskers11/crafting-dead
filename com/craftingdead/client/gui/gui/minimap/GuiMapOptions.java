package com.craftingdead.client.gui.gui.minimap;

import com.craftingdead.client.gui.gui.minimap.GuiMapOptionSlot;
import com.craftingdead.client.mapwriter.MapManager;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiMapOptions extends GuiScreen {

   private final MapManager a;
   private final GuiScreen b;
   private GuiMapOptionSlot c = null;


   public GuiMapOptions(GuiScreen var1, MapManager var2) {
      this.a = var2;
      this.b = var1;
   }

   public void initGui() {
      this.c = new GuiMapOptionSlot(this, super.mc, this.a);
      this.c.registerScrollButtons(7, 8);
      super.buttonList.add(new GuiButton(200, super.width / 2 - 50, super.height - 28, 100, 20, "Done"));
   }

   protected void actionPerformed(GuiButton var1) {
      if(var1.id == 200) {
         this.a.f();
         super.mc.displayGuiScreen(this.b);
      }

   }

   public void drawScreen(int var1, int var2, float var3) {
      this.drawDefaultBackground();
      this.c.drawScreen(var1, var2, var3);
      this.drawCenteredString(super.fontRenderer, "Map Options", super.width / 2, 10, 16777215);
      super.drawScreen(var1, var2, var3);
   }

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
   }

   protected void keyTyped(char var1, int var2) {
      if(this.c.a(var1, var2)) {
         super.keyTyped(var1, var2);
      }

   }
}
