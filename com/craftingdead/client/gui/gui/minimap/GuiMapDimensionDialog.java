package com.craftingdead.client.gui.gui.minimap;

import com.craftingdead.client.gui.gui.minimap.GuiMapTextDialog;
import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.map.MapView;

import net.minecraft.client.gui.GuiScreen;

public class GuiMapDimensionDialog extends GuiMapTextDialog {

   final MapManager a;
   final MapView b;
   final int c;


   public GuiMapDimensionDialog(GuiScreen var1, MapManager var2, MapView var3, int var4) {
      super(var1, "Set dimension to:", "" + var4, "invalid dimension");
      this.a = var2;
      this.b = var3;
      this.c = var4;
   }

   public boolean a() {
      boolean var1 = false;
      int var2 = this.h();
      if(super.s) {
         this.b.d(var2);
         this.a.U.g.c(var2);
         this.a.a(var2);
         var1 = true;
      }

      return var1;
   }
}
