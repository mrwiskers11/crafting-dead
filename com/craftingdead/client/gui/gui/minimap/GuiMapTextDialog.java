package com.craftingdead.client.gui.gui.minimap;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class GuiMapTextDialog extends GuiScreen {

   private final GuiScreen a;
   String e;
   String p;
   String q;
   GuiTextField r = null;
   boolean s = false;
   boolean t = false;
   boolean u = false;
   static final int v = 50;
   static final int w = 80;
   static final int x = 92;
   static final int y = 108;


   public GuiMapTextDialog(GuiScreen var1, String var2, String var3, String var4) {
      this.a = var1;
      this.e = var2;
      this.p = var3;
      this.q = var4;
   }

   private void j() {
      if(this.r != null) {
         this.p = this.r.getText();
      }

      int var1 = super.width * 50 / 100;
      this.r = new GuiTextField(super.fontRenderer, (super.width - var1) / 2 + 5, 92, var1 - 10, 12);
      this.r.setMaxStringLength(32);
      this.r.setFocused(true);
      this.r.setCanLoseFocus(false);
      this.r.setText(this.p);
   }

   public void a(String var1) {
      this.r.setText(var1);
      this.p = var1;
   }

   public String g() {
      String var1 = this.r.getText().trim();
      this.s = var1.length() > 0;
      this.t = !this.s;
      return var1;
   }

   public int h() {
      String var1 = this.g();
      int var2 = 0;

      try {
         var2 = Integer.parseInt(var1);
         this.s = true;
         this.t = false;
      } catch (NumberFormatException var4) {
         this.s = false;
         this.t = true;
      }

      return var2;
   }

   public int i() {
      String var1 = this.g();
      int var2 = 0;

      try {
         var2 = Integer.parseInt(var1, 16);
         this.s = true;
         this.t = false;
      } catch (NumberFormatException var4) {
         this.s = false;
         this.t = true;
      }

      return var2;
   }

   public boolean a() {
      return false;
   }

   public void initGui() {
      this.j();
   }

   public void drawScreen(int var1, int var2, float var3) {
      if(this.a != null) {
         this.a.drawScreen(var1, var2, var3);
      } else {
         this.drawDefaultBackground();
      }

      int var4 = super.width * 50 / 100;
      drawRect((super.width - var4) / 2, 76, (super.width - var4) / 2 + var4, 122, Integer.MIN_VALUE);
      this.drawCenteredString(super.fontRenderer, this.e, super.width / 2, 80, 16777215);
      this.r.drawTextBox();
      if(this.t) {
         this.drawCenteredString(super.fontRenderer, this.q, super.width / 2, 108, 16777215);
      }

      super.drawScreen(var1, var2, var3);
   }

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
   }

   protected void keyTyped(char var1, int var2) {
      switch(var2) {
      case 1:
         super.mc.displayGuiScreen(this.a);
         break;
      case 28:
         if(this.a()) {
            if(!this.u) {
               super.mc.displayGuiScreen(this.a);
            } else {
               super.mc.displayGuiScreen((GuiScreen)null);
            }
         }
         break;
      default:
         this.r.textboxKeyTyped(var1, var2);
         this.p = this.r.getText();
      }

   }
}
