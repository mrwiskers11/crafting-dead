package com.craftingdead.client.gui.gui.minimap;

import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.client.gui.gui.minimap.GuiMapDimensionDialog;
import com.craftingdead.client.gui.gui.minimap.GuiMapMarkerDialog;
import com.craftingdead.client.gui.gui.minimap.GuiMapOptions;
import com.craftingdead.client.gui.gui.minimap.GuiMapTeleportDialog;
import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.MapUtil;
import com.craftingdead.client.mapwriter.api.IMwDataProvider;
import com.craftingdead.client.mapwriter.api.MwAPI;
import com.craftingdead.client.mapwriter.map.MapRenderer;
import com.craftingdead.client.mapwriter.map.MapView;
import com.craftingdead.client.mapwriter.map.Marker;
import com.craftingdead.client.mapwriter.map.mapmode.FullScreenMapMode;
import com.craftingdead.client.mapwriter.map.mapmode.MapMode;
import com.craftingdead.client.mapwriter.tasks.MergeTask;
import com.craftingdead.client.mapwriter.tasks.RebuildRegionsTask;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.awt.Point;
import java.awt.geom.Point2D.Double;
import java.util.Iterator;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

@SideOnly(Side.CLIENT)
public class GuiMap extends GuiScreen {
   private MapManager a;
   private MapMode b;
   private MapView c;
   private MapRenderer d;
   private static final double e = 0.3D;
   private static final int p = 5;
   private static final int q = 5;
   private int r;
   private int s;
   private int t;
   private double u;
   private double v;
   private Marker w;
   private int x;
   private int y;
   private int z;
   private int A;
   private int B;
   private int C;
   private a D;
   private a E;
   private a F;
   private a G;
   private a H;

   public GuiMap(MapManager var1) {
      this.r = 0;
      this.s = 0;
      this.t = 0;
      this.w = null;
      this.x = 0;
      this.y = 0;
      this.z = 0;
      this.A = 0;
      this.B = 0;
      this.C = 0;
      this.a = var1;
      this.b = new FullScreenMapMode(var1.c);
      this.c = new MapView(this.a);
      this.d = new MapRenderer(this.a, this.b, this.c);
      this.c.c(this.a.U.g.g());
      this.c.a(this.a.D, this.a.E, this.a.K);
      this.c.a(0);
      this.D = new a(this);
      this.E = new a(this);
      this.F = new a(this);
      this.G = new a(this);
      this.H = new a(this);
   }

   public GuiMap(MapManager var1, int var2, int var3, int var4) {
      this(var1);
      this.c.c(var2);
      this.c.a((double)var3, (double)var4, var2);
      this.c.a(0);
   }

   public void initGui() {
   }

   protected void actionPerformed(GuiButton var1) {
   }

   public void a() {
      this.b.c();
      Keyboard.enableRepeatEvents(false);
      super.mc.displayGuiScreen((GuiScreen)null);
      super.mc.setIngameFocus();
      super.mc.sndManager.resumeAllSounds();
   }

   public Marker a(int var1, int var2) {
      Marker var3 = null;
      Iterator var4 = this.a.V.c.iterator();

      while(var4.hasNext()) {
         Marker var5 = (Marker)var4.next();
         if(var5.h != null && var5.h.distanceSq((double)var1, (double)var2) < 6.0D) {
            var3 = var5;
         }
      }

      return var3;
   }

   public int b(int var1, int var2) {
      int var3 = 0;
      if(this.a.a.theWorld.provider.dimensionId == this.c.g()) {
         var3 = this.a.a.theWorld.getChunkFromBlockCoords(var1, var2).getHeightValue(var1 & 15, var2 & 15);
      }

      return var3;
   }

   public boolean c(int var1, int var2) {
      Double var3 = this.d.a;
      return var3.distanceSq((double)var1, (double)var2) < 9.0D;
   }

   public void g() {
      if(this.a.V.d != null) {
         this.a.V.c(this.a.V.d);
         this.a.V.c();
         this.a.V.d = null;
      }

   }

   public void h() {
      this.a.Y.b();
      this.a.T.a(new MergeTask(this.a.X, (int)this.c.a(), (int)this.c.b(), (int)this.c.c(), (int)this.c.d(), this.c.g(), this.a.e, this.a.e.getName()));
      MapUtil.b("merging to \'" + this.a.e.getAbsolutePath() + "\'");
   }

   public void i() {
      MapUtil.b(String.format("regenerating %dx%d blocks starting from (%d, %d)", new Object[]{Integer.valueOf((int)this.c.c()), Integer.valueOf((int)this.c.d()), Integer.valueOf((int)this.c.h()), Integer.valueOf((int)this.c.j())}));
      this.a.j();
      this.a.T.a(new RebuildRegionsTask(this.a, (int)this.c.h(), (int)this.c.j(), (int)this.c.c(), (int)this.c.d(), this.c.g()));
   }

   protected void keyTyped(char var1, int var2) {
      switch(var2) {
      case 1:
         this.a();
         break;
      case 19:
         this.i();
         this.a();
         break;
      case 20:
         if(this.a.V.d != null) {
            this.a.a(this.a.V.d);
            this.a();
         } else {
            super.mc.displayGuiScreen(new GuiMapTeleportDialog(this, this.a, this.c, this.z, this.a.k, this.B));
         }
         break;
      case 25:
         this.h();
         this.a();
         break;
      case 46:
         if(this.a.V.d != null) {
            this.a.V.d.c();
         }
         break;
      case 49:
         this.a.V.e();
         break;
      case 57:
         this.a.V.d();
         this.a.V.c();
         break;
      case 199:
         this.c.a(this.a.D, this.a.E, this.a.K);
         break;
      case 200:
         this.c.b(0.0D, -0.3D);
         break;
      case 203:
         this.c.b(-0.3D, 0.0D);
         break;
      case 205:
         this.c.b(0.3D, 0.0D);
         break;
      case 207:
         if(this.a.V.d != null) {
            this.c.a((double)this.a.V.d.c, (double)this.a.V.d.e, 0);
         }
         break;
      case 208:
         this.c.b(0.0D, 0.3D);
         break;
      case 211:
         this.g();
         break;
      default:
         if(var2 == KeyBindingManager.j.keyCode) {
            this.C = 1;
         } else if(var2 == KeyBindingManager.o.keyCode) {
            this.c.b(-1);
         } else if(var2 == KeyBindingManager.p.keyCode) {
            this.c.b(1);
         } else if(var2 == KeyBindingManager.m.keyCode) {
            this.a.V.d();
            this.a.V.c();
         }
      }

   }

   public void handleMouseInput() {
      if(MwAPI.b() == null || !MwAPI.b().b(this.c, this.b)) {
         int var1 = Mouse.getEventX() * super.width / super.mc.displayWidth;
         int var2 = super.height - Mouse.getEventY() * super.height / super.mc.displayHeight - 1;
         int var3 = Mouse.getEventDWheel();
         if(var3 != 0) {
            this.c(var1, var2, var3);
         }

         super.handleMouseInput();
      }
   }

   protected void mouseClicked(int var1, int var2, int var3) {
      Marker var4 = this.a(var1, var2);
      Marker var5 = this.a.V.d;
      if(var3 == 0) {
         if(this.F.a(var1, var2)) {
            super.mc.displayGuiScreen(new GuiMapDimensionDialog(this, this.a, this.c, this.c.g()));
         } else if(this.E.a(var1, var2)) {
            super.mc.displayGuiScreen(new GuiMapOptions(this, this.a));
         } else {
            this.r = 1;
            this.s = var1;
            this.t = var2;
            this.a.V.d = var4;
            if(var4 != null && var5 == var4) {
               this.w = var4;
               this.x = var4.c;
               this.y = var4.e;
            }
         }
      } else if(var3 == 1) {
         if(var4 != null && var5 == var4) {
            super.mc.displayGuiScreen(new GuiMapMarkerDialog(this, this.a.V, var4));
         } else if(var4 == null) {
            String var6 = this.a.V.a();
            if(var6.equals("none")) {
               var6 = "group";
            }

            int var7;
            int var8;
            int var9;
            if(this.c(var1, var2)) {
               var7 = this.a.G;
               var8 = this.a.H;
               var9 = this.a.I;
            } else {
               var7 = this.z;
               var8 = this.A > 0?this.A:this.a.k;
               var9 = this.B;
            }

            super.mc.displayGuiScreen(new GuiMapMarkerDialog(this, this.a.V, "", var6, var7, var8, var9, this.c.g()));
         }
      } else if(var3 == 2) {
         Point var10 = this.b.a(this.c, var1, var2);
         IMwDataProvider var11 = MwAPI.b();
         if(var11 != null) {
            var11.a(this.c.g(), var10.x, var10.y, this.c);
         }
      }

      this.u = this.c.a();
      this.v = this.c.b();
   }

   protected void mouseMovedOrUp(int var1, int var2, int var3) {
      if(var3 == 0) {
         this.r = 0;
         this.w = null;
      } else if(var3 == 1) {
         ;
      }

   }

   public void c(int var1, int var2, int var3) {
      Marker var4 = this.a(var1, var2);
      if(var4 != null && var4 == this.a.V.d) {
         if(var3 > 0) {
            var4.c();
         } else {
            var4.d();
         }
      } else {
         int var5;
         if(this.F.a(var1, var2)) {
            var5 = var3 > 0?1:-1;
            this.c.a(this.a.C, var5);
         } else if(this.G.a(var1, var2)) {
            var5 = var3 > 0?1:-1;
            this.a.V.a(var5);
            this.a.V.c();
         } else if(this.H.a(var1, var2)) {
            var5 = var3 > 0?1:-1;
            if(MwAPI.b() != null) {
               MwAPI.b().b(this.c);
            }

            if(var5 == 1) {
               MwAPI.d();
            } else {
               MwAPI.e();
            }

            if(MwAPI.b() != null) {
               MwAPI.b().a(this.c);
            }
         } else {
            var5 = var3 > 0?-1:1;
            this.c.a(this.c.e() + var5, (double)this.z, (double)this.B);
         }
      }

   }

   public void updateScreen() {
      if(this.C > 0) {
         ++this.C;
      }

      if(this.C > 2) {
         this.a();
      }

      super.updateScreen();
   }

   public void d(int var1, int var2, int var3) {
      String var4;
      if(var2 != 0) {
         var4 = String.format("cursor: (%d, %d, %d)", new Object[]{Integer.valueOf(var1), Integer.valueOf(var2), Integer.valueOf(var3)});
      } else {
         var4 = String.format("cursor: (%d, ?, %d)", new Object[]{Integer.valueOf(var1), Integer.valueOf(var3)});
      }

      if(super.mc.theWorld != null && !super.mc.theWorld.getChunkFromBlockCoords(var1, var3).isEmpty()) {
         var4 = var4 + String.format(", biome: %s", new Object[]{super.mc.theWorld.getBiomeGenForCoords(var1, var3).biomeName});
      }

      IMwDataProvider var5 = MwAPI.b();
      if(var5 != null) {
         var4 = var4 + var5.a(this.c.g(), var1, var2, var3);
      }

      drawRect(10, super.height - 21, super.width - 20, super.height - 6, Integer.MIN_VALUE);
      this.drawCenteredString(super.fontRenderer, var4, super.width / 2, super.height - 18, 16777215);
   }

   public void j() {
      drawRect(10, 20, super.width - 20, super.height - 30, Integer.MIN_VALUE);
      super.fontRenderer.drawSplitString("Keys:\n\n  Space\n  Delete\n  C\n  Home\n  End\n  N\n  T\n  P\n  R\nLeft click drag or arrow keys pan the map.\nMouse wheel or Page Up/Down zooms map.\nRight click map to create a new marker.\nLeft click drag a selected marker to move it.\nMouse wheel over selected marker to cycle colour.\nMouse wheel over dimension or group box to cycle.\n", 15, 24, super.width - 30, 16777215);
      super.fontRenderer.drawSplitString("| Next marker group\n| Delete selected marker\n| Cycle selected marker colour\n| Centre map on player\n| Centre map on selected marker\n| Select next marker\n| Teleport to cursor or selected marker\n| Save PNG of visible map area\n| Regenerate visible map area from region files\n", 75, 42, super.width - 90, 16777215);
   }

   public void a(int var1, int var2, String var3, int var4, int var5, int var6) {
      String var7 = String.format("(%d, %d, %d)", new Object[]{Integer.valueOf(var4), Integer.valueOf(var5), Integer.valueOf(var6)});
      int var8 = Math.max(super.fontRenderer.getStringWidth(var3), super.fontRenderer.getStringWidth(var7));
      var1 = Math.min(var1, super.width - (var8 + 16));
      var2 = Math.min(Math.max(10, var2), super.height - 14);
      drawRect(var1 + 8, var2 - 10, var1 + var8 + 16, var2 + 14, Integer.MIN_VALUE);
      this.drawString(super.fontRenderer, var3, var1 + 10, var2 - 8, 16777215);
      this.drawString(super.fontRenderer, var7, var1 + 10, var2 + 4, 13421772);
   }

   public void drawScreen(int var1, int var2, float var3) {
      this.drawDefaultBackground();
      double var4 = 0.0D;
      double var6 = 0.0D;
      if(this.r > 2) {
         var4 = (double)(this.s - var1) * this.c.c() / (double)this.b.f;
         var6 = (double)(this.t - var2) * this.c.d() / (double)this.b.g;
         if(this.w != null) {
            double var8 = this.c.e(this.w.f);
            this.w.c = this.x - (int)(var4 / var8);
            this.w.e = this.y - (int)(var6 / var8);
         } else {
            this.c.a(this.u + var4, this.v + var6);
         }
      }

      if(this.r > 0) {
         ++this.r;
      }

      this.d.a();
      Point var13 = this.b.a(this.c, var1, var2);
      this.z = var13.x;
      this.B = var13.y;
      this.A = this.b(this.z, this.B);
      Marker var9 = this.a(var1, var2);
      if(var9 != null) {
         this.a(var1, var2, var9.a, var9.c, var9.d, var9.e);
      }

      if(this.c(var1, var2)) {
         this.a(var1, var2, super.mc.thePlayer.getEntityName(), this.a.G, this.a.H, this.a.I);
      }

      this.d(this.z, this.A, this.B);
      this.D.a(5, 5, "[help]");
      this.E.a(this.D, "[options]");
      String var10 = String.format("[dimension: %d]", new Object[]{Integer.valueOf(this.c.g())});
      this.F.a(this.E, var10);
      String var11 = String.format("[group: %s]", new Object[]{this.a.V.a()});
      this.G.a(this.F, var11);
      String var12 = String.format("[overlay : %s]", new Object[]{MwAPI.c()});
      this.H.a(this.G, var12);
      if(this.D.a(var1, var2)) {
         this.j();
      }

      super.drawScreen(var1, var2, var3);
   }

   static FontRenderer a(GuiMap var0) {
      return var0.fontRenderer;
   }

   static FontRenderer b(GuiMap var0) {
      return var0.fontRenderer;
   }
   
   class a {
	   int a;
	   int b;
	   int c;
	   int d;
	   final GuiMap e;

	   public a(GuiMap var1) {
	      this.e = var1;
	      this.a = 0;
	      this.b = 0;
	      this.c = 1;
	      this.d = 12;
	   }

	   public void a(int var1, int var2, String var3) {
	      this.a = var1;
	      this.b = var2;
	      this.c = GuiMap.a(this.e).getStringWidth(var3) + 4;
	      GuiMap.drawRect(this.a, this.b, this.a + this.c, this.b + this.d, Integer.MIN_VALUE);
	      this.e.drawString(GuiMap.b(this.e), var3, this.a + 2, this.b + 2, 16777215);
	   }

	   public void a(a var1, String var2) {
	      this.a(var1.a + var1.c + 5, var1.b, var2);
	   }

	   public boolean a(int var1, int var2) {
	      return var1 >= this.a && var2 >= this.b && var1 <= this.a + this.c && var2 <= this.b + this.d;
	   }
	}

}
