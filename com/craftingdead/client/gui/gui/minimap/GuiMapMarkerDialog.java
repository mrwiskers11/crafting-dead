package com.craftingdead.client.gui.gui.minimap;

import com.craftingdead.client.gui.gui.minimap.GuiMapTextDialog;
import com.craftingdead.client.mapwriter.map.Marker;
import com.craftingdead.client.mapwriter.map.MarkerManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiScreen;

@SideOnly(Side.CLIENT)
public class GuiMapMarkerDialog extends GuiMapTextDialog {

   private final MarkerManager a;
   private Marker b;
   private String c = "name";
   private String d = "group";
   private int z = 0;
   private int A = 80;
   private int B = 0;
   private int C = 0;
   private int D = 0;


   public GuiMapMarkerDialog(GuiScreen var1, MarkerManager var2, String var3, String var4, int var5, int var6, int var7, int var8) {
      super(var1, "Marker Name:", var3, "marker must have a name");
      this.a = var2;
      this.c = var3;
      this.d = var4;
      this.z = var5;
      this.A = var6;
      this.B = var7;
      this.b = null;
      this.D = var8;
   }

   public GuiMapMarkerDialog(GuiScreen var1, MarkerManager var2, Marker var3) {
      super(var1, "Edit Marker Name:", var3.a, "marker must have a name");
      this.a = var2;
      this.b = var3;
      this.c = var3.a;
      this.d = var3.b;
      this.z = var3.c;
      this.A = var3.d;
      this.B = var3.e;
      this.D = var3.f;
   }

   public boolean a() {
      boolean var1 = false;
      switch(this.C) {
      case 0:
         this.c = this.g();
         if(super.s) {
            super.e = "Marker Group:";
            this.a(this.d);
            super.q = "marker must have a group name";
            ++this.C;
         }
         break;
      case 1:
         this.d = this.g();
         if(super.s) {
            super.e = "Marker X:";
            this.a("" + this.z);
            super.q = "invalid value";
            ++this.C;
         }
         break;
      case 2:
         this.z = this.h();
         if(super.s) {
            super.e = "Marker Y:";
            this.a("" + this.A);
            super.q = "invalid value";
            ++this.C;
         }
         break;
      case 3:
         this.A = this.h();
         if(super.s) {
            super.e = "Marker Z:";
            this.a("" + this.B);
            super.q = "invalid value";
            ++this.C;
         }
         break;
      case 4:
         this.B = this.h();
         if(super.s) {
            var1 = true;
            int var2 = Marker.b();
            if(this.b != null) {
               var2 = this.b.g;
               this.a.c(this.b);
               this.b = null;
            }

            this.a.a(this.c, this.d, this.z, this.A, this.B, this.D, var2);
            this.a.a(this.d);
            this.a.c();
         }
      }

      return var1;
   }
}
