package com.craftingdead.client.gui.gui.minimap;

import com.craftingdead.client.gui.gui.minimap.GuiMapTextDialog;
import com.craftingdead.client.mapwriter.MapManager;
import com.craftingdead.client.mapwriter.map.MapView;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiScreen;

@SideOnly(Side.CLIENT)
public class GuiMapTeleportDialog extends GuiMapTextDialog {

   final MapManager a;
   final MapView b;
   final int c;
   final int d;


   public GuiMapTeleportDialog(GuiScreen var1, MapManager var2, MapView var3, int var4, int var5, int var6) {
      super(var1, "Teleport Height:", "" + var5, "invalid height");
      this.a = var2;
      this.b = var3;
      this.c = var4;
      this.d = var6;
      super.u = true;
   }

   public boolean a() {
      boolean var1 = false;
      int var2 = this.h();
      if(super.s) {
         var2 = Math.min(Math.max(0, var2), 255);
         this.a.k = var2;
         this.a.a(this.b, this.c, var2, this.d);
         var1 = true;
      }

      return var1;
   }
}
