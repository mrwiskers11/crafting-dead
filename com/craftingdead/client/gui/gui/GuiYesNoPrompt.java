package com.craftingdead.client.gui.gui;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.gui.IGuiPromptYesNo;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.render.CDRenderHelper;
import java.awt.Rectangle;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class GuiYesNoPrompt extends GuiCDScreen {

   protected GuiScreen c;
   private boolean r = false;
   private boolean s = false;
   public String[] d;
   private int t;
   private int u;
   public int e = 200;
   public int p = 70;
   public int q = 0;
   private String v = "No";
   private String w = "Yes";


   public GuiYesNoPrompt(int var1, GuiScreen var2) {
      this.q = var1;
      this.c = var2;
   }

   public GuiYesNoPrompt a(String ... var1) {
      this.d = var1;
      return this;
   }

   public GuiYesNoPrompt i() {
      this.v = "Disable";
      this.w = "Enable";
      return this;
   }

   public void initGui() {
      this.t = super.width / 2 - this.e / 2;
      this.u = super.height / 2 - this.p / 2 - 40;
      super.buttonList.add(new GuiCDButton(10, this.t + 2, this.u + this.p - 22, 80, 20, this.v));
      super.buttonList.add(new GuiCDButton(11, this.t + this.e - 80 - 2, this.u + this.p - 22, 80, 20, this.w));
      if(this.s) {
         super.mc.displayGuiScreen(this.c);
      }

      this.s = true;
   }

   public void actionPerformed(GuiButton var1) {
      if(var1.id == 11 && this.c instanceof IGuiPromptYesNo) {
         ((IGuiPromptYesNo)this.c).a(this.q, true);
      }

      if(var1.id == 10 && this.c instanceof IGuiPromptYesNo) {
         ((IGuiPromptYesNo)this.c).a(this.q, false);
      }

      if(super.mc.currentScreen == this) {
         super.mc.displayGuiScreen(this.c);
      }

   }

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
      Rectangle var4 = new Rectangle(this.t, this.u, this.e, this.p);
      if(!var4.contains(var1, var2)) {
         if(this.r) {
            this.r = false;
         } else {
            if(super.mc.currentScreen == this) {
               super.mc.displayGuiScreen(this.c);
            }

         }
      }
   }

   public void drawScreen(int var1, int var2, float var3) {
      CDRenderHelper.a((double)this.t, (double)this.u, (double)this.e, (double)this.p, 5066061, 1.0F);

      int var4;
      for(var4 = 0; var4 < super.buttonList.size(); ++var4) {
         GuiButton var5 = (GuiButton)super.buttonList.get(var4);
         var5.drawButton(super.mc, var1, var2);
      }

      if(this.d != null) {
         for(var4 = 0; var4 < this.d.length; ++var4) {
            String var6 = this.d[var4];
            CDRenderHelper.b(var6, this.t + this.e / 2, this.u + 5 + var4 * 11);
         }
      }

   }
}
