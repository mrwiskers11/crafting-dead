package com.craftingdead.client.gui.gui;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.ClientProxy;
import com.craftingdead.client.gui.gui.GuiHome;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownCommunity;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownPlay;
import com.craftingdead.client.gui.gui.dropdown.GuiDropDownSettings;
import com.craftingdead.client.gui.gui.faction.GuiFactions;
import com.craftingdead.client.gui.gui.faction.dropdown.GuiDropDownFactions;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.player.PlayerDataHandler;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class GuiCDScreen extends GuiScreen {

   private static final Random c = new Random();
   private static final ResourceLocation d = new ResourceLocation("craftingdead", "textures/gui/background_" + c.nextInt(10) + ".png");
   private static final ResourceLocation e = new ResourceLocation("craftingdead", "textures/gui/home.png");
   private static final ResourceLocation p = new ResourceLocation("craftingdead", "textures/gui/settings.png");
   private static final ResourceLocation q = new ResourceLocation("craftingdead", "textures/gui/power.png");
   private static final ResourceLocation r = new ResourceLocation("craftingdead", "textures/gui/smoke.png");
   private static final ResourceLocation s = new ResourceLocation("craftingdead", "textures/gui/smoke.png");
   private static double t = 0.0D;
   private static double u;
   public ClientProxy a;
   private CDPlayer v;
   public float b = 0.0F;
   private ArrayList w = new ArrayList();


   public void a(GuiWidget var1) {
      var1.a();
      this.w.add(var1);
   }

   public GuiWidget a(int var1) {
      Iterator var2 = this.w.iterator();

      GuiWidget var3;
      do {
         if(!var2.hasNext()) {
            return null;
         }

         var3 = (GuiWidget)var2.next();
      } while(var3.a != var1);

      return var3;
   }

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
      Iterator var4 = this.w.iterator();

      while(var4.hasNext()) {
         GuiWidget var5 = (GuiWidget)var4.next();
         var5.a(var1, var2, var3);
      }

   }

   public void drawScreen(int var1, int var2, float var3) {
      this.drawDefaultBackground();
      super.drawScreen(var1, var2, var3);
      Iterator var4 = this.w.iterator();

      while(var4.hasNext()) {
         GuiWidget var5 = (GuiWidget)var4.next();
         var5.a(var1, var2, var3);
      }

   }

   public void initGui() {
      super.initGui();
      this.w.clear();
      this.v = PlayerDataHandler.a(super.mc.getSession().getUsername());
   }

   public void actionPerformed(GuiButton var1) {
      super.actionPerformed(var1);
      Iterator var2 = this.w.iterator();

      while(var2.hasNext()) {
         GuiWidget var3 = (GuiWidget)var2.next();
         var3.a(var1);
      }

      int var4 = super.width / 2 - 173;
      if(var1.id == 1) {
         super.mc.displayGuiScreen(new GuiHome());
      }

      if(var1.id == 2) {
         GuiDropDownPlay var5 = new GuiDropDownPlay(this, var4 + 32, 23, 85, 20);
         super.mc.displayGuiScreen(var5);
      }

      if(var1.id == 3) {
         if(FactionManager.c().b()) {
            super.mc.displayGuiScreen(new GuiFactions(this));
         } else {
            super.mc.displayGuiScreen(new GuiDropDownFactions(this, var4 + 32 + 86, 23, 85, 20, this.v.h));
         }
      }

      if(var1.id == 4) {
         super.mc.displayGuiScreen(new GuiDropDownCommunity(this, var4 + 32 + 172, 23, 85, 20));
      }

      if(var1.id == 5) {
         GuiDropDownSettings var6 = new GuiDropDownSettings(this, var4 + 35 + 255, 23, 85, 20);
         super.mc.displayGuiScreen(var6);
      }

      if(var1.id == 6) {
         super.mc.shutdown();
      }

   }

   public void updateScreen() {
      super.updateScreen();
      Iterator var1 = this.w.iterator();

      while(var1.hasNext()) {
         GuiWidget var2 = (GuiWidget)var1.next();
         var2.b();
      }

      this.b += 0.05F;
      if(this.b >= 360.0F) {
         this.b = 0.0F;
      }

      if(t > (double)(-super.width)) {
         u = t;
         t -= 0.2D;
      } else {
         u = 0.0D;
         t = 0.0D;
      }

   }

   public void drawBackground(int var1) {
      GL11.glPushMatrix();
      GL11.glTranslated(Math.sin((double)this.b) * 2.0D, Math.cos((double)this.b) * 2.0D, 0.0D);
      GL11.glTranslatef((float)((super.width - this.g()) / 8 - 80) / 4.0F, (float)((super.height - this.h()) / 12 - 10) / 10.0F, 0.0F);
      double var2 = 0.0D;
      GL11.glScaled(1.3D + Math.cos((double)this.b) / 100.0D, 1.3D + Math.sin((double)this.b) / 100.0D, var2);
      CDRenderHelper.a(0.0D, 0.0D, d, (double)super.width, (double)super.height, 1.0F);
      GL11.glPopMatrix();
      double var4 = 0.5D;
      double var6 = u + (t - u) * var4;
      CDRenderHelper.a(var6, 0.0D, r, (double)super.width, (double)super.height, 0.3F);
      CDRenderHelper.a(var6 + (double)super.width, 0.0D, s, (double)super.width, (double)super.height, 0.3F);
      CDRenderHelper.a(0.0D, 0.0D, (double)super.width, 22.0D, 3355443, 1.0F);
      String var8 = EnumChatFormatting.GRAY + "Crafting Dead Origins " + "1.2.5";
      CDRenderHelper.b(var8, super.width / 12 * 6, super.height - 10);
   }

   public void a() {
      int var1 = super.width / 2 - 173;
      byte var2 = 1;
      byte var3 = 85;
      super.buttonList.add(new GuiCDButton(1, var1 + 1, var2, 30, 20, e));
      super.buttonList.add((new GuiCDButton(2, var1 + 32, var2, var3, 20, EnumChatFormatting.BOLD + "PLAY")).b(true));
      super.buttonList.add((new GuiCDButton(3, var1 + 33 + var3, var2, var3, 20, EnumChatFormatting.BOLD + "FACTION")).b(true));
      super.buttonList.add((new GuiCDButton(4, var1 + 34 + 2 * var3, var2, var3, 20, EnumChatFormatting.BOLD + "COMMUNITY")).b(true));
      super.buttonList.add(new GuiCDButton(5, var1 + 35 + 3 * var3, var2, 30, 20, p));
      super.buttonList.add(new GuiCDButton(6, var1 + 66 + 3 * var3, var2, 30, 20, q));
   }

   public static void a(URI var0) {
      if(var0 != null) {
         try {
            if(Desktop.isDesktopSupported()) {
               Desktop.getDesktop().browse(var0);
            }
         } catch (IOException var2) {
            CraftingDead.d.log(Level.WARNING, "Failed to open " + var0.getPath(), var2);
         }
      }

   }

   public int g() {
      return Mouse.getX() * super.width / super.mc.displayWidth;
   }

   public int h() {
      return super.height - Mouse.getY() * super.height / super.mc.displayHeight - 1;
   }

}
