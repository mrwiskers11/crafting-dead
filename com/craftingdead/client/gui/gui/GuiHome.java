package com.craftingdead.client.gui.gui;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidgetBanner;
import com.craftingdead.client.gui.widget.GuiWidgetNews;
import com.craftingdead.client.gui.widget.GuiWidgetPlayer;
import com.craftingdead.client.gui.widget.GuiWidgetPlayerInfo;
import com.craftingdead.client.gui.widget.GuiWidgetText;

import net.minecraft.util.EnumChatFormatting;

public class GuiHome extends GuiCDScreen {

   public void initGui() {
      super.initGui();
      super.a();
      int var1 = super.width / 2 - 173;
      this.a(new GuiWidgetPlayerInfo(1, var1 + 1, 25, 100, 60, this));
      this.a(new GuiWidgetBanner(2, var1 + 104, 25, 247, 40, this));
      this.a((new GuiWidgetText(5, var1 + 104, 68, 247, 14, this)).a(new String[]{EnumChatFormatting.RED.toString() + EnumChatFormatting.BOLD + "News"}).a(13355979));
      this.a(new GuiWidgetNews(4, var1 + 104, 83, 247, 142, this));
      this.a(new GuiWidgetPlayer(4, var1 + 1, 83, 100, 142, this));
   }
}
