package com.craftingdead.client.gui.gui;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.render.CDRenderHelper;
import java.awt.Rectangle;
import java.util.Iterator;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.input.Keyboard;

public abstract class GuiTextPrompt extends GuiCDScreen {

   protected GuiScreen c;
   private boolean x = false;
   private boolean y = false;
   public GuiTextField d;
   public String[] e;
   public int p = 32;
   protected int q;
   protected int r;
   public int s = 200;
   public int t = 20;
   public String u = "";
   public boolean v = false;
   protected String w = "";
   private int z = 0;


   public GuiTextPrompt(GuiScreen var1) {
      this.c = var1;
   }

   public GuiTextPrompt a(String ... var1) {
      this.e = var1;
      return this;
   }

   public GuiTextPrompt i() {
      this.v = true;
      return this;
   }

   public void initGui() {
      this.q = super.width / 2 - this.s / 2;
      this.r = super.height / 2 - this.t / 2;
      this.d = new GuiTextField(super.fontRenderer, this.q, this.r, this.s, this.t);
      this.d.setMaxStringLength(this.p);
      this.d.setText(this.u);
      super.buttonList.add(new GuiCDButton(10, this.q, this.r + 25, 80, 20, "Close"));
      super.buttonList.add(new GuiCDButton(11, this.q + this.s - 80, this.r + 25, 80, 20, "Confirm"));
      if(this.y) {
         super.mc.displayGuiScreen(this.c);
      }

      this.y = true;
   }

   public void actionPerformed(GuiButton var1) {
      if(var1.id == 11) {
         this.x = true;
         String var2 = this.d.getText();
         if(var2.isEmpty()) {
            this.w = "Empty Field";
            return;
         }

         if(this.v) {
            if(var2.contains(" ")) {
               this.w = "Spaces are Invalid!";
               return;
            }

            for(int var3 = 0; var3 < var2.length(); ++var3) {
               char var4 = var2.charAt(var3);
               if(!Character.isAlphabetic(var4) && !Character.isDigit(var4) && var4 != 95) {
                  this.w = "Invalid Character Found";
                  return;
               }
            }
         }

         this.j();
      }

      if(var1.id == 10 && super.mc.currentScreen == this) {
         super.mc.displayGuiScreen(this.c);
      }

   }

   public void updateScreen() {
      super.updateScreen();
      this.d.updateCursorCounter();
      if(Keyboard.isKeyDown(14)) {
         if(this.z >= 15) {
            this.d.deleteFromCursor(-1);
         } else {
            ++this.z;
         }
      } else {
         this.z = 0;
      }

   }

   protected void keyTyped(char var1, int var2) {
      super.keyTyped(var1, var2);
      this.d.textboxKeyTyped(var1, var2);
   }

   public abstract void j();

   public String k() {
      return this.d.getText();
   }

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
      this.d.mouseClicked(var1, var2, var3);
      Rectangle var4 = new Rectangle(this.q, this.r, this.s, this.t);
      if(!var4.contains(var1, var2)) {
         if(this.x) {
            this.x = false;
         } else {
            if(super.mc.currentScreen == this) {
               super.mc.displayGuiScreen(this.c);
            }

         }
      }
   }

   public void drawScreen(int var1, int var2, float var3) {
      byte var4 = 60;
      CDRenderHelper.a((double)(this.q - 4), (double)(this.r - var4), (double)(this.s + 8), (double)(this.t + var4 + 30), 3355443, 0.5F);
      Iterator var5 = super.buttonList.iterator();

      while(var5.hasNext()) {
         Object var6 = var5.next();
         ((GuiButton)var6).drawButton(super.mc, var1, var2);
      }

      if(this.e != null) {
         for(int var7 = 0; var7 < this.e.length; ++var7) {
            String var8 = this.e[var7];
            CDRenderHelper.b(var8, this.q + this.s / 2, this.r - var4 + 5 + var7 * 11);
         }
      }

      CDRenderHelper.b(EnumChatFormatting.RED + this.w, this.q + this.s / 2, this.r - var4 + 48);
      this.d.drawTextBox();
   }
}
