package com.craftingdead.client.gui;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.BlockShelfLoot;
import com.craftingdead.block.BlockVendingMachine;
import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.client.gui.gui.GuiSettings;
import com.craftingdead.inventory.ContainerBackpack;
import com.craftingdead.inventory.ContainerForge;
import com.craftingdead.inventory.ContainerFuelTanks;
import com.craftingdead.inventory.ContainerInventoryCDA;
import com.craftingdead.inventory.ContainerInventoryCDACrafting;
import com.craftingdead.inventory.ContainerShelf;
import com.craftingdead.inventory.ContainerTacticalVest;
import com.craftingdead.inventory.ContainerVendingMachine;
import com.craftingdead.inventory.InventoryBackpack;
import com.craftingdead.inventory.InventoryFuelTanks;
import com.craftingdead.inventory.InventoryShelfLoot;
import com.craftingdead.inventory.InventoryTacticalVest;
import com.craftingdead.inventory.InventoryVendingMachine;
import com.craftingdead.inventory.gui.GuiBackpack;
import com.craftingdead.inventory.gui.GuiForge;
import com.craftingdead.inventory.gui.GuiFuelTanks;
import com.craftingdead.inventory.gui.GuiInventoryCDA;
import com.craftingdead.inventory.gui.GuiInventoryCDACrafting;
import com.craftingdead.inventory.gui.GuiShelf;
import com.craftingdead.inventory.gui.GuiTacticalVest;
import com.craftingdead.inventory.gui.GuiVendingMachine;
import com.craftingdead.item.EnumBackpackSize;
import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemFuelTankBackpack;
import com.craftingdead.item.ItemTacticalVest;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler {

   public Object getServerGuiElement(int var1, EntityPlayer var2, World var3, int var4, int var5, int var6) {
      PlayerData var7 = PlayerDataHandler.a(var2);
      TileEntity var8 = var3.getBlockTileEntity(var4, var5, var6);
      if(var1 == CraftingDeadGui.a.a()) {
         return new ContainerInventoryCDA(var2, var7.d());
      } else if(var1 == CraftingDeadGui.b.a()) {
         return new ContainerInventoryCDACrafting(var2.inventory, var2);
      } else {
         if(var1 == CraftingDeadGui.c.a()) {
            if(ItemBackpack.a(var2) != null) {
               var3.playSoundAtEntity(var2, "craftingdead:backpack", 1.0F, 1.0F);
               return new ContainerBackpack(var2.inventory, ItemBackpack.a(var2));
            }

            if(ItemFuelTankBackpack.a(var2) != null) {
               return new ContainerFuelTanks(var2.inventory, ItemFuelTankBackpack.a(var2));
            }
         } else if(var1 == CraftingDeadGui.d.a()) {
            if(ItemTacticalVest.a(var2) != null) {
               return new ContainerTacticalVest(var2.inventory, ItemTacticalVest.a(var2));
            }
         } else if(var1 == CraftingDeadGui.e.a()) {
            if(BlockVendingMachine.a(var2) != null) {
               return new ContainerVendingMachine(var2.inventory, BlockVendingMachine.a(var2));
            }
         } else if(var1 == CraftingDeadGui.f.a()) {
            if(BlockShelfLoot.a(var2) != null) {
               return new ContainerShelf(var2.inventory, BlockShelfLoot.a(var2));
            }
         } else if(var1 == CraftingDeadGui.g.a()) {
            if(var8 != null && var8 instanceof TileEntityForge) {
               return new ContainerForge(var2.inventory, (TileEntityForge)var8);
            }
         } else if(var1 == CraftingDeadGui.h.a()) {
            return new GuiSettings((GuiScreen)null);
         }

         return null;
      }
   }

   public Object getClientGuiElement(int var1, EntityPlayer var2, World var3, int var4, int var5, int var6) {
      PlayerData var7 = PlayerDataHandler.a(var2);
      TileEntity var8 = var3.getBlockTileEntity(var4, var5, var6);
      if(var1 == CraftingDeadGui.a.a()) {
         return new GuiInventoryCDA(var2);
      } else if(var1 == CraftingDeadGui.b.a()) {
         return new GuiInventoryCDACrafting(var2);
      } else {
         ItemStack var9;
         if(var1 == CraftingDeadGui.c.a()) {
            if(var7.d().a("backpack") != null) {
               var9 = new ItemStack(var7.d().a("backpack").getItem());
               if(var9.getItem() instanceof ItemBackpack) {
                  EnumBackpackSize var10 = ItemBackpack.b(var9);
                  if(var10 != null && var10.f > 0) {
                     InventoryBackpack var11 = new InventoryBackpack(var10, var9);
                     return new GuiBackpack(var2.inventory, var11);
                  }
               }

               if(var9.getItem() instanceof ItemFuelTankBackpack) {
                  InventoryFuelTanks var12 = new InventoryFuelTanks(var9);
                  return new GuiFuelTanks(var2.inventory, var12);
               }
            }
         } else if(var1 == CraftingDeadGui.d.a()) {
            if(var7.d().a("vest") != null) {
               var9 = new ItemStack(var7.d().a("vest").getItem());
               InventoryTacticalVest var13 = new InventoryTacticalVest(var9);
               return new GuiTacticalVest(var2.inventory, var13);
            }
         } else if(var1 == CraftingDeadGui.e.a()) {
            if(BlockManager.ab != null) {
               var9 = new ItemStack(BlockManager.ab);
               InventoryVendingMachine var14 = new InventoryVendingMachine(var9);
               return new GuiVendingMachine(var2.inventory, var14);
            }
         } else if(var1 == CraftingDeadGui.f.a()) {
            if(BlockManager.ae != null) {
               var9 = new ItemStack(BlockManager.ae);
               InventoryShelfLoot var15 = new InventoryShelfLoot(var9);
               return new GuiShelf(var2.inventory, var15);
            }
         } else if(var1 == CraftingDeadGui.g.a()) {
            if(var8 != null && var8 instanceof TileEntityForge) {
               return new GuiForge(var2.inventory, (TileEntityForge)var8);
            }
         } else if(var1 == CraftingDeadGui.h.a()) {
            return new GuiSettings((GuiScreen)null);
         }

         return null;
      }
   }
}
