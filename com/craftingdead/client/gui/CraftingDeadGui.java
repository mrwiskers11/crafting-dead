package com.craftingdead.client.gui;


public enum CraftingDeadGui {

   a("INVENTORY", 0, 45),
   b("INVENTORY_CRAFTING", 1, 46),
   c("BACKPACK", 2, 47),
   d("TACTICAL_VEST", 3, 48),
   e("VENDING_MACHINE", 4, 49),
   f("SHELF", 5, 50),
   g("FORGE", 6, 51),
   h("SETTINGS", 7, 52);
   private int i;
   // $FF: synthetic field
   private static final CraftingDeadGui[] j = new CraftingDeadGui[]{a, b, c, d, e, f, g, h};


   private CraftingDeadGui(String var1, int var2, int var3) {
      this.i = var3;
   }

   public int a() {
      return this.i;
   }

}
