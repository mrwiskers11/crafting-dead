package com.craftingdead.client.gui.modules;

import com.craftingdead.client.render.CDRenderHelper;
import java.awt.Rectangle;
import org.lwjgl.input.Mouse;

public class GuiScrollBar {

   private int a;
   private int b;
   private int c;
   private int d;
   private int e;
   private int f;
   private Rectangle g = new Rectangle();
   private boolean h = false;
   private int i = 0;
   private int j = 0;
   private int k = 0;
   private int l = 0;
   private int m = 0;
   private int n = 0;
   private int o = 0;
   private int p = 0;


   public GuiScrollBar(int var1, int var2, int var3, int var4, int var5) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.j = var2 + 2;
      this.k = var2 + var4 - 34;
      this.l = this.k - this.j;
      this.g = new Rectangle(var1 + 2, var2 + 2, var3 - 4, 32);
      this.p = var5;
   }

   public void a(int var1) {
      this.n = var1;
   }

   public void a() {
      this.g.y = this.b + 2;
   }

   public void b() {
      if(this.i > 0) {
         --this.i;
      }

      if(Mouse.isButtonDown(0)) {
         if(this.g.contains(this.e, this.f)) {
            this.h = true;
         }
      } else {
         this.h = false;
      }

      int var1;
      if(this.h) {
         var1 = this.f - 16;
         if(var1 < this.j) {
            var1 = this.j;
         }

         if(var1 > this.k) {
            var1 = this.k;
         }

         this.g = new Rectangle(this.g.x, var1, this.g.width, this.g.height);
      }

      var1 = this.g.y * 100 / this.l - (this.b - 14);
      var1 -= this.p;
      this.m = var1;
      if(this.n > 0) {
         if(var1 > 98) {
            var1 = 100;
         }

         this.o = Math.round((float)(this.n * var1 / 100));
      }

   }

   public void c() {
      boolean var1 = false;
      if(!Mouse.isButtonDown(0)) {
         if(!Mouse.isButtonDown(1)) {
            int var4;
            while(Mouse.next() && (var4 = Mouse.getEventDWheel()) != 0) {
               this.i = 5;
               if(var4 > 0) {
                  var4 = -1;
               } else if(var4 < 0) {
                  var4 = 1;
               }

               int var2 = this.g.y + var4 * 10;
               if(var2 < this.j) {
                  var2 = this.j;
               }

               if(var2 > this.k) {
                  var2 = this.k;
               }

               this.g = new Rectangle(this.g.x, var2, this.g.width, this.g.height);
               int var3 = this.g.y * 100 / this.l - (this.b - 14);
               var3 -= this.p;
               this.m = var3;
               if(this.n > 0) {
                  if(var3 > 98) {
                     var3 = 100;
                  }

                  this.o = Math.round((float)(this.n * var3 / 100));
               }
            }

         }
      }
   }

   public int d() {
      return this.m;
   }

   public int e() {
      return this.o;
   }

   public boolean f() {
      return this.h;
   }

   public boolean g() {
      return this.i > 0;
   }

   public void a(int var1, int var2, float var3) {
      this.e = var1;
      this.f = var2;
      this.c();
      CDRenderHelper.b((double)this.a, (double)this.b, (double)this.c, (double)this.d, 0, 1.0F);
      CDRenderHelper.b((double)this.g.x, (double)this.g.y, (double)this.g.width, (double)this.g.height, 16777215, 1.0F);
   }
}
