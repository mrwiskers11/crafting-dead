package com.craftingdead.client.gui.modules;

import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiCDButton extends GuiButton {

   public int o = 2;
   private ResourceLocation w = null;
   public ItemStack p = null;
   public String q = "";
   public boolean r = true;
   public boolean s = true;
   public int t = 9902618;
   public float u = 1.0F;
   public int v = 1;


   public GuiCDButton(int var1, int var2, int var3, int var4, int var5, String var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public GuiCDButton(int var1, int var2, int var3, ItemStack var4) {
      super(var1, var2, var3, 18, 18, "");
      this.p = var4;
   }

   public GuiCDButton(int var1, int var2, int var3, int var4, int var5, ResourceLocation var6) {
      super(var1, var2, var3, var4, var5, "");
      this.w = var6;
   }

   public GuiCDButton b(boolean var1) {
      this.v = 1;
      return this;
   }

   public GuiCDButton a(int var1) {
      this.t = var1;
      return this;
   }

   public void drawButton(Minecraft var1, int var2, int var3) {
      if(super.drawButton) {
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         super.field_82253_i = var2 >= super.xPosition && var3 >= super.yPosition && var2 < super.xPosition + super.width && var3 < super.yPosition + super.height;
         this.o = this.getHoverState(super.field_82253_i);
         if(this.r) {
            if(this.s) {
               CDRenderHelper.a((double)super.xPosition, (double)super.yPosition, (double)super.width, (double)super.height, this.t, this.u);
            } else {
               CDRenderHelper.b((double)super.xPosition, (double)super.yPosition, (double)super.width, (double)super.height, this.t, this.u);
            }
         }

         this.mouseDragged(var1, var2, var3);
         String var4 = super.displayString;
         if(this.w != null) {
            float var5 = 1.0F;
            if(this.o == 2) {
               var5 = 0.5F;
            }

            CDRenderHelper.a((double)(super.xPosition + super.width / 2 - 8), (double)(super.yPosition + (super.height - 8) - 10), this.w, 16.0D, 16.0D, var5);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            return;
         }

         if(this.p != null) {
            CDRenderHelper.a(this.p, super.xPosition, super.yPosition + 1);
            return;
         }

         if(!super.enabled) {
            var4 = EnumChatFormatting.GRAY + var4;
         }

         if(this.v == 1) {
            CDRenderHelper.b(var4, super.xPosition + super.width / 2, super.yPosition + (super.height - 8) / 2, this.o == 2?11211539:16777215);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            return;
         }

         if(this.v == 2) {
            CDRenderHelper.a(var4, super.xPosition + super.width, super.yPosition + (super.height - 8) / 2, true, this.o == 2?11211539:16777215);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            return;
         }

         CDRenderHelper.a(var4, super.xPosition + 4, super.yPosition + (super.height - 8) / 2, this.o == 2?11211539:16777215);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      }

   }
}
