package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.mojang.MojangAPI;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;

public class GuiWidgetPlayerInfo extends GuiWidget {

   public GuiWidgetPlayerInfo(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void a(int var1, int var2, float var3) {
      this.c();
      String var4 = Minecraft.getMinecraft().getSession().getUsername();
      CDPlayer var5 = PlayerDataHandler.a(var4);
      CDRenderHelper.a((double)(super.b + 4), (double)(super.c + 4), CDRenderHelper.a(MojangAPI.getUUID(var4, false)), 18.0D, 18.0D, 1.0F);
      CDRenderHelper.a(var4, super.b + 26, super.c + 4);
      CDRenderHelper.a(EnumChatFormatting.AQUA + "Player Kills: " + EnumChatFormatting.BOLD + var5.b, super.b + 4, super.c + 27, 0.8D);
      CDRenderHelper.a(EnumChatFormatting.GREEN + "Zombie Kills: " + EnumChatFormatting.BOLD + var5.c, super.b + 4, super.c + 37, 0.8D);
      CDRenderHelper.a(EnumChatFormatting.RED + "Deaths: " + EnumChatFormatting.BOLD + var5.d, super.b + 4, super.c + 47, 0.8D);
   }
}
