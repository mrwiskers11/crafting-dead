package com.craftingdead.client.gui.widget.server;

import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.EnumChatFormatting;

public class GuiWidgetServersSlot {
    public CDServerData a;
    private String d = EnumChatFormatting.AQUA + "Pinging...";
    private boolean e = true;
    private int f = 0;
    public int b;
    public int c;

    public GuiWidgetServersSlot(CDServerData cDServerData) {
        this.a = cDServerData;
    }

    public void a() {
        if (this.a.i != 0) {
            this.e = false;
        }
        if (this.e) {
            int n = 5;
            ++this.f;
            if (this.f < n) {
                this.d = EnumChatFormatting.AQUA + "Pinging";
            } else if (this.f < n * 2) {
                this.d = EnumChatFormatting.AQUA + "Pinging .";
            } else if (this.f < n * 3) {
                this.d = EnumChatFormatting.AQUA + "Pinging . .";
            } else if (this.f < n * 4) {
                this.d = EnumChatFormatting.AQUA + "Pinging . . .";
            } else if (this.f < n * 5) {
                this.d = EnumChatFormatting.AQUA + "Pinging . . . .";
            } else {
                this.f = 0;
            }
        }
    }

    public boolean b() {
        return true;
    }

    public void a(int n, int n2, boolean bl, int n3, int n4) {
        CDRenderHelper.a((double)n, (double)n2, (double)n3, (double)(n4 - 3), (int)0, (float)(bl ? 1.0f : 0.7f));
        if (bl) {
            CDRenderHelper.a((double)(n + 1), (double)(n2 + 1), (double)(n3 - 2), (double)(n4 - 5), (int)16718362, (float)0.3f);
        }
        if (this.e) {
            CDRenderHelper.a((String)this.d, (int)(n + 2), (int)(n2 + 3));
        } else {
            if (this.a.i == 1) {
                CDRenderHelper.a((String)(EnumChatFormatting.RED + "OFFLINE"), (int)(n + 2), (int)(n2 + 3));
                return;
            }
            CDRenderHelper.a((String)this.a.j, (int)(n + 3), (int)(n2 + 5));
            String string = this.a.m + EnumChatFormatting.RESET + " | " + EnumChatFormatting.GRAY + this.a.l;
            CDRenderHelper.a((String)string, (int)(n + n3 - Minecraft.getMinecraft().fontRenderer.getStringWidth(string) - 2), (int)(n2 + 5));
        }
    }
}
