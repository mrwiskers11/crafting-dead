package com.craftingdead.client.gui.widget.server;

import java.io.IOException;

import com.craftingdead.client.gui.gui.server.GuiServerList;

public class ThreadServerPoll
extends Thread {
    public CDServerData a;

    public ThreadServerPoll(CDServerData cDServerData) {
        this.a = cDServerData;
    }

    public void run() {
        this.a.i = 0;
        try {
            long l = System.nanoTime();
            GuiServerList.a((CDServerData)this.a);
            long l2 = System.nanoTime();
            this.a.a((long)((double)((l2 - l) / 1000000L) * 0.5));
        }
        catch (IOException iOException) {
            this.a.i = 1;
        }
    }
}
