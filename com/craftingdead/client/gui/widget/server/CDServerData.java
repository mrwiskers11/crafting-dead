package com.craftingdead.client.gui.widget.server;

import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.util.EnumChatFormatting;

public class CDServerData
extends ServerData {
    public int i = 0;
    public String j = "???";
    public String k = "???";
    public String l = "?/?";
    public String m = EnumChatFormatting.RED + "null";

    public CDServerData(String string, String string2) {
        super(string, string2);
    }

    public void a(long l) {
        this.m = l < 200L ? EnumChatFormatting.GREEN + "" + l + "ms" : (l < 400L ? EnumChatFormatting.YELLOW + "" + l + "ms" : (l < 1200L ? EnumChatFormatting.RED + "" + l + "ms" : EnumChatFormatting.DARK_RED + "" + l + "ms"));
    }
}
