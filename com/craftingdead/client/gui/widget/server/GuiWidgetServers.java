package com.craftingdead.client.gui.widget.server;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;

public class GuiWidgetServers
extends GuiWidget {
    public ArrayList<GuiWidgetServersSlot> i = new ArrayList();
    private int j;
    private int k;
    private Rectangle l;
    private int m = 0;
    private int n = 0;
    private boolean o = false;
    private int p = 21;
    private int q = -1;

    public GuiWidgetServers(int n, int n2, int n3, int n4, int n5, GuiCDScreen guiCDScreen, List<String> list) {
        super(n, n2, n3, n4, n5, guiCDScreen);
        this.a(list);
    }

    public void a() {
        this.l = new Rectangle(this.b + this.d - 21, this.c + 1, 20, 40);
        this.m = this.c + 2;
        this.n = this.c + this.e - 2 - this.l.height;
        for (GuiWidgetServersSlot guiWidgetServersSlot : this.i) {
            ThreadServerPoll threadServerPoll = new ThreadServerPoll(guiWidgetServersSlot.a);
            threadServerPoll.start();
        }
    }

    public void b() {
        int n;
        this.f();
        for (n = 0; n < this.i.size(); ++n) {
            GuiWidgetServersSlot guiWidgetServersSlot = (GuiWidgetServersSlot)this.i.get(n);
            guiWidgetServersSlot.a();
        }
        if (this.o) {
            n = this.k - this.l.height / 2;
            if (n > this.n) {
                n = this.n;
            }
            if (n < this.m) {
                n = this.m;
            }
            this.l = new Rectangle(this.l.x, n, this.l.width, this.l.height);
        }
    }

    public void a(int n, int n2, int n3) {
        for (int i = 0; i < this.i.size(); ++i) {
            GuiWidgetServersSlot guiWidgetServersSlot = (GuiWidgetServersSlot)this.i.get(i);
            Rectangle rectangle = new Rectangle(guiWidgetServersSlot.b, guiWidgetServersSlot.c, this.d - 20, this.p);
            if (!rectangle.contains(n, n2) || !guiWidgetServersSlot.b()) continue;
            this.q = i;
        }
    }

    public void d() {
    }

    public void e() {
    }

    public void f() {
        if (Mouse.isButtonDown((int)0)) {
            if (this.l.contains(this.j, this.k)) {
                this.o = true;
            }
        } else {
            this.o = false;
        }
    }

    public int g() {
        int n = this.n - this.m;
        int n2 = this.l.y - this.l.height / 2 - (this.m - this.l.height / 2);
        float f = n2 * 100 / n;
        return (int)f;
    }

    public void a(int n, int n2, float f) {
        this.c();
        this.j = n;
        this.k = n2;
        this.h();
        int n3 = this.i.size() * this.p;
        int n4 = (n3 - this.e) * this.g() / 100;
        if (n3 > this.e) {
            CDRenderHelper.b((double)(this.b + this.d - 18), (double)(this.c + 4), (double)(this.l.width - 6), (double)(this.e - 7), (int)0, (float)1.0f);
            CDRenderHelper.a((double)this.l.x, (double)this.l.y, (ResourceLocation)new ResourceLocation("craftingdead", "textures/gui/scoller.png"), (double)this.l.width, (double)this.l.height, (float)1.0f);
        }
        for (int i = 0; i < this.i.size(); ++i) {
            boolean bl = this.q == i;
            int n5 = this.c + 2 + this.p * i - (n3 > this.e ? n4 : 0) + 15;
            int n6 = n5 - this.c - 2;
            if (n6 % this.p != 0) {
                n5 -= n6 % this.p;
            }
            if (n6 < 0) continue;
            ((GuiWidgetServersSlot)this.i.get((int)i)).b = this.b + 3;
            ((GuiWidgetServersSlot)this.i.get((int)i)).c = n5 - 3;
            if (n5 < this.c || n5 > this.c + this.e - 2 - (this.p - 6)) continue;
            ((GuiWidgetServersSlot)this.i.get(i)).a(this.b + 4, n5 + 3, bl, this.d - 30, this.p);
        }
    }

    public void h() {
        int n = 0;
        if (Mouse.isButtonDown((int)0)) {
            return;
        }
        while (Mouse.next() && (n = Mouse.getEventDWheel()) != 0) {
            if (n > 0) {
                n = -1;
            } else if (n < 0) {
                n = 1;
            }
            int n2 = this.l.y + n * 13;
            if (n2 > this.n) {
                n2 = this.n;
            }
            if (n2 < this.m) {
                n2 = this.m;
            }
            this.l = new Rectangle(this.l.x, n2, this.l.width, this.l.height);
        }
    }

    public GuiWidgetServersSlot i() {
        if (this.q == -1) {
            return null;
        }
        return (GuiWidgetServersSlot)this.i.get(this.q);
    }

    public void a(List<String> list) {
        this.i.clear();
        for (String string : list) {
            CDServerData cDServerData = new CDServerData("Crafting Dead", string);
            this.i.add(new GuiWidgetServersSlot(cDServerData));
        }
    }
}
