package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;

public class GuiWidgetText extends GuiWidget {

   private String[] i;
   private boolean j = false;


   public GuiWidgetText(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public GuiWidgetText a(String ... var1) {
      this.i = var1;
      return this;
   }

   public GuiWidgetText d() {
      this.j = true;
      return this;
   }

   public void a(int var1, int var2, float var3) {
      this.c();

      for(int var4 = 0; var4 < this.i.length; ++var4) {
         String var5 = this.i[var4];
         if(this.j) {
            CDRenderHelper.b(var5, super.b + super.d / 2, super.c + 3 + 10 * var4);
         } else {
            CDRenderHelper.a(var5, super.b + 3, super.c + 3 + 10 * var4);
         }
      }

   }
}
