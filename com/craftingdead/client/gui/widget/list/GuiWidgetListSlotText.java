package com.craftingdead.client.gui.widget.list;

import com.craftingdead.client.gui.widget.list.GuiWidgetListSlot;
import com.craftingdead.client.render.CDRenderHelper;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.util.EnumChatFormatting;

public class GuiWidgetListSlotText extends GuiWidgetListSlot {

   private String c;


   public GuiWidgetListSlotText(String var1) {
      this.c = var1;
   }

   public boolean c() {
      return this.c != null && !this.c.equals("");
   }

   public void b() {
      String[] var1 = this.c.split(" ");
      if(var1.length >= 1) {
         String[] var2 = var1;
         int var3 = var1.length;

         for(int var4 = 0; var4 < var3; ++var4) {
            String var5 = var2[var4];
            if(var5.contains(".") && (var5.contains(".com") || var5.contains(".net") || var5.contains("https://") || var5.startsWith("http://"))) {
               this.a(var5);
            }
         }
      }

   }

   public void a(int var1, int var2, boolean var3, int var4, int var5) {
      CDRenderHelper.a((var3?EnumChatFormatting.GRAY:EnumChatFormatting.WHITE) + this.c, var1 + 2, var2);
   }

   public static ArrayList a(ArrayList var0) {
      ArrayList var1 = new ArrayList();
      Iterator var2 = var0.iterator();

      while(var2.hasNext()) {
         String var3 = (String)var2.next();
         GuiWidgetListSlotText var4 = new GuiWidgetListSlotText(var3);
         var1.add(var4);
      }

      return var1;
   }
}
