package com.craftingdead.client.gui.widget.list;

import java.awt.Desktop;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

public class GuiWidgetListSlot {

   public int a;
   public int b;


   public void a() {}

   public void b() {}

   public boolean c() {
      return true;
   }

   public void a(int var1, int var2, boolean var3, int var4, int var5) {}

   protected void a(String var1) {
      try {
         if(Desktop.isDesktopSupported()) {
            try {
               Desktop.getDesktop().browse(new URI(var1));
            } catch (URISyntaxException var3) {
               var3.printStackTrace();
            }
         }
      } catch (MalformedURLException var4) {
         var4.printStackTrace();
      } catch (IOException var5) {
         var5.printStackTrace();
      }

   }
}
