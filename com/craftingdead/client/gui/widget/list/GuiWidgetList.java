package com.craftingdead.client.gui.widget.list;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.gui.widget.list.GuiWidgetListSlot;
import com.craftingdead.client.gui.widget.list.GuiWidgetListSlotText;
import com.craftingdead.client.render.CDRenderHelper;
import java.awt.Rectangle;
import java.util.ArrayList;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;

public class GuiWidgetList extends GuiWidget {

   private ArrayList i = new ArrayList();
   private int j;
   private int k;
   private Rectangle l;
   private int m = 0;
   private int n = 0;
   private boolean o = false;
   private int p = 11;
   private int q = -1;


   public GuiWidgetList(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public GuiWidgetList a(ArrayList var1) {
      this.c(var1);
      return this;
   }

   public GuiWidgetList b(ArrayList var1) {
      this.c(GuiWidgetListSlotText.a(var1));
      return this;
   }

   public GuiWidgetList b(int var1) {
      this.p = var1;
      return this;
   }

   public void a() {
      this.l = new Rectangle(super.b + super.d - 21, super.c + 1, 20, 40);
      this.m = super.c + 2;
      this.n = super.c + super.e - 2 - this.l.height;
   }

   public void b() {
      this.f();

      int var1;
      for(var1 = 0; var1 < this.i.size(); ++var1) {
         GuiWidgetListSlot var2 = (GuiWidgetListSlot)this.i.get(var1);
         var2.a();
      }

      if(this.o) {
         var1 = this.k - this.l.height / 2;
         if(var1 > this.n) {
            var1 = this.n;
         }

         if(var1 < this.m) {
            var1 = this.m;
         }

         this.l = new Rectangle(this.l.x, var1, this.l.width, this.l.height);
      }

   }

   public void a(int var1, int var2, int var3) {
      for(int var4 = 0; var4 < this.i.size(); ++var4) {
         GuiWidgetListSlot var5 = (GuiWidgetListSlot)this.i.get(var4);
         Rectangle var6 = new Rectangle(var5.a, var5.b, super.d - 20, this.p);
         if(var6.contains(var1, var2) && var5.c()) {
            if(this.q == var4) {
               var5.b();
            }

            this.q = var4;
         }
      }

   }

   public void d() {}

   public void e() {}

   public void f() {
      if(Mouse.isButtonDown(0)) {
         if(this.l.contains(this.j, this.k)) {
            this.o = true;
         }
      } else {
         this.o = false;
      }

   }

   public int g() {
      int var1 = this.n - this.m;
      int var2 = this.l.y - this.l.height / 2 - (this.m - this.l.height / 2);
      float var3 = (float)(var2 * 100 / var1);
      return (int)var3;
   }

   public void a(int var1, int var2, float var3) {
      this.c();
      this.j = var1;
      this.k = var2;
      this.h();
      int var4 = this.i.size() * this.p;
      int var5 = (var4 - super.e) * this.g() / 100;
      var5 = var5 < 0?0:var5;
      if(var4 > super.e) {
         CDRenderHelper.b((double)(super.b + super.d - 18), (double)(super.c + 4), (double)(this.l.width - 6), (double)(super.e - 7), 0, 1.0F);
         CDRenderHelper.a((double)this.l.x, (double)this.l.y, new ResourceLocation("craftingdead", "textures/gui/scoller.png"), (double)this.l.width, (double)this.l.height, 1.0F);
      }

      for(int var6 = 0; var6 < this.i.size(); ++var6) {
         boolean var7 = this.q == var6;
         int var8 = super.c + 2 + this.p * var6 - (var4 > super.e?var5:0);
         int var9 = var8 - super.c - 2;
         if(var9 % this.p != 0) {
            var8 -= var9 % this.p;
         }

         if(var9 >= 0) {
            ((GuiWidgetListSlot)this.i.get(var6)).a = super.b;
            ((GuiWidgetListSlot)this.i.get(var6)).b = var8;
            if(var8 >= super.c - 2 && var8 <= super.c + super.e - 2 - (this.p - 6)) {
               ((GuiWidgetListSlot)this.i.get(var6)).a(super.b + 4, var8, var7, super.d - 30, this.p);
            }
         }
      }

   }

   public void h() {
      boolean var1 = false;
      if(!Mouse.isButtonDown(0)) {
         Rectangle var2 = new Rectangle(super.b, super.c, super.d, super.e);
         if(var2.contains(this.j, this.k)) {
            int var3;
            int var4;
            for(; Mouse.next() && (var4 = Mouse.getEventDWheel()) != 0; this.l = new Rectangle(this.l.x, var3, this.l.width, this.l.height)) {
               if(var4 > 0) {
                  var4 = -1;
               } else if(var4 < 0) {
                  var4 = 1;
               }

               var3 = this.l.y + var4 * this.p;
               if(var3 > this.n) {
                  var3 = this.n;
               }

               if(var3 < this.m) {
                  var3 = this.m;
               }
            }

         }
      }
   }

   public GuiWidgetListSlot i() {
      return this.q == -1?null:(GuiWidgetListSlot)this.i.get(this.q);
   }

   public void c(ArrayList var1) {
      this.i.clear();
      this.i.addAll(var1);
   }
}
