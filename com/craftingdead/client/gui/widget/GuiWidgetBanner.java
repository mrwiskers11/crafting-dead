package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.util.ResourceLocation;

public class GuiWidgetBanner extends GuiWidget {

   private static final ResourceLocation i = new ResourceLocation("craftingdead", "textures/gui/banner.png");


   public GuiWidgetBanner(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void a(int var1, int var2, float var3) {
      this.c();
      CDRenderHelper.a((double)(super.b + 1), (double)(super.c + 1), i, (double)(super.d - 2), (double)(super.e - 2), 1.0F);
   }

}
