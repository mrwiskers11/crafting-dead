package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.list.GuiWidgetList;
import com.craftingdead.client.gui.widget.list.GuiWidgetListSlotText;
import com.craftingdead.cloud.packet.PacketClientRequest;
import com.craftingdead.cloud.packet.RequestType;
import com.f3rullo14.fds.StringListHelper;

import java.util.ArrayList;

public class GuiWidgetNews extends GuiWidgetList {

   private static int i = 0;
   private static ArrayList j = new ArrayList();


   public GuiWidgetNews(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
      PacketClientRequest.a(RequestType.c, new String[0]);
      this.c(GuiWidgetListSlotText.a(j));
   }

   public void b() {
      super.b();
      if(i++ > 20) {
         j.clear();
         j.addAll(StringListHelper.a(super.f.a.l(), 210));
         if(!super.f.a.c()) {
            j.clear();
            j.add(StringListHelper.a("&7&m---------------------------------------"));
            j.add(StringListHelper.a("&4Connection to the cloud was not established."));
            j.add(StringListHelper.a("&7&m---------------------------------------"));
            j.add("1. Check Internet Connection.");
            j.add("2. Make sure you have a \'legit\' MC Account.");
            j.add("3. Maybe the Cloud is down? Check Forums.");
            j.add("4. Did you edit any of the game files?");
            j.add(StringListHelper.a("&7&m---------------------------------------"));
            j.add(StringListHelper.a("&d      &n http://www.craftingdead.com/status &r      "));
            j.add(StringListHelper.a("&7&m---------------------------------------"));
         }

         this.c(GuiWidgetListSlotText.a(j));
         i = 0;
      }

   }

}
