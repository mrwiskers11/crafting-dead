package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.cloud.packet.PacketClientRequest;
import com.craftingdead.cloud.packet.RequestType;

import net.minecraft.util.EnumChatFormatting;

public class GuiWidgetMenuInfo extends GuiWidget {

   private int i = 0;


   public GuiWidgetMenuInfo(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public void b() {
      if(this.i++ >= 40) {
         PacketClientRequest.a(RequestType.d, new String[0]);
         this.i = 0;
      }

   }

   public void a(int var1, int var2, float var3) {
      CDRenderHelper.a(EnumChatFormatting.DARK_RED + "Online: " + EnumChatFormatting.RED + CMClient.a, super.b + 4, super.c + 4, 1.1D);
   }

   public static double a(double var0, int var2) {
      if(var2 < 0) {
         throw new IllegalArgumentException();
      } else {
         long var3 = (long)Math.pow(10.0D, (double)var2);
         var0 *= (double)var3;
         long var5 = Math.round(var0);
         return (double)var5 / (double)var3;
      }
   }
}
