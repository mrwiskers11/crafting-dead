package com.craftingdead.client.gui.widget.team;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.gui.widget.team.GuiWidgetTeamSlot;
import com.craftingdead.client.gui.widget.team.TeamMember;
import com.craftingdead.client.render.CDRenderHelper;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.input.Mouse;

public class GuiWidgetTeam extends GuiWidget {

   public ArrayList i = new ArrayList();
   private int j;
   private int k;
   private Rectangle l;
   private int m = 0;
   private int n = 0;
   private boolean o = false;
   private int p = 73;
   private int q = -1;


   public GuiWidgetTeam(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
   }

   public GuiWidgetTeam a(List var1) {
      this.i.clear();
      this.b(var1);
      return this;
   }

   public void a() {
      this.l = new Rectangle(super.b + super.d - 21, super.c + 1, 20, 40);
      this.m = super.c + 2;
      this.n = super.c + super.e - 2 - this.l.height;
   }

   public void b() {
      this.f();
      if(this.o) {
         int var1 = this.k - this.l.height / 2;
         if(var1 > this.n) {
            var1 = this.n;
         }

         if(var1 < this.m) {
            var1 = this.m;
         }

         this.l = new Rectangle(this.l.x, var1, this.l.width, this.l.height);
      }

   }

   public void a(int var1, int var2, int var3) {
      for(int var4 = 0; var4 < this.i.size(); ++var4) {
         GuiWidgetTeamSlot var5 = (GuiWidgetTeamSlot)this.i.get(var4);
         Rectangle var6 = new Rectangle(var5.b, var5.b, super.d - 20, this.p);
         if(var6.contains(var1, var2)) {
            this.q = var4;
         }
      }

   }

   public void d() {}

   public void e() {}

   public void f() {
      if(Mouse.isButtonDown(0)) {
         if(this.l.contains(this.j, this.k)) {
            this.o = true;
         }
      } else {
         this.o = false;
      }

   }

   public int g() {
      int var1 = this.n - this.m;
      int var2 = this.l.y - this.l.height / 2 - (this.m - this.l.height / 2);
      float var3 = (float)(var2 * 100 / var1);
      return (int)var3;
   }

   public void a(int var1, int var2, float var3) {
      this.j = var1;
      this.k = var2;
      this.h();
      int var4 = this.i.size() * this.p;
      int var5 = (var4 - super.e) * this.g() / 100;
      if(var4 > super.e) {
         CDRenderHelper.b((double)(super.b + super.d - 18), (double)(super.c + 4), (double)(this.l.width - 6), (double)(super.e - 7), 0, 1.0F);
         CDRenderHelper.a((double)this.l.x, (double)this.l.y, new ResourceLocation("craftingdead", "textures/gui/scoller.png"), (double)this.l.width, (double)this.l.height, 1.0F);
      }

      for(int var6 = 0; var6 < this.i.size(); ++var6) {
         int var7 = super.c + 2 + this.p * var6 - (var4 > super.e?var5:0) + 15;
         int var8 = var7 - super.c - 2;
         if(var8 % this.p != 0) {
            var7 -= var8 % this.p;
         }

         if(var8 >= 0) {
            ((GuiWidgetTeamSlot)this.i.get(var6)).b = super.b + 3;
            ((GuiWidgetTeamSlot)this.i.get(var6)).c = var7 - 3;
            if(var7 >= super.c && var7 <= super.c + super.e - 2 - (this.p - 6)) {
               ((GuiWidgetTeamSlot)this.i.get(var6)).a(super.b + 4, var7 + 3, super.d - 30, this.p);
            }
         }
      }

   }

   public void h() {
      if(!Mouse.isButtonDown(0)) {
         int var1;
         int var2;
         for(; Mouse.next() && (var1 = Mouse.getEventDWheel()) != 0; this.l = new Rectangle(this.l.x, var2, this.l.width, this.l.height)) {
            if(var1 > 0) {
               var1 = -1;
            } else if(var1 < 0) {
               var1 = 1;
            }

            var2 = this.l.y + var1 * 13;
            if(var2 > this.n) {
               var2 = this.n;
            }

            if(var2 < this.m) {
               var2 = this.m;
            }
         }

      }
   }

   public GuiWidgetTeamSlot i() {
      return this.q == -1?null:(GuiWidgetTeamSlot)this.i.get(this.q);
   }

   public void b(List var1) {
      this.i.clear();
      Iterator var2 = var1.iterator();

      while(var2.hasNext()) {
         TeamMember var3 = (TeamMember)var2.next();
         this.i.add(new GuiWidgetTeamSlot(var3));
      }

   }
}
