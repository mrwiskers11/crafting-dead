package com.craftingdead.client.gui.widget.team;

import com.craftingdead.client.gui.widget.team.TeamMember;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.mojang.MojangAPI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.minecraft.client.Minecraft;

public class GuiWidgetTeamSlot {

   public TeamMember a;
   public int b;
   public int c;


   public GuiWidgetTeamSlot(TeamMember var1) {
      this.a = var1;
   }

   public boolean a() {
      return true;
   }

   public void a(int var1, int var2, int var3, int var4) {
      CDRenderHelper.a((double)var1, (double)var2, (double)var3, (double)var4, 3355443, 0.9F);
      CDRenderHelper.a((double)(this.b + 4), (double)(this.c + 9), CDRenderHelper.a(MojangAPI.getUUID(this.a.a(), false)), 32.0D, 32.0D, 1.0F);
      CDRenderHelper.a(this.a.a(), this.b + 40, this.c + 15, 1.3D);
      CDRenderHelper.a(this.a.b().a(), this.b + 40, this.c + 28);
      int var5 = 0;

      for(Iterator var6 = a(this.a.c(), 60).iterator(); var6.hasNext(); var5 += Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT + 2) {
         String var7 = (String)var6.next();
         CDRenderHelper.a(var7, this.b + 5, this.c + 45 + var5, 1.0D);
      }

   }

   public static List a(String var0, int var1) {
      ArrayList var2 = new ArrayList();
      Pattern var3 = Pattern.compile("\\b.{1," + (var1 - 1) + "}\\b\\W?");
      Matcher var4 = var3.matcher(var0);

      while(var4.find()) {
         var2.add(var4.group());
      }

      return var2;
   }
}
