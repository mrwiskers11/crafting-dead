package com.craftingdead.client.gui.widget.team;

import net.minecraft.util.EnumChatFormatting;

public class TeamMember {
   private String a;
   private a b;
   private String c;

   public TeamMember(String var1, a var2, String var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public String a() {
      return this.a;
   }

   public a b() {
      return this.b;
   }

   public String c() {
      return this.c;
   }
   

public enum a {
   a("Leader", EnumChatFormatting.RED),
   b("Developer", EnumChatFormatting.DARK_BLUE),
   c("Staff", EnumChatFormatting.AQUA),
   d("Owner", EnumChatFormatting.GOLD),
   e("Community Manager", EnumChatFormatting.DARK_RED),
   f("Asset Developer", EnumChatFormatting.LIGHT_PURPLE),
   g("YouTuber", EnumChatFormatting.RED);

   private String h;
   private EnumChatFormatting i;

   private a(String var3, EnumChatFormatting var4) {
      this.h = var3;
      this.i = var4;
   }

   public String toString() {
      return this.h;
   }

   public String a() {
      return this.i + EnumChatFormatting.BOLD.toString() + this.toString();
   }
}

}
