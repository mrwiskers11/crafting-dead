package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.gui.widget.GuiWidget;
import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumChatFormatting;

public class GuiWidgetPlayer extends GuiWidget {

   private EntityPlayer i;
   private float j = 0.0F;


   public GuiWidgetPlayer(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      super(var1, var2, var3, var4, var5, var6);
      this.i = super.f.a.g().b();
   }

   public void b() {
      ++this.j;
   }

   public void a(int var1, int var2, float var3) {
      this.c();
      byte var4 = 0;
      byte var5 = 12;
      this.i.rotationPitch = (float)Math.cos((double)super.f.b) * 5.0F;
      this.i.rotationYaw = (float)Math.sin((double)super.f.b) * 10.0F;
      CDRenderHelper.b(EnumChatFormatting.BOLD + this.i.getDisplayName(), super.b + super.d / 2, super.c + 8);
      if(this.i != null) {
         CDRenderHelper.a(this.i, super.b + super.d / 2 + var4, super.c + super.e - 20 + var5, 60.0F, this.j);
      }

   }

   public static double a(double var0, int var2) {
      if(var2 < 0) {
         throw new IllegalArgumentException();
      } else {
         long var3 = (long)Math.pow(10.0D, (double)var2);
         var0 *= (double)var3;
         long var5 = Math.round(var0);
         return (double)var5 / (double)var3;
      }
   }
}
