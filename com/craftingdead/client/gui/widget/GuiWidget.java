package com.craftingdead.client.gui.widget;

import com.craftingdead.client.gui.gui.GuiCDScreen;
import com.craftingdead.client.render.CDRenderHelper;
import net.minecraft.client.gui.GuiButton;

public class GuiWidget {

   public int a;
   protected int b;
   protected int c;
   protected int d;
   protected int e;
   protected GuiCDScreen f;
   protected int g = 3355443;
   public float h = 1.0F;


   public GuiWidget(int var1, int var2, int var3, int var4, int var5, GuiCDScreen var6) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
      this.e = var5;
      this.f = var6;
   }

   public GuiWidget a(int var1) {
      this.g = var1;
      return this;
   }

   public GuiWidget a(float var1) {
      this.h = var1;
      return this;
   }

   public void a() {}

   public void a(GuiButton var1) {}

   public void a(int var1, int var2, int var3) {}

   public void b() {}

   public void a(int var1, int var2, float var3) {
      this.c();
   }

   public void c() {
      CDRenderHelper.a((double)this.b, (double)this.c, (double)this.d, (double)this.e, this.g, this.h);
   }
}
