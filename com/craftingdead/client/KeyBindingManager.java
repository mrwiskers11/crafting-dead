package com.craftingdead.client;

import cpw.mods.fml.client.registry.KeyBindingRegistry.KeyHandler;
import cpw.mods.fml.common.TickType;
import java.util.Arrays;
import java.util.EnumSet;
import net.minecraft.client.settings.KeyBinding;

public class KeyBindingManager extends KeyHandler {

   public static final KeyBinding a = new KeyBinding("Reload", 19);
   public static final KeyBinding b = new KeyBinding("Cycle Fire-Modes", 33);
   public static final KeyBinding c = new KeyBinding("Sprint", 29);
   public static final KeyBinding d = new KeyBinding("Quick Gun", 34);
   public static final KeyBinding e = new KeyBinding("Quick Melee", 45);
   public static final KeyBinding f = new KeyBinding("Quick Backpack", 48);
   public static final KeyBinding g = new KeyBinding("Quick Tac-Vest", 46);
   public static final KeyBinding h = new KeyBinding("Toggle NV", 47);
   public static final KeyBinding i = new KeyBinding("Toggle HUD", 24);
   public static final KeyBinding j = new KeyBinding("Open map GUI", 50);
   public static final KeyBinding k = new KeyBinding("New waypoint", 210);
   public static final KeyBinding l = new KeyBinding("Next map mode", 49);
   public static final KeyBinding m = new KeyBinding("Next waypoint group", 51);
   public static final KeyBinding n = new KeyBinding("Teleport to waypoint", 52);
   public static final KeyBinding o = new KeyBinding("Minimap zoom in", 201);
   public static final KeyBinding p = new KeyBinding("Minimap zoom out", 209);
   public static KeyBinding[] q = new KeyBinding[]{a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p};
   public static boolean[] r = new boolean[q.length];


   public KeyBindingManager() {
      super(q, r);
   }

   public String getLabel() {
      return "Crafting Dead Keys";
   }

   public void keyDown(EnumSet var1, KeyBinding var2, boolean var3, boolean var4) {}

   public void keyUp(EnumSet var1, KeyBinding var2, boolean var3) {}

   public EnumSet ticks() {
      return EnumSet.of(TickType.CLIENT);
   }

   static {
      Arrays.fill(r, true);
   }
}
