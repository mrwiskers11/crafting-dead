package com.craftingdead;

import com.craftingdead.CraftingDead;
import com.craftingdead.entity.EntityCorpse;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.IPlayerTracker;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public class CommonPlayerTracker implements IPlayerTracker {

   public void onPlayerLogin(EntityPlayer var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         CraftingDead.m().a(var1.username);
      }

      NBTTagCompound var2 = var1.getEntityData().getCompoundTag("cda");
      PlayerData var3 = PlayerDataHandler.a(var1);
      var3.a();
      var3.a(var2);
      var3.L = false;
      if(var2.hasKey("isdead") && var2.getBoolean("isdead") && !var1.capabilities.isCreativeMode) {
         var1.inventory.clearInventory(-1, -1);
         var3.c();
         var3.L = true;
         var3.M = false;
      }

   }

   public void onPlayerLogout(EntityPlayer var1) {
      NBTTagCompound var2 = new NBTTagCompound();
      PlayerData var3 = PlayerDataHandler.a(var1);
      var2.setBoolean("isdead", false);
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         CraftingDead.m().b(var1.username);
         if(var3.K != -1 && var3.K > 10 && !var1.capabilities.isCreativeMode) {
            var2.setBoolean("isdead", true);
            EntityCorpse.b(var1);
         }
      }

      var1.getEntityData().setCompoundTag("cda", var2);
      var3.b(var2);
   }

   public void onPlayerChangedDimension(EntityPlayer var1) {}

   public void onPlayerRespawn(EntityPlayer var1) {}
}
