package com.craftingdead.asm;

import com.craftingdead.CraftingDead;
import com.craftingdead.asm.CDLoadingPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import net.minecraft.launchwrapper.IClassTransformer;
import org.apache.commons.io.IOUtils;

public class ClassInjector implements IClassTransformer {

   public byte[] transform(String var1, String var2, byte[] var3) {
      boolean var4 = false;
      if(var1.equals("bhj") || var1.equals("bbj") || var1.equals("bid")) {
         var4 = true;
      }

      if(var4) {
         CraftingDead.d.info("Patching: " + var2);
         byte[] var5 = this.a(var1.replace('.', '/') + ".class", CDLoadingPlugin.a());
         return var5 != null?var5:var3;
      } else {
         return var3;
      }
   }

   public byte[] a(String var1, File var2) {
      try {
         ZipFile var3 = new ZipFile(var2);
         Throwable var4 = null;

         Object var8;
         try {
            ZipEntry var5 = var3.getEntry(var1);
            if(var5 == null) {
               throw new IOException(var1 + " not found in " + var3.getName());
            }

            InputStream var6 = var3.getInputStream(var5);
            Throwable var7 = null;

            try {
               var8 = IOUtils.toByteArray(var6);
            } catch (Throwable var33) {
               var8 = var33;
               var7 = var33;
               throw var33;
            } finally {
               if(var6 != null) {
                  if(var7 != null) {
                     try {
                        var6.close();
                     } catch (Throwable var32) {
                        var7.addSuppressed(var32);
                     }
                  } else {
                     var6.close();
                  }
               }

            }
         } catch (Throwable var35) {
            var4 = var35;
            throw var35;
         } finally {
            if(var3 != null) {
               if(var4 != null) {
                  try {
                     var3.close();
                  } catch (Throwable var31) {
                     var4.addSuppressed(var31);
                  }
               } else {
                  var3.close();
               }
            }

         }

         return (byte[])var8;
      } catch (IOException var37) {
         CraftingDead.d.log(Level.WARNING, String.format("Could not read %s from zip file %s", new Object[]{var1, var2.getAbsolutePath()}), var37);
         return null;
      }
   }
}
