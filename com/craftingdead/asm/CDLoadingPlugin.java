package com.craftingdead.asm;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin.MCVersion;
import java.io.File;
import java.util.Map;

import com.craftingdead.asm.ClassInjector;

@MCVersion("1.6.4")
public class CDLoadingPlugin implements IFMLLoadingPlugin {

   private static final String[] a = new String[]{ClassInjector.class.getCanonicalName()};
   private static File b;


   public String[] getLibraryRequestClass() {
      return null;
   }

   public String[] getASMTransformerClass() {
      return a;
   }

   public String getModContainerClass() {
      return null;
   }

   public String getSetupClass() {
      return null;
   }

   public void injectData(Map var1) {
      b = (File)var1.get("coremodLocation");
   }

   public static File a() {
      return b;
   }

}
