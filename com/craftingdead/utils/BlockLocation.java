package com.craftingdead.utils;


public class BlockLocation {

   public int b;
   public int c;
   public int d;
   public double e;
   public double f;
   public double g;
   public double h;
   public double i;


   public BlockLocation(int var1, int var2, int var3) {
      this.b = var1;
      this.c = var2;
      this.d = var3;
   }

   public BlockLocation(double var1, double var3, double var5, int var7, int var8) {
      this.e = var1;
      this.f = var3;
      this.g = var5;
      this.h = (double)var7;
      this.i = (double)var8;
   }
}
