package com.craftingdead.utils;

import java.util.ArrayList;

public class StringHelper {

   public static String a(ArrayList var0, boolean var1) {
      String var2 = "";

      for(int var3 = 0; var3 < var0.size(); ++var3) {
         var2 = var2 + (String)var0.get(var3);
         if(var3 != var0.size() - 1) {
            var2 = var2 + (var1?", ":" ");
         }
      }

      return var2;
   }
}
