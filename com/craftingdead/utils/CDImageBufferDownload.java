package com.craftingdead.utils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import net.minecraft.client.renderer.ImageBufferDownload;

@SideOnly(Side.CLIENT)
public class CDImageBufferDownload extends ImageBufferDownload {

   private int a;
   private int b;


   public BufferedImage parseUserSkin(BufferedImage var1) {
      if(var1 == null) {
         return null;
      } else {
         this.a = 924;
         this.b = 192;
         BufferedImage var2 = new BufferedImage(this.a, this.b, 2);
         Graphics var3 = var2.getGraphics();
         var3.drawImage(var1, 0, 0, (ImageObserver)null);
         var3.dispose();
         return var2;
      }
   }
}
