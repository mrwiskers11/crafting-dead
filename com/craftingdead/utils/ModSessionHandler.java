package com.craftingdead.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import net.minecraft.client.Minecraft;

public class ModSessionHandler {

   public String a = "ModSessionHandlers";


   public boolean a() {
      this.a = this.b();
      Minecraft var1 = Minecraft.getMinecraft();
      String var2 = var1.getSession().getSessionID();
      String var3 = var1.getSession().getUsername();
      if(this.a(var2)) {
         String var4 = this.a(var3, var2);
         if(var4.equalsIgnoreCase("ok")) {
            return true;
         }
      }

      return false;
   }

   private boolean a(String var1) {
      return var1 != null && !var1.equals("-") && !var1.trim().equals("token:0:0") && var1.length() >= 20;
   }

   private String a(String var1, String var2) {
      try {
         URL var3 = new URL("http://session.minecraft.net/game/joinserver.jsp?user=" + this.b(var1) + "&sessionId=" + this.b(var2) + "&serverId=" + this.b(this.a));
         InputStream var4 = var3.openConnection().getInputStream();
         BufferedReader var5 = new BufferedReader(new InputStreamReader(var4));
         String var6 = var5.readLine();
         var5.close();
         return var6;
      } catch (IOException var7) {
         return var7.toString();
      }
   }

   private String b() {
      SecureRandom var1 = new SecureRandom();
      return (new BigInteger(200, var1)).toString(16).substring(0, 40);
   }

   private String b(String var1) throws IOException {
      return URLEncoder.encode(var1, "UTF-8");
   }
}
