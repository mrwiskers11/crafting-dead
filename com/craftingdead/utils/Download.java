package com.craftingdead.utils;

import com.craftingdead.CraftingDead;
import com.craftingdead.utils.ProgressDialog;

import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Observable;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Download extends Observable implements Callable {

   private static final int g = 1024;
   public static final String[] a = new String[]{"Downloading", "Paused", "Complete", "Cancelled", "Error"};
   public static final int b = 0;
   public static final int c = 1;
   public static final int d = 2;
   public static final int e = 3;
   public static final int f = 4;
   private static ExecutorService h = Executors.newCachedThreadPool();
   private URL i;
   private File j;
   private int k;
   private int l;
   private int m;
   private ProgressDialog n;


   public Download(URL var1, File var2) {
      this.i = var1;
      this.j = var2;
      this.k = -1;
      this.l = 0;
      this.n = new ProgressDialog("Downloading " + var2.getName(), "Please Wait...");
   }

   public String a() {
      return this.i.toString();
   }

   public int b() {
      return this.k;
   }

   public float c() {
      return (float)this.l / (float)this.k * 100.0F;
   }

   public int d() {
      return this.m;
   }

   public void e() {
      this.m = 1;
      this.j();
   }

   public Future f() {
      this.m = 0;
      this.j();
      return h.submit(this);
   }

   public void g() {
      this.m = 3;
      this.j();
   }

   private void i() {
      this.m = 4;
      this.j();
   }

   public Void h() {
      RandomAccessFile var1 = null;
      InputStream var2 = null;

      try {
         URLConnection var3 = this.i.openConnection();
         var3.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
         var3.setRequestProperty("Range", "bytes=" + this.l + "-");
         var3.connect();
         if(((HttpURLConnection)var3).getResponseCode() / 100 != 2) {
            this.i();
         }

         int var4 = var3.getContentLength();
         if(var4 < 1) {
            this.i();
         }

         if(this.k == -1) {
            this.k = var4;
            this.j();
         }

         var1 = new RandomAccessFile(this.j.getAbsolutePath(), "rw");
         var1.seek((long)this.l);
         var2 = var3.getInputStream();

         while(this.m == 0) {
            byte[] var5;
            if(this.k - this.l > 1024) {
               var5 = new byte[1024];
            } else {
               var5 = new byte[this.k - this.l];
            }

            int var6 = var2.read(var5);
            if(var6 == -1) {
               break;
            }

            var1.write(var5, 0, var6);
            this.l += var6;
            this.j();
         }

         if(this.m == 0) {
            this.m = 2;
            this.j();
         }
      } catch (Exception var19) {
         this.i();
      } finally {
         if(var1 != null) {
            try {
               var1.close();
            } catch (Exception var18) {
               ;
            }
         }

         if(var2 != null) {
            try {
               var2.close();
            } catch (Exception var17) {
               ;
            }
         }

      }

      return null;
   }

   private void j() {
      CraftingDead.d.info("Downloading: " + this.i.toString() + " " + this.c() + "%");
      this.n.a((int)this.c());
      this.setChanged();
      this.notifyObservers();
   }

   // $FF: synthetic method
   public Object call() throws Exception {
      return this.h();
   }

}
