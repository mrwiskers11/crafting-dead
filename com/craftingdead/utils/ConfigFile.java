package com.craftingdead.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ConfigFile {

   private File a;


   public ConfigFile(File var1) {
      this.a = var1;
      if(!this.a.exists()) {
         this.a.getParentFile().mkdirs();

         try {
            this.a.createNewFile();
         } catch (IOException var3) {
            var3.printStackTrace();
         }
      }

   }

   public int a(String var1, int var2) throws IOException {
      return Integer.parseInt(this.b(var1, "" + var2, (String)null));
   }

   public int a(String var1, int var2, String var3) throws IOException {
      return Integer.parseInt(this.b(var1, "" + var2, var3));
   }

   public String a(String var1, String var2) throws IOException {
      return this.b(var1, var2, (String)null);
   }

   public String a(String var1, String var2, String var3) throws IOException {
      return this.b(var1, var2, var3);
   }

   public boolean a(String var1, boolean var2) throws IOException {
      return this.b(var1, var2?"true":"false", (String)null).equals("true");
   }

   public boolean a(String var1, boolean var2, String var3) throws IOException {
      return this.b(var1, var2?"true":"false", var3).equals("true");
   }

   public String b(String var1, String var2, String var3) throws IOException {
      boolean var4 = false;
      var1 = var1.replace(".", "-");
      BufferedReader var5 = new BufferedReader(new FileReader(this.a));
      Throwable var6 = null;

      label122: {
         String var9;
         try {
            StringBuffer var7 = new StringBuffer();

            String var8;
            do {
               if((var8 = var5.readLine()) == null) {
                  break label122;
               }

               var7.append(var8);
               var7.append("\n");
            } while(var8.startsWith("//") || !var8.split("=")[0].equals(var1));

            var4 = true;
            var9 = var8.split("=")[1];
         } catch (Throwable var19) {
            var6 = var19;
            throw var19;
         } finally {
            if(var5 != null) {
               if(var6 != null) {
                  try {
                     var5.close();
                  } catch (Throwable var18) {
                     var6.addSuppressed(var18);
                  }
               } else {
                  var5.close();
               }
            }

         }

         return var9;
      }

      if(!var4) {
         FileWriter var21 = new FileWriter(this.a, true);
         BufferedWriter var22 = new BufferedWriter(var21);
         if(var3 != null) {
            var22.write("//" + var3);
            var22.newLine();
         }

         var22.write(var1 + "=" + var2);
         var22.newLine();
         var22.close();
      }

      return var2;
   }
}
