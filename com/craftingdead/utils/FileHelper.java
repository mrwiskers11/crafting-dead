package com.craftingdead.utils;

import com.craftingdead.CraftingDead;
import com.craftingdead.utils.Download;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

public class FileHelper {

   public static void a(String var0) {
      File var1 = new File(var0);
      if(var1.exists()) {
         var1.delete();
      }

   }

   public static InputStream a(URL var0) throws IOException {
      URLConnection var1 = var0.openConnection();
      var1.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
      var1.connect();
      if(((HttpURLConnection)var1).getResponseCode() / 100 != 2) {
         return null;
      } else {
         int var2 = var1.getContentLength();
         return var2 < 1?null:var1.getInputStream();
      }
   }

   public static boolean a(File var0, URL var1, String var2) throws NoSuchAlgorithmException, InterruptedException {
      boolean var3 = false;

      try {
         String var4 = null;
         FileInputStream var5 = new FileInputStream(var0);
         Throwable var6 = null;

         try {
            var4 = a((InputStream)var5);
         } catch (Throwable var18) {
            var6 = var18;
            try {
				throw var18;
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         } finally {
            if(var5 != null) {
               if(var6 != null) {
                  try {
                     var5.close();
                  } catch (Throwable var17) {
                     var6.addSuppressed(var17);
                  }
               } else {
                  try {
					var5.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
               }
            }

         }

         if(!var4.equalsIgnoreCase(var2)) {
            CraftingDead.d.info("Remote hash has changed for " + var0.getName());
            var3 = true;
         }
      } catch (FileNotFoundException var21) {
         CraftingDead.d.info(var0.getName() + " does not exists");
         var3 = true;
      }

      if(var3) {
         CraftingDead.d.info("Downloading new version of " + var0.getName() + " from " + var1.toString());
         Download var23 = new Download(var1, var0);

         try {
            var23.f().get();
            return true;
         } catch (ExecutionException var19) {
            CraftingDead.d.log(Level.WARNING, "Could not download file from " + var1.toString(), var19);
         }
      } else {
         CraftingDead.d.info(var0.getName() + " is up to date");
      }

      return false;
   }

   public static String a(InputStream var0) throws NoSuchAlgorithmException, IOException {
      MessageDigest var1 = MessageDigest.getInstance("SHA-1");
      byte[] var2 = new byte[8192];

      for(int var3 = var0.read(var2); var3 != -1; var3 = var0.read(var2)) {
         var1.update(var2, 0, var3);
      }

      return (new HexBinaryAdapter()).marshal(var1.digest());
   }

   public static ArrayList a(String var0, String var1) {
      b(var0, var1);
      return b(var0);
   }

   public static ArrayList b(String var0) {
      File var1 = new File(var0);
      if(!var1.exists()) {
         var1.getParentFile().mkdirs();

         try {
            var1.createNewFile();
         } catch (IOException var7) {
            var7.printStackTrace();
         }
      }

      ArrayList var2 = new ArrayList();

      try {
         FileReader var3 = new FileReader(var1);
         BufferedReader var4 = new BufferedReader(var3);
         StringBuffer var5 = new StringBuffer();

         String var6;
         while((var6 = var4.readLine()) != null) {
            var5.append(var6);
            var5.append("\n");
            var2.add(var6.trim());
         }

         var4.close();
         var3.close();
      } catch (IOException var8) {
         var8.printStackTrace();
      }

      return var2;
   }

   public static void b(String var0, String var1) {
      try {
         File var2 = new File(var0);
         if(var2.exists()) {
            var2.delete();
         }

         BufferedInputStream var3 = null;
         FileOutputStream var4 = null;

         try {
            var3 = new BufferedInputStream((new URL(var1)).openStream());
            var4 = new FileOutputStream(var0);
            byte[] var5 = new byte[1024];

            int var6;
            while((var6 = var3.read(var5, 0, 1024)) != -1) {
               var4.write(var5, 0, var6);
            }
         } finally {
            if(var3 != null) {
               var3.close();
            }

            if(var4 != null) {
               var4.close();
            }

         }
      } catch (MalformedURLException var12) {
         var12.printStackTrace();
      } catch (IOException var13) {
         var13.printStackTrace();
      }

   }
}
