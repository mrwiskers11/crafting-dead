package com.craftingdead.utils;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.border.TitledBorder;

public class ProgressDialog {

   private JFrame a;
   private JProgressBar b;


   public ProgressDialog(String var1, String var2) {
      this.a = new JFrame(var1);
      this.a.setDefaultCloseOperation(0);
      this.a.addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent var1) {
            JOptionPane.showMessageDialog(ProgressDialog.this.a, "Please wait for the operation to finish.", "Warning", 2);
         }
      });
      Container var3 = this.a.getContentPane();
      this.b = new JProgressBar();
      this.b.setStringPainted(true);
      TitledBorder var4 = BorderFactory.createTitledBorder(var2);
      this.b.setBorder(var4);
      var3.add(this.b, "North");
      this.a.setSize(500, 75);
      this.a.setResizable(false);
      this.a.setLocationRelativeTo((Component)null);
      this.a.setVisible(true);
   }

   public void a(int var1) {
      this.b.setValue(var1);
      if(var1 >= 100) {
         this.a.dispose();
      }

   }

   public void a() {
      this.a.dispose();
   }
}
