package com.craftingdead;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockLootSpawner;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.entity.EntityPlayerHead;
import com.craftingdead.entity.SupplyDropManager;
import com.craftingdead.inventory.InventoryFuelTanks;
import com.craftingdead.item.IItemAntiDupe;
import com.craftingdead.item.IItemAntiDupeHelper;
import com.craftingdead.item.ItemClothing;
import com.craftingdead.item.ItemClothingArmor;
import com.craftingdead.item.ItemClothingJuggernaut;
import com.craftingdead.item.ItemFuelTank;
import com.craftingdead.item.ItemFuelTankBackpack;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.potions.PotionManager;
import com.craftingdead.mojang.MojangAPI;
import com.craftingdead.network.packets.CDAPacketPlayerDataToPlayer;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;
import com.craftingdead.server.ServerProxy;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class CommonTickHandler implements ITickHandler {

   private int b = 50;
   private int c = 15;
   private int d = 0;
   private int e = 50;
   private int f = 0;
   private int g = 0;
   private int h = 0;
   private int i = 0;
   private int j = 0;
   private int k = 18000;
   public ArrayList a = new ArrayList();
   private SupplyDropManager l = new SupplyDropManager();


   public void tickStart(EnumSet var1, Object ... var2) {}

   public void tickEnd(EnumSet var1, Object ... var2) {
      if(var1.equals(EnumSet.of(TickType.PLAYER))) {
         this.b((EntityPlayer)var2[0]);
      } else if(var1.equals(EnumSet.of(TickType.SERVER))) {
         this.a(MinecraftServer.getServer());
      }

   }

   public void a(FMLServerStartingEvent var1) {
      this.l.a(var1);
   }

   public void a(FMLServerStoppedEvent var1) {
      this.l.a(var1);
   }

   private void a(MinecraftServer var1) {
      if(FMLCommonHandler.instance().getSide().isServer()) {
         CraftingDead.m().d().a();
      }

      MojangAPI.downloadPendingUUIDs();
      int var2;
      WorldServer var3;
      int var4;
      EntityPlayer var5;
      if(this.d++ >= this.e * 20) {
         for(var2 = 0; var2 < var1.worldServers.length; ++var2) {
            var3 = var1.worldServers[var2];

            for(var4 = 0; var4 < var3.playerEntities.size(); ++var4) {
               var5 = (EntityPlayer)var3.playerEntities.get(var4);
               this.b(var5, this.b);
            }
         }

         this.d = 0;
      }

      if(this.j++ >= this.k) {
         for(var2 = 0; var2 < var1.worldServers.length; ++var2) {
            var3 = var1.worldServers[var2];

            for(var4 = 0; var4 < var3.loadedTileEntityList.size(); ++var4) {
               TileEntity var9 = (TileEntity)var3.loadedTileEntityList.get(var4);
               if(var9 instanceof TileEntityBaseCenter) {
                  TileEntityBaseCenter var6 = (TileEntityBaseCenter)var9;
                  var6.a(15);
               }
            }
         }

         this.j = 0;
      }

      if(this.f++ >= 4) {
         for(var2 = 0; var2 < var1.worldServers.length; ++var2) {
            var3 = var1.worldServers[var2];

            for(var4 = 0; var4 < var3.playerEntities.size(); ++var4) {
               var5 = (EntityPlayer)var3.playerEntities.get(var4);
               ArrayList var10 = a(var5, ServerProxy.g);
               Iterator var7 = var10.iterator();

               while(var7.hasNext()) {
                  EntityPlayer var8 = (EntityPlayer)var7.next();
                  PacketDispatcher.sendPacketToPlayer(CDAPacketPlayerDataToPlayer.a(var8), (Player)var5);
               }
            }
         }

         this.f = 0;
      }

      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.a();
      }

      this.l.a((World)var1.worldServers[0]);
   }

   private void b(EntityPlayer var1) {
      World var2 = var1.worldObj;
      PlayerData var3 = PlayerDataHandler.a(var1);
      InventoryPlayer var4 = var1.inventory;
      ++var3.k;
      if(var3.K > 0) {
         --var3.K;
      }

      if(var3.Q && var3.R >= 200) {
         var3.Q = false;
         var3.R = 0;
      }

      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         if(var3.L) {
            var3.c();
            var1.inventory.clearInventory(-1, -1);
            var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Combat Logging was detected, punishment invoking."));
            EntityPlayerMP var5 = (EntityPlayerMP)var1;
            var5.hurtResistantTime = 0;
            var5.attackEntityFrom(DamageSource.outOfWorld, Float.MAX_VALUE);
            var3.L = false;
            var3.M = true;
         }

         if(!var3.M) {
            var3.L = true;
         }
      }

      if(var3.k % 100 == 0) {
         var3.j = var3.k / 24000;
      }

      int var13;
      if(this.i++ >= 10) {
         if(var1 instanceof EntityPlayerMP) {
            if(!this.a(var1)) {
               EntityPlayerHead var12 = new EntityPlayerHead(var2);
               var12.a = var1;
               var12.setPosition(var1.posX, var1.posY, var1.posZ);
               var2.spawnEntityInWorld(var12);
               this.a.add(var12);
            }

            for(var13 = 0; var13 < this.a.size(); ++var13) {
               if(((EntityPlayerHead)this.a.get(var13)).isDead || ((EntityPlayerHead)this.a.get(var13)).a.isDead) {
                  this.a.remove(var13);
               }
            }
         }

         this.i = 0;
      }

      if(var1.capabilities.isCreativeMode) {
         if(var1.isPotionActive(PotionManager.c.id)) {
            var1.removePotionEffect(PotionManager.c.id);
         }

         if(var1.isPotionActive(PotionManager.b.id)) {
            var1.removePotionEffect(PotionManager.b.id);
         }

         if(var1.isPotionActive(PotionManager.a.id)) {
            var1.removePotionEffect(PotionManager.a.id);
         }
      }

      if(var3.u > 0.0F) {
         var3.u -= 0.05F;
         if(var3.u < 0.0F) {
            var3.u = 0.0F;
         }
      }

      if(var3.x > 0) {
         --var3.x;
      }

      var3.e().a(var1);
      if(!var1.capabilities.isCreativeMode && !var1.isPotionActive(PotionManager.c.id) && var1.onGround && var1.fallDistance > 0.0F && !var1.isInWater()) {
         if(var1.fallDistance > 4.0F) {
            Random var14 = new Random();
            if(var14.nextInt(3) == 0) {
               var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Ouch! You broke your leg!"));
               var1.addPotionEffect(new PotionEffect(PotionManager.c.id, 9999999, 4));
               var1.addPotionEffect(new PotionEffect(Potion.blindness.id, 100, 1));
            }
         } else if(var1.fallDistance > 10.0F) {
            var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Ouch! You broke your leg!"));
            var1.addPotionEffect(new PotionEffect(PotionManager.c.id, 9999999, 4));
            var1.addPotionEffect(new PotionEffect(Potion.blindness.id, 100, 1));
         }
      }

      if(this.h++ > 1) {
         int var6;
         int var7;
         if(var3.d() != null) {
            InventoryFuelTanks var15 = (InventoryFuelTanks)ItemFuelTankBackpack.a(var1);
            if(var15 != null) {
               var6 = 0;

               for(var7 = 0; var7 < var15.getSizeInventory(); ++var7) {
                  ItemStack var8 = var15.getStackInSlot(var7);
                  if(var8 != null) {
                     ItemFuelTank var9 = (ItemFuelTank)var8.getItem();
                     var6 += var9.d - var8.getItemDamage();
                  }
               }

               var3.P = var6;
            }

            ItemStack var16;
            if(var3.d().a("clothing") != null) {
               var16 = var3.d().a("clothing");
               if(var16.getItem() instanceof ItemClothingJuggernaut) {
                  var1.addPotionEffect(new PotionEffect(PotionManager.e.id, 9999999, 1));
               } else if(var1.isPotionActive(PotionManager.e)) {
                  var1.removePotionEffect(PotionManager.e.id);
               }
            } else if(var1.isPotionActive(PotionManager.e)) {
               var1.removePotionEffect(PotionManager.e.id);
            }

            ItemStack var18;
            if(var3.d().a("hat") != null && var3.d().a("clothing") != null) {
               var16 = var3.d().a("hat");
               var18 = var3.d().a("clothing");
               if(var16.getItem() == ItemManager.gd && var18.getItem() == ItemManager.em) {
                  var1.addPotionEffect(new PotionEffect(PotionManager.f.id, 9999999, 1));
               } else if(var1.isPotionActive(PotionManager.f)) {
                  var1.removePotionEffect(PotionManager.f.id);
               }
            } else if(var1.isPotionActive(PotionManager.f)) {
               var1.removePotionEffect(PotionManager.f.id);
            }

            for(var6 = 0; var6 < var4.getSizeInventory(); ++var6) {
               var18 = var4.getStackInSlot(var6);
               if(var6 != 36 && var6 != 37 && var6 != 38 && var6 != 39 && var18 != null) {
                  if(var18.getItem() instanceof ItemClothingArmor) {
                     var4.setInventorySlotContents(var6, (ItemStack)null);
                  } else {
                     if(var18.getItem() instanceof IItemAntiDupe) {
                        IItemAntiDupe var19 = (IItemAntiDupe)var18.getItem();
                        if(var19.h_()) {
                           if(var19.a(var18) == null) {
                              var19.b(var18);
                           } else {
                              String var21 = var19.a(var18);
                              if(IItemAntiDupeHelper.a(var4, var21)) {
                                 var4.setInventorySlotContents(var6, (ItemStack)null);
                                 continue;
                              }
                           }
                        }
                     }

                     int var20;
                     if(var18.stackSize > var18.getMaxStackSize()) {
                        var20 = var18.stackSize - var18.getMaxStackSize();
                        if(var20 > 16) {
                           var20 = 16;
                        }

                        for(int var22 = 0; var22 < var20; ++var22) {
                           ItemStack var10 = new ItemStack(var18.getItem(), 1, 0);
                           if(!var4.addItemStackToInventory(var10)) {
                              EntityGroundItem var11 = new EntityGroundItem(var1.worldObj, var1.posX, var1.posY, var1.posZ);
                              var11.a(var10);
                              var1.worldObj.spawnEntityInWorld(var11);
                           }
                        }

                        var18.stackSize = var18.getMaxStackSize();
                     } else if(FMLCommonHandler.instance().getSide() == Side.SERVER && !ServerProxy.e && !MinecraftServer.getServer().getConfigurationManager().isPlayerOpped(var1.username)) {
                        var20 = var18.itemID - 256;
                        if(var18.getItem() instanceof ItemBlock) {
                           var4.setInventorySlotContents(var6, (ItemStack)null);
                        } else if(!ItemManager.g.contains(Integer.valueOf(var20)) && !ItemManager.g.contains(Integer.valueOf(var18.itemID))) {
                           var4.setInventorySlotContents(var6, (ItemStack)null);
                        }
                     }
                  }
               }
            }

            this.h = 0;
         }

         ItemStack var17;
         if(this.g++ > 4) {
            if(FMLCommonHandler.instance().getSide() == Side.SERVER && ServerProxy.f) {
               this.g = 0;
               return;
            }

            var17 = var3.d().a("clothing");
            if(var17 != null) {
               var6 = ItemClothing.b(var17);
               if(var3.y != var6) {
                  for(var7 = 0; var7 < var4.armorInventory.length; ++var7) {
                     if(var4.armorInventory[var7] != null) {
                        var4.armorInventory[var7] = null;
                     }
                  }

                  var3.y = var6;
               }

               switch(var6) {
               case 1:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }
                  break;
               case 2:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }

                  if(var4.getStackInSlot(38) == null) {
                     var4.setInventorySlotContents(38, new ItemStack(ItemManager.es));
                  }
                  break;
               case 3:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }

                  if(var4.getStackInSlot(38) == null) {
                     var4.setInventorySlotContents(38, new ItemStack(ItemManager.es));
                  }

                  if(var4.getStackInSlot(37) == null) {
                     var4.setInventorySlotContents(37, new ItemStack(ItemManager.et));
                  }
               }
            } else {
               for(var6 = 0; var6 < var4.armorInventory.length; ++var6) {
                  if(var4.armorInventory[var6] != null) {
                     var4.armorInventory[var6] = null;
                  }
               }
            }

            this.g = 0;
         }

         if(this.g++ > 4) {
            if(FMLCommonHandler.instance().getSide() == Side.SERVER && ServerProxy.f) {
               this.g = 0;
               return;
            }

            if(ItemClothing.a(var3)) {
               var17 = var3.d().a("clothing");
               var6 = ItemClothing.b(var17);
               if(var3.y != var6) {
                  for(var7 = 0; var7 < var4.armorInventory.length; ++var7) {
                     if(var4.armorInventory[var7] != null) {
                        var4.armorInventory[var7] = null;
                     }
                  }

                  var3.y = var6;
               }

               switch(var6) {
               case 1:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }
                  break;
               case 2:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }

                  if(var4.getStackInSlot(38) == null) {
                     var4.setInventorySlotContents(38, new ItemStack(ItemManager.es));
                  }
                  break;
               case 3:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }

                  if(var4.getStackInSlot(38) == null) {
                     var4.setInventorySlotContents(38, new ItemStack(ItemManager.es));
                  }

                  if(var4.getStackInSlot(37) == null) {
                     var4.setInventorySlotContents(37, new ItemStack(ItemManager.et));
                  }
                  break;
               case 4:
                  if(var4.getStackInSlot(39) == null) {
                     var4.setInventorySlotContents(39, new ItemStack(ItemManager.er));
                  }

                  if(var4.getStackInSlot(38) == null) {
                     var4.setInventorySlotContents(38, new ItemStack(ItemManager.es));
                  }

                  if(var4.getStackInSlot(37) == null) {
                     var4.setInventorySlotContents(37, new ItemStack(ItemManager.et));
                  }

                  if(var4.getStackInSlot(36) == null) {
                     var4.setInventorySlotContents(36, new ItemStack(ItemManager.eu));
                  }
               }
            } else {
               for(var13 = 0; var13 < var4.armorInventory.length; ++var13) {
                  if(var4.armorInventory[var13] != null) {
                     var4.armorInventory[var13] = null;
                  }
               }
            }

            this.g = 0;
         }
      }

   }

   public static ArrayList a(EntityPlayer var0, int var1) {
      ArrayList var2 = new ArrayList();
      AxisAlignedBB var3 = AxisAlignedBB.getBoundingBox(var0.posX - (double)var1, var0.posY - (double)var1, var0.posZ - (double)var1, var0.posX + (double)var1, var0.posY + (double)var1, var0.posZ + (double)var1);
      List var4 = var0.worldObj.getEntitiesWithinAABB(EntityPlayer.class, var3);

      for(int var5 = 0; var5 < var4.size(); ++var5) {
         Entity var6 = (Entity)var4.get(var5);
         if(var6 instanceof EntityPlayer) {
            var2.add((EntityPlayer)var6);
         }
      }

      return var2;
   }

   public boolean a(EntityPlayer var1) {
      for(int var2 = 0; var2 < this.a.size(); ++var2) {
         if(((EntityPlayerHead)this.a.get(var2)).a == var1) {
            return true;
         }
      }

      return false;
   }

   public void b(EntityPlayer var1, int var2) {
      int var3 = (int)var1.posX;
      int var4 = (int)var1.posY;
      int var5 = (int)var1.posZ;
      byte var6 = 15;

      for(int var7 = -var2; var7 < var2; ++var7) {
         for(int var8 = -var6; var8 < var6; ++var8) {
            for(int var9 = -var2; var9 < var2; ++var9) {
               int var10 = var1.worldObj.getBlockId(var3 + var7, var4 + var8, var5 + var9);
               if(var10 != 0 && Block.blocksList[var10] instanceof BlockLootSpawner && var1.getDistance((double)(var3 + var7), (double)(var4 + var8), (double)(var5 + var9)) > (double)this.c) {
                  BlockLootSpawner var11 = (BlockLootSpawner)Block.blocksList[var10];
                  if(var11.e()) {
                     int var12 = var1.worldObj.getBlockId(var3 + var7, var4 + var8 + 1, var5 + var9);
                     if(var12 == 0) {
                        var1.worldObj.setBlock(var3 + var7, var4 + var8 + 1, var5 + var9, var11.a);
                     }
                  }
               }
            }
         }
      }

   }

   public SupplyDropManager a() {
      return this.l;
   }

   public EnumSet ticks() {
      return EnumSet.of(TickType.PLAYER, TickType.SERVER);
   }

   public String getLabel() {
      return "commontick";
   }
}
