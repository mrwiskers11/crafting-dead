package com.craftingdead.entity;

import com.craftingdead.entity.EntityGrenade;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityGrenadePipeBomb extends EntityGrenade {

   public boolean j = false;


   public EntityGrenadePipeBomb(World var1, EntityLivingBase var2, double var3, int var5) {
      super(var1, var2.posX, var2.posY + 1.5D, var2.posZ, var2.rotationYaw, var2.rotationPitch, var3, var5);
      super.i = var2;
   }

   public EntityGrenadePipeBomb(World var1) {
      super(var1);
      super.e = 60;
   }
}
