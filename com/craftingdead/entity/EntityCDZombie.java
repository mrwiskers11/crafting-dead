package com.craftingdead.entity;

import com.craftingdead.damage.DamageSourceZombie;
import com.craftingdead.entity.EntityCDAIAttackOnCollide;
import com.craftingdead.item.ItemManager;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentThorns;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class EntityCDZombie extends EntityMob {

   private String bp = "";


   public EntityCDZombie(World var1) {
      super(var1);
      this.setSize(1.0F, 2.0F);
      this.bp = "" + (1 + super.rand.nextInt(23));
      this.getNavigator().setBreakDoors(true);
      super.tasks.addTask(0, new EntityAISwimming(this));
      super.tasks.addTask(1, new EntityAIBreakDoor(this));
      super.tasks.addTask(2, new EntityCDAIAttackOnCollide(this, EntityPlayer.class, 1.0D, false));
      super.tasks.addTask(4, new EntityAIMoveTowardsRestriction(this, 1.0D));
      super.tasks.addTask(5, new EntityAIMoveThroughVillage(this, 1.0D, false));
      super.tasks.addTask(6, new EntityAIWander(this, 0.6D));
      super.tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class, 12.0F));
      super.tasks.addTask(7, new EntityAILookIdle(this));
      super.targetTasks.addTask(1, new EntityAIHurtByTarget(this, true));
      super.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));
   }

   protected void entityInit() {
      super.entityInit();
   }

   protected void applyEntityAttributes() {
      super.applyEntityAttributes();
      this.getEntityAttribute(SharedMonsterAttributes.followRange).setAttribute(30.0D);
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(20.0D);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.22D);
      this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setAttribute(3.0D);
   }

   public int getTotalArmorValue() {
      int var1 = super.getTotalArmorValue() + 2;
      if(var1 > 20) {
         var1 = 20;
      }

      return var1;
   }

   public int getMaxSpawnedInChunk() {
      return 12;
   }

   public void onEntityUpdate() {
      super.onEntityUpdate();
   }

   public String b() {
      return this.bp;
   }

   public boolean getCanSpawnHere() {
      return true;
   }

   protected boolean isAIEnabled() {
      return true;
   }

   protected String getLivingSound() {
      return "mob.zombie.say";
   }

   protected String getHurtSound() {
      return "mob.zombie.hurt";
   }

   protected String getDeathSound() {
      return "mob.zombie.death";
   }

   protected void playStepSound(int var1, int var2, int var3, int var4) {
      this.playSound("mob.zombie.step", 0.15F, 1.0F);
   }

   protected int getDropItemId() {
      if(super.rand.nextInt(16) == 0) {
         switch(super.rand.nextInt(3)) {
         case 0:
            return ItemManager.eW.itemID;
         case 1:
            return ItemManager.eX.itemID;
         case 2:
            return ItemManager.eY.itemID;
         }
      }

      return super.rand.nextInt(2) == 0?ItemManager.br.itemID:0;
   }

   protected void dropFewItems(boolean var1, int var2) {
      int var3 = this.getDropItemId();
      if(var3 > 0) {
         this.dropItem(var3, 1);
      }

   }

   public int c(Entity var1) {
      return 6;
   }

   public boolean attackEntityAsMob(Entity var1) {
      int var2 = this.c(var1);
      boolean var3 = var1.attackEntityFrom(new DamageSourceZombie(this), (float)var2);
      if(var3) {
         int var4 = EnchantmentHelper.getFireAspectModifier(this);
         if(var4 > 0) {
            var1.setFire(var4 * 4);
         }

         if(var1 instanceof EntityLiving) {
            EnchantmentThorns.func_92096_a(this, (EntityLiving)var1, super.rand);
         }
      }

      return var3;
   }

   public EnumCreatureAttribute getCreatureAttribute() {
      return EnumCreatureAttribute.UNDEAD;
   }
}
