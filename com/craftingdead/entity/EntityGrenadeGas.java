package com.craftingdead.entity;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.entity.EntityManager;
import com.craftingdead.item.ItemManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class EntityGrenadeGas extends EntityGrenade {

   public boolean j = false;
   private Random ax = new Random();
   public int au = 500;
   public double av = 0.4D;
   public double aw = 3.25D;
   private double ay = 0.0D;
   private double az = 0.5D;


   public EntityGrenadeGas(World var1) {
      super(var1);
      super.e = -1;
   }

   public EntityGrenadeGas(World var1, EntityLivingBase var2, double var3, int var5) {
      super(var1, var2, var3, var5);
      super.e = -1;
   }

   public void onUpdate() {
      super.onUpdate();
      if(this.j) {
         if(this.au % 10 == 0 && this.av < this.aw) {
            this.av += 0.25D;
         }

         if(super.worldObj.isRemote && this.au % 6 == 0) {
            for(double var1 = -this.av; var1 < this.av; ++var1) {
               for(double var3 = -this.av; var3 < this.av; ++var3) {
                  for(double var5 = -this.av; var5 < this.av; ++var5) {
                     double var7 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     double var9 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     double var11 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     EntityManager.a("smoke", super.posX + var1, super.posY + var3 + 1.0D, super.posZ + var5, 0.0D + var7, 0.0D + var9, 0.0D + var11);
                  }
               }
            }
         }

         if(!super.worldObj.isRemote) {
            if(this.ay > this.az) {
               ArrayList var13 = this.a(this, 2);
               if(var13.size() > 0) {
                  for(int var2 = 0; var2 < var13.size(); ++var2) {
                     EntityPlayer var14 = (EntityPlayer)var13.get(var2);
                     PlayerData var4 = PlayerDataHandler.a(var14);
                     if(var4 != null && var4.d().a("hat") != null && var4.d().a("hat").itemID == ItemManager.fx.itemID) {
                        return;
                     }

                     var14.attackEntityFrom(DamageSource.generic, 2.0F);
                  }
               }

               this.ay = 0.0D;
            } else {
               this.ay += 0.1D;
            }
         }

         if(this.au-- <= 0) {
            this.setDead();
         }
      }

   }

   public void j() {
      this.j = true;
   }

   public void k() {}

   public ArrayList a(EntityGrenadeGas var1, int var2) {
      ArrayList var3 = new ArrayList();
      AxisAlignedBB var4 = AxisAlignedBB.getBoundingBox(var1.posX - (double)var2, var1.posY - (double)var2, var1.posZ - (double)var2, var1.posX + (double)var2, var1.posY + (double)var2, var1.posZ + (double)var2);
      List var5 = var1.worldObj.getEntitiesWithinAABB(EntityPlayer.class, var4);

      for(int var6 = 0; var6 < var5.size(); ++var6) {
         Entity var7 = (Entity)var5.get(var6);
         if(var7 instanceof EntityPlayer) {
            var3.add((EntityPlayer)var7);
         }
      }

      return var3;
   }
}
