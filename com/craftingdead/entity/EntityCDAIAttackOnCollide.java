package com.craftingdead.entity;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityCDAIAttackOnCollide extends EntityAIBase {

   World a;
   EntityCreature b;
   int c;
   double d;
   boolean e;
   PathEntity f;
   Class g;
   private int h;
   private int i;


   public EntityCDAIAttackOnCollide(EntityCreature var1, Class var2, double var3, boolean var5) {
      this(var1, var3, var5);
      this.g = var2;
   }

   public EntityCDAIAttackOnCollide(EntityCreature var1, double var2, boolean var4) {
      this.b = var1;
      this.a = var1.worldObj;
      this.d = var2;
      this.e = var4;
      this.setMutexBits(3);
   }

   public boolean shouldExecute() {
      EntityLivingBase var1 = this.b.getAttackTarget();
      if(var1 == null) {
         return false;
      } else if(!var1.isEntityAlive()) {
         return false;
      } else if(this.g != null && !this.g.isAssignableFrom(var1.getClass())) {
         return false;
      } else if(--this.h <= 0) {
         this.f = this.b.getNavigator().getPathToEntityLiving(var1);
         this.h = 4 + this.b.getRNG().nextInt(7);
         return this.f != null;
      } else {
         return true;
      }
   }

   public boolean continueExecuting() {
      EntityLivingBase var1 = this.b.getAttackTarget();
      return var1 == null?false:(!var1.isEntityAlive()?false:(!this.e?!this.b.getNavigator().noPath():this.b.func_110176_b(MathHelper.floor_double(var1.posX), MathHelper.floor_double(var1.posY), MathHelper.floor_double(var1.posZ))));
   }

   public void startExecuting() {
      this.b.getNavigator().setPath(this.f, this.d);
      this.h = 0;
   }

   public void resetTask() {
      this.b.getNavigator().clearPathEntity();
   }

   public void updateTask() {
      EntityLivingBase var1 = this.b.getAttackTarget();
      this.b.getLookHelper().setLookPositionWithEntity(var1, 30.0F, 30.0F);
      if((this.e || this.b.getEntitySenses().canSee(var1)) && --this.h <= 0) {
         this.h = this.i + 4 + this.b.getRNG().nextInt(7);
         this.b.getNavigator().tryMoveToEntityLiving(var1, this.d);
         if(this.b.getNavigator().getPath() != null) {
            PathPoint var2 = this.b.getNavigator().getPath().getFinalPathPoint();
            if(var2 != null && var1.getDistanceSq((double)var2.xCoord, (double)var2.yCoord, (double)var2.zCoord) < 1.0D) {
               this.i = 0;
            } else {
               this.i += 10;
            }
         } else {
            this.i += 10;
         }
      }

      this.c = Math.max(this.c - 1, 0);
      if(this.b.getDistanceToEntity(var1) <= 1.5F && this.c <= 0) {
         this.c = 10;
         if(this.b.getHeldItem() != null) {
            this.b.swingItem();
         }

         this.b.attackEntityAsMob(var1);
      }

   }
}
