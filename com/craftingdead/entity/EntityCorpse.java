package com.craftingdead.entity;

import com.craftingdead.inventory.ContainerCDChest;
import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.item.ItemClothingArmor;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.ArrayList;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet100OpenWindow;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;

public class EntityCorpse extends Entity implements IInventory {

   public ItemStack[] a = new ItemStack[45];
   public String b = "";
   public int c = 4800;
   public int d = 0;


   public EntityCorpse(World var1) {
      super(var1);
      this.setSize(1.0F, 0.5F);
   }

   public EntityCorpse(EntityPlayer var1) {
      super(var1.worldObj);
      this.b = var1.getEntityName();
      this.a(this.b);
      this.setPosition(var1.posX, var1.posY, var1.posZ);
      this.a = new ItemStack[45];
      this.setSize(1.0F, 0.5F);
   }

   protected void entityInit() {
      super.dataWatcher.addObject(20, "");
      super.dataWatcher.addObject(21, Integer.valueOf(0));
   }

   public void onEntityUpdate() {
      super.onEntityUpdate();
      if(this.i() > 4) {
         this.setDead();
      }

      if(this.d++ > this.c) {
         this.setDead();
         this.d = 0;
      }

      if(!super.onGround) {
         super.motionY -= 0.02D;
      }

      this.moveEntity(super.motionX, super.motionY, super.motionZ);
   }

   public boolean hitByEntity(Entity var1) {
      this.c(this.i() + 1);
      return false;
   }

   public boolean canBeCollidedWith() {
      return true;
   }

   public boolean interactFirst(EntityPlayer var1) {
      if(!super.worldObj.isRemote && !super.isDead) {
         this.a(var1, this);
         return true;
      } else {
         return false;
      }
   }

   public void a(EntityPlayer var1, IInventory var2) {
      if(var1 instanceof EntityPlayerMP) {
         EntityPlayerMP var3 = (EntityPlayerMP)var1;
         if(var3.openContainer != var3.inventoryContainer) {
            var3.closeScreen();
         }

         var3.incrementWindowID();
         var3.playerNetServerHandler.sendPacketToPlayer(new Packet100OpenWindow(var3.currentWindowId, 0, var2.getInvName(), var2.getSizeInventory(), var2.isInvNameLocalized()));
         var3.openContainer = new ContainerCDChest(var3.inventory, var2);
         var3.openContainer.windowId = var3.currentWindowId;
         var3.openContainer.addCraftingToCrafters(var3);
      }

   }

   protected void readEntityFromNBT(NBTTagCompound var1) {
      this.b = var1.getString("texture");
      this.a(this.b);
      this.d = var1.getInteger("age");
      NBTTagList var2 = var1.getTagList("Items");
      this.a = new ItemStack[this.getSizeInventory()];

      for(int var3 = 0; var3 < var2.tagCount(); ++var3) {
         NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
         int var5 = var4.getByte("Slot") & 255;
         if(var5 >= 0 && var5 < this.a.length) {
            this.a[var5] = ItemStack.loadItemStackFromNBT(var4);
         }
      }

   }

   protected void writeEntityToNBT(NBTTagCompound var1) {
      var1.setString("texture", this.b);
      var1.setInteger("age", this.d);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.a.length; ++var3) {
         if(this.a[var3] != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.setByte("Slot", (byte)var3);
            this.a[var3].writeToNBT(var4);
            var2.appendTag(var4);
         }
      }

      var1.setTag("Items", var2);
   }

   public void a(String var1) {
      super.dataWatcher.updateObject(20, var1);
   }

   public void c(int var1) {
      super.dataWatcher.updateObject(21, Integer.valueOf(var1));
   }

   public String h() {
      return super.dataWatcher.getWatchableObjectString(20);
   }

   public int i() {
      return super.dataWatcher.getWatchableObjectInt(21);
   }

   public int getSizeInventory() {
      return this.a.length;
   }

   public ItemStack getStackInSlot(int var1) {
      return this.a[var1];
   }

   public ItemStack decrStackSize(int var1, int var2) {
      if(this.a[var1] != null) {
         ItemStack var3;
         if(this.a[var1].stackSize <= var2) {
            var3 = this.a[var1];
            this.a[var1] = null;
            return var3;
         } else {
            var3 = this.a[var1].splitStack(var2);
            if(this.a[var1].stackSize == 0) {
               this.a[var1] = null;
            }

            return var3;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int var1) {
      if(this.a[var1] != null) {
         ItemStack var2 = this.a[var1];
         this.a[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void a(ArrayList var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if(this.a[var2] == null && var1.get(var2) != null && !(((ItemStack)var1.get(var2)).getItem() instanceof ItemClothingArmor)) {
            this.setInventorySlotContents(var2, (ItemStack)var1.get(var2));
         }
      }

      this.j();
   }

   public void b(ArrayList var1) {
      for(int var2 = 0; var2 < var1.size(); ++var2) {
         if(this.a[var2] == null && var1.get(var2) != null && !(((EntityItem)var1.get(var2)).getEntityItem().getItem() instanceof ItemClothingArmor)) {
            this.setInventorySlotContents(var2, ((EntityItem)var1.get(var2)).getEntityItem());
         }
      }

      this.j();
   }

   private void j() {
      for(int var1 = 0; var1 < this.a.length; ++var1) {
         if(this.a[var1] != null && this.a[var1].getItem() instanceof ItemClothingArmor) {
            this.decrStackSize(var1, 1);
         }
      }

   }

   public void setInventorySlotContents(int var1, ItemStack var2) {
      this.a[var1] = var2;
      if(var2 != null && var2.stackSize > this.getInventoryStackLimit()) {
         var2.stackSize = this.getInventoryStackLimit();
      }

   }

   public String getInvName() {
      return this.h() + "\'s Corpse";
   }

   public boolean isInvNameLocalized() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public void onInventoryChanged() {}

   public boolean isUseableByPlayer(EntityPlayer var1) {
      return super.isDead?false:var1.getDistanceSqToEntity(this) <= 64.0D;
   }

   public void openChest() {}

   public void closeChest() {}

   public boolean isItemValidForSlot(int var1, ItemStack var2) {
      return false;
   }

   public static void b(EntityPlayer var0) {
      World var1 = MinecraftServer.getServer().getEntityWorld();
      PlayerData var2 = PlayerDataHandler.a(var0);
      InventoryPlayer var3 = var0.inventory;
      InventoryCDA var4 = var2.d();
      ArrayList var5 = new ArrayList();

      int var6;
      ItemStack var7;
      for(var6 = 0; var6 < var3.getSizeInventory(); ++var6) {
         var7 = var3.getStackInSlot(var6);
         if(var7 != null && !(var7.getItem() instanceof ItemClothingArmor)) {
            var5.add(var7.copy());
         }
      }

      for(var6 = 0; var6 < var4.getSizeInventory(); ++var6) {
         var7 = var4.getStackInSlot(var6);
         if(var7 != null && !(var7.getItem() instanceof ItemClothingArmor)) {
            var5.add(var7.copy());
         }
      }

      EntityCorpse var8 = new EntityCorpse(var0);
      var8.a(var5);
      var1.spawnEntityInWorld(var8);
   }
}
