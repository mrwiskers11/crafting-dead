package com.craftingdead.entity;

import com.craftingdead.entity.EntityCDZombie;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.world.World;

public class EntityCDZombieFast extends EntityCDZombie {

   public EntityCDZombieFast(World var1) {
      super(var1);
   }

   protected void applyEntityAttributes() {
      super.applyEntityAttributes();
      this.getEntityAttribute(SharedMonsterAttributes.followRange).setAttribute(30.0D);
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(10.0D);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.34D);
      this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setAttribute(4.0D);
   }

   public int c(Entity var1) {
      return 4;
   }
}
