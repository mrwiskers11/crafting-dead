package com.craftingdead.entity;

import com.craftingdead.entity.EntityCDZombie;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.world.World;

public class EntityCDZombieWeak extends EntityCDZombie {

   public EntityCDZombieWeak(World var1) {
      super(var1);
   }

   protected void applyEntityAttributes() {
      super.applyEntityAttributes();
      this.getEntityAttribute(SharedMonsterAttributes.followRange).setAttribute(25.0D);
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(5.0D);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.22D);
      this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setAttribute(2.0D);
   }

   public int c(Entity var1) {
      return 2;
   }
}
