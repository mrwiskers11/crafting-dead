package com.craftingdead.entity;

import java.util.Random;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.entity.EntityManager;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityGrenadeDecoy extends EntityGrenade {

   public boolean j = false;
   public String au = "akmshoot";
   private Random aw = new Random();
   public int av = 300;


   public EntityGrenadeDecoy(World var1) {
      super(var1);
   }

   public EntityGrenadeDecoy(World var1, EntityLivingBase var2, double var3, int var5) {
      super(var1, var2, var3, var5);
   }

   public EntityGrenadeDecoy a(String var1) {
      this.au = var1;
      return this;
   }

   public void onUpdate() {
      super.onUpdate();
      if(this.j) {
         if(this.aw.nextInt(20) == 0) {
            super.worldObj.playSoundAtEntity(this, "craftingdead:" + this.au, 1.0F, 1.0F);

            for(int var1 = 0; var1 < 6; ++var1) {
               EntityManager.a("smoke", super.posX, super.posY + 0.1D, super.posZ, 0.0D, 0.0D, 0.0D);
            }
         }

         if(this.av-- <= 0) {
            this.setDead();
            super.worldObj.createExplosion((Entity)null, super.posX, super.posY, super.posZ, 0.2F, false);
         }
      }

   }

   public void j() {
      this.j = true;
   }

   public void k() {}
}
