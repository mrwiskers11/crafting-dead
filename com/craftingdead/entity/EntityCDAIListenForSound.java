package com.craftingdead.entity;

import com.craftingdead.entity.EntityCDZombie;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;

public class EntityCDAIListenForSound extends EntityAIBase {

   private EntityPlayer b;
   protected EntityCDZombie a;
   private double c = 10.0D;


   public EntityCDAIListenForSound(EntityCDZombie var1, double var2) {
      this.a = var1;
      this.c = var2;
   }

   public boolean shouldExecute() {
      List var1 = this.a.worldObj.getEntitiesWithinAABBExcludingEntity(this.a, this.a.boundingBox.expand(this.c, this.c, this.c));
      if(!var1.isEmpty()) {
         Iterator var2 = var1.iterator();

         while(var2.hasNext()) {
            Entity var3 = (Entity)var2.next();
            if(var3 instanceof EntityPlayer) {
               EntityPlayer var4 = (EntityPlayer)var3;
               PlayerData var5 = PlayerDataHandler.a(var4);
               if((float)var5.g() >= this.a.getDistanceToEntity(var4)) {
                  this.b = var4;
                  return true;
               }
            }
         }
      }

      return false;
   }

   public void startExecuting() {
      this.a.setAttackTarget(this.b);
   }
}
