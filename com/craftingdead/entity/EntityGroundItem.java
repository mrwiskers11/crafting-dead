package com.craftingdead.entity;

import com.craftingdead.item.ItemManager;
import com.craftingdead.server.ServerProxy;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.util.Iterator;
import net.minecraft.entity.DataWatcher;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;

public class EntityGroundItem extends Entity {

   private int b;
   private int c;
   private final int d;
   private boolean e;
   public float a;


   public EntityGroundItem(World var1) {
      super(var1);
      this.b = 0;
      this.c = 2400;
      this.d = 10;
      this.e = false;
      this.a = 0.0F;
      this.setSize(0.25F, 0.25F);
      this.a = (float)super.rand.nextInt(360);
   }

   public EntityGroundItem(World var1, double var2, double var4, double var6) {
      this(var1);
      this.setPosition(var2, var4, var6);
   }

   public EntityGroundItem(World var1, double var2, double var4, double var6, ItemStack var8) {
      this(var1);
      this.a(var8);
      this.setPosition(var2, var4, var6);
   }

   protected void entityInit() {
      DataWatcher var10000 = this.getDataWatcher();
      this.getClass();
      var10000.addObjectByDataType(10, 5);
   }

   public void onUpdate() {
      super.onUpdate();
      if(++this.b > this.c) {
         this.setDead();
      }

      if(FMLCommonHandler.instance().getSide() == Side.SERVER && !ServerProxy.e) {
         ItemStack var1 = this.b();
         int var2 = var1.itemID - 256;
         if(var1.getItem() instanceof ItemBlock) {
            this.setDead();
         }

         if(!ItemManager.g.contains(Integer.valueOf(var2)) && !ItemManager.g.contains(Integer.valueOf(var1.itemID))) {
            this.setDead();
         }
      }

      super.prevPosX = super.posX;
      super.prevPosY = super.posY;
      super.prevPosZ = super.posZ;
      super.motionY -= 0.03999999910593033D;
      super.noClip = this.pushOutOfBlocks(super.posX, (super.boundingBox.minY + super.boundingBox.maxY) / 2.0D, super.posZ);
      this.moveEntity(super.motionX, super.motionY, super.motionZ);
      double var7;
      if(this.b % 40 == 0 && !super.worldObj.isRemote) {
         var7 = 2.0D;
         Iterator var3 = super.worldObj.getEntitiesWithinAABB(EntityGroundItem.class, super.boundingBox.expand(var7, 0.0D, var7)).iterator();

         while(var3.hasNext()) {
            EntityGroundItem var4 = (EntityGroundItem)var3.next();
            if(var4 != this && !var4.isDead && !super.isDead) {
               ItemStack var5 = this.b();
               ItemStack var6 = var4.b();
               if(var5.getItem().itemID == var6.getItem().itemID && var5.stackSize < var5.getMaxStackSize() && var5.stackSize + var6.stackSize <= var5.getMaxStackSize()) {
                  var5.stackSize += var6.stackSize;
                  this.b = 0;
                  this.a(var5);
                  var4.setDead();
               }
            }
         }
      }

      var7 = 0.9800000190734863D;
      super.motionX *= var7;
      super.motionY *= 0.9800000190734863D;
      super.motionZ *= var7;
      if(super.onGround) {
         super.motionX = 0.0D;
         super.motionY = 0.0D;
         super.motionZ = 0.0D;
      }

   }

   public boolean canBeCollidedWith() {
      return true;
   }

   public boolean interactFirst(EntityPlayer var1) {
      if(!super.worldObj.isRemote && !this.e && this.b > 10 && this.a(var1)) {
         this.e = true;
         EntityItemPickupEvent var2 = new EntityItemPickupEvent(var1, new EntityItem(super.worldObj, super.posX, super.posY, super.posZ, this.b()));
         if(MinecraftForge.EVENT_BUS.post(var2)) {
            return false;
         } else {
            this.playSound("random.pop", 0.2F, ((super.rand.nextFloat() - super.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            var1.inventory.addItemStackToInventory(this.b());
            this.setDead();
            return true;
         }
      } else {
         return true;
      }
   }

   protected void readEntityFromNBT(NBTTagCompound var1) {
      this.b = var1.getInteger("age");
      NBTTagCompound var2 = var1.getCompoundTag("itemstack");
      this.a(ItemStack.loadItemStackFromNBT(var2));
      DataWatcher var10000 = this.getDataWatcher();
      this.getClass();
      ItemStack var3 = var10000.getWatchableObjectItemStack(10);
      if(var3 == null || var3.stackSize <= 0) {
         this.setDead();
      }

   }

   protected void writeEntityToNBT(NBTTagCompound var1) {
      var1.setInteger("age", this.b);
      if(this.b() != null) {
         var1.setCompoundTag("itemstack", this.b().writeToNBT(new NBTTagCompound()));
      }

   }

   public boolean a(EntityPlayer var1) {
      InventoryPlayer var2 = var1.inventory;
      ItemStack var3 = this.b();

      for(int var4 = 0; var4 < var2.mainInventory.length - 1; ++var4) {
         if(var2.mainInventory[var4] == null || var2.mainInventory[var4].itemID == var3.itemID && var2.mainInventory[var4].stackSize < var2.mainInventory[var4].getMaxStackSize()) {
            return true;
         }
      }

      return false;
   }

   public ItemStack b() {
      DataWatcher var10000 = this.getDataWatcher();
      this.getClass();
      ItemStack var1 = var10000.getWatchableObjectItemStack(10);
      if(var1 == null) {
         if(super.worldObj != null) {
            super.worldObj.getWorldLogAgent().logSevere("Item entity " + super.entityId + " has no item?!");
         }

         return new ItemStack(ItemManager.bK);
      } else {
         return var1;
      }
   }

   public void a(ItemStack var1) {
      DataWatcher var10000 = this.getDataWatcher();
      this.getClass();
      var10000.updateObject(10, var1);
      var10000 = this.getDataWatcher();
      this.getClass();
      var10000.setObjectWatched(10);
   }
}
