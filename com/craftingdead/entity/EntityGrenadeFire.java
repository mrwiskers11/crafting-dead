package com.craftingdead.entity;

import cpw.mods.fml.common.FMLCommonHandler;
import java.util.Random;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.entity.EntityManager;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntityGrenadeFire extends EntityGrenade {

   public boolean j = false;
   private Random aw = new Random();
   public int au = 200;
   public boolean av = false;


   public EntityGrenadeFire(World var1) {
      super(var1);
      super.e = -1;
   }

   public EntityGrenadeFire(World var1, EntityLivingBase var2, double var3, int var5) {
      super(var1, var2, var3, var5);
      super.e = -1;
   }

   public void onUpdate() {
      super.onUpdate();
      if(this.j && this.au-- <= 0) {
         this.setDead();
         byte var1 = 4;

         for(double var2 = (double)(-var1); var2 < (double)var1; ++var2) {
            for(double var4 = (double)(-var1); var4 < (double)var1; ++var4) {
               for(double var6 = (double)(-var1); var6 < (double)var1; ++var6) {
                  int var8 = super.worldObj.getBlockId((int)(super.posX + var2), (int)(super.posY + var4), (int)(super.posZ + var6));
                  if(var8 == 51 && !super.worldObj.isRemote) {
                     super.worldObj.setBlock((int)(super.posX + var2), (int)(super.posY + var4), (int)(super.posZ + var6), 0);
                  }
               }
            }
         }

         if(!super.worldObj.isRemote) {
            this.setDead();
         }

      }
   }

   public void m() {
      byte var1 = 3;

      for(double var2 = (double)(-var1); var2 < (double)var1; ++var2) {
         for(double var4 = -2.0D; var4 < 0.0D; ++var4) {
            for(double var6 = (double)(-var1); var6 < (double)var1; ++var6) {
               int var8 = super.worldObj.getBlockId((int)(super.posX + var2), (int)(super.posY + var4), (int)(super.posZ + var6));
               int var9 = super.worldObj.getBlockId((int)(super.posX + var2), (int)(super.posY + var4 + 1.0D), (int)(super.posZ + var6));
               if(var8 != 0 && var9 == 0 && !super.worldObj.isRemote && this.aw.nextInt(3) == 0) {
                  super.worldObj.setBlock((int)(super.posX + var2), (int)(super.posY + var4 + 1.0D), (int)(super.posZ + var6), Block.fire.blockID);
               }
            }
         }
      }

   }

   public void n() {
      for(double var1 = 0.0D; var1 < 2.0D; var1 += 0.25D) {
         double var3 = var1;

         for(double var5 = -var1; var5 < var3; ++var5) {
            for(double var7 = -var3; var7 < var3; ++var7) {
               for(double var9 = -var3; var9 < var3; ++var9) {
                  double var11 = (-0.5D + this.aw.nextDouble()) / 4.0D;
                  double var13 = (-0.5D + this.aw.nextDouble()) / 4.0D;
                  double var15 = (-0.5D + this.aw.nextDouble()) / 4.0D;
                  EntityManager.a("flame", super.posX + var5, super.posY + var7, super.posZ + var9, 0.0D + var11, 0.0D + var13, 0.0D + var15);
               }
            }
         }
      }

      double var2;
      double var4;
      double var6;
      double var8;
      double var10;
      int var17;
      for(var17 = 0; var17 < 100; ++var17) {
         var2 = this.aw.nextDouble() * 0.2D;
         var4 = this.aw.nextDouble() * 3.141592653589793D * 2.0D;
         var6 = Math.cos(var4) * var2;
         var8 = 0.01D + this.aw.nextDouble() * 0.3D;
         var10 = Math.sin(var4) * var2;
         EntityManager.a("flame", super.posX + var6 * 0.1D, super.posY + 0.1D, super.posZ + var10 * 0.1D, var6, var8, var10);
      }

      for(var17 = 0; var17 < 100; ++var17) {
         var2 = this.aw.nextDouble() * 0.5D;
         var4 = this.aw.nextDouble() * 3.141592653589793D * 2.0D;
         var6 = Math.cos(var4) * var2;
         var8 = 0.01D + this.aw.nextDouble() * 0.1D;
         var10 = Math.sin(var4) * var2;
         EntityManager.a("flame", super.posX + var6 * 0.1D, super.posY + 0.3D, super.posZ + var10 * 0.1D, var6, -var8, var10);
      }

   }

   public void i() {
      super.worldObj.playSoundAtEntity(this, "random.glass", 1.0F, 0.9F);
      this.n();
      if(!super.worldObj.isRemote && !this.av) {
         FMLCommonHandler.instance().getMinecraftServerInstance().worldServers[0].getGameRules().setOrCreateGameRule("doFireTick", "false");
         this.m();
         this.av = true;
      }

      this.j = true;
   }

   public boolean b() {
      return true;
   }

   public void k() {}

   public void writeEntityToNBT(NBTTagCompound var1) {}

   public void readEntityFromNBT(NBTTagCompound var1) {
      this.setDead();
   }
}
