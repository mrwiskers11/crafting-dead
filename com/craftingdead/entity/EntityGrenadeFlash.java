package com.craftingdead.entity;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.network.packets.CDAPacketGrenadeFlash;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class EntityGrenadeFlash extends EntityGrenade {

   public EntityGrenadeFlash(World var1) {
      super(var1);
      this.setSize(0.5F, 0.5F);
      super.yOffset = super.height / 2.0F;
      super.d = 0;
   }

   public EntityGrenadeFlash(World var1, EntityLivingBase var2, double var3, int var5) {
      super(var1, var2, var3, var5);
   }

   public void k() {
      double var1 = 18.0D;
      super.worldObj.playSoundAtEntity(this, "random.explode", 2.0F, 1.5F);
      AxisAlignedBB var3 = super.boundingBox.expand(var1, var1, var1);
      List var4 = super.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, var3);
      if(var4 != null && !var4.isEmpty()) {
         Iterator var5 = var4.iterator();

         while(var5.hasNext()) {
            EntityLivingBase var6 = (EntityLivingBase)var5.next();
            double var7 = (double)this.getDistanceToEntity(var6);
            if(var6 instanceof EntityPlayer && var6.canEntityBeSeen(this) && var7 < var1) {
               EntityPlayer var9 = (EntityPlayer)var6;
               PacketDispatcher.sendPacketToPlayer(CDAPacketGrenadeFlash.a(var7, super.entityId), (Player)var9);
            }
         }
      }

      this.setDead();
   }
}
