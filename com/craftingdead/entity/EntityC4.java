package com.craftingdead.entity;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.event.EventC4Off;
import com.craftingdead.item.ItemManager;

import java.util.ArrayList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class EntityC4 extends EntityGrenade {

   private boolean j = false;
   private int au = 12000;
   private int av = 0;
   private boolean aw = false;
   private String ax = "";
   private boolean ay = false;


   public EntityC4(World var1) {
      super(var1);
      super.e = -1;
   }

   public EntityC4(World var1, EntityLivingBase var2, double var3) {
      super(var1, var2, var3 > 0.800000011920929D?0.800000011920929D:var3, -1);
      this.ax = var2.getEntityName();
      super.e = -1;
   }

   public void onUpdate() {
      super.onUpdate();
      if(!super.worldObj.isRemote && this.j && this.av++ >= this.au) {
         this.setDead();
      }

   }

   public boolean interactFirst(EntityPlayer var1) {
      if(!super.worldObj.isRemote && !this.aw && this.av > 10 && this.a(var1) && var1.inventory.addItemStackToInventory(new ItemStack(ItemManager.eU))) {
         this.aw = true;
         this.playSound("random.pop", 0.2F, ((super.rand.nextFloat() - super.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
         this.setDead();
         return true;
      } else {
         return false;
      }
   }

   public boolean a(EntityPlayer var1) {
      InventoryPlayer var2 = var1.inventory;

      for(int var3 = 0; var3 < var2.mainInventory.length - 1; ++var3) {
         if(var2.mainInventory[var3] == null) {
            return true;
         }
      }

      return false;
   }

   public boolean b() {
      this.j = true;
      return true;
   }

   public String g() {
      return "damage.fallsmall";
   }

   public void a(EntityPlayer var1, String var2) {
      EventC4Off var3 = new EventC4Off(var2, this, var1);
      if(!MinecraftForge.EVENT_BUS.post(var3)) {
         this.f(var1);
      }
   }

   public boolean b(EntityPlayer var1) {
      if(this.ax.equals(var1.username)) {
         EventC4Off var2 = new EventC4Off("touched", this, var1);
         if(MinecraftForge.EVENT_BUS.post(var2)) {
            return false;
         } else {
            this.f(var1);
            return true;
         }
      } else {
         return false;
      }
   }

   private void f(EntityPlayer var1) {
      if(!this.ay) {
         this.ay = true;
         this.a(super.posX, super.posY, super.posZ, 5.0F);
         double var2 = 2.5D;
         ArrayList var4 = (ArrayList)super.worldObj.getEntitiesWithinAABB(EntityC4.class, AxisAlignedBB.getBoundingBox(super.posX - var2, super.posY - var2, super.posZ - var2, super.posX + var2, super.posY + var2, super.posZ + var2));
         if(var4.size() > 0) {
            for(int var5 = 0; var5 < var4.size(); ++var5) {
               EntityC4 var6 = (EntityC4)var4.get(var5);
               if(var6 != this && var6.entityId != super.entityId) {
                  var6.a(var1, "chain");
               }
            }
         }

         this.setDead();
      }

   }

   public boolean canBeCollidedWith() {
      return true;
   }

   public void writeEntityToNBT(NBTTagCompound var1) {
      super.writeEntityToNBT(var1);
      var1.setInteger("life", this.av);
      var1.setString("user", this.ax);
   }

   public void readEntityFromNBT(NBTTagCompound var1) {
      super.readEntityFromNBT(var1);
      this.av = var1.getInteger("life");
      this.ax = var1.getString("user");
   }
}
