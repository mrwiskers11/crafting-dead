package com.craftingdead.entity;

import java.util.Random;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.entity.EntityManager;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityGrenadeSmoke extends EntityGrenade {

   public boolean j = false;
   private Random ax = new Random();
   public int au = 300;
   public double av = 0.2D;
   public double aw = 3.25D;


   public EntityGrenadeSmoke(World var1) {
      super(var1);
      super.e = -1;
   }

   public EntityGrenadeSmoke(World var1, EntityLivingBase var2, double var3, int var5) {
      super(var1, var2, var3, var5);
      super.e = -1;
   }

   public void onUpdate() {
      super.onUpdate();
      if(this.j) {
         if(this.au % 10 == 0 && this.av < this.aw) {
            this.av += 0.25D;
         }

         if(super.worldObj.isRemote && this.au % 6 == 0) {
            for(double var1 = -this.av; var1 < this.av; ++var1) {
               for(double var3 = -this.av; var3 < this.av; ++var3) {
                  for(double var5 = -this.av; var5 < this.av; ++var5) {
                     double var7 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     double var9 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     double var11 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     EntityManager.a("explode", super.posX + var1, super.posY + var3 + 1.0D, super.posZ + var5, 0.0D + var7, 0.0D + var9, 0.0D + var11);
                     var7 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     var9 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     var11 = (-0.5D + this.ax.nextDouble()) / 4.0D;
                     EntityManager.a("largesmoke", super.posX + var1, super.posY + var3 + 1.0D, super.posZ + var5, 0.0D + var7, 0.0D + var9, 0.0D + var11);
                  }
               }
            }
         }

         if(this.au-- <= 0) {
            this.setDead();
         }
      }

   }

   public void j() {
      this.j = true;
   }

   public void k() {}
}
