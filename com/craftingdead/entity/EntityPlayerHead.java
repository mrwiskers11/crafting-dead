package com.craftingdead.entity;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class EntityPlayerHead extends Entity {

   public EntityPlayer a;


   public EntityPlayerHead(World var1) {
      super(var1);
      this.setSize(0.325F, 0.325F);
   }

   protected void entityInit() {
      super.dataWatcher.addObject(21, Integer.valueOf(0));
   }

   public boolean attackEntityFrom(DamageSource var1, float var2) {
      return this.a == null?false:this.a.attackEntityFrom(var1, var2);
   }

   public void onUpdate() {
      double var1 = 1.45D;
      if(!super.worldObj.isRemote) {
         if(this.a != null) {
            if(this.a.isDead) {
               this.setDead();
            }

            super.dataWatcher.updateObject(21, Integer.valueOf(this.a.entityId));
            super.prevPosX = super.posX;
            super.prevPosY = super.posY;
            super.prevPosZ = super.posZ;
            this.setLocationAndAngles(this.a.posX, this.a.posY + var1, this.a.posZ, this.a.rotationYaw, this.a.rotationPitch);
         }
      } else if(this.a == null) {
         Entity var3 = super.worldObj.getEntityByID(super.dataWatcher.getWatchableObjectInt(21));
         if(var3 != null && var3 instanceof EntityPlayer) {
            this.a = (EntityPlayer)var3;
         }
      } else if(this.a != Minecraft.getMinecraft().thePlayer) {
         this.setLocationAndAngles(this.a.posX, this.a.posY + var1, this.a.posZ, this.a.rotationYaw, this.a.rotationPitch);
      }

   }

   public boolean canBeCollidedWith() {
      return this.a == null?false:(super.worldObj.isRemote && Minecraft.getMinecraft().thePlayer.username == this.a.username?false:super.worldObj.isRemote);
   }

   protected void readEntityFromNBT(NBTTagCompound var1) {
      this.setDead();
   }

   protected void writeEntityToNBT(NBTTagCompound var1) {
      this.setDead();
   }
}
