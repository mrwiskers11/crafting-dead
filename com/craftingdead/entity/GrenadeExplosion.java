package com.craftingdead.entity;

import com.craftingdead.entity.EntityManager;
import com.craftingdead.network.packets.CDAPacketGrenadeExplosion;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

public class GrenadeExplosion extends Explosion {

   public boolean i;
   public boolean j = true;
   private World p;
   public double k;
   public double l;
   public double m;
   public Entity n;
   public float o;


   public GrenadeExplosion(World var1, EntityLivingBase var2, double var3, double var5, double var7, float var9) {
      super(var1, var2, var3, var5, var7, var9);
      this.p = var1;
      this.n = var2;
      this.o = var9;
      this.k = var3;
      this.l = var5;
      this.m = var7;
   }

   public void doExplosionA() {
      if(!this.p.isRemote) {
         PacketDispatcher.sendPacketToAllAround(this.k, this.l, this.m, 50.0D, 0, CDAPacketGrenadeExplosion.a(this.k, this.l, this.m, this.o));
         this.p.playSoundEffect(this.k, this.l, this.m, "random.explode", 4.0F, (1.0F + (this.p.rand.nextFloat() - this.p.rand.nextFloat()) * 0.2F) * 0.7F);
         float var1 = this.o;
         this.o *= 2.0F;
         int var2 = MathHelper.floor_double(this.k - (double)this.o - 1.0D);
         int var3 = MathHelper.floor_double(this.k + (double)this.o + 1.0D);
         int var4 = MathHelper.floor_double(this.l - (double)this.o - 1.0D);
         int var11 = MathHelper.floor_double(this.l + (double)this.o + 1.0D);
         int var12 = MathHelper.floor_double(this.m - (double)this.o - 1.0D);
         int var13 = MathHelper.floor_double(this.m + (double)this.o + 1.0D);
         List var14 = this.p.getEntitiesWithinAABBExcludingEntity(this.n, AxisAlignedBB.getAABBPool().getAABB((double)var2, (double)var4, (double)var12, (double)var3, (double)var11, (double)var13));
         Vec3 var15 = this.p.getWorldVec3Pool().getVecFromPool(this.k, this.l, this.m);

         for(int var16 = 0; var16 < var14.size(); ++var16) {
            Entity var17 = (Entity)var14.get(var16);
            double var18 = var17.getDistance(this.k, this.l, this.m) / (double)this.o;
            if(var18 <= 1.0D) {
               double var5 = var17.posX - this.k;
               double var7 = var17.posY + (double)var17.getEyeHeight() - this.l;
               double var9 = var17.posZ - this.m;
               double var20 = (double)MathHelper.sqrt_double(var5 * var5 + var7 * var7 + var9 * var9);
               if(var20 != 0.0D) {
                  double var10000 = var5 / var20;
                  var10000 = var7 / var20;
                  var10000 = var9 / var20;
                  double var22 = (double)this.p.getBlockDensity(var15, var17.boundingBox);
                  double var24 = (1.0D - var18) * var22;
                  var17.attackEntityFrom(DamageSource.setExplosionSource(this), (float)((int)((var24 * var24 + var24) / 2.0D * 8.0D * (double)this.o + 1.0D)));
                  var17.motionX = 0.0D;
                  var17.motionY = 0.0D;
                  var17.motionZ = 0.0D;
               }
            }
         }

         this.o = var1;
      }
   }

   public void doExplosionB(boolean var1) {
      if(this.p.isRemote) {
         if(this.o >= 2.0F && this.j) {
            EntityManager.a("hugeexplosion", this.k, this.l, this.m, 1.0D, 0.0D, 0.0D);
         } else {
            EntityManager.a("largeexplode", this.k, this.l, this.m, 1.0D, 0.0D, 0.0D);
         }
      }

      if(this.j) {
         Iterator var2 = super.affectedBlockPositions.iterator();

         while(var2.hasNext()) {
            ChunkPosition var3 = (ChunkPosition)var2.next();
            int var4 = var3.x;
            int var5 = var3.y;
            int var6 = var3.z;
            if(var1) {
               double var7 = (double)((float)var4 + this.p.rand.nextFloat());
               double var9 = (double)((float)var5 + this.p.rand.nextFloat());
               double var11 = (double)((float)var6 + this.p.rand.nextFloat());
               double var13 = var7 - this.k;
               double var15 = var9 - this.l;
               double var17 = var11 - this.m;
               double var19 = (double)MathHelper.sqrt_double(var13 * var13 + var15 * var15 + var17 * var17);
               var13 /= var19;
               var15 /= var19;
               var17 /= var19;
               double var21 = 0.5D / (var19 / (double)this.o + 0.1D);
               var21 *= (double)(this.p.rand.nextFloat() * this.p.rand.nextFloat() + 0.3F);
               var13 *= var21;
               var15 *= var21;
               var17 *= var21;
               EntityManager.a("explode", (var7 + this.k * 1.0D) / 2.0D, (var9 + this.l * 1.0D) / 2.0D, (var11 + this.m * 1.0D) / 2.0D, var13, var15, var17);
               EntityManager.a("smoke", var7, var9, var11, var13, var15, var17);
            }
         }
      }

   }
}
