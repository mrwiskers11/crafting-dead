package com.craftingdead.entity;

import java.util.Random;

import com.craftingdead.item.IItemAntiDupeHelper;
import com.craftingdead.item.ItemManager;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class EntitySupplyDrop extends Entity implements IInventory {

   public int a = 12000;
   public int b = 0;
   public ItemStack[] c = new ItemStack[54];


   public EntitySupplyDrop(World var1) {
      super(var1);
      this.setSize(2.0F, 1.2F);
   }

   protected void entityInit() {}

   public void onEntityUpdate() {
      super.onEntityUpdate();
      if(this.b++ > this.a) {
         this.setDead();
      }

      if(!super.onGround) {
         super.motionY -= 0.003D;
      }

      this.moveEntity(super.motionX, super.motionY, super.motionZ);
   }

   public boolean interactFirst(EntityPlayer var1) {
      if(!super.worldObj.isRemote && !super.isDead) {
         var1.displayGUIChest(this);
         return true;
      } else {
         return false;
      }
   }

   protected void readEntityFromNBT(NBTTagCompound var1) {
      this.b = var1.getInteger("age");
      NBTTagList var2 = var1.getTagList("Items");
      this.c = new ItemStack[this.getSizeInventory()];

      for(int var3 = 0; var3 < var2.tagCount(); ++var3) {
         NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
         int var5 = var4.getByte("Slot") & 255;
         if(var5 >= 0 && var5 < this.c.length) {
            this.c[var5] = ItemStack.loadItemStackFromNBT(var4);
         }
      }

   }

   protected void writeEntityToNBT(NBTTagCompound var1) {
      var1.setInteger("age", this.b);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.c.length; ++var3) {
         if(this.c[var3] != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.setByte("Slot", (byte)var3);
            this.c[var3].writeToNBT(var4);
            var2.appendTag(var4);
         }
      }

      var1.setTag("Items", var2);
   }

   public AxisAlignedBB getCollisionBox(Entity var1) {
      return var1.boundingBox;
   }

   public AxisAlignedBB getBoundingBox() {
      return super.boundingBox;
   }

   public boolean canBePushed() {
      return false;
   }

   public boolean canBeCollidedWith() {
      return true;
   }

   public int getSizeInventory() {
      return 54;
   }

   public ItemStack getStackInSlot(int var1) {
      return this.c[var1];
   }

   public ItemStack decrStackSize(int var1, int var2) {
      if(this.c[var1] != null) {
         ItemStack var3;
         if(this.c[var1].stackSize <= var2) {
            var3 = this.c[var1];
            this.c[var1] = null;
            this.onInventoryChanged();
            return var3;
         } else {
            var3 = this.c[var1].splitStack(var2);
            if(this.c[var1].stackSize == 0) {
               this.c[var1] = null;
            }

            this.onInventoryChanged();
            return var3;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int var1) {
      if(this.c[var1] != null) {
         ItemStack var2 = this.c[var1];
         this.c[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int var1, ItemStack var2) {
      this.c[var1] = var2;
      if(var2 != null && var2.stackSize > this.getInventoryStackLimit()) {
         var2.stackSize = this.getInventoryStackLimit();
      }

      this.onInventoryChanged();
   }

   public String getInvName() {
      return "Supply Drop";
   }

   public boolean isInvNameLocalized() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public void onInventoryChanged() {}

   public boolean isUseableByPlayer(EntityPlayer var1) {
      return super.isDead?false:var1.getDistanceSqToEntity(this) <= 64.0D;
   }

   public void openChest() {}

   public void closeChest() {}

   public boolean isItemValidForSlot(int var1, ItemStack var2) {
      return false;
   }

   public void a(int var1, String var2) {
      for(int var3 = 0; var3 < var1; ++var3) {
         this.a(var2);
      }

   }

   private void a(String var1) {
      if(var1.equals("medical")) {
         this.h();
      } else if(var1.equals("military")) {
         this.i();
      } else {
         this.i();
      }
   }

   private void h() {
      this.a(ItemManager.bC, 70, 9);
      this.a(ItemManager.cF, 70, 9);
      this.a(ItemManager.gK, 50, 4);
      this.a(ItemManager.gR, 50, 4);
      this.a(ItemManager.cA, 40, 8);
      this.a(ItemManager.gL, 70, 12);
      this.a(ItemManager.gN, 50, 4);
      this.a(ItemManager.gV, 50, 4);
      this.a(ItemManager.gO, 50, 8);
      this.a(ItemManager.gU, 50, 3);
      this.a(ItemManager.gS, 50, 4);
      this.a(ItemManager.ee, 50, 2);
      this.a(ItemManager.ad, 50, 3);
      this.a(ItemManager.ab, 50, 3);
      this.a(ItemManager.ac, 50, 3);
      this.a(ItemManager.gG, 50, 3);
      this.a(ItemManager.gD, 50, 3);
      this.a(ItemManager.gB, 50, 2);
      this.a(ItemManager.gH, 50, 2);
      this.a(ItemManager.s, 40, 4);
      this.a(ItemManager.t, 30, 2);
      this.a(ItemManager.u, 30, 2);
   }

   private void i() {
      this.a(ItemManager.bC, 70, 9);
      this.a(ItemManager.cF, 70, 9);
      this.a(ItemManager.gK, 50, 8);
      this.a(ItemManager.fe, 40, 2);
      this.a(ItemManager.bj, 70, 2);
      this.a(ItemManager.eU, 20, 8);
      this.a(ItemManager.eR, 70, 2);
      this.a(ItemManager.fc, 70, 1);
      this.a(ItemManager.fb, 50, 4);
      this.a(ItemManager.fa, 30, 1);
      this.a(ItemManager.eS, 90, 1);
      this.a(ItemManager.S, 50, 8);
      this.a(ItemManager.ao, 40, 4);
      this.a(ItemManager.ap, 80, 2);
      this.a(ItemManager.ad, 50, 5);
      this.a(ItemManager.ac, 50, 5);
      this.a(ItemManager.ay, 40, 2);
      this.a(ItemManager.aA, 40, 2);
      this.a(ItemManager.aB, 60, 2);
      this.a(ItemManager.V, 50, 3);
      this.a(ItemManager.gF, 50, 3);
      this.a(ItemManager.gE, 50, 3);
      this.a(ItemManager.gB, 50, 2);
      this.a(ItemManager.gH, 50, 2);
      this.a(ItemManager.aG, 40, 4);
      this.a(ItemManager.aL, 30, 3);
      this.a(ItemManager.aJ, 20, 3);
      this.a(ItemManager.dB, 40, 3);
      this.a(ItemManager.dL, 30, 4);
      this.a(ItemManager.dN, 20, 3);
      this.a(ItemManager.dP, 80, 2);
      this.a(ItemManager.dG, 80, 2);
      this.a(ItemManager.ea, 60, 1);
      this.a(ItemManager.eo, 50, 2);
      this.a(ItemManager.m, 45, 3);
      this.a(ItemManager.k, 50, 3);
      this.a(ItemManager.E, 20, 3);
      this.a(ItemManager.s, 40, 7);
      this.a(ItemManager.t, 30, 5);
      this.a(ItemManager.u, 30, 5);
      this.a(ItemManager.w, 50, 5);
      this.a(ItemManager.F, 70, 1);
      this.a(ItemManager.L, 90, 1);
      this.a(ItemManager.N, 20, 1);
      this.a(ItemManager.C, 50, 3);
      this.a(ItemManager.r, 30, 2);
      this.a(ItemManager.gw, 50, 2);
      this.a(ItemManager.gA, 40, 2);
      this.a(ItemManager.gz, 40, 2);
   }

   public void a(Item var1, int var2, int var3) {
      this.a(var1.itemID, var2, var3);
   }

   public void a(int var1, int var2, int var3) {
      Random var4 = new Random();

      for(int var5 = 0; var5 < var3; ++var5) {
         if(var4.nextInt(100) <= var2) {
            int var6 = var4.nextInt(54);
            if(this.getStackInSlot(var6) == null || var4.nextBoolean()) {
               ItemStack var7 = new ItemStack(var1, 1, 0);
               IItemAntiDupeHelper.a(var7);
               this.setInventorySlotContents(var6, var7.copy());
            }
         }
      }

   }
}
