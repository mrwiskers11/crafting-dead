package com.craftingdead.entity;

import com.craftingdead.CraftingDead;
import com.craftingdead.entity.EntitySupplyDrop;
import com.craftingdead.event.EventSupplyDropCalled;
import com.craftingdead.server.ServerProxy;
import com.craftingdead.utils.BlockLocation;
import com.craftingdead.utils.ConfigFile;
import com.f3rullo14.fds.FDSCompound;

import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.gen.ChunkProviderServer;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import net.minecraftforge.common.ForgeChunkManager.Type;

public class SupplyDropManager {

   public ArrayList a = new ArrayList();
   private ConfigFile c;
   public static boolean b = true;
   private int d = 0;
   private int e = 0;
   private int f = 0;
   private int g = 0;
   private BlockLocation h = null;
   private String i;


   public SupplyDropManager() {
      try {
         this.a(new File(CraftingDead.f.k(), "supplydrops.properties"));
      } catch (IOException var2) {
         var2.printStackTrace();
      }

   }

   private void a(File var1) throws IOException {
      this.c = new ConfigFile(var1);
      b = this.c.a("enabled", true);
      this.d = 1200 * this.c.a("time.between.drops", 20, "Time between supply drops spawning");
      this.e = 1200 * this.c.a("time.init", 1, "The time in minutes till a drop actually spawns, once picked");
   }

   public void a(FDSCompound var1) {
      if(this.a.size() > 0) {
         var1.a("size", this.a.size());
         int var2 = this.a.size();

         for(int var3 = 0; var3 < var2; ++var3) {
            BlockLocation var4 = (BlockLocation)this.a.get(var3);
            var1.a("supplyPosX" + var3, var4.b);
            var1.a("supplyPosY" + var3, var4.c);
            var1.a("supplyPosZ" + var3, var4.d);
         }
      }

   }

   public void b(FDSCompound var1) {
      int var2 = var1.b("size");
      this.a.clear();

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = var1.b("supplyPosX" + var3);
         int var5 = var1.b("supplyPosY" + var3);
         int var6 = var1.b("supplyPosZ" + var3);
         BlockLocation var7 = new BlockLocation(var4, var5, var6);
         this.a.add(var7);
      }

   }

   public void a(World var1) {
      if(b) {
         if(!var1.isRemote) {
            if(this.a.size() <= 0) {
               return;
            }

            int var2;
            int var3;
            String var4;
            if(this.h == null) {
               ++this.f;
               if(this.f > this.d) {
                  this.h = this.b();
                  if(this.h != null) {
                     var2 = this.h.b;
                     var3 = this.h.d;
                     var4 = this.a(this.e);
                     Random var5 = new Random();
                     this.i = var5.nextBoolean()?"Medical":"Military";
                     ServerProxy.c(EnumChatFormatting.RED + "[Supply Drop] [" + this.i + "] " + var4 + " remaining. x:" + var2 + " z:" + var3);
                  }

                  this.f = 0;
               }
            } else {
               ++this.g;
               if(this.g == this.e / 2) {
                  var2 = this.h.b;
                  var3 = this.h.d;
                  var4 = this.a(this.e / 2);
                  ServerProxy.c(EnumChatFormatting.RED + "[Supply Drop] [" + this.i + "] " + var4 + " remaining. x:" + var2 + " z:" + var3);
               }

               if(this.g > this.e) {
                  var2 = this.h.b;
                  var3 = this.h.d;
                  ServerProxy.c(EnumChatFormatting.RED + "[Supply Drop] [" + this.i + "] Supply Drop Incoming! x:" + var2 + " z:" + var3);
                  this.b(var1);
                  this.a(var1, this.h, this.i.toLowerCase(), 1);
                  this.g = 0;
                  this.h = null;
               }
            }
         }

      }
   }

   public void a(World var1, BlockLocation var2, String var3, int var4) {
      if(var2 != null && !var1.isRemote) {
         try {
            if(var1.getChunkProvider() instanceof ChunkProviderServer && !((ChunkProviderServer)var1.getChunkProvider()).chunkExists(var2.b, var2.d)) {
               Ticket var5 = ForgeChunkManager.requestTicket(CraftingDead.f, var1, Type.NORMAL);
               ForgeChunkManager.forceChunk(var5, var1.getChunkFromBlockCoords(var2.b, var2.d).getChunkCoordIntPair());
            }
         } catch (Exception var7) {
            var7.printStackTrace();
         }

         EntitySupplyDrop var8 = new EntitySupplyDrop(var1);
         var8.setPosition((double)var2.b, (double)(var2.c + 50), (double)var2.d);
         var8.a(var4, var3);
         EventSupplyDropCalled var6 = new EventSupplyDropCalled(var8);
         MinecraftForge.EVENT_BUS.post(var6);
         var1.spawnEntityInWorld(var8);
      }

   }

   public void b(World var1) {
      ArrayList var2 = (ArrayList)var1.loadedEntityList;

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         if(var2.get(var3) instanceof EntitySupplyDrop) {
            ((Entity)var2.get(var3)).setDead();
         }
      }

   }

   public void a(BlockLocation var1) {
      this.a.add(var1);
   }

   public void a() {
      this.a.clear();
   }

   public BlockLocation b() {
      Random var1 = new Random();
      return this.a.size() > 0?(BlockLocation)this.a.get(var1.nextInt(this.a.size())):null;
   }

   public String a(int var1) {
      int var2 = 0;
      int var3 = 0;

      while(var1 > 0) {
         if(var1 >= 1200) {
            var1 -= 1200;
            ++var2;
         } else if(var1 >= 20) {
            var1 -= 20;
            ++var3;
         } else {
            var1 = 0;
         }
      }

      return var2 + "m " + var3 + "s";
   }

   public void a(FMLServerStartingEvent var1) {
      this.d();
   }

   public void a(FMLServerStoppedEvent var1) {
      this.c();
   }

   public void c() {
      File var1 = new File(CraftingDead.f.k(), "supply.fds");
      FDSCompound var2 = new FDSCompound(var1);
      var2.b();
      this.a(var2);
      var2.c();
   }

   public void d() {
      File var1 = new File(CraftingDead.f.k(), "supply.fds");
      FDSCompound var2 = new FDSCompound(var1);
      if(!var1.exists()) {
         var2.c();
      }

      var2.a();
      this.b(var2);
   }

}
