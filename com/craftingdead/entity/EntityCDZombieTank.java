package com.craftingdead.entity;

import com.craftingdead.entity.EntityCDZombie;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.world.World;

public class EntityCDZombieTank extends EntityCDZombie {

   public EntityCDZombieTank(World var1) {
      super(var1);
   }

   protected void applyEntityAttributes() {
      super.applyEntityAttributes();
      this.getEntityAttribute(SharedMonsterAttributes.followRange).setAttribute(20.0D);
      this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setAttribute(100.0D);
      this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setAttribute(0.19D);
      this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setAttribute(15.0D);
   }

   public int c(Entity var1) {
      return 15;
   }
}
