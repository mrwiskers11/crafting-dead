package com.craftingdead.entity;

import com.craftingdead.event.EventFlamethrowerSetFire;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class EntityFlameThrowerFire extends EntityThrowable {

   private int c = 0;


   public EntityFlameThrowerFire(World var1) {
      super(var1);
      this.setSize(0.25F, 0.25F);
   }

   public EntityFlameThrowerFire(World var1, EntityLivingBase var2) {
      super(var1, var2);
   }

   protected float func_70182_d() {
      return 1.1F;
   }

   public void onUpdate() {
      super.onUpdate();
      if(this.c++ > 10) {
         this.setDead();
      }

   }

   protected void onImpact(MovingObjectPosition var1) {
      if(!super.worldObj.isRemote && var1 != null && var1.typeOfHit == EnumMovingObjectType.ENTITY) {
         Entity var2 = var1.entityHit;
         if(!var2.isImmuneToFire() && var2 instanceof EntityLivingBase) {
            EntityLivingBase var3 = (EntityLivingBase)var2;
            if(!var3.isEntityInvulnerable()) {
               EventFlamethrowerSetFire var4 = new EventFlamethrowerSetFire((EntityPlayer)this.getThrower(), var3, 1, 8.0D);
               if(MinecraftForge.EVENT_BUS.post(var4)) {
                  return;
               }

               if(var4.c > 0) {
                  var3.hurtResistantTime = 0;
                  var3.attackEntityFrom(DamageSource.onFire, (float)var4.c);
               }

               if(var4.d > 0.0D) {
                  var3.setFire((int)var4.d);
               }
            }
         }
      }

      this.setDead();
   }
}
