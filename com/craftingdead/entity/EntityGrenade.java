package com.craftingdead.entity;

import com.craftingdead.entity.GrenadeExplosion;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityGrenade extends EntityThrowable {

   public float c;
   public int d;
   public int e;
   public float f;
   public float g;
   public boolean h;
   private boolean j;
   public EntityLivingBase i;
   private boolean au;


   public EntityGrenade(World var1, double var2, double var4, double var6, float var8, float var9, double var10, int var12) {
      super(var1);
      this.d = 0;
      this.e = 28;
      this.f = 0.0F;
      this.g = 0.25F;
      this.j = true;
      this.au = false;
      this.setSize(0.25F, 0.25F);
      this.setRotation(var8, 0.0F);
      double var13 = (double)(-MathHelper.sin(var8 * 3.141593F / 180.0F));
      double var15 = (double)MathHelper.cos(var8 * 3.141593F / 180.0F);
      super.motionX = var10 * var13 * (double)MathHelper.cos(var9 / 180.0F * 3.141593F);
      super.motionY = -var10 * (double)MathHelper.sin(var9 / 180.0F * 3.141593F);
      super.motionZ = var10 * var15 * (double)MathHelper.cos(var9 / 180.0F * 3.141593F);
      this.setPosition(var2 + var13 * 0.8D, var4, var6 + var15 * 0.8D);
      super.prevPosX = super.posX;
      super.prevPosY = super.posY;
      super.prevPosZ = super.posZ;
      this.e = var12;
   }

   public EntityGrenade(World var1, EntityLivingBase var2, double var3, int var5) {
      this(var1, var2.posX, var2.posY + 1.5D, var2.posZ, var2.rotationYaw, var2.rotationPitch, var3, var5);
      this.i = var2;
   }

   public EntityGrenade(World var1) {
      super(var1);
      this.d = 0;
      this.e = 28;
      this.f = 0.0F;
      this.g = 0.25F;
      this.j = true;
      this.au = false;
      this.setSize(0.25F, 0.25F);
      super.yOffset = super.height / 2.0F;
      this.d = 0;
      this.e = 28;
   }

   public EntityGrenade(World var1, double var2, double var4, double var6) {
      super(var1, var2, var4, var6);
      this.d = 0;
      this.e = 28;
      this.f = 0.0F;
      this.g = 0.25F;
      this.j = true;
      this.au = false;
   }

   public void onUpdate() {
      super.prevPosX = super.posX;
      super.prevPosY = super.posY;
      super.prevPosZ = super.posZ;
      double var1 = super.motionX;
      double var3 = super.motionY;
      double var5 = super.motionZ;
      this.moveEntity(super.motionX, super.motionY, super.motionZ);
      if(super.motionX != var1) {
         this.l();
         super.motionX = (double)(-this.g) * var1;
      }

      if(super.motionY != var3) {
         this.l();
         super.motionY = (double)(-this.g) * var3;
      }

      if(super.motionZ != var5) {
         this.l();
         super.motionZ = (double)(-this.g) * var5;
      } else {
         super.motionY -= 0.04D;
      }

      super.motionX *= 0.9800000190734863D;
      super.motionY *= 0.9800000190734863D;
      super.motionZ *= 0.9800000190734863D;
      if(super.onGround) {
         super.motionX *= 0.699999988079071D;
         super.motionZ *= 0.699999988079071D;
         super.motionY *= 1.699999988079071D;
      }

      if(super.onGround && super.motionX * super.motionX + super.motionY * super.motionY + super.motionZ * super.motionZ < 0.02D) {
         super.motionX = 0.0D;
         super.motionY = 0.0D;
         super.motionZ = 0.0D;
      }

      if(super.onGround && super.motionX == 0.0D && super.motionY == 0.0D && super.motionZ == 0.0D) {
         this.j();
      }

      if(this.au) {
         super.motionX = 0.0D;
         super.motionY = 0.0D;
         super.motionZ = 0.0D;
      }

      if(super.worldObj.isRemote && !super.onGround && super.motionX != 0.0D && super.motionY != 0.0D && super.motionZ != 0.0D) {
         byte var7 = 60;

         for(int var8 = 0; var8 < var7; ++var8) {
            this.f += 0.5F;
         }
      }

      if(this.e != -1 && this.d++ >= this.e) {
         this.k();
      }

   }

   public void i() {}

   public void j() {}

   public void k() {
      if(!this.h) {
         this.h = true;
         if(!super.worldObj.isRemote) {
            this.a(super.posX, super.posY, super.posZ, 5.0F);
         }
      }

      this.setDead();
   }

   public boolean b() {
      return false;
   }

   public String g() {
      return "random.break";
   }

   public void l() {
      if(this.j) {
         this.i();
         this.j = false;
         if(this.b()) {
            this.au = true;
         }
      }

      if(!super.worldObj.isRemote && (super.motionX != 0.0D || super.motionY != 0.0D || super.motionZ != 0.0D)) {
         super.worldObj.playSoundAtEntity(this, this.g(), 1.0F, 1.5F);
      }

   }

   public void a(double var1, double var3, double var5, float var7) {
      GrenadeExplosion var8 = new GrenadeExplosion(super.worldObj, this.i, var1, var3, var5, var7);
      var8.i = false;
      var8.j = true;
      var8.doExplosionA();
   }

   public void writeEntityToNBT(NBTTagCompound var1) {
      super.writeEntityToNBT(var1);
   }

   public void readEntityFromNBT(NBTTagCompound var1) {
      super.readEntityFromNBT(var1);
   }

   public void onCollideWithPlayer(EntityPlayer var1) {}

   protected void onImpact(MovingObjectPosition var1) {}

   public void e(EntityPlayer var1) {}
}
