package com.craftingdead.entity;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.render.RenderTickHandler;
import com.craftingdead.entity.EntityC4;
import com.craftingdead.entity.EntityCDZombie;
import com.craftingdead.entity.EntityCDZombieFast;
import com.craftingdead.entity.EntityCDZombieTank;
import com.craftingdead.entity.EntityCDZombieWeak;
import com.craftingdead.entity.EntityCorpse;
import com.craftingdead.entity.EntityFlameThrowerFire;
import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.entity.EntityGrenadeDecoy;
import com.craftingdead.entity.EntityGrenadeFire;
import com.craftingdead.entity.EntityGrenadeFlash;
import com.craftingdead.entity.EntityGrenadeGas;
import com.craftingdead.entity.EntityGrenadePipeBomb;
import com.craftingdead.entity.EntityGrenadeSmoke;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.entity.EntityPlayerHead;
import com.craftingdead.entity.EntitySupplyDrop;
import com.craftingdead.world.WorldManager;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.EnumCreatureType;

public class EntityManager {

   public void a() {
      EntityRegistry.registerGlobalEntityID(EntityCDZombie.class, "cdzombie", 80, 0, 16777215);
      EntityRegistry.registerGlobalEntityID(EntityCDZombieFast.class, "cdzombiefast", 101, 0, 16777215);
      EntityRegistry.registerGlobalEntityID(EntityCDZombieTank.class, "cdzombietank", 102, 0, 16777215);
      EntityRegistry.registerGlobalEntityID(EntityCDZombieWeak.class, "cdzombieweak", 103, 0, 16777215);
      EntityRegistry.addSpawn(EntityCDZombie.class, 40, 2, 8, EnumCreatureType.monster, WorldManager.f);
      EntityRegistry.addSpawn(EntityCDZombieFast.class, 15, 2, 4, EnumCreatureType.monster, WorldManager.f);
      EntityRegistry.addSpawn(EntityCDZombieTank.class, 5, 2, 4, EnumCreatureType.monster, WorldManager.f);
      EntityRegistry.addSpawn(EntityCDZombieWeak.class, 30, 2, 12, EnumCreatureType.monster, WorldManager.f);
      EntityRegistry.registerModEntity(EntityGrenade.class, "grenade", 81, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityGrenadeFlash.class, "grenadeflash", 82, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityGrenadeDecoy.class, "grenadedecoy", 83, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityGrenadeSmoke.class, "grenadesmoke", 84, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityGrenadeFire.class, "grenadefire", 85, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityCorpse.class, "corpse", 86, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityGroundItem.class, "grounditem", 87, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityPlayerHead.class, "head", 88, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityGrenadePipeBomb.class, "grenadepipebomb", 89, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityC4.class, "c4", 90, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntityFlameThrowerFire.class, "flamethrowerfire", 91, CraftingDead.f, 48, 20, true);
      EntityRegistry.registerModEntity(EntitySupplyDrop.class, "supplydrop", 92, CraftingDead.f, 72, 20, true);
      EntityRegistry.registerModEntity(EntityGrenadeGas.class, "grenadegas", 93, CraftingDead.f, 48, 20, true);
      LanguageRegistry.instance().addStringLocalization("entity.cdzombie.name", "CD Zombie");
      LanguageRegistry.instance().addStringLocalization("entity.cdzombiefast.name", "CD Fast Zombie");
      LanguageRegistry.instance().addStringLocalization("entity.cdzombietank.name", "CD Tank Zombie");
      LanguageRegistry.instance().addStringLocalization("entity.cdzombieweak.name", "CD Weak Zombie");
      LanguageRegistry.instance().addStringLocalization("entity.cdghost.name", "CD Ghost");
      LanguageRegistry.instance().addStringLocalization("entity.cdtrader.name", "CD Trader");
   }

   public static void a(String var0, double var1, double var3, double var5, double var7, double var9, double var11) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT && CraftingDead.l() != null && CraftingDead.l().d() != null) {
         RenderTickHandler.a(var0, var1, var3, var5, var7, var9, var11);
      }

   }
}
