package com.craftingdead;

import com.craftingdead.event.ItemCraftedEvent;
import com.craftingdead.item.IItemCanOpener;
import com.craftingdead.item.ItemChalk;

import cpw.mods.fml.common.ICraftingHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;

public class CraftingHandler implements ICraftingHandler {

   public void onCrafting(EntityPlayer var1, ItemStack var2, IInventory var3) {
      int var4 = 0;
      int var5 = -1;
      ItemStack var6 = null;

      ItemStack var8;
      for(int var7 = 0; var7 < var3.getSizeInventory(); ++var7) {
         if(var3.getStackInSlot(var7) != null) {
            var8 = var3.getStackInSlot(var7);
            if(var8 != null && var8.getItem() instanceof IItemCanOpener) {
               ++var4;
               var5 = var7;
               var6 = var8;
            }
         }
      }

      ItemStack var13;
      if(var6 != null && var4 == 1) {
         var13 = new ItemStack(var6.getItem(), 2, var6.getItemDamage() + 2);
         if(var13.getItemDamage() >= var13.getMaxDamage()) {
            var13 = null;
         }

         var3.setInventorySlotContents(var5, var13);
         if(var13 == null) {
            var3.setInventorySlotContents(var5, (ItemStack)null);
         }
      }

      var13 = null;
      var8 = null;
      int var9 = 0;

      for(int var10 = 0; var10 < var3.getSizeInventory(); ++var10) {
         if(var3.getStackInSlot(var10) != null) {
            ItemStack var11 = var3.getStackInSlot(var10);
            ++var9;
            if(var11.getItem() instanceof ItemChalk) {
               var13 = var11;
            } else {
               var8 = var11;
            }
         }
      }

      if(var2 != null && var13 != null && var9 == 2) {
         String var15 = ((ItemChalk)var13.getItem()).b(var13);
         var2.setTagCompound(var8.getTagCompound());
         var2.setItemDamage(var8.getItemDamage());
         if(var15 != null && var15.length() > 0) {
            var2.setItemName(EnumChatFormatting.RED + "" + EnumChatFormatting.ITALIC + var15);
         } else {
            var2.func_135074_t();
         }
      } else {
         ItemCraftedEvent var14 = new ItemCraftedEvent(var2);

         for(int var16 = 0; var16 < var3.getSizeInventory(); ++var16) {
            if(var3.getStackInSlot(var16) != null) {
               ItemStack var12 = var3.getStackInSlot(var16);
               var14.a.add(var12);
            }
         }

         MinecraftForge.EVENT_BUS.post(var14);
      }
   }

   public void onSmelting(EntityPlayer var1, ItemStack var2) {}
}
