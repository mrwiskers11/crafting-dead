package com.craftingdead.network;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacket {

   public static Packet a() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {}
}
