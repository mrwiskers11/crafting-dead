package com.craftingdead.network;

import com.craftingdead.CraftingDead;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

public class PacketHandlerServer implements IPacketHandler {

   public void onPacketData(INetworkManager var1, Packet250CustomPayload var2, Player var3) {
      String var4 = ((EntityPlayer)var3).username;
      EntityPlayerMP var5 = FMLCommonHandler.instance().getSidedDelegate().getServer().getConfigurationManager().getPlayerForUsername(var4);
      a(var2, var5, var1);
   }

   public static final void a(Packet250CustomPayload var0, EntityPlayer var1, INetworkManager var2) {
      if(var0.channel.equals("cdaNetworking")) {
         NetworkManager var3 = CraftingDead.f.a();

         try {
            DataInputStream var4 = new DataInputStream(new ByteArrayInputStream(var0.data));
            int var5 = var4.read();
            if(var5 == 0) {
               CraftingDead.d.info("Incoming Pakcet processed by SERVER with 0 ID!");
               var4.close();
               return;
            }

            try {
               if(var3.a(var5) != null) {
                  CDAPacket var6 = (CDAPacket)var3.a(var5).getClass().newInstance();
                  var6.a(var4, var1, new Object[0], Side.SERVER);
               }
            } catch (Exception var7) {
               CraftingDead.d.warning("Bad Packet Execution from " + var1.username + ". Maybe Invalid IDs?");
            }

            var4.close();
         } catch (Exception var8) {
            var8.printStackTrace();
         }
      }

   }
}
