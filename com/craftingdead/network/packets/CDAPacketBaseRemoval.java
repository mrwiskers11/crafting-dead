package com.craftingdead.network.packets;

import com.craftingdead.block.BlockBaseCenter;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketBaseRemoval extends CDAPacket {

   public static Packet b() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         var2.write(NetworkManager.a(CDAPacketBaseRemoval.class));
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(!var2.worldObj.isRemote) {
         PlayerData var5 = PlayerDataHandler.b(var2.username);
         if(var5.a(var2)) {
            BlockBaseCenter var6 = (BlockBaseCenter)Block.blocksList[var2.worldObj.getBlockId(var5.z, var5.A, var5.B)];
            var6.b(var2.worldObj, var5.z, var5.A, var5.B, var2);
         }
      }

   }
}
