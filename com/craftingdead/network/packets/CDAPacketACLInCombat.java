package com.craftingdead.network.packets;

import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketACLInCombat extends CDAPacket {

   public static Packet b() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         NetworkManager.b();
         var2.write(NetworkManager.a(CDAPacketACLInCombat.class));
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   @SideOnly(Side.CLIENT)
   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      PlayerData var5 = PlayerDataHandler.b();
      var5.N = var5.O;
   }
}
