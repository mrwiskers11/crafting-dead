package com.craftingdead.network.packets;

import com.craftingdead.CraftingDead;
import com.craftingdead.item.blueprint.BlueprintRecipe;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketBlueprintCraft extends CDAPacket {

   public static Packet a(BlueprintRecipe var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         var3.write(NetworkManager.a(CDAPacketBlueprintCraft.class));
         var3.writeInt(var0.a);
         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return var1;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(var2 instanceof EntityPlayerMP) {
         try {
            int var5 = var1.readInt();
            BlueprintRecipe var6 = CraftingDead.f.c().a(var5);
            if(var6 != null && var6.a(var2)) {
               ItemStack var7 = var6.a().copy();
               var6.b(var2);
               var2.inventory.addItemStackToInventory(var7);
               Random var8 = new Random();
               var2.worldObj.playSoundAtEntity(var2, "random.pop", 0.2F, ((var8.nextFloat() - var8.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            }
         } catch (IOException var9) {
            var9.printStackTrace();
         }
      }

   }
}
