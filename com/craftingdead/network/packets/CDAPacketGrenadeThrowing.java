package com.craftingdead.network.packets;

import com.craftingdead.item.ItemGrenade;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CDAPacketGrenadeThrowing extends CDAPacket {

   public static Packet a(double var0) {
      Packet250CustomPayload var2 = new Packet250CustomPayload();
      var2.channel = "cdaNetworking";
      ByteArrayOutputStream var3 = new ByteArrayOutputStream();
      DataOutputStream var4 = new DataOutputStream(var3);

      try {
         var4.write(NetworkManager.a(CDAPacketGrenadeThrowing.class));
         var4.writeDouble(var0);
         var2.data = var3.toByteArray();
         var2.length = var2.data.length;
         var4.close();
         var3.close();
      } catch (Exception var6) {
         var6.printStackTrace();
      }

      return var2;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         if(var2 instanceof EntityPlayerMP) {
            double var5 = var1.readDouble();
            EntityPlayerMP var7 = (EntityPlayerMP)var2;
            World var8 = var7.worldObj;
            ItemStack var9 = var7.inventory.getCurrentItem();
            if(var9 != null && var9.getItem() != null && var9.getItem() instanceof ItemGrenade) {
               ((ItemGrenade)var9.getItem()).a(var9, var8, var7, var5);
            }
         }
      } catch (IOException var10) {
         var10.printStackTrace();
      }

   }
}
