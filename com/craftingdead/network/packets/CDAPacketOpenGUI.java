package com.craftingdead.network.packets;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketOpenGUI extends CDAPacket {

   public static Packet a(CraftingDeadGui var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         var3.write(NetworkManager.a(CDAPacketOpenGUI.class));
         var3.writeInt(var0.a());
         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return var1;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(var2 instanceof EntityPlayerMP) {
         try {
            int var5 = var1.readInt();
            EntityPlayerMP var6 = (EntityPlayerMP)var2;
            var6.openGui(CraftingDead.f, var5, var2.worldObj, 0, 0, 0);
         } catch (IOException var7) {
            var7.printStackTrace();
         }
      }

   }
}
