package com.craftingdead.network.packets;

import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CDAPacketBaseMemberRemove extends CDAPacket {

   public static Packet a(String var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         var3.write(NetworkManager.a(CDAPacketBaseMemberRemove.class));
         var3.writeUTF(var0);
         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return var1;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         if(var2 instanceof EntityPlayerMP && !var2.worldObj.isRemote) {
            PlayerData var5 = PlayerDataHandler.b(var2.username);
            String var6 = var1.readUTF();
            if(var5.a(var2)) {
               TileEntityBaseCenter var7 = (TileEntityBaseCenter)var2.worldObj.getBlockTileEntity(var5.z, var5.A, var5.B);
               if(var7 != null && var7.f().equals(var5.a)) {
                  var7.c(var6);
                  var2.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "" + var6 + "\'s access to your base has been removed."));
                  var7.g();
               }
            }
         }
      } catch (Exception var8) {
         var8.printStackTrace();
      }

   }
}
