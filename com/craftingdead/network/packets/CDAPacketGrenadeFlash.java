package com.craftingdead.network.packets;

import com.craftingdead.item.ItemHat;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.MathHelper;

public class CDAPacketGrenadeFlash extends CDAPacket {

   public static Packet a(double var0, int var2) {
      Packet250CustomPayload var3 = new Packet250CustomPayload();
      var3.channel = "cdaNetworking";
      ByteArrayOutputStream var4 = new ByteArrayOutputStream();
      DataOutputStream var5 = new DataOutputStream(var4);

      try {
         var5.write(NetworkManager.a(CDAPacketGrenadeFlash.class));
         var5.writeDouble(var0);
         var5.writeInt(var2);
         var3.data = var4.toByteArray();
         var3.length = var3.data.length;
         var5.close();
         var4.close();
      } catch (Exception var7) {
         var7.printStackTrace();
      }

      return var3;
   }

   @SideOnly(Side.CLIENT)
   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         double var5 = var1.readDouble();
         int var7 = var1.readInt();
         PlayerData var8 = PlayerDataHandler.b(var2.username);
         if(this.a(var2.worldObj.getEntityByID(var7), var2)) {
            var8.w += (26.0D - var5) * 0.9D;
         } else {
            var8.w = 2.0999999999999996D;
         }

         if(var8.w > 14.0D) {
            var8.w = 14.0D;
         }

         if(var8.d().a("hat") != null && ((ItemHat)var8.d().a("hat").getItem()).e()) {
            var8.w /= 2.0D;
         }
      } catch (IOException var9) {
         var9.printStackTrace();
      }

   }

   protected boolean a(Entity var1, EntityLivingBase var2) {
      float var3 = var2.rotationYaw;
      float var4 = var2.rotationPitch;
      this.a(var2, var1, 360.0F, 360.0F);
      float var5 = var2.rotationYaw;
      float var6 = var2.rotationPitch;
      var2.rotationYaw = var3;
      var2.rotationPitch = var4;
      float var7 = 60.0F;
      float var8 = 60.0F;
      float var9 = var2.rotationYaw - var7;
      float var10 = var2.rotationYaw + var7;
      float var11 = var2.rotationPitch - var8;
      float var12 = var2.rotationPitch + var8;
      boolean var10000;
      if((var9 >= 0.0F || var5 < var9 + 360.0F && var5 > var10) && (var10 < 360.0F || var5 > var10 - 360.0F && var5 < var9) && (var10 >= 360.0F || var9 < 0.0F || var5 > var10 || var5 < var9)) {
         var10000 = false;
      } else {
         var10000 = true;
      }

      boolean var13 = var5 < var10 && var5 > var9;
      boolean var14 = var11 <= -180.0F && (var6 >= var11 + 360.0F || var6 <= var12) || var12 > 180.0F && (var6 <= var12 - 360.0F || var6 >= var11) || var12 < 180.0F && var11 >= -180.0F && var6 <= var12 && var6 >= var11;
      return var13 && var14 && var2.canEntityBeSeen(var1);
   }

   public void a(EntityLivingBase var1, Entity var2, float var3, float var4) {
      double var5 = var2.posX - var1.posX;
      double var7 = var2.posZ - var1.posZ;
      double var9;
      if(var2 instanceof EntityLivingBase) {
         EntityLivingBase var11 = (EntityLivingBase)var2;
         var9 = var11.posY + (double)var11.getEyeHeight() - (var1.posY + (double)var1.getEyeHeight());
      } else {
         var9 = (var2.boundingBox.minY + var2.boundingBox.maxY) / 2.0D - (var1.posY + (double)var1.getEyeHeight());
      }

      double var15 = (double)MathHelper.sqrt_double(var5 * var5 + var7 * var7);
      float var13 = (float)(Math.atan2(var7, var5) * 180.0D / 3.141592653589793D) - 90.0F;
      float var14 = (float)(-(Math.atan2(var9, var15) * 180.0D / 3.141592653589793D));
      var1.rotationPitch = this.a(var1.rotationPitch, var14, var4);
      var1.rotationYaw = this.a(var1.rotationYaw, var13, var3);
   }

   private float a(float var1, float var2, float var3) {
      float var4 = MathHelper.wrapAngleTo180_float(var2 - var1);
      if(var4 > var3) {
         var4 = var3;
      }

      if(var4 < -var3) {
         var4 = -var3;
      }

      return var1 + var4;
   }
}
