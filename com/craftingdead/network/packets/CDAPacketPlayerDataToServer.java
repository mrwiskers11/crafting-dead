package com.craftingdead.network.packets;

import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketPlayerDataToServer extends CDAPacket {

   public static Packet b() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         var2.write(NetworkManager.a(CDAPacketPlayerDataToServer.class));
         PlayerData var3 = PlayerDataHandler.b(Minecraft.getMinecraft().getSession().getUsername());
         var2.writeBoolean(var3.c);
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         PlayerData var5 = PlayerDataHandler.b(var2.username);
         if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
            var5.c = var1.readBoolean();
         }
      } catch (IOException var6) {
         var6.printStackTrace();
      }

   }
}
