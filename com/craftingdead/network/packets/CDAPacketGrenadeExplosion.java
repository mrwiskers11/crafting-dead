package com.craftingdead.network.packets;

import com.craftingdead.entity.GrenadeExplosion;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketGrenadeExplosion extends CDAPacket {

   public static Packet a(double var0, double var2, double var4, float var6) {
      Packet250CustomPayload var7 = new Packet250CustomPayload();
      var7.channel = "cdaNetworking";
      ByteArrayOutputStream var8 = new ByteArrayOutputStream();
      DataOutputStream var9 = new DataOutputStream(var8);

      try {
         var9.write(NetworkManager.a(CDAPacketGrenadeExplosion.class));
         var9.writeDouble(var0);
         var9.writeDouble(var2);
         var9.writeDouble(var4);
         var9.writeFloat(var6);
         var7.data = var8.toByteArray();
         var7.length = var7.data.length;
         var9.close();
         var8.close();
      } catch (Exception var11) {
         var11.printStackTrace();
      }

      return var7;
   }

   @SideOnly(Side.CLIENT)
   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         double var5 = var1.readDouble();
         double var7 = var1.readDouble();
         double var9 = var1.readDouble();
         float var11 = var1.readFloat();
         this.b(var5, var7, var9, var11);
      } catch (IOException var12) {
         var12.printStackTrace();
      }

   }

   @SideOnly(Side.CLIENT)
   public void b(double var1, double var3, double var5, float var7) {
      GrenadeExplosion var8 = new GrenadeExplosion(Minecraft.getMinecraft().theWorld, (EntityLivingBase)null, var1, var3, var5, var7);
      var8.i = false;
      var8.j = true;
      var8.doExplosionB(true);
   }
}
