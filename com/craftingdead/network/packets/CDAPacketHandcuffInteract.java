package com.craftingdead.network.packets;

import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CDAPacketHandcuffInteract extends CDAPacket {

   public static Packet b() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         var2.write(NetworkManager.a(CDAPacketHandcuffInteract.class));
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(var2 instanceof EntityPlayerMP) {
         PlayerData var5 = PlayerDataHandler.a(var2);
         World var6 = var2.worldObj;
         if(var5.Q) {
            ++var5.R;
            Random var7 = new Random();
            var6.playSoundAtEntity(var2, "random.break", 0.8F, 0.8F + var7.nextFloat() / 2.0F);
         }
      }

   }
}
