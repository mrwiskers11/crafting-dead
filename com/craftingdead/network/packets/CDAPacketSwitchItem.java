package com.craftingdead.network.packets;

import com.craftingdead.item.ItemMeleeWeapon;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketSwitchItem extends CDAPacket {

   public static Packet a(boolean var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         var3.write(NetworkManager.a(CDAPacketSwitchItem.class));
         var3.writeBoolean(var0);
         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return var1;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(var2 instanceof EntityPlayerMP) {
         try {
            boolean var5 = var1.readBoolean();
            EntityPlayerMP var6 = (EntityPlayerMP)var2;
            PlayerData var7 = PlayerDataHandler.b(var6.username);
            ItemStack var8 = var2.inventory.getCurrentItem();
            ItemStack var9 = var7.d().a(var5?"gun":"melee");
            if(var5) {
               if(var8 == null) {
                  if(var9 != null && var9.getItem() instanceof ItemGun) {
                     var6.inventory.setInventorySlotContents(var2.inventory.currentItem, var9);
                     var7.d().setInventorySlotContents(var7.d().b("gun"), (ItemStack)null);
                     return;
                  }
               } else if(var8.getItem() instanceof ItemGun && !var7.b) {
                  if(var9 == null) {
                     var6.inventory.setInventorySlotContents(var2.inventory.currentItem, (ItemStack)null);
                     var7.d().setInventorySlotContents(var7.d().b("gun"), var8);
                     return;
                  }

                  var6.inventory.setInventorySlotContents(var2.inventory.currentItem, var9);
                  var7.d().setInventorySlotContents(var7.d().b("gun"), var8);
                  return;
               }
            } else if(var8 == null) {
               if(var9 != null && var9.getItem() instanceof ItemMeleeWeapon) {
                  var6.inventory.setInventorySlotContents(var2.inventory.currentItem, var9);
                  var7.d().setInventorySlotContents(var7.d().b("melee"), (ItemStack)null);
                  return;
               }
            } else if(var8.getItem() instanceof ItemMeleeWeapon) {
               if(var9 == null) {
                  var6.inventory.setInventorySlotContents(var2.inventory.currentItem, (ItemStack)null);
                  var7.d().setInventorySlotContents(var7.d().b("melee"), var8);
                  return;
               }

               var6.inventory.setInventorySlotContents(var2.inventory.currentItem, var9);
               var7.d().setInventorySlotContents(var7.d().b("melee"), var8);
               return;
            }
         } catch (IOException var10) {
            var10.printStackTrace();
         }
      }

   }
}
