package com.craftingdead.network.packets;

import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.item.potions.PotionManager;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public class CDAPacketPlayerDataToPlayer extends CDAPacket {

   public static Packet a(EntityPlayer var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         var3.write(NetworkManager.a(CDAPacketPlayerDataToPlayer.class));
         PlayerData var4 = PlayerDataHandler.b(var0.username);
         var3.writeUTF(var4.a);
         var3.writeBoolean(var4.c);
         var3.writeInt(var4.e().a());
         var3.writeBoolean(var4.C);
         var3.writeInt(var4.h);
         var3.writeInt(var4.i);
         var3.writeInt(var4.j);
         var3.writeBoolean(var0.isPotionActive(PotionManager.b.id));
         var3.writeBoolean(var0.isPotionActive(PotionManager.a.id));
         var3.writeBoolean(var0.isPotionActive(PotionManager.c.id));
         var3.writeInt(var4.P);
         var3.writeBoolean(var4.Q);
         var3.writeInt(var4.R);
         InventoryCDA var5 = var4.d();
         var3.writeInt(var5.f().size());

         for(int var6 = 0; var6 < var5.getSizeInventory(); ++var6) {
            ItemStack var7 = var5.getStackInSlot(var6);
            boolean var8 = false;
            if(var7 != null) {
               if(var7.getItem() instanceof ItemGun) {
                  var8 = true;
               }

               var3.writeInt(var6);
               var3.writeInt(var7.itemID);
               var3.writeBoolean(var8);
               if(var8) {
                  NBTTagCompound var9 = new NBTTagCompound("itemstack" + var6);
                  var7.writeToNBT(var9);
                  NBTTagCompound.writeNamedTag(var9, var3);
               }
            }
         }

         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var10) {
         var10.printStackTrace();
      }

      return var1;
   }

   @SideOnly(Side.CLIENT)
   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         if(var2.worldObj.isRemote) {
            String var5 = var1.readUTF();
            boolean var6 = var5.equals(Minecraft.getMinecraft().getSession().getUsername());
            PlayerData var7 = PlayerDataHandler.b(var5);
            boolean var8 = var1.readBoolean();
            int var9 = var1.readInt();
            boolean var10 = var1.readBoolean();
            int var11 = var1.readInt();
            int var12 = var1.readInt();
            int var13 = var1.readInt();
            boolean var14 = var1.readBoolean();
            boolean var15 = var1.readBoolean();
            boolean var16 = var1.readBoolean();
            int var17 = var1.readInt();
            boolean var18 = var1.readBoolean();
            int var19 = var1.readInt();
            if(!var6) {
               var7.c = var8;
            }

            var7.e().a(var9);
            var7.C = var10;
            var7.h = var11;
            var7.i = var12;
            var7.j = var13;
            var7.r = var14;
            var7.s = var15;
            var7.t = var16;
            int var20 = var1.readInt();
            if(Minecraft.getMinecraft().isSingleplayer()) {
               return;
            }

            var7.Q = var18;
            var7.R = var19;
            var7.P = var17;
            var7.d().h();

            for(int var21 = 0; var21 < var20; ++var21) {
               int var22 = var1.readInt();
               int var23 = var1.readInt();
               boolean var24 = var1.readBoolean();
               ItemStack var25 = new ItemStack(var23, 1, 0);
               if(var24) {
                  NBTTagCompound var26 = (NBTTagCompound)NBTTagCompound.readNamedTag(var1);
                  var25.readFromNBT(var26);
               }

               var7.d().setInventorySlotContents(var22, var25);
            }
         }
      } catch (IOException var27) {
         var27.printStackTrace();
      }

   }
}
