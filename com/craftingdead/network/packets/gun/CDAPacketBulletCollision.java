package com.craftingdead.network.packets.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.event.EnumBulletCollision;
import com.craftingdead.event.EventBulletCollision;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.network.packets.gun.CDAPacketBulletCollisionClient;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class CDAPacketBulletCollision extends CDAPacket {

   public static Packet a(boolean var0, Object ... var1) {
      Packet250CustomPayload var2 = new Packet250CustomPayload();
      var2.channel = "cdaNetworking";
      ByteArrayOutputStream var3 = new ByteArrayOutputStream();
      DataOutputStream var4 = new DataOutputStream(var3);

      try {
         NetworkManager.b();
         var4.write(NetworkManager.a(CDAPacketBulletCollision.class));
         var4.writeBoolean(var0);
         if(var0) {
            Entity var5 = (Entity)var1[0];
            int var6 = var5.entityId;
            var4.writeInt(var6);
            var4.writeBoolean(((Boolean)var1[1]).booleanValue());
            var4.writeDouble(((Double)var1[2]).doubleValue());
            var4.writeDouble(((Double)var1[3]).doubleValue());
            var4.writeDouble(((Double)var1[4]).doubleValue());
            var4.writeInt(((Integer)var1[5]).intValue());
         } else {
            int var20 = ((Integer)var1[0]).intValue();
            double var21 = ((Double)var1[1]).doubleValue();
            double var8 = ((Double)var1[2]).doubleValue();
            double var10 = ((Double)var1[3]).doubleValue();
            int var12 = ((Integer)var1[4]).intValue();
            double var13 = ((Double)var1[5]).doubleValue();
            double var15 = ((Double)var1[6]).doubleValue();
            double var17 = ((Double)var1[7]).doubleValue();
            var4.writeInt(var20);
            var4.writeDouble(var21);
            var4.writeDouble(var8);
            var4.writeDouble(var10);
            var4.writeInt(var12);
            var4.writeDouble(var13);
            var4.writeDouble(var15);
            var4.writeDouble(var17);
         }

         var2.data = var3.toByteArray();
         var2.length = var2.data.length;
         var4.close();
         var3.close();
      } catch (Exception var19) {
         var19.printStackTrace();
      }

      return var2;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         boolean var5 = var1.readBoolean();
         World var6 = var2.worldObj;
         ItemStack var7 = var2.getCurrentEquippedItem();
         if(var7 != null && var7.getItem() instanceof ItemGun && !var6.isRemote) {
            ItemGun var8 = (ItemGun)var7.getItem();
            if(!var8.A(var7)) {
               return;
            }

            int var9;
            if(var5) {
               var9 = var1.readInt();
               boolean var10 = var1.readBoolean();
               double var11 = var1.readDouble();
               double var13 = var1.readDouble();
               double var15 = var1.readDouble();
               int var17 = var1.readInt();
               Vec3 var18 = Vec3.createVectorHelper(var11, var13, var15);
               Entity var19 = var6.getEntityByID(var9);
               if(var19 != null && !var19.isDead) {
                  if(var19 instanceof EntityLivingBase) {
                     if(var2.getDistanceToEntity(var19) < 100.0F) {
                        CraftingDead.f.b().a(var2, var7, (EntityLivingBase)var19, var10, var18, var6, var17);
                        new CDAPacketBulletCollisionClient();
                        PacketDispatcher.sendPacketToAllAround(var19.posX, var19.posY, var19.posZ, 128.0D, 0, CDAPacketBulletCollisionClient.a(true, new Object[]{var19, Boolean.valueOf(var10), Double.valueOf(var11), Double.valueOf(var13), Double.valueOf(var15)}));
                     }
                  } else {
                     EventBulletCollision var20 = new EventBulletCollision(EnumBulletCollision.c, var2, var7, var6);
                     var20.a(var19);
                     MinecraftForge.EVENT_BUS.post(var20);
                  }
               }
            } else {
               var9 = var1.readInt();
               double var26 = var1.readDouble();
               double var12 = var1.readDouble();
               double var14 = var1.readDouble();
               int var16 = var1.readInt();
               double var27 = var1.readDouble();
               double var28 = var1.readDouble();
               double var21 = var1.readDouble();
               Vec3 var23 = Vec3.createVectorHelper(var27, var28, var21);
               int var24 = var6.getBlockId((int)var26, (int)var12, (int)var14);
               if(var9 == var24 && var9 != 0) {
                  CraftingDead.f.b().a(var2, var7, var6, var26, var12, var14, var9, var16, var23);
                  new CDAPacketBulletCollisionClient();
                  PacketDispatcher.sendPacketToAllAround(var26, var12, var14, 128.0D, 0, CDAPacketBulletCollisionClient.a(false, new Object[]{Integer.valueOf(var9), Double.valueOf(var26), Double.valueOf(var12), Double.valueOf(var14), Integer.valueOf(var16), Double.valueOf(var27), Double.valueOf(var28), Double.valueOf(var21)}));
               }
            }
         }
      } catch (IOException var25) {
         var25.printStackTrace();
      }

   }
}
