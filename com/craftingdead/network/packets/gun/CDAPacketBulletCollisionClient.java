package com.craftingdead.network.packets.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class CDAPacketBulletCollisionClient extends CDAPacket {

   public static Packet a(boolean var0, Object ... var1) {
      Packet250CustomPayload var2 = new Packet250CustomPayload();
      var2.channel = "cdaNetworking";
      ByteArrayOutputStream var3 = new ByteArrayOutputStream();
      DataOutputStream var4 = new DataOutputStream(var3);

      try {
         NetworkManager.b();
         var4.write(NetworkManager.a(CDAPacketBulletCollisionClient.class));
         var4.writeBoolean(var0);
         if(var0) {
            Entity var5 = (Entity)var1[0];
            int var6 = var5.entityId;
            var4.writeInt(var6);
            var4.writeBoolean(((Boolean)var1[1]).booleanValue());
            var4.writeDouble(((Double)var1[2]).doubleValue());
            var4.writeDouble(((Double)var1[3]).doubleValue());
            var4.writeDouble(((Double)var1[4]).doubleValue());
         } else {
            int var20 = ((Integer)var1[0]).intValue();
            double var21 = ((Double)var1[1]).doubleValue();
            double var8 = ((Double)var1[2]).doubleValue();
            double var10 = ((Double)var1[3]).doubleValue();
            int var12 = ((Integer)var1[4]).intValue();
            double var13 = ((Double)var1[5]).doubleValue();
            double var15 = ((Double)var1[6]).doubleValue();
            double var17 = ((Double)var1[7]).doubleValue();
            var4.writeInt(var20);
            var4.writeDouble(var21);
            var4.writeDouble(var8);
            var4.writeDouble(var10);
            var4.writeInt(var12);
            var4.writeDouble(var13);
            var4.writeDouble(var15);
            var4.writeDouble(var17);
         }

         var2.data = var3.toByteArray();
         var2.length = var2.data.length;
         var4.close();
         var3.close();
      } catch (Exception var19) {
         var19.printStackTrace();
      }

      return var2;
   }

   @SideOnly(Side.CLIENT)
   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         boolean var5 = var1.readBoolean();
         World var6 = var2.worldObj;
         if(var6.isRemote) {
            int var7;
            if(var5) {
               var7 = var1.readInt();
               boolean var8 = var1.readBoolean();
               double var9 = var1.readDouble();
               double var11 = var1.readDouble();
               double var13 = var1.readDouble();
               Vec3 var15 = Vec3.createVectorHelper(var9, var11, var13);
               Entity var16 = var6.getEntityByID(var7);
               if(var16 != null && !var16.isDead && var16 instanceof EntityLivingBase) {
                  CraftingDead.l().i().a((EntityLivingBase)var16, var8, var15);
               }
            } else {
               var7 = var1.readInt();
               double var24 = var1.readDouble();
               double var10 = var1.readDouble();
               double var12 = var1.readDouble();
               int var14 = var1.readInt();
               double var25 = var1.readDouble();
               double var17 = var1.readDouble();
               double var19 = var1.readDouble();
               Vec3 var21 = Vec3.createVectorHelper(var25, var17, var19);
               int var22 = var6.getBlockId((int)var24, (int)var10, (int)var12);
               if(var7 == var22 && var7 != 0) {
                  CraftingDead.l().i().a(var6, var24, var10, var12, var7, var14, var21);
               }
            }
         }
      } catch (IOException var23) {
         var23.printStackTrace();
      }

   }
}
