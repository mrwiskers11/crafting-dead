package com.craftingdead.network.packets.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.item.ItemFlameThrower;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CDAPacketFTClient extends CDAPacket {

   public static Packet a(EntityPlayer var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         NetworkManager.b();
         var3.write(NetworkManager.a(CDAPacketFTClient.class));
         var3.writeInt(var0.entityId);
         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return var1;
   }

   @SideOnly(Side.CLIENT)
   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         int var5 = var1.readInt();
         World var6 = var2.worldObj;
         if(var6.isRemote) {
            try {
               if(var6.getEntityByID(var5) != null) {
                  Entity var7 = var6.getEntityByID(var5);
                  if(var7 instanceof EntityPlayer) {
                     EntityPlayer var8 = (EntityPlayer)var7;
                     if(var8.getDistanceToEntity(var2) <= 100.0F && var8.getCurrentEquippedItem() != null && var8.getCurrentEquippedItem().getItem() instanceof ItemFlameThrower) {
                        CraftingDead.l().i().a(var8.getCurrentEquippedItem(), var6, var8);
                     }
                  }
               }
            } catch (Exception var9) {
               ;
            }
         }
      } catch (IOException var10) {
         var10.printStackTrace();
      }

   }
}
