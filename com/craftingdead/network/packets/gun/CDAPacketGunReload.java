package com.craftingdead.network.packets.gun;

import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CDAPacketGunReload extends CDAPacket {

   public static Packet b() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         NetworkManager.b();
         var2.write(NetworkManager.a(CDAPacketGunReload.class));
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(var2 instanceof EntityPlayerMP) {
         EntityPlayerMP var5 = (EntityPlayerMP)var2;
         World var6 = var5.worldObj;
         ItemStack var7 = var5.inventory.getCurrentItem();
         if(var7 != null && var7.getItem() != null && var7.getItem() instanceof ItemGun) {
            ((ItemGun)var7.getItem()).a((EntityPlayer)var5, var6, var7);
         }
      }

   }
}
