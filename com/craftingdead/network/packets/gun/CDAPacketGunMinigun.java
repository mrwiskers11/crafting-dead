package com.craftingdead.network.packets.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.logging.Level;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CDAPacketGunMinigun extends CDAPacket {

   public static Packet a(boolean var0) {
      Packet250CustomPayload var1 = new Packet250CustomPayload();
      var1.channel = "cdaNetworking";
      ByteArrayOutputStream var2 = new ByteArrayOutputStream();
      DataOutputStream var3 = new DataOutputStream(var2);

      try {
         NetworkManager.b();
         var3.write(NetworkManager.a(CDAPacketGunMinigun.class));
         var3.writeBoolean(var0);
         var1.data = var2.toByteArray();
         var1.length = var1.data.length;
         var3.close();
         var2.close();
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      return var1;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      try {
         if(var2 instanceof EntityPlayerMP) {
            EntityPlayerMP var5 = (EntityPlayerMP)var2;
            boolean var6 = var1.readBoolean();
            World var7 = var5.worldObj;
            ItemStack var8 = var5.inventory.getCurrentItem();
            if(var8 != null && var8.getItem() != null && var8.getItem() instanceof ItemGun) {
               ((ItemGun)var8.getItem()).a(var5, var7, var8, var6);
            }
         }
      } catch (Exception var9) {
         CraftingDead.d.log(Level.WARNING, "Minigun barrel failed", var9);
      }

   }
}
