package com.craftingdead.network.packets.gun;

import com.craftingdead.item.ItemFlameThrower;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

public class CDAPacketFTTriggerStop extends CDAPacket {

   public static Packet b() {
      Packet250CustomPayload var0 = new Packet250CustomPayload();
      var0.channel = "cdaNetworking";
      ByteArrayOutputStream var1 = new ByteArrayOutputStream();
      DataOutputStream var2 = new DataOutputStream(var1);

      try {
         NetworkManager.b();
         var2.write(NetworkManager.a(CDAPacketFTTriggerStop.class));
         var0.data = var1.toByteArray();
         var0.length = var0.data.length;
         var2.close();
         var1.close();
      } catch (Exception var4) {
         var4.printStackTrace();
      }

      return var0;
   }

   public void a(DataInputStream var1, EntityPlayer var2, Object[] var3, Side var4) {
      if(var2 instanceof EntityPlayerMP) {
         World var5 = var2.worldObj;
         ItemStack var6 = var2.inventory.getCurrentItem();
         if(var6 != null && var6.getItem() != null && var6.getItem() instanceof ItemFlameThrower) {
            ((ItemFlameThrower)var6.getItem()).e(var6, var5, var2);
         }
      }

   }
}
