package com.craftingdead.network;

import com.craftingdead.CraftingDead;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.NetworkManager;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

public class PacketHandlerClient implements IPacketHandler {

   public void onPacketData(INetworkManager var1, Packet250CustomPayload var2, Player var3) {
      EntityClientPlayerMP var4 = FMLClientHandler.instance().getClient().thePlayer;
      a(var2, var4, var1);
   }

   public static final void a(Packet250CustomPayload var0, EntityPlayer var1, INetworkManager var2) {
      if(var0.channel.equals("cdaNetworking")) {
         NetworkManager var3 = CraftingDead.f.a();

         try {
            DataInputStream var4 = new DataInputStream(new ByteArrayInputStream(var0.data));
            int var5 = var4.read();
            if(var5 == 0) {
               CraftingDead.d.info("Incoming Pakcet processed by client with 0 ID!");
               var4.close();
               return;
            }

            if(var3.a(var5) != null) {
               CDAPacket var6 = (CDAPacket)var3.a(var5).getClass().newInstance();
               var6.a(var4, var1, new Object[0], Side.CLIENT);
            }

            var4.close();
         } catch (Exception var7) {
            var7.printStackTrace();
         }
      }

   }
}
