package com.craftingdead.network;

import com.craftingdead.CraftingDead;
import com.craftingdead.network.CDAPacket;
import com.craftingdead.network.packets.CDAPacketACLInCombat;
import com.craftingdead.network.packets.CDAPacketACLPaused;
import com.craftingdead.network.packets.CDAPacketACLUnpaused;
import com.craftingdead.network.packets.CDAPacketBaseMemberAdd;
import com.craftingdead.network.packets.CDAPacketBaseMemberRemove;
import com.craftingdead.network.packets.CDAPacketBaseRemoval;
import com.craftingdead.network.packets.CDAPacketBlueprintCraft;
import com.craftingdead.network.packets.CDAPacketChalk;
import com.craftingdead.network.packets.CDAPacketGrenadeExplosion;
import com.craftingdead.network.packets.CDAPacketGrenadeFlash;
import com.craftingdead.network.packets.CDAPacketGrenadeThrowing;
import com.craftingdead.network.packets.CDAPacketHandcuffInteract;
import com.craftingdead.network.packets.CDAPacketOpenGUI;
import com.craftingdead.network.packets.CDAPacketPlayerDataToPlayer;
import com.craftingdead.network.packets.CDAPacketPlayerDataToServer;
import com.craftingdead.network.packets.CDAPacketSwitchItem;
import com.craftingdead.network.packets.gun.CDAPacketBulletCollision;
import com.craftingdead.network.packets.gun.CDAPacketBulletCollisionClient;
import com.craftingdead.network.packets.gun.CDAPacketFTClient;
import com.craftingdead.network.packets.gun.CDAPacketFTTrigger;
import com.craftingdead.network.packets.gun.CDAPacketFTTriggerStop;
import com.craftingdead.network.packets.gun.CDAPacketGunMinigun;
import com.craftingdead.network.packets.gun.CDAPacketGunReload;
import com.craftingdead.network.packets.gun.CDAPacketGunTrigger;

import java.util.HashMap;
import java.util.Map;

public class NetworkManager {

   private static int d = 1;
   public static final String a = "cdaNetworking";
   public static Map b = new HashMap();
   public static Map c = new HashMap();


   public void a() {
      this.a(d++, new CDAPacketGrenadeFlash());
      this.a(d++, new CDAPacketGrenadeThrowing());
      this.a(d++, new CDAPacketGrenadeExplosion());
      this.a(d++, new CDAPacketSwitchItem());
      this.a(d++, new CDAPacketPlayerDataToPlayer());
      this.a(d++, new CDAPacketACLPaused());
      this.a(d++, new CDAPacketACLUnpaused());
      this.a(d++, new CDAPacketACLInCombat());
      this.a(d++, new CDAPacketFTTrigger());
      this.a(d++, new CDAPacketFTClient());
      this.a(d++, new CDAPacketFTTriggerStop());
      this.a(d++, new CDAPacketHandcuffInteract());
      this.a(d++, new CDAPacketChalk());
      this.a(d++, new CDAPacketBaseMemberAdd());
      this.a(d++, new CDAPacketBaseMemberRemove());
      this.a(d++, new CDAPacketGunMinigun());
      this.a(d++, new CDAPacketOpenGUI());
      this.a(d++, new CDAPacketBaseRemoval());
      this.a(d++, new CDAPacketBlueprintCraft());
      this.a(d++, new CDAPacketPlayerDataToServer());
      this.a(d++, new CDAPacketBulletCollisionClient());
      this.a(d++, new CDAPacketBulletCollision());
      this.a(d++, new CDAPacketGunReload());
      this.a(d++, new CDAPacketGunTrigger());
   }

   public void a(int var1, CDAPacket var2) {
      b.put(Integer.valueOf(var1), var2);
      c.put(var2.getClass(), Integer.valueOf(var1));
   }

   public static int a(Class var0) {
      return c.containsKey(var0)?((Integer)c.get(var0)).intValue():0;
   }

   public CDAPacket a(int var1) {
      return b.containsKey(Integer.valueOf(var1))?(CDAPacket)b.get(Integer.valueOf(var1)):null;
   }

   public static NetworkManager b() {
      return CraftingDead.f.a();
   }

}
