package com.craftingdead.world;

import com.craftingdead.world.ChunkProviderCD;
import com.craftingdead.world.WorldChunkManagerCD;
import com.craftingdead.world.WorldManager;

import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.WorldChunkManager;
import net.minecraft.world.chunk.IChunkProvider;

public class WorldTypeCD extends WorldType {

   public WorldTypeCD(int var1, String var2) {
      super(var1, var2);
      super.biomesForWorldType = WorldManager.f;
   }

   public WorldChunkManager a(World var1) {
      WorldChunkManagerCD.a.clear();
      BiomeGenBase[] var2 = WorldManager.f;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         BiomeGenBase var5 = var2[var4];
         WorldChunkManagerCD.a.add(var5);
      }

      return new WorldChunkManagerCD(var1);
   }

   public IChunkProvider a(World var1, String var2) {
      return new ChunkProviderCD(var1, var1.getSeed(), var1.getWorldInfo().isMapFeaturesEnabled());
   }

   public int g() {
      return 100;
   }
}
