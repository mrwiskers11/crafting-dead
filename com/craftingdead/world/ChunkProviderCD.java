package com.craftingdead.world;

import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockSand;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.IProgressUpdate;
import net.minecraft.util.MathHelper;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.SpawnerAnimals;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.MapGenBase;
import net.minecraft.world.gen.MapGenCaves;
import net.minecraft.world.gen.MapGenRavine;
import net.minecraft.world.gen.NoiseGeneratorOctaves;
import net.minecraft.world.gen.feature.MapGenScatteredFeature;
import net.minecraft.world.gen.feature.WorldGenDungeons;
import net.minecraft.world.gen.feature.WorldGenLakes;
import net.minecraft.world.gen.structure.MapGenMineshaft;
import net.minecraft.world.gen.structure.MapGenStronghold;
import net.minecraft.world.gen.structure.MapGenVillage;

public class ChunkProviderCD implements IChunkProvider {

   private Random k;
   private NoiseGeneratorOctaves l;
   private NoiseGeneratorOctaves m;
   private NoiseGeneratorOctaves n;
   private NoiseGeneratorOctaves o;
   public NoiseGeneratorOctaves a;
   public NoiseGeneratorOctaves b;
   public NoiseGeneratorOctaves c;
   private World p;
   private final boolean q;
   private double[] r;
   private double[] s = new double[256];
   private MapGenBase t = new MapGenCaves();
   private MapGenStronghold u = new MapGenStronghold();
   private MapGenVillage v = new MapGenVillage();
   private MapGenMineshaft w = new MapGenMineshaft();
   private MapGenScatteredFeature x = new MapGenScatteredFeature();
   private MapGenBase y = new MapGenRavine();
   private BiomeGenBase[] z;
   double[] d;
   double[] e;
   double[] f;
   double[] g;
   double[] h;
   float[] i;
   int[][] j = new int[32][32];


   public ChunkProviderCD(World var1, long var2, boolean var4) {
      this.p = var1;
      this.q = var4;
      this.k = new Random(var2);
      this.l = new NoiseGeneratorOctaves(this.k, 16);
      this.m = new NoiseGeneratorOctaves(this.k, 16);
      this.n = new NoiseGeneratorOctaves(this.k, 8);
      this.o = new NoiseGeneratorOctaves(this.k, 4);
      this.a = new NoiseGeneratorOctaves(this.k, 10);
      this.b = new NoiseGeneratorOctaves(this.k, 16);
      this.c = new NoiseGeneratorOctaves(this.k, 8);
   }

   public void a(int var1, int var2, byte[] var3) {
      byte var4 = 4;
      byte var5 = 16;
      byte var6 = 63;
      int var7 = var4 + 1;
      byte var8 = 17;
      int var9 = var4 + 1;
      this.z = this.p.getWorldChunkManager().getBiomesForGeneration(this.z, var1 * 4 - 2, var2 * 4 - 2, var7 + 5, var9 + 5);
      this.r = this.a(this.r, var1 * var4, 0, var2 * var4, var7, var8, var9);

      for(int var10 = 0; var10 < var4; ++var10) {
         for(int var11 = 0; var11 < var4; ++var11) {
            for(int var12 = 0; var12 < var5; ++var12) {
               double var13 = 0.125D;
               double var15 = this.r[((var10 + 0) * var9 + var11 + 0) * var8 + var12 + 0];
               double var17 = this.r[((var10 + 0) * var9 + var11 + 1) * var8 + var12 + 0];
               double var19 = this.r[((var10 + 1) * var9 + var11 + 0) * var8 + var12 + 0];
               double var21 = this.r[((var10 + 1) * var9 + var11 + 1) * var8 + var12 + 0];
               double var23 = (this.r[((var10 + 0) * var9 + var11 + 0) * var8 + var12 + 1] - var15) * var13;
               double var25 = (this.r[((var10 + 0) * var9 + var11 + 1) * var8 + var12 + 1] - var17) * var13;
               double var27 = (this.r[((var10 + 1) * var9 + var11 + 0) * var8 + var12 + 1] - var19) * var13;
               double var29 = (this.r[((var10 + 1) * var9 + var11 + 1) * var8 + var12 + 1] - var21) * var13;

               for(int var31 = 0; var31 < 8; ++var31) {
                  double var32 = 0.25D;
                  double var34 = var15;
                  double var36 = var17;
                  double var38 = (var19 - var15) * var32;
                  double var40 = (var21 - var17) * var32;

                  for(int var42 = 0; var42 < 4; ++var42) {
                     int var43 = var42 + var10 * 4 << 11 | 0 + var11 * 4 << 7 | var12 * 8 + var31;
                     short var44 = 128;
                     var43 -= var44;
                     double var45 = 0.25D;
                     double var47 = (var36 - var34) * var45;
                     double var49 = var34 - var47;

                     for(int var51 = 0; var51 < 4; ++var51) {
                        if((var49 += var47) > 0.0D) {
                           var3[var43 += var44] = (byte)Block.stone.blockID;
                        } else if(var12 * 8 + var31 < var6) {
                           var3[var43 += var44] = (byte)Block.waterStill.blockID;
                        } else {
                           var3[var43 += var44] = 0;
                        }
                     }

                     var34 += var38;
                     var36 += var40;
                  }

                  var15 += var23;
                  var17 += var25;
                  var19 += var27;
                  var21 += var29;
               }
            }
         }
      }

   }

   public void a(int var1, int var2, byte[] var3, BiomeGenBase[] var4) {
      byte var5 = 63;
      double var6 = 0.03125D;
      this.s = this.o.generateNoiseOctaves(this.s, var1 * 16, var2 * 16, 0, 16, 16, 1, var6 * 2.0D, var6 * 2.0D, var6 * 2.0D);

      for(int var8 = 0; var8 < 16; ++var8) {
         for(int var9 = 0; var9 < 16; ++var9) {
            BiomeGenBase var10 = var4[var9 + var8 * 16];
            float var11 = var10.getFloatTemperature();
            int var12 = (int)(this.s[var8 + var9 * 16] / 3.0D + 3.0D + this.k.nextDouble() * 0.25D);
            int var13 = -1;
            byte var14 = var10.topBlock;
            byte var15 = var10.fillerBlock;

            for(int var16 = 127; var16 >= 0; --var16) {
               int var17 = (var9 * 16 + var8) * 128 + var16;
               if(var16 <= 0 + this.k.nextInt(5)) {
                  var3[var17] = (byte)Block.bedrock.blockID;
               } else {
                  byte var18 = var3[var17];
                  if(var18 == 0) {
                     var13 = -1;
                  } else if(var18 == Block.stone.blockID) {
                     if(var13 == -1) {
                        if(var12 <= 0) {
                           var14 = 0;
                           var15 = (byte)Block.stone.blockID;
                        } else if(var16 >= var5 - 4 && var16 <= var5 + 1) {
                           var14 = var10.topBlock;
                           var15 = var10.fillerBlock;
                        }

                        if(var16 < var5 && var14 == 0) {
                           if(var11 < 0.15F) {
                              var14 = (byte)Block.ice.blockID;
                           } else {
                              var14 = (byte)Block.waterStill.blockID;
                           }
                        }

                        var13 = var12;
                        if(var16 >= var5 - 1) {
                           var3[var17] = var14;
                        } else {
                           var3[var17] = var15;
                        }
                     } else if(var13 > 0) {
                        --var13;
                        var3[var17] = var15;
                        if(var13 == 0 && var15 == Block.sand.blockID) {
                           var13 = this.k.nextInt(4);
                           var15 = (byte)Block.sandStone.blockID;
                        }
                     }
                  }
               }
            }
         }
      }

   }

   public Chunk loadChunk(int var1, int var2) {
      return this.provideChunk(var1, var2);
   }

   public Chunk provideChunk(int var1, int var2) {
      this.k.setSeed((long)var1 * 341873128712L + (long)var2 * 132897987541L);
      byte[] var3 = new byte['\u8000'];
      this.a(var1, var2, var3);
      this.z = this.p.getWorldChunkManager().loadBlockGeneratorData(this.z, var1 * 16, var2 * 16, 16, 16);
      this.a(var1, var2, var3, this.z);
      this.t.generate(this, this.p, var1, var2, var3);
      this.y.generate(this, this.p, var1, var2, var3);
      if(this.q) {
         this.w.generate(this, this.p, var1, var2, var3);
         this.v.generate(this, this.p, var1, var2, var3);
         this.u.generate(this, this.p, var1, var2, var3);
         this.x.generate(this, this.p, var1, var2, var3);
      }

      Chunk var4 = new Chunk(this.p, var3, var1, var2);
      byte[] var5 = var4.getBiomeArray();

      for(int var6 = 0; var6 < var5.length; ++var6) {
         var5[var6] = (byte)this.z[var6].biomeID;
      }

      var4.generateSkylightMap();
      return var4;
   }

   private double[] a(double[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
      if(var1 == null) {
         var1 = new double[var5 * var6 * var7];
      }

      if(this.i == null) {
         this.i = new float[25];

         for(int var8 = -2; var8 <= 2; ++var8) {
            for(int var9 = -2; var9 <= 2; ++var9) {
               float var10 = 10.0F / MathHelper.sqrt_float((float)(var8 * var8 + var9 * var9) + 0.2F);
               this.i[var8 + 2 + (var9 + 2) * 5] = var10;
            }
         }
      }

      double var42 = 684.412D;
      double var43 = 684.412D;
      this.g = this.a.generateNoiseOctaves(this.g, var2, var4, var5, var7, 1.121D, 1.121D, 0.5D);
      this.h = this.b.generateNoiseOctaves(this.h, var2, var4, var5, var7, 200.0D, 200.0D, 0.5D);
      this.d = this.n.generateNoiseOctaves(this.d, var2, var3, var4, var5, var6, var7, var42 / 80.0D, var43 / 160.0D, var42 / 80.0D);
      this.e = this.l.generateNoiseOctaves(this.e, var2, var3, var4, var5, var6, var7, var42, var43, var42);
      this.f = this.m.generateNoiseOctaves(this.f, var2, var3, var4, var5, var6, var7, var42, var43, var42);
      int var12 = 0;
      int var13 = 0;

      for(int var14 = 0; var14 < var5; ++var14) {
         for(int var15 = 0; var15 < var7; ++var15) {
            float var16 = 0.0F;
            float var17 = 0.0F;
            float var18 = 0.0F;
            byte var19 = 2;
            BiomeGenBase var20 = this.z[var14 + 2 + (var15 + 2) * (var5 + 5)];

            for(int var21 = -var19; var21 <= var19; ++var21) {
               for(int var22 = -var19; var22 <= var19; ++var22) {
                  BiomeGenBase var23 = this.z[var14 + var21 + 2 + (var15 + var22 + 2) * (var5 + 5)];
                  float var24 = this.i[var21 + 2 + (var22 + 2) * 5] / (var23.minHeight + 2.0F);
                  if(var23.minHeight > var20.minHeight) {
                     var24 /= 2.0F;
                  }

                  var16 += var23.maxHeight * var24;
                  var17 += var23.minHeight * var24;
                  var18 += var24;
               }
            }

            var16 /= var18;
            var17 /= var18;
            var16 = var16 * 0.9F + 0.1F;
            var17 = (var17 * 4.0F - 1.0F) / 8.0F;
            double var44 = this.h[var13] / 8000.0D;
            if(var44 < 0.0D) {
               var44 = -var44 * 0.3D;
            }

            var44 = var44 * 3.0D - 2.0D;
            if(var44 < 0.0D) {
               var44 /= 2.0D;
               if(var44 < -1.0D) {
                  var44 = -1.0D;
               }

               var44 /= 1.4D;
               var44 /= 2.0D;
            } else {
               if(var44 > 1.0D) {
                  var44 = 1.0D;
               }

               var44 /= 8.0D;
            }

            ++var13;

            for(int var45 = 0; var45 < var6; ++var45) {
               double var46 = (double)var17;
               double var26 = (double)var16;
               var46 += var44 * 0.2D;
               var46 = var46 * (double)var6 / 16.0D;
               double var28 = (double)var6 / 2.0D + var46 * 4.0D;
               double var30 = 0.0D;
               double var32 = ((double)var45 - var28) * 12.0D * 128.0D / 128.0D / var26;
               if(var32 < 0.0D) {
                  var32 *= 4.0D;
               }

               double var34 = this.e[var12] / 512.0D;
               double var36 = this.f[var12] / 512.0D;
               double var38 = (this.d[var12] / 10.0D + 1.0D) / 2.0D;
               if(var38 < 0.0D) {
                  var30 = var34;
               } else if(var38 > 1.0D) {
                  var30 = var36;
               } else {
                  var30 = var34 + (var36 - var34) * var38;
               }

               var30 -= var32;
               if(var45 > var6 - 4) {
                  double var40 = (double)((float)(var45 - (var6 - 4)) / 3.0F);
                  var30 = var30 * (1.0D - var40) + -10.0D * var40;
               }

               var1[var12] = var30;
               ++var12;
            }
         }
      }

      return var1;
   }

   public boolean chunkExists(int var1, int var2) {
      return true;
   }

   public void populate(IChunkProvider var1, int var2, int var3) {
      BlockSand.fallInstantly = true;
      int var4 = var2 * 16;
      int var5 = var3 * 16;
      BiomeGenBase var6 = this.p.getBiomeGenForCoords(var4 + 16, var5 + 16);
      this.k.setSeed(this.p.getSeed());
      long var7 = this.k.nextLong() / 2L * 2L + 1L;
      long var9 = this.k.nextLong() / 2L * 2L + 1L;
      this.k.setSeed((long)var2 * var7 + (long)var3 * var9 ^ this.p.getSeed());
      boolean var11 = false;
      if(this.q) {
         this.w.generateStructuresInChunk(this.p, this.k, var2, var3);
         var11 = this.v.generateStructuresInChunk(this.p, this.k, var2, var3);
         this.u.generateStructuresInChunk(this.p, this.k, var2, var3);
         this.x.generateStructuresInChunk(this.p, this.k, var2, var3);
      }

      int var12;
      int var13;
      int var14;
      if(!var11 && this.k.nextInt(4) == 0) {
         var12 = var4 + this.k.nextInt(16) + 8;
         var13 = this.k.nextInt(128);
         var14 = var5 + this.k.nextInt(16) + 8;
         (new WorldGenLakes(Block.waterStill.blockID)).generate(this.p, this.k, var12, var13, var14);
      }

      if(!var11 && this.k.nextInt(8) == 0) {
         var12 = var4 + this.k.nextInt(16) + 8;
         var13 = this.k.nextInt(this.k.nextInt(120) + 8);
         var14 = var5 + this.k.nextInt(16) + 8;
         if(var13 < 63 || this.k.nextInt(10) == 0) {
            (new WorldGenLakes(Block.lavaStill.blockID)).generate(this.p, this.k, var12, var13, var14);
         }
      }

      for(var12 = 0; var12 < 8; ++var12) {
         var13 = var4 + this.k.nextInt(16) + 8;
         var14 = this.k.nextInt(128);
         int var15 = var5 + this.k.nextInt(16) + 8;
         if((new WorldGenDungeons()).generate(this.p, this.k, var13, var14, var15)) {
            ;
         }
      }

      var6.decorate(this.p, this.k, var4, var5);
      SpawnerAnimals.performWorldGenSpawning(this.p, var6, var4 + 8, var5 + 8, 16, 16, this.k);
      var4 += 8;
      var5 += 8;

      for(var12 = 0; var12 < 16; ++var12) {
         for(var13 = 0; var13 < 16; ++var13) {
            var14 = this.p.getPrecipitationHeight(var4 + var12, var5 + var13);
            if(this.p.isBlockFreezable(var12 + var4, var14 - 1, var13 + var5)) {
               this.p.setBlock(var12 + var4, var14 - 1, var13 + var5, Block.ice.blockID);
            }

            if(this.p.canSnowAt(var12 + var4, var14, var13 + var5)) {
               this.p.setBlock(var12 + var4, var14, var13 + var5, Block.snow.blockID);
            }
         }
      }

      BlockSand.fallInstantly = false;
   }

   public boolean saveChunks(boolean var1, IProgressUpdate var2) {
      return true;
   }

   public boolean a() {
      return false;
   }

   public boolean canSave() {
      return true;
   }

   public String makeString() {
      return "RandomLevelSource";
   }

   public List getPossibleCreatures(EnumCreatureType var1, int var2, int var3, int var4) {
      BiomeGenBase var5 = this.p.getBiomeGenForCoords(var2, var4);
      return var5 == null?null:var5.getSpawnableList(var1);
   }

   public ChunkPosition findClosestStructure(World var1, String var2, int var3, int var4, int var5) {
      return "Stronghold".equals(var2) && this.u != null?this.u.getNearestInstance(var1, var3, var4, var5):null;
   }

   public int getLoadedChunkCount() {
      return 0;
   }

   public void recreateStructures(int var1, int var2) {}

   public boolean unloadQueuedChunks() {
      return false;
   }

   public void saveExtraData() {}
}
