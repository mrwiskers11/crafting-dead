package com.craftingdead.world;

import java.util.Random;

import com.craftingdead.world.WorldGenDeadTree;

import net.minecraft.world.World;

public class WorldGenCD {

   public static void a(World var0, Random var1, int var2, int var3, int var4) {
      if(var1.nextInt(var4) == 0) {
         int var5 = var2 + var1.nextInt(16);
         int var6 = var3 + var1.nextInt(16);
         int var7 = var0.getHeightValue(var5, var6);
         (new WorldGenDeadTree(true)).generate(var0, var1, var5, var7, var6);
      }

   }
}
