package com.craftingdead.world;

import com.craftingdead.CraftingDead;
import com.craftingdead.world.CDChunkCallbackHandler;
import com.craftingdead.world.WorldTypeCD;
import com.craftingdead.world.a.BiomeGenZombieDesert;
import com.craftingdead.world.a.BiomeGenZombieForest;
import com.craftingdead.world.a.BiomeGenZombiePlains;
import com.craftingdead.world.a.BiomeGenZombieSnow;
import com.craftingdead.world.a.BiomeGenZombieWasteland;
import com.craftingdead.world.b.StructureManager;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.ForgeChunkManager;

public class WorldManager {

   public static final BiomeGenBase a = (new BiomeGenZombieForest(28)).setBiomeName("Zombie Forest");
   public static final BiomeGenBase b = (new BiomeGenZombieDesert(29)).setBiomeName("Zombie Desert").setMinMaxHeight(0.2F, 0.2F).setDisableRain().setTemperatureRainfall(2.0F, 0.0F);
   public static final BiomeGenBase c = (new BiomeGenZombiePlains(30)).setBiomeName("Zombie Plains").setMinMaxHeight(0.1F, 0.2F);
   public static final BiomeGenBase d = (new BiomeGenZombieSnow(31)).setBiomeName("Zombie Snow").setMinMaxHeight(0.1F, 0.3F).setEnableSnow().setTemperatureRainfall(0.0F, 0.5F);
   public static final BiomeGenBase e = (new BiomeGenZombieWasteland(32)).setBiomeName("Zombie Wasteland").setColor(13786898).setDisableRain().setTemperatureRainfall(2.0F, 0.0F);
   public static final BiomeGenBase[] f = new BiomeGenBase[]{a, b, c, d, e};
   public static final WorldType g = new WorldTypeCD(14, "CD");
   private StructureManager h;


   public WorldManager() {
      LanguageRegistry.instance().addStringLocalization("generator.CD", "en_US", "Crafting Dead");
      ForgeChunkManager.setForcedChunkLoadingCallback(CraftingDead.f, new CDChunkCallbackHandler());
      this.h = new StructureManager();
   }

   public StructureManager a() {
      return this.h;
   }

}
