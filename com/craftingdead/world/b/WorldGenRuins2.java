package com.craftingdead.world.b;

import java.util.Random;

import com.craftingdead.world.b.IStructure;
import com.craftingdead.world.b.StructureManager;

import net.minecraft.world.World;

public class WorldGenRuins2 implements IStructure {

   public void a(World var1, Random var2, int var3, int var4, int var5, String ... var6) {
      byte var7 = 9;
      byte var8 = 4;
      if(var1.getBlockId(var3, var4 - var8, var5) != 0 && var1.getBlockId(var3 + var7, var4 - var8, var5 + var7) != 0) {
         int var10;
         int var11;
         int var12;
         for(int var9 = 0; var9 <= var7; ++var9) {
            for(var10 = 0; var10 <= var7; ++var10) {
               for(var11 = 0; var11 < var8; ++var11) {
                  var12 = var1.getBlockId(var3 + var9, var4 - var11, var5 + var10);
                  if(var12 == 0 || var12 == 8) {
                     var1.setBlock(var3 + var9, var4 - var11, var5 + var10, 3);
                  }
               }
            }
         }

         var1.setBlock(var3 + 0, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 2, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 3, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 4, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 5, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 6, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 7, 2);
         var1.setBlock(var3 + 0, var4 + 0, var5 + 8, 2);
         var1.setBlockMetadataWithNotify(var3 + 0, var4 + 1, var5 + 4, 44, 3);
         var1.setBlockMetadataWithNotify(var3 + 0, var4 + 1, var5 + 5, 44, 3);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 2, 2);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 3, 3);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 4, 3);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 5, 3);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 7, 2);
         var1.setBlock(var3 + 1, var4 + 0, var5 + 8, 2);
         var1.setBlock(var3 + 1, var4 + 1, var5 + 1, 7);
         var1.setBlockMetadataWithNotify(var3 + 1, var4 + 1, var5 + 2, 44, 3);
         var1.setBlock(var3 + 1, var4 + 1, var5 + 3, 4);
         var1.setBlock(var3 + 1, var4 + 1, var5 + 4, 1);
         var1.setBlock(var3 + 1, var4 + 1, var5 + 5, 1);
         var1.setBlock(var3 + 1, var4 + 1, var5 + 6, 4);
         var1.setBlock(var3 + 1, var4 + 2, var5 + 4, 48);
         var1.setBlock(var3 + 1, var4 + 2, var5 + 6, 1);
         var1.setBlock(var3 + 1, var4 + 3, var5 + 4, 48);
         var1.setBlock(var3 + 1, var4 + 3, var5 + 5, 48);
         var1.setBlock(var3 + 1, var4 + 3, var5 + 6, 1);
         var1.setBlockMetadataWithNotify(var3 + 1, var4 + 4, var5 + 5, 44, 3);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 2, 3);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 3, 3);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 4, 48);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 5, 48);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 7, 3);
         var1.setBlock(var3 + 2, var4 + 0, var5 + 8, 2);
         var1.setBlockMetadataWithNotify(var3 + 2, var4 + 1, var5 + 1, 44, 3);
         var1.setBlock(var3 + 2, var4 + 1, var5 + 2, 5);
         var1.setBlock(var3 + 2, var4 + 1, var5 + 3, 1);
         var1.setBlock(var3 + 2, var4 + 1, var5 + 4, 7);
         var1.setBlock(var3 + 2, var4 + 1, var5 + 6, 1);
         var1.setBlock(var3 + 2, var4 + 1, var5 + 7, 1);
         var1.setBlock(var3 + 2, var4 + 2, var5 + 2, 5);
         var1.setBlock(var3 + 2, var4 + 2, var5 + 3, 1);
         var1.setBlock(var3 + 2, var4 + 2, var5 + 6, 1);
         var1.setBlock(var3 + 2, var4 + 2, var5 + 7, 1);
         var1.setBlockMetadataWithNotify(var3 + 2, var4 + 3, var5 + 2, 44, 3);
         var1.setBlock(var3 + 2, var4 + 3, var5 + 3, 1);
         var1.setBlock(var3 + 2, var4 + 3, var5 + 6, 1);
         var1.setBlock(var3 + 2, var4 + 3, var5 + 7, 1);
         var1.setBlock(var3 + 2, var4 + 4, var5 + 3, 4);
         var1.setBlock(var3 + 2, var4 + 4, var5 + 4, 4);
         var1.setBlock(var3 + 2, var4 + 4, var5 + 5, 4);
         var1.setBlock(var3 + 2, var4 + 4, var5 + 6, 1);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 2, 3);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 3, 4);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 4, 48);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 5, 4);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 7, 2);
         var1.setBlock(var3 + 3, var4 + 0, var5 + 8, 2);
         var1.setBlock(var3 + 3, var4 + 1, var5 + 2, 1);
         var1.setBlock(var3 + 3, var4 + 1, var5 + 5, 7);
         var1.setBlock(var3 + 3, var4 + 1, var5 + 6, 4);
         var1.setBlock(var3 + 3, var4 + 2, var5 + 6, 1);
         var1.setBlock(var3 + 3, var4 + 3, var5 + 2, 1);
         var1.setBlockMetadataWithNotify(var3 + 3, var4 + 3, var5 + 6, 44, 3);
         var1.setBlock(var3 + 3, var4 + 4, var5 + 2, 4);
         var1.setBlock(var3 + 3, var4 + 4, var5 + 3, 4);
         var1.setBlock(var3 + 3, var4 + 4, var5 + 4, 48);
         var1.setBlock(var3 + 3, var4 + 4, var5 + 5, 1);
         var1.setBlockMetadataWithNotify(var3 + 3, var4 + 5, var5 + 3, 44, 3);
         var1.setBlock(var3 + 3, var4 + 5, var5 + 4, 7);
         var1.setBlockMetadataWithNotify(var3 + 3, var4 + 5, var5 + 5, 44, 3);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 1, 3);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 2, 3);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 3, 4);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 4, 4);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 5, 4);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 7, 3);
         var1.setBlock(var3 + 4, var4 + 0, var5 + 8, 2);
         var1.setBlock(var3 + 4, var4 + 1, var5 + 1, 1);
         var1.setBlock(var3 + 4, var4 + 1, var5 + 2, 4);
         var1.setBlock(var3 + 4, var4 + 1, var5 + 6, 4);
         var1.setBlock(var3 + 4, var4 + 1, var5 + 7, 48);
         var1.setBlock(var3 + 4, var4 + 2, var5 + 1, 48);
         var1.setBlock(var3 + 4, var4 + 2, var5 + 6, 4);
         var1.setBlock(var3 + 4, var4 + 3, var5 + 2, 1);
         var1.setBlock(var3 + 4, var4 + 3, var5 + 6, 1);
         var1.setBlockMetadataWithNotify(var3 + 4, var4 + 4, var5 + 3, 44, 3);
         var1.setBlock(var3 + 4, var4 + 4, var5 + 4, 1);
         var1.setBlock(var3 + 4, var4 + 4, var5 + 5, 1);
         var1.setBlockMetadataWithNotify(var3 + 4, var4 + 4, var5 + 6, 44, 3);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 2, 4);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 3, 4);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 4, 48);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 5, 48);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 7, 3);
         var1.setBlock(var3 + 5, var4 + 0, var5 + 8, 2);
         var1.setBlock(var3 + 5, var4 + 1, var5 + 2, 4);
         var1.setBlock(var3 + 5, var4 + 1, var5 + 3, 7);
         var1.setBlock(var3 + 5, var4 + 1, var5 + 6, 4);
         var1.setBlock(var3 + 5, var4 + 1, var5 + 7, 4);
         var1.setBlock(var3 + 5, var4 + 2, var5 + 2, 4);
         var1.setBlock(var3 + 5, var4 + 2, var5 + 6, 48);
         var1.setBlock(var3 + 5, var4 + 3, var5 + 2, 4);
         var1.setBlock(var3 + 5, var4 + 3, var5 + 6, 48);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 2, 3);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 3, 3);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 4, 4);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 5, 48);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 7, 2);
         var1.setBlock(var3 + 6, var4 + 0, var5 + 8, 2);
         var1.setBlock(var3 + 6, var4 + 1, var5 + 2, 5);
         var1.setBlock(var3 + 6, var4 + 1, var5 + 3, 1);
         var1.setBlock(var3 + 6, var4 + 1, var5 + 6, 1);
         var1.setBlock(var3 + 6, var4 + 2, var5 + 2, 5);
         var1.setBlock(var3 + 6, var4 + 2, var5 + 3, 4);
         var1.setBlock(var3 + 6, var4 + 2, var5 + 6, 1);
         var1.setBlock(var3 + 6, var4 + 3, var5 + 2, 5);
         var1.setBlock(var3 + 6, var4 + 3, var5 + 3, 4);
         var1.setBlock(var3 + 6, var4 + 4, var5 + 2, 4);
         var1.setBlockMetadataWithNotify(var3 + 6, var4 + 4, var5 + 3, 44, 3);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 1, 3);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 2, 3);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 3, 3);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 4, 3);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 5, 48);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 6, 3);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 7, 2);
         var1.setBlock(var3 + 7, var4 + 0, var5 + 8, 2);
         var1.setBlock(var3 + 7, var4 + 1, var5 + 1, 48);
         var1.setBlock(var3 + 7, var4 + 1, var5 + 2, 7);
         var1.setBlock(var3 + 7, var4 + 1, var5 + 3, 5);
         var1.setBlock(var3 + 7, var4 + 1, var5 + 4, 1);
         var1.setBlock(var3 + 7, var4 + 1, var5 + 6, 4);
         var1.setBlockMetadataWithNotify(var3 + 7, var4 + 1, var5 + 7, 44, 3);
         var1.setBlockMetadataWithNotify(var3 + 7, var4 + 2, var5 + 1, 44, 3);
         var1.setBlock(var3 + 7, var4 + 2, var5 + 3, 5);
         var1.setBlock(var3 + 7, var4 + 2, var5 + 4, 1);
         var1.setBlock(var3 + 7, var4 + 3, var5 + 3, 5);
         var1.setBlockMetadataWithNotify(var3 + 7, var4 + 3, var5 + 4, 44, 3);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 0, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 1, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 2, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 3, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 4, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 5, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 6, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 7, 2);
         var1.setBlock(var3 + 8, var4 + 0, var5 + 8, 2);
         byte var17 = 12;

         for(var10 = 0; var10 < var17; ++var10) {
            for(var11 = 0; var11 < var17; ++var11) {
               for(var12 = 0; var12 < var17; ++var12) {
                  int var13 = var3 + var10;
                  int var14 = var4 + var11;
                  int var15 = var5 + var12;
                  int var16 = var1.getBlockId(var13, var14, var15);
                  if(var16 != 0 && var16 == 7) {
                     StructureManager.a(var1, var13, var14, var15);
                  }
               }
            }
         }

      }
   }
}
