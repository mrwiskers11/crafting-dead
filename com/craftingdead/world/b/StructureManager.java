package com.craftingdead.world.b;

import com.craftingdead.block.BlockManager;
import com.craftingdead.world.b.IStructure;
import com.craftingdead.world.b.WorldGenHouse;
import com.craftingdead.world.b.WorldGenPrison;
import com.craftingdead.world.b.WorldGenRuins;
import com.craftingdead.world.b.WorldGenRuins2;
import com.craftingdead.world.b.WorldGenTower;

import java.util.HashMap;
import java.util.Random;
import net.minecraft.world.World;

public class StructureManager {

   private static HashMap a = new HashMap();


   public StructureManager() {
      a("ruins", WorldGenRuins.class);
      a("tower", WorldGenTower.class);
      a("prison", WorldGenPrison.class);
      a("ruin2", WorldGenRuins2.class);
      a("building1", WorldGenHouse.class);
   }

   public static void a(String var0, World var1, int var2, int var3, int var4, String ... var5) {
      if(a.containsKey(var0.toLowerCase())) {
         try {
            IStructure var6 = (IStructure)((Class)a.get(var0.toLowerCase())).newInstance();
            Random var7 = new Random();
            if(var7.nextInt(var2) == 0) {
               int var8 = var3 + var7.nextInt(16);
               int var9 = var4 + var7.nextInt(16);
               int var10 = var1.getHeightValue(var8, var9);
               var6.a(var1, var7, var8, var10 - 1, var9, var5);
            }
         } catch (InstantiationException var11) {
            var11.printStackTrace();
         } catch (IllegalAccessException var12) {
            var12.printStackTrace();
         }
      }

   }

   public static void a(World var0, int var1, int var2, int var3) {
      Random var4 = new Random();
      int[] var5 = new int[]{BlockManager.D.blockID, BlockManager.E.blockID, BlockManager.G.blockID, BlockManager.F.blockID, BlockManager.H.blockID};
      var0.setBlock(var1, var2, var3, var5[var4.nextInt(var5.length)]);
   }

   public static void a(String var0, Class var1) {
      a.put(var0, var1);
   }

}
