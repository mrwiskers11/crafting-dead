package com.craftingdead.world.a;

import java.util.Random;

import com.craftingdead.world.a.BiomeGenZombie;
import com.craftingdead.world.b.StructureManager;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenTaiga1;
import net.minecraft.world.gen.feature.WorldGenTaiga2;
import net.minecraft.world.gen.feature.WorldGenerator;

public class BiomeGenZombieForest extends BiomeGenZombie {

   public BiomeGenZombieForest(int var1) {
      super(var1);
      super.spawnableCreatureList.clear();
      super.spawnableMonsterList.clear();
      super.theBiomeDecorator.treesPerChunk = 4;
      super.theBiomeDecorator.grassPerChunk = 5;
      super.theBiomeDecorator.flowersPerChunk = 0;
      super.theBiomeDecorator.generateLakes = false;
      super.topBlock = (byte)Block.grass.blockID;
      super.fillerBlock = (byte)Block.dirt.blockID;
      super.maxHeight = 0.12F;
      super.minHeight = 0.11F;
      super.waterColorMultiplier = '\uc99e';
   }

   public WorldGenerator getRandomWorldGenForTrees(Random var1) {
      return (WorldGenerator)(var1.nextInt(14) == 1?new WorldGenTaiga1():new WorldGenTaiga2(false));
   }

   public void decorate(World var1, Random var2, int var3, int var4) {
      super.decorate(var1, var2, var3, var4);
      StructureManager.a("tower", var1, 100, var3, var4, new String[0]);
      StructureManager.a("prison", var1, 250, var3, var4, new String[0]);
      StructureManager.a("ruin2", var1, 60, var3, var4, new String[0]);
   }
}
