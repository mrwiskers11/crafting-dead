package com.craftingdead.world.a;

import java.util.Random;

import com.craftingdead.world.WorldGenCD;
import com.craftingdead.world.a.BiomeGenZombie;
import com.craftingdead.world.b.StructureManager;

import net.minecraft.world.World;

public class BiomeGenZombieWasteland extends BiomeGenZombie {

   public BiomeGenZombieWasteland(int var1) {
      super(var1);
      super.theBiomeDecorator.deadBushPerChunk = 2;
      super.theBiomeDecorator.generateLakes = false;
      super.theBiomeDecorator.treesPerChunk = -999;
      super.spawnableCreatureList.clear();
      super.spawnableMonsterList.clear();
   }

   public void decorate(World var1, Random var2, int var3, int var4) {
      super.decorate(var1, var2, var3, var4);
      StructureManager.a("tower", var1, 50, var3, var4, new String[0]);
      StructureManager.a("prison", var1, 150, var3, var4, new String[0]);
      StructureManager.a("building1", var1, 90, var3, var4, new String[0]);
      StructureManager.a("ruin2", var1, 20, var3, var4, new String[0]);
      WorldGenCD.a(var1, var2, var3, var4, 2);
      WorldGenCD.a(var1, var2, var3, var4, 2);
   }
}
