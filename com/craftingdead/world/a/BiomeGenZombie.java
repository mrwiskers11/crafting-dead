package com.craftingdead.world.a;

import java.util.Random;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;

public class BiomeGenZombie extends BiomeGenBase {

   public BiomeGenZombie(int var1) {
      super(var1);
   }

   public void decorate(World var1, Random var2, int var3, int var4) {
      super.decorate(var1, var2, var3, var4);
   }

   public float getSpawningChance() {
      return 0.15F;
   }
}
