package com.craftingdead.world.a;

import java.util.Random;

import com.craftingdead.world.a.BiomeGenZombie;
import com.craftingdead.world.b.StructureManager;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class BiomeGenZombieDesert extends BiomeGenZombie {

   public BiomeGenZombieDesert(int var1) {
      super(var1);
      super.topBlock = (byte)Block.sand.blockID;
      super.fillerBlock = (byte)Block.sand.blockID;
      super.theBiomeDecorator.treesPerChunk = -999;
      super.theBiomeDecorator.deadBushPerChunk = 0;
      super.theBiomeDecorator.reedsPerChunk = 0;
      super.theBiomeDecorator.cactiPerChunk = 0;
      super.spawnableCreatureList.clear();
      super.spawnableMonsterList.clear();
   }

   public void decorate(World var1, Random var2, int var3, int var4) {
      super.decorate(var1, var2, var3, var4);
      StructureManager.a("ruins", var1, 40, var3, var4, new String[0]);
      StructureManager.a("tower", var1, 70, var3, var4, new String[]{"sand"});
      StructureManager.a("prison", var1, 160, var3, var4, new String[0]);
   }
}
