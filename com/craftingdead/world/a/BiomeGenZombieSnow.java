package com.craftingdead.world.a;

import java.util.Random;

import com.craftingdead.world.a.BiomeGenZombie;
import com.craftingdead.world.b.StructureManager;

import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenTaiga1;
import net.minecraft.world.gen.feature.WorldGenTaiga2;
import net.minecraft.world.gen.feature.WorldGenerator;

public class BiomeGenZombieSnow extends BiomeGenZombie {

   public BiomeGenZombieSnow(int var1) {
      super(var1);
      super.spawnableCreatureList.clear();
      super.spawnableMonsterList.clear();
   }

   public WorldGenerator getRandomWorldGenForTrees(Random var1) {
      return (WorldGenerator)(var1.nextInt(18) == 1?new WorldGenTaiga1():new WorldGenTaiga2(false));
   }

   public void decorate(World var1, Random var2, int var3, int var4) {
      super.decorate(var1, var2, var3, var4);
      StructureManager.a("tower", var1, 80, var3, var4, new String[0]);
      StructureManager.a("prison", var1, 190, var3, var4, new String[0]);
      StructureManager.a("building1", var1, 110, var3, var4, new String[0]);
   }
}
