package com.craftingdead.item;

import com.craftingdead.block.BlockManager;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class CreativeTabCDDecorative extends CreativeTabs {

   public CreativeTabCDDecorative(String var1) {
      super(var1);
   }

   public ItemStack getIconItemStack() {
      return new ItemStack(BlockManager.V);
   }
}
