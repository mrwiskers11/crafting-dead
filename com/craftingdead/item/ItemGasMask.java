package com.craftingdead.item;

import com.craftingdead.item.ItemHat;
import com.craftingdead.item.ItemManager;

public class ItemGasMask extends ItemHat {

   public ItemGasMask(int var1) {
      super(var1);
      this.setCreativeTab(ItemManager.e);
   }
}
