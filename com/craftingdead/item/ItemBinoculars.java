package com.craftingdead.item;

import com.craftingdead.item.ItemCD;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.lwjgl.input.Mouse;

public class ItemBinoculars extends ItemCD {

   private static boolean d;
   private static boolean cB = d;


   public ItemBinoculars(int var1) {
      super(var1);
      this.f("Right click to Look into Binoculars");
   }

   public synchronized void a(World var1, EntityPlayer var2, ItemStack var3) {
      PlayerData var4 = PlayerDataHandler.a(var2);
      cB = d;
      d = Mouse.isButtonDown(Mouse.getButtonIndex("BUTTON1"));
      if(var2.getCurrentEquippedItem().getItem() instanceof ItemBinoculars) {
         if(var4.Q) {
            var4.d = false;
            return;
         }

         if(var2.isSprinting()) {
            var4.d = false;
            return;
         }

         if(d && !cB) {
            if(!var4.d) {
               var4.d = true;
               return;
            }

            if(var4.d) {
               var4.d = false;
               return;
            }
         }
      }

   }

}
