package com.craftingdead.item;

import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.network.packets.CDAPacketChalk;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.HashMap;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.EnumChatFormatting;

public class GuiChalk extends GuiScreen {

   public GuiTextField chalkText;
   private static HashMap bannedWords = new HashMap();


   public void initGui() {
      this.addNoNoWord("fuck", EnumChatFormatting.YELLOW + "Ducks <3");
      this.addNoNoWord("nigg", "Lemonade <3");
      this.addNoNoWord("cunt", "Punt");
      this.addNoNoWord("shit", "Really?");
      this.addNoNoWord("pussy", EnumChatFormatting.GOLD + "Cat");
      this.addNoNoWord("bitch", EnumChatFormatting.BLUE + "Female Dog");
      this.addNoNoWord("dick", "Pen Island");
      this.addNoNoWord("penis", EnumChatFormatting.AQUA + "Pen15");
      this.chalkText = new GuiTextField(super.fontRenderer, super.width / 2 - 75, super.height / 2 - 15, 150, 20);
      this.chalkText.setMaxStringLength(16);
//TODO: Longer name on nametags
//this.a.setMaxStringLength(9999);
      this.a.setText("");
      super.buttonList.add(new GuiCDButton(11, super.width / 2 - 50, super.height / 2 + 10, 100, 20, "Confirm"));
   }

   protected void actionPerformed(GuiButton var1) {
      if(var1.id == 11) {
         String var2 = this.a.getText();
         if(var2.length() > 0) {
            if(b.containsKey(var2.toLowerCase().trim())) {
               PacketDispatcher.sendPacketToServer(CDAPacketChalk.a((String)b.get(var2)));
            } else {
               PacketDispatcher.sendPacketToServer(CDAPacketChalk.a(var2));
            }
         }

         super.mc.displayGuiScreen((GuiScreen)null);
      }

   }

   public void updateScreen() {
      this.a.updateCursorCounter();
   }

   protected void keyTyped(char var1, int var2) {
      super.keyTyped(var1, var2);
      this.a.textboxKeyTyped(var1, var2);
   }

   protected void mouseClicked(int var1, int var2, int var3) {
      super.mouseClicked(var1, var2, var3);
      this.a.mouseClicked(var1, var2, var3);
   }

   public boolean doesGuiPauseGame() {
      return false;
   }

   public void drawScreen(int var1, int var2, float var3) {
      CDRenderHelper.a((double)(super.width / 2 - 100), (double)(super.height / 2 - 35), 200.0D, 70.0D, 15987699, 1.0F);
      CDRenderHelper.b(EnumChatFormatting.DARK_GRAY + "" + EnumChatFormatting.BOLD + "Chalk!", super.width / 2, super.height / 2 - 30);
      this.a.drawTextBox();
      super.drawScreen(var1, var2, var3);
   }

   //Simple replace method
   public void addNoNoWord(String var1, String var2) {
      if(!bannedWords.containsKey(var1)) {
         bannedWords.put(var1, var2);
      }

   }

}
