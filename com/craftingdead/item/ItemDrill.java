package com.craftingdead.item;

import com.craftingdead.block.BlockManager;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.blueprint.ItemBlueprint;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemDrill extends ItemCD {

   public int d;
   public int cB;
   public int cC;


   public ItemDrill(int var1) {
      super(var1);
      this.f("Hold right click to remove concrete from your base");
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return 32;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      return var1;
   }

   public boolean onItemUse(ItemStack var1, EntityPlayer var2, World var3, int var4, int var5, int var6, int var7, float var8, float var9, float var10) {
      var2.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      if(var3.getBlockId(var4, var5, var6) == BlockManager.aA.blockID && ItemBlueprint.a(var3, var2, var4, var5, var6, 7)) {
         var3.playSoundAtEntity(var2, "craftingdead:drill", 1.0F, 1.0F);
         this.d = var4;
         this.cB = var5;
         this.cC = var6;
      }

      return true;
   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      var2.setBlockToAir(this.d, this.cB, this.cC);
      return var1;
   }
}
