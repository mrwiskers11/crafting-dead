package com.craftingdead.item;

import com.craftingdead.client.render.hat.RenderHat;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;

public class ItemHat extends ItemCD {

   private RenderHat d;
   private boolean cB = false;


   public ItemHat(int var1) {
      super(var1);
      this.setCreativeTab(ItemManager.e);
   }

   public ItemHat d() {
      this.cB = true;
      this.f("Reduces Flash Bangs");
      return this;
   }

   public ItemHat a(RenderHat var1) {
      this.d = var1;
      return this;
   }

   public boolean e() {
      return this.cB;
   }

   public RenderHat g() {
      return this.d;
   }
}
