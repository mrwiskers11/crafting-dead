package com.craftingdead.item;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;

import com.craftingdead.item.GuiChalk;
import com.craftingdead.item.ItemCD;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemChalk extends ItemCD {

   public ItemChalk(int var1) {
      super(var1);
      this.f("Right Click to set Name Tag! Craft with");
      this.f("Guns, Melee Weapons, Backpacks, Vests!");
   }

   @SideOnly(Side.CLIENT)
   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT && var2.isRemote) {
         Minecraft.getMinecraft().displayGuiScreen(new GuiChalk());
      }

      return var1;
   }

   public void a(EntityPlayer var1, ItemStack var2, String var3) {
      if(!var1.worldObj.isRemote) {
         this.a(var2, var3);
      }

   }

   public boolean a(ItemStack var1) {
      return this.b(var1) != null;
   }

   public String b(ItemStack var1) {
      NBTTagCompound var2 = this.c(var1);
      return var2.hasKey("nametag")?var2.getString("nametag"):null;
   }

   public void a(ItemStack var1, String var2) {
      NBTTagCompound var3 = this.c(var1);
      var3.setString("nametag", var2);
   }

   public boolean getShareTag() {
      return true;
   }

   public boolean hasEffect(ItemStack var1, int var2) {
      return this.a(var1);
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      if(this.a(var1)) {
         var3.add(EnumChatFormatting.RED + "" + EnumChatFormatting.ITALIC + "" + this.b(var1));
      }

      super.addInformation(var1, var2, var3, var4);
   }

   protected NBTTagCompound c(ItemStack var1) {
      String var2 = "chalk";
      if(var1.stackTagCompound == null) {
         var1.setTagCompound(new NBTTagCompound());
      }

      if(!var1.stackTagCompound.hasKey(var2)) {
         var1.stackTagCompound.setTag(var2, new NBTTagCompound(var2));
      }

      return (NBTTagCompound)var1.stackTagCompound.getTag(var2);
   }
}
