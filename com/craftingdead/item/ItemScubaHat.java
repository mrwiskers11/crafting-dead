package com.craftingdead.item;

import com.craftingdead.client.render.hat.RenderHat;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;

public class ItemScubaHat extends ItemCD {

   private RenderHat d;


   public ItemScubaHat(int var1) {
      super(var1);
      this.setCreativeTab(ItemManager.e);
   }

   public ItemScubaHat a(RenderHat var1) {
      this.d = var1;
      return this;
   }

   public RenderHat d() {
      return this.d;
   }
}
