package com.craftingdead.item;

import com.craftingdead.item.IItemAntiDupe;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class IItemAntiDupeHelper {

   public static void a(ItemStack var0) {
      if(var0 != null && var0.getItem() instanceof IItemAntiDupe) {
         IItemAntiDupe var1 = (IItemAntiDupe)var0.getItem();
         if(var1.h_() && var1.a(var0) == null) {
            var1.b(var0);
         }
      }

   }

   public static boolean a(IInventory var0, String var1) {
      int var2 = 0;

      for(int var3 = 0; var3 < var0.getSizeInventory(); ++var3) {
         ItemStack var4 = var0.getStackInSlot(var3);
         if(var4 != null && var4.getItem() instanceof IItemAntiDupe) {
            IItemAntiDupe var5 = (IItemAntiDupe)var4.getItem();
            if(var5.h_() && var5.a(var4) != null && var5.a(var4).equals(var1)) {
               ++var2;
            }
         }
      }

      return var2 < 1;
   }
}
