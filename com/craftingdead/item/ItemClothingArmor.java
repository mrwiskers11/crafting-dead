package com.craftingdead.item;

import com.craftingdead.item.ItemCD;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ItemClothingArmor extends ItemArmor {

   private static final int[] cD = new int[]{11, 16, 15, 13};
   protected String cB = "";
   protected String cC = "";


   public ItemClothingArmor(int var1, EnumArmorMaterial var2, int var3, int var4) {
      super(var1, var2, var3, var4);
      this.setMaxDamage(var2.getDurability(var4));
      super.maxStackSize = 1;
      this.setCreativeTab((CreativeTabs)null);
   }

   static int[] getMaxDamageArray() {
      return cD;
   }

   public String getArmorTexture(ItemStack var1, Entity var2, int var3, String var4) {
      return "craftingdead:textures/models/armor/default_" + var3 + ".png";
   }

   public ItemClothingArmor a(String var1) {
      this.cC = var1;
      this.setUnlocalizedName(var1.replace(" ", ""));
      LanguageRegistry.addName(this, var1);
      return this;
   }

   public ItemClothingArmor e(String var1) {
      this.cB = var1;
      return this;
   }

   public String g(ItemStack var1) {
      return this.cC;
   }

   public static String m(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemCD?((ItemCD)var0.getItem()).g(var0):"";
   }

   public void registerIcons(IconRegister var1) {
      super.itemIcon = var1.registerIcon("craftingdead:" + this.cB);
   }

}
