package com.craftingdead.item;

import com.craftingdead.CraftingDead;
import com.craftingdead.entity.SupplyDropManager;
import com.craftingdead.item.ItemCD;
import com.craftingdead.utils.BlockLocation;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemRadio extends ItemCD {

   private String d;


   public ItemRadio(int var1, String var2) {
      super(var1);
      this.d = var2;
      this.f(EnumChatFormatting.RED + "Right Click to callin a Supply Drop!");
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      if(!var2.isRemote) {
         SupplyDropManager var4 = CraftingDead.f.h().a();
         BlockLocation var5 = new BlockLocation((int)var3.posX, (int)var3.posY, (int)var3.posZ);
         var4.a(var2, var5, this.d, 1);
         var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Spawning " + this.d.toUpperCase() + " Supply Drop!"));
      }

      if(!var3.capabilities.isCreativeMode) {
         --var1.stackSize;
      }

      return var1;
   }

   public boolean hasEffect(ItemStack var1, int var2) {
      return true;
   }
}
