package com.craftingdead.item;

import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.player.PlayerData;

import java.util.List;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemClothing extends ItemCD {

   private String d = "default";
   private int cB = 0;
   private int cC = 0;
   private boolean cD = false;
   private boolean cE = false;


   public ItemClothing(int var1, String var2) {
      super(var1);
      this.d = "clothing_" + var2;
      this.setCreativeTab(ItemManager.e);
   }

   public ItemClothing b(int var1) {
      this.cC = var1;
      return this;
   }

   public ItemClothing d() {
      this.cE = true;
      return this;
   }

   public ItemClothing e() {
      this.cD = true;
      return this;
   }

   public boolean g() {
      return this.cE;
   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      if(!var2.isRemote) {
         Random var4 = new Random();
         int var5 = var4.nextInt(3) + 3;

         for(int var6 = 0; var6 < var5; ++var6) {
            EntityGroundItem var7 = new EntityGroundItem(var2, var3.posX, var3.posY + 2.0D, var3.posZ);
            if(var4.nextBoolean()) {
               var7.a(new ItemStack(ItemManager.gR));
            } else {
               var7.a(new ItemStack(ItemManager.gQ));
            }

            var2.spawnEntityInWorld(var7);
         }
      }

      --var1.stackSize;
      return var1;
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return 32;
   }

   public EnumAction getItemUseAction(ItemStack var1) {
      return EnumAction.block;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      return var1;
   }

   public static String a(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemClothing?((ItemClothing)var0.getItem()).r():null;
   }

   public int i() {
      return this.cC;
   }

   public boolean j() {
      return this.cD;
   }

   public static int b(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemClothing?((ItemClothing)var0.getItem()).i():0;
   }

   public static boolean a(PlayerData var0) {
      return var0.d().getStackInSlot(5) != null;
   }

   public int k() {
      return this.cB;
   }

   public String r() {
      return this.d;
   }

   public String B() {
      return this.cC == 0?"None":(this.cC == 1?"Low":(this.cC == 2?"Medium":"High"));
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      var3.add("Protection Level " + EnumChatFormatting.RED + this.B());
      if(this.cB == 1) {
         var3.add(EnumChatFormatting.RED + "Low Protection");
      }

      if(this.cB == 2) {
         var3.add(EnumChatFormatting.RED + "Medium Protection");
      }

      if(this.cB == 3) {
         var3.add(EnumChatFormatting.RED + "High Protection");
      }

      var3.add(EnumChatFormatting.RED + "Hold Right Click" + EnumChatFormatting.GRAY + " to Tear");
   }
}
