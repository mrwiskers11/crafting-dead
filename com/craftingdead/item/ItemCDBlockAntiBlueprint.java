package com.craftingdead.item;

import com.craftingdead.item.ItemCDBlock;

public class ItemCDBlockAntiBlueprint extends ItemCDBlock {

   public ItemCDBlockAntiBlueprint(int var1) {
      super(var1);
      this.a("Prevents all blueprints within 100 Blocks");
      this.a("from being placed.");
   }
}
