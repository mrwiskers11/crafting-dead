package com.craftingdead.item;

import com.craftingdead.CraftingDead;
import com.craftingdead.entity.EntityFlameThrowerFire;
import com.craftingdead.inventory.InventoryFuelTanks;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemFuelTank;
import com.craftingdead.item.ItemFuelTankBackpack;
import com.craftingdead.network.packets.gun.CDAPacketFTClient;
import com.craftingdead.network.packets.gun.CDAPacketFTTrigger;
import com.craftingdead.network.packets.gun.CDAPacketFTTriggerStop;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import org.lwjgl.input.Mouse;

public class ItemFlameThrower extends ItemCD {

   private static boolean d;
   private static boolean cB = d;
   private double cC = 0.0D;
   private double cD = 0.0D;
   private double cE = 0.0D;
   private boolean cF = false;


   public ItemFlameThrower(int var1) {
      super(var1);
   }

   public void c(ItemStack var1, World var2, EntityPlayer var3) {
      int var4 = -1;
      InventoryFuelTanks var5 = (InventoryFuelTanks)ItemFuelTankBackpack.a(var3);
      if(!var3.isSprinting()) {
         if(var5 != null) {
            if(!var3.capabilities.isCreativeMode) {
               for(int var6 = 0; var6 < var5.getSizeInventory(); ++var6) {
                  ItemStack var7 = var5.getStackInSlot(var6);
                  if(var7 != null && var7.getItem() instanceof ItemFuelTank) {
                     ItemFuelTank var8 = (ItemFuelTank)var7.getItem();
                     if(var7.getItemDamage() < var8.d) {
                        var4 = var6;
                        break;
                     }
                  }
               }
            }

            if(var4 == -1 && !var3.capabilities.isCreativeMode) {
               this.a(var1, false);
               if(this.cF) {
                  var2.playSoundAtEntity(var3, "craftingdead:flameout", 1.0F, 1.0F);
                  this.cF = false;
               }
            } else {
               if(!this.cF) {
                  var2.playSoundAtEntity(var3, "craftingdead:flamestart", 1.0F, 1.0F);
                  this.cF = true;
               }

               if(this.cE++ > 18.0D) {
                  var2.playSoundAtEntity(var3, "craftingdead:flame", 1.0F, 1.0F);
                  this.cE = 0.0D;
               }

               if(this.cC++ < 2.0D) {
                  return;
               }

               EntityFlameThrowerFire var9 = new EntityFlameThrowerFire(var2, var3);
               var2.spawnEntityInWorld(var9);
               this.a(var1, true);
               this.a(var3, 100);
               if(!var3.capabilities.isCreativeMode) {
                  int var10 = var5.getStackInSlot(var4).getItemDamage();
                  var5.setInventorySlotContents(var4, new ItemStack(var5.getStackInSlot(var4).getItem(), 1, var10 + 1));
               }
            }
         } else {
            this.a(var1, false);
         }

      }
   }

   public void e(ItemStack var1, World var2, EntityPlayer var3) {
      if(!var2.isRemote) {
         this.cF = false;
         this.cE = 0.0D;
         this.cC = 0.0D;
         if(this.a(var1)) {
            var2.playSoundAtEntity(var3, "craftingdead:flameout", 1.0F, 1.0F);
         }
      }

   }

   public void onUpdate(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      if(var2.isRemote && Minecraft.getMinecraft().currentScreen == null && var3 != null && var3 instanceof EntityPlayer) {
         EntityPlayer var6 = (EntityPlayer)var3;
         PlayerData var7 = PlayerDataHandler.a(var6);
         if(var7.Q) {
            return;
         }

         if(var3.isSprinting()) {
            return;
         }

         if(var6.getCurrentEquippedItem() == var1) {
            cB = d;
            d = Mouse.isButtonDown(Mouse.getButtonIndex("BUTTON1"));
            if(d) {
               PacketDispatcher.sendPacketToServer(CDAPacketFTTrigger.b());
               if(this.a(var1) && this.cD++ > 1.0D) {
                  CraftingDead.l().i().a(var1, var2, var6);
               }
            }

            if(d != cB && !d) {
               PacketDispatcher.sendPacketToServer(CDAPacketFTTriggerStop.b());
               this.cD = 0.0D;
            }
         }
      }

   }

   public void a(EntityPlayer var1, int var2) {
      if(!var1.worldObj.isRemote) {
         ArrayList var3 = (ArrayList)var1.worldObj.getEntitiesWithinAABB(EntityPlayer.class, AxisAlignedBB.getBoundingBox(var1.posX - (double)var2, var1.posY - (double)var2, var1.posZ - (double)var2, var1.posX + (double)var2, var1.posY + (double)var2, var1.posZ + (double)var2));
         Iterator var4 = var3.iterator();

         while(var4.hasNext()) {
            EntityPlayer var5 = (EntityPlayer)var4.next();
            if(!var5.username.equals(var1.username)) {
               PacketDispatcher.sendPacketToPlayer(CDAPacketFTClient.a(var1), (Player)var5);
            }
         }
      }

   }

   public void a(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.b(var1);
      var3.setBoolean("ammo", var2);
   }

   public boolean a(ItemStack var1) {
      NBTTagCompound var2 = this.b(var1);
      return var2.hasKey("ammo")?var2.getBoolean("ammo"):false;
   }

   public boolean getShareTag() {
      return true;
   }

   @SideOnly(Side.CLIENT)
   public boolean isFull3D() {
      return true;
   }

   protected NBTTagCompound b(ItemStack var1) {
      String var2 = "thrower";
      if(var1.stackTagCompound == null) {
         var1.setTagCompound(new NBTTagCompound());
      }

      if(!var1.stackTagCompound.hasKey(var2)) {
         var1.stackTagCompound.setTag(var2, new NBTTagCompound(var2));
      }

      return (NBTTagCompound)var1.stackTagCompound.getTag(var2);
   }

}
