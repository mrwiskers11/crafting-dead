package com.craftingdead.item;

import com.craftingdead.event.EventThirstQuenched;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.potions.PotionManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import java.util.Random;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemConsumable extends ItemCD {

   public boolean d = false;
   public int cB = 0;
   public boolean cC = false;
   public int cD = 0;
   public boolean cE = false;
   public boolean cF = false;
   public int cG = 0;
   public boolean cH = false;
   public boolean cI = false;
   public boolean cJ = false;
   public boolean cK = false;
   public boolean cL = false;
   public boolean cM = false;
   public boolean cN = false;
   public int cO = 0;
   public EnumAction cP;


   public ItemConsumable(int var1) {
      super(var1);
      this.cP = EnumAction.eat;
      this.setCreativeTab(ItemManager.d);
   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      PlayerData var4 = PlayerDataHandler.a(var3);
      if(this.d && var4.e().c()) {
         if(!var2.isRemote) {
            EventThirstQuenched var5 = new EventThirstQuenched(var3, var1, var4.e().b(), this.cB);
            if(MinecraftForge.EVENT_BUS.post(var5)) {
               return var1;
            }
         }

         var4.e().c(this.cB);
      }

      if(this.cC && var3.getFoodStats().needFood()) {
         var3.getFoodStats().addStats(this.cD, (float)this.cD);
      }

      if(this.cF && var3.getHealth() < 20.0F) {
         var3.heal((float)this.cG);
      }

      Random var6;
      if(this.cH && var3.isPotionActive(PotionManager.b.id)) {
         if(var1.getItem() == ItemManager.gX) {
            var6 = new Random();
            if(var6.nextInt(5) == 0) {
               var3.addPotionEffect(new PotionEffect(PotionManager.a.id, 999999));
               var3.removePotionEffect(PotionManager.b.id);
               var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You have bandaged yourself with a bloody rag! It was infected!"));
            } else {
               var3.removePotionEffect(PotionManager.b.id);
            }
         } else {
            var3.removePotionEffect(PotionManager.b.id);
         }
      }

      if(this.cI && var3.isPotionActive(PotionManager.a.id)) {
         var3.removePotionEffect(PotionManager.a.id);
      }

      if(this.cJ && var3.isPotionActive(PotionManager.c.id)) {
         var3.removePotionEffect(PotionManager.c.id);
      }

      if(this.cK) {
         var3.addPotionEffect(new PotionEffect(PotionManager.d.id, 400));
      }

      if(this.cO != 0) {
         return new ItemStack(this.cO, 1, 0);
      } else {
         if(this.cL) {
            var6 = new Random();
            if(var6.nextInt(3) == 0 && !var3.isPotionActive(Potion.poison.id)) {
               var3.addPotionEffect(new PotionEffect(Potion.confusion.id, 150));
               var3.addPotionEffect(new PotionEffect(Potion.poison.id, 500));
               var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "I feel sick... I think that food was out of date..."));
            }
         }

         --var1.stackSize;
         return var1;
      }
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return 32;
   }

   public EnumAction getItemUseAction(ItemStack var1) {
      return this.cP;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      PlayerData var4 = PlayerDataHandler.a(var3);
      if(this.d && var4.e().c()) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else if(this.cC && var3.getFoodStats().needFood()) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else if(this.cF && var3.getHealth() < 20.0F) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else if(this.cH && var3.isPotionActive(PotionManager.b.id) && !this.cE) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else if(this.cI && var3.isPotionActive(PotionManager.a.id)) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else if(this.cJ && var3.isPotionActive(PotionManager.c.id)) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else if(this.cK) {
         var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
         return var1;
      } else {
         return var1;
      }
   }

   public boolean itemInteractionForEntity(ItemStack var1, EntityPlayer var2, EntityLivingBase var3) {
      World var4 = var2.worldObj;
      if(!var4.isRemote && var3 instanceof EntityPlayer) {
         EntityPlayer var5 = (EntityPlayer)var3;
         if(var5.capabilities.isCreativeMode) {
            return false;
         }

         if(this.cH && var5.isPotionActive(PotionManager.b)) {
            if(var2.isPotionActive(PotionManager.b.id)) {
               var2.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You must heal yourself first before you can heal " + var5.getEntityName()));
            } else {
               Random var6 = new Random();
               this.cE = true;
               var2.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You are bandaging " + var5.getEntityName()));
               if(var6.nextInt(5) == 0 && var1.getItem() == ItemManager.gX) {
                  var5.addPotionEffect(new PotionEffect(PotionManager.a.id, 999999));
                  var5.removePotionEffect(PotionManager.b.id);
                  var5.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + var2.getEntityName() + " has bandaged you with a bloody rag, however it was infected!"));
                  var2.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You have bandaged " + var5.getEntityName() + ". However they caught RBI Infection..."));
                  this.cE = false;
               } else {
                  var5.removePotionEffect(PotionManager.b.id);
                  var5.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + var2.getEntityName() + " has bandaged you!"));
                  var2.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You have bandaged " + var5.getEntityName()));
                  --var1.stackSize;
                  this.cE = false;
               }
            }
         }
      }

      return false;
   }

   public ItemConsumable b(int var1) {
      this.d = true;
      this.cB = var1;
      return this;
   }

   public ItemConsumable c(int var1) {
      this.cC = true;
      this.cD = var1;
      return this;
   }

   public ItemConsumable f(int var1) {
      this.cF = true;
      this.cG = var1;
      return this;
   }

   public ItemConsumable d() {
      this.cN = true;
      return this;
   }

   public ItemConsumable e() {
      this.cH = true;
      return this;
   }

   public ItemConsumable g() {
      this.cI = true;
      return this;
   }

   public ItemConsumable i() {
      this.cJ = true;
      return this;
   }

   public ItemConsumable a(EnumAction var1) {
      this.cP = var1;
      return this;
   }

   public ItemConsumable g(int var1) {
      this.cO = var1;
      return this;
   }

   public ItemConsumable j() {
      this.cK = true;
      return this;
   }

   public ItemConsumable k() {
      this.cL = true;
      return this;
   }

   public ItemConsumable r() {
      this.cM = true;
      return this;
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      if(this.cD != 0) {
         var3.add("Food " + EnumChatFormatting.RED + this.cD);
      }

      if(this.cB != 0) {
         var3.add("Water " + EnumChatFormatting.RED + this.cB);
      }

      if(this.cG != 0) {
         var3.add("Hearts " + EnumChatFormatting.RED + this.cG);
      }

      if(this.cH) {
         var3.add("Stops Bleeding");
      }

      if(this.cI) {
         var3.add("Cures RB Infection");
      }

      if(this.cJ) {
         var3.add("Fixes Broken Legs");
      }

      if(this.cK) {
         var3.add("Induces Adrenaline");
      }

      if(this.cL) {
         var3.add("Chance food poisoning could be induced...");
      }

   }
}
