package com.craftingdead.item;

import com.craftingdead.entity.EntityC4;
import com.craftingdead.item.ItemGrenade;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemC4 extends ItemGrenade {

   public ItemC4(int var1) {
      super(var1);
   }

   public void a(ItemStack var1, World var2, EntityPlayer var3, double var4) {
      var2.spawnEntityInWorld(new EntityC4(var2, var3, var4));
      if(!var3.capabilities.isCreativeMode) {
         var3.inventory.setInventorySlotContents(var3.inventory.currentItem, (ItemStack)null);
      }

   }
}
