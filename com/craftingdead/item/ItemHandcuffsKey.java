package com.craftingdead.item;

import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.Random;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemHandcuffsKey extends ItemCD {

   public ItemHandcuffsKey(int var1) {
      super(var1);
      this.f("Right Click on a handcuffed player");
   }

   public boolean itemInteractionForEntity(ItemStack var1, EntityPlayer var2, EntityLivingBase var3) {
      World var4 = var2.worldObj;
      if(!var4.isRemote && var3 instanceof EntityPlayer) {
         EntityPlayer var5 = (EntityPlayer)var3;
         PlayerData var6 = PlayerDataHandler.a(var5);
         if(var6.Q) {
            var6.Q = false;
            var5.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "The handcuffs are removed!"));
            ItemStack var7 = new ItemStack(ItemManager.fe);
            if(!var2.inventory.addItemStackToInventory(var7)) {
               var7.setItemDamage(var6.R);
               var5.dropPlayerItem(var7);
            } else {
               Random var8 = new Random();
               var5.playSound("random.pop", 0.2F, ((var8.nextFloat() - var8.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            }

            var6.R = 0;
         }
      }

      return false;
   }

   @SideOnly(Side.CLIENT)
   public boolean isFull3D() {
      return false;
   }
}
