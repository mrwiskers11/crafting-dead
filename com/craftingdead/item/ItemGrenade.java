package com.craftingdead.item;

import com.craftingdead.entity.EntityGrenade;
import com.craftingdead.item.ItemCD;
import com.craftingdead.network.packets.CDAPacketGrenadeThrowing;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import org.lwjgl.input.Mouse;

public class ItemGrenade extends ItemCD {

   public static boolean d;
   public static boolean cB = d;
   public static boolean cC;
   public static boolean cD = cC;
   public boolean cE = false;
   public boolean cF = false;
   public double cG = 1.1D;
   public boolean cH = false;
   private static ItemStack cI;
   private static int cJ = 0;


   public ItemGrenade(int var1) {
      super(var1);
      this.setMaxStackSize(1);
   }

   public void onUpdate(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      if(var2.isRemote && Minecraft.getMinecraft().currentScreen == null && var3 != null && var3 instanceof EntityPlayer) {
         EntityPlayer var6 = (EntityPlayer)var3;
         PlayerData var7 = PlayerDataHandler.a(var6);
         if(var7.Q) {
            return;
         }

         if(var6.getCurrentEquippedItem() == var1) {
            if(cI != var1) {
               cJ = 5;
               cI = var1;
            }

            if(--cJ > 0) {
               return;
            }

            cB = d;
            d = Mouse.isButtonDown(Mouse.getButtonIndex("BUTTON1"));
            cD = cC;
            cC = Mouse.isButtonDown(Mouse.getButtonIndex("BUTTON0"));
            if(cC) {
               this.cG = 1.45D;
               this.cE = true;
            }

            if(d) {
               this.cG = 0.3D;
               this.cE = true;
            }

            if(d && cC) {
               this.cG = 0.7D;
               this.cE = true;
            }

            if(!cC && !d && this.cE) {
               PacketDispatcher.sendPacketToServer(CDAPacketGrenadeThrowing.a(this.cG));
               this.cE = false;
            }
         }
      }

   }

   public void a(ItemStack var1, World var2, EntityPlayer var3, double var4) {
      PlayerData var6 = PlayerDataHandler.a(var3);
      if(var6.I) {
         if(this.cH) {
            this.cH = false;
         } else {
            double var7 = 1.4D;
            var2.spawnEntityInWorld(new EntityGrenade(var2, var3, var4, (int)(var7 * 20.0D)));
            if(!var3.capabilities.isCreativeMode) {
               var3.inventory.setInventorySlotContents(var3.inventory.currentItem, (ItemStack)null);
            }

         }
      }
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      var3.add(EnumChatFormatting.RED + "Left click" + EnumChatFormatting.GRAY + " to throw far!");
      var3.add(EnumChatFormatting.RED + "Right click" + EnumChatFormatting.GRAY + " to throw close!");
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      MovingObjectPosition var4 = this.getMovingObjectPositionFromPlayer(var2, var3, true);
      if(var4 != null && var4.typeOfHit == EnumMovingObjectType.TILE) {
         int var5 = var4.blockX;
         int var6 = var4.blockY;
         int var7 = var4.blockZ;
         if(var2.getBlockMaterial(var5, var6, var7) == Block.doorWood.blockMaterial || var2.getBlockMaterial(var5, var6, var7) == Block.doorIron.blockMaterial) {
            this.cH = true;
         }
      }

      return var1;
   }

   public boolean onEntitySwing(EntityLivingBase var1, ItemStack var2) {
      if(var1 instanceof EntityPlayer) {
         EntityPlayer var3 = (EntityPlayer)var1;
         MovingObjectPosition var4 = this.getMovingObjectPositionFromPlayer(var3.worldObj, var3, true);
         if(var4 != null && var4.typeOfHit == EnumMovingObjectType.TILE) {
            int var5 = var4.blockX;
            int var6 = var4.blockY;
            int var7 = var4.blockZ;
            if(var3.worldObj.getBlockMaterial(var5, var6, var7) == Block.doorWood.blockMaterial || var3.worldObj.getBlockMaterial(var5, var6, var7) == Block.doorIron.blockMaterial) {
               this.cH = true;
               return true;
            }
         }
      }

      return false;
   }

   public boolean onLeftClickEntity(ItemStack var1, EntityPlayer var2, Entity var3) {
      return true;
   }

   public boolean onBlockStartBreak(ItemStack var1, int var2, int var3, int var4, EntityPlayer var5) {
      if(var5.worldObj.getBlockMaterial(var2, var3, var4) == Block.doorWood.blockMaterial || var5.worldObj.getBlockMaterial(var2, var3, var4) == Block.doorIron.blockMaterial) {
         this.cH = true;
      }

      return false;
   }

}
