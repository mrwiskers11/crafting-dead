package com.craftingdead.item;


public enum EnumBackpackSize {

   a("NONE", 0, 0),
   b("SMALL", 1, 9),
   c("MEDIUM", 2, 18),
   d("LARGE", 3, 27),
   e("GUNBAG", 4, 18);
   public int f = 0;
   // $FF: synthetic field
   private static final EnumBackpackSize[] g = new EnumBackpackSize[]{a, b, c, d, e};


   private EnumBackpackSize(String var1, int var2, int var3) {
      this.f = var3;
   }

}
