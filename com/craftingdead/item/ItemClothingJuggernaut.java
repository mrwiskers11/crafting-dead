package com.craftingdead.item;

import com.craftingdead.item.ItemClothing;

public class ItemClothingJuggernaut extends ItemClothing {

   public ItemClothingJuggernaut(int var1, String var2) {
      super(var1, var2);
      this.f("Reduces bullet damage greatly, Slow movement");
   }
}
