package com.craftingdead.item;

import com.craftingdead.item.IItemCanOpener;
import com.craftingdead.item.ItemCD;

public class ItemCanOpener extends ItemCD implements IItemCanOpener {

   public ItemCanOpener(int var1) {
      super(var1);
      this.f("Craft with canned goods to open them!");
   }
}
