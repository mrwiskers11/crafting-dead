package com.craftingdead.item;

import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.item.CrateContent;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.gun.ItemGun;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

public class ItemLootcrate extends ItemCD {

   public ArrayList d = new ArrayList();


   public ItemLootcrate(int var1) {
      super(var1);
      this.setMaxStackSize(16);
      this.f("Right Click to Open!");
   }

   public ItemLootcrate a(Item var1, int var2) {
      this.d.add(new CrateContent(var1.itemID, var2));
      return this;
   }

   public ItemStack a(EntityPlayer var1) {
      ArrayList var2 = new ArrayList();

      for(int var3 = 0; var3 < this.d.size(); ++var3) {
         CrateContent var4 = (CrateContent)this.d.get(var3);

         for(int var5 = 0; var5 < var4.b; ++var5) {
            ItemStack var6 = new ItemStack(var4.a, 1, 0);
            var2.add(var6);
         }
      }

      Random var7 = new Random();
      ItemStack var8 = ((ItemStack)var2.get(var7.nextInt(var2.size()))).copy();
      if(var8.getItem() instanceof ItemGun) {
         ItemGun var9 = (ItemGun)var8.getItem();
         var9.a(var1, var8, new ItemStack(Item.itemsList[var9.i()]));
      }

      return var8;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      if(!var2.isRemote) {
         var2.playSoundAtEntity(var3, "random.pop", 1.0F, 1.0F);
         var2.playSoundAtEntity(var3, "random.pop", 0.5F, 1.5F);
         var2.playSoundAtEntity(var3, "random.pop", 0.7F, 2.0F);
         ItemStack var4 = this.a(var3);
         --var1.stackSize;
         if(!var3.inventory.addItemStackToInventory(var4)) {
            EntityGroundItem var5 = new EntityGroundItem(var2, var3.posX, var3.posY, var3.posZ);
            var5.a(var4);
            var2.spawnEntityInWorld(var5);
         }

         return var1;
      } else {
         return var1;
      }
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      if(Keyboard.isKeyDown(19)) {
         var3.add(EnumChatFormatting.RED + "-=-=-=- Content and Weights -=-=-=-");

         for(int var5 = 0; var5 < this.d.size(); ++var5) {
            CrateContent var6 = (CrateContent)this.d.get(var5);
            ItemStack var7 = new ItemStack(var6.a, 1, 0);
            var3.add(EnumChatFormatting.RED + "" + var6.b + " " + EnumChatFormatting.GRAY + var7.getDisplayName());
         }
      } else {
         var3.add(EnumChatFormatting.RED + "Press \'R\' to view potential content!");
      }

   }
}
