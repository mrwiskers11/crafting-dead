package com.craftingdead.item;

import com.craftingdead.entity.EntityC4;
import com.craftingdead.item.ItemCD;

import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemC4Detonator extends ItemCD {

   private int d = 50;


   public ItemC4Detonator(int var1) {
      super(var1);
      this.f(EnumChatFormatting.RED + "Right Click" + EnumChatFormatting.GRAY + " to set off all your C4");
      this.f("within " + this.d + " blocks of you!");
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      if(!var2.isRemote) {
         var2.playSoundAtEntity(var3, "random.click", 0.8F, 1.2F);
         ArrayList var4 = (ArrayList)var2.getEntitiesWithinAABB(EntityC4.class, AxisAlignedBB.getBoundingBox(var3.posX - (double)this.d, var3.posY - (double)this.d, var3.posZ - (double)this.d, var3.posX + (double)this.d, var3.posY + (double)this.d, var3.posZ + (double)this.d));
         if(var4.size() > 0) {
            for(int var5 = 0; var5 < var4.size(); ++var5) {
               EntityC4 var6 = (EntityC4)var4.get(var5);
               var6.b(var3);
            }
         }
      }

      return var1;
   }
}
