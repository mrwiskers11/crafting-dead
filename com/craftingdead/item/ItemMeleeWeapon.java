package com.craftingdead.item;

import com.craftingdead.faction.FactionManager;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemMeleeWeapon extends ItemCD {

   private int d;


   public ItemMeleeWeapon(int var1, int var2, int var3) {
      super(var1);
      this.d = var2;
      this.setMaxDamage(var3);
      this.setMaxStackSize(1);
      this.setCreativeTab(ItemManager.c);
   }

   public boolean hitEntity(ItemStack var1, EntityLivingBase var2, EntityLivingBase var3) {
      if(var2 instanceof EntityPlayer && var3 instanceof EntityPlayer) {
         if(!FactionManager.c().a(var2.getEntityName(), var3.getEntityName())) {
            var1.damageItem(1, var3);
         }
      } else {
         var1.damageItem(1, var3);
      }

      if(!var3.worldObj.isRemote) {
         var2.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer)var3), (float)this.d);
      }

      return true;
   }

   public boolean isFull3D() {
      return true;
   }

   public EnumAction getItemUseAction(ItemStack var1) {
      return EnumAction.block;
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return 72000;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      return var1;
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      var3.add("Damage " + EnumChatFormatting.RED + this.d);
      var3.add("Durability " + EnumChatFormatting.RED + this.getMaxDamage());
   }
}
