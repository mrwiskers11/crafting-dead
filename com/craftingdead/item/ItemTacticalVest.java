package com.craftingdead.item;

import com.craftingdead.client.render.RenderTacticalVest;
import com.craftingdead.inventory.InventoryTacticalVest;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.input.Keyboard;

public class ItemTacticalVest extends ItemCD {

   private RenderTacticalVest d;


   public ItemTacticalVest(int var1) {
      super(var1);
      this.setMaxStackSize(1);
      this.setCreativeTab(ItemManager.e);
   }

   public ItemTacticalVest a(RenderTacticalVest var1) {
      this.d = var1;
      return this;
   }

   public static IInventory a(EntityPlayer var0) {
      InventoryTacticalVest var1 = null;
      if(!var0.worldObj.isRemote) {
         PlayerData var2 = PlayerDataHandler.a(var0);
         ItemStack var3 = var2.d().a("vest");
         if(var3 != null && var3.getItem() instanceof ItemTacticalVest) {
            var1 = new InventoryTacticalVest(var3);
         }
      }

      return var1;
   }

   public static IInventory a(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemTacticalVest?new InventoryTacticalVest(var0):null;
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      var3.add("Reduces Bullet Damage to the body by 20%");
      InventoryTacticalVest var5 = (InventoryTacticalVest)a(var1);
      if(var5 != null) {
         int var6 = var5.h().size();
         var3.add("Hoard Items! Press \'R\' for more info!");
         var3.add("Holding " + EnumChatFormatting.RED + var6 + EnumChatFormatting.GRAY + " Item(s)");
         if(Keyboard.isKeyDown(19)) {
            var3.add(EnumChatFormatting.RED + "-=-=- Tactical Vest Content -=-=-");
            Iterator var7 = var5.h().iterator();

            while(var7.hasNext()) {
               ItemStack var8 = (ItemStack)var7.next();
               var3.add(var8.getDisplayName());
            }
         }
      }

   }

   public boolean getShareTag() {
      return true;
   }

   public RenderTacticalVest d() {
      return this.d;
   }
}
