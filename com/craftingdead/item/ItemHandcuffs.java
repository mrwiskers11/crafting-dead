package com.craftingdead.item;

import com.craftingdead.item.ItemCD;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemHandcuffs extends ItemCD {

   public ItemHandcuffs(int var1) {
      super(var1);
      this.f("Right Click on a player");
   }

   public boolean itemInteractionForEntity(ItemStack var1, EntityPlayer var2, EntityLivingBase var3) {
      World var4 = var2.worldObj;
      if(!var4.isRemote && var3 instanceof EntityPlayer) {
         EntityPlayer var5 = (EntityPlayer)var3;
         PlayerData var6 = PlayerDataHandler.a(var5);
         if(var5.capabilities.isCreativeMode) {
            return false;
         }

         if(!var6.Q && var1.stackSize > 0) {
            var6.Q = true;
            var6.R = var1.getItemDamage();
            var5.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You\'ve been handcuffed by " + var2.getDisplayName()));
            --var1.stackSize;
         }
      }

      return false;
   }

   @SideOnly(Side.CLIENT)
   public boolean isFull3D() {
      return false;
   }
}
