package com.craftingdead.item;

import com.craftingdead.item.ItemManager;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class CreativeTabCDFood extends CreativeTabs {

   public CreativeTabCDFood(String var1) {
      super(var1);
   }

   public ItemStack getIconItemStack() {
      return new ItemStack(ItemManager.bM);
   }
}
