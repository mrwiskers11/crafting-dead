package com.craftingdead.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface IItemMovementPenalty {

   boolean a(EntityPlayer var1, ItemStack var2);

   double c(ItemStack var1);
}
