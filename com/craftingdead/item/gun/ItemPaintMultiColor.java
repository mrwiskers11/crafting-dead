package com.craftingdead.item.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.item.LootManager;
import com.craftingdead.item.gun.GunPaint;
import com.craftingdead.item.gun.ItemPaint;

import java.util.List;
import java.util.Random;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

public class ItemPaintMultiColor extends ItemPaint {

   public static int[] cB = new int[]{0, 0, 0};


   public ItemPaintMultiColor(int var1) {
      super(var1, GunPaint.k);
   }

   public void onUpdate(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      if(!var2.isRemote && !this.a(var1)) {
         this.b(var1);
      }

   }

   public boolean a(ItemStack var1) {
      NBTTagCompound var2 = this.n(var1);
      return var2.hasKey("red");
   }

   public void b(ItemStack var1) {
      NBTTagCompound var2 = this.n(var1);
      Random var3 = new Random();
      var2.setInteger("red", var3.nextInt(256));
      var2.setInteger("green", var3.nextInt(256));
      var2.setInteger("blue", var3.nextInt(256));
   }

   public int[] c(ItemStack var1) {
      NBTTagCompound var2 = this.n(var1);
      int[] var3 = new int[]{var2.getInteger("red"), var2.getInteger("green"), var2.getInteger("blue")};
      return var3;
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      int[] var5 = this.c(var1);
      var3.add(EnumChatFormatting.AQUA + "Unique Color Code");
      var3.add(EnumChatFormatting.RED + "R: " + var5[0]);
      var3.add(EnumChatFormatting.GREEN + "G: " + var5[1]);
      var3.add(EnumChatFormatting.BLUE + "B: " + var5[2]);
      GL11.glPushMatrix();
      GL11.glDisable(2896);
      if(!var2.capabilities.isCreativeMode) {
         CDRenderHelper.b(4.0D, 4.0D, 110.0D, 30.0D, 0, 0.5F);
         CDRenderHelper.b("Paint Color Preview", 57, 8);
         CDRenderHelper.a(6.0D, 20.0D, 112.0D, 30.0D, (float)var5[0] / 255.0F, (float)var5[1] / 255.0F, (float)var5[2] / 255.0F, 1.0F);
      }

      GL11.glEnable(2896);
      GL11.glPopMatrix();
      if(super.c.size() > 0) {
         var3.addAll(super.c);
      }

      if(CraftingDead.l().e().d) {
         var3.add("Press \'" + EnumChatFormatting.AQUA + Keyboard.getKeyName(KeyBindingManager.b.keyCode) + EnumChatFormatting.GRAY + "\' to view Loot %s");
         if(Keyboard.isKeyDown(KeyBindingManager.b.keyCode)) {
            var3.add("Residental Loot: " + EnumChatFormatting.AQUA + LootManager.a.a(var1) + "%");
            var3.add("Rare Residental Loot: " + EnumChatFormatting.AQUA + LootManager.b.a(var1) + "%");
            var3.add("Medical Loot: " + EnumChatFormatting.AQUA + LootManager.c.a(var1) + "%");
            var3.add("Police Loot: " + EnumChatFormatting.AQUA + LootManager.e.a(var1) + "%");
            var3.add("Military Loot: " + EnumChatFormatting.AQUA + LootManager.d.a(var1) + "%");
         }
      }

   }

   public boolean hasEffect(ItemStack var1, int var2) {
      return true;
   }

   public boolean getShareTag() {
      return true;
   }

   protected NBTTagCompound n(ItemStack var1) {
      String var2 = "cdapaintcolor";
      if(var1.stackTagCompound == null) {
         var1.setTagCompound(new NBTTagCompound());
      }

      if(!var1.stackTagCompound.hasKey(var2)) {
         var1.stackTagCompound.setTag(var2, new NBTTagCompound(var2));
      }

      return (NBTTagCompound)var1.stackTagCompound.getTag(var2);
   }

}
