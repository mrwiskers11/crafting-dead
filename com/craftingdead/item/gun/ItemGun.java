package com.craftingdead.item.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.ClientTickHandler;
import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.client.animation.AnimationManager;
import com.craftingdead.client.animation.GunAnimation;
import com.craftingdead.client.animation.gun.GunAnimationReload;
import com.craftingdead.client.render.gun.RenderGun;
import com.craftingdead.event.EventGunFired;
import com.craftingdead.event.EventGunOutOfAmmo;
import com.craftingdead.event.EventGunReload;
import com.craftingdead.item.IItemAntiDupe;
import com.craftingdead.item.IItemMovementPenalty;
import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.ItemTacticalVest;
import com.craftingdead.item.gun.BulletSpawner;
import com.craftingdead.item.gun.EnumFireMode;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.GunPaint;
import com.craftingdead.item.gun.ItemAmmo;
import com.craftingdead.item.gun.ItemPaintMultiColor;
import com.craftingdead.network.packets.gun.CDAPacketGunMinigun;
import com.craftingdead.network.packets.gun.CDAPacketGunReload;
import com.craftingdead.network.packets.gun.CDAPacketGunTrigger;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class ItemGun extends ItemCD implements IItemAntiDupe, IItemMovementPenalty {

   private int cR;
   private int cS;
   private float cT;
   private float cU;
   private float cV;
   public int d = 120;
   public double cB = 0.0D;
   public float cC = 0.0F;
   public String cD;
   public String cE;
   public String cF;
   private String cW = "gunEmpty";
   private String cX = "gunFiremode";
   private float cY = 1.0F;
   public static Map cG = new HashMap();
   private Class cZ;
   private Class da;
   private Class db;
   private int[] dc = new int[0];
   private EnumFireMode[] dd = new EnumFireMode[0];
   private EnumFireMode de;
   private GunAttachment[] df;
   private GunPaint[] dg;
   public boolean cH;
   public float cI;
   public boolean cJ;
   public boolean cK;
   public boolean cL;
   public boolean cM;
   public int cN;
   public boolean cO;
   private double dh;
   private double di;
   private double dj;
   private double dk;
   private double dl;
   private static double dm = 0.0D;
   private static boolean dn;
   private static boolean duu = dn;
   private static boolean dp;
   private static boolean dq = !dp;
   private static boolean dr;
   private static boolean ds = !dr;
   public static boolean cP;
   public static boolean cQ = !cP;
   private static boolean dt;
   private static boolean du = dt;
   private static int dv = 0;
   private int dw;
   private boolean dx;
   private int dy;
   private int dz;
   private boolean dA;


   public ItemGun(int var1, double var2, double var4, float var6, float var7) {
      super(var1);
      this.de = EnumFireMode.a;
      this.cH = true;
      this.cI = 2.0F;
      this.cJ = false;
      this.cK = false;
      this.cL = false;
      this.cM = true;
      this.cN = 1;
      this.cO = false;
      this.dh = 0.0D;
      this.di = 40.0D;
      this.dk = 0.0D;
      this.dl = 0.0D;
      this.dw = 0;
      this.dx = false;
      this.dy = 0;
      this.dz = 0;
      this.dA = false;
      this.da = GunAnimationReload.class;
      this.cV = (float)var2;
      //TODO: RPM Spoofing
      //this.cV = 99999999.0F;
      this.dj = (double)((int)(60.0D / var2 * 40.0D));
      this.di = (double)((int)(var4 * 20.0D));
      this.cU = var6 * 0.75F;
      //TODO: Hip Accuracy
      //this.cU = 9999999.0F;
      this.cT = var7;
      //TODO: No Recoil
      //this.cT = 0.0D;
      this.setMaxStackSize(1);
      this.setCreativeTab(ItemManager.c);
   }

   public synchronized void a(World var1, EntityPlayer var2, ItemStack var3) {
      Minecraft var4 = FMLClientHandler.instance().getClient();
      if(this.dz > 0) {
         --this.dz;
      }

      if(dm > 0.0D) {
         --dm;
      }

      if(var4.currentScreen != null) {
         this.dz = 5;
      }

      if(var3 != null && var3.getItem() instanceof ItemGun && var4.currentScreen == null && var4.thePlayer.getCurrentEquippedItem() == var3) {
         duu = dn;
         dn = Mouse.isButtonDown(Mouse.getButtonIndex("BUTTON0"));
         dq = dp;
         dp = Keyboard.isKeyDown(KeyBindingManager.a.keyCode);
         ds = dr;
         dr = Keyboard.isKeyDown(KeyBindingManager.b.keyCode);
         du = dt;
         dt = Mouse.isButtonDown(Mouse.getButtonIndex("BUTTON1"));
         PlayerData var5 = PlayerDataHandler.a(var2);
         byte var6 = 6;
         if(dv <= var6) {
            ++dv;
         }

         if(this.dx) {
            var5.c = false;
            PacketDispatcher.sendPacketToServer(CDAPacketGunMinigun.a(dt));
            if(dt) {
               if(this.dy-- <= 0) {
                  Minecraft.getMinecraft().sndManager.playSoundFX("craftingdead:minigunbarrel", 1.0F, 1.0F);
                  this.dy = 18;
               }
            } else {
               this.dy = 0;
            }
         }

         if(var5.Q) {
            var5.c = false;
            dv = 0;
            return;
         }

         if(var2.isSprinting()) {
            var5.c = false;
            dv = 0;
            return;
            //TODO: I’m guessing if you just make this if bloack return without it setting the bool it would let you shoot while sprinting changing the block would look like this:
         //return;
         }

         if(this.cH && dt && !du && dv > var6) {
            var5.c = !var5.c;
            dv = 0;
         }

         if(dp && !dq) {
            this.c(var2, var3);
            dm = 5.0D;
            return;
         }

         if(cP && !cQ && AnimationManager.d().c() == null && this.db != null) {
            GunAnimation var7 = this.j();
            AnimationManager.d().b();
            AnimationManager.d().a(var7);
            return;
         }

         if(this.dh < this.dj) {
            ++this.dh;
         }

         if(this.dw != var2.inventory.currentItem) {
            this.g();
            this.dw = var2.inventory.currentItem;
         }

         if(dr && !ds) {
            this.d();
         }

         if(!this.a(this.e())) {
            this.de = this.dd[0];
         }

         if(!this.q(var3) || this.v(var3) > 1) {
            if(this.dh >= this.dj && dn && !duu) {
               Minecraft.getMinecraft().sndManager.playSoundFX("craftingdead:" + this.cW, 1.0F, 1.0F);
               this.dh = 0.0D;
            }

            return;
         }

         if(this.dx && !this.p(var3)) {
            return;
         }

         if(this.dz > 0) {
            return;
         }

         if(dm > 0.0D) {
            return;
         }

         if(this.e() == EnumFireMode.a) {
            if(dn && this.dh >= this.dj) {
               this.b(var2, var3);
               this.dh = 0.0D;
            }
         } else if(this.e() == EnumFireMode.b) {
            if(this.dh >= this.dj && dn && !duu) {
               this.b(var2, var3);
               this.dh = 0.0D;
            }
         } else if(this.e() == EnumFireMode.c) {
            if(this.dl > 0.0D) {
               --this.dl;
               return;
            }

            if(this.dh >= this.dj && dn && !duu && this.dk <= 0.0D) {
               this.dk = 9.0D;
            }

            if(this.dk > 0.0D) {
               if(this.dk % 3.0D == 0.0D) {
                  this.b(var2, var3);
                  if(this.dk == 3.0D) {
                     this.dl = 10.0D;
                  }
               }

               --this.dk;
            }
         }
      }

   }

   public void b(EntityPlayer var1, ItemStack var2) {
      if(this.q(var2)) {
         if(this.w(var2) != null) {
            if(this.cJ) {
               ClientTickHandler.i = 3.5D;
            }

            PacketDispatcher.sendPacketToServer(CDAPacketGunTrigger.b());
            if(CraftingDead.l().d() != null) {
               float var3 = this.cT;
               if(this.d(var2, 1) != null) {
                  var3 += this.d(var2, 1).c;
               }

               CraftingDead.l().d().a(var3);

               for(int var4 = 0; var4 < this.cN; ++var4) {
                  BulletSpawner.a().a(var1, this);
               }

               if(this.cZ != null) {
                  GunAnimation var6 = this.k();
                  AnimationManager.d().b();
                  AnimationManager.d().a(var6);
               }
            }

            PlayerData var5 = PlayerDataHandler.a(var1);
            var5.a(this.cU);
         }
      }
   }

   public void c(EntityPlayer var1, ItemStack var2) {
      if(this.v(var2) <= 0) {
         PacketDispatcher.sendPacketToServer(CDAPacketGunReload.b());
         AnimationManager.d().b();
         AnimationManager.d().a((GunAnimation)this.b(this.w(var2) != null));
         PlayerDataHandler.b().c = false;
      }

   }

   public void onUpdate(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      EntityPlayer var6 = (EntityPlayer)var3;
      if(var6 == null && this.p(var1)) {
         this.a(var1, false);
      }

      if(var6 != null && var6.getCurrentEquippedItem() != null && var6.getCurrentEquippedItem() == var1 && !var2.isRemote) {
         int var7 = this.v(var1);
         if(var7 > 0) {
            --var7;
            this.c(var1, var7);
         }

         this.C(var1);
      }

   }

   public void c(ItemStack var1, World var2, EntityPlayer var3) {
      if(this.q(var1) && this.w(var1) != null || var3.capabilities.isCreativeMode) {
         PlayerData var4 = PlayerDataHandler.a(var3);
         ItemStack var5 = this.e(var1, var2, var3);
         var3.inventory.setInventorySlotContents(var3.inventory.currentItem, var5);
         float var6 = this.cY;
         if(this.b(var5, GunAttachment.l)) {
            var6 = 0.2F;
            if(var4.u < 0.2F) {
               var4.b(var6);
            }

            return;
         }

         var4.b(var6);
      }

   }

   public ItemStack e(ItemStack var1, World var2, EntityPlayer var3) {
      if(!var2.isRemote) {
         this.B(var1);
         if(!var3.capabilities.isCreativeMode) {
            int var4 = this.r(var1);
            this.b(var1, var4 - 1);
            if(this.r(var1) == 0) {
               EventGunOutOfAmmo var5 = new EventGunOutOfAmmo(var1);
               MinecraftForge.EVENT_BUS.post(var5);
               if(this.cL) {
                  this.y(var1);
               }
            }
         }

         String var6 = "craftingdead:" + this.cD;
         if(this.d(var1, 2) != null) {
            var6 = "craftingdead:" + this.cE;
         }

         var2.playSoundAtEntity(var3, var6, 1.0F, 1.0F);
         EventGunFired var7 = new EventGunFired(var3.username, var1);
         MinecraftForge.EVENT_BUS.post(var7);
      }

      return var1;
   }

   public void a(EntityPlayer var1, World var2, ItemStack var3) {
      InventoryPlayer var4 = var1.inventory;
      EventGunReload var5 = new EventGunReload(this);
      ItemStack var6 = this.w(var3);
      if(this.v(var3) <= 0) {
         if(var6 != null) {
            var5.a(var6);
            if(!MinecraftForge.EVENT_BUS.post(var5)) {
               if(var4.getFirstEmptyStack() != -1) {
                  var4.addItemStackToInventory(var6);
               } else {
                  var1.dropPlayerItem(var6);
               }

               this.y(var3);
               this.c(var3, (int)(this.di / 2.0D));
               this.c(var3, false);
               this.d(var3, false);
               this.e(var3, false);
            }
         } else {
            ItemStack var7 = this.a(var1);
            if(var7 != null) {
               var5.b(var7);
               if(!MinecraftForge.EVENT_BUS.post(var5)) {
                  ItemStack var8 = var5.a();
                  this.a(var1, var3, var8);
                  var2.playSoundAtEntity(var1, "craftingdead:" + this.cF, 1.0F, 1.0F);
                  this.c(var3, (int)this.di);
               }
            } else if(var1.capabilities.isCreativeMode) {
               this.a(var1, var3, new ItemStack(Item.itemsList[this.dc[0]]));
               var2.playSoundAtEntity(var1, "craftingdead:" + this.cF, 1.0F, 1.0F);
            } else {
               MinecraftForge.EVENT_BUS.post(var5);
            }
         }
      }
   }

   public void a(EntityPlayer var1, World var2, ItemStack var3, boolean var4) {
      if(!var2.isRemote) {
         this.a(var3, var4);
         this.b(var3, this.n(var3));
         if(var4) {
            this.a(var3, this.n(var3) + 50.0F);
         }
      }

   }

   protected void d() {
      Minecraft.getMinecraft().sndManager.playSoundFX("craftingdead:" + this.cX, 1.0F, 1.0F);
      if(this.dd.length > 1) {
         for(int var1 = 0; var1 < this.dd.length; ++var1) {
            if(this.dd[var1] == this.de) {
               if(var1 + 1 < this.dd.length) {
                  this.de = this.dd[var1 + 1];
                  break;
               }

               this.de = this.dd[0];
            }
         }
      }

      CraftingDead.l().d().a(this.de);
   }

   protected boolean a(EnumFireMode var1) {
      for(int var2 = 0; var2 < this.dd.length; ++var2) {
         if(this.dd[var2] == var1) {
            return true;
         }
      }

      return false;
   }

   public EnumFireMode e() {
      if(this.de == null) {
         this.de = this.dd[0];
      }

      if(this.dd.length <= 1) {
         this.de = this.dd[0];
      }

      return this.de;
   }

   protected void g() {
      this.de = this.dd[0];
   }

   public void a(ItemStack var1, float var2) {
      NBTTagCompound var3 = this.I(var1);
      var3.setFloat("barRot", var2);
   }

   public float n(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.getFloat("barRot");
   }

   public void b(ItemStack var1, float var2) {
      NBTTagCompound var3 = this.I(var1);
      var3.setFloat("lastBarRot", var2);
   }

   public float o(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.getFloat("lastBarRot");
   }

   public void a(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.I(var1);
      var3.setBoolean("minigunBarrel", var2);
   }

   public boolean p(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.hasKey("minigunBarrel")?var2.getBoolean("minigunBarrel"):false;
   }

   public boolean q(ItemStack var1) {
      return this.r(var1) > 0;
   }

   public int r(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2 != null?(var2.hasKey("gunAmmo")?var2.getInteger("gunAmmo"):var2.getInteger("gunAmmo")):0;
   }

   protected void b(ItemStack var1, int var2) {
      NBTTagCompound var3 = this.I(var1);
      if(var3 != null) {
         var3.setInteger("gunAmmo", var2);
      }

   }

   private void c(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.I(var1);
      var3.setBoolean("infectedRounds", var2);
   }

   public boolean s(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.hasKey("infectedRounds")?var2.getBoolean("infectedRounds"):false;
   }

   private void d(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.I(var1);
      var3.setBoolean("incendiaryRounds", var2);
   }

   public boolean t(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.hasKey("incendiaryRounds")?var2.getBoolean("incendiaryRounds"):false;
   }

   private void e(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.I(var1);
      var3.setBoolean("explosiveRounds", var2);
   }

   public boolean u(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.hasKey("explosiveRounds")?var2.getBoolean("explosiveRounds"):false;
   }

   public void c(ItemStack var1, int var2) {
      NBTTagCompound var3 = this.I(var1);
      if(var3 != null) {
         var3.setInteger("gunreloading", var2);
      }

   }

   public int v(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2 != null?var2.getInteger("gunreloading"):0;
   }

   public ItemStack w(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2 != null && var2.hasKey("gunLoadedClip")) {
         int var3 = var2.getInteger("gunLoadedClip");
         if(var3 != 0) {
            ItemAmmo var4 = (ItemAmmo)Item.itemsList[var3];
            ItemStack var5 = new ItemStack(var4);
            int var6 = this.r(var1);
            var5.setItemDamage(var4.d - var6);
            var4.c(var5, this.s(var1));
            var4.b(var5, this.u(var1));
            var4.a(var5, this.t(var1));
            return var5;
         }
      }

      return null;
   }

   public ItemAmmo x(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2 != null && var2.hasKey("gunLoadedClip")) {
         int var3 = var2.getInteger("gunLoadedClip");
         if(var3 != 0) {
            return (ItemAmmo)Item.itemsList[var3];
         }
      }

      return null;
   }

   public void a(EntityPlayer var1, ItemStack var2, ItemStack var3) {
      NBTTagCompound var4 = this.I(var2);
      if(var4 != null && var3 != null && this.b(var3.getItem())) {
         var4.setInteger("gunLoadedClip", var3.getItem().itemID);
         ItemAmmo var5 = (ItemAmmo)var3.getItem();
         this.b(var2, var5.d - var3.getItemDamage());
         this.c(var2, var5.c(var3));
         this.d(var2, var5.a(var3));
         this.e(var2, var5.b(var3));
      }

   }

   public boolean b(Item var1) {
      for(int var2 = 0; var2 < this.dc.length; ++var2) {
         if(this.dc[var2] == var1.itemID) {
            return true;
         }
      }

      return false;
   }

   public int i() {
      return this.dc[0];
   }

   public void y(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2 != null) {
         var2.setInteger("gunLoadedClip", 0);
         this.b(var1, 0);
      }

   }

   public ItemStack a(EntityPlayer var1) {
      InventoryPlayer var2 = var1.inventory;

      ItemStack var4;
      for(int var3 = 0; var3 < var2.getSizeInventory(); ++var3) {
         var4 = var2.getStackInSlot(var3);
         if(var4 != null && var4.getItem() instanceof ItemAmmo && this.b(var4.getItem()) && !this.z(var4)) {
            var2.setInventorySlotContents(var3, (ItemStack)null);
            return var4.copy();
         }
      }

      PlayerData var9 = PlayerDataHandler.a(var1);
      var4 = var9.d().a("vest");
      if(var4 != null && var4.itemID == ItemManager.dz.itemID) {
         IInventory var5 = ItemTacticalVest.a(var1);
         if(var5 != null) {
            for(int var6 = 0; var6 < var5.getSizeInventory(); ++var6) {
               ItemStack var7 = var5.getStackInSlot(var6);
               if(var7 != null && var7.getItem() instanceof ItemAmmo && this.b(var7.getItem()) && !this.z(var7)) {
                  var5.setInventorySlotContents(var6, (ItemStack)null);
                  return var7.copy();
               }
            }
         }
      }

      ItemStack var10 = var9.d().a("backpack");
      if(var10 != null && var10.itemID == ItemManager.dt.itemID) {
         IInventory var11 = ItemBackpack.a(var1);
         if(var11 != null) {
            for(int var12 = 0; var12 < var11.getSizeInventory(); ++var12) {
               ItemStack var8 = var11.getStackInSlot(var12);
               if(var8 != null && var8.getItem() instanceof ItemAmmo && this.b(var8.getItem()) && !this.z(var8)) {
                  var11.setInventorySlotContents(var12, (ItemStack)null);
                  return var8.copy();
               }
            }
         }
      }

      return null;
   }

   protected boolean z(ItemStack var1) {
      ItemAmmo var2 = (ItemAmmo)var1.getItem();
      return var1.getItemDamage() == var2.d;
   }

   public GunAnimation j() {
      try {
         return (GunAnimation)this.db.newInstance();
      } catch (InstantiationException var2) {
         var2.printStackTrace();
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
      }

      return null;
   }

   public GunAnimation k() {
      try {
         return (GunAnimation)this.cZ.newInstance();
      } catch (InstantiationException var2) {
         var2.printStackTrace();
      } catch (IllegalAccessException var3) {
         var3.printStackTrace();
      }

      return null;
   }

   public GunAnimationReload b(boolean var1) {
      try {
         GunAnimationReload var2 = (GunAnimationReload)this.da.newInstance();
         var2.b(var1);
         return var2;
      } catch (InstantiationException var3) {
         var3.printStackTrace();
      } catch (IllegalAccessException var4) {
         var4.printStackTrace();
      }

      return null;
   }

   public boolean A(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.hasKey("fired")?var2.getInteger("fired") > 0:false;
   }

   public void B(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      var2.setInteger("fired", 5);
   }

   public void C(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2.getInteger("fired") > 0) {
         var2.setInteger("fired", var2.getInteger("fired") - 1);
      }

   }

   public void a(ItemStack var1, GunAttachment var2) {
      if(var2 != null && this.a(var2)) {
         int var3 = var2.o?0:(var2.p?1:2);
         NBTTagCompound var4 = this.I(var1);
         if(var4 != null && this.d(var1, var3) == null) {
            var4.setInteger("attachmentSlot" + var3, var2.m);
         }
      }

   }

   public GunAttachment d(ItemStack var1, int var2) {
      NBTTagCompound var3 = this.I(var1);
      if(var3 != null && var3.hasKey("attachmentSlot" + var2)) {
         int var4 = var3.getInteger("attachmentSlot" + var2);
         GunAttachment var5 = GunAttachment.a[var4];
         return var5;
      } else {
         return null;
      }
   }

   public void D(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2 != null) {
         for(int var3 = 0; var3 < 3; ++var3) {
            var2.setInteger("attachmentSlot" + var3, 0);
         }
      }

   }

   public boolean b(ItemStack var1, GunAttachment var2) {
      NBTTagCompound var3 = this.I(var1);
      if(var3 != null) {
         int var4 = var2.o?0:(var2.p?1:2);
         if(this.d(var1, var4) != null) {
            return true;
         }
      }

      return false;
   }

   public boolean E(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2 != null) {
         for(int var3 = 0; var3 < 3; ++var3) {
            if(this.d(var1, var3) != null) {
               return true;
            }
         }
      }

      return false;
   }

   public boolean a(GunAttachment var1) {
      for(int var2 = 0; var2 < this.df.length; ++var2) {
         if(this.df[var2] == var1) {
            return true;
         }
      }

      return false;
   }

   public void e(ItemStack var1, int var2) {
      if(var2 == 0 || var2 == 1 || var2 == 2) {
         NBTTagCompound var3 = this.I(var1);
         if(var3 != null) {
            var3.setInteger("attachmentSlot" + var2, 0);
         }
      }

   }

   public void a(ItemStack var1, GunPaint var2, ItemStack var3) {
      NBTTagCompound var4 = this.I(var1);
      if(var4 != null && var2 != null && this.a(var2)) {
         var4.setInteger("gunSkin", var2.q);
         if(var2 == GunPaint.k) {
            int[] var5 = ((ItemPaintMultiColor)var3.getItem()).c(var3);
            var4.setInteger("gunSkinR", var5[0]);
            var4.setInteger("gunSkinG", var5[1]);
            var4.setInteger("gunSkinB", var5[2]);
         } else {
            var4.removeTag("gunSkinR");
            var4.removeTag("gunSkinG");
            var4.removeTag("gunSkinB");
         }
      }

   }

   public GunPaint F(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      if(var2 != null && var2.hasKey("gunSkin")) {
         int var3 = var2.getInteger("gunSkin");
         return GunPaint.a[var3];
      } else {
         return null;
      }
   }

   public int[] G(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      int[] var3 = new int[3];
      GunPaint var4 = this.F(var1);
      if(var4 != null && var4 == GunPaint.k) {
         var3[0] = var2.getInteger("gunSkinR");
         var3[1] = var2.getInteger("gunSkinG");
         var3[2] = var2.getInteger("gunSkinB");
      }

      return var3;
   }

   public void H(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      var2.setString("gunSkin", "");
   }

   public boolean a(GunPaint var1) {
      if(var1 == GunPaint.k) {
         return true;
      } else {
         for(int var2 = 0; var2 < this.dg.length; ++var2) {
            if(var1.q == this.dg[var2].q) {
               return true;
            }
         }

         return false;
      }
   }

   protected NBTTagCompound I(ItemStack var1) {
      String var2 = "cdagun";
      if(var1.stackTagCompound == null) {
         var1.setTagCompound(new NBTTagCompound());
      }

      if(!var1.stackTagCompound.hasKey(var2)) {
         var1.stackTagCompound.setTag(var2, new NBTTagCompound(var2));
      }

      return (NBTTagCompound)var1.stackTagCompound.getTag(var2);
   }

   public ItemGun a(int ... var1) {
      this.dc = var1;
      return this;
   }

   public ItemGun a(String var1, String var2, String var3, String var4) {
      this.cD = var1;
      this.cE = var2;
      this.cF = var3;
      return this;
   }

   public ItemGun b(int var1) {
      this.cN = var1;
      return this;
   }

   public ItemGun c(boolean var1) {
      this.cM = var1;
      return this;
   }

   public ItemGun a(double var1) {
      this.cB = var1 > 1.0D?1.0D:(var1 < 0.0D?0.0D:var1);
      return this;
   }

   public ItemGun a(float var1) {
      this.cC = var1;
      return this;
   }

   public ItemGun b(int var1, int var2) {
      this.cR = var1;
      this.cS = var2;
      return this;
   }

   public ItemGun c(int var1) {
      this.d = var1;
      return this;
   }

   public ItemGun a(GunAttachment ... var1) {
      this.df = var1;
      return this;
   }

   public ItemGun a(EnumFireMode ... var1) {
      this.dd = var1;
      return this;
   }

   public ItemGun b(float var1) {
      this.cY = var1;
      return this;
   }

   public ItemGun r() {
      this.cK = true;
      return this;
   }

   public ItemGun B() {
      this.dA = true;
      return this;
   }

   public ItemGun c(float var1) {
      this.cI = var1;
      return this;
   }

   public ItemGun a(Class var1) {
      this.cZ = var1;
      return this;
   }

   public ItemGun b(Class var1) {
      this.da = var1;
      return this;
   }

   public ItemGun c(Class var1) {
      this.db = var1;
      return this;
   }

   public ItemGun C() {
      this.dx = true;
      return this;
   }

   public ItemGun D() {
      this.cJ = true;
      return this;
   }

   public ItemGun d(boolean var1) {
      this.cH = var1;
      return this;
   }

   public ItemGun d(Class var1) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
         try {
            IItemRenderer var2 = (IItemRenderer)var1.newInstance();
            MinecraftForgeClient.registerItemRenderer(super.itemID, var2);
            cG.put(this, (RenderGun)var2);
         } catch (InstantiationException var3) {
            var3.printStackTrace();
         } catch (IllegalAccessException var4) {
            var4.printStackTrace();
         }
      }

      return this;
   }

   public ItemGun a(GunPaint ... var1) {
      this.dg = var1;
      return this;
   }

   public ItemGun E() {
      this.cO = true;
      return this;
   }

   public ItemGun F() {
      this.cL = true;
      return this;
   }

   public int b(ItemStack var1, boolean var2) {
      int var3 = var2?this.cS:this.cR;
      if(this.d(var1, 2) != null) {
         var3 += this.d(var1, 2).b;
      }

      return var3;
      //TODO: Damage Spoofing (not tested)
      //return 999999;
   }

   public float G() {
      return this.cU;
   }

   public String getItemDisplayName(ItemStack var1) {
      if(this.F(var1) != null) {
         String var2 = this.F(var1).b();
         return EnumChatFormatting.RED + "" + EnumChatFormatting.ITALIC + super.b + " " + var2;
      } else {
         return super.getItemDisplayName(var1);
      }
   }

   public boolean getShareTag() {
      return true;
   }

   public boolean hasEffect(ItemStack var1, int var2) {
      return this.E(var1)?true:this.F(var1) != null;
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      String var5;
      if(Keyboard.isKeyDown(19)) {
         var5 = "Ammo " + EnumChatFormatting.RED + this.r(var1);
         var3.add(var5);
         if(this.s(var1)) {
            var3.add(EnumChatFormatting.RED + "Infected Rounds");
         }

         if(this.t(var1)) {
            var3.add(EnumChatFormatting.GOLD + "Incendiary Rounds");
         }

         if(this.u(var1)) {
            var3.add(EnumChatFormatting.RED + "Explosive Rounds");
         }

         var5 = "Damage " + EnumChatFormatting.RED + this.b(var1, false);
         var3.add(var5);
         if(this.cN > 1) {
            var5 = "Pellets Shot " + EnumChatFormatting.RED + this.cN;
            var3.add(var5);
         }

         var5 = "Head Shot " + EnumChatFormatting.RED + this.b(var1, true);
         var3.add(var5);

         for(int var6 = 0; var6 < 3; ++var6) {
            if(this.d(var1, var6) != null) {
               String var7 = "Attachment " + EnumChatFormatting.RED + this.d(var1, var6).d();
               var3.add(var7);
            }
         }

         var5 = "RPM " + EnumChatFormatting.RED + this.cV;
         var3.add(var5);
         var5 = "Hip Accuracy " + EnumChatFormatting.RED + this.cU;
         var3.add(var5);
         var5 = "Recoil " + EnumChatFormatting.RED + this.cT;
         var3.add(var5);
         var5 = "Holding Movement " + EnumChatFormatting.RED + (100.0D - 100.0D * this.cB) + "%";
         var3.add(var5);
         var5 = "Carrying Movement " + EnumChatFormatting.RED + (100.0D - 100.0D * (this.cB / 2.0D)) + "%";
         var3.add(var5);
      } else {
         var5 = "Press \'R\' For More Info!";
         var3.add(var5);
      }

   }

   public boolean onEntitySwing(EntityLivingBase var1, ItemStack var2) {
      return true;
   }

   public boolean onLeftClickEntity(ItemStack var1, EntityPlayer var2, Entity var3) {
      return true;
   }

   public boolean onBlockStartBreak(ItemStack var1, int var2, int var3, int var4, EntityPlayer var5) {
      return true;
   }

   public static ItemGun H() {
      return (ItemGun)ItemManager.k;
   }

   public boolean a(EntityPlayer var1, ItemStack var2) {
      return true;
   }

   public double c(ItemStack var1) {
      return this.cB;
   }

   public float I() {
      return this.cC;
   }

   public String a(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      return var2.getString("antidupe");
   }

   public boolean h_() {
      return this.dA;
   }

   public void b(ItemStack var1) {
      NBTTagCompound var2 = this.I(var1);
      var2.setString("antidupe", UUID.randomUUID().toString());
   }

}
