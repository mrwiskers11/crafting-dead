package com.craftingdead.item.gun;

import com.craftingdead.client.model.attachment.ModelAttachmentACOG;
import com.craftingdead.client.model.attachment.ModelAttachmentBipod;
import com.craftingdead.client.model.attachment.ModelAttachmentEOTECH;
import com.craftingdead.client.model.attachment.ModelAttachmentGrip;
import com.craftingdead.client.model.attachment.ModelAttachmentReddot;
import com.craftingdead.client.model.attachment.ModelAttachmentScope;
import com.craftingdead.client.model.attachment.ModelAttachmentScope2;
import com.craftingdead.client.model.attachment.ModelAttachmentSuppressor;
import com.craftingdead.item.ItemManager;

import net.minecraft.client.model.ModelBase;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;

public class GunAttachment {

   public static GunAttachment[] a = new GunAttachment[24];
   public int b = 0;
   public float c = 0.0F;
   public float d = 1.0F;
   public static GunAttachment e = (new GunAttachment(1, "Red Dot", ItemManager.aF.itemID)).a(new ModelAttachmentReddot(), "attachmentreddot").a(2.5F);
   public static GunAttachment f = (new GunAttachment(2, "ACOG", ItemManager.aG.itemID)).a(new ModelAttachmentACOG(), "attachmentacog").a(3.25F);
   public static GunAttachment g = (new GunAttachment(3, "EOTech", ItemManager.aH.itemID)).a(new ModelAttachmentEOTECH(), "attachmenteotech").a(2.5F);
   public static GunAttachment h = (new GunAttachment(4, "LP Scope", ItemManager.aI.itemID)).a(new ModelAttachmentScope(), "attachmentscope").a(5.0F).a("scope1");
   public static GunAttachment i = (new GunAttachment(5, "HP Scope", ItemManager.aJ.itemID)).a(new ModelAttachmentScope2(), "attachmentscope2").a(8.0F).a("scope2");
   public static GunAttachment j = (new GunAttachment(10, "Tactical Grip", ItemManager.aK.itemID)).a(new ModelAttachmentGrip(), "attachmentgrip").b().b(-1.0F);
   public static GunAttachment k = (new GunAttachment(11, "Bipod", ItemManager.aM.itemID)).a(new ModelAttachmentBipod(), "attachmentbipod").b().b(-2.0F);
   public static GunAttachment l = (new GunAttachment(20, "Suppressor", ItemManager.aL.itemID)).a(new ModelAttachmentSuppressor(), "attachmentsuppressor").a().a(-2);
   public int m;
   public String n;
   public boolean o = false;
   public boolean p = false;
   public boolean q = false;
   public ModelBase r;
   public ResourceLocation s;
   public int t;
   public ResourceLocation u;


   public GunAttachment(int var1, String var2, int var3) {
      this.m = var1;
      this.n = var2;
      this.t = var3;
      a[var1] = this;
   }

   public GunAttachment a(ModelBase var1, String var2) {
      this.s = new ResourceLocation("craftingdead", "textures/models/attachments/" + var2 + ".png");
      this.r = var1;
      return this;
   }

   public GunAttachment a(String var1) {
      this.u = new ResourceLocation("craftingdead", "textures/misc/sights/" + var1 + ".png");
      return this;
   }

   public GunAttachment a(float var1) {
      this.d = var1;
      this.o = true;
      return this;
   }

   public GunAttachment a() {
      this.q = true;
      return this;
   }

   public GunAttachment b() {
      this.p = true;
      return this;
   }

   public GunAttachment b(float var1) {
      this.c = var1;
      return this;
   }

   public GunAttachment a(int var1) {
      this.b = var1;
      return this;
   }

   public static GunAttachment b(int var0) {
      if(var0 != 0) {
         for(int var1 = 0; var1 < a.length; ++var1) {
            if(a[var1] != null && a[var1].t == var0) {
               return a[var1];
            }
         }
      }

      return null;
   }

   public static Item a(GunAttachment var0) {
      return var0 != null && var0.t != 0?Item.itemsList[var0.t]:null;
   }

   public int c() {
      return this.m;
   }

   public String d() {
      return this.n;
   }

}
