package com.craftingdead.item.gun;


public class WallBangable {

   public int a;
   public double b;
   public boolean c = true;


   public WallBangable(int var1, double var2) {
      this.a = var1;
      this.b = var2;
   }

   public WallBangable(int var1, double var2, boolean var4) {
      this.a = var1;
      this.b = var2;
      this.c = var4;
   }
}
