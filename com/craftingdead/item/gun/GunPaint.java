package com.craftingdead.item.gun;

import java.lang.reflect.Field;

public class GunPaint {

   public static GunPaint[] a = new GunPaint[128];
   public static GunPaint b = new GunPaint(1, "Asmo", "asmo");
   public static GunPaint c = new GunPaint(2, "Cyrex", "cyrex");
   public static GunPaint d = new GunPaint(3, "Vulcan", "vulcan");
   public static GunPaint e = new GunPaint(4, "Gem", "gem");
   public static GunPaint f = new GunPaint(5, "Faded", "fade");
   public static GunPaint g = new GunPaint(6, "Ruby", "ruby");
   public static GunPaint h = new GunPaint(7, "UV", "uv");
   public static GunPaint i = new GunPaint(8, "Candy Apple", "candyapple");
   public static GunPaint j = new GunPaint(9, "Dragon", "dragon");
   public static GunPaint k = new GunPaint(10, "Multi", "multi");
   public static GunPaint l = new GunPaint(11, "Fury", "fury");
   public static GunPaint m = new GunPaint(12, "Slaughter", "slaughter");
   public static GunPaint n = new GunPaint(13, "Diamond", "diamond");
   public static GunPaint o = new GunPaint(14, "Inferno", "inferno");
   public static GunPaint p = new GunPaint(15, "Scorched", "scorched");
   private String r;
   private String s;
   public int q;


   public GunPaint(int var1) {
      this.q = var1;
      a[var1] = this;
   }

   public GunPaint(int var1, String var2, String var3) {
      this(var1);
      this.r = var2;
      this.s = var3;
   }

   public String a() {
      return this.s;
   }

   public String b() {
      return this.r;
   }

   public static GunPaint a(String var0) {
      Field[] var1 = c.getClass().getDeclaredFields();

      for(int var2 = 0; var2 < var1.length; ++var2) {
         if(var1[var2].getName().equals(var0)) {
            try {
               return (GunPaint)var1[var2].get(c);
            } catch (IllegalArgumentException var4) {
               var4.printStackTrace();
            } catch (IllegalAccessException var5) {
               var5.printStackTrace();
            }
         }
      }

      return null;
   }

}
