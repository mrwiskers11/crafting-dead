package com.craftingdead.item.gun;

import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;

public class ItemGunAttachment extends ItemCD {

   public ItemGunAttachment(int var1) {
      super(var1);
      this.setCreativeTab(ItemManager.c);
   }
}
