package com.craftingdead.item.gun;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockManager;
import com.craftingdead.entity.EntityPlayerHead;
import com.craftingdead.item.gun.BlockWallBangableHit;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.item.gun.WallBangable;
import com.craftingdead.network.packets.gun.CDAPacketBulletCollision;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class BulletSpawner {

   private Minecraft e = Minecraft.getMinecraft();
   private Entity f;
   public int a = 0;
   public double b = 0.0D;
   private ArrayList g = new ArrayList();
   private static final WallBangable[] h = new WallBangable[]{new WallBangable(Block.glass.blockID, 0.0D), new WallBangable(Block.thinGlass.blockID, 0.0D), new WallBangable(Block.planks.blockID, 50.0D), new WallBangable(BlockManager.ae.blockID, 0.0D), new WallBangable(BlockManager.ax.blockID, 50.0D), new WallBangable(BlockManager.ay.blockID, 60.0D), new WallBangable(Block.cloth.blockID, 25.0D), new WallBangable(Block.bookShelf.blockID, 50.0D), new WallBangable(Block.ice.blockID, 25.0D), new WallBangable(Block.hay.blockID, 25.0D), new WallBangable(Block.web.blockID, 0.0D), new WallBangable(Block.leaves.blockID, 0.0D), new WallBangable(Block.vine.blockID, 0.0D), new WallBangable(Block.tallGrass.blockID, 0.0D), new WallBangable(Block.signPost.blockID, 0.0D), new WallBangable(Block.signWall.blockID, 0.0D), new WallBangable(Block.fence.blockID, 0.0D), new WallBangable(Block.fenceIron.blockID, 50.0D), new WallBangable(Block.fenceGate.blockID, 0.0D), new WallBangable(Block.doorWood.blockID, 25.0D)};
   //= new WallBangable[]{new WallBangable(Block.glass.blockID, 0.0D), new WallBangable(Block.thinGlass.blockID, 0.0D), new WallBangable(Block.planks.blockID, 50.0D), new WallBangable(BlockManager.ae.blockID, 0.0D), new WallBangable(BlockManager.ax.blockID, 50.0D), new WallBangable(BlockManager.ay.blockID, 60.0D), new WallBangable(Block.cloth.blockID, 25.0D), new WallBangable(Block.bookShelf.blockID, 50.0D), new WallBangable(Block.ice.blockID, 25.0D), new WallBangable(Block.hay.blockID, 25.0D), new WallBangable(Block.web.blockID, 0.0D), new WallBangable(Block.leaves.blockID, 0.0D), new WallBangable(Block.vine.blockID, 0.0D), new WallBangable(Block.tallGrass.blockID, 0.0D), new WallBangable(Block.signPost.blockID, 0.0D), new WallBangable(Block.signWall.blockID, 0.0D), new WallBangable(Block.fence.blockID, 0.0D), new WallBangable(Block.fenceIron.blockID, 50.0D), new WallBangable(Block.fenceGate.blockID, 0.0D), new WallBangable(Block.doorWood.blockID, 25.0D)};
   public MovingObjectPosition c;
   public EntityLivingBase d;
   private float i;
   private float j;
   private static BulletSpawner k;


   public void a(EntityPlayer var1, ItemGun var2) {
      try {
         this.b(var1, var2);
      } catch (Exception var4) {
         ;
      }

   }

   public void b(EntityPlayer var1, ItemGun var2) {
      if(var1 != null && var1.worldObj != null && var1.worldObj.isRemote) {
         double var3 = (double)var2.d;
         this.a = 0;
         this.b = 0.0D;
         this.a(0.0F, var3);
         MovingObjectPosition var5 = this.c;
         if(var5 != null) {
            if(var5.entityHit != null) {
               Object var6 = var5.entityHit;
               boolean var7 = false;
               if(var6 instanceof EntityPlayerHead) {
                  this.e.sndManager.playSoundFX("random.break", 1.5F, 1.5F);
                  var6 = ((EntityPlayerHead)var6).a;
                  var7 = true;
               }

               if(var6 instanceof EntityLivingBase && var6 != var1) {
                  EntityLivingBase var8 = (EntityLivingBase)var6;
                  if(!var8.isDead && var8.getHealth() > 0.0F) {
                     BlockWallBangableHit var10;
                     if(this.g.size() > 0) {
                        for(Iterator var9 = this.g.iterator(); var9.hasNext(); this.b = this.a(var10.a).b) {
                           var10 = (BlockWallBangableHit)var9.next();
                        }
                     }

                     new CDAPacketBulletCollision();
                     PacketDispatcher.sendPacketToServer(CDAPacketBulletCollision.a(true, new Object[]{var6, Boolean.valueOf(var7), Double.valueOf(var5.hitVec.xCoord), Double.valueOf(var5.hitVec.yCoord), Double.valueOf(var5.hitVec.zCoord), Integer.valueOf((int)this.b)}));
                     this.g.clear();
                  }
               }
            } else if(var5.typeOfHit == EnumMovingObjectType.TILE) {
               World var11 = var1.worldObj;
               int var12 = var11.getBlockId(var5.blockX, var5.blockY, var5.blockZ);
               new CDAPacketBulletCollision();
               PacketDispatcher.sendPacketToServer(CDAPacketBulletCollision.a(false, new Object[]{Integer.valueOf(var12), Double.valueOf((double)var5.blockX), Double.valueOf((double)var5.blockY), Double.valueOf((double)var5.blockZ), Integer.valueOf(var5.sideHit), Double.valueOf(var5.hitVec.xCoord), Double.valueOf(var5.hitVec.yCoord), Double.valueOf(var5.hitVec.zCoord)}));
            }
         }
      }

      this.c = null;
      this.d = null;
      this.f = null;
   }

   public void a(float var1, double var2) {
      if(this.e.thePlayer != null && this.e.theWorld != null) {
         PlayerData var4 = PlayerDataHandler.b();
         this.d = null;
         this.c = this.a(this.e.thePlayer, var2, var1);
         double var7 = var2;
         Vec3 var9 = this.e.thePlayer.getPosition(var1);
         if(this.c != null) {
            var7 = this.c.hitVec.distanceTo(var9);
         }

         Vec3 var10 = this.a(!var4.c, this.e.thePlayer);
         Vec3 var11 = var9.addVector(var10.xCoord * var2, var10.yCoord * var2, var10.zCoord * var2);
         this.f = null;
         List var12 = this.e.theWorld.getEntitiesWithinAABBExcludingEntity(this.e.thePlayer, this.e.thePlayer.boundingBox.addCoord(var10.xCoord * var2, var10.yCoord * var2, var10.zCoord * var2));
         double var13 = var7;
         double var15 = var7;

         int var17;
         Entity var18;
         AxisAlignedBB var20;
         MovingObjectPosition var21;
         float var25;
         for(var17 = 0; var17 < var12.size(); ++var17) {
            var18 = (Entity)var12.get(var17);
            if(!var18.isDead && var18.canBeCollidedWith() && !(var18 instanceof EntityPlayerHead) && var11 != null) {
               if(var18 instanceof EntityLivingBase) {
                  EntityLivingBase var19 = (EntityLivingBase)var18;
                  if(var19.deathTime != 0) {
                     continue;
                  }
               }

               var25 = 0.0F;
               var20 = var18.boundingBox.expand((double)var25, (double)var25, (double)var25);
               var21 = var20.calculateIntercept(var9, var11);
               if(var20.isVecInside(var9)) {
                  if(0.0D < var13 || var13 == 0.0D) {
                     this.f = var18;
                     var13 = 0.0D;
                  }
               } else if(var21 != null) {
                  double var22 = var9.distanceTo(var21.hitVec);
                  if(var22 < var13 || var13 == 0.0D) {
                     if(var18 == this.e.thePlayer.ridingEntity && !var18.canRiderInteract()) {
                        if(var13 == 0.0D) {
                           this.f = var18;
                        }
                     } else {
                        this.f = var18;
                        var13 = var22;
                     }
                  }
               }
            }
         }

         for(var17 = 0; var17 < var12.size(); ++var17) {
            var18 = (Entity)var12.get(var17);
            if(!var18.isDead && var18.canBeCollidedWith() && var18 instanceof EntityPlayerHead) {
               var25 = var18.getCollisionBorderSize();
               var20 = var18.boundingBox.expand((double)var25, (double)var25, (double)var25);
               var21 = var20.calculateIntercept(var9, var11);
               EntityPlayerHead var26 = (EntityPlayerHead)var18;
               if(var26.a != this.e.thePlayer) {
                  if(var20.isVecInside(var9)) {
                     if(0.0D < var15 || var15 == 0.0D) {
                        this.f = var18;
                        var15 = 0.0D;
                        break;
                     }
                  } else if(var21 != null) {
                     double var23 = var9.distanceTo(var21.hitVec);
                     if(var23 < var15 || var15 == 0.0D) {
                        if(var18 != this.e.thePlayer.ridingEntity || var18.canRiderInteract()) {
                           this.f = var18;
                           var15 = var23;
                           break;
                        }

                        if(var15 == 0.0D) {
                           this.f = var18;
                           break;
                        }
                     }
                  }
               }
            }
         }

         if(this.f != null && (var13 < var7 || this.c == null || var15 < var7)) {
            this.c = new MovingObjectPosition(this.f);
            if(this.f instanceof EntityLivingBase) {
               this.d = (EntityLivingBase)this.f;
            }
         }
      }

   }

   @SideOnly(Side.CLIENT)
   public MovingObjectPosition a(EntityLivingBase var1, double var2, float var4) {
      Vec3 var5 = var1.getPosition(var4);
      boolean var6 = true;
      if(var1 instanceof EntityPlayer) {
         EntityPlayer var7 = (EntityPlayer)var1;
         if(PlayerDataHandler.a(var7).c) {
            var6 = false;
            if(var7.getCurrentEquippedItem() != null && var7.getCurrentEquippedItem().getItem() instanceof ItemGun) {
               ItemGun var8 = (ItemGun)var7.getCurrentEquippedItem().getItem();
               if(var8.cO) {
                  var6 = true;
               }
            }
         }
      }

      Vec3 var11 = this.a(var6, var1);
      Vec3 var12 = var5.addVector(var11.xCoord * var2, var11.yCoord * var2, var11.zCoord * var2);

      try {
         if(!var1.isDead && var1.worldObj != null) {
            return this.a(var1.worldObj, var5, var12);
         }
      } catch (Exception var10) {
         ;
      }

      return null;
   }

   public Vec3 a(boolean var1, EntityLivingBase var2) {
      this.i = var2.rotationYaw;
      this.j = var2.rotationPitch;
      if(var1) {
         PlayerData var7 = PlayerDataHandler.b();
         Random var8 = new Random();
         float var9 = var7.e;
         if(var8.nextInt(4) != 0) {
            float var10 = (float)var8.nextInt((int)(var9 * 100.0F)) / 100.0F;
            float var11 = var8.nextBoolean()?var10:-var10;
            this.i += var11 * 1.5F;
         }

         this.j -= (float)var8.nextInt((int)(var9 * 1.5F * 100.0F)) / 100.0F;
      }

      float var3 = MathHelper.cos(-this.i * 0.017453292F - 3.1415927F);
      float var4 = MathHelper.sin(-this.i * 0.017453292F - 3.1415927F);
      float var5 = -MathHelper.cos(-this.j * 0.017453292F);
      float var6 = MathHelper.sin(-this.j * 0.017453292F);
      return var2.worldObj.getWorldVec3Pool().getVecFromPool((double)(var4 * var5), (double)var6, (double)(var3 * var5));
   }

   public static BulletSpawner a() {
      if(k == null) {
         k = new BulletSpawner();
      }

      return k;
   }

   private MovingObjectPosition a(World var1, Vec3 var2, Vec3 var3) {
      if(!Double.isNaN(var2.xCoord) && !Double.isNaN(var2.yCoord) && !Double.isNaN(var2.zCoord) && !Double.isNaN(var3.xCoord) && !Double.isNaN(var3.yCoord) && !Double.isNaN(var3.zCoord)) {
         int var4 = MathHelper.floor_double(var3.xCoord);
         int var5 = MathHelper.floor_double(var3.yCoord);
         int var6 = MathHelper.floor_double(var3.zCoord);
         int var7 = MathHelper.floor_double(var2.xCoord);
         int var8 = MathHelper.floor_double(var2.yCoord);
         int var9 = MathHelper.floor_double(var2.zCoord);
         int var10 = var1.getBlockId(var7, var8, var9);
         int var11 = var1.getBlockMetadata(var7, var8, var9);
         Block var12 = Block.blocksList[var10];
         boolean var13;
         if(var12 != null && var12.getCollisionBoundingBoxFromPool(var1, var7, var8, var9) != null && var10 > 0 && var12.canCollideCheck(var11, false)) {
            var13 = true;
            if(this.b(var10)) {
               WallBangable var14 = this.a(var10);
               this.a = (int)((double)this.a + var14.b);
               this.g.add(new BlockWallBangableHit((double)var7, (double)var8, (double)var9, var12.blockID));
               if(this.a <= 100) {
                  if(var10 != 0) {
                     CraftingDead.l().i().a(var1, var2.xCoord, var2.yCoord, var2.zCoord, var10, 0, var2);
                  }

                  var13 = false;
               }
            }

            if(var13) {
               MovingObjectPosition var40 = var12.collisionRayTrace(var1, var7, var8, var9, var2, var3);
               if(var40 != null) {
                  return var40;
               }
            }
         }

         var10 = 200;

         while(var10-- >= 0) {
            if(Double.isNaN(var2.xCoord) || Double.isNaN(var2.yCoord) || Double.isNaN(var2.zCoord)) {
               return null;
            }

            if(var7 == var4 && var8 == var5 && var9 == var6) {
               return null;
            }

            var13 = true;
            boolean var41 = true;
            boolean var15 = true;
            double var16 = 999.0D;
            double var18 = 999.0D;
            double var20 = 999.0D;
            if(var4 > var7) {
               var16 = (double)var7 + 1.0D;
            } else if(var4 < var7) {
               var16 = (double)var7 + 0.0D;
            } else {
               var13 = false;
            }

            if(var5 > var8) {
               var18 = (double)var8 + 1.0D;
            } else if(var5 < var8) {
               var18 = (double)var8 + 0.0D;
            } else {
               var41 = false;
            }

            if(var6 > var9) {
               var20 = (double)var9 + 1.0D;
            } else if(var6 < var9) {
               var20 = (double)var9 + 0.0D;
            } else {
               var15 = false;
            }

            double var22 = 999.0D;
            double var24 = 999.0D;
            double var26 = 999.0D;
            double var28 = var3.xCoord - var2.xCoord;
            double var30 = var3.yCoord - var2.yCoord;
            double var32 = var3.zCoord - var2.zCoord;
            if(var13) {
               var22 = (var16 - var2.xCoord) / var28;
            }

            if(var41) {
               var24 = (var18 - var2.yCoord) / var30;
            }

            if(var15) {
               var26 = (var20 - var2.zCoord) / var32;
            }

            byte var34;
            if(var22 < var24 && var22 < var26) {
               if(var4 > var7) {
                  var34 = 4;
               } else {
                  var34 = 5;
               }

               var2.xCoord = var16;
               var2.yCoord += var30 * var22;
               var2.zCoord += var32 * var22;
            } else if(var24 < var26) {
               if(var5 > var8) {
                  var34 = 0;
               } else {
                  var34 = 1;
               }

               var2.xCoord += var28 * var24;
               var2.yCoord = var18;
               var2.zCoord += var32 * var24;
            } else {
               if(var6 > var9) {
                  var34 = 2;
               } else {
                  var34 = 3;
               }

               var2.xCoord += var28 * var26;
               var2.yCoord += var30 * var26;
               var2.zCoord = var20;
            }

            Vec3 var35 = var1.getWorldVec3Pool().getVecFromPool(var2.xCoord, var2.yCoord, var2.zCoord);
            var7 = (int)(var35.xCoord = (double)MathHelper.floor_double(var2.xCoord));
            if(var34 == 5) {
               --var7;
               ++var35.xCoord;
            }

            var8 = (int)(var35.yCoord = (double)MathHelper.floor_double(var2.yCoord));
            if(var34 == 1) {
               --var8;
               ++var35.yCoord;
            }

            var9 = (int)(var35.zCoord = (double)MathHelper.floor_double(var2.zCoord));
            if(var34 == 3) {
               --var9;
               ++var35.zCoord;
            }

            int var36 = var1.getBlockId(var7, var8, var9);
            int var37 = var1.getBlockMetadata(var7, var8, var9);
            Block var38 = Block.blocksList[var36];
            if(var38 != null && var38.getCollisionBoundingBoxFromPool(var1, var7, var8, var9) != null && var36 > 0 && var38.canCollideCheck(var37, false)) {
               if(this.b(var36)) {
                  WallBangable var39 = this.a(var36);
                  this.a = (int)((double)this.a + var39.b);
                  this.g.add(new BlockWallBangableHit((double)var7, (double)var8, (double)var9, var38.blockID));
                  if(this.a < 100) {
                     if(var39.c && var36 != 0) {
                        CraftingDead.l().i().a(var1, var2.xCoord, var2.yCoord, var2.zCoord, var36, 0, var2);
                     }
                     continue;
                  }
               }

               MovingObjectPosition var42 = var38.collisionRayTrace(var1, var7, var8, var9, var2, var3);
               if(var42 != null) {
                  return var42;
               }
            }
         }
      }

      return null;
   }

   private WallBangable a(int var1) {
      WallBangable[] var2 = h;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         WallBangable var5 = var2[var4];
         if(var5.a == var1) {
            return var5;
         }
      }

      return null;
   }

   private boolean b(int var1) {
      WallBangable[] var2 = h;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         WallBangable var5 = var2[var4];
         if(var5.a == var1) {
            return true;
         }
      }

      return false;
      //TODO: Legit rape mode
      //return true;
   }

}
