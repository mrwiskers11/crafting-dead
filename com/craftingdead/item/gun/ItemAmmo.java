package com.craftingdead.item.gun;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;

import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;

import net.minecraft.client.model.ModelBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemAmmo extends ItemCD {

   public int d = 1;
   public ModelBase cB;
   public String cC;
   public double cD = 10.0D;


   public ItemAmmo(int var1, int var2) {
      super(var1);
      this.d = var2;
      this.setMaxDamage(var2 + 1);
      this.setMaxStackSize(1);
      this.setCreativeTab(ItemManager.c);
   }

   public void onUpdate(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      if(var3 instanceof EntityPlayer) {
         EntityPlayer var6 = (EntityPlayer)var3;
         if(var6.getCurrentEquippedItem() == var1 && this.getDamage(var1) > this.d) {
            var6.inventory.setInventorySlotContents(var6.inventory.currentItem, (ItemStack)null);
         }
      }

   }

   public void a(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.n(var1);
      var3.setBoolean("incendiary", var2);
   }

   public boolean a(ItemStack var1) {
      NBTTagCompound var2 = this.n(var1);
      return var2.hasKey("incendiary")?var2.getBoolean("incendiary"):false;
   }

   public void b(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.n(var1);
      var3.setBoolean("explosive", var2);
   }

   public boolean b(ItemStack var1) {
      NBTTagCompound var2 = this.n(var1);
      return var2.hasKey("explosive")?var2.getBoolean("explosive"):false;
   }

   public void c(ItemStack var1, boolean var2) {
      NBTTagCompound var3 = this.n(var1);
      var3.setBoolean("infected", var2);
   }

   public boolean c(ItemStack var1) {
      NBTTagCompound var2 = this.n(var1);
      return var2.hasKey("infected")?var2.getBoolean("infected"):false;
   }

   public ItemAmmo a(ModelBase var1, String var2) {
      this.cB = var1;
      this.cC = var2;
      return this;
   }

   public ItemAmmo a(double var1) {
      this.cD = var1;
      return this;
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      int var5 = this.d - var1.getItemDamage();
      var3.add("Ammo " + EnumChatFormatting.RED + var5);
      if(this.c(var1)) {
         var3.add(EnumChatFormatting.RED + "Infected Rounds");
      }

      if(this.a(var1)) {
         var3.add(EnumChatFormatting.GOLD + "Incendiary Rounds");
      }

      if(this.b(var1)) {
         var3.add(EnumChatFormatting.RED + "Explosive Rounds");
      }

      if(this.cD > 10.0D) {
         var3.add("Armor Penetration: " + EnumChatFormatting.RED + "" + (int)this.cD + "%");
      }

   }

   public boolean hasEffect(ItemStack var1, int var2) {
      return this.c(var1)?true:(this.a(var1)?true:this.b(var1));
   }

   protected NBTTagCompound n(ItemStack var1) {
      String var2 = "cdaammo";
      if(var1.stackTagCompound == null) {
         var1.setTagCompound(new NBTTagCompound());
      }

      if(!var1.stackTagCompound.hasKey(var2)) {
         var1.stackTagCompound.setTag(var2, new NBTTagCompound(var2));
      }

      return (NBTTagCompound)var1.stackTagCompound.getTag(var2);
   }
}
