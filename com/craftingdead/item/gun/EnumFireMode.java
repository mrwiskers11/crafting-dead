package com.craftingdead.item.gun;


public enum EnumFireMode {

   a("AUTO", 0, 1),
   b("SEMI", 1, 2),
   c("BURST", 2, 3);
   public int d;
   // $FF: synthetic field
   private static final EnumFireMode[] e = new EnumFireMode[]{a, b, c};


   private EnumFireMode(String var1, int var2, int var3) {
      this.d = var3;
   }

}
