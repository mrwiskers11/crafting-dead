package com.craftingdead.item.gun;

import java.util.ArrayList;
import java.util.List;

import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.GunPaint;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public class ItemPaint extends ItemCD {

   public GunPaint d;
   private ArrayList cB = new ArrayList();


   public ItemPaint(int var1, GunPaint var2) {
      super(var1);
      this.d = var2;
      this.setCreativeTab(ItemManager.c);
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      var3.add("Available Guns");
      int var5;
      if(this.cB.size() == 0) {
         for(var5 = 0; var5 < Item.itemsList.length; ++var5) {
            if(Item.itemsList[var5] != null && Item.itemsList[var5] instanceof ItemGun && ((ItemGun)Item.itemsList[var5]).a(this.d)) {
               this.cB.add(Integer.valueOf(Item.itemsList[var5].itemID));
            }
         }
      }

      for(var5 = 0; var5 < this.cB.size(); ++var5) {
         ItemStack var6 = new ItemStack(((Integer)this.cB.get(var5)).intValue(), 1, 0);
         var3.add(EnumChatFormatting.RED + "" + var6.getDisplayName());
      }

   }
}
