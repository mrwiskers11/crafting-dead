package com.craftingdead.item;

import com.craftingdead.item.ItemCD;

public class ItemScrapMetal extends ItemCD {

   public ItemScrapMetal(int var1) {
      super(var1);
      this.setMaxStackSize(64);
      this.f("Can be used in the trader at shop");
   }
}
