package com.craftingdead.item;

import com.craftingdead.block.BlockManager;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.blueprint.ItemBlueprint;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.world.World;

public class ItemWireCutters extends ItemCD {

   public int d;
   public int cB;
   public int cC;
   public boolean cD = false;


   public ItemWireCutters(int var1) {
      super(var1);
      this.f("Hold Right Click on Barbed Wire");
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return 32;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      return var1;
   }

   public boolean onItemUse(ItemStack var1, EntityPlayer var2, World var3, int var4, int var5, int var6, int var7, float var8, float var9, float var10) {
      this.cD = false;
      var2.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      if(var3.getBlockId(var4, var5, var6) == BlockManager.ax.blockID) {
         this.d = var4;
         this.cB = var5;
         this.cC = var6;
         this.cD = true;
         if(!var3.isRemote) {
            var2.sendChatToPlayer(ChatMessageComponent.createFromText("Cutting Wire..."));
         }

         return true;
      } else {
         if(var3.getBlockId(var4, var5, var6) == BlockManager.ay.blockID && ItemBlueprint.a(var3, var2, var4, var5, var6, 7)) {
            this.d = var4;
            this.cB = var5;
            this.cC = var6;
            this.cD = true;
            if(!var3.isRemote) {
               var2.sendChatToPlayer(ChatMessageComponent.createFromText("Cutting Wire..."));
            }
         }

         return true;
      }
   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      if(this.cD) {
         var2.setBlockToAir(this.d, this.cB, this.cC);
      }

      return var1;
   }

   public EnumAction getItemUseAction(ItemStack var1) {
      return EnumAction.block;
   }
}
