package com.craftingdead.item;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockLoot;
import com.craftingdead.block.LootType;
import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.item.ItemCDBlock;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.input.Keyboard;

public class ItemCDBlockLoot extends ItemCDBlock {

   public ItemCDBlockLoot(int var1) {
      super(var1);
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      Block var5 = Block.blocksList[this.getBlockID()];
      if(CraftingDead.l().e().d && var5 != null && var5 instanceof BlockLoot) {
         LootType var6 = ((BlockLoot)var5).e();
         ArrayList var7 = var6.d();
         var3.add(EnumChatFormatting.GRAY + "Press \'" + EnumChatFormatting.AQUA + Keyboard.getKeyName(KeyBindingManager.b.keyCode) + EnumChatFormatting.GRAY + "\' to view " + var7.size() + " loot %s.");
         if(Keyboard.isKeyDown(KeyBindingManager.b.keyCode)) {
            for(int var8 = 0; var8 < var7.size(); ++var8) {
               ItemStack var9 = (ItemStack)var7.get(var8);
               var3.add(EnumChatFormatting.AQUA + "" + var6.a(var9) + "% " + EnumChatFormatting.GRAY + var9.getDisplayName() + "");
            }
         }
      }

   }
}
