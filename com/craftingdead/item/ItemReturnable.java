package com.craftingdead.item;

import com.craftingdead.block.BlockManager;
import com.craftingdead.entity.EntityCDZombie;
import com.craftingdead.item.ItemCD;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.List;
import java.util.Random;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class ItemReturnable extends ItemCD {

   protected int d;
   protected boolean cB = false;
   protected boolean cC = false;
   protected boolean cD = false;
   protected boolean cE = false;
   protected int cF;


   public ItemReturnable(int var1, int var2) {
      super(var1);
      this.d = var2;
   }

   public ItemReturnable d() {
      this.cB = true;
      return this;
   }

   public ItemReturnable e() {
      this.cE = true;
      return this;
   }

   public ItemReturnable g() {
      this.cC = true;
      return this;
   }

   public ItemReturnable b(int var1) {
      this.cD = true;
      this.cF = var1;
      return this;
   }

   public boolean itemInteractionForEntity(ItemStack var1, EntityPlayer var2, EntityLivingBase var3) {
      PlayerData var4 = PlayerDataHandler.a(var2);
      if(var4.Q) {
         return false;
      } else if(this.cC) {
         if(var3 instanceof EntityPlayer) {
            --var1.stackSize;
            if(!var2.inventory.addItemStackToInventory(new ItemStack(this.d + 256, 1, 0))) {
               var2.dropPlayerItem(new ItemStack(this.d + 256, 1, 0));
            }
         }

         return true;
      } else if(this.cD) {
         Random var5 = new Random();
         if(var3 instanceof EntityCDZombie && var5.nextInt(100) <= this.cF) {
            --var1.stackSize;
            if(!var2.inventory.addItemStackToInventory(new ItemStack(this.d + 256, 1, 0))) {
               var2.dropPlayerItem(new ItemStack(this.d + 256, 1, 0));
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      MovingObjectPosition var4 = this.getMovingObjectPositionFromPlayer(var2, var3, true);
      if(var4 == null) {
         return var1;
      } else {
         if(var4.typeOfHit == EnumMovingObjectType.TILE) {
            int var5 = var4.blockX;
            int var6 = var4.blockY;
            int var7 = var4.blockZ;
            if(!var2.canMineBlock(var3, var5, var6, var7)) {
               return var1;
            }

            if(!var3.canPlayerEdit(var5, var6, var7, var4.sideHit, var1)) {
               return var1;
            }

            if(var2.getBlockMaterial(var5, var6, var7) == Material.water && this.cB || var2.getBlockId(var5, var6, var7) == BlockManager.S.blockID && this.cE) {
               --var1.stackSize;
               if(var1.stackSize <= 0) {
                  return new ItemStack(this.d + 256, 1, 0);
               }

               if(!var3.inventory.addItemStackToInventory(new ItemStack(this.d + 256, 1, 0))) {
                  var3.dropPlayerItem(new ItemStack(this.d + 256, 1, 0));
               }
            }
         }

         return var1;
      }
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      if(this.cC) {
         var3.add("Right Click on a " + EnumChatFormatting.RED + "Player");
      }

      if(this.cD) {
         var3.add("Right Click on a " + EnumChatFormatting.RED + "Zombie");
      }

      if(this.cB) {
         var3.add("Right Click on any " + EnumChatFormatting.RED + "Water Source");
      }

      if(this.cE) {
         var3.add("Right Click on a " + EnumChatFormatting.RED + "Water Pump");
      }

   }
}
