package com.craftingdead.item;

import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.event.EventBaseMaterialHarvested;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.ItemMeleeWeapon;
import com.craftingdead.item.blueprint.ItemBlueprint;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemCDAxe extends ItemMeleeWeapon {

   public int d = 1;
   public float cB = 1.2F;
   public boolean cC = false;
   public boolean cD = false;


   public ItemCDAxe(int var1, int var2, int var3) {
      super(var1, var2, var3);
   }

   public ItemCDAxe b(int var1) {
      this.d = var1;
      return this;
   }

   public ItemCDAxe a(float var1) {
      this.cB = var1;
      return this;
   }

   public boolean onBlockStartBreak(ItemStack var1, int var2, int var3, int var4, EntityPlayer var5) {
      World var6 = var5.worldObj;
      int var7 = var6.getBlockId(var2, var3, var4);
      if(var5.capabilities.isCreativeMode) {
         return false;
      } else if(var5.getDistance((double)var2, (double)var3, (double)var4) > 5.0D) {
         return true;
      } else if(var7 != 0 && var7 == Block.wood.blockID) {
         Random var8 = new Random();
         if(!var6.isRemote) {
            int var9 = var8.nextInt(this.d) + 1;
            ItemStack var10 = new ItemStack(ItemManager.eP.itemID, var9, 0);
            EventBaseMaterialHarvested var11 = new EventBaseMaterialHarvested(this, var10);
            MinecraftForge.EVENT_BUS.post(var11);
            if(!var5.inventory.addItemStackToInventory(var10)) {
               EntityGroundItem var12 = new EntityGroundItem(var6, (double)var2, (double)var3, (double)var4);
               var12.a(var10);
               var6.spawnEntityInWorld(var12);
            } else {
               var6.playSoundAtEntity(var5, "random.pop", 0.2F, ((var8.nextFloat() - var8.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            }
         }

         this.a(var1, var5, 2);
         return true;
      } else if((var7 == 0 || !this.c(var7)) && var7 != Block.doorWood.blockID) {
         return false;
      } else {
         if(ItemBlueprint.a(var6, var5, var2, var3, var4, 7) && !var6.isRemote) {
            if(var5.isDead) {
               this.cD = true;
            }

            if(!this.cD) {
               var6.destroyBlock(var2, var3, var4, false);
            }
         }

         this.a(var1, var5, 1);
         return true;
      }
   }

   public void a(ItemStack var1, EntityPlayer var2, int var3) {
      int var4 = var1.getItemDamage();
      var1.damageItem(var3, var2);
      if(var4 + var3 >= var1.getMaxDamage()) {
         var2.playSound("random.break", 0.8F, 0.8F + var2.worldObj.rand.nextFloat() * 0.4F);
         var2.destroyCurrentEquippedItem();
      }

   }

   public float getStrVsBlock(ItemStack var1, Block var2) {
      return this.c(var2.blockID)?this.cB:1.0F;
   }

   public boolean c(int var1) {
      int[] var2 = ItemManager.i;
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         int var5 = var2[var4];
         if(var5 == var1) {
            return true;
         }
      }

      return false;
   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      var3.add("Collect wood by cutting trees down!");
      super.addInformation(var1, var2, var3, var4);
   }
}
