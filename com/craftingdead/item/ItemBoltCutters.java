package com.craftingdead.item;

import com.craftingdead.item.ItemMeleeWeapon;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemBoltCutters extends ItemMeleeWeapon {

   private int d = 0;


   public ItemBoltCutters(int var1, int var2, int var3) {
      super(var1, var2, var3);
      this.f("Right Click To Damage Handcuffs");
   }

   public boolean itemInteractionForEntity(ItemStack var1, EntityPlayer var2, EntityLivingBase var3) {
      World var4 = var2.worldObj;
      if(!var4.isRemote && var3 instanceof EntityPlayer) {
         EntityPlayer var5 = (EntityPlayer)var3;
         PlayerData var6 = PlayerDataHandler.a(var5);
         if(var6.Q) {
            int var7 = var6.R;
            short var8 = 200;
            if(var7 + this.d >= var8) {
               var5.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Your handcuffs were cut free by " + var2.getDisplayName()));
               var5.playSound("random.break", 0.2F, 0.5F);
               var6.Q = false;
               var6.R = 0;
            } else {
               var5.playSound("random.break", 0.2F, 0.5F);
               var6.R += this.d;
            }

            var1.damageItem(1, var2);
         }
      }

      return false;
   }

   public ItemBoltCutters b(int var1) {
      this.d = var1;
      return this;
   }
}
