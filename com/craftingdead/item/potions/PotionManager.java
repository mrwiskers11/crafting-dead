package com.craftingdead.item.potions;

import com.craftingdead.CraftingDead;
import com.craftingdead.damage.DamageSourceBleeding;
import com.craftingdead.damage.DamageSourceRBInfection;
import com.craftingdead.entity.EntityManager;
import com.craftingdead.item.potions.PotionBleeding;
import com.craftingdead.item.potions.PotionBrokenLeg;
import com.craftingdead.item.potions.PotionRBInfection;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.registry.LanguageRegistry;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;

public class PotionManager {

   public static Potion a;
   public static Potion b;
   public static Potion c;
   public static Potion d;
   public static Potion e;
   public static Potion f;
   public static final int g = 10;
   public static final int h = 22;


   public void a() {
      f = (new PotionBleeding(26, false, 0)).setIconIndex(0, 0).setPotionName("potion.scuba");
      a = (new PotionRBInfection(27, false, 0)).setIconIndex(0, 0).setPotionName("potion.rbinfection");
      b = (new PotionBleeding(28, false, 0)).setIconIndex(0, 0).setPotionName("potion.bleeding");
      c = (new PotionBrokenLeg(29, false, 0)).setIconIndex(0, 0).setPotionName("potion.brokenleg").func_111184_a(SharedMonsterAttributes.movementSpeed, "7107DE5E-7CE8-4532-940E-514C1F160890", -0.15000000596046448D, 2);
      d = (new PotionBleeding(30, false, 0)).setIconIndex(0, 0).setPotionName("potion.adrenaline").func_111184_a(SharedMonsterAttributes.movementSpeed, "91AEAA56-376B-4498-935B-2F7F68070635", 0.20000000298023224D, 2);
      e = (new PotionBleeding(31, false, 0)).setIconIndex(0, 0).setPotionName("potion.juggernaut").func_111184_a(SharedMonsterAttributes.movementSpeed, "7107DE5E-7CE8-4532-940E-514C1F160891", -0.15000000596046448D, 1);
      LanguageRegistry.instance().addStringLocalization("potion.rbinfection", "RB Infection");
      LanguageRegistry.instance().addStringLocalization("potion.bleeding", "Bleeding Out");
      LanguageRegistry.instance().addStringLocalization("potion.brokenleg", "Broken Leg");
      LanguageRegistry.instance().addStringLocalization("potion.adrenaline", "Adrenaline");
      LanguageRegistry.instance().addStringLocalization("potion.juggernaut", "Juggernaut");
      LanguageRegistry.instance().addStringLocalization("potion.scuba", "Scuba Diving");
   }

   public void a(EntityPlayer var1) {
      Minecraft var2 = Minecraft.getMinecraft();
      double var3 = var1.posX;
      double var5 = var1.posY;
      double var7 = var1.posZ;
      PlayerData var9 = PlayerDataHandler.a(var1);
      Random var10 = new Random();
      if(var1 != null && var1.username != null) {
         if(!var1.username.equals(var2.thePlayer.username)) {
            ++var5;
         }

         byte var11 = 15;
         int var12 = CraftingDead.l().e().a;
         if(var12 == 0) {
            var11 = 2;
         }

         if(var12 == 1) {
            var11 = 8;
         }

         if(var12 == 2) {
            var11 = 16;
         }

         if(var12 == 3) {
            var11 = 32;
         }

         int var13;
         double var14;
         double var16;
         double var18;
         double var20;
         double var22;
         if((var9.s || var1.worldObj.isRemote && var1.username.equals(var2.thePlayer.username) && var1.isPotionActive(a)) && var9.p-- <= 0) {
            for(var13 = 0; var13 < var11; ++var13) {
               var14 = var10.nextDouble();
               var16 = var14 * (double)(0.5F - var10.nextFloat());
               var18 = var14 * (double)(0.5F - var10.nextFloat());
               var20 = var14 * (double)(0.5F - var10.nextFloat());
               var22 = -0.4D;
               EntityManager.a("tilecrack_35_5", var3 + var16, var5 + var22 + var18, var7 + var20, 0.0D, 0.0D, 0.0D);
            }

            var9.p = 10 + var10.nextInt(7);
         }

         if((var9.r || var1.worldObj.isRemote && var1.username.equals(var2.thePlayer.username) && var1.isPotionActive(b)) && var9.o-- <= 0) {
            for(var13 = 0; var13 < var11; ++var13) {
               var14 = var10.nextDouble();
               var16 = var14 * (double)(0.5F - var10.nextFloat());
               var18 = var14 * (double)(0.5F - var10.nextFloat());
               var20 = var14 * (double)(0.5F - var10.nextFloat());
               var22 = -0.4D;
               EntityManager.a("tilecrack_152_0", var3 + var16, var5 + var22 + var18, var7 + var20, 0.0D, 0.0D, 0.0D);
            }

            var9.o = 2 + var10.nextInt(7);
         }

         if((var9.t || var1.worldObj.isRemote && var1.username.equals(var2.thePlayer.username) && var1.isPotionActive(c)) && var9.q-- <= 0) {
            for(var13 = 0; var13 < 3; ++var13) {
               var14 = var10.nextDouble();
               var16 = var14 * (double)(0.5F - var10.nextFloat());
               var18 = var14 * (double)(0.5F - var10.nextFloat());
               var20 = var14 * (double)(0.5F - var10.nextFloat());
               var22 = -0.7D;
               EntityManager.a("tilecrack_152_0", var3 + var16, var5 + var22 + var18, var7 + var20, 0.0D, 0.0D, 0.0D);
            }

            var9.q = 5 + var10.nextInt(20);
         }

      }
   }

   public void a(LivingUpdateEvent var1) {
      EntityLivingBase var2 = var1.entityLiving;
      if(var2 != null && !var2.isDead && var2 instanceof EntityPlayer) {
         EntityPlayer var3 = (EntityPlayer)var2;
         PlayerData var4 = PlayerDataHandler.a(var3);
         Random var5 = new Random();
         if(var3.isPotionActive(a.id) && var4.H && var4.n-- <= 0) {
            var2.attackEntityFrom(new DamageSourceRBInfection(), 1.0F);
            var4.n = 200 + var5.nextInt(120);
         }

         if(var3.isPotionActive(f.id) && var3.isInWater()) {
            var3.setAir(300);
         }

         if(var3.isPotionActive(f.id) && var3.isInWater()) {
            var3.moveFlying(0.0F, 0.2F, 0.1F);
         }

         if(var3.isPotionActive(b.id) && var4.G && var4.m-- <= 0) {
            var2.attackEntityFrom(new DamageSourceBleeding(), 1.0F);
            var4.m = 100 + var5.nextInt(120);
         }
      }

   }
}
