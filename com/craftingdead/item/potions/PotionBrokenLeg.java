package com.craftingdead.item.potions;

import net.minecraft.potion.Potion;

public class PotionBrokenLeg extends Potion {

   protected PotionBrokenLeg(int var1, boolean var2, int var3) {
      super(var1, var2, var3);
   }
}
