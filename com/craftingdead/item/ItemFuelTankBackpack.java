package com.craftingdead.item;

import com.craftingdead.client.model.ModelFuelTanks;
import com.craftingdead.client.render.RenderFuelTanks;
import com.craftingdead.inventory.InventoryFuelTanks;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemFuelTank;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class ItemFuelTankBackpack extends ItemCD {

   private RenderFuelTanks d;


   public ItemFuelTankBackpack(int var1) {
      super(var1);
      this.setMaxStackSize(1);
      this.a(new RenderFuelTanks("modelfueltanks", new ModelFuelTanks()));
   }

   public ItemFuelTankBackpack a(RenderFuelTanks var1) {
      this.d = var1;
      return this;
   }

   public static IInventory a(EntityPlayer var0) {
      InventoryFuelTanks var1 = null;
      PlayerData var2 = PlayerDataHandler.a(var0);
      ItemStack var3 = var2.d().a("backpack");
      if(!var0.worldObj.isRemote && var3 != null && var3.getItem() instanceof ItemFuelTankBackpack) {
         var1 = new InventoryFuelTanks(var3);
      }

      return var1;
   }

   public static ItemStack b(EntityPlayer var0) {
      InventoryFuelTanks var1 = (InventoryFuelTanks)a(var0);
      if(var1 != null && !var0.worldObj.isRemote) {
         for(int var2 = 0; var2 < var1.getSizeInventory(); ++var2) {
            ItemStack var3 = var1.getStackInSlot(var2);
            if(var3 != null && var3.getItem() instanceof ItemFuelTank) {
               ItemFuelTank var4 = (ItemFuelTank)var3.getItem();
               if(var3.getItemDamage() < var4.d) {
                  return var3;
               }
            }
         }
      }

      return null;
   }

   public boolean getShareTag() {
      return true;
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      var3.add("Fuels a Flame Thrower while worn.");
   }

   public RenderFuelTanks d() {
      return this.d;
   }
}
