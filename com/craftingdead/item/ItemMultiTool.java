package com.craftingdead.item;

import com.craftingdead.item.IItemCanOpener;
import com.craftingdead.item.ItemMeleeWeapon;

public class ItemMultiTool extends ItemMeleeWeapon implements IItemCanOpener {

   public ItemMultiTool(int var1, int var2, int var3) {
      super(var1, var2, var3);
      this.f("Craft with canned goods to open them!");
   }
}
