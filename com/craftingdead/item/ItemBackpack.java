package com.craftingdead.item;

import com.craftingdead.client.render.RenderBackpack;
import com.craftingdead.inventory.InventoryBackpack;
import com.craftingdead.item.EnumBackpackSize;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.ItemManager;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import java.util.Iterator;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.input.Keyboard;

public class ItemBackpack extends ItemCD {

   private EnumBackpackSize d;
   private RenderBackpack cB;


   public ItemBackpack(int var1, EnumBackpackSize var2) {
      super(var1);
      this.d = var2;
      this.setMaxStackSize(1);
      this.setCreativeTab(ItemManager.e);
   }

   public ItemBackpack a(RenderBackpack var1) {
      this.cB = var1;
      return this;
   }

   public static IInventory a(EntityPlayer var0) {
      InventoryBackpack var1 = null;
      PlayerData var2 = PlayerDataHandler.a(var0);
      ItemStack var3 = var2.d().a("backpack");
      if(!var0.worldObj.isRemote && var3 != null && var3.getItem() instanceof ItemBackpack) {
         var1 = new InventoryBackpack(b(var3), var3);
      }

      return var1;
   }

   public static IInventory a(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemBackpack?new InventoryBackpack(b(var0), var0):null;
   }

   public static EnumBackpackSize b(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemBackpack?((ItemBackpack)var0.getItem()).e():null;
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      InventoryBackpack var5 = (InventoryBackpack)a(var1);
      if(var5 != null) {
         int var6 = var5.h().size();
         var3.add("Hoard Items! Press \'R\' for more info!");
         var3.add("Holding " + EnumChatFormatting.RED + var6 + EnumChatFormatting.GRAY + " Item(s)");
         if(Keyboard.isKeyDown(19)) {
            var3.add(EnumChatFormatting.RED + "-=-=-=- Backpack Content -=-=-=-");
            Iterator var7 = var5.h().iterator();

            while(var7.hasNext()) {
               ItemStack var8 = (ItemStack)var7.next();
               var3.add(var8.getDisplayName());
            }
         }
      }

   }

   public boolean getShareTag() {
      return true;
   }

   public RenderBackpack d() {
      return this.cB;
   }

   public EnumBackpackSize e() {
      return this.d;
   }
}
