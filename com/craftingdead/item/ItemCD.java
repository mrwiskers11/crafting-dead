package com.craftingdead.item;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.KeyBindingManager;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.LootManager;

import cpw.mods.fml.common.registry.LanguageRegistry;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

public class ItemCD extends Item {

   protected String a = "";
   protected String b = "";
   protected ArrayList c = new ArrayList();


   public ItemCD(int var1) {
      super(var1);
      this.setCreativeTab(ItemManager.a);
      this.setMaxStackSize(1);
      ItemManager.g.add(Integer.valueOf(var1));
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      return var1;
   }

   public ItemCD a(String var1) {
      this.b = var1;
      this.setUnlocalizedName(var1.replace(" ", ""));
      LanguageRegistry.addName(this, var1);
      return this;
   }

   public ItemCD e(String var1) {
      this.a = var1;
      return this;
   }

   public ItemCD f(String var1) {
      this.c.add(var1);
      return this;
   }

   public String g(ItemStack var1) {
      return this.b;
   }

   public static String m(ItemStack var0) {
      return var0 != null && var0.getItem() instanceof ItemCD?((ItemCD)var0.getItem()).g(var0):"";
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      if(this.c.size() > 0) {
         var3.addAll(this.c);
      }

      if(CraftingDead.l().e().d) {
         var3.add("Press \'" + EnumChatFormatting.AQUA + Keyboard.getKeyName(KeyBindingManager.b.keyCode) + EnumChatFormatting.GRAY + "\' to view Loot %s");
         if(Keyboard.isKeyDown(KeyBindingManager.b.keyCode)) {
            var3.add("Residental Loot: " + EnumChatFormatting.AQUA + LootManager.a.a(var1) + "%");
            var3.add("Rare Residental Loot: " + EnumChatFormatting.AQUA + LootManager.b.a(var1) + "%");
            var3.add("Medical Loot: " + EnumChatFormatting.AQUA + LootManager.c.a(var1) + "%");
            var3.add("Police Loot: " + EnumChatFormatting.AQUA + LootManager.e.a(var1) + "%");
            var3.add("Military Loot: " + EnumChatFormatting.AQUA + LootManager.d.a(var1) + "%");
         }
      }

   }

   public void registerIcons(IconRegister var1) {
      super.itemIcon = var1.registerIcon("craftingdead:" + this.a);
   }
}
