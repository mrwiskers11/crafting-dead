package com.craftingdead.item;

import com.craftingdead.block.BlockManager;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.blueprint.ItemBlueprint;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemHammer extends ItemCD {

   public int d;
   public int cB;
   public int cC;
   private boolean cD;


   public ItemHammer(int var1) {
      super(var1);
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return 32;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      return var1;
   }

   public boolean onItemUse(ItemStack var1, EntityPlayer var2, World var3, int var4, int var5, int var6, int var7, float var8, float var9, float var10) {
      this.cD = false;
      var2.setItemInUse(var1, this.getMaxItemUseDuration(var1));
      int var11 = var3.getBlockId(var4, var5, var6);
      if((var11 == BlockManager.af.blockID || var11 == BlockManager.aw.blockID || var11 == BlockManager.aA.blockID || var11 == Block.planks.blockID || var11 == Block.stoneBrick.blockID || var11 == BlockManager.az.blockID || var11 == Block.stoneBrick.blockID) && ItemBlueprint.a(var3, var2, var4, var5, var6, 8)) {
         this.d = var4;
         this.cB = var5;
         this.cC = var6;
         this.cD = true;
         return true;
      } else {
         return true;
      }
   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      if(this.cD) {
         var2.setBlockToAir(this.d, this.cB, this.cC);
      }

      return var1;
   }

   public EnumAction getItemUseAction(ItemStack var1) {
      return EnumAction.block;
   }
}
