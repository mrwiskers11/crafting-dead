package com.craftingdead.item.blueprint;

import java.util.ArrayList;

import com.craftingdead.item.ItemManager;
import com.craftingdead.item.blueprint.BlueprintRecipeInput;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

public class BlueprintRecipe {

   public int a = 0;
   public int b = 0;
   public ArrayList c = new ArrayList();


   public BlueprintRecipe(int var1, int var2) {
      this.a = var1;
      this.b = var2;
   }

   public BlueprintRecipe a(int var1, int var2) {
      BlueprintRecipeInput var3 = new BlueprintRecipeInput(var1, var2);
      this.c.add(var3);
      return this;
   }

   public ItemStack a() {
      return new ItemStack(this.b, 1, 0);
   }

   public boolean a(EntityPlayer var1) {
      InventoryPlayer var2 = var1.inventory;
      boolean var3 = true;

      for(int var4 = 0; var4 < this.c.size(); ++var4) {
         BlueprintRecipeInput var5 = (BlueprintRecipeInput)this.c.get(var4);
         int var6 = 0;

         for(int var7 = 0; var7 < var2.getSizeInventory(); ++var7) {
            ItemStack var8 = var2.getStackInSlot(var7);
            if(var8 != null && var8.itemID == var5.a) {
               var6 += var8.stackSize;
            }
         }

         if(var6 < var5.b) {
            var3 = false;
         }
      }

      if(!var2.hasItem(ItemManager.ew.itemID)) {
         var3 = false;
      }

      return var3;
   }

   public void b(EntityPlayer var1) {
      InventoryPlayer var2 = var1.inventory;

      int var3;
      for(var3 = 0; var3 < this.c.size(); ++var3) {
         BlueprintRecipeInput var4 = (BlueprintRecipeInput)this.c.get(var3);
         int var5 = var4.b;

         while(var5 > 0) {
            for(int var6 = 0; var6 < var2.getSizeInventory() && var5 > 0; ++var6) {
               ItemStack var7 = var2.getStackInSlot(var6);
               if(var7 != null && var7.itemID == var4.a && var7.stackSize > 0) {
                  var2.decrStackSize(var6, 1);
                  --var5;
               }
            }
         }
      }

      for(var3 = 0; var3 < var2.getSizeInventory(); ++var3) {
         ItemStack var8 = var2.getStackInSlot(var3);
         if(var8 != null && var8.itemID == ItemManager.ew.itemID) {
            var2.decrStackSize(var3, 1);
            break;
         }
      }

   }
}
