package com.craftingdead.item.blueprint;

import com.craftingdead.block.BlockManager;
import com.craftingdead.item.blueprint.Blueprint;

import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.world.World;

public class BlueprintBase extends Blueprint {

   public boolean a(EntityPlayer var1, ItemStack var2, double var3, double var5, double var7) {
      World var9 = var1.worldObj;
      return this.a(var9, (double)((int)var3), (double)((int)var5 + 1), (double)((int)var7));
   }

   public void a(World var1, EntityPlayer var2, ItemStack var3, int var4) {
      if(this.c() != null && this.c().typeOfHit == EnumMovingObjectType.TILE && var4 % 10 == 0) {
         this.a(var1, var2, super.a.blockX, super.a.blockY, super.a.blockZ);
      }

   }

   public void a(World var1, EntityPlayer var2, int var3, int var4, int var5) {
      this.a(var1, var2, var3, var4 + 1, var5, BlockManager.N.blockID);
   }

   public void a(EntityPlayer var1, ItemStack var2, int var3, int var4, int var5, boolean var6, float var7) {
      this.a(var3, var4 + 1, var5, var6, var7);
   }

   public void a(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      var3.add("With permission, place your base\'s center");
   }

   public boolean a() {
      return false;
   }

   public int b() {
      return 100;
   }
}
