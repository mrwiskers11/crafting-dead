package com.craftingdead.item.blueprint;

import java.util.List;

import com.craftingdead.item.blueprint.Blueprint;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.world.World;

public class BlueprintConcreatePlatform extends Blueprint {

   private int b = 1231;


   public boolean a(EntityPlayer var1, ItemStack var2, double var3, double var5, double var7) {
      World var9 = var1.worldObj;
      int var10 = this.b(var1);
      int var11;
      if(var10 == 0) {
         if(var9.getBlockId((int)var3, (int)var5, (int)var7 - 1) == 0) {
            return false;
         }

         if(var9.getBlockId((int)var3, (int)var5, (int)var7 + 1) == 0) {
            return false;
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 + 1.0D + (double)var11, var5, var7)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 + 1.0D + (double)var11, var5, var7 - 1.0D)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 + 1.0D + (double)var11, var5, var7 + 1.0D)) {
               return false;
            }
         }
      }

      if(var10 == 3) {
         if(var9.getBlockId((int)var3 + 1, (int)var5, (int)var7) == 0) {
            return false;
         }

         if(var9.getBlockId((int)var3 - 1, (int)var5, (int)var7) == 0) {
            return false;
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3, var5, var7 - 1.0D - (double)var11)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 + 1.0D, var5, var7 - 1.0D - (double)var11)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 - 1.0D, var5, var7 - 1.0D - (double)var11)) {
               return false;
            }
         }
      }

      if(var10 == 2) {
         if(var9.getBlockId((int)var3, (int)var5, (int)var7 - 1) == 0) {
            return false;
         }

         if(var9.getBlockId((int)var3, (int)var5, (int)var7 + 1) == 0) {
            return false;
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 - 1.0D - (double)var11, var5, var7)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 - 1.0D - (double)var11, var5, var7 - 1.0D)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 - 1.0D - (double)var11, var5, var7 + 1.0D)) {
               return false;
            }
         }
      }

      if(var10 == 1) {
         if(var9.getBlockId((int)var3 + 1, (int)var5, (int)var7) == 0) {
            return false;
         }

         if(var9.getBlockId((int)var3 - 1, (int)var5, (int)var7) == 0) {
            return false;
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3, var5, var7 + 1.0D + (double)var11)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 + 1.0D, var5, var7 + 1.0D + (double)var11)) {
               return false;
            }
         }

         for(var11 = 0; var11 < 3; ++var11) {
            if(!this.b(var9, var3 - 1.0D, var5, var7 + 1.0D + (double)var11)) {
               return false;
            }
         }
      }

      return true;
   }

   public boolean b(World var1, double var2, double var4, double var6) {
      int var8 = var1.getBlockId((int)var2, (int)var4, (int)var6);
      return var8 == 0 || var8 == this.b || var8 == 31;
   }

   public void a(World var1, EntityPlayer var2, ItemStack var3, int var4) {
      if(this.c() != null && this.c().typeOfHit == EnumMovingObjectType.TILE && var4 % 10 == 0) {
         this.a(var1, var2, super.a.blockX, super.a.blockY, super.a.blockZ);
      }

   }

   public void a(World var1, EntityPlayer var2, int var3, int var4, int var5) {
      int var6 = this.b(var2);
      if(var6 == 2) {
         this.a(var1, var2, var3 - 1, var4, var5, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 - 1, this.b);
         this.a(var1, var2, var3 - 2, var4, var5, this.b);
         this.a(var1, var2, var3 - 2, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 - 2, var4, var5 - 1, this.b);
         this.a(var1, var2, var3 - 3, var4, var5, this.b);
         this.a(var1, var2, var3 - 3, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 - 3, var4, var5 - 1, this.b);
      } else if(var6 == 0) {
         this.a(var1, var2, var3 + 1, var4, var5, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 - 1, this.b);
         this.a(var1, var2, var3 + 2, var4, var5, this.b);
         this.a(var1, var2, var3 + 2, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 + 2, var4, var5 - 1, this.b);
         this.a(var1, var2, var3 + 3, var4, var5, this.b);
         this.a(var1, var2, var3 + 3, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 + 3, var4, var5 - 1, this.b);
      } else if(var6 == 3) {
         this.a(var1, var2, var3, var4, var5 - 1, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 - 1, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 - 1, this.b);
         this.a(var1, var2, var3, var4, var5 - 2, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 - 2, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 - 2, this.b);
         this.a(var1, var2, var3, var4, var5 - 3, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 - 3, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 - 3, this.b);
      } else if(var6 == 1) {
         this.a(var1, var2, var3, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 + 1, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 + 1, this.b);
         this.a(var1, var2, var3, var4, var5 + 2, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 + 2, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 + 2, this.b);
         this.a(var1, var2, var3, var4, var5 + 3, this.b);
         this.a(var1, var2, var3 + 1, var4, var5 + 3, this.b);
         this.a(var1, var2, var3 - 1, var4, var5 + 3, this.b);
      }
   }

   public void a(EntityPlayer var1, ItemStack var2, int var3, int var4, int var5, boolean var6, float var7) {
      int var8 = this.b(var1);
      if(var8 == 2) {
         this.a(var3 - 1, var4, var5, var6, var7);
         this.a(var3 - 1, var4, var5 + 1, var6, var7);
         this.a(var3 - 1, var4, var5 - 1, var6, var7);
         this.a(var3 - 2, var4, var5, var6, var7);
         this.a(var3 - 2, var4, var5 + 1, var6, var7);
         this.a(var3 - 2, var4, var5 - 1, var6, var7);
         this.a(var3 - 3, var4, var5, var6, var7);
         this.a(var3 - 3, var4, var5 + 1, var6, var7);
         this.a(var3 - 3, var4, var5 - 1, var6, var7);
      } else if(var8 == 0) {
         this.a(var3 + 1, var4, var5, var6, var7);
         this.a(var3 + 1, var4, var5 + 1, var6, var7);
         this.a(var3 + 1, var4, var5 - 1, var6, var7);
         this.a(var3 + 2, var4, var5, var6, var7);
         this.a(var3 + 2, var4, var5 + 1, var6, var7);
         this.a(var3 + 2, var4, var5 - 1, var6, var7);
         this.a(var3 + 3, var4, var5, var6, var7);
         this.a(var3 + 3, var4, var5 + 1, var6, var7);
         this.a(var3 + 3, var4, var5 - 1, var6, var7);
      } else if(var8 == 3) {
         this.a(var3, var4, var5 - 1, var6, var7);
         this.a(var3 + 1, var4, var5 - 1, var6, var7);
         this.a(var3 - 1, var4, var5 - 1, var6, var7);
         this.a(var3, var4, var5 - 2, var6, var7);
         this.a(var3 + 1, var4, var5 - 2, var6, var7);
         this.a(var3 - 1, var4, var5 - 2, var6, var7);
         this.a(var3, var4, var5 - 3, var6, var7);
         this.a(var3 + 1, var4, var5 - 3, var6, var7);
         this.a(var3 - 1, var4, var5 - 3, var6, var7);
      } else if(var8 == 1) {
         this.a(var3, var4, var5 + 1, var6, var7);
         this.a(var3 + 1, var4, var5 + 1, var6, var7);
         this.a(var3 - 1, var4, var5 + 1, var6, var7);
         this.a(var3, var4, var5 + 2, var6, var7);
         this.a(var3 + 1, var4, var5 + 2, var6, var7);
         this.a(var3 - 1, var4, var5 + 2, var6, var7);
         this.a(var3, var4, var5 + 3, var6, var7);
         this.a(var3 + 1, var4, var5 + 3, var6, var7);
         this.a(var3 - 1, var4, var5 + 3, var6, var7);
      }
   }

   public void a(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      var3.add("Build a 3x1x3 concreate platform");
   }
}
