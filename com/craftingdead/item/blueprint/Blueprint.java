package com.craftingdead.item.blueprint;

import com.craftingdead.block.BlockManager;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.entity.EntityManager;
import com.craftingdead.item.blueprint.ItemBlueprint;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class Blueprint {

   protected MovingObjectPosition a;


   public boolean a(EntityPlayer var1, ItemStack var2, double var3, double var5, double var7) {
      return true;
   }

   public void a(World var1, EntityPlayer var2, ItemStack var3, int var4) {}

   public void a(World var1, EntityPlayer var2, int var3, int var4, int var5) {}

   @SideOnly(Side.CLIENT)
   public void a(EntityPlayer var1, ItemStack var2, int var3, int var4, int var5, boolean var6, float var7) {}

   public void a(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {}

   public boolean a(World var1, double var2, double var4, double var6) {
      int var8 = var1.getBlockId((int)var2, (int)var4, (int)var6);

      for(int var9 = 0; var9 < BlockManager.y.length; ++var9) {
         if(BlockManager.y[var9] == var8) {
            return true;
         }
      }

      return false;
   }

   public boolean a() {
      return true;
   }

   public int b() {
      return 100;
   }

   public void a(MovingObjectPosition var1) {
      this.a = var1;
   }

   public MovingObjectPosition c() {
      return this.a;
   }

   public void a(World var1, EntityPlayer var2, int var3, int var4, int var5, int var6) {
      boolean var7 = false;
      if(!var1.isRemote) {
         if(this.a() && !ItemBlueprint.b(var1, var2, var3, var4, var5)) {
            return;
         }

         var1.setBlock(var3, var4, var5, var6);
         var1.playSoundEffect((double)var3, (double)var4, (double)var5, "dig.wood", 1.0F, 1.0F);
         var7 = true;
      } else {
         Random var8 = new Random();

         for(int var9 = 0; var9 < 5; ++var9) {
            double var10 = 1.5D;
            double var12 = var10 * (double)(0.5F - var8.nextFloat());
            double var14 = var10 * (double)(0.5F - var8.nextFloat());
            double var16 = var10 * (double)(0.5F - var8.nextFloat());
            EntityManager.a("tilecrack_" + var6 + "_" + 0, (double)var3 + 0.5D + var12, (double)var4 + 0.5D + var14, (double)var5 + 0.5D + var16, 0.0D, 0.0D, 0.0D);
         }

         if(!var7) {
            Minecraft.getMinecraft().sndManager.playSoundFX("dig.wood", 1.0F, 1.0F);
         }
      }

   }

   protected boolean a(EntityPlayer var1) {
      int var2 = this.b(var1);
      return var2 == 0 || var2 == 2;
   }

   protected int b(EntityPlayer var1) {
      double var2 = (double)(Math.abs(var1.rotationYaw) % 360.0F);
      return var2 > 45.0D && var2 <= 135.0D?0:(var2 > 135.0D && var2 <= 225.0D?1:(var2 > 225.0D && var2 <= 315.0D?2:(var2 <= 315.0D && var2 > 45.0D?-1:3)));
   }

   @SideOnly(Side.CLIENT)
   protected void a(int var1, int var2, int var3, float var4) {
      CDRenderHelper.a(var1, var2, var3, var4);
   }

   @SideOnly(Side.CLIENT)
   protected void a(int var1, int var2, int var3, boolean var4, float var5) {
      if(var4) {
         CDRenderHelper.a(var1, var2, var3, var5);
      } else {
         this.b(var1, var2, var3, var5);
      }

   }

   @SideOnly(Side.CLIENT)
   protected void b(int var1, int var2, int var3, float var4) {
      CDRenderHelper.a((double)var1, (double)var2, (double)var3, 255.0F, 0.0F, 0.0F, 1.0D, var4);
   }

   @SideOnly(Side.CLIENT)
   protected void c(int var1, int var2, int var3, float var4) {
      CDRenderHelper.a((double)var1, (double)var2, (double)var3, 0.0F, 255.0F, 0.0F, 1.0D, var4);
   }
}
