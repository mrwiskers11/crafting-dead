package com.craftingdead.item.blueprint;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.item.ItemCD;
import com.craftingdead.item.blueprint.Blueprint;

import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class ItemBlueprint extends ItemCD {

   protected Blueprint d;


   public ItemBlueprint(int var1) {
      super(var1);
      this.setMaxStackSize(1);
   }

   public ItemBlueprint a(Blueprint var1) {
      this.d = var1;
      return this;
   }

   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      MovingObjectPosition var4 = this.getMovingObjectPositionFromPlayer(var2, var3, true);
      if(this.d != null && var4 != null) {
         int var5 = var4.blockX;
         int var6 = var4.blockY;
         int var7 = var4.blockZ;
         if(this.d.a(var3, var1, (double)var5, (double)var6, (double)var7)) {
            if(this.d.a() && !b(var2, var3, var5, var6, var7)) {
               return var1;
            }

            if(var6 >= 200) {
               return var1;
            }

            this.d.a(var4);
            var3.setItemInUse(var1, this.getMaxItemUseDuration(var1));
            return var1;
         }
      }

      return var1;
   }

   public int getMaxItemUseDuration(ItemStack var1) {
      return this.d.b();
   }

   public EnumAction getItemUseAction(ItemStack var1) {
      return EnumAction.block;
   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      MovingObjectPosition var4 = this.getMovingObjectPositionFromPlayer(var2, var3, true);
      if(this.d != null && var4 != null) {
         int var5 = var4.blockX;
         int var6 = var4.blockY;
         int var7 = var4.blockZ;
         if(!var2.isRemote) {
            if(this.d.a() && !b(var2, var3, var5, var6, var7)) {
               this.a(var3, "Must stay within 7 blocks to your base.");
               return var1;
            }

            if(!a(var2, var5, var6, var7)) {
               this.a(var3, "Blueprints denied in this area.");
               return var1;
            }

            if(!a(var2, var3, var5, var6, var7)) {
               return var1;
            }

            if(!this.d.a(var3, var1, (double)var5, (double)var6, (double)var7)) {
               return var1;
            }

            this.d.a(var2, var3, var5, var6, var7);
            if(!var3.capabilities.isCreativeMode) {
               --var1.stackSize;
            }
         }
      }

      return var1;
   }

   public static boolean a(World var0, EntityPlayer var1, int var2, int var3, int var4) {
      int var5 = (int)var1.posX;
      int var6 = (int)var1.posY;
      int var7 = (int)var1.posZ;
      return var5 != var2 || var7 != var4 || var6 != var3 && var6 != var3 + 1;
   }

   public static boolean b(World var0, EntityPlayer var1, int var2, int var3, int var4) {
      return a(var0, var1, var2, var3, var4, 7);
   }

   public static boolean a(World var0, EntityPlayer var1, int var2, int var3, int var4, int var5) {
      int var6 = var5;
      int var7 = var5 * 2;

      for(int var8 = -var5; var8 <= var6; ++var8) {
         for(int var9 = -var6; var9 <= var7; ++var9) {
            for(int var10 = -var6; var10 <= var6; ++var10) {
               int var11 = var0.getBlockId(var2 + var8, var3 + var9, var4 + var10);
               if(var11 == BlockManager.N.blockID) {
                  TileEntityBaseCenter var12 = (TileEntityBaseCenter)var0.getBlockTileEntity(var2 + var8, var3 + var9, var4 + var10);
                  if(var12.f() != null && var12.f().equals(var1.username)) {
                     return true;
                  }
               }
            }
         }
      }

      return false;
   }

   public static boolean a(World var0, int var1, int var2, int var3) {
      for(int var4 = -100; var4 < 100; ++var4) {
         for(int var5 = -100; var5 < 100; ++var5) {
            for(int var6 = -100; var6 < 100; ++var6) {
               int var7 = var0.getBlockId(var1 + var4, var2 + var5, var3 + var6);
               if(var7 == BlockManager.O.blockID) {
                  return false;
               }
            }
         }
      }

      return true;
   }

   public static boolean a(World var0, int var1, int var2, int var3, int var4, int var5) {
      for(int var6 = -var4; var6 <= var4; ++var6) {
         for(int var7 = -var4; var7 <= var4; ++var7) {
            for(int var8 = -var4; var8 <= var4; ++var8) {
               int var9 = var0.getBlockId(var1 + var6, var2 + var7, var3 + var8);
               if(var9 == var5) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   protected void a(EntityPlayer var1, String var2) {
      var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + var2));
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      var3.add("=-=-= Building Blueprint =-=-=");
      this.d.a(var1, var2, var3, var4);
   }

   public Blueprint d() {
      return this.d;
   }
}
