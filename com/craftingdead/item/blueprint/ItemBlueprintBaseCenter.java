package com.craftingdead.item.blueprint;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.event.EventBasePlaced;
import com.craftingdead.item.blueprint.ItemBlueprint;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;
import com.craftingdead.server.ServerProxy;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.command.ICommandManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

public class ItemBlueprintBaseCenter extends ItemBlueprint {

   public ItemBlueprintBaseCenter(int var1) {
      super(var1);
   }

   public void a(EntityPlayer var1, ItemStack var2) {
      PlayerData var3 = PlayerDataHandler.a(var1);
      MovingObjectPosition var4 = super.d.c();
      if(!var3.a(var1)) {
         EventBasePlaced var5 = new EventBasePlaced(var1);
         if(MinecraftForge.EVENT_BUS.post(var5)) {
            return;
         }

         int var6 = var4.blockX;
         int var7 = var4.blockY;
         int var8 = var4.blockZ;
         super.d.a(var1.worldObj, var1, var6, var7, var8);
         TileEntityBaseCenter var9 = (TileEntityBaseCenter)var1.worldObj.getBlockTileEntity(var6, var7 + 1, var8);
         var9.e(var3.a);
         var9.g();
         var3.a(var6, var7 + 1, var8);
         --var2.stackSize;
         this.a(var1, "Your base was created!");
      }

   }

   public ItemStack onEaten(ItemStack var1, World var2, EntityPlayer var3) {
      PlayerData var4 = PlayerDataHandler.a(var3);
      MovingObjectPosition var5 = this.getMovingObjectPositionFromPlayer(var2, var3, true);
      if(super.d != null && var5 != null) {
         int var6 = var5.blockX;
         int var7 = var5.blockY;
         int var8 = var5.blockZ;
         super.d.a(var5);
         if(!var2.isRemote) {
            int var9 = this.a(var2, var6, var7, var8, var3);
            if(var9 != -1) {
               this.a(var3, "Area around is not clear enough. (" + Block.blocksList[var9].getLocalizedName() + ")");
               return var1;
            }

            if(!ItemBlueprint.a(var2, var3, var6, var7, var8)) {
               return var1;
            }

            if(!ItemBlueprint.a(var2, var6, var7, var8)) {
               this.a(var3, "Blueprints denied in this area.");
               return var1;
            }

            if(ItemBlueprint.a(var2, var6, var7, var8, 35, BlockManager.N.blockID)) {
               this.a(var3, "Too close to another base.");
               return var1;
            }

            if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
               this.a(var3, "Checking permissions...");
               this.a(var3);
               return var1;
            }

            if(!var4.a(var3)) {
               super.d.a(var2, var3, var6, var7, var8);
               TileEntityBaseCenter var10 = (TileEntityBaseCenter)var2.getBlockTileEntity(var6, var7 + 1, var8);
               System.out.println(var4);
               System.out.println(var10);
               var10.e(var4.a);
               var10.g();
               var4.a(var6, var7 + 1, var8);
               if(!var3.capabilities.isCreativeMode) {
                  --var1.stackSize;
               }

               this.a(var3, "Your base was created!");
            } else {
               this.a(var3, "You have already placed a base. Coords: [x=" + var4.z + ", y=" + var4.A + ", z=" + var4.B + "]");
            }
         }
      }

      return var1;
   }

   public void a(EntityPlayer var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         if(!ServerProxy.i) {
            var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Bases are disabled on this server."));
            return;
         }

         if(var1 instanceof EntityPlayerMP) {
            EntityPlayerMP var2 = (EntityPlayerMP)var1;
            if(!var2.worldObj.isRemote) {
               MinecraftServer var3 = MinecraftServer.getServer();
               if(var3 != null) {
                  ICommandManager var4 = var3.getCommandManager();
                  var4.executeCommand(var2, "/cdbasecenter");
               }
            }
         }
      }

   }

   public int a(World var1, int var2, int var3, int var4, EntityPlayer var5) {
      byte var6 = 8;
      byte var7 = 2;
      int var8 = -1;

      int var9;
      int var10;
      int var11;
      int var12;
      for(var9 = -var6; var9 < var6; ++var9) {
         for(var10 = -var7; var10 < var6; ++var10) {
            for(var11 = -var6; var11 < var6; ++var11) {
               var12 = var1.getBlockId(var9 + var2, var10 + var3, var11 + var4);
               if(!this.b(var12)) {
                  var8 = var12;
               }
            }
         }
      }

      var6 = 5;

      for(var9 = -var6; var9 < var6; ++var9) {
         for(var10 = 1; var10 < var6; ++var10) {
            for(var11 = -var6; var11 < var6; ++var11) {
               var12 = var1.getBlockId(var9 + var2, var10 + var3, var11 + var4);
               if(var12 != 0 && var12 != 31) {
                  var8 = var12;
               }
            }
         }
      }

      return var8;
   }

   private boolean b(int var1) {
      for(int var2 = 0; var2 < BlockManager.x.length; ++var2) {
         if(var1 == BlockManager.x[var2]) {
            return true;
         }
      }

      return false;
   }
}
