package com.craftingdead.item.blueprint;

import java.util.List;

import com.craftingdead.item.blueprint.Blueprint;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.world.World;

public class BlueprintForge extends Blueprint {

   private int b = 1244;


   public boolean a(EntityPlayer var1, ItemStack var2, double var3, double var5, double var7) {
      World var9 = var1.worldObj;
      return !this.b(var9, var3, var5 + 1.0D, var7)?false:(this.a(var1)?false:(var9.getBlockId((int)var3 + 1, (int)var5, (int)var7) == 0?false:(var9.getBlockId((int)var3 - 1, (int)var5, (int)var7) == 0?false:(!this.b(var9, var3 + 1.0D, var5 + 1.0D, var7)?false:this.b(var9, var3 - 1.0D, var5 + 1.0D, var7)))));
   }

   public boolean b(World var1, double var2, double var4, double var6) {
      if(this.a(var1, var2, var4, var6)) {
         return true;
      } else {
         int var8 = var1.getBlockId((int)var2, (int)var4, (int)var6);
         return var8 == this.b;
      }
   }

   public void a(World var1, EntityPlayer var2, ItemStack var3, int var4) {
      if(this.c() != null && this.c().typeOfHit == EnumMovingObjectType.TILE && var4 % 10 == 0) {
         this.a(var1, var2, super.a.blockX, super.a.blockY, super.a.blockZ);
      }

   }

   public void a(World var1, EntityPlayer var2, int var3, int var4, int var5) {
      World var6 = var2.worldObj;
      if(this.a(var2)) {
         this.a(var6, var2, var3, var4 + 1, var5, this.b);
      } else {
         this.a(var6, var2, var3, var4 + 1, var5, this.b);
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, int var3, int var4, int var5, boolean var6, float var7) {
      if(this.a(var1)) {
         this.a(var3, var4 + 1, var5, var6, var7);
         this.a(var3, var4 + 1, var5 + 1, var6, var7);
         this.a(var3, var4 + 1, var5 - 1, var6, var7);
      } else {
         this.a(var3, var4 + 1, var5, var6, var7);
         this.a(var3 + 1, var4 + 1, var5, var6, var7);
         this.a(var3 - 1, var4 + 1, var5, var6, var7);
      }

   }

   public void a(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      var3.add("Build the notorious ammunition forge!");
   }
}
