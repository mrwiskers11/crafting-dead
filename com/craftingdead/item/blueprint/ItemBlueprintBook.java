package com.craftingdead.item.blueprint;

import com.craftingdead.client.gui.gui.blueprint.GuiBlueprintRecipes;
import com.craftingdead.item.ItemCD;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemBlueprintBook extends ItemCD {

   public ItemBlueprintBook(int var1) {
      super(var1);
   }

   @SideOnly(Side.CLIENT)
   public ItemStack onItemRightClick(ItemStack var1, World var2, EntityPlayer var3) {
      if(var2.isRemote) {
         Minecraft.getMinecraft().displayGuiScreen(new GuiBlueprintRecipes());
      }

      return var1;
   }
}
