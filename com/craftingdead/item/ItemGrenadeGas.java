package com.craftingdead.item;

import com.craftingdead.entity.EntityGrenadeGas;
import com.craftingdead.item.ItemGrenade;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemGrenadeGas extends ItemGrenade {

   public ItemGrenadeGas(int var1) {
      super(var1);
   }

   public void a(ItemStack var1, World var2, EntityPlayer var3, double var4) {
      PlayerData var6 = PlayerDataHandler.a(var3);
      if(var6.I) {
         double var7 = 2.0D;
         var2.spawnEntityInWorld(new EntityGrenadeGas(var2, var3, var4, (int)(var7 * 20.0D)));
         if(!var3.capabilities.isCreativeMode) {
            var3.inventory.setInventorySlotContents(var3.inventory.currentItem, (ItemStack)null);
         }

      }
   }
}
