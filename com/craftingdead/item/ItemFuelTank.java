package com.craftingdead.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;

import com.craftingdead.item.ItemCD;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ItemFuelTank extends ItemCD {

   public int d = -1;


   public ItemFuelTank(int var1, int var2) {
      super(var1);
      this.d = var2;
      this.setMaxDamage(var2 + 1);
      this.setMaxStackSize(1);
   }

   public void onUpdate(ItemStack var1, World var2, Entity var3, int var4, boolean var5) {
      if(var3 instanceof EntityPlayer) {
         EntityPlayer var6 = (EntityPlayer)var3;
         if(var6.getCurrentEquippedItem() == var1 && this.getDamage(var1) > this.d) {
            var6.inventory.setInventorySlotContents(var6.inventory.currentItem, (ItemStack)null);
         }
      }

   }

   @SideOnly(Side.CLIENT)
   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      super.addInformation(var1, var2, var3, var4);
      int var5 = this.d - var1.getItemDamage();
      var3.add("Fuel " + EnumChatFormatting.RED + var5);
   }
}
