package com.craftingdead.item;

import com.craftingdead.damage.DamageSourceBleeding;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.ItemReturnable;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class ItemReturnableBloodBag extends ItemReturnable {

   public ItemReturnableBloodBag(int var1, int var2) {
      super(var1, var2);
      this.setCreativeTab(ItemManager.d);
   }

   public boolean itemInteractionForEntity(ItemStack var1, EntityPlayer var2, EntityLivingBase var3) {
      if(super.cC) {
         if(var3 instanceof EntityPlayer) {
            EntityPlayer var4 = (EntityPlayer)var3;
            if(var4.getHealth() <= 4.0F) {
               return false;
            }

            var4.attackEntityFrom(new DamageSourceBleeding(), 2.0F);
            --var1.stackSize;
            if(!var2.inventory.addItemStackToInventory(new ItemStack(super.d + 256, 1, 0))) {
               var2.dropPlayerItem(new ItemStack(super.d + 256, 1, 0));
            }
         }

         return true;
      } else {
         return false;
      }
   }
}
