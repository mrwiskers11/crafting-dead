package com.craftingdead.item;

import com.craftingdead.item.ItemManager;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class CreativeTabCDClothing extends CreativeTabs {

   public CreativeTabCDClothing(String var1) {
      super(var1);
   }

   public ItemStack getIconItemStack() {
      return new ItemStack(ItemManager.dK);
   }
}
