package com.craftingdead.item;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemCDBlock extends ItemBlock {

   protected ArrayList a = new ArrayList();


   public ItemCDBlock(int var1) {
      super(var1);
   }

   public ItemCDBlock a(String var1) {
      this.a.add(var1);
      return this;
   }

   public void addInformation(ItemStack var1, EntityPlayer var2, List var3, boolean var4) {
      if(this.a.size() > 0) {
         var3.addAll(this.a);
      }

   }
}
