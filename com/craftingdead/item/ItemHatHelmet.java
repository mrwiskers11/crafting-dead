package com.craftingdead.item;

import com.craftingdead.item.ItemHat;

public class ItemHatHelmet extends ItemHat {

   public ItemHatHelmet(int var1) {
      super(var1);
      this.f("Reduces Headshot wounds by 20%");
   }
}
