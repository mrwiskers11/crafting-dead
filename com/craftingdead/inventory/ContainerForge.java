package com.craftingdead.inventory;

import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.inventory.slot.SlotForgeIngredient;
import com.craftingdead.inventory.slot.SlotForgeMagazine;
import com.craftingdead.inventory.slot.SlotForgeScrap;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerForge extends Container {

   private TileEntityForge a;


   public ContainerForge(InventoryPlayer var1, TileEntityForge var2) {
      this.a = var2;

      int var3;
      for(var3 = 0; var3 < 3; ++var3) {
         for(int var4 = 0; var4 < 9; ++var4) {
            this.addSlotToContainer(new Slot(var1, var4 + var3 * 9 + 9, 8 + var4 * 18, 84 + var3 * 18));
         }
      }

      for(var3 = 0; var3 < 9; ++var3) {
         this.addSlotToContainer(new Slot(var1, var3, 8 + var3 * 18, 142));
      }

      this.addSlotToContainer(new SlotForgeScrap(var2, 0, 56, 17));
      this.addSlotToContainer(new SlotForgeIngredient(var2, 1, 28, 17));
      this.addSlotToContainer(new SlotForgeMagazine(var2, 2, 42, 53));
      this.addSlotToContainer(new SlotFurnace(var1.player, var2, 3, 116, 35));
   }

   public ItemStack slotClick(int var1, int var2, int var3, EntityPlayer var4) {
      return super.slotClick(var1, var2, var3, var4);
   }

   public void detectAndSendChanges() {
      super.detectAndSendChanges();
   }

   public boolean canInteractWith(EntityPlayer var1) {
      return this.a.isUseableByPlayer(var1);
   }

   public ItemStack transferStackInSlot(EntityPlayer var1, int var2) {
      return null;
   }
}
