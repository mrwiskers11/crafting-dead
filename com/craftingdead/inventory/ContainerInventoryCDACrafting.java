package com.craftingdead.inventory;

import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemTacticalVest;
import com.craftingdead.item.gun.ItemAmmo;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;

public class ContainerInventoryCDACrafting extends Container {

   public InventoryCrafting a = new InventoryCrafting(this, 3, 3);
   public IInventory f = new InventoryCraftResult();
   protected final EntityPlayer g;


   public ContainerInventoryCDACrafting(InventoryPlayer var1, EntityPlayer var2) {
      this.g = var2;
      this.addSlotToContainer(new SlotCrafting(var1.player, this.a, this.f, 0, 145, 36));
      byte var5 = 0;

      int var3;
      int var4;
      for(var3 = 0; var3 < 3; ++var3) {
         for(var4 = 0; var4 < 3; ++var4) {
            this.addSlotToContainer(new Slot(this.a, var4 + var3 * 3, 72 + var4 * 18, 18 + var3 * 18));
         }
      }

      for(var3 = 0; var3 < 3; ++var3) {
         for(var4 = 0; var4 < 9; ++var4) {
            this.addSlotToContainer(new Slot(var1, var4 + (var3 + 1) * 9, 8 + var4 * 18, 84 + var3 * 18 + var5));
         }
      }

      for(var3 = 0; var3 < 9; ++var3) {
         this.addSlotToContainer(new Slot(var1, var3, 8 + var3 * 18, 142 + var5));
      }

      this.onCraftMatrixChanged(this.a);
   }

   public void onCraftMatrixChanged(IInventory var1) {
      this.f.setInventorySlotContents(0, CraftingManager.getInstance().findMatchingRecipe(this.a, this.g.worldObj));
   }

   public void onContainerClosed(EntityPlayer var1) {
      super.onContainerClosed(var1);

      for(int var2 = 0; var2 < 9; ++var2) {
         ItemStack var3 = this.a.getStackInSlotOnClosing(var2);
         if(var3 != null) {
            var1.dropPlayerItem(var3);
         }
      }

      this.f.setInventorySlotContents(0, (ItemStack)null);
   }

   public ItemStack transferStackInSlot(EntityPlayer var1, int var2) {
      ItemStack var3 = null;
      Slot var4 = (Slot)super.inventorySlots.get(var2);
      if(var4 != null && var4.getHasStack()) {
         ItemStack var5 = var4.getStack();
         var3 = var5.copy();
         if(var2 == 0) {
            if(var5.getItem() instanceof ItemAmmo) {
               return null;
            }

            if(var5.getItem() instanceof ItemBackpack) {
               return null;
            }

            if(var5.getItem() instanceof ItemTacticalVest) {
               return null;
            }
         }

         if(var2 == 0) {
            if(!this.mergeItemStack(var5, 10, 46, true)) {
               return null;
            }

            var4.onSlotChange(var5, var3);
         } else if(var2 >= 10 && var2 < 37) {
            if(!this.mergeItemStack(var5, 37, 46, false)) {
               return null;
            }
         } else if(var2 >= 37 && var2 < 46) {
            if(!this.mergeItemStack(var5, 10, 37, false)) {
               return null;
            }
         } else if(!this.mergeItemStack(var5, 10, 46, false)) {
            return null;
         }

         if(var5.stackSize == 0) {
            var4.putStack((ItemStack)null);
         } else {
            var4.onSlotChanged();
         }

         if(var5.stackSize == var3.stackSize) {
            return null;
         }

         var4.onPickupFromSlot(var1, var5);
      }

      return var3;
   }

   public boolean func_94530_a(ItemStack var1, Slot var2) {
      return var2.inventory != this.f && super.func_94530_a(var1, var2);
   }

   public boolean canInteractWith(EntityPlayer var1) {
      return true;
   }
}
