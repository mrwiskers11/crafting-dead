package com.craftingdead.inventory;

import java.util.ArrayList;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryTacticalVest implements IInventory {

   private ItemStack a;
   private boolean b = false;
   private ItemStack[] c = new ItemStack[18];


   public InventoryTacticalVest(ItemStack var1) {
      this.a = var1;
      this.f();
   }

   public String getInvName() {
      return "Tactical Vest";
   }

   public boolean isInvNameLocalized() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public void onInventoryChanged() {
      if(!this.b) {
         this.a();
      }

   }

   public void openChest() {
      this.f();
   }

   public void closeChest() {
      this.a();
   }

   public boolean isUseableByPlayer(EntityPlayer var1) {
      return true;
   }

   public boolean isItemValidForSlot(int var1, ItemStack var2) {
      return true;
   }

   public int getSizeInventory() {
      return this.c != null?this.c.length:0;
   }

   public ItemStack getStackInSlot(int var1) {
      return this.c[var1];
   }

   public ItemStack decrStackSize(int var1, int var2) {
      if(this.c[var1] != null) {
         ItemStack var3;
         if(this.c[var1].stackSize <= var2) {
            var3 = this.c[var1];
            this.c[var1] = null;
            this.onInventoryChanged();
            return var3;
         } else {
            var3 = this.c[var1].splitStack(var2);
            if(this.c[var1].stackSize == 0) {
               this.c[var1] = null;
            }

            this.onInventoryChanged();
            return var3;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int var1) {
      if(this.c[var1] != null) {
         ItemStack var2 = this.c[var1];
         this.c[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int var1, ItemStack var2) {
      this.c[var1] = var2;
      this.onInventoryChanged();
   }

   public void a() {
      NBTTagCompound var1 = this.a(this.a);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.getSizeInventory(); ++var3) {
         if(this.getStackInSlot(var3) != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.setByte("slot", (byte)var3);
            this.getStackInSlot(var3).writeToNBT(var4);
            var2.appendTag(var4);
         }
      }

      var1.setTag("content", var2);
   }

   public void f() {
      this.b = true;
      NBTTagCompound var1 = this.a(this.a);
      NBTTagList var2 = var1.getTagList("content");

      for(int var3 = 0; var3 < var2.tagCount(); ++var3) {
         NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
         byte var5 = var4.getByte("slot");
         ItemStack var6 = ItemStack.loadItemStackFromNBT(var4);
         if(var6 != null) {
            this.setInventorySlotContents(var5, var6);
         }
      }

      this.b = false;
   }

   public ArrayList h() {
      ArrayList var1 = new ArrayList();

      for(int var2 = 0; var2 < this.c.length; ++var2) {
         if(this.c[var2] != null) {
            var1.add(this.c[var2].copy());
         }
      }

      return var1;
   }

   public NBTTagCompound a(ItemStack var1) {
      String var2 = "vestinv";
      if(!var1.hasTagCompound()) {
         var1.setTagCompound(new NBTTagCompound("tag"));
      }

      if(!var1.stackTagCompound.hasKey(var2)) {
         NBTTagCompound var3 = new NBTTagCompound(var2);
         var1.stackTagCompound.setTag(var2, var3);
      }

      return (NBTTagCompound)var1.stackTagCompound.getTag(var2);
   }
}
