package com.craftingdead.inventory.gui;

import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.inventory.ContainerInventoryCDACrafting;
import com.craftingdead.item.ItemManager;
import com.craftingdead.network.packets.CDAPacketOpenGUI;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiInventoryCDACrafting extends GuiContainer {

   private ResourceLocation t = new ResourceLocation("craftingdead", "textures/gui/cdainventorycrafting.png");
   private ResourceLocation u = new ResourceLocation("craftingdead", "textures/gui/cdainventory.png");
   private float v;
   private float w;
   private static float x = 0.0F;


   public GuiInventoryCDACrafting(EntityPlayer var1) {
      super(new ContainerInventoryCDACrafting(var1.inventory, var1));
      super.allowUserInput = true;
      x = 0.0F;
   }

   public void initGui() {
      super.initGui();
      super.guiTop += 0;
      GuiCDButton var1 = new GuiCDButton(1, super.guiLeft + 6, super.guiTop - 21, new ItemStack(ItemManager.dg));
      var1.q = "Inventory";
      var1.r = false;
      super.buttonList.add(var1);
      var1 = new GuiCDButton(2, super.guiLeft + 35, super.guiTop - 21, new ItemStack(ItemManager.bf));
      var1.q = "Crafting";
      var1.r = false;
      super.buttonList.add(var1);
      if(super.mc.thePlayer.capabilities.isCreativeMode) {
         var1 = new GuiCDButton(3, super.guiLeft + 64, super.guiTop - 20, new ItemStack(ItemManager.duu));
         var1.q = "Creative Inventory";
         var1.r = false;
         super.buttonList.add(var1);
      }

   }

   public void drawScreen(int var1, int var2, float var3) {
      super.drawScreen(var1, var2, var3);
      this.v = (float)var1;
      this.w = (float)var2;

      for(int var4 = 0; var4 < super.buttonList.size(); ++var4) {
         if(super.buttonList.get(var4) instanceof GuiCDButton) {
            GuiCDButton var5 = (GuiCDButton)super.buttonList.get(var4);
            if(var5.o == 2 && var5.q != null && var5.q.length() > 0) {
               ArrayList var6 = new ArrayList();
               var6.add(var5.q);
               this.drawHoveringText(var6, var1, var2, super.fontRenderer);
            }
         }
      }

   }

   protected boolean checkHotbarKeys(int var1) {
      return false;
   }

   protected void actionPerformed(GuiButton var1) {
      if(var1.id == 1) {
         PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.a));
      }

      if(var1.id == 3) {
         super.mc.displayGuiScreen(new GuiContainerCreative(super.mc.thePlayer));
      }

   }

   protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      int var4 = super.guiLeft;
      int var5 = super.guiTop;
      super.mc.renderEngine.bindTexture(this.u);
      this.drawTexturedModalRect(var4, var5 - 27, 28, 186, 28, 32);
      if(super.mc.thePlayer.capabilities.isCreativeMode) {
         this.drawTexturedModalRect(var4 + 58, var5 - 27, 0, 186, 28, 30);
      }

      super.mc.renderEngine.bindTexture(this.t);
      this.drawTexturedModalRect(var4, var5, 0, 0, super.xSize, super.ySize);
      super.mc.renderEngine.bindTexture(this.u);
      this.drawTexturedModalRect(var4 + 29, var5 - 28, 28, 216, 28, 32);
      GL11.glPushMatrix();
      double var6 = 1.0D;
      GL11.glScaled(var6, var6, var6);
      a(super.mc, var4 + 33, var5 + 75, 30, (float)(var4 + 51) - this.v, (float)(var5 + 75 - 50) - this.w);
      GL11.glPopMatrix();
   }

   public void updateScreen() {
      x += 2.0F;
   }

   public static void a(Minecraft var0, int var1, int var2, int var3, float var4, float var5) {
      GL11.glEnable(2903);
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var1, (float)var2, 50.0F);
      GL11.glScalef((float)(-var3), (float)var3, (float)var3);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      float var6 = var0.thePlayer.renderYawOffset;
      float var7 = var0.thePlayer.rotationYaw;
      float var8 = var0.thePlayer.rotationPitch;
      GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
      RenderHelper.enableStandardItemLighting();
      GL11.glRotatef(-135.0F + x, 0.0F, 1.0F, 0.0F);
      var0.thePlayer.renderYawOffset = 0.0F;
      var0.thePlayer.rotationYaw = 0.0F;
      var0.thePlayer.rotationPitch = 0.0F;
      var0.thePlayer.rotationYawHead = 0.0F;
      GL11.glTranslatef(0.0F, var0.thePlayer.yOffset, 0.0F);
      RenderManager.instance.playerViewY = 180.0F;
      RenderManager.instance.renderEntityWithPosYaw(var0.thePlayer, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
      var0.thePlayer.renderYawOffset = var6;
      var0.thePlayer.rotationYaw = var7;
      var0.thePlayer.rotationPitch = var8;
      GL11.glPopMatrix();
      RenderHelper.disableStandardItemLighting();
      GL11.glDisable('\u803a');
      OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
      GL11.glDisable(3553);
      OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
   }

}
