package com.craftingdead.inventory.gui;

import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.inventory.ContainerShelf;

import java.util.ArrayList;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import org.lwjgl.opengl.GL11;

public class GuiShelf extends GuiContainer {

   private IInventory t;
   private IInventory u;
   private static final ResourceLocation v = new ResourceLocation("craftingdead", "textures/gui/backpack.png");
   private int w = 0;


   public GuiShelf(IInventory var1, IInventory var2) {
      super(new ContainerShelf(var1, var2));
      this.t = var1;
      this.u = var2;
      super.allowUserInput = false;
      short var3 = 222;
      int var4 = var3 - 108;
      this.w = var2.getSizeInventory() / 9;
      super.ySize = var4 + this.w * 18;
   }

   public void initGui() {
      super.initGui();
   }

   protected void actionPerformed(GuiButton var1) {}

   protected void drawGuiContainerForegroundLayer(int var1, int var2) {
      super.fontRenderer.drawString(this.u.isInvNameLocalized()?this.u.getInvName():StatCollector.translateToLocal(this.u.getInvName()), 8, 6, 4210752);
      super.fontRenderer.drawString(this.t.isInvNameLocalized()?this.t.getInvName():StatCollector.translateToLocal(this.t.getInvName()), 8, super.ySize - 96 + 2, 4210752);
   }

   protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      super.mc.renderEngine.bindTexture(v);
      int var4 = (super.width - super.xSize) / 2;
      int var5 = (super.height - super.ySize) / 2;
      super.mc.renderEngine.bindTexture(v);
      this.drawTexturedModalRect(var4, var5, 0, 0, super.xSize, this.w * 18 + 17);
      this.drawTexturedModalRect(var4, var5 + this.w * 18 + 17, 0, 126, super.xSize, 96);

      for(int var6 = 0; var6 < super.buttonList.size(); ++var6) {
         if(super.buttonList.get(var6) instanceof GuiCDButton) {
            GuiCDButton var7 = (GuiCDButton)super.buttonList.get(var6);
            if(var7.o == 2 && var7.q != null && var7.q.length() > 0) {
               ArrayList var8 = new ArrayList();
               var8.add(var7.q);
               this.drawHoveringText(var8, var2, var3, super.fontRenderer);
            }
         }
      }

   }

}
