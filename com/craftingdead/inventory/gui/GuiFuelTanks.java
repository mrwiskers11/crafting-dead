package com.craftingdead.inventory.gui;

import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.inventory.ContainerFuelTanks;
import com.craftingdead.item.ItemManager;
import com.craftingdead.network.packets.CDAPacketOpenGUI;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.ArrayList;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import org.lwjgl.opengl.GL11;

public class GuiFuelTanks extends GuiContainer {

   private IInventory t;
   private IInventory u;
   private ResourceLocation v = new ResourceLocation("craftingdead", "textures/gui/backpack.png");
   private ResourceLocation w = new ResourceLocation("craftingdead:textures/gui/cdainventory.png");
   private int x = 0;


   public GuiFuelTanks(IInventory var1, IInventory var2) {
      super(new ContainerFuelTanks(var1, var2));
      this.t = var1;
      this.u = var2;
      super.allowUserInput = false;
      short var3 = 222;
      int var4 = var3 - 108;
      this.x = var2.getSizeInventory() / 9;
      super.ySize = var4 + this.x * 18;
   }

   public void initGui() {
      super.initGui();
      GuiCDButton var1 = new GuiCDButton(1, super.guiLeft + 6, super.guiTop - 21, new ItemStack(ItemManager.dg));
      var1.q = "Inventory";
      var1.r = false;
      super.buttonList.add(var1);
   }

   protected void actionPerformed(GuiButton var1) {
      if(var1.id == 1) {
         PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.a));
      }

   }

   protected void drawGuiContainerForegroundLayer(int var1, int var2) {
      super.fontRenderer.drawString(this.u.isInvNameLocalized()?this.u.getInvName():StatCollector.translateToLocal(this.u.getInvName()), 8, 6, 4210752);
      super.fontRenderer.drawString(this.t.isInvNameLocalized()?this.t.getInvName():StatCollector.translateToLocal(this.t.getInvName()), 8, super.ySize - 96 + 2, 4210752);
   }

   protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      super.mc.renderEngine.bindTexture(this.v);
      int var4 = (super.width - super.xSize) / 2;
      int var5 = (super.height - super.ySize) / 2;
      super.mc.renderEngine.bindTexture(this.w);
      this.drawTexturedModalRect(var4, var5 - 28, 28, 186, 28, 32);
      super.mc.renderEngine.bindTexture(this.v);
      this.drawTexturedModalRect(var4, var5, 0, 0, super.xSize, this.x * 18 + 17);
      this.drawTexturedModalRect(var4, var5 + this.x * 18 + 17, 0, 126, super.xSize, 96);

      for(int var6 = 0; var6 < super.buttonList.size(); ++var6) {
         if(super.buttonList.get(var6) instanceof GuiCDButton) {
            GuiCDButton var7 = (GuiCDButton)super.buttonList.get(var6);
            if(var7.o == 2 && var7.q != null && var7.q.length() > 0) {
               ArrayList var8 = new ArrayList();
               var8.add(var7.q);
               this.drawHoveringText(var8, var2, var3, super.fontRenderer);
            }
         }
      }

   }
}
