package com.craftingdead.inventory.gui;

import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.client.gui.modules.GuiCDButton;
import com.craftingdead.inventory.ContainerInventoryCDA;
import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.item.ItemManager;
import com.craftingdead.network.packets.CDAPacketOpenGUI;

import cpw.mods.fml.common.network.PacketDispatcher;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiInventoryCDA extends GuiContainer {

   private static final InventoryCDA t = new InventoryCDA();
   private float u;
   private float v;
   private ResourceLocation w = new ResourceLocation("craftingdead", "textures/gui/cdainventory.png");
   private static float x = 0.0F;


   public GuiInventoryCDA(EntityPlayer var1) {
      super(new ContainerInventoryCDA(var1, t));
      super.allowUserInput = true;
      x = 0.0F;
      var1.addStat(AchievementList.openInventory, 1);
      super.ySize = 186;
   }

   public void initGui() {
      super.initGui();
      super.guiTop += 10;
      GuiCDButton var1 = new GuiCDButton(1, super.guiLeft + 6, super.guiTop - 21, new ItemStack(ItemManager.dg));
      var1.q = "Inventory";
      var1.r = false;
      super.buttonList.add(var1);
      var1 = new GuiCDButton(2, super.guiLeft + 35, super.guiTop - 21, new ItemStack(ItemManager.bf));
      var1.q = "Crafting";
      var1.r = false;
      super.buttonList.add(var1);
      if(super.mc.thePlayer.capabilities.isCreativeMode) {
         var1 = new GuiCDButton(5, super.guiLeft + 64, super.guiTop - 20, new ItemStack(ItemManager.duu));
         var1.q = "Creative Inventory";
         var1.r = false;
         super.buttonList.add(var1);
      }

      var1 = new GuiCDButton(3, super.guiLeft + 83, super.guiTop + 48, 10, 16, ">");
      var1.s = true;
      super.buttonList.add(var1);
      var1 = new GuiCDButton(4, super.guiLeft + 83, super.guiTop + 66, 10, 16, ">");
      var1.s = true;
      super.buttonList.add(var1);
   }

   protected boolean checkHotbarKeys(int var1) {
      return false;
   }

   public void drawScreen(int var1, int var2, float var3) {
      super.drawScreen(var1, var2, var3);
      this.u = (float)var1;
      this.v = (float)var2;

      for(int var4 = 0; var4 < super.buttonList.size(); ++var4) {
         if(super.buttonList.get(var4) instanceof GuiCDButton) {
            GuiCDButton var5 = (GuiCDButton)super.buttonList.get(var4);
            if(var5.o == 2 && var5.q != null && var5.q.length() > 0) {
               ArrayList var6 = new ArrayList();
               var6.add(var5.q);
               this.drawHoveringText(var6, var1, var2, super.fontRenderer);
            }
         }
      }

   }

   protected void actionPerformed(GuiButton var1) {
      ContainerInventoryCDA var2 = (ContainerInventoryCDA)super.inventorySlots;
      InventoryCDA var3 = var2.f;
      if(var1.id == 2) {
         PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.b));
      }

      if(var1.id == 3 && var3.a("backpack") != null) {
         PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.c));
      }

      if(var1.id == 4 && var3.a("vest") != null) {
         PacketDispatcher.sendPacketToServer(CDAPacketOpenGUI.a(CraftingDeadGui.d));
      }

      if(var1.id == 5) {
         super.mc.displayGuiScreen(new GuiContainerCreative(super.mc.thePlayer));
      }

   }

   protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      super.mc.renderEngine.bindTexture(this.w);
      int var4 = super.guiLeft;
      int var5 = super.guiTop;
      if(super.mc.thePlayer.capabilities.isCreativeMode) {
         this.drawTexturedModalRect(var4 + 58, var5 - 27, 0, 186, 28, 30);
      }

      this.drawTexturedModalRect(var4 + 29, var5 - 27, 0, 186, 28, 32);
      this.drawTexturedModalRect(var4, var5, 0, 0, super.xSize, super.ySize);
      this.drawTexturedModalRect(var4, var5 - 28, 0, 216, 28, 32);
      ContainerInventoryCDA var6 = (ContainerInventoryCDA)super.inventorySlots;
      InventoryCDA var7 = var6.f;
      super.mc.renderEngine.bindTexture(this.w);
      if(var7.a("cgun") != null) {
         this.drawTexturedModalRect(var4 + 122, var5 + 45, 176, 0, 22, 22);
         if(var7.a()) {
            this.drawTexturedModalRect(var4 + 122, var5 + 45, 176, 22, 22, 22);
         }

         if(!var7.c(var6.a.getItemStack())) {
            this.drawTexturedModalRect(var4 + 122, var5 + 45, 176, 44, 22, 22);
         }
      }

      GL11.glPushMatrix();
      double var8 = 1.0D;
      GL11.glScaled(var8, var8, var8);
      a(super.mc, var4 + 33, var5 + 97, 30, (float)(var4 + 51) - this.u, (float)(var5 + 75 - 50) - this.v);
      GL11.glPopMatrix();
   }

   public void updateScreen() {
      x += 2.0F;
   }

   public static void a(Minecraft var0, int var1, int var2, int var3, float var4, float var5) {
      GL11.glEnable(2903);
      GL11.glPushMatrix();
      GL11.glTranslatef((float)var1, (float)var2, 50.0F);
      GL11.glScalef((float)(-var3), (float)var3, (float)var3);
      GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
      float var6 = var0.thePlayer.renderYawOffset;
      float var7 = var0.thePlayer.rotationYaw;
      float var8 = var0.thePlayer.rotationPitch;
      GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
      RenderHelper.enableStandardItemLighting();
      GL11.glRotatef(-135.0F + x, 0.0F, 1.0F, 0.0F);
      var0.thePlayer.renderYawOffset = 0.0F;
      var0.thePlayer.rotationYaw = 0.0F;
      var0.thePlayer.rotationPitch = 0.0F;
      var0.thePlayer.rotationYawHead = 0.0F;
      GL11.glTranslatef(0.0F, var0.thePlayer.yOffset, 0.0F);
      RenderManager.instance.playerViewY = 180.0F;
      RenderManager.instance.renderEntityWithPosYaw(var0.thePlayer, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
      var0.thePlayer.renderYawOffset = var6;
      var0.thePlayer.rotationYaw = var7;
      var0.thePlayer.rotationPitch = var8;
      GL11.glPopMatrix();
      RenderHelper.disableStandardItemLighting();
      GL11.glDisable('\u803a');
      OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
      GL11.glDisable(3553);
      OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
   }

}
