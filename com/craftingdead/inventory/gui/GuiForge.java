package com.craftingdead.inventory.gui;

import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.client.render.CDRenderHelper;
import com.craftingdead.inventory.ContainerForge;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiForge extends GuiContainer {

   private ResourceLocation u = new ResourceLocation("craftingdead", "textures/gui/forge.png");
   public TileEntityForge t;


   public GuiForge(InventoryPlayer var1, TileEntityForge var2) {
      super(new ContainerForge(var1, var2));
      this.t = var2;
      super.xSize = 176;
      super.ySize = 165;
   }

   public void initGui() {
      super.initGui();
   }

   public void drawGuiContainerForegroundLayer(int var1, int var2) {
      String var3 = this.t.isInvNameLocalized()?this.t.getInvName():I18n.getString(this.t.getInvName());
      super.fontRenderer.drawString(var3, super.xSize / 2 - super.fontRenderer.getStringWidth(var3) / 2, 6, 4210752);
      super.fontRenderer.drawString(I18n.getString("Inventory"), 8, super.ySize - 96 + 5, 4210752);
      CDRenderHelper.b((double)(super.xSize / 2 + 59), (double)(super.ySize / 2 - 74), 11.0D, 70.8D, 16777215, 1.0F);
      CDRenderHelper.b((double)(super.xSize / 2 + 60), (double)(super.ySize / 2 - 73), 9.0D, 69.0D, 9145227, 1.0F);
      int var4 = (int)((float)this.t.f() / 100.0F * 70.0F);
      CDRenderHelper.b((double)(super.xSize / 2 + 60), (double)(super.ySize / 2 - 73), 9.0D, (double)var4, 16732240, 1.0F);
   }

   public void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      Minecraft.getMinecraft().getTextureManager().bindTexture(this.u);
      this.drawTexturedModalRect(super.guiLeft, super.guiTop, 0, 0, super.xSize, super.ySize);
   }
}
