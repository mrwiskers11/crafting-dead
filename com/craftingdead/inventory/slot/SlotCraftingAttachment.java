package com.craftingdead.inventory.slot;

import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotCraftingAttachment extends Slot {

   private int a = 0;


   public SlotCraftingAttachment(IInventory var1, int var2, int var3, int var4, int var5) {
      super(var1, var2, var3, var4);
      this.a = var5;
   }

   public boolean isItemValid(ItemStack var1) {
      if(super.inventory instanceof InventoryCDA) {
         InventoryCDA var2 = (InventoryCDA)super.inventory;
         ItemStack var3 = var2.a("cgun");
         GunAttachment var4 = GunAttachment.b(var1.itemID);
         if(var3 != null && var4 != null) {
            ItemGun var5 = (ItemGun)var3.getItem();
            if(var5.a(var4)) {
               if(var4.o && this.a == 0) {
                  return true;
               }

               if(var4.p && this.a == 1) {
                  return true;
               }

               if(var4.q && this.a == 2) {
                  return true;
               }
            }
         }
      }

      return false;
   }
}
