package com.craftingdead.inventory.slot;

import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.item.gun.GunPaint;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.item.gun.ItemPaint;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotCraftingPaints extends Slot {

   public SlotCraftingPaints(IInventory var1, int var2, int var3, int var4) {
      super(var1, var2, var3, var4);
   }

   public boolean isItemValid(ItemStack var1) {
      if(var1.getItem() instanceof ItemPaint && super.inventory instanceof InventoryCDA) {
         InventoryCDA var2 = (InventoryCDA)super.inventory;
         ItemStack var3 = var2.a("cgun");
         if(var3 != null) {
            ItemGun var4 = (ItemGun)var3.getItem();
            GunPaint var5 = ((ItemPaint)var1.getItem()).d;
            if(var4.a(var5) || var5 == GunPaint.k) {
               return true;
            }
         }
      }

      return false;
   }
}
