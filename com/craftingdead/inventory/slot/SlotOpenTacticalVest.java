package com.craftingdead.inventory.slot;

import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemFuelTankBackpack;
import com.craftingdead.item.ItemTacticalVest;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotOpenTacticalVest extends Slot {

   public SlotOpenTacticalVest(IInventory var1, int var2, int var3, int var4) {
      super(var1, var2, var3, var4);
   }

   public boolean isItemValid(ItemStack var1) {
      return !(var1.getItem() instanceof ItemBackpack) && !(var1.getItem() instanceof ItemGun) && !(var1.getItem() instanceof ItemTacticalVest) && !(var1.getItem() instanceof ItemFuelTankBackpack);
   }
}
