package com.craftingdead.inventory.slot;

import com.craftingdead.item.ItemMeleeWeapon;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotHeldMelee extends Slot {

   public SlotHeldMelee(IInventory var1, int var2, int var3, int var4) {
      super(var1, var2, var3, var4);
   }

   public boolean isItemValid(ItemStack var1) {
      return var1.getItem() instanceof ItemMeleeWeapon;
   }
}
