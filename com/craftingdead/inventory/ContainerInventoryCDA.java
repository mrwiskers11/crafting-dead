package com.craftingdead.inventory;

import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.inventory.slot.SlotBackpack;
import com.craftingdead.inventory.slot.SlotClothing;
import com.craftingdead.inventory.slot.SlotCraftingAttachment;
import com.craftingdead.inventory.slot.SlotCraftingGun;
import com.craftingdead.inventory.slot.SlotCraftingPaints;
import com.craftingdead.inventory.slot.SlotHat;
import com.craftingdead.inventory.slot.SlotHeldGun;
import com.craftingdead.inventory.slot.SlotHeldMelee;
import com.craftingdead.inventory.slot.SlotVest;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerInventoryCDA extends Container {

   public InventoryPlayer a;
   public InventoryCDA f;


   public ContainerInventoryCDA(EntityPlayer var1, InventoryCDA var2) {
      this.a = var1.inventory;
      this.f = var2;
      byte var5 = 20;

      int var3;
      for(var3 = 0; var3 < 3; ++var3) {
         for(int var4 = 0; var4 < 9; ++var4) {
            this.addSlotToContainer(new Slot(this.a, var4 + (var3 + 1) * 9, 8 + var4 * 18, 84 + var3 * 18 + var5));
         }
      }

      for(var3 = 0; var3 < 9; ++var3) {
         this.addSlotToContainer(new Slot(this.a, var3, 8 + var3 * 18, 142 + var5));
      }

      this.addSlotToContainer(new SlotHeldMelee(this.f, this.f.b("melee"), 26, 9));
      this.addSlotToContainer(new SlotHeldGun(this.f, this.f.b("gun"), 44, 9));
      this.addSlotToContainer(new SlotClothing(this.f, this.f.b("clothing"), 65, 9));
      this.addSlotToContainer(new SlotHat(this.f, this.f.b("hat"), 65, 30));
      this.addSlotToContainer(new SlotBackpack(this.f, this.f.b("backpack"), 65, 48));
      this.addSlotToContainer(new SlotVest(this.f, this.f.b("vest"), 65, 66));
      this.addSlotToContainer(new SlotCraftingGun(this.f, this.f.b("cgun"), 125, 48));
      this.addSlotToContainer(new SlotCraftingAttachment(this.f, this.f.b("cattach3"), 104, 48, 2));
      this.addSlotToContainer(new SlotCraftingAttachment(this.f, this.f.b("cattach2"), 125, 69, 1));
      this.addSlotToContainer(new SlotCraftingAttachment(this.f, this.f.b("cattach1"), 125, 27, 0));
      this.addSlotToContainer(new SlotCraftingPaints(this.f, this.f.b("cpaint"), 146, 48));
   }

   public void putStackInSlot(int var1, ItemStack var2) {
      if(var1 < super.inventorySlots.size()) {
         this.getSlot(var1).putStack(var2);
      }

   }

   public Slot getSlot(int var1) {
      return (Slot)super.inventorySlots.get(var1);
   }

   @SideOnly(Side.CLIENT)
   public void putStacksInSlots(ItemStack[] var1) {
      for(int var2 = 0; var2 < var1.length; ++var2) {
         if(var2 < 46) {
            this.getSlot(var2).putStack(var1[var2]);
         }
      }

   }

   public boolean canInteractWith(EntityPlayer var1) {
      return true;
   }

   public void onContainerClosed(EntityPlayer var1) {
      super.onContainerClosed(var1);

      for(int var2 = this.f.b("cattach1"); var2 <= this.f.b("cattach3"); ++var2) {
         if(!var1.worldObj.isRemote) {
            ItemStack var3 = this.f.getStackInSlot(var2);
            if(var3 != null) {
               var1.dropPlayerItem(var3);
            }
         }

         this.f.setInventorySlotContents(var2, (ItemStack)null);
      }

      if(!var1.worldObj.isRemote && this.f.a("cgun") != null) {
         if(!var1.isDead) {
            var1.dropPlayerItem(this.f.a("cgun"));
            this.f.setInventorySlotContents(this.f.b("cgun"), (ItemStack)null);
         } else {
            this.f.setInventorySlotContents(this.f.b("cgun"), (ItemStack)null);
         }

         if(this.f.a("cpaint") != null) {
            var1.dropPlayerItem(this.f.a("cpaint"));
            this.f.setInventorySlotContents(this.f.b("cpaint"), (ItemStack)null);
         }
      }

   }

   public ItemStack slotClick(int var1, int var2, int var3, EntityPlayer var4) {
      return super.slotClick(var1, var2, var3, var4);
   }

   public ItemStack transferStackInSlot(EntityPlayer var1, int var2) {
      ItemStack var3 = null;
      Slot var4 = (Slot)super.inventorySlots.get(var2);
      if(var4 != null && var4.getHasStack()) {
         ItemStack var5 = var4.getStack();
         var3 = var5.copy();
         if(var2 >= 0 && var2 < 27) {
            if(!this.mergeItemStack(var5, 27, 36, false)) {
               return null;
            }
         } else if(var2 >= 27 && var2 < 36 && !this.mergeItemStack(var5, 0, 27, false)) {
            return null;
         }

         if(var2 == 42) {
            this.f.a(var5);
            if(!this.mergeItemStack(var5, 0, 27, false)) {
               return null;
            }
         } else if(var2 >= 36 && var2 <= 47 && !this.mergeItemStack(var5, 0, 27, false)) {
            return null;
         }

         if(var5.stackSize == 0) {
            var4.putStack((ItemStack)null);
         } else {
            var4.onSlotChanged();
         }

         if(var5.stackSize == var3.stackSize) {
            return null;
         }

         var4.onPickupFromSlot(var1, var5);
      }

      return var3;
   }
}
