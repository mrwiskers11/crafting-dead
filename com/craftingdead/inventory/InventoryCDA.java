package com.craftingdead.inventory;

import java.util.ArrayList;

import com.craftingdead.item.gun.GunAttachment;
import com.craftingdead.item.gun.GunPaint;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.item.gun.ItemGunAttachment;
import com.craftingdead.item.gun.ItemPaint;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryCDA implements IInventory {

   private int a = 11;
   private ItemStack[] b;


   public InventoryCDA() {
      this.b = new ItemStack[this.a];
   }

   public int getSizeInventory() {
      return this.b.length;
   }

   public ItemStack getStackInSlot(int var1) {
      return this.b[var1];
   }

   public ItemStack decrStackSize(int var1, int var2) {
      ItemStack[] var3 = this.b;
      if(var3[var1] != null) {
         ItemStack var4;
         if(var3[var1].stackSize <= var2) {
            var4 = var3[var1];
            var3[var1] = null;
            if(var1 == this.b("cgun") && this.b(var4)) {
               this.a(var4);
            }

            return var4;
         } else {
            var4 = var3[var1].splitStack(var2);
            if(var3[var1].stackSize == 0) {
               var3[var1] = null;
            }

            return var4;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int var1) {
      if(this.b[var1] != null) {
         ItemStack var2 = this.b[var1];
         this.b[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int var1, ItemStack var2) {
      if(var1 == this.b("cgun") && var2 != null && var2.getItem() instanceof ItemGun) {
         ItemGun var3 = (ItemGun)var2.getItem();
         int var4 = 0;

         for(int var5 = this.b("cattach1"); var5 <= this.b("cattach3"); ++var5) {
            if(var3.d(var2, var4) != null) {
               this.setInventorySlotContents(var5, new ItemStack(GunAttachment.a(var3.d(var2, var4))));
            }

            ++var4;
         }

         var3.D(var2);
      }

      if(var1 < this.a) {
         this.b[var1] = var2;
      }

   }

   public String getInvName() {
      return "Crafting Dead Inventory";
   }

   public boolean isInvNameLocalized() {
      return false;
   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public void onInventoryChanged() {}

   public void a(NBTTagList var1) {
      for(int var2 = 0; var2 < this.a; ++var2) {
         if(this.b[var2] != null) {
            NBTTagCompound var3 = new NBTTagCompound();
            var3.setByte("Slot", (byte)var2);
            this.b[var2].writeToNBT(var3);
            var1.appendTag(var3);
         }
      }

   }

   public void b(NBTTagList var1) {
      this.b = new ItemStack[this.a];

      for(int var2 = 0; var2 < var1.tagCount(); ++var2) {
         NBTTagCompound var3 = (NBTTagCompound)var1.tagAt(var2);
         byte var4 = var3.getByte("Slot");
         if(var4 != this.b("cgun") && var4 != this.b("cattach1") && var4 != this.b("cattach2") && var4 != this.b("cattach3") && var4 != this.b("cpaint")) {
            ItemStack var5 = ItemStack.loadItemStackFromNBT(var3);
            if(var5 != null && var4 < this.a) {
               this.b[var4] = var5;
            }
         }
      }

   }

   public void a(ItemStack var1) {
      if(var1 != null && var1.getItem() instanceof ItemGun) {
         ItemGun var2 = (ItemGun)var1.getItem();

         for(int var3 = this.b("cattach1"); var3 <= this.b("cattach3"); ++var3) {
            if(this.b[var3] != null && this.b[var3].getItem() instanceof ItemGunAttachment) {
               GunAttachment var4 = GunAttachment.b(this.b[var3].itemID);
               if(var4 != null && var2.a(var4)) {
                  var2.a(var1, var4);
                  this.setInventorySlotContents(var3, (ItemStack)null);
               }
            }
         }

         ItemStack var5 = this.a("cpaint");
         if(var5 != null) {
            GunPaint var6 = ((ItemPaint)var5.getItem()).d;
            var2.a(var1, var6, var5);
            this.setInventorySlotContents(this.b("cpaint"), (ItemStack)null);
         }
      }

   }

   public boolean a() {
      return this.b(this.a("cgun"));
   }

   public boolean b(ItemStack var1) {
      if(var1 != null && var1.getItem() instanceof ItemGun) {
         boolean var2 = false;
         ItemGun var3 = (ItemGun)var1.getItem();

         for(int var4 = this.b("cattach1"); var4 <= this.b("cattach3"); ++var4) {
            if(this.b[var4] != null) {
               GunAttachment var5 = GunAttachment.b(this.b[var4].itemID);
               if(var5 == null || !var3.a(var5)) {
                  return false;
               }

               var2 = true;
            }
         }

         ItemStack var6 = this.a("cpaint");
         if(var6 != null) {
            var2 = true;
         }

         if(!var2) {
            return false;
         } else {
            return true;
         }
      } else {
         return false;
      }
   }

   public boolean c(ItemStack var1) {
      if(var1 == null) {
         return true;
      } else {
         if(var1.getItem() instanceof ItemGunAttachment) {
            GunAttachment var2 = GunAttachment.b(var1.itemID);
            if(((ItemGun)this.a("cgun").getItem()).a(var2)) {
               return true;
            }
         }

         if(var1.getItem() instanceof ItemPaint) {
            GunPaint var3 = ((ItemPaint)var1.getItem()).d;
            if(((ItemGun)this.a("cgun").getItem()).a(var3)) {
               return true;
            }
         }

         return false;
      }
   }

   public ItemStack a(String var1) {
      int var2 = this.b(var1);
      return var2 != -1?this.b[var2]:null;
   }

   public int b(String var1) {
      return var1.equals("gun")?0:(var1.equals("backpack")?1:(var1.equals("vest")?2:(var1.equals("melee")?3:(var1.equals("hat")?4:(var1.equals("clothing")?5:(var1.equals("cgun")?6:(var1.equals("cattach1")?7:(var1.equals("cattach2")?8:(var1.equals("cattach3")?9:(var1.equals("cpaint")?10:-1))))))))));
   }

   public ArrayList f() {
      ArrayList var1 = new ArrayList();

      for(int var2 = 0; var2 < this.b.length; ++var2) {
         if(this.b[var2] != null) {
            var1.add(this.b[var2]);
         }
      }

      return var1;
   }

   public void h() {
      this.b = new ItemStack[this.a];
   }

   public ItemStack[] i() {
      return this.b;
   }

   public boolean isUseableByPlayer(EntityPlayer var1) {
      return true;
   }

   public void openChest() {}

   public void closeChest() {}

   public boolean isItemValidForSlot(int var1, ItemStack var2) {
      return false;
   }
}
