package com.craftingdead.inventory;

import com.craftingdead.inventory.InventoryBackpack;
import com.craftingdead.inventory.slot.SlotOpenBackpack;
import com.craftingdead.item.EnumBackpackSize;
import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemFuelTankBackpack;
import com.craftingdead.item.ItemTacticalVest;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerBackpack extends Container {

   private int a;
   private InventoryBackpack f;


   public ContainerBackpack(IInventory var1, IInventory var2) {
      this.f = (InventoryBackpack)var2;
      this.a = var2.getSizeInventory() / 9;
      var2.openChest();
      int var3 = (this.a - 4) * 18;

      int var4;
      int var5;
      for(var4 = 0; var4 < this.a; ++var4) {
         for(var5 = 0; var5 < 9; ++var5) {
            this.addSlotToContainer(new SlotOpenBackpack(var2, var5 + var4 * 9, 8 + var5 * 18, 18 + var4 * 18));
         }
      }

      for(var4 = 0; var4 < 3; ++var4) {
         for(var5 = 0; var5 < 9; ++var5) {
            this.addSlotToContainer(new Slot(var1, var5 + var4 * 9 + 9, 8 + var5 * 18, 103 + var4 * 18 + var3));
         }
      }

      for(var4 = 0; var4 < 9; ++var4) {
         this.addSlotToContainer(new Slot(var1, var4, 8 + var4 * 18, 161 + var3));
      }

   }

   public boolean canInteractWith(EntityPlayer var1) {
      return true;
   }

   public ItemStack transferStackInSlot(EntityPlayer var1, int var2) {
      ItemStack var3 = null;
      Slot var4 = (Slot)super.inventorySlots.get(var2);
      if(var4 != null && var4.getHasStack()) {
         ItemStack var5 = var4.getStack();
         var3 = var5.copy();
         if(var5.getItem() instanceof ItemGun && this.f.i() != EnumBackpackSize.e) {
            return null;
         }

         if(var5.getItem() instanceof ItemBackpack || var5.getItem() instanceof ItemTacticalVest || var5.getItem() instanceof ItemFuelTankBackpack) {
            return null;
         }

         if(var2 < this.a * 9) {
            if(!this.mergeItemStack(var5, this.a * 9, super.inventorySlots.size(), true)) {
               return null;
            }
         } else if(!this.mergeItemStack(var5, 0, this.a * 9, false)) {
            return null;
         }

         if(var5.stackSize == 0) {
            var4.putStack((ItemStack)null);
         } else {
            var4.onSlotChanged();
         }

         if(var5.stackSize == var3.stackSize) {
            return null;
         }

         var4.onPickupFromSlot(var1, var5);
      }

      return var3;
   }
}
