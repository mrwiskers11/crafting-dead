package com.craftingdead.event;

import com.craftingdead.item.gun.ItemGun;

import net.minecraft.item.ItemStack;
import net.minecraftforge.event.Event;

public class EventGunFired extends Event {

   private ItemStack a;
   private String b;


   public EventGunFired(String var1, ItemStack var2) {
      this.b = var1;
      this.a = var2;
   }

   public String a() {
      return this.b;
   }

   public ItemStack b() {
      return this.a;
   }

   public ItemGun c() {
      return (ItemGun)this.a.getItem();
   }
}
