package com.craftingdead.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class EventThirstQuenched extends Event {

   private EntityPlayer a;
   private ItemStack b;
   private int c;
   private int d;


   public EventThirstQuenched(EntityPlayer var1, ItemStack var2, int var3, int var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var3;
   }

   public EntityPlayer a() {
      return this.a;
   }

   public ItemStack b() {
      return this.b;
   }

   public int c() {
      return this.c;
   }

   public int d() {
      return this.d;
   }

   public int e() {
      int var1 = this.c + this.d;
      return var1 > 20?20:var1;
   }
}
