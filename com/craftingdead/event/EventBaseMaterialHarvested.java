package com.craftingdead.event;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.Event;

public class EventBaseMaterialHarvested extends Event {

   private Item a;
   private ItemStack b;


   public EventBaseMaterialHarvested(Item var1, ItemStack var2) {
      this.a = var1;
      this.b = var2;
   }

   public ItemStack a() {
      return this.b;
   }

   public Item b() {
      return this.a;
   }
}
