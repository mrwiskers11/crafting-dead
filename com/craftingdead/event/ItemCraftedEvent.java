package com.craftingdead.event;

import java.util.ArrayList;
import java.util.Iterator;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.Event;

public class ItemCraftedEvent extends Event {

   public ArrayList a = new ArrayList();
   public ItemStack b;


   public ItemCraftedEvent(ItemStack var1) {
      this.b = var1;
   }

   public boolean a(Class var1) {
      Iterator var2 = this.a.iterator();

      ItemStack var3;
      do {
         if(!var2.hasNext()) {
            return false;
         }

         var3 = (ItemStack)var2.next();
      } while(!var3.getItem().getClass().isAssignableFrom(var1));

      return true;
   }
}
