package com.craftingdead.event;

import com.craftingdead.item.gun.ItemGun;

import net.minecraft.item.ItemStack;
import net.minecraftforge.event.Event;

public class EventGunOutOfAmmo extends Event {

   private ItemStack a;


   public EventGunOutOfAmmo(ItemStack var1) {
      this.a = var1;
   }

   public ItemStack a() {
      return this.a;
   }

   public ItemGun b() {
      return (ItemGun)this.a.getItem();
   }
}
