package com.craftingdead.event;

import com.craftingdead.item.gun.ItemGun;

import net.minecraft.item.ItemStack;
import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class EventGunReload extends Event {

   private ItemGun a;
   private ItemStack b;
   private ItemStack c;


   public EventGunReload(ItemGun var1) {
      this.a = var1;
   }

   public void a(ItemStack var1) {
      this.b = var1;
   }

   public void b(ItemStack var1) {
      this.c = var1;
   }

   public ItemStack a() {
      return this.c;
   }

   public ItemStack b() {
      return this.b;
   }

   public ItemGun c() {
      return this.a;
   }
}
