package com.craftingdead.event;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class EventFlamethrowerSetFire extends Event {

   public EntityPlayer a;
   public EntityLivingBase b;
   public int c = 0;
   public double d = 0.0D;


   public EventFlamethrowerSetFire(EntityPlayer var1, EntityLivingBase var2, int var3, double var4) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
      this.d = var4;
   }
}
