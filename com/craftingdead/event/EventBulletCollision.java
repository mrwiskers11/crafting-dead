package com.craftingdead.event;

import com.craftingdead.event.EnumBulletCollision;
import com.craftingdead.item.gun.ItemGun;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class EventBulletCollision extends Event {

   private World a;
   private Entity b;
   private DamageSource c;
   private int d;
   private EntityPlayer e;
   private ItemStack f;
   private int g;
   private int h;
   private int i;
   private EnumBulletCollision j;


   public EventBulletCollision(EnumBulletCollision var1, EntityPlayer var2, ItemStack var3, World var4) {
      this.j = var1;
      this.e = var2;
      this.f = var3;
      this.a = var4;
   }

   public EventBulletCollision a(DamageSource var1, int var2) {
      this.c = var1;
      this.d = var2;
      return this;
   }

   public EventBulletCollision a(Entity var1) {
      this.b = var1;
      return this;
   }

   public EventBulletCollision a(int var1, int var2, int var3) {
      this.g = var1;
      this.h = var2;
      this.i = var3;
      return this;
   }

   public EnumBulletCollision a() {
      return this.j;
   }

   public ItemStack b() {
      return this.f;
   }

   public ItemGun c() {
      return (ItemGun)this.f.getItem();
   }

   public DamageSource d() {
      return this.c;
   }

   public int e() {
      return this.d;
   }

   public void a(int var1) {
      this.d = var1;
   }

   public int f() {
      return this.g;
   }

   public int g() {
      return this.h;
   }

   public int h() {
      return this.i;
   }

   public Entity i() {
      return this.b;
   }

   public EntityPlayer j() {
      return (EntityPlayer)this.b;
   }

   public EntityPlayer k() {
      return this.e;
   }

   public World l() {
      return this.a;
   }
}
