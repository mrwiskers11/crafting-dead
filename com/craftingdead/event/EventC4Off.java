package com.craftingdead.event;

import com.craftingdead.entity.EntityC4;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class EventC4Off extends Event {

   public String a = "touched";
   public EntityPlayer b;
   public EntityC4 c;


   public EventC4Off(String var1, EntityC4 var2, EntityPlayer var3) {
      this.a = var1;
      this.c = var2;
      this.b = var3;
   }
}
