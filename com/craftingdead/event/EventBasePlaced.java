package com.craftingdead.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;

@Cancelable
public class EventBasePlaced extends Event {

   private EntityPlayer a;


   public EventBasePlaced(EntityPlayer var1) {
      this.a = var1;
   }

   public EntityPlayer a() {
      return this.a;
   }
}
