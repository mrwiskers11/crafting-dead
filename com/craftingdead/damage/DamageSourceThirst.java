package com.craftingdead.damage;

import com.craftingdead.damage.DamageSourceCD;

public class DamageSourceThirst extends DamageSourceCD {

   public DamageSourceThirst() {
      super("thirst");
   }
}
