package com.craftingdead.damage;

import com.craftingdead.damage.DamageSourceCD;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ChatMessageComponent;

public class DamageSourceBleeding extends DamageSourceCD {

   public DamageSourceBleeding() {
      super("bleeding");
   }

   public ChatMessageComponent getDeathMessage(EntityLivingBase var1) {
      return ChatMessageComponent.createFromText("damage.cda.bleeding");
   }
}
