package com.craftingdead.damage;

import com.craftingdead.damage.DamageSourceCD;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;

public class DamageSourceBulletHeadshot extends DamageSourceCD {

   private Entity p;
   private ItemStack q;


   public DamageSourceBulletHeadshot(Entity var1, ItemStack var2) {
      super("bullethead");
      this.p = var1;
      this.q = var2;
   }

   public Entity getEntity() {
      return this.p;
   }

   public ItemStack s() {
      return this.q;
   }

   public ChatMessageComponent getDeathMessage(EntityLivingBase var1) {
      return ChatMessageComponent.createFromText("damage.cda.gun.bullet.head");
   }
}
