package com.craftingdead.damage;

import com.craftingdead.damage.DamageSourceCD;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ChatMessageComponent;

public class DamageSourceZombie extends DamageSourceCD {

   private Entity p;


   public DamageSourceZombie(Entity var1) {
      super("zombie");
      this.p = var1;
   }

   public Entity getEntity() {
      return this.p;
   }

   public ChatMessageComponent getDeathMessage(EntityLivingBase var1) {
      return ChatMessageComponent.createFromText("damage.cda.zombie");
   }
}
