package com.craftingdead;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockBaseCenter;
import com.craftingdead.block.BlockLoot;
import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.cloud.CMServer;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.cloud.cdplayer.EnumKarmaType;
import com.craftingdead.cloud.packet.serverpacket.PacketPlayerDataEditValue;
import com.craftingdead.damage.DamageSourceBleeding;
import com.craftingdead.damage.DamageSourceBullet;
import com.craftingdead.damage.DamageSourceBulletHeadshot;
import com.craftingdead.damage.DamageSourceRBInfection;
import com.craftingdead.damage.DamageSourceThirst;
import com.craftingdead.entity.EntityC4;
import com.craftingdead.entity.EntityCDZombie;
import com.craftingdead.entity.EntityCorpse;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.event.EnumBulletCollision;
import com.craftingdead.event.EventBulletCollision;
import com.craftingdead.event.EventFlamethrowerSetFire;
import com.craftingdead.event.ItemCraftedEvent;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.item.IItemAntiDupeHelper;
import com.craftingdead.item.ItemClothing;
import com.craftingdead.item.ItemClothingArmor;
import com.craftingdead.item.ItemClothingJuggernaut;
import com.craftingdead.item.ItemHatHelmet;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.ItemAmmo;
import com.craftingdead.item.gun.ItemGun;
import com.craftingdead.item.potions.PotionManager;
import com.craftingdead.network.packets.CDAPacketACLInCombat;
import com.craftingdead.network.packets.CDAPacketHandcuffInteract;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import cpw.mods.fml.relauncher.Side;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockButton;
import net.minecraft.block.BlockChest;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.BlockLever;
import net.minecraft.block.BlockTNT;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.Event.Result;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import net.minecraftforge.event.world.ChunkEvent.Load;

public class CommonEvents {

   public static final int a = 128;
   public static final boolean b = true;
   public static final boolean c = true;
   private boolean d = true;


   @ForgeSubscribe
   public void a(EntityInteractEvent var1) {
      if(var1.entityPlayer != null) {
         PlayerData var2 = PlayerDataHandler.a(var1.entityPlayer);
         if(var2.Q) {
            var1.setCanceled(true);
         }
      }

   }

   @ForgeSubscribe
   public void a(EventFlamethrowerSetFire var1) {
      if(var1.b instanceof EntityPlayer) {
         EntityPlayer var2 = (EntityPlayer)var1.b;
         PlayerData var3 = PlayerDataHandler.a(var2);
         if(var3.d() != null && var3.d().a("clothing") != null) {
            ItemStack var4 = var3.d().a("clothing");
            ItemClothing var5 = (ItemClothing)var4.getItem();
            if(var5.j()) {
               var1.c = 0;
               var1.d /= 2.0D;
            }
         }
      }

   }

   @ForgeSubscribe
   public void a(PlayerInteractEvent var1) {
      if(var1.entityPlayer != null) {
         EntityPlayer var2 = var1.entityPlayer;
         PlayerData var3 = PlayerDataHandler.a(var2);
         if(var3.Q) {
            if(var2.worldObj.isRemote) {
               PacketDispatcher.sendPacketToServer(CDAPacketHandcuffInteract.b());
            }

            if(var1.action == Action.LEFT_CLICK_BLOCK || var1.action == Action.RIGHT_CLICK_BLOCK) {
               var1.setCanceled(true);
            }
         }

         if(var1.action == Action.RIGHT_CLICK_BLOCK) {
            Block var4 = Block.blocksList[var1.entityPlayer.worldObj.getBlockId(var1.x, var1.y, var1.z)];
            if((var4 instanceof BlockChest || var4 instanceof BlockDoor) && BlockBaseCenter.e(var2.worldObj, var1.x, var1.y, var1.z)) {
               TileEntityBaseCenter var5 = BlockBaseCenter.j(var2.worldObj, var1.x, var1.y, var1.z);
               if(var5 != null) {
                  if(!var5.a(var2.username)) {
                     var1.setResult(Result.DENY);
                     var1.setCanceled(true);
                  }

                  if(FMLCommonHandler.instance().getSide() == Side.SERVER && MinecraftServer.getServer().getConfigurationManager().isPlayerOpped(var2.username)) {
                     var1.setResult(Result.ALLOW);
                     var1.setCanceled(false);
                  }

                  if(var1.isCanceled()) {
                     var2.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You do not have access to this base."));
                  }
               }
            }
         }
      }

   }

   @ForgeSubscribe
   public void a(AttackEntityEvent var1) {
      if(var1.entityPlayer != null) {
         PlayerData var2 = PlayerDataHandler.a(var1.entityPlayer);
         if(var2.Q) {
            var1.setCanceled(true);
         }
      }

   }

   @ForgeSubscribe
   public void a(CommandEvent var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER && (var1.command.getCommandName().equals("stop") || var1.command.getCommandName().equals("reload"))) {
         World var2 = MinecraftServer.getServer().getEntityWorld();

         for(int var3 = 0; var3 < var2.playerEntities.size(); ++var3) {
            EntityPlayer var4 = (EntityPlayer)var2.playerEntities.get(var3);
            PlayerData var5 = PlayerDataHandler.a(var4);
            var5.K = -1;
         }
      }

   }

   @ForgeSubscribe
   public void a(ItemCraftedEvent var1) {
      if(var1.a.size() == 2) {
         ItemStack var2 = null;
         ItemStack var3 = null;

         for(int var4 = 0; var4 < var1.a.size(); ++var4) {
            if(((ItemStack)var1.a.get(var4)).getItem() instanceof ItemAmmo) {
               var2 = (ItemStack)var1.a.get(var4);
            } else if(((ItemStack)var1.a.get(var4)).itemID == ItemManager.gU.itemID) {
               var3 = (ItemStack)var1.a.get(var4);
            }
         }

         if(var2 != null && var3 != null) {
            ItemAmmo var5 = (ItemAmmo)var2.getItem();
            var1.b.setItemDamage(var2.getItemDamage());
            var5.c(var1.b, true);
            return;
         }
      }

   }

   @ForgeSubscribe
   public void a(LivingDropsEvent var1) {
      if(!var1.entityLiving.worldObj.isRemote) {
         if(var1.drops.size() > 0) {
            ArrayList var2 = var1.drops;
            Iterator var3 = var2.iterator();

            while(var3.hasNext()) {
               EntityItem var4 = (EntityItem)var3.next();
               ItemStack var5 = var4.getEntityItem();
               EntityGroundItem var6 = new EntityGroundItem(var1.entityLiving.worldObj, var4.posX, var4.posY, var4.posZ);
               var6.a(var5);
               var1.entityLiving.worldObj.spawnEntityInWorld(var6);
            }

            var1.setCanceled(true);
         }

      }
   }

   @ForgeSubscribe
   public void a(EntityItemPickupEvent var1) {
      if(var1.item != null && var1.item.getEntityItem() != null) {
         ItemStack var2 = var1.item.getEntityItem();
         IItemAntiDupeHelper.a(var2);
      }

   }

   @ForgeSubscribe
   public void a(ItemTossEvent var1) {
      World var2 = var1.player.worldObj;
      EntityItem var3 = var1.entityItem;
      ItemStack var4 = var3.getEntityItem();
      if(!var2.isRemote) {
         if(!(var4.getItem() instanceof ItemBlock)) {
            EntityGroundItem var5 = new EntityGroundItem(var2, var3.posX, var3.posY, var3.posZ);
            var5.a(var4);
            var2.spawnEntityInWorld(var5);
            var1.setCanceled(true);
         }
      }
   }

   @ForgeSubscribe
   public void a(PlayerDropsEvent var1) {
      EntityPlayer var2 = var1.entityPlayer;
      if(!var2.worldObj.isRemote) {
         PlayerData var3 = PlayerDataHandler.a(var2);
         ItemStack[] var4 = var3.d().i();
         if(var3.Q) {
            ItemStack var5 = new ItemStack(ItemManager.fe);
            var5.setItemDamage(var3.R);
            EntityGroundItem var6 = new EntityGroundItem(var2.worldObj, var2.posX, var2.posY, var2.posZ);
            var6.a(var5);
            var2.worldObj.spawnEntityInWorld(var6);
            var3.Q = false;
         }

         EntityCorpse var7 = new EntityCorpse(var2);

         int var8;
         for(var8 = 0; var8 < var4.length; ++var8) {
            if(var4[var8] != null) {
               var1.drops.add(new EntityItem(var2.worldObj, var2.posX, var2.posY, var2.posZ, var4[var8]));
            }
         }

         for(var8 = 0; var8 < var1.drops.size(); ++var8) {
            if(var1.drops.get(var8) != null && ((EntityItem)var1.drops.get(var8)).getEntityItem().getItem() instanceof ItemClothingArmor) {
               var1.drops.remove(var8);
            }
         }

         var3.c();
         var7.b(var1.drops);
         if(!var3.L) {
            var2.worldObj.spawnEntityInWorld(var7);
         }
      }

      var1.setCanceled(true);
   }

   @ForgeSubscribe
   public void a(LivingAttackEvent var1) {
      if(var1.source.getSourceOfDamage() != null) {
         EntityLivingBase var2 = var1.entityLiving;
         if(var2 instanceof EntityPlayer) {
            this.d = !this.d;
            EntityPlayer var3 = (EntityPlayer)var2;
            PlayerData var4 = PlayerDataHandler.a(var3);
            Random var5 = new Random();
            if(var3.isDead) {
               return;
            }

            if(this.d) {
               return;
            }

            if(var3.worldObj.isRemote) {
               return;
            }

            if(var3.capabilities.isCreativeMode) {
               return;
            }

            if(var1.source.getEntity() != null && var1.source.getEntity() instanceof EntityPlayer) {
               EntityPlayer var6 = (EntityPlayer)var1.source.getEntity();
               if(FactionManager.c().a(var3.username, var6.username)) {
                  var1.setCanceled(true);
                  return;
               }
            }

            if(!(var1.source instanceof DamageSourceBleeding) && !(var1.source instanceof DamageSourceThirst) && !(var1.source instanceof DamageSourceRBInfection) && var1.ammount > 0.0F) {
               PacketDispatcher.sendPacketToPlayer(CDAPacketACLInCombat.b(), (Player)var3);
            }

            int var10;
            if(var1.source.getSourceOfDamage() instanceof EntityCDZombie) {
               var10 = var5.nextInt(100);
               byte var7 = 10;
               if(var10 <= var7 && !var3.isPotionActive(PotionManager.a.id)) {
                  var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You\'ve been infected with RBI!"));
                  var3.addPotionEffect(new PotionEffect(PotionManager.a.id, 9999999));
                  return;
               }
            }

            var10 = var5.nextInt(100);
            int var11 = 22;
            if(var4.d().a("clothing") != null) {
               ItemStack var8 = var4.d().a("clothing");
               int var9 = ((ItemClothing)var8.getItem()).i();
               if(var9 == 2 || var9 == 3) {
                  var11 /= 2;
               }
            }

            if(var10 <= var11 && !var3.isPotionActive(PotionManager.b.id) && var1.ammount >= 4.0F) {
               var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You\'re bleeding out!"));
               var3.addPotionEffect(new PotionEffect(PotionManager.b.id, 9999999));
            }
         }
      }

   }

   @ForgeSubscribe
   public void a(Load var1) {
      boolean var2 = false;
      int var3 = 0;

      for(int var4 = 0; var4 < 16; ++var4) {
         for(int var5 = 0; var5 < 256; ++var5) {
            for(int var6 = 0; var6 < 16; ++var6) {
               int var7 = var1.getChunk().getBlockID(var4, var5, var6);
               int var8 = BlockManager.a(var7);
               if(var7 != 0 && var8 != -1) {
                  var1.getChunk().setBlockIDWithMetadata(var4, var5, var6, var8, 0);
                  ++var3;
                  var2 = true;
               }
            }
         }
      }

      if(var2) {
         CraftingDead.d.info("Replaced a total of " + var3 + " OLD Loot Spawner(s) in one chunk.");
      }

   }

   @ForgeSubscribe
   public void a(LivingDeathEvent var1) {
      EntityPlayer var2;
      PlayerData var3;
      if(var1.entityLiving instanceof EntityPlayer) {
         var2 = (EntityPlayer)var1.entityLiving;
         var3 = PlayerDataHandler.a(var2);
         if(var3.Q) {
            EntityGroundItem var4 = new EntityGroundItem(var2.worldObj, var2.posX, var2.posY, var2.posZ);
            var4.a(new ItemStack(ItemManager.fe));
            var2.worldObj.spawnEntityInWorld(var4);
         }

         if(!var2.worldObj.isRemote) {
            var3.a();
         }

         if(var1.source.getSourceOfDamage() instanceof EntityPlayer) {
            EntityPlayer var9 = (EntityPlayer)var1.source.getSourceOfDamage();
            if(!var9.worldObj.isRemote) {
               ++var3.h;
            }

            if(FMLCommonHandler.instance().getSide() == Side.SERVER && CraftingDead.m().m) {
               CDPlayer var5 = PlayerDataHandler.a(var2.username);
               EnumKarmaType var6 = var5.a();
               CDPlayer var7 = PlayerDataHandler.a(var9.username);
               EnumKarmaType var8 = var5.a();
               CMServer.b(new PacketPlayerDataEditValue(var7.b(), "pkills", 1));
               if(var8 != null) {
                  if(var8.k) {
                     if(var6 == null) {
                        CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", -300));
                     } else if(var6.k) {
                        CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", -500));
                     } else {
                        CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", 300));
                     }
                  } else if(var6 == null) {
                     CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", -200));
                  } else if(var6.k) {
                     CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", -400));
                  } else {
                     CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", 200));
                  }
               } else if(var6 == null) {
                  CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", -200));
               } else if(var6.k) {
                  CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", -500));
               } else {
                  CMServer.b(new PacketPlayerDataEditValue(var7.b(), "karma", 500));
               }
            }
         }
      }

      if(var1.entityLiving instanceof EntityCDZombie && var1.source.getSourceOfDamage() instanceof EntityPlayer) {
         var2 = (EntityPlayer)var1.source.getSourceOfDamage();
         if(!var2.worldObj.isRemote) {
            var3 = PlayerDataHandler.b(var2.username);
            ++var3.i;
            if(FMLCommonHandler.instance().getSide() == Side.SERVER && CraftingDead.m().m) {
               CMServer.b(new PacketPlayerDataEditValue(var3.a, "zkills", 1));
               CMServer.b(new PacketPlayerDataEditValue(var3.a, "karma", 20));
            }
         }
      }

   }

   @ForgeSubscribe
   public void a(EventBulletCollision var1) {
      if(!var1.l().isRemote) {
         if(var1.a() == EnumBulletCollision.c) {
            if(var1.i() instanceof EntityC4) {
               EntityC4 var2 = (EntityC4)var1.i();
               var2.a(var1.k(), "shot");
            }

            if(var1.i() instanceof EntityCDZombie && ItemGun.H().s(var1.b())) {
               var1.a(var1.e() * 2);
            }
         } else if(var1.a() == EnumBulletCollision.b) {
            EntityPlayer var8 = var1.j();
            PlayerData var3 = PlayerDataHandler.a(var8);
            PacketDispatcher.sendPacketToPlayer(CDAPacketACLInCombat.b(), (Player)var8);
            if(FactionManager.c().a(var8.username, var1.k().username)) {
               var1.setCanceled(true);
               return;
            }

            if(var8.isEntityInvulnerable()) {
               var1.setCanceled(true);
               return;
            }

            if(!var3.F) {
               var1.setCanceled(true);
               return;
            }

            int var5;
            int var6;
            if(ItemGun.H().s(var1.b())) {
               Random var4 = new Random();
               var5 = var4.nextInt(100);
               var6 = 20;
               if(var3.d().a("clothing") != null) {
                  int var7 = ((ItemClothing)var3.d().a("clothing").getItem()).i();
                  if(var7 == 2 || var7 == 3) {
                     var6 /= 2;
                  }
               }

               if(var5 <= var6 && !var8.isPotionActive(PotionManager.a.id)) {
                  var8.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You have been infected with RBI!"));
                  var8.addPotionEffect(new PotionEffect(PotionManager.a.id, 9999999));
                  return;
               }
            }

            ItemStack var9;
            if(var1.d() instanceof DamageSourceBulletHeadshot) {
               var9 = var3.d().a("hat");
               if(var9 != null && var9.getItem() instanceof ItemHatHelmet) {
                  ItemAmmo var11 = var1.c().x(var1.b());
                  if(var11 != null && var11.cD > 75.0D) {
                     return;
                  }

                  var6 = var1.e();
                  var1.a((int)((double)var6 * 0.8D));
               }
            } else if(var3.d().a("vest") != null) {
               int var10 = (int)((double)var1.e() * 0.8D);
               var1.a(var10);
            }

            if(var3.d().a("clothing") != null) {
               var9 = var3.d().a("clothing");
               if(var9.getItem() instanceof ItemClothingJuggernaut) {
                  var5 = var1.e();
                  Random var13 = new Random();
                  if(var13.nextBoolean()) {
                     var1.a((int)((double)var5 * 0.75D));
                  }
               }
            }

            if(var1.k().getHeldItem().getItem() == ItemManager.Q) {
               var8.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 400, 1));
               var8.addPotionEffect(new PotionEffect(Potion.blindness.id, 400, 1));
               var8.addPotionEffect(new PotionEffect(Potion.confusion.id, 400, 1));
               var9 = var8.getHeldItem();
               if(var9.getItem() instanceof ItemGun) {
                  var8.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You have been Tased by " + var1.k().getEntityName()));
                  Random var12 = new Random();
                  if(var12.nextInt(5) == 0) {
                     ItemStack var14 = var9.copy();
                     var8.dropPlayerItem(var14);
                     var8.inventory.consumeInventoryItem(var9.itemID);
                  }
               }
            }
         }
      }

   }

   public void a(EntityPlayer var1, ItemStack var2, EntityLivingBase var3, boolean var4, Vec3 var5, World var6, int var7) {
      ItemGun var8 = (ItemGun)var2.getItem();
      ItemAmmo var9 = var8.x(var2);
      int var10 = var8.b(var2, var4);
      if(var7 > 0) {
         var10 -= var10 * var7 / 100;
      }

      this.a("Start: " + var10);
      Object var11 = var4?new DamageSourceBulletHeadshot(var1, var2):new DamageSourceBullet(var1, var2);
      EventBulletCollision var12 = new EventBulletCollision(var3 instanceof EntityPlayer?EnumBulletCollision.b:EnumBulletCollision.c, var1, var2, var6);
      var12.a(var3);
      var12.a((DamageSource)var11, var10);
      if(!MinecraftForge.EVENT_BUS.post(var12)) {
         int var13 = var12.e();
         this.a("After Event: " + var13);
         int var14 = (int)var3.getHealth();
         if(var4) {
            var1.worldObj.playSoundAtEntity(var1, "random.break", 2.0F, 1.5F);
            var3.worldObj.playSoundAtEntity(var3, "random.break", 2.0F, 1.5F);
         }

         Random var15;
         if(var2.getItem() == ItemManager.P) {
            var15 = new Random();
            if(var15.nextInt(4) == 0) {
               EntityGroundItem var16 = new EntityGroundItem(var6, var3.posX, var3.posY, var3.posZ);
               var16.a(new ItemStack(ItemManager.aw));
               var6.spawnEntityInWorld(var16);
            }
         }

         if((var3 instanceof EntityPlayerMP || var3 instanceof EntityCDZombie) && var2.getItem() instanceof ItemGun && var8.t(var2)) {
            var15 = new Random();
            if(var15.nextInt(5) == 0) {
               var3.setFire(2);
            }
         }

         var3.hurtResistantTime = 0;
         var3.attackEntityFrom((DamageSource)var11, (float)var13);
         var3.motionX = var3.motionY = var3.motionZ = 0.0D;
         if(var3 instanceof EntityPlayer && !var4 && !var3.isDead && var3.getHealth() > 0.0F) {
            EntityPlayer var22 = (EntityPlayer)var3;
            PlayerData var23 = PlayerDataHandler.a(var22);
            if(var23.d().a("clothing") != null && var9 != null) {
               int var17 = var14 - (int)var3.getHealth();
               this.a("Dealt Damage: " + var17);
               double var18 = var9.cD;
               if(var18 <= 5.0D) {
                  return;
               }

               int var20 = (int)((double)((float)(var13 - var17)) * (var18 / 100.0D));
               this.a("AP Extra Damage: " + var20);
               float var21 = var22.getHealth() - (float)var20;
               var22.setHealth(var21);
               if(var21 <= 0.0F) {
                  var22.onDeath((DamageSource)var11);
               }
            }
         }

      }
   }

   private void a(World var1, Entity var2, double var3, double var5, double var7, float var9, boolean var10) {
      var1.createExplosion(var2, var3, var5, var7, var9, var10);
   }

   public void a(EntityPlayer var1, ItemStack var2, World var3, double var4, double var6, double var8, int var10, int var11, Vec3 var12) {
      if(var1 instanceof EntityPlayerMP) {
         EntityPlayerMP var13 = (EntityPlayerMP)var1;
         PlayerData var14 = PlayerDataHandler.a((EntityPlayer)var13);
         Block var15 = Block.blocksList[var3.getBlockId((int)var4, (int)var6, (int)var8)];
         EventBulletCollision var16 = new EventBulletCollision(EnumBulletCollision.a, var1, var2, var3);
         var16.a((int)var4, (int)var6, (int)var8);
         if(MinecraftForge.EVENT_BUS.post(var16)) {
            return;
         }

         if(var15 != null) {
            if(var2.getItem() instanceof ItemGun) {
               ItemGun var17 = (ItemGun)var2.getItem();
               if(var17.u(var2)) {
                  Random var18 = new Random();
                  if(var18.nextInt(8) == 0) {
                     this.a(var3, var1, var4, var6, var8, 1.2F, false);
                  }
               }
            }

            if((var15 instanceof BlockLever || var15 instanceof BlockTNT || var15 instanceof BlockButton || var15 instanceof BlockLoot) && var14.x <= 0) {
               if(var15 instanceof BlockTNT) {
                  ((BlockTNT)var15).primeTnt(var3, (int)var4, (int)var6, (int)var8, 5, var1);
                  var3.setBlock((int)var4, (int)var6, (int)var8, 0);
               }

               var13.theItemInWorldManager.activateBlockOrUseItem(var13, MinecraftServer.getServer().worldServerForDimension(var13.dimension), var2, (int)var4, (int)var6, (int)var8, var11, 0.0F, 0.0F, 0.0F);
               var14.x = 5;
            }
         }

         if(var13.inventory.getCurrentItem().getItem() == ItemManager.P) {
            EntityGroundItem var19 = new EntityGroundItem(var3, (double)var16.f(), (double)var16.g(), (double)var16.h());
            var19.a(new ItemStack(ItemManager.aw));
            var3.spawnEntityInWorld(var19);
         }
      }

   }

   @ForgeSubscribe
   public void a(LivingUpdateEvent var1) {
      if(var1.entityLiving != null && var1.entityLiving instanceof EntityPlayer) {
         CraftingDead.f.c().e().a(var1);
      }

   }

   public void a(String var1) {}
}
