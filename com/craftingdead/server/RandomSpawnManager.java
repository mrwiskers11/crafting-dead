package com.craftingdead.server;

import com.craftingdead.CraftingDead;
import com.craftingdead.utils.BlockLocation;
import com.f3rullo14.fds.FDSCompound;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

public class RandomSpawnManager {

   private static final Random a = new Random();
   private ArrayList b = new ArrayList();


   public void a(BlockLocation var1) {
      this.b.add(var1);
   }

   public void a() {
      this.b.clear();
   }

   public BlockLocation b() {
      return this.b.isEmpty()?null:(BlockLocation)this.b.get(a.nextInt(this.b.size()));
   }

   public void a(FDSCompound var1) {
      int var2 = var1.b("size");

      for(int var3 = 0; var3 < var2; ++var3) {
         int var4 = var1.b("rspawnPosX" + var3);
         int var5 = var1.b("rspawnPosY" + var3);
         int var6 = var1.b("rspawnPosZ" + var3);
         BlockLocation var7 = new BlockLocation(var4, var5, var6);
         this.b.add(var7);
      }

   }

   public void b(FDSCompound var1) {
      if(this.b.size() > 0) {
         var1.a("size", this.b.size());
         int var2 = this.b.size();

         for(int var3 = 0; var3 < var2; ++var3) {
            BlockLocation var4 = (BlockLocation)this.b.get(var3);
            var1.a("rspawnPosX" + var3, var4.b);
            var1.a("rspawnPosY" + var3, var4.c);
            var1.a("rspawnPosZ" + var3, var4.d);
         }
      }

   }

   public void c() {
      File var1 = new File(CraftingDead.f.k(), "rspawn.fds");
      FDSCompound var2 = new FDSCompound(var1);
      var2.b();
      this.b(var2);
      var2.c();
   }

   public void d() {
      File var1 = new File(CraftingDead.f.k(), "rspawn.fds");
      FDSCompound var2 = new FDSCompound(var1);
      if(!var1.exists()) {
         var2.c();
      }

      var2.a();
      this.a(var2);
   }

}
