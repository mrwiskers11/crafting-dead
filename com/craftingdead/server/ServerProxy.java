package com.craftingdead.server;

import com.craftingdead.CraftingDead;
import com.craftingdead.IProxy;
import com.craftingdead.cloud.CMServer;
import com.craftingdead.cloud.packet.PacketClientRequest;
import com.craftingdead.cloud.packet.RequestType;
import com.craftingdead.item.ItemManager;
import com.craftingdead.player.PlayerDataHandler;
import com.craftingdead.utils.ConfigFile;
import com.f3rullo14.fds.FFReader;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;

public class ServerProxy implements IProxy {

   private ConfigFile o;
   public static boolean a = false;
   public static boolean b = false;
   public static String c = "Addition Mods are not allowed on this server.";
   public static String d = "Custom Texturepacks are not allowed on this server.";
   public static boolean e = false;
   public static boolean f = false;
   public static int g = 100;
   public static long h = -1L;
   public static boolean i = false;
   public String j = "";
   public String k = "cloud.craftingdead.com";
   public boolean l = false;
   public boolean m = false;
   public String n = "";
   private CMServer p;


   public void a(FMLPreInitializationEvent var1) {
      try {
         this.b();
      } catch (IOException var3) {
         var3.printStackTrace();
      }

      this.j = UUID.randomUUID().toString();
      this.p = new CMServer(this);
   }

   public void a(FMLInitializationEvent var1) {}

   public void a(FMLPostInitializationEvent var1) {
      MinecraftServer var2 = FMLCommonHandler.instance().getMinecraftServerInstance();
      String var3 = "1.2.5&&" + var2.getMOTD();
      var2.setMOTD(var3);
      CraftingDead.d.info("MOTD:" + var2.getMOTD());
   }

   public void a() {}

   public void b() throws IOException {
      this.o = new ConfigFile(new File(CraftingDead.f.k(), "server.properties"));
      a = this.o.a("allow.mods", false, "Allow the client modpack to be edited on this server?");
      b = this.o.a("allow.texturepacks", false, "Allow the client modpack to run texturepacks on this server?");
      c = this.o.a("message.mods.kicked", "Addition Mods are not allowed on this server.", "Message when a player is kicked from the server for having extra mods installed.");
      d = this.o.a("message.texturepacks.kicked", "Custom Texturepacks are not allowed on this server.", "Message when a player is kicked for having Custom Texturepacks.");
      e = this.o.a("allow.vanilla.items", true, "Checks for vanilla items, removes them if false. (Add items in whitelist_vanilla, item IDs)");
      f = this.o.a("allow.vanilla.armor", false, "If true, Crafting Dead\'s Armor system will be turned off.");
      g = this.o.a("network.playerdata.dispatch", 100, "The distance in blocks a player\'s CD Data will be sent to another player.");
      h = (long)this.o.a("client.modpack.size", -1);
      String var1 = this.o.a("cloud.ip", "default", "Data Base IP, default for hard coded ip address");
      this.l = this.o.a("cloud.official", false);
      this.n = this.o.a("cloud.official.password", "password");
      i = this.o.a("allow.bases", true, "When set to true, players will be able to build bases.");
      if(!var1.equals("default")) {
         this.k = var1;
      }

      this.c();
   }

   public void c() {
      FFReader var1 = new FFReader();
      ArrayList var2 = var1.a(new File(CraftingDead.f.k(), "whitelist_vanilla.txt"));

      for(int var3 = 0; var3 < var2.size(); ++var3) {
         int var4 = Integer.parseInt(((String)var2.get(var3)).toLowerCase().trim());
         if(!ItemManager.g.contains(Integer.valueOf(var4))) {
            ItemManager.g.add(Integer.valueOf(var4));
         }
      }

      if(var2.size() > 0) {
         if(!e) {
            CraftingDead.d.info("Enforcing rule \'ALLOW_VANILLA_ITEMS\' = false. Vanilla items that are not whitelisted will be removed from gameplay.");
         }

         CraftingDead.d.info("Added Additional Whitelisted Vanilla Items...");
      }

   }

   public void a(String var1) {
      PacketClientRequest.b(RequestType.a, new String[]{var1});
   }

   public void b(String var1) {
      PlayerDataHandler.c(var1);
      PlayerDataHandler.a.remove(var1);
   }

   public static void c(String var0) {
      String[] var1 = MinecraftServer.getServer().getAllUsernames();
      int var2 = var1.length;

      for(int var3 = 0; var3 < var2; ++var3) {
         String var4 = var1[var3];
         d(var4).sendChatToPlayer(ChatMessageComponent.createFromText(var0));
      }

   }

   public static EntityPlayerMP d(String var0) {
      return var0 == null?null:MinecraftServer.getServer().getConfigurationManager().getPlayerForUsername(var0);
   }

   public CMServer d() {
      return this.p;
   }

}
