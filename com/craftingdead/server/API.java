package com.craftingdead.server;

import com.craftingdead.CraftingDead;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.util.logging.Level;
import net.minecraft.entity.player.EntityPlayerMP;

public class API {

   public static void a(String var0, int var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         EntityPlayerMP var2 = FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().getPlayerForUsername(var0);
         if(var2 != null) {
            try {
               var2.removePotionEffect(var1);
            } catch (Exception var4) {
               CraftingDead.d.log(Level.WARNING, "Failed to remove a potion effect", var4);
            }
         }
      }

   }

   public static void changeWayer(String var0, int var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.b(var0).e().c(var1);
      }

   }

   public static void a(String var0, boolean var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.b(var0).C = var1;
      }

   }

   public static void b(String var0, boolean var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.b(var0).F = var1;
      }

   }

   public static void c(String var0, boolean var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.b(var0).G = var1;
      }

   }

   public static void d(String var0, boolean var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.b(var0).H = var1;
      }

   }

   public static void e(String var0, boolean var1) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         PlayerDataHandler.b(var0).I = var1;
      }

   }
}
