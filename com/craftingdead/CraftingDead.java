package com.craftingdead;

import com.craftingdead.CommonEvents;
import com.craftingdead.CommonPlayerTracker;
import com.craftingdead.CommonTickHandler;
import com.craftingdead.IProxy;
import com.craftingdead.block.BlockManager;
import com.craftingdead.client.ClientProxy;
import com.craftingdead.client.gui.GuiHandler;
import com.craftingdead.cloud.MySQLManager;
import com.craftingdead.commands.CommandManager;
import com.craftingdead.entity.EntityManager;
import com.craftingdead.faction.FactionManager;
import com.craftingdead.item.ItemManager;
import com.craftingdead.network.NetworkManager;
import com.craftingdead.network.PacketHandlerClient;
import com.craftingdead.network.PacketHandlerServer;
import com.craftingdead.server.RandomSpawnManager;
import com.craftingdead.server.ServerProxy;
import com.craftingdead.world.WorldManager;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.NetworkMod.SidedPacketHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import java.io.File;
import java.util.logging.Logger;
import net.minecraftforge.common.MinecraftForge;

@Mod(
   modid = "craftingdead",
   name = "Crafting Dead Origins",
   version = "1.2.5"
)
@NetworkMod(
   clientSideRequired = false,
   serverSideRequired = false,
   clientPacketHandlerSpec = @SidedPacketHandler(
   channels = {"cdaNetworking"},
   packetHandler = PacketHandlerClient.class
),
   serverPacketHandlerSpec = @SidedPacketHandler(
   channels = {"cdaNetworking"},
   packetHandler = PacketHandlerServer.class
)
)
public class CraftingDead {
   public static final String a = "craftingdead";
   public static final String b = "1.2.5";
   public static final String c = "Crafting Dead Origins";
   public static final Logger d = Logger.getLogger("craftingdead");
   @SidedProxy(
      clientSide = "com.craftingdead.client.ClientProxy",
      serverSide = "com.craftingdead.server.ServerProxy"
   )
   public static IProxy e;
   @Instance("craftingdead")
   public static CraftingDead f;
   private File g;
   private GuiHandler h;
   private ItemManager i;
   private BlockManager j;
   private EntityManager k;
   private WorldManager l;
   private NetworkManager m;
   private CommandManager n;
   private CommonEvents o;
   private CommonTickHandler p;
   private CommonPlayerTracker q;
   private MySQLManager r;
   private FactionManager s;
   private RandomSpawnManager t;

   @EventHandler
   public void a(FMLPreInitializationEvent var1) {
      d.info("Initializing");
      this.g = new File(System.getProperty("user.dir"), "craftingdead");
      Runtime.getRuntime().addShutdownHook(new innerClass(this));
      e.a(var1);
      this.r = new MySQLManager();
      this.s = new FactionManager();
   }

   @EventHandler
   public void a(FMLInitializationEvent var1) {
      e.a(var1);
      this.o = new CommonEvents();
      MinecraftForge.EVENT_BUS.register(this.o);
      this.t = new RandomSpawnManager();
      this.i = new ItemManager();
      this.i.c();
      this.p = new CommonTickHandler();
      TickRegistry.registerTickHandler(this.p, Side.SERVER);
      this.n = new CommandManager();
      this.j = new BlockManager();
      this.j.d();
      this.m = new NetworkManager();
      this.m.a();
      this.k = new EntityManager();
      this.k.a();
      this.l = new WorldManager();
      this.h = new GuiHandler();
      NetworkRegistry.instance().registerGuiHandler(this, this.h);
      this.q = new CommonPlayerTracker();
      GameRegistry.registerPlayerTracker(this.q);
   }

   @EventHandler
   public void a(FMLPostInitializationEvent var1) {
      e.a(var1);
   }

   @EventHandler
   public void a(FMLServerStartingEvent var1) {
      this.n.a(var1);
      this.t.d();
      this.p.a(var1);
   }

   @EventHandler
   public void a(FMLServerStoppedEvent var1) {
      this.t.c();
      this.p.a(var1);
   }

   public NetworkManager a() {
      return this.m;
   }

   public CommonEvents b() {
      return this.o;
   }

   public ItemManager c() {
      return this.i;
   }

   public EntityManager d() {
      return this.k;
   }

   public BlockManager e() {
      return this.j;
   }

   public WorldManager f() {
      return this.l;
   }

   public RandomSpawnManager g() {
      return this.t;
   }

   public CommonTickHandler h() {
      return this.p;
   }

   public FactionManager i() {
      return this.s;
   }

   public MySQLManager j() {
      return this.r;
   }

   public File k() {
      return this.g;
   }

   public static ClientProxy l() {
      if(e instanceof ClientProxy) {
         return (ClientProxy)e;
      } else {
         throw new RuntimeException("Accessing client proxy from wrong side!");
      }
   }

   public static ServerProxy m() {
      if(e instanceof ServerProxy) {
         return (ServerProxy)e;
      } else {
         throw new RuntimeException("Accessing server proxy from wrong side!");
      }
   }

   public static int a(String var0) {
      return FMLCommonHandler.instance().getSide().isClient()?RenderingRegistry.addNewArmourRendererPrefix(var0):0;
   }
   
   class innerClass extends Thread {
	   final CraftingDead a;

	   innerClass(CraftingDead var1) {
	      this.a = var1;
	   }

	   public void run() {
	      CraftingDead.e.a();
	   }
	}

}
