package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.client.gui.gui.blueprint.GuiBlueprintRecipes;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.Random;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockBaseCenter extends BlockContainer {

   protected BlockBaseCenter(int var1) {
      super(var1, Material.rock);
      this.setBlockUnbreakable();
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
      this.setCreativeTab(ItemManager.a);
   }

   @SideOnly(Side.CLIENT)
   public boolean onBlockActivated(World var1, int var2, int var3, int var4, EntityPlayer var5, int var6, float var7, float var8, float var9) {
      TileEntityBaseCenter var10 = (TileEntityBaseCenter)var1.getBlockTileEntity(var2, var3, var4);
      if(var1.isRemote && var10 != null && var10.f() != null && var10.f().equals(var5.username)) {
         Minecraft.getMinecraft().displayGuiScreen(new GuiBlueprintRecipes(true));
      }

      return false;
   }

   public void b(World var1, int var2, int var3, int var4, EntityPlayer var5) {
      this.k(var1, var2, var3, var4);
      var1.destroyBlock(var2, var3, var4, false);
      if(var5 != null) {
         var5.inventory.addItemStackToInventory(new ItemStack(ItemManager.ex));
      }

   }

   public static boolean e(World var0, int var1, int var2, int var3) {
      byte var4 = 7;
      int var5 = var4 * 2;
      int var6 = var4 * 2;

      for(int var7 = -var4; var7 < var4; ++var7) {
         for(int var8 = -var5; var8 < var6; ++var8) {
            for(int var9 = -var4; var9 < var4; ++var9) {
               int var10 = var0.getBlockId(var7 + var1, var8 + var2, var9 + var3);
               if(var10 == BlockManager.N.blockID) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   public static int[] i(World var0, int var1, int var2, int var3) {
      byte var4 = 7;
      int var5 = var4 * 2;
      int var6 = var4 * 2;

      for(int var7 = -var4; var7 < var4; ++var7) {
         for(int var8 = -var5; var8 < var6; ++var8) {
            for(int var9 = -var4; var9 < var4; ++var9) {
               int var10 = var0.getBlockId(var7 + var1, var8 + var2, var9 + var3);
               if(var10 == BlockManager.N.blockID) {
                  return new int[]{var7 + var1, var8 + var2, var9 + var3};
               }
            }
         }
      }

      return null;
   }

   public static TileEntityBaseCenter j(World var0, int var1, int var2, int var3) {
      int[] var4 = i(var0, var1, var2, var3);
      return var4 != null?(TileEntityBaseCenter)var0.getBlockTileEntity(var4[0], var4[1], var4[2]):null;
   }

   public void k(World var1, int var2, int var3, int var4) {
      Random var5 = new Random();
      byte var6 = 7;
      byte var7 = 3;
      int var8 = var6 * 2;

      for(int var9 = -var6; var9 <= var6; ++var9) {
         for(int var10 = -var7; var10 <= var8; ++var10) {
            int var11 = -var6;

            while(var11 <= var6) {
               int var12 = var1.getBlockId(var9 + var2, var10 + var3, var11 + var4);
               int var13 = 0;

               while(true) {
                  if(var13 < BlockManager.z.length) {
                     if(var12 != BlockManager.z[var13]) {
                        ++var13;
                        continue;
                     }

                     var1.destroyBlock(var9 + var2, var10 + var3, var11 + var4, false);
                     EntityGroundItem var14;
                     if(var12 == 5) {
                        var14 = new EntityGroundItem(var1, (double)(var9 + var2), (double)(var10 + var3), (double)(var11 + var4));
                        var14.a(new ItemStack(ItemManager.eP, var5.nextInt(3) + 1, 0));
                        var1.spawnEntityInWorld(var14);
                     }

                     if(var12 == 98) {
                        var14 = new EntityGroundItem(var1, (double)(var9 + var2), (double)(var10 + var3), (double)(var11 + var4));
                        var14.a(new ItemStack(ItemManager.eO, var5.nextInt(3) + 1, 0));
                        var1.spawnEntityInWorld(var14);
                     }
                  }

                  ++var11;
                  break;
               }
            }
         }
      }

   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
   }

   public int getRenderType() {
      return BlockManager.b;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityBaseCenter();
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }
}
