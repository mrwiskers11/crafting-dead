package com.craftingdead.block;

import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.util.Icon;

public class BlockCD extends Block {

   private String a = "defaultcdblock";


   public BlockCD(int var1) {
      super(var1, Material.rock);
      this.setCreativeTab(ItemManager.a);
   }

   public BlockCD(int var1, Material var2) {
      super(var1, var2);
      this.setCreativeTab(ItemManager.a);
   }

   @SideOnly(Side.CLIENT)
   public Icon getIcon(int var1, int var2) {
      return super.blockIcon;
   }

   public BlockCD a(String var1) {
      this.a = var1;
      return this;
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:" + this.a);
   }
}
