package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.LootType;
import com.craftingdead.block.tileentity.TileEntityLoot;
import com.craftingdead.entity.EntityGroundItem;
import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.ItemAmmo;
import com.craftingdead.item.gun.ItemGun;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.Random;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockLoot extends BlockContainer {

   private LootType a;


   protected BlockLoot(int var1, LootType var2) {
      super(var1, Material.rock);
      this.a = var2;
      this.setBlockUnbreakable();
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
      this.setCreativeTab(ItemManager.a);
   }

   public boolean onBlockActivated(World var1, int var2, int var3, int var4, EntityPlayer var5, int var6, float var7, float var8, float var9) {
      TileEntityLoot var10 = (TileEntityLoot)var1.getBlockTileEntity(var2, var3, var4);
      if(var10 != null) {
         if(var10.a() != null && !var1.isRemote && !var10.c) {
            ItemStack var11 = var10.a();
            EntityGroundItem var12 = new EntityGroundItem(var1, (double)var2 + 0.5D, (double)var3 + 0.5D, (double)var4 + 0.5D);
            var12.a(var11);
            var1.spawnEntityInWorld(var12);
            var10.c = true;
            Random var13 = new Random();
            var1.playSoundAtEntity(var5, "random.pop", 0.2F, ((var13.nextFloat() - var13.nextFloat()) * 0.7F + 1.0F) * 2.0F);
            if(var11.getItem() instanceof ItemGun && var13.nextInt(8) == 0) {
               ItemAmmo var14 = (ItemAmmo)Item.itemsList[((ItemGun)var11.getItem()).i()];
               if(var14 != null) {
                  ItemStack var15 = new ItemStack(var14);
                  EntityGroundItem var16 = new EntityGroundItem(var1, (double)var2, (double)var3, (double)var4);
                  var16.a(var15);
                  var1.spawnEntityInWorld(var16);
               }
            }
         }

         var1.removeBlockTileEntity(var2, var3, var4);
         var1.setBlock(var2, var3, var4, 0);
         return true;
      } else {
         return false;
      }
   }

   public int getRenderType() {
      return BlockManager.a;
   }

   public LootType e() {
      return this.a;
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.25F, 1.0F);
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityLoot(this.a);
   }
}
