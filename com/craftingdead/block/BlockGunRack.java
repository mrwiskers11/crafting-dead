package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityGunRack;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockGunRack extends BlockContainer {

   public BlockGunRack(int var1) {
      super(var1, Material.rock);
      this.setHardness(2.0F);
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 2.5F, 2.0F);
      this.setCreativeTab(ItemManager.a);
   }

   public int getRenderType() {
      return BlockManager.f;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onBlockPlacedBy(World var1, int var2, int var3, int var4, EntityLivingBase var5, ItemStack var6) {
      if(var5 != null) {
         TileEntityGunRack var7 = (TileEntityGunRack)var1.getBlockTileEntity(var2, var3, var4);
         var7.a = MathHelper.floor_double((double)(var5.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
      }
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      TileEntityGunRack var5 = (TileEntityGunRack)var1.getBlockTileEntity(var2, var3, var4);
      if(var5 != null) {
         this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 2.5F, 2.0F);
      }

   }

   public void addCollisionBoxesToList(World var1, int var2, int var3, int var4, AxisAlignedBB var5, List var6, Entity var7) {
      this.setBlockBoundsBasedOnState(var1, var2, var3, var4);
      super.addCollisionBoxesToList(var1, var2, var3, var4, var5, var6, var7);
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityGunRack();
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }
}
