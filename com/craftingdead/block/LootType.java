package com.craftingdead.block;

import java.util.ArrayList;
import java.util.Random;

import com.craftingdead.block.LootContent;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class LootType {

   private String a;
   private String b;
   private double c;
   private ArrayList d = new ArrayList();


   public LootType(String var1, String var2, double var3) {
      this.a = var1;
      this.b = var2;
      this.c = var3;
   }

   public String a() {
      return this.a;
   }

   public String b() {
      return this.b;
   }

   public boolean c() {
      Random var1 = new Random();
      return (double)var1.nextInt(101) <= this.c;
   }

   public LootType a(Item var1, double var2) {
      this.d.add(new LootContent(var1.itemID, var2));
      return this;
   }

   public void a(int var1, double var2) {
      this.d.add(new LootContent(var1, var2));
   }

   public double a(ItemStack var1) {
      int var2 = 0;
      ArrayList var3 = this.e();

      for(int var4 = 0; var4 < var3.size(); ++var4) {
         if(((ItemStack)var3.get(var4)).itemID == var1.itemID) {
            ++var2;
         }
      }

      if(var2 == 0) {
         return 0.0D;
      } else {
         double var8 = (double)(var2 * 100 / (var3.size() / 4));
         double var6 = var8 == 0.0D?0.1D:var8;
         return var6;
      }
   }

   public ArrayList d() {
      ArrayList var1 = new ArrayList();

      for(int var2 = 0; var2 < this.d.size(); ++var2) {
         LootContent var3 = (LootContent)this.d.get(var2);
         var1.add(var3.a());
      }

      return var1;
   }

   public ArrayList e() {
      ArrayList var1 = new ArrayList();

      for(int var2 = 0; var2 < this.d.size(); ++var2) {
         LootContent var3 = (LootContent)this.d.get(var2);

         for(int var4 = 0; (double)var4 < var3.b; ++var4) {
            var1.add(var3.a());
         }
      }

      return var1;
   }

   public ItemStack f() {
      Random var1 = new Random();
      ArrayList var2 = new ArrayList();

      for(int var3 = 0; var3 < this.d.size(); ++var3) {
         LootContent var4 = (LootContent)this.d.get(var3);

         for(int var5 = 0; (double)var5 < var4.b; ++var5) {
            var2.add(var4.a());
         }
      }

      return (ItemStack)var2.get(var1.nextInt(var2.size()));
   }
}
