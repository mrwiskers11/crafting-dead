package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityTrashCan;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockTrash extends BlockContainer {

   public BlockTrash(int var1) {
      super(var1, Material.rock);
      this.setHardness(2.0F);
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.8F, 1.0F);
      this.setCreativeTab(ItemManager.f);
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(0.1F, 0.1F, 0.1F, 0.9F, 0.8F, 0.9F);
   }

   public int getRenderType() {
      return BlockManager.o;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onBlockPlacedBy(World var1, int var2, int var3, int var4, EntityLivingBase var5, ItemStack var6) {
      if(var5 != null) {
         TileEntityTrashCan var7 = (TileEntityTrashCan)var1.getBlockTileEntity(var2, var3, var4);
         var7.a = MathHelper.floor_double((double)(var5.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
      }
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityTrashCan();
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }
}
