package com.craftingdead.block;

import com.craftingdead.block.BlockBarbedWire;
import com.craftingdead.block.BlockBarrier;
import com.craftingdead.block.BlockBarrier1;
import com.craftingdead.block.BlockBarrier2;
import com.craftingdead.block.BlockBarrier3;
import com.craftingdead.block.BlockBaseCenter;
import com.craftingdead.block.BlockCD;
import com.craftingdead.block.BlockCampfire;
import com.craftingdead.block.BlockClaySlab;
import com.craftingdead.block.BlockEmptyShelf;
import com.craftingdead.block.BlockForge;
import com.craftingdead.block.BlockLoot;
import com.craftingdead.block.BlockLootSpawner;
import com.craftingdead.block.BlockMilitarySign;
import com.craftingdead.block.BlockRoad;
import com.craftingdead.block.BlockRoadBlock;
import com.craftingdead.block.BlockSandBarrier;
import com.craftingdead.block.BlockShelfLoot;
import com.craftingdead.block.BlockStopSign;
import com.craftingdead.block.BlockTrafficCone;
import com.craftingdead.block.BlockTrafficPole;
import com.craftingdead.block.BlockTrash;
import com.craftingdead.block.BlockVendingMachine;
import com.craftingdead.block.BlockWaterPump;
import com.craftingdead.block.tileentity.TileEntityBarrier1;
import com.craftingdead.block.tileentity.TileEntityBarrier2;
import com.craftingdead.block.tileentity.TileEntityBarrier3;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.block.tileentity.TileEntityCampfire;
import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.block.tileentity.TileEntityGunRack;
import com.craftingdead.block.tileentity.TileEntityLoot;
import com.craftingdead.block.tileentity.TileEntityLootSpawner;
import com.craftingdead.block.tileentity.TileEntityMilitarySign;
import com.craftingdead.block.tileentity.TileEntityRoadBlock;
import com.craftingdead.block.tileentity.TileEntitySandBarrier;
import com.craftingdead.block.tileentity.TileEntityShelfEmpty;
import com.craftingdead.block.tileentity.TileEntityShelfLoot;
import com.craftingdead.block.tileentity.TileEntityStopSign;
import com.craftingdead.block.tileentity.TileEntityTrafficCone;
import com.craftingdead.block.tileentity.TileEntityTrafficPole;
import com.craftingdead.block.tileentity.TileEntityTrashCan;
import com.craftingdead.block.tileentity.TileEntityVendingMachine;
import com.craftingdead.block.tileentity.TileEntityWaterPump;
import com.craftingdead.item.ItemCDBlockAntiBlueprint;
import com.craftingdead.item.ItemCDBlockLoot;
import com.craftingdead.item.LootManager;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemSlab;
import net.minecraft.item.ItemStack;

public class BlockManager {

   public static int a;
   public static int b;
   public static int c;
   public static int d;
   public static int e;
   public static int f;
   public static int g;
   public static int h;
   public static int i;
   public static int j;
   public static int k;
   public static int l;
   public static int m;
   public static int n;
   public static int o;
   public static int p;
   public static int q;
   public static int r;
   public static int s;
   public static final int t = 7;
   public static final int u = 35;
   public static final int v = 7;
   public static final int w = 100;
   public static final int[] x = new int[]{0, 1, 2, 3, 31, 12, 24, 1225, 80};
   public static final int[] y = new int[]{0, 31, 78};
   public static final int[] z = new int[]{5, 98, 64, 54, 1225, 1226, 1227, 1228, 1244, 1231};
   public static final int[] A = new int[]{1004, 1005, 1006, 1007, 1012, 1013, 1015, 1026, 1030};
   public static final int[] B = new int[]{1210, 1210, 1211, 1211, 1214, 1212, 1213, 1251, 1250};
   public static Block C;
   public static Block D;
   public static Block E;
   public static Block F;
   public static Block G;
   public static Block H;
   public static Block I;
   public static Block J;
   public static Block K;
   public static Block L;
   public static Block M;
   public static Block N;
   public static Block O;
   public static Block P;
   public static Block Q;
   public static Block R;
   public static Block S;
   public static Block T;
   public static Block U;
   public static Block V;
   public static Block W;
   public static Block X;
   public static Block Y;
   public static Block Z;
   public static Block aa;
   public static Block ab;
   public static Block ac;
   public static Block ad;
   public static Block ae;
   public static Block af;
   public static Block ag;
   public static Block ah;
   public static Block ai;
   public static Block aj;
   public static Block ak;
   public static Block al;
   public static Block am;
   public static Block an;
   public static Block ao;
   public static Block ap;
   public static Block aq;
   public static Block ar;
   public static BlockHalfSlab as;
   public static BlockHalfSlab at;
   public static BlockHalfSlab au;
   public static BlockHalfSlab av;
   public static Block aw;
   public static Block ax;
   public static Block ay;
   public static Block az;
   public static Block aA;


   public void a() {
      aj = (new BlockCD(1004)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("commonLootSpawner");
      ak = (new BlockCD(1005)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("rareLootSpawner");
      al = (new BlockCD(1006)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("veryRareLootSpawner");
      am = (new BlockCD(1007)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("extremeLootSpawner");
      an = (new BlockCD(1012)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("militaryLootSpawner");
      ao = (new BlockCD(1013)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("medicalLootSpawner");
      ap = (new BlockCD(1015)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("policeLootSpawner");
      aq = (new BlockCD(1026)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("sandbag");
      ar = (new BlockCD(1030)).setCreativeTab((CreativeTabs)null).setUnlocalizedName("whitestonebrick");
      D = (new BlockLoot(1200, LootManager.a)).setUnlocalizedName("loot.residential");
      E = (new BlockLoot(1201, LootManager.b)).setUnlocalizedName("loot.residentialrare");
      F = (new BlockLoot(1202, LootManager.c)).setUnlocalizedName("loot.medical");
      G = (new BlockLoot(1203, LootManager.e)).setUnlocalizedName("loot.police");
      H = (new BlockLoot(1204, LootManager.d)).setUnlocalizedName("loot.military");
      I = (new BlockLootSpawner(1210, "residential", D.blockID)).setUnlocalizedName("loot.residential.spawner");
      J = (new BlockLootSpawner(1211, "residentialrare", E.blockID)).setUnlocalizedName("loot.residentialrare.spawner");
      K = (new BlockLootSpawner(1212, "medical", F.blockID)).setUnlocalizedName("loot.medical.spawner");
      L = (new BlockLootSpawner(1213, "police", G.blockID)).setUnlocalizedName("loot.police.spawner");
      M = (new BlockLootSpawner(1214, "military", H.blockID)).setUnlocalizedName("loot.military.spawner");
      N = (new BlockBaseCenter(1220)).setUnlocalizedName("base.center");
      O = (new BlockCD(1221)).a("antibase").setUnlocalizedName("base.antiplacer");
      aw = (new BlockCampfire(1225)).setHardness(2.0F).setUnlocalizedName("campfire");
      ax = (new BlockBarbedWire(1226, 1)).a("barbedwire").setHardness(2.0F).setUnlocalizedName("barbedWire");
      ay = (new BlockBarbedWire(1227, 3)).a("barbedwire2").setHardness(2.0F).setUnlocalizedName("barbedWireInforced");
      az = (new BlockSandBarrier(1228)).setHardness(2.0F).setUnlocalizedName("sandbarrier");
      aA = (new BlockCD(1231)).a("reinforcedconcreate").setHardness(2.0F).setUnlocalizedName("reinforcedconcreate");
      S = (new BlockWaterPump(1229)).setHardness(2.0F).setUnlocalizedName("waterpump");
      T = (new BlockMilitarySign(1230, new String[]{"Airfield", "", "CAUTION"}, "0x005700")).setHardness(2.0F).setUnlocalizedName("signairfield");
      U = (new BlockMilitarySign(1232, new String[]{"Shop", "Safe Zone"}, "0x0087ff")).setHardness(2.0F).setUnlocalizedName("signshop");
      V = (new BlockBarrier1(1233)).setHardness(2.0F).setUnlocalizedName("barrier1");
      W = (new BlockBarrier2(1234)).setHardness(2.0F).setUnlocalizedName("barrier2");
      X = (new BlockBarrier3(1235)).setHardness(2.0F).setUnlocalizedName("barrier3");
      Y = (new BlockTrafficPole(1236)).setHardness(2.0F).setUnlocalizedName("trafficpole");
      Z = (new BlockTrafficCone(1237)).setHardness(2.0F).setUnlocalizedName("trafficcone");
      aa = (new BlockStopSign(1239)).setHardness(2.0F).setUnlocalizedName("stopsign");
      ab = (new BlockVendingMachine(1240)).setHardness(2.0F).setUnlocalizedName("vendingmachine");
      ac = (new BlockRoadBlock(1241)).setHardness(2.0F).setUnlocalizedName("roadblock");
      ad = (new BlockEmptyShelf(1242)).setHardness(2.0F).setUnlocalizedName("emptyshelf");
      ae = (new BlockShelfLoot(1243)).setHardness(2.0F).setUnlocalizedName("shelf");
      af = (new BlockForge(1244)).setHardness(2.0F).setUnlocalizedName("forge");
      ag = (new BlockCD(1250)).a("stonewhite").setHardness(2.0F).setUnlocalizedName("cosmetic.whitestone");
      ah = (new BlockCD(1251)).a("sandbag").setHardness(2.0F).setUnlocalizedName("cosmetic.sandbag");
      ai = (new BlockTrash(1252)).setUnlocalizedName("cosmetic.trash");
      as = (BlockHalfSlab)(new BlockClaySlab(1050, true, true)).setUnlocalizedName("stainedclaydoubleslab");
      at = (BlockHalfSlab)(new BlockClaySlab(1051, false, true)).setUnlocalizedName("stainedclayslab");
      au = (BlockHalfSlab)(new BlockClaySlab(1052, true, false)).e().setUnlocalizedName("stainedclaydoubleslab2");
      av = (BlockHalfSlab)(new BlockClaySlab(1053, false, false)).e().setUnlocalizedName("stainedclayslab2");
      Item.itemsList[at.blockID] = (new ItemSlab(at.blockID - 256, at, as, false)).setUnlocalizedName("stainedClaySlab");
      Item.itemsList[as.blockID] = (new ItemSlab(as.blockID - 256, at, as, true)).setUnlocalizedName("stainedClayDoubleSlab");
      Item.itemsList[av.blockID] = (new ItemSlab(av.blockID - 256, av, au, false)).setUnlocalizedName("stainedClaySlab2");
      Item.itemsList[au.blockID] = (new ItemSlab(au.blockID - 256, av, au, true)).setUnlocalizedName("stainedClayDoubleSlab2");
      P = (new BlockCD(1040)).a("road").setHardness(2.0F).setUnlocalizedName("road");
      Q = (new BlockRoad(1041, "broken")).setHardness(2.0F).setUnlocalizedName("road.lined.broken");
      R = (new BlockRoad(1042, "solid")).setHardness(2.0F).setUnlocalizedName("road.lined");
      C = new BlockBarrier(1043);
   }

   public void b() {
      GameRegistry.registerBlock(aj, "cd.commonLootSpawner");
      GameRegistry.registerBlock(ak, "cd.rareLootSpawner");
      GameRegistry.registerBlock(al, "cd.veryRareLootSpawner");
      GameRegistry.registerBlock(am, "cd.extremeLootSpawner");
      GameRegistry.registerBlock(an, "cd.militaryLootSpawner");
      GameRegistry.registerBlock(ao, "cd.medicalLootSpawner");
      GameRegistry.registerBlock(ap, "cd.policeLootSpawner");
      GameRegistry.registerBlock(aq, "cd.sandbag");
      GameRegistry.registerBlock(ar, "cd.whitestone");
      GameRegistry.registerBlock(D, ItemCDBlockLoot.class, "cda.block.loot.residential");
      GameRegistry.registerBlock(E, ItemCDBlockLoot.class, "cda.block.loot.residentialrare");
      GameRegistry.registerBlock(F, ItemCDBlockLoot.class, "cda.block.loot.medical");
      GameRegistry.registerBlock(G, ItemCDBlockLoot.class, "cda.block.loot.police");
      GameRegistry.registerBlock(H, ItemCDBlockLoot.class, "cda.block.loot.military");
      GameRegistry.registerBlock(I, "cda.block.loot.residential.spawner");
      GameRegistry.registerBlock(J, "cda.block.loot.residentialrare.spawner");
      GameRegistry.registerBlock(K, "cda.block.loot.medical.spawner");
      GameRegistry.registerBlock(L, "cda.block.loot.police.spawner");
      GameRegistry.registerBlock(M, "cda.block.loot.military.spawner");
      GameRegistry.registerBlock(N, "cda.block.base.center");
      GameRegistry.registerBlock(O, ItemCDBlockAntiBlueprint.class, "cda.block.base.antiplacer");
      GameRegistry.registerBlock(aw, "cda.block.campfire");
      GameRegistry.registerBlock(ax, "cda.block.barbedwire");
      GameRegistry.registerBlock(ay, "cda.block.barbedwireinforced");
      GameRegistry.registerBlock(az, "cda.block.sandbarrier");
      GameRegistry.registerBlock(aA, "cda.block.reinforcedconcreate");
      GameRegistry.registerBlock(S, "cda.block.waterpump");
      GameRegistry.registerBlock(T, "cda.block.sign.airfield");
      GameRegistry.registerBlock(U, "cda.block.sign.shop");
      GameRegistry.registerBlock(V, "cda.block.barrier.one");
      GameRegistry.registerBlock(W, "cda.block.barrier.two");
      GameRegistry.registerBlock(X, "cda.block.barrier.three");
      GameRegistry.registerBlock(Y, "cda.block.traffic.pole");
      GameRegistry.registerBlock(Z, "cda.block.traffic.cone");
      GameRegistry.registerBlock(aa, "cda.block.stopsign");
      GameRegistry.registerBlock(ab, "cda.block.vendingmachine");
      GameRegistry.registerBlock(ac, "cda.block.roadblock");
      GameRegistry.registerBlock(ae, "cda.block.shelf");
      GameRegistry.registerBlock(ad, "cda.block.emptyshelf");
      GameRegistry.registerBlock(af, "cda.block.forge");
      GameRegistry.registerBlock(ag, "cda.block.cosmetic.whitestone");
      GameRegistry.registerBlock(ah, "cda.block.cosmetic.sandbag");
      GameRegistry.registerBlock(ai, "cda.block.cosmetic.trash");
      GameRegistry.registerBlock(P, "cda.block.road");
      GameRegistry.registerBlock(Q, "cda.block.road.broken");
      GameRegistry.registerBlock(R, "cda.block.road.solid");
      GameRegistry.registerBlock(C, "cda.block.barrier");
      GameRegistry.registerTileEntity(TileEntityLoot.class, "loot");
      GameRegistry.registerTileEntity(TileEntityLootSpawner.class, "lootspawner");
      GameRegistry.registerTileEntity(TileEntityBaseCenter.class, "basecenter");
      GameRegistry.registerTileEntity(TileEntityCampfire.class, "campfire");
      GameRegistry.registerTileEntity(TileEntitySandBarrier.class, "sandbarrier");
      GameRegistry.registerTileEntity(TileEntityWaterPump.class, "waterpump");
      GameRegistry.registerTileEntity(TileEntityGunRack.class, "gunrack");
      GameRegistry.registerTileEntity(TileEntityMilitarySign.class, "militarysign");
      GameRegistry.registerTileEntity(TileEntityBarrier1.class, "barrier1");
      GameRegistry.registerTileEntity(TileEntityBarrier2.class, "barrier2");
      GameRegistry.registerTileEntity(TileEntityBarrier3.class, "barrier3");
      GameRegistry.registerTileEntity(TileEntityTrafficPole.class, "trafficpole");
      GameRegistry.registerTileEntity(TileEntityTrafficCone.class, "trafficcone");
      GameRegistry.registerTileEntity(TileEntityStopSign.class, "stopsign");
      GameRegistry.registerTileEntity(TileEntityTrashCan.class, "trash");
      GameRegistry.registerTileEntity(TileEntityVendingMachine.class, "vendingmachine");
      GameRegistry.registerTileEntity(TileEntityRoadBlock.class, "roadblock");
      GameRegistry.registerTileEntity(TileEntityShelfEmpty.class, "emptyshelf");
      GameRegistry.registerTileEntity(TileEntityShelfLoot.class, "shelf");
      GameRegistry.registerTileEntity(TileEntityForge.class, "forge");
   }

   public void c() {
      LanguageRegistry.addName(aj, "OLD Common Loot Spawner");
      LanguageRegistry.addName(ak, "OLD Rare Loot Spawner");
      LanguageRegistry.addName(al, "OLD Very Rare Loot Spawner");
      LanguageRegistry.addName(am, "OLD Extremely Rare Loot Spawner");
      LanguageRegistry.addName(an, "Military Loot Spawner");
      LanguageRegistry.addName(ao, "Medical Loot Spawner");
      LanguageRegistry.addName(ap, "Police Loot Spawner");
      LanguageRegistry.addName(D, "Residential Loot");
      LanguageRegistry.addName(E, "Rare Residential Loot");
      LanguageRegistry.addName(F, "Medical Loot");
      LanguageRegistry.addName(G, "Police Loot");
      LanguageRegistry.addName(H, "Military Loot");
      LanguageRegistry.addName(I, "Residential Loot Spawner");
      LanguageRegistry.addName(J, "Rare Residential Loot Spawner");
      LanguageRegistry.addName(K, "Medical Loot Spawner");
      LanguageRegistry.addName(L, "Police Loot Spawner");
      LanguageRegistry.addName(M, "Military Loot Spawner");
      LanguageRegistry.addName(N, "Base Center");
      LanguageRegistry.addName(O, "Anti-Base Spawner");
      LanguageRegistry.addName(aw, "Campfire");
      LanguageRegistry.addName(ax, "Barbed Wire");
      LanguageRegistry.addName(ay, "Reinforced Barbed Wire");
      LanguageRegistry.addName(az, "Sand Barrier");
      LanguageRegistry.addName(aA, "Reinforced Concreate");
      LanguageRegistry.addName(S, "Water Pump");
      LanguageRegistry.addName(T, "Military Airfield Sign");
      LanguageRegistry.addName(U, "Server Shop Sign");
      LanguageRegistry.addName(V, "Concrete Barrier 1");
      LanguageRegistry.addName(W, "Concrete Barrier 2");
      LanguageRegistry.addName(X, "Concrete Barrier 3");
      LanguageRegistry.addName(Y, "Traffic Pole");
      LanguageRegistry.addName(Z, "Traffic Cone");
      LanguageRegistry.addName(aa, "Stop Sign");
      LanguageRegistry.addName(ab, "Vending Machine");
      LanguageRegistry.addName(ac, "Road Block");
      LanguageRegistry.addName(ad, "Empty Shelf");
      LanguageRegistry.addName(ae, "Lootable Shelf");
      LanguageRegistry.addName(af, "Forge Table");
      LanguageRegistry.addName(ag, "White Stone Brick");
      LanguageRegistry.addName(ah, "Sand Bag");
      LanguageRegistry.addName(ai, "Pile of Trash");
      LanguageRegistry.addName(P, "Road");
      LanguageRegistry.addName(Q, "Broken Lined Road");
      LanguageRegistry.addName(R, "Solid Lined Road");
      LanguageRegistry.addName(C, "Barrier");

      for(int var1 = 0; var1 < 16; ++var1) {
         String var2 = ItemDye.dyeColorNames[~var1 & 15].toUpperCase();
         if(var1 < 7) {
            LanguageRegistry.addName(new ItemStack(at.blockID, 1, var1), var2 + " Stained Clay Slab");
         } else {
            LanguageRegistry.addName(new ItemStack(av.blockID, 1, var1 - 7), var2 + " Stained Clay Slab");
         }
      }

   }

   public static int a(int var0) {
      for(int var1 = 0; var1 < A.length; ++var1) {
         if(A[var1] == var0) {
            return B[var1];
         }
      }

      return -1;
   }

   public void d() {
      this.a();
      this.b();
      this.c();
   }

}
