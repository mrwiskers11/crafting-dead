package com.craftingdead.block.tileentity;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMilitarySign extends TileEntity {

   public int a;
   public boolean b = false;
   public String[] c = new String[]{"Hello!", "CAUTION"};
   public String d = "0xffffff";


   public TileEntityMilitarySign() {}

   public TileEntityMilitarySign(String[] var1, String var2) {
      this.c = var1;
      this.d = var2;
   }

   @SideOnly(Side.CLIENT)
   public double getMaxRenderDistanceSquared() {
      return 30000.0D;
   }

   public void updateEntity() {}

   public Packet getDescriptionPacket() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      return new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1);
   }

   public void onDataPacket(INetworkManager var1, Packet132TileEntityData var2) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
         this.a = var2.data.getInteger("dir");
      }

   }

   public boolean receiveClientEvent(int var1, int var2) {
      return true;
   }

   public TileEntityMilitarySign a() {
      this.b = true;
      return this;
   }

   public void readFromNBT(NBTTagCompound var1) {
      super.readFromNBT(var1);
      this.a = var1.getInteger("dir");
   }

   public void writeToNBT(NBTTagCompound var1) {
      super.writeToNBT(var1);
      var1.setInteger("dir", this.a);
   }
}
