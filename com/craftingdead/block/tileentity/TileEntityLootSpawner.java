package com.craftingdead.block.tileentity;

import com.craftingdead.block.BlockLoot;
import com.craftingdead.block.BlockLootSpawner;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class TileEntityLootSpawner extends TileEntity {

   public int a = 0;
   public int b = 0;


   public Packet getDescriptionPacket() {
      this.a();
      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      return new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1);
   }

   public void a() {
      if(super.worldObj != null) {
         byte var1 = 1;
         boolean var2 = false;
         int var3 = super.xCoord;
         int var4 = super.yCoord;
         int var5 = super.zCoord;
         int var6 = super.worldObj.getBlockId(var3, var4, var5);

         for(int var7 = -var1; var7 <= var1; ++var7) {
            for(int var8 = -var1; var8 <= var1; ++var8) {
               int var9 = super.worldObj.getBlockId(var3 + var7, var4, var5 + var8);
               if(var9 != 0) {
                  Block var10 = Block.blocksList[var9];
                  if(var10 != null && !(var10 instanceof BlockLootSpawner) && !(var10 instanceof BlockLoot) && var9 != 0 && !var2) {
                     int var11 = super.worldObj.getBlockMetadata(var3 + var7, var4, var5 + var8);
                     this.a = var9;
                     this.b = var11;
                     var2 = true;
                  }
               }
            }
         }

         if(!var2) {
            this.a = var6;
            this.b = 0;
         }
      }

   }

   public void onDataPacket(INetworkManager var1, Packet132TileEntityData var2) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
         this.a = var2.data.getInteger("blockid");
         this.b = var2.data.getInteger("blockmeta");
      }

   }

   public boolean receiveClientEvent(int var1, int var2) {
      return true;
   }

   public void readFromNBT(NBTTagCompound var1) {
      super.readFromNBT(var1);
      if(var1.hasKey("blockid")) {
         this.a = var1.getInteger("blockid");
      }

      if(var1.hasKey("blockmeta")) {
         this.b = var1.getInteger("blockmeta");
      }

   }

   public void writeToNBT(NBTTagCompound var1) {
      super.writeToNBT(var1);
      var1.setInteger("blockid", this.a);
      var1.setInteger("blockmeta", this.b);
   }
}
