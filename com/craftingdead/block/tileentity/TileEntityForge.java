package com.craftingdead.block.tileentity;

import com.craftingdead.item.ItemManager;
import com.craftingdead.item.gun.ItemAmmo;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class TileEntityForge extends TileEntity implements ISidedInventory {

   private static final int[] e = new int[]{0};
   private static final int[] f = new int[]{2, 1};
   private static final int[] g = new int[]{1};
   private ItemStack[] h = new ItemStack[4];
   public int a;
   public static final int b = 300;
   public int c = 0;
   public boolean d = false;
   private String i;


   public TileEntityForge a() {
      this.d = true;
      return this;
   }

   @SideOnly(Side.CLIENT)
   public double getMaxRenderDistanceSquared() {
      return 30000.0D;
   }

   public void updateEntity() {
      ItemStack var1 = this.getStackInSlot(0);
      ItemStack var2 = this.getStackInSlot(1);
      ItemStack var3 = this.getStackInSlot(2);
      ItemStack var4 = this.getStackInSlot(3);
      if(this.c < 300) {
         if(var3 != null && var2 != null && var1 != null && var4 == null) {
            ++this.c;
         } else {
            this.c = 0;
         }
      } else {
         this.a(super.worldObj, var3, var2, var1, var4);
         this.c = 0;
      }

   }

   public int f() {
      return Math.round((float)(this.c * 100 / 300));
   }

   public int j() {
      boolean var1 = false;
      boolean var2 = false;
      int var3 = 300 - this.c;
      int var4 = 300 - var3;
      return var4;
   }

   public int getSizeInventory() {
      return this.h.length;
   }

   public ItemStack getStackInSlot(int var1) {
      return this.h[var1];
   }

   public ItemStack decrStackSize(int var1, int var2) {
      if(this.h[var1] != null) {
         ItemStack var3;
         if(this.h[var1].stackSize <= var2) {
            var3 = this.h[var1];
            this.h[var1] = null;
            return var3;
         } else {
            var3 = this.h[var1].splitStack(var2);
            if(this.h[var1].stackSize == 0) {
               this.h[var1] = null;
            }

            return var3;
         }
      } else {
         return null;
      }
   }

   public ItemStack getStackInSlotOnClosing(int var1) {
      if(this.h[var1] != null) {
         ItemStack var2 = this.h[var1];
         this.h[var1] = null;
         return var2;
      } else {
         return null;
      }
   }

   public void setInventorySlotContents(int var1, ItemStack var2) {
      this.h[var1] = var2;
      if(var2 != null && var2.stackSize > this.getInventoryStackLimit()) {
         var2.stackSize = this.getInventoryStackLimit();
      }

   }

   public String getInvName() {
      return this.isInvNameLocalized()?this.i:"Forge";
   }

   public boolean isInvNameLocalized() {
      return this.i != null && this.i.length() > 0;
   }

   public void a(String var1) {
      this.i = var1;
   }

   public void readFromNBT(NBTTagCompound var1) {
      super.readFromNBT(var1);
      NBTTagList var2 = var1.getTagList("Items");
      this.h = new ItemStack[this.getSizeInventory()];

      for(int var3 = 0; var3 < var2.tagCount(); ++var3) {
         NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
         byte var5 = var4.getByte("Slot");
         if(var5 >= 0 && var5 < this.h.length) {
            this.h[var5] = ItemStack.loadItemStackFromNBT(var4);
         }
      }

      if(var1.hasKey("CustomName")) {
         this.i = var1.getString("CustomName");
      }

   }

   public void writeToNBT(NBTTagCompound var1) {
      super.writeToNBT(var1);
      NBTTagList var2 = new NBTTagList();

      for(int var3 = 0; var3 < this.h.length; ++var3) {
         if(this.h[var3] != null) {
            NBTTagCompound var4 = new NBTTagCompound();
            var4.setByte("Slot", (byte)var3);
            this.h[var3].writeToNBT(var4);
            var2.appendTag(var4);
         }
      }

      var1.setTag("Items", var2);
      if(this.isInvNameLocalized()) {
         var1.setString("CustomName", this.i);
      }

   }

   public int getInventoryStackLimit() {
      return 64;
   }

   public boolean isUseableByPlayer(EntityPlayer var1) {
      return super.worldObj.getBlockTileEntity(super.xCoord, super.yCoord, super.zCoord) != this?false:var1.getDistanceSq((double)super.xCoord + 0.5D, (double)super.yCoord + 0.5D, (double)super.zCoord + 0.5D) <= 64.0D;
   }

   public void openChest() {}

   public void closeChest() {}

   public boolean isItemValidForSlot(int var1, ItemStack var2) {
      return true;
   }

   public int[] getAccessibleSlotsFromSide(int var1) {
      return var1 == 0?f:(var1 == 1?e:g);
   }

   public boolean canInsertItem(int var1, ItemStack var2, int var3) {
      return true;
   }

   public boolean canExtractItem(int var1, ItemStack var2, int var3) {
      return true;
   }

   public void a(ItemStack var1, ItemStack var2, ItemStack var3, ItemStack var4) {
      if(var1 != null && var2 != null && var3 != null && var4 == null) {
         ItemStack var5;
         ItemAmmo var6;
         if(var2.getItem() == ItemManager.eR) {
            this.setInventorySlotContents(3, var4 == null?new ItemStack(var1.getItem(), 1):new ItemStack(var4.getItem(), var4.stackSize + 1));
            var5 = this.getStackInSlot(3);
            var5.setItemDamage(var1.getItemDamage());
            var6 = (ItemAmmo)var5.getItem();
            var6.b(var5, true);
         }

         if(var2.getItem() == ItemManager.gU || var2.getItem() == ItemManager.gT) {
            this.setInventorySlotContents(3, var4 == null?new ItemStack(var1.getItem(), 1):new ItemStack(var4.getItem(), var4.stackSize + 1));
            var5 = this.getStackInSlot(3);
            var5.setItemDamage(var1.getItemDamage());
            var6 = (ItemAmmo)var5.getItem();
            var6.c(var5, true);
         }

         if(var2.getItem() == ItemManager.eZ) {
            this.setInventorySlotContents(3, var4 == null?new ItemStack(var1.getItem(), 1):new ItemStack(var4.getItem(), var4.stackSize + 1));
            var5 = this.getStackInSlot(3);
            var5.setItemDamage(var1.getItemDamage());
            var6 = (ItemAmmo)var5.getItem();
            var6.a(var5, true);
         }

         this.setInventorySlotContents(0, var3.stackSize == 1?null:new ItemStack(var3.getItem(), var3.stackSize - 1));
         this.setInventorySlotContents(1, var2.stackSize == 1?null:new ItemStack(var2.getItem(), var2.stackSize - 1));
         this.setInventorySlotContents(2, var1.stackSize == 1?null:new ItemStack(var1.getItem(), var1.stackSize - 1));
      }

   }

   public void a(World var1, ItemStack var2, ItemStack var3, ItemStack var4, ItemStack var5) {
      if(!var1.isRemote && var2 != null && var3 != null && var4 != null && var5 == null) {
         ItemAmmo var6 = (ItemAmmo)var2.getItem();
         if(var6.d >= 1) {
            this.a(var2, var3, var4, var5);
         }
      }

   }

}
