package com.craftingdead.block.tileentity;

import cpw.mods.fml.common.network.PacketDispatcher;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class TileEntityCampfire extends TileEntity {

   public void updateEntity() {
      if(super.worldObj.isRemote) {
         for(int var1 = 0; var1 < 1; ++var1) {
            super.worldObj.spawnParticle("flame", (double)super.xCoord, (double)super.yCoord + 0.5D, (double)super.yCoord, 0.0D, 0.0D, 0.0D);
         }
      }

   }

   public void a() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      PacketDispatcher.sendPacketToAllPlayers(new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1));
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      return new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1);
   }

   public void onDataPacket(INetworkManager var1, Packet132TileEntityData var2) {
      if(super.worldObj.isRemote) {
         ;
      }

   }

   public boolean receiveClientEvent(int var1, int var2) {
      return true;
   }

   public void readFromNBT(NBTTagCompound var1) {
      super.readFromNBT(var1);
   }

   public void writeToNBT(NBTTagCompound var1) {
      super.writeToNBT(var1);
   }
}
