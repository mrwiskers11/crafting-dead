package com.craftingdead.block.tileentity;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.util.Random;

import com.craftingdead.block.LootType;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

public class TileEntityLoot extends TileEntity {

   private ItemStack d;
   private LootType e;
   public boolean a = false;
   public int b = 0;
   private int f = 0;
   private int g = 120;
   private int h = 50;
   public boolean c = false;


   public TileEntityLoot() {
      Random var1 = new Random();
      this.b = 90 * var1.nextInt(4);
   }

   public TileEntityLoot(LootType var1) {
      this.e = var1;
      Random var2 = new Random();
      this.b = 90 * var2.nextInt(4);
   }

   public void updateEntity() {
      if(super.worldObj.getClosestPlayer((double)super.xCoord, (double)super.yCoord, (double)super.zCoord, (double)this.h) == null && this.f++ >= 20 * this.g) {
         if(!super.worldObj.isRemote) {
            super.worldObj.destroyBlock(super.xCoord, super.yCoord, super.zCoord, false);
            if(super.worldObj.getBlockTileEntity(super.xCoord, super.yCoord, super.zCoord) != null) {
               super.worldObj.removeBlockTileEntity(super.xCoord, super.yCoord, super.zCoord);
            }
         }

         this.f = 0;
      }

   }

   public ItemStack a() {
      return this.d;
   }

   public String b() {
      return this.e.a();
   }

   public Packet getDescriptionPacket() {
      if(super.worldObj != null && !super.worldObj.isRemote && this.d == null && this.e != null) {
         this.d = this.e.f();
      }

      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      return new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1);
   }

   public void onDataPacket(INetworkManager var1, Packet132TileEntityData var2) {
      if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
         this.d = ItemStack.loadItemStackFromNBT(var2.data.getCompoundTag("loot"));
      }

   }

   public boolean receiveClientEvent(int var1, int var2) {
      return true;
   }

   public void readFromNBT(NBTTagCompound var1) {
      super.readFromNBT(var1);
      if(var1.hasKey("loot")) {
         this.d = ItemStack.loadItemStackFromNBT(var1.getCompoundTag("loot"));
      }

   }

   public void writeToNBT(NBTTagCompound var1) {
      super.writeToNBT(var1);
      if(this.d != null) {
         NBTTagCompound var2 = new NBTTagCompound();
         this.d.writeToNBT(var2);
         var1.setCompoundTag("loot", var2);
      }

   }
}
