package com.craftingdead.block.tileentity;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.ArrayList;

import com.craftingdead.block.BlockBaseCenter;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;

public class TileEntityBaseCenter extends TileEntity {

   private String a;
   private ArrayList b = new ArrayList();
   private int c = 0;
   private int d = 0;


   public void updateEntity() {
      if(!super.worldObj.isRemote && this.d++ >= 2400) {
         if(this.c()) {
            this.c = 0;
         }

         if((this.a == null || this.a.equals("")) && !super.worldObj.isRemote) {
            BlockBaseCenter var1 = (BlockBaseCenter)this.getBlockType();
            var1.b(super.worldObj, super.xCoord, super.yCoord, super.zCoord, (EntityPlayer)null);
         }

         this.d = 0;
      }

   }

   @SideOnly(Side.CLIENT)
   public double getMaxRenderDistanceSquared() {
      return 30000.0D;
   }

   public int a() {
      return this.c / 1440;
   }

   public int b() {
      return this.c;
   }

   public void a(int var1) {
      this.c += var1;
      if(!super.worldObj.isRemote && this.a() >= 7) {
         BlockBaseCenter var2 = (BlockBaseCenter)this.getBlockType();
         var2.b(super.worldObj, super.xCoord, super.yCoord, super.zCoord, (EntityPlayer)null);
      }

   }

   public boolean c() {
      if(!super.worldObj.isRemote && !super.worldObj.isRemote) {
         try {
            MinecraftServer var1 = MinecraftServer.getServer();
            if(var1.getConfigurationManager().getPlayerForUsername(this.a) != null) {
               return true;
            }

            for(int var2 = 0; var2 < this.b.size(); ++var2) {
               if(var1.getConfigurationManager().getPlayerForUsername((String)this.b.get(var2)) != null) {
                  return true;
               }
            }
         } catch (Exception var3) {
            ;
         }
      }

      return false;
   }

   public boolean a(String var1) {
      return var1 != null && this.f() != null?this.f().equals(var1) || this.d(var1):false;
   }

   public void b(String var1) {
      if(!this.b.contains(var1)) {
         this.b.add(var1);
      }

   }

   public void c(String var1) {
      this.b.remove(var1);
   }

   public boolean d(String var1) {
      return this.b != null && this.b.size() > 0?this.b.contains(var1):false;
   }

   public ArrayList d() {
      return this.b;
   }

   public void e(String var1) {
      this.a = var1;
   }

   public String f() {
      return this.a;
   }

   public void g() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      PacketDispatcher.sendPacketToAllPlayers(new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1));
   }

   public Packet getDescriptionPacket() {
      NBTTagCompound var1 = new NBTTagCompound();
      this.writeToNBT(var1);
      return new Packet132TileEntityData(super.xCoord, super.yCoord, super.zCoord, 1, var1);
   }

   public void onDataPacket(INetworkManager var1, Packet132TileEntityData var2) {
      if(super.worldObj.isRemote && var2.data.hasKey("owner")) {
         this.a = var2.data.getString("owner");
         int var3 = var2.data.getInteger("members");
         this.b.clear();

         for(int var4 = 0; var4 < var3; ++var4) {
            String var5 = var2.data.getString("member" + var4);
            this.b.add(var5);
         }
      }

   }

   public boolean receiveClientEvent(int var1, int var2) {
      return true;
   }

   public void readFromNBT(NBTTagCompound var1) {
      super.readFromNBT(var1);
      if(var1.hasKey("owner")) {
         this.a = var1.getString("owner");
         this.c = var1.getInteger("timeIdled");
         int var2 = var1.getInteger("members");
         this.b.clear();

         for(int var3 = 0; var3 < var2; ++var3) {
            String var4 = var1.getString("member" + var3);
            this.b.add(var4);
         }
      }

   }

   public void writeToNBT(NBTTagCompound var1) {
      super.writeToNBT(var1);
      if(!super.worldObj.isRemote && this.a != null) {
         var1.setString("owner", this.a);
         var1.setInteger("members", this.b.size());
         var1.setInteger("timeIdled", this.c);

         for(int var2 = 0; var2 < this.b.size(); ++var2) {
            var1.setString("member" + var2, (String)this.b.get(var2));
         }
      }

   }
}
