package com.craftingdead.block;

import net.minecraft.item.ItemStack;

public class LootContent {

   public int a;
   public double b;


   public LootContent(int var1, double var2) {
      this.a = var1;
      this.b = (double)((int)var2 / 2);
   }

   public ItemStack a() {
      return new ItemStack(this.a, 1, 0);
   }
}
