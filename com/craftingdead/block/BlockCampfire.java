package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityCampfire;
import com.craftingdead.item.ItemCDPickaxe;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.Random;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockCampfire extends BlockContainer {

   public BlockCampfire(int var1) {
      super(var1, Material.wood);
      this.setLightValue(1.0F);
      this.setHardness(2.0F);
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
      this.setCreativeTab(ItemManager.a);
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
   }

   public int getRenderType() {
      return BlockManager.c;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public boolean removeBlockByPlayer(World var1, EntityPlayer var2, int var3, int var4, int var5) {
      return var2.capabilities.isCreativeMode?var1.setBlockToAir(var3, var4, var5):(var2.getCurrentEquippedItem() != null && var2.getCurrentEquippedItem().getItem() instanceof ItemCDPickaxe?var1.setBlockToAir(var3, var4, var5):false);
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityCampfire();
   }

   public void onEntityWalking(World var1, int var2, int var3, int var4, Entity var5) {
      if(!var1.isRemote) {
         var5.setFire(3);
      }

   }

   public void randomDisplayTick(World var1, int var2, int var3, int var4, Random var5) {
      int var6;
      double var7;
      double var9;
      double var11;
      for(var6 = 0; var6 < 2; ++var6) {
         var7 = (0.5D - var5.nextDouble()) / 2.0D;
         var9 = var5.nextDouble() / 2.0D;
         var11 = (0.5D - var5.nextDouble()) / 2.0D;
         var1.spawnParticle("flame", (double)var2 + 0.5D + var7, (double)var3 + 0.25D + var9, (double)var4 + 0.5D + var11, 0.0D, 0.0D, 0.0D);
      }

      for(var6 = 0; var6 < 4; ++var6) {
         var7 = (0.5D - var5.nextDouble()) / 2.0D;
         var9 = var5.nextDouble() / 2.0D;
         var11 = (0.5D - var5.nextDouble()) / 2.0D;
         var1.spawnParticle("smoke", (double)var2 + 0.5D + var7, (double)var3 + 0.5D + var9, (double)var4 + 0.5D + var11, 0.0D, 0.0D, 0.0D);
      }

   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }
}
