package com.craftingdead.block;

import com.craftingdead.block.BlockCD;
import com.craftingdead.item.ItemCDAxe;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

public class BlockBarbedWire extends BlockCD {

   public int a = 1;


   public BlockBarbedWire(int var1, int var2) {
      super(var1, Material.web);
      this.a = var2;
   }

   public void onEntityCollidedWithBlock(World var1, int var2, int var3, int var4, Entity var5) {
      var5.attackEntityFrom(DamageSource.cactus, (float)this.a);
      var5.setInWeb();
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public AxisAlignedBB getCollisionBoundingBoxFromPool(World var1, int var2, int var3, int var4) {
      return null;
   }

   public int getRenderType() {
      return 1;
   }

   public boolean removeBlockByPlayer(World var1, EntityPlayer var2, int var3, int var4, int var5) {
      return var2.capabilities.isCreativeMode?var1.setBlockToAir(var3, var4, var5):(var2.getCurrentEquippedItem() != null && var2.getCurrentEquippedItem().getItem() instanceof ItemCDAxe?var1.setBlockToAir(var3, var4, var5):false);
   }

   public boolean renderAsNormalBlock() {
      return false;
   }
}
