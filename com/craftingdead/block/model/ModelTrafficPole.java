package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTrafficPole extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;


   public ModelTrafficPole() {
      super.textureWidth = 512;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 8, 25);
      this.a.addBox(0.0F, 0.0F, 0.0F, 3, 2, 3);
      this.a.setRotationPoint(-1.5F, 22.0F, -1.5F);
      this.a.setTextureSize(512, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 10, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 2, 22, 2);
      this.b.setRotationPoint(-1.0F, 1.0F, -1.0F);
      this.b.setTextureSize(512, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
