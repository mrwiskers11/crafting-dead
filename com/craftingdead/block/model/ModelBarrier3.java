package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBarrier3 extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;


   public ModelBarrier3() {
      super.textureWidth = 64;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 7, 8);
      this.a.addBox(0.0F, 0.0F, 0.0F, 16, 14, 3);
      this.a.setRotationPoint(-8.0F, 10.0F, -1.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 9, 26);
      this.b.addBox(0.0F, -0.4F, -2.0F, 16, 4, 2);
      this.b.setRotationPoint(-8.0F, 19.0F, 2.0F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.4833219F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 9, 26);
      this.c.addBox(0.0F, 0.6F, -2.0F, 16, 4, 2);
      this.c.setRotationPoint(-8.0F, 19.0F, 1.1F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, -0.4833219F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 9, 28);
      this.d.addBox(0.0F, 0.0F, 0.0F, 16, 2, 2);
      this.d.setRotationPoint(-8.0F, 22.0F, 1.7F);
      this.d.setTextureSize(64, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 9, 28);
      this.e.addBox(0.0F, 0.0F, 0.0F, 16, 2, 2);
      this.e.setRotationPoint(-8.0F, 22.0F, -2.8F);
      this.e.setTextureSize(64, 32);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 11, 5);
      this.f.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.f.setRotationPoint(5.0F, 9.6F, 0.0F);
      this.f.setTextureSize(64, 32);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 35, 5);
      this.g.addBox(0.0F, 0.0F, 0.0F, 2, 1, 1);
      this.g.setRotationPoint(-7.0F, 9.6F, 0.0F);
      this.g.setTextureSize(64, 32);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
