package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelCampfire extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;


   public ModelCampfire() {
      super.textureWidth = 64;
      super.textureHeight = 32;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 3, 3, 6);
      this.a.setRotationPoint(5.0F, 21.0F, -3.0F);
      this.a.setTextureSize(64, 32);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 3, 2, 6);
      this.b.setRotationPoint(-8.0F, 22.0F, -3.0F);
      this.b.setTextureSize(64, 32);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 0, 0);
      this.c.addBox(0.0F, 0.0F, 0.0F, 4, 2, 3);
      this.c.setRotationPoint(-2.0F, 22.0F, 5.0F);
      this.c.setTextureSize(64, 32);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 0, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 3, 3, 4);
      this.d.setRotationPoint(-7.0F, 21.0F, 5.0F);
      this.d.setTextureSize(64, 32);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.7853982F, 0.0F);
      this.e = new ModelRenderer(this, 0, 0);
      this.e.addBox(0.0F, 0.0F, 0.0F, 5, 2, 2);
      this.e.setRotationPoint(2.0F, 22.0F, 6.0F);
      this.e.setTextureSize(64, 32);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.6283185F, 0.0F);
      this.f = new ModelRenderer(this, 0, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 6, 2, 3);
      this.f.setRotationPoint(-3.0F, 22.0F, -8.0F);
      this.f.setTextureSize(64, 32);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 0, 0);
      this.g.addBox(0.0F, 0.0F, 0.0F, 3, 3, 4);
      this.g.setRotationPoint(3.0F, 21.0F, -6.0F);
      this.g.setTextureSize(64, 32);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.7853982F, 0.0F);
      this.h = new ModelRenderer(this, 0, 0);
      this.h.addBox(0.0F, 0.0F, 0.0F, 3, 2, 2);
      this.h.setRotationPoint(-7.0F, 22.0F, -5.0F);
      this.h.setTextureSize(64, 32);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.7853982F, 0.0F);
      this.i = new ModelRenderer(this, 0, 20);
      this.i.addBox(0.0F, -10.0F, 0.0F, 2, 10, 2);
      this.i.setRotationPoint(3.0F, 24.0F, 2.0F);
      this.i.setTextureSize(64, 32);
      this.i.mirror = true;
      this.a(this.i, 0.2268928F, 0.0F, -0.3141593F);
      this.j = new ModelRenderer(this, 0, 20);
      this.j.addBox(0.0F, -10.0F, 0.0F, 2, 10, 2);
      this.j.setRotationPoint(3.0F, 24.0F, -4.0F);
      this.j.setTextureSize(64, 32);
      this.j.mirror = true;
      this.a(this.j, -0.1919862F, 0.0F, -0.3665191F);
      this.k = new ModelRenderer(this, 0, 20);
      this.k.addBox(0.0F, -10.0F, 0.0F, 2, 10, 2);
      this.k.setRotationPoint(-4.0F, 24.0F, -4.0F);
      this.k.setTextureSize(64, 32);
      this.k.mirror = true;
      this.a(this.k, -0.2268928F, 0.0F, 0.1745329F);
      this.l = new ModelRenderer(this, 0, 20);
      this.l.addBox(0.0F, -10.0F, 0.0F, 2, 10, 2);
      this.l.setRotationPoint(-3.5F, 24.0F, 2.5F);
      this.l.setTextureSize(64, 32);
      this.l.mirror = true;
      this.a(this.l, 0.2617994F, 0.0F, 0.1570796F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
