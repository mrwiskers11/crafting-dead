package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelRoadBlock extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;


   public ModelRoadBlock() {
      super.textureWidth = 128;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 1, 1);
      this.a.addBox(-1.0F, 0.0F, 0.0F, 16, 4, 1);
      this.a.setRotationPoint(-7.0F, 11.0F, 0.0F);
      this.a.setTextureSize(128, 128);
      this.a.mirror = true;
      this.a(this.a, 0.2617994F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 41, 1);
      this.b.addBox(-1.0F, 0.0F, -1.0F, 16, 4, 1);
      this.b.setRotationPoint(-7.0F, 11.0F, 0.0F);
      this.b.setTextureSize(128, 128);
      this.b.mirror = true;
      this.a(this.b, -0.2617994F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 81, 1);
      this.c.addBox(0.0F, 1.0F, -0.5F, 2, 13, 1);
      this.c.setRotationPoint(-7.0F, 11.0F, 0.0F);
      this.c.setTextureSize(128, 128);
      this.c.mirror = true;
      this.a(this.c, -0.2617994F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 89, 1);
      this.d.addBox(0.0F, 1.0F, -0.5F, 2, 13, 1);
      this.d.setRotationPoint(5.0F, 11.0F, 0.0F);
      this.d.setTextureSize(128, 128);
      this.d.mirror = true;
      this.a(this.d, -0.2617994F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 97, 1);
      this.e.addBox(0.0F, 1.0F, -0.5F, 2, 13, 1);
      this.e.setRotationPoint(-7.0F, 11.0F, 0.0F);
      this.e.setTextureSize(128, 128);
      this.e.mirror = true;
      this.a(this.e, 0.2617994F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 105, 1);
      this.f.addBox(0.0F, 1.0F, -0.5F, 2, 13, 1);
      this.f.setRotationPoint(5.0F, 11.0F, 0.0F);
      this.f.setTextureSize(128, 128);
      this.f.mirror = true;
      this.a(this.f, 0.2617994F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 38, 9);
      this.g.addBox(-1.0F, 5.0F, -1.0F, 16, 4, 1);
      this.g.setRotationPoint(-7.0F, 11.0F, 0.0F);
      this.g.setTextureSize(128, 128);
      this.g.mirror = true;
      this.a(this.g, -0.2617994F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 38, 15);
      this.h.addBox(-1.0F, 5.0F, 0.0F, 16, 4, 1);
      this.h.setRotationPoint(-7.0F, 11.0F, 0.0F);
      this.h.setTextureSize(128, 128);
      this.h.mirror = true;
      this.a(this.h, 0.2617994F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 28, 13);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.i.setRotationPoint(-6.5F, 10.5F, -0.5F);
      this.i.setTextureSize(128, 128);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 19, 18);
      this.j.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.j.setRotationPoint(5.5F, 10.5F, -0.5F);
      this.j.setTextureSize(128, 128);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 17, 13);
      this.k.addBox(0.0F, 0.0F, 0.0F, 2, 2, 1);
      this.k.setRotationPoint(5.0F, 8.5F, -0.5F);
      this.k.setTextureSize(128, 128);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 0, 25);
      this.l.addBox(0.0F, 0.0F, 0.0F, 2, 2, 1);
      this.l.setRotationPoint(-7.0F, 8.5F, -0.5F);
      this.l.setTextureSize(128, 128);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 0, 16);
      this.m.addBox(0.0F, 0.0F, 0.0F, 2, 1, 4);
      this.m.setRotationPoint(5.0F, 19.0F, -2.0F);
      this.m.setTextureSize(128, 128);
      this.m.mirror = true;
      this.a(this.m, 0.0F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 0, 8);
      this.n.addBox(0.0F, 0.0F, 0.0F, 2, 1, 4);
      this.n.setRotationPoint(-7.0F, 19.0F, -2.0F);
      this.n.setTextureSize(128, 128);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
