package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTrash extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;


   public ModelTrash() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 200, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 20, 2, 4);
      this.a.setRotationPoint(0.0F, 5.0F, -4.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.6108652F, 1.22173F);
      this.b = new ModelRenderer(this, 140, 0);
      this.b.addBox(0.0F, 0.0F, 0.0F, 20, 2, 4);
      this.b.setRotationPoint(0.3F, 5.0F, -0.5F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 1.134464F);
      this.c = new ModelRenderer(this, 0, 100);
      this.c.addBox(0.0F, 0.0F, 0.0F, 7, 10, 7);
      this.c.setRotationPoint(-4.5F, 14.0F, -3.0F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 20, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 2, 2, 2);
      this.d.setRotationPoint(-6.0F, 22.0F, 6.0F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 35, 0);
      this.e.addBox(0.0F, 0.0F, 0.0F, 3, 2, 3);
      this.e.setRotationPoint(-1.0F, 12.0F, -1.0F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, -0.4363323F, 0.0F);
      this.f = new ModelRenderer(this, 50, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 1, 1, 1);
      this.f.setRotationPoint(-3.0F, 13.0F, -2.0F);
      this.f.setTextureSize(256, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 0, 85);
      this.g.addBox(0.0F, 0.0F, 0.0F, 5, 8, 3);
      this.g.setRotationPoint(-3.0F, 15.0F, 1.5F);
      this.g.setTextureSize(256, 128);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 0, 10);
      this.h.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
      this.h.setRotationPoint(-5.6F, 21.0F, 0.0F);
      this.h.setTextureSize(256, 128);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 0, 20);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
      this.i.setRotationPoint(-5.6F, 21.0F, 1.2F);
      this.i.setTextureSize(256, 128);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 0, 30);
      this.j.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
      this.j.setRotationPoint(-6.8F, 21.0F, 0.0F);
      this.j.setTextureSize(256, 128);
      this.j.mirror = true;
      this.a(this.j, 0.0F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 0, 40);
      this.k.addBox(0.0F, 0.0F, 0.0F, 1, 3, 1);
      this.k.setRotationPoint(-7.0F, 21.1F, 1.0F);
      this.k.setTextureSize(256, 128);
      this.k.mirror = true;
      this.a(this.k, 0.2268928F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 0, 50);
      this.l.addBox(0.0F, 0.0F, 0.0F, 3, 1, 3);
      this.l.setRotationPoint(-7.25F, 23.0F, -0.35F);
      this.l.setTextureSize(256, 128);
      this.l.mirror = true;
      this.a(this.l, 0.0F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 20, 20);
      this.m.addBox(0.0F, 0.0F, 0.0F, 3, 5, 1);
      this.m.setRotationPoint(-3.0F, 19.0F, -4.0F);
      this.m.setTextureSize(256, 128);
      this.m.mirror = true;
      this.a(this.m, -0.2094395F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 100, 1);
      this.n.addBox(0.0F, 0.0F, 0.0F, 1, 2, 7);
      this.n.setRotationPoint(2.0F, 22.0F, -8.0F);
      this.n.setTextureSize(256, 128);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.1570796F, 0.0698132F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
