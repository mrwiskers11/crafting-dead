package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelSandBarrier extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;


   public ModelSandBarrier() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 15, 16, 15);
      this.a.setRotationPoint(-7.5F, 8.0F, -7.5F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 32, 32);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 16, 14);
      this.b.setRotationPoint(-8.0F, 8.0F, -7.0F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 32, 32);
      this.c.addBox(0.0F, 0.0F, 0.0F, 1, 16, 14);
      this.c.setRotationPoint(7.0F, 8.0F, -7.0F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 0, 32);
      this.d.addBox(0.0F, 0.0F, 0.0F, 14, 16, 1);
      this.d.setRotationPoint(-7.0F, 8.0F, -8.0F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 0, 32);
      this.e.addBox(0.0F, 0.0F, 0.0F, 14, 16, 1);
      this.e.setRotationPoint(-7.0F, 8.0F, 7.0F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 61, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 14, 1, 14);
      this.f.setRotationPoint(-7.0F, 7.5F, -7.0F);
      this.f.setTextureSize(256, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
