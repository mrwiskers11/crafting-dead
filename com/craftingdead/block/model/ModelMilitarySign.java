package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMilitarySign extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;


   public ModelMilitarySign() {
      super.textureWidth = 512;
      super.textureHeight = 256;
      this.a = new ModelRenderer(this, 160, 54);
      this.a.addBox(0.0F, 0.0F, 0.0F, 1, 24, 1);
      this.a.setRotationPoint(-13.0F, 0.0F, -3.0F);
      this.a.setTextureSize(512, 256);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 75, 55);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 24, 1);
      this.b.setRotationPoint(14.0F, 0.0F, -3.0F);
      this.b.setTextureSize(512, 256);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 84, 39);
      this.c.addBox(0.0F, 0.0F, 0.0F, 32, 13, 1);
      this.c.setRotationPoint(-15.0F, -2.0F, -4.0F);
      this.c.setTextureSize(512, 256);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 170, 70);
      this.d.addBox(0.0F, 0.0F, -4.0F, 1, 19, 1);
      this.d.setRotationPoint(-13.0F, 6.0F, 0.0F);
      this.d.setTextureSize(512, 256);
      this.d.mirror = true;
      this.a(this.d, 0.5205006F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 70, 72);
      this.e.addBox(27.0F, 0.0F, -4.0F, 1, 19, 1);
      this.e.setRotationPoint(-13.0F, 6.0F, 0.0F);
      this.e.setTextureSize(512, 256);
      this.e.mirror = true;
      this.a(this.e, 0.5205006F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 82, 33);
      this.f.addBox(0.0F, 0.0F, 0.0F, 30, 1, 1);
      this.f.setRotationPoint(-14.0F, -1.0F, -3.0F);
      this.f.setTextureSize(512, 256);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 82, 62);
      this.g.addBox(0.0F, 0.0F, 0.0F, 30, 1, 1);
      this.g.setRotationPoint(-14.0F, 9.0F, -3.0F);
      this.g.setTextureSize(512, 256);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.e.render(var7);
      this.f.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
