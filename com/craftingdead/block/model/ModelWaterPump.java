package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelWaterPump extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;


   public ModelWaterPump() {
      super.textureWidth = 128;
      super.textureHeight = 64;
      this.a = new ModelRenderer(this, 31, 31);
      this.a.addBox(0.0F, 0.0F, 0.0F, 12, 9, 12);
      this.a.setRotationPoint(-6.0F, 15.0F, -6.0F);
      this.a.setTextureSize(128, 64);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 8, 32);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 9, 10);
      this.b.setRotationPoint(-7.0F, 15.0F, -5.0F);
      this.b.setTextureSize(128, 64);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, -0.1487144F);
      this.c = new ModelRenderer(this, 43, 54);
      this.c.addBox(0.0F, 0.0F, 0.0F, 10, 9, 1);
      this.c.setRotationPoint(-5.0F, 15.0F, -7.0F);
      this.c.setTextureSize(128, 64);
      this.c.mirror = true;
      this.a(this.c, 0.1487144F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 80, 32);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 9, 10);
      this.d.setRotationPoint(6.0F, 15.0F, -5.0F);
      this.d.setTextureSize(128, 64);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.1487144F);
      this.e = new ModelRenderer(this, 44, 19);
      this.e.addBox(0.0F, 0.0F, 0.0F, 10, 10, 1);
      this.e.setRotationPoint(-5.0F, 15.0F, 6.0F);
      this.e.setTextureSize(128, 64);
      this.e.mirror = true;
      this.a(this.e, -0.1487144F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 24, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 16, 1, 16);
      this.f.setRotationPoint(-8.0F, 14.0F, -8.0F);
      this.f.setTextureSize(128, 64);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 109, 36);
      this.g.addBox(0.0F, 0.0F, 0.0F, 2, 17, 2);
      this.g.setRotationPoint(4.0F, -3.0F, -4.0F);
      this.g.setTextureSize(128, 64);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 105, 57);
      this.h.addBox(0.0F, 0.0F, 0.0F, 4, 2, 4);
      this.h.setRotationPoint(3.0F, 12.0F, -5.0F);
      this.h.setTextureSize(128, 64);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, 0.0F);
      this.i = new ModelRenderer(this, 113, 9);
      this.i.addBox(0.0F, 0.0F, 0.0F, 1, 1, 5);
      this.i.setRotationPoint(4.5F, 6.0F, -8.666667F);
      this.i.setTextureSize(128, 64);
      this.i.mirror = true;
      this.a(this.i, 0.1858931F, 0.0F, 0.0F);
      this.j = new ModelRenderer(this, 120, 49);
      this.j.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1);
      this.j.setRotationPoint(4.5F, 6.0F, -8.666667F);
      this.j.setTextureSize(128, 64);
      this.j.mirror = true;
      this.a(this.j, -0.1115358F, 0.0F, 0.0F);
      this.k = new ModelRenderer(this, 107, 29);
      this.k.addBox(0.0F, 0.0F, 0.0F, 3, 2, 3);
      this.k.setRotationPoint(3.5F, -4.0F, -4.5F);
      this.k.setTextureSize(128, 64);
      this.k.mirror = true;
      this.a(this.k, 0.0F, 0.0F, 0.0F);
      this.l = new ModelRenderer(this, 109, 22);
      this.l.addBox(0.0F, 0.0F, 0.0F, 2, 4, 1);
      this.l.setRotationPoint(4.0F, -2.466667F, -2.533333F);
      this.l.setTextureSize(128, 64);
      this.l.mirror = true;
      this.a(this.l, -0.1487144F, 0.0F, 0.0F);
      this.m = new ModelRenderer(this, 109, 22);
      this.m.addBox(0.0F, 0.0F, 0.0F, 2, 4, 1);
      this.m.setRotationPoint(4.0F, -2.5F, -4.5F);
      this.m.setTextureSize(128, 64);
      this.m.mirror = true;
      this.a(this.m, 0.1487144F, 0.0F, 0.0F);
      this.n = new ModelRenderer(this, 98, 18);
      this.n.addBox(-0.4666667F, 0.0F, 0.0F, 10, 1, 1);
      this.n.setRotationPoint(6.0F, -1.0F, -3.5F);
      this.n.setTextureSize(128, 64);
      this.n.mirror = true;
      this.a(this.n, 0.0F, 0.1487144F, 1.003822F);
      this.o = new ModelRenderer(this, 119, 22);
      this.o.addBox(9.5F, -1.0F, -0.5F, 2, 1, 2);
      this.o.setRotationPoint(6.0F, -1.0F, -3.5F);
      this.o.setTextureSize(128, 64);
      this.o.mirror = true;
      this.a(this.o, -1.59868F, 0.1487144F, 1.003822F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
