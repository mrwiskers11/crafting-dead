package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelShelfLoot extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;
   ModelRenderer i;
   ModelRenderer j;
   ModelRenderer k;
   ModelRenderer l;
   ModelRenderer m;
   ModelRenderer n;
   ModelRenderer o;
   ModelRenderer v;


   public ModelShelfLoot() {
      super.textureWidth = 128;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 32, 1, 16);
      this.a.setRotationPoint(-16.0F, -6.0F, -8.0F);
      this.a.setTextureSize(128, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 93);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.b.setRotationPoint(-15.5F, -5.0F, -7.5F);
      this.b.setTextureSize(128, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 5, 93);
      this.c.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.c.setRotationPoint(-15.5F, -5.0F, 6.5F);
      this.c.setTextureSize(128, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 10, 93);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.d.setRotationPoint(14.5F, -5.0F, 6.5F);
      this.d.setTextureSize(128, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 15, 93);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.e.setRotationPoint(14.5F, -5.0F, -7.5F);
      this.e.setTextureSize(128, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 0, 18);
      this.f.addBox(0.0F, 0.0F, 0.0F, 32, 1, 16);
      this.f.setRotationPoint(-16.0F, 4.0F, -8.0F);
      this.f.setTextureSize(128, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 0, 36);
      this.g.addBox(0.0F, 0.0F, 0.0F, 32, 1, 16);
      this.g.setRotationPoint(-16.0F, 14.0F, -8.0F);
      this.g.setTextureSize(128, 128);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
      this.h = new ModelRenderer(this, 0, 55);
      this.h.addBox(0.0F, 0.0F, 0.0F, 6, 7, 3);
      this.h.setRotationPoint(-14.0F, 7.0F, -3.0F);
      this.h.setTextureSize(128, 128);
      this.h.mirror = true;
      this.a(this.h, 0.0F, -0.122173F, 0.0174533F);
      this.i = new ModelRenderer(this, 0, 66);
      this.i.addBox(0.0F, 0.0F, 0.0F, 5, 2, 2);
      this.i.setRotationPoint(-3.0F, 12.0F, -4.0F);
      this.i.setTextureSize(128, 128);
      this.i.mirror = true;
      this.a(this.i, 0.0F, 0.1919862F, 0.0F);
      this.j = new ModelRenderer(this, 0, 71);
      this.j.addBox(1.0F, 0.0F, 0.0F, 3, 5, 4);
      this.j.setRotationPoint(-14.0F, -1.0F, -4.0F);
      this.j.setTextureSize(128, 128);
      this.j.mirror = true;
      this.a(this.j, 0.0F, -0.1396263F, 0.0F);
      this.k = new ModelRenderer(this, 15, 72);
      this.k.addBox(0.5F, 0.0F, 0.5F, 1, 5, 3);
      this.k.setRotationPoint(-14.0F, -1.0F, -4.0F);
      this.k.setTextureSize(128, 128);
      this.k.mirror = true;
      this.a(this.k, 0.0F, -0.1396263F, 0.0F);
      this.l = new ModelRenderer(this, 24, 72);
      this.l.addBox(3.5F, 0.0F, 0.5F, 1, 5, 3);
      this.l.setRotationPoint(-14.0F, -1.0F, -4.0F);
      this.l.setTextureSize(128, 128);
      this.l.mirror = true;
      this.a(this.l, 0.0F, -0.1396263F, 0.0F);
      this.m = new ModelRenderer(this, 33, 71);
      this.m.addBox(1.0F, 0.0F, 0.0F, 3, 5, 4);
      this.m.setRotationPoint(6.0F, 9.0F, -2.0F);
      this.m.setTextureSize(128, 128);
      this.m.mirror = true;
      this.a(this.m, 0.0F, -0.1396263F, 0.0F);
      this.n = new ModelRenderer(this, 0, 81);
      this.n.addBox(0.5F, 0.0F, 0.5F, 1, 5, 3);
      this.n.setRotationPoint(6.0F, 9.0F, -2.0F);
      this.n.setTextureSize(128, 128);
      this.n.mirror = true;
      this.a(this.n, 0.0F, -0.1396263F, 0.0F);
      this.o = new ModelRenderer(this, 10, 81);
      this.o.addBox(3.5F, 0.0F, 0.5F, 1, 5, 3);
      this.o.setRotationPoint(6.0F, 9.0F, -2.0F);
      this.o.setTextureSize(128, 128);
      this.o.mirror = true;
      this.a(this.o, 0.0F, -0.1396263F, 0.0F);
      this.v = new ModelRenderer(this, 19, 55);
      this.v.addBox(0.0F, 0.0F, 0.0F, 6, 7, 3);
      this.v.setRotationPoint(-3.0F, 4.0F, -2.0F);
      this.v.setTextureSize(128, 128);
      this.v.mirror = true;
      this.a(this.v, 1.570796F, 0.5934119F, 0.0174533F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
      this.i.render(var7);
      this.j.render(var7);
      this.k.render(var7);
      this.l.render(var7);
      this.m.render(var7);
      this.n.render(var7);
      this.o.render(var7);
      this.v.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
