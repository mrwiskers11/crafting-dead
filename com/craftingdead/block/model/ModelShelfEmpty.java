package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelShelfEmpty extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;


   public ModelShelfEmpty() {
      super.textureWidth = 128;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 0, 0);
      this.a.addBox(0.0F, 0.0F, 0.0F, 32, 1, 16);
      this.a.setRotationPoint(-16.0F, -6.0F, -8.0F);
      this.a.setTextureSize(128, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 93);
      this.b.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.b.setRotationPoint(-15.5F, -5.0F, -7.5F);
      this.b.setTextureSize(128, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 5, 93);
      this.c.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.c.setRotationPoint(-15.5F, -5.0F, 6.5F);
      this.c.setTextureSize(128, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 10, 93);
      this.d.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.d.setRotationPoint(14.5F, -5.0F, 6.5F);
      this.d.setTextureSize(128, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 15, 93);
      this.e.addBox(0.0F, 0.0F, 0.0F, 1, 29, 1);
      this.e.setRotationPoint(14.5F, -5.0F, -7.5F);
      this.e.setTextureSize(128, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.0F);
      this.f = new ModelRenderer(this, 0, 18);
      this.f.addBox(0.0F, 0.0F, 0.0F, 32, 1, 16);
      this.f.setRotationPoint(-16.0F, 4.0F, -8.0F);
      this.f.setTextureSize(128, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, 0.0F);
      this.g = new ModelRenderer(this, 0, 36);
      this.g.addBox(0.0F, 0.0F, 0.0F, 32, 1, 16);
      this.g.setRotationPoint(-16.0F, 14.0F, -8.0F);
      this.g.setTextureSize(128, 128);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.0F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
