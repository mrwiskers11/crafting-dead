package com.craftingdead.block.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelStopSign extends ModelBase {

   ModelRenderer a;
   ModelRenderer b;
   ModelRenderer c;
   ModelRenderer d;
   ModelRenderer e;
   ModelRenderer f;
   ModelRenderer g;
   ModelRenderer h;


   public ModelStopSign() {
      super.textureWidth = 256;
      super.textureHeight = 128;
      this.a = new ModelRenderer(this, 0, 50);
      this.a.addBox(0.0F, 0.0F, 0.0F, 3, 5, 3);
      this.a.setRotationPoint(-1.0F, 19.0F, -1.0F);
      this.a.setTextureSize(256, 128);
      this.a.mirror = true;
      this.a(this.a, 0.0F, 0.0F, 0.0F);
      this.b = new ModelRenderer(this, 0, 80);
      this.b.addBox(0.0F, 0.0F, 0.0F, 2, 30, 2);
      this.b.setRotationPoint(-0.5F, -9.0F, -0.5F);
      this.b.setTextureSize(256, 128);
      this.b.mirror = true;
      this.a(this.b, 0.0F, 0.0F, 0.0F);
      this.c = new ModelRenderer(this, 175, 0);
      this.c.addBox(0.0F, 0.0F, 0.0F, 7, 17, 2);
      this.c.setRotationPoint(-3.0F, -26.0F, -0.5F);
      this.c.setTextureSize(256, 128);
      this.c.mirror = true;
      this.a(this.c, 0.0F, 0.0F, 0.0F);
      this.d = new ModelRenderer(this, 200, 0);
      this.d.addBox(0.0F, 0.0F, 0.0F, 16, 7, 3);
      this.d.setRotationPoint(-7.5F, -21.5F, -1.0F);
      this.d.setTextureSize(256, 128);
      this.d.mirror = true;
      this.a(this.d, 0.0F, 0.0F, 0.0F);
      this.e = new ModelRenderer(this, 155, 0);
      this.e.addBox(0.0F, 0.0F, 0.0F, 5, 6, 2);
      this.e.setRotationPoint(-3.1F, -26.0F, -0.5F);
      this.e.setTextureSize(256, 128);
      this.e.mirror = true;
      this.a(this.e, 0.0F, 0.0F, 0.7679449F);
      this.f = new ModelRenderer(this, 130, 0);
      this.f.addBox(0.0F, 0.0F, 0.0F, 5, 6, 2);
      this.f.setRotationPoint(0.5F, -22.4F, -0.5F);
      this.f.setTextureSize(256, 128);
      this.f.mirror = true;
      this.a(this.f, 0.0F, 0.0F, -0.7853982F);
      this.g = new ModelRenderer(this, 110, 0);
      this.g.addBox(0.0F, 0.0F, 0.0F, 5, 7, 2);
      this.g.setRotationPoint(4.8F, -17.7F, -0.5F);
      this.g.setTextureSize(256, 128);
      this.g.mirror = true;
      this.a(this.g, 0.0F, 0.0F, 0.715585F);
      this.h = new ModelRenderer(this, 90, 0);
      this.h.addBox(0.0F, 0.0F, 0.0F, 5, 7, 2);
      this.h.setRotationPoint(-7.5F, -14.4F, -0.5F);
      this.h.setTextureSize(256, 128);
      this.h.mirror = true;
      this.a(this.h, 0.0F, 0.0F, -0.715585F);
   }

   public void render(Entity var1, float var2, float var3, float var4, float var5, float var6, float var7) {
      super.render(var1, var2, var3, var4, var5, var6, var7);
      this.setRotationAngles(var2, var3, var4, var5, var6, var7, var1);
      this.a.render(var7);
      this.b.render(var7);
      this.c.render(var7);
      this.d.render(var7);
      this.e.render(var7);
      this.f.render(var7);
      this.g.render(var7);
      this.h.render(var7);
   }

   private void a(ModelRenderer var1, float var2, float var3, float var4) {
      var1.rotateAngleX = var2;
      var1.rotateAngleY = var3;
      var1.rotateAngleZ = var4;
   }

   public void setRotationAngles(float var1, float var2, float var3, float var4, float var5, float var6, Entity var7) {
      super.setRotationAngles(var1, var2, var3, var4, var5, var6, var7);
   }
}
