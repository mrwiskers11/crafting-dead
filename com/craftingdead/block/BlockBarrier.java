package com.craftingdead.block;

import com.craftingdead.block.BlockCD;

import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;

public class BlockBarrier extends BlockCD {

   public BlockBarrier(int var1) {
      super(var1, Material.air);
      this.setBlockUnbreakable();
      this.setResistance(6000001.0F);
      this.disableStats();
      this.a("barrierblock");
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public int getRenderType() {
      return Minecraft.getMinecraft().playerController.isInCreativeMode()?0:-1;
   }
}
