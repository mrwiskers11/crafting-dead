package com.craftingdead.block;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityShelfLoot;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.inventory.InventoryShelfLoot;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockShelfLoot extends BlockContainer {

   public BlockShelfLoot(int var1) {
      super(var1, Material.rock);
      this.setHardness(2.0F);
      this.setCreativeTab(ItemManager.f);
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(1.0F, 0.0F, -0.0F, -0.0F, 1.9F, 1.1F);
   }

   public boolean onBlockActivated(World var1, int var2, int var3, int var4, EntityPlayer var5, int var6, float var7, float var8, float var9) {
      TileEntityShelfLoot var10 = (TileEntityShelfLoot)var1.getBlockTileEntity(var2, var3, var4);
      if(var10.d <= 0) {
         if(!var1.isRemote) {
            var5.openGui(CraftingDead.f, CraftingDeadGui.f.a(), var1, var2, var3, var4);
            var10.d = 4800;
         }
      } else {
         var5.sendChatToPlayer(ChatMessageComponent.createFromText("This shelf is empty."));
      }

      return false;
   }

   public int getRenderType() {
      return BlockManager.r;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onBlockPlacedBy(World var1, int var2, int var3, int var4, EntityLivingBase var5, ItemStack var6) {
      if(var5 != null) {
         TileEntityShelfLoot var7 = (TileEntityShelfLoot)var1.getBlockTileEntity(var2, var3, var4);
         var7.a = MathHelper.floor_double((double)(var5.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
      }
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityShelfLoot();
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }

   public static IInventory a(EntityPlayer var0) {
      InventoryShelfLoot var1 = null;
      if(!var0.worldObj.isRemote) {
         ItemStack var2 = new ItemStack(BlockManager.ae);
         if(var2 != null) {
            var1 = new InventoryShelfLoot(var2);
         }
      }

      return var1;
   }
}
