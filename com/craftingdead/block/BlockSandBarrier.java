package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntitySandBarrier;
import com.craftingdead.item.ItemCDPickaxe;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockSandBarrier extends BlockContainer {

   public BlockSandBarrier(int var1) {
      super(var1, Material.rock);
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
      this.setCreativeTab(ItemManager.a);
   }

   public int getRenderType() {
      return BlockManager.d;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntitySandBarrier();
   }

   public boolean removeBlockByPlayer(World var1, EntityPlayer var2, int var3, int var4, int var5) {
      return var2.capabilities.isCreativeMode?var1.setBlockToAir(var3, var4, var5):(var2.getCurrentEquippedItem() != null && var2.getCurrentEquippedItem().getItem() instanceof ItemCDPickaxe?var1.setBlockToAir(var3, var4, var5):false);
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }
}
