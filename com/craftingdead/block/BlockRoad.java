package com.craftingdead.block;

import com.craftingdead.block.BlockCD;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class BlockRoad extends BlockCD {

   @SideOnly(Side.CLIENT)
   protected Icon a;
   @SideOnly(Side.CLIENT)
   protected Icon b;
   protected Icon c;
   private String d;


   public BlockRoad(int var1, String var2) {
      super(var1);
      this.d = var2;
      this.setCreativeTab(ItemManager.f);
   }

   public void onBlockAdded(World var1, int var2, int var3, int var4) {
      super.onBlockAdded(var1, var2, var3, var4);
      this.e(var1, var2, var3, var4);
   }

   private void e(World var1, int var2, int var3, int var4) {
      if(!var1.isRemote) {
         int var5 = var1.getBlockId(var2, var3, var4 - 1);
         int var6 = var1.getBlockId(var2, var3, var4 + 1);
         int var7 = var1.getBlockId(var2 - 1, var3, var4);
         int var8 = var1.getBlockId(var2 + 1, var3, var4);
         byte var9 = 3;
         if(Block.opaqueCubeLookup[var5] && !Block.opaqueCubeLookup[var6]) {
            var9 = 3;
         }

         if(Block.opaqueCubeLookup[var6] && !Block.opaqueCubeLookup[var5]) {
            var9 = 2;
         }

         if(Block.opaqueCubeLookup[var7] && !Block.opaqueCubeLookup[var8]) {
            var9 = 5;
         }

         if(Block.opaqueCubeLookup[var8] && !Block.opaqueCubeLookup[var7]) {
            var9 = 4;
         }

         var1.setBlockMetadataWithNotify(var2, var3, var4, var9, 2);
      }

   }

   public void onBlockPlacedBy(World var1, int var2, int var3, int var4, EntityLivingBase var5, ItemStack var6) {
      int var7 = MathHelper.floor_double((double)(var5.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
      if(var7 == 0) {
         var1.setBlockMetadataWithNotify(var2, var3, var4, 2, 2);
      }

      if(var7 == 1) {
         var1.setBlockMetadataWithNotify(var2, var3, var4, 5, 2);
      }

      if(var7 == 2) {
         var1.setBlockMetadataWithNotify(var2, var3, var4, 3, 2);
      }

      if(var7 == 3) {
         var1.setBlockMetadataWithNotify(var2, var3, var4, 4, 2);
      }

   }

   @SideOnly(Side.CLIENT)
   public Icon getIcon(int var1, int var2) {
      return (var2 == 4 || var2 == 5) && var1 == 1?this.c:((var2 == 2 || var2 == 3) && var1 == 1?this.b:(var1 == 1?this.b:this.a));
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      this.b = var1.registerIcon("craftingdead:road_" + this.d);
      this.c = var1.registerIcon("craftingdead:road_" + this.d + "_1");
      this.a = var1.registerIcon("craftingdead:road");
   }
}
