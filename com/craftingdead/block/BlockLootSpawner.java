package com.craftingdead.block;

import com.craftingdead.block.BlockLoot;
import com.craftingdead.block.LootType;
import com.craftingdead.block.tileentity.TileEntityLootSpawner;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockLootSpawner extends BlockContainer {

   private String b = "";
   public int a = 0;


   public BlockLootSpawner(int var1, String var2, int var3) {
      super(var1, Material.wood);
      this.b = var2;
      this.a = var3;
      this.setBlockUnbreakable();
      this.setCreativeTab(ItemManager.a);
   }

   @SideOnly(Side.CLIENT)
   public Icon getBlockTexture(IBlockAccess var1, int var2, int var3, int var4, int var5) {
      TileEntityLootSpawner var6 = (TileEntityLootSpawner)var1.getBlockTileEntity(var2, var3, var4);
      var6.a();
      return var6 != null && var6.a != 0?Block.blocksList[var6.a].getIcon(var5, var6.b):this.getIcon(var5, var1.getBlockMetadata(var2, var3, var4));
   }

   public boolean e() {
      LootType var1 = ((BlockLoot)Block.blocksList[this.a]).e();
      return var1.c();
   }

   @SideOnly(Side.CLIENT)
   public int colorMultiplier(IBlockAccess var1, int var2, int var3, int var4) {
      TileEntityLootSpawner var5 = (TileEntityLootSpawner)var1.getBlockTileEntity(var2, var3, var4);
      var5.a();
      if(var5 != null && var5.a != 0) {
         Block var6 = Block.blocksList[var5.a];
         if(!(var6 instanceof BlockLootSpawner)) {
            return var6 != null?var6.colorMultiplier(var1, var2, var3, var4):16777215;
         }
      }

      return 16777215;
   }

   @SideOnly(Side.CLIENT)
   public Icon getIcon(int var1, int var2) {
      return super.blockIcon;
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawner" + this.b);
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityLootSpawner();
   }
}
