package com.craftingdead.block;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityForge;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.Random;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockForge extends BlockContainer {

   private final Random a = new Random();
   private static boolean b;


   public BlockForge(int var1) {
      super(var1, Material.rock);
      this.setHardness(2.0F);
      this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.8F, 1.0F);
      this.setCreativeTab(ItemManager.f);
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(-0.0F, -0.0F, -0.0F, 0.9F, 1.0F, 1.0F);
   }

   public int getRenderType() {
      return BlockManager.s;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onBlockPlacedBy(World var1, int var2, int var3, int var4, EntityLivingBase var5, ItemStack var6) {
      if(var5 != null) {
         ;
      }
   }

   public boolean onBlockActivated(World var1, int var2, int var3, int var4, EntityPlayer var5, int var6, float var7, float var8, float var9) {
      var5.openGui(CraftingDead.f, CraftingDeadGui.g.a(), var1, var2, var3, var4);
      return false;
   }

   public static void a(boolean var0, World var1, int var2, int var3, int var4) {
      int var5 = var1.getBlockMetadata(var2, var3, var4);
      TileEntity var6 = var1.getBlockTileEntity(var2, var3, var4);
      b = true;
      if(var0) {
         var1.setBlock(var2, var3, var4, BlockManager.af.blockID);
      } else {
         var1.setBlock(var2, var3, var4, BlockManager.af.blockID);
      }

      b = false;
      var1.setBlockMetadataWithNotify(var2, var3, var4, var5, 2);
      if(var6 != null) {
         var6.validate();
         var1.setBlockTileEntity(var2, var3, var4, var6);
      }

   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityForge();
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }

   public void breakBlock(World var1, int var2, int var3, int var4, int var5, int var6) {
      if(!b) {
         TileEntityForge var7 = (TileEntityForge)var1.getBlockTileEntity(var2, var3, var4);
         if(var7 != null) {
            for(int var8 = 0; var8 < var7.getSizeInventory(); ++var8) {
               ItemStack var9 = var7.getStackInSlot(var8);
               if(var9 != null) {
                  float var10 = this.a.nextFloat() * 0.8F + 0.1F;
                  float var11 = this.a.nextFloat() * 0.8F + 0.1F;
                  float var12 = this.a.nextFloat() * 0.8F + 0.1F;

                  while(var9.stackSize > 0) {
                     int var13 = this.a.nextInt(21) + 10;
                     if(var13 > var9.stackSize) {
                        var13 = var9.stackSize;
                     }

                     var9.stackSize -= var13;
                     EntityItem var14 = new EntityItem(var1, (double)((float)var2 + var10), (double)((float)var3 + var11), (double)((float)var4 + var12), new ItemStack(var9.itemID, var13, var9.getItemDamage()));
                     if(var9.hasTagCompound()) {
                        var14.getEntityItem().setTagCompound((NBTTagCompound)var9.getTagCompound().copy());
                     }

                     float var15 = 0.05F;
                     var14.motionX = (double)((float)this.a.nextGaussian() * var15);
                     var14.motionY = (double)((float)this.a.nextGaussian() * var15 + 0.2F);
                     var14.motionZ = (double)((float)this.a.nextGaussian() * var15);
                     var1.spawnEntityInWorld(var14);
                  }
               }
            }

            var1.func_96440_m(var2, var3, var4, var5);
         }
      }

      super.breakBlock(var1, var2, var3, var4, var5, var6);
   }

   public boolean hasComparatorInputOverride() {
      return true;
   }

   public int getComparatorInputOverride(World var1, int var2, int var3, int var4, int var5) {
      return Container.calcRedstoneFromInventory((IInventory)var1.getBlockTileEntity(var2, var3, var4));
   }

   @SideOnly(Side.CLIENT)
   public int idPicked(World var1, int var2, int var3, int var4) {
      return BlockManager.af.blockID;
   }
}
