package com.craftingdead.block;

import com.craftingdead.CraftingDead;
import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityVendingMachine;
import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.inventory.InventoryVendingMachine;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockVendingMachine extends BlockContainer {

   public BlockVendingMachine(int var1) {
      super(var1, Material.rock);
      this.setHardness(2.0F);
      this.setCreativeTab(ItemManager.f);
   }

   public void setBlockBoundsBasedOnState(IBlockAccess var1, int var2, int var3, int var4) {
      this.setBlockBounds(-0.0F, -0.0F, -0.0F, 0.98F, 1.93F, 1.0F);
   }

   public boolean onBlockActivated(World var1, int var2, int var3, int var4, EntityPlayer var5, int var6, float var7, float var8, float var9) {
      TileEntityVendingMachine var10 = (TileEntityVendingMachine)var1.getBlockTileEntity(var2, var3, var4);
      if(var10.d <= 0) {
         if(!var1.isRemote) {
            var5.openGui(CraftingDead.f, CraftingDeadGui.e.a(), var1, var2, var3, var4);
            var10.d = 4800;
         }
      } else {
         var5.sendChatToPlayer(ChatMessageComponent.createFromText("This vending machine is empty."));
      }

      return false;
   }

   public int getRenderType() {
      return BlockManager.p;
   }

   public boolean isOpaqueCube() {
      return false;
   }

   public boolean renderAsNormalBlock() {
      return false;
   }

   public void onBlockPlacedBy(World var1, int var2, int var3, int var4, EntityLivingBase var5, ItemStack var6) {
      if(var5 != null) {
         TileEntityVendingMachine var7 = (TileEntityVendingMachine)var1.getBlockTileEntity(var2, var3, var4);
         var7.a = MathHelper.floor_double((double)(var5.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
      }
   }

   public TileEntity createNewTileEntity(World var1) {
      return new TileEntityVendingMachine();
   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      super.blockIcon = var1.registerIcon("craftingdead:lootspawnerresidential");
   }

   public static IInventory a(EntityPlayer var0) {
      InventoryVendingMachine var1 = null;
      if(!var0.worldObj.isRemote) {
         ItemStack var2 = new ItemStack(BlockManager.ab);
         if(var2 != null) {
            var1 = new InventoryVendingMachine(var2);
         }
      }

      return var1;
   }
}
