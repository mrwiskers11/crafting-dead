package com.craftingdead.block;

import com.craftingdead.block.BlockManager;
import com.craftingdead.item.ItemManager;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHalfSlab;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockClaySlab extends BlockHalfSlab {

   @SideOnly(Side.CLIENT)
   private Icon[] b;
   private Icon[] c;
   private boolean d = true;
   private boolean e = false;


   public BlockClaySlab(int var1, boolean var2, boolean var3) {
      super(var1, var2, Material.clay);
      this.d = var3;
      this.setHardness(2.0F);
      this.setCreativeTab(ItemManager.f);
      Block.useNeighborBrightness[var1] = true;
   }

   @SideOnly(Side.CLIENT)
   public Icon getIcon(int var1, int var2) {
      return this.d?this.b[var2 % 8]:this.c[var2 % 8];
   }

   public BlockClaySlab e() {
      this.e = true;
      return this;
   }

   public int idDropped(int var1, Random var2, int var3) {
      return this.e?BlockManager.av.blockID:BlockManager.at.blockID;
   }

   protected ItemStack createStackedBlock(int var1) {
      return this.e?new ItemStack(BlockManager.av.blockID, 2, var1):new ItemStack(BlockManager.at.blockID, 2, var1);
   }

   public int idPicked(World var1, int var2, int var3, int var4) {
      return this.e?BlockManager.av.blockID:BlockManager.at.blockID;
   }

   public String getFullSlabName(int var1) {
      if(var1 < 0 || var1 >= ItemDye.dyeColors.length) {
         var1 = 0;
      }

      return super.getUnlocalizedName() + "." + ItemDye.dyeColorNames[var1];
   }

   @SideOnly(Side.CLIENT)
   public void getSubBlocks(int var1, CreativeTabs var2, List var3) {
      if(!super.isDoubleSlab) {
         int var4;
         if(this.d) {
            if(var1 != BlockManager.as.blockID) {
               for(var4 = 0; var4 < 7; ++var4) {
                  var3.add(new ItemStack(var1, 1, var4));
               }
            }
         } else if(var1 != BlockManager.as.blockID) {
            for(var4 = 0; var4 < 8; ++var4) {
               var3.add(new ItemStack(var1, 1, var4));
            }
         }
      }

   }

   @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister var1) {
      this.b = new Icon[16];
      this.c = new Icon[16];
      int var2;
      if(this.d) {
         for(var2 = 0; var2 < 7; ++var2) {
            this.b[var2] = var1.registerIcon("craftingdead:clay/hardened_clay_stained_" + ItemDye.dyeColorNames[d(var2)].toLowerCase());
         }
      } else {
         for(var2 = 0; var2 < 8; ++var2) {
            this.c[var2] = var1.registerIcon("craftingdead:clay/hardened_clay_stained_" + ItemDye.dyeColorNames[d(var2 + 7)].toLowerCase());
         }
      }

   }

   public static int d(int var0) {
      return ~var0 & 15;
   }
}
