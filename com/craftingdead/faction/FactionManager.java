package com.craftingdead.faction;

import com.craftingdead.CraftingDead;
import com.craftingdead.cloud.CMClient;
import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.cloud.packet.PacketRegistry;
import com.craftingdead.faction.Faction;
import com.craftingdead.faction.packet.PacketFactionCommand;
import com.craftingdead.player.PlayerDataHandler;
import com.f3rullo14.fds.tag.FDSTagCompound;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import java.util.ArrayList;
import java.util.TreeMap;
import net.minecraft.client.Minecraft;

public class FactionManager {

   private TreeMap a;
   private ArrayList b;
   private int c;


   public FactionManager() {
      this.a = new TreeMap(String.CASE_INSENSITIVE_ORDER);
      this.b = new ArrayList();
      this.c = 5;
   }

   public void a(String var1) {
      if(!this.b.contains(var1)) {
         PacketRegistry.a(new PacketFactionCommand("GET " + var1));
         this.b.add(var1);
      }
   }

   public void a(String var1, FDSTagCompound var2) {
      this.b.remove(var1);
      Faction var3 = Faction.a(var2);
      this.a.put(var1, var3);
   }

   public Faction b(String var1) {
      Faction var2 = (Faction)this.a.get(var1);
      if(var2 == null) {
         this.a(var1);
      }

      return var2;
   }

   public Faction a() {
      CDPlayer var1 = PlayerDataHandler.a(Minecraft.getMinecraft().getSession().getUsername());
      return this.b(var1.g);
   }

   public String a(CDPlayer var1, String var2) {
      if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
         return "Can not perform action on servers.";
      } else if(this.a(var1)) {
         return "Already apart of a faction.";
      } else if(!this.d(var2)) {
         return "Faction tag too small or too big!";
      } else if(!this.c(var2)) {
         return "Invalid characters.";
      } else {
         CraftingDead.d.info("[Faction] Sending creation request.");
         CMClient.b(new PacketFactionCommand("CREATE " + var2));
         return null;
      }
   }

   public boolean a(CDPlayer var1) {
      return var1 != null && var1.g != null;
   }

   public boolean b() {
      return this.a(PlayerDataHandler.a(Minecraft.getMinecraft().getSession().getUsername()));
   }

   public boolean c(String var1) {
      for(int var2 = 0; var2 < var1.length(); ++var2) {
         if(!Character.isLetter(var1.charAt(var2)) && !Character.isDigit(var1.charAt(var2))) {
            return false;
         }
      }

      return true;
   }

   public boolean d(String var1) {
      if(var1.length() + 1 <= this.c) {
         CraftingDead.d.info("Faction tag: " + var1);

         for(int var2 = 0; var2 < var1.length(); ++var2) {
            if(var1.charAt(var2) != 38) {
               return false;
            }
         }
      }

      return true;
   }

   public boolean a(String var1, String var2) {
      CDPlayer var3 = PlayerDataHandler.a(var1);
      CDPlayer var4 = PlayerDataHandler.a(var2);
      return var3 != null && var4 != null && var3.g != null && var4.g != null && var3.g.equalsIgnoreCase(var4.g);
   }

   public boolean e(String var1) {
      Faction var2 = this.a();
      return var2 != null && var2.c(var1);
   }

   public static FactionManager c() {
      return CraftingDead.f.i();
   }
}
