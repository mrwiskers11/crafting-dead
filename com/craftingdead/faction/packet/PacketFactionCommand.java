package com.craftingdead.faction.packet;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketFactionCommand extends G_TcpPacketCustomPayload {

   private String e = "";


   public PacketFactionCommand(String var1) {
      this.e = var1;
   }

   public void a(DataOutputStream var1) throws IOException {
      a(var1, this.e);
   }

   public void a(DataInputStream var1) throws IOException {}

   public void a(G_ITcpConnectionHandler var1) throws IOException {}
}
