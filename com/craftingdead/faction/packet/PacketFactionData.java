package com.craftingdead.faction.packet;

import com.craftingdead.faction.FactionManager;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import com.f3rullo14.fds.tag.FDSTagCompound;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketFactionData extends G_TcpPacketCustomPayload {

   private String e;
   private FDSTagCompound f;


   public void a(DataOutputStream var1) throws IOException {}

   public void a(DataInputStream var1) throws IOException {
      this.e = a((DataInput)var1);
      this.f = new FDSTagCompound();
      this.f.a((DataInput)var1);
   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {
      FactionManager.c().a(this.e, this.f);
   }
}
