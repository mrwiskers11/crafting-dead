package com.craftingdead.faction;

import com.craftingdead.faction.FactionUserList;
import com.f3rullo14.fds.tag.FDSTagCompound;

public class Faction {

   public static final int a = 5;
   public static final int b = 3;
   public String c;
   public String d;
   public String e;
   public FactionUserList f = new FactionUserList();
   public FactionUserList g = new FactionUserList();
   public String h = "I\'m a bad description... ";
   public int i = 0;
   public int j = 0;
   public int k = 0;
   public int l = 0;


   public static Faction a(FDSTagCompound var0) {
      Faction var1 = new Faction();
      var1.c = var0.f("tag");
      var1.d = var0.f("name");
      var1.e = var0.f("owner");
      var1.f.clear();
      var1.f.addAll(var0.i("members"));
      var1.g.clear();
      var1.g.addAll(var0.i("admins"));
      var1.h = var0.f("desc");
      var1.i = var0.e("pkills");
      var1.j = var0.e("zkills");
      var1.k = var0.e("deaths");
      var1.l = var0.e("karma");
      return var1;
   }

   public boolean a(String var1) {
      return this.g.contains(var1);
   }

   public boolean b(String var1) {
      return this.e.equals(var1);
   }

   public boolean c(String var1) {
      return this.f.contains(var1);
   }
}
