package com.craftingdead.faction;

import java.util.ArrayList;
import java.util.Iterator;

public class FactionUserList extends ArrayList {

   public boolean contains(Object var1) {
      String var2 = (String)var1;
      Iterator var3 = this.iterator();

      String var4;
      do {
         if(!var3.hasNext()) {
            return false;
         }

         var4 = (String)var3.next();
      } while(!var2.equalsIgnoreCase(var4));

      return true;
   }

   public String a() {
      String var1 = "";

      String var3;
      for(Iterator var2 = this.iterator(); var2.hasNext(); var1 = var1 + ":" + var3) {
         var3 = (String)var2.next();
      }

      return var1.trim().replaceFirst(":", "");
   }

   public static FactionUserList a(String var0) {
      String[] var1 = var0.split(":");
      FactionUserList var2 = new FactionUserList();
      String[] var3 = var1;
      int var4 = var1.length;

      for(int var5 = 0; var5 < var4; ++var5) {
         String var6 = var3[var5];
         var2.add(var6);
      }

      return var2;
   }
}
