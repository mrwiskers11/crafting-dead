package com.craftingdead.cloud;

import com.craftingdead.CraftingDead;
import com.craftingdead.cloud.packet.PacketRegistry;
import com.craftingdead.cloud.packet.serverpacket.PacketServerOfficial;
import com.craftingdead.server.ServerProxy;
import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.client.G_ClientTcpConnectionHandler;
import com.f3rullo14.cloud.client.IClientCloudConnection;
import com.f3rullo14.fds.tag.FDSTagCompound;

import net.minecraft.server.MinecraftServer;

public class CMServer implements IClientCloudConnection {

   private G_ClientTcpConnectionHandler a;
   private ServerProxy b;
   private int c = 1920;
   private int d = 580;


   public CMServer(ServerProxy var1) {
      this.b = var1;
      this.a = new G_ClientTcpConnectionHandler(this);
      PacketRegistry.a();
   }

   public void a() {
      this.a.e();
      if(this.a.g() && this.d++ > 600) {
         b(new PacketServerOfficial(this.b));
         this.d = 0;
      }

   }

   public void j() {
      this.a.b("Logging Out");
   }

   public String b() {
      return this.b.j;
   }

   public String e() {
      return this.b.k;
   }

   public int f() {
      return this.c;
   }

   public void a(FDSTagCompound var1) {
      var1.a("port", MinecraftServer.getServer().getServerPort());
   }

   public void a(String var1) {
      CraftingDead.d.info(var1);
   }

   public void a(G_TcpPacket var1) {}

   public boolean h() {
      return true;
   }

   public void i() {
      this.a.c();
   }

   public String d() {
      return "server";
   }

   public static void b(G_TcpPacket var0) {
      CraftingDead.m().d().a.c(var0);
   }

   public String c() {
      return "1.2.5";
   }

   public boolean g() {
      return true;
   }

   public void a(boolean var1) {}
}
