package com.craftingdead.cloud;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.ClientNotification;
import com.craftingdead.cloud.packet.PacketClientRequest;
import com.craftingdead.cloud.packet.PacketRegistry;
import com.craftingdead.cloud.packet.RequestType;
import com.craftingdead.player.PlayerDataHandler;
import com.craftingdead.utils.ModSessionHandler;
import com.f3rullo14.cloud.G_TcpPacket;
import com.f3rullo14.cloud.client.G_ClientTcpConnectionHandler;
import com.f3rullo14.cloud.client.IClientCloudConnection;
import com.f3rullo14.fds.tag.FDSTagCompound;

import cpw.mods.fml.common.Loader;
import net.minecraft.client.Minecraft;

public class CMClient implements IClientCloudConnection {

   private G_ClientTcpConnectionHandler c;
   private String d;
   private int e = 1920;
   private ModSessionHandler f = new ModSessionHandler();
   private int g = 0;
   private int h = 100000;
   public static int a = 0;
   public String b = "Main Menu";
   private boolean i = true;


   public CMClient(String var1) {
      this.d = var1;
      PacketRegistry.a();
      this.c = new G_ClientTcpConnectionHandler(this);
   }

   public void a() {
      this.c.e();
      if(this.g < 40) {
         ++this.g;
      } else {
         if(this.i) {
            PacketClientRequest.a(RequestType.a, new String[]{this.b()});
            PacketClientRequest.a(RequestType.b, new String[0]);
            this.i = false;
         }

         if(this.c.g() && this.h++ > 100) {
            PacketClientRequest.a(RequestType.a, new String[]{this.b()});
            this.h = 0;
         }

         PlayerDataHandler.a(Minecraft.getMinecraft());
      }
   }

   public String b() {
      return Minecraft.getMinecraft().getSession().getUsername();
   }

   public String c() {
      return "1.2.5";
   }

   public String d() {
      return "player";
   }

   public String e() {
      return this.d;
   }

   public int f() {
      return this.e;
   }

   public boolean g() {
      return true;
   }

   public void a(FDSTagCompound var1) {
      this.f.a();
      var1.a("debug", true);
      var1.a("key", this.f.a);
   }

   public void a(String var1) {}

   public void a(G_TcpPacket var1) {}

   public void a(boolean var1) {
      String var2 = "Connected to the Cloud!";
      if(!var1) {
         var2 = "Connection to the Cloud Lost!";
      }

      ClientNotification.b(var2);
   }

   public boolean h() {
      return true;
   }

   public void i() {
      this.c.c();
   }

   public G_ClientTcpConnectionHandler j() {
      return this.c;
   }

   public static void b(G_TcpPacket var0) {
      if(CraftingDead.l().h().j().g()) {
         CraftingDead.l().h().j().c(var0);
      }

   }

   private boolean k() {
      return Loader.instance().getActiveModList().size() > 4;
   }

}
