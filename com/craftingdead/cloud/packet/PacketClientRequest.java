package com.craftingdead.cloud.packet;

import com.craftingdead.cloud.CMClient;
import com.craftingdead.cloud.CMServer;
import com.craftingdead.cloud.packet.RequestType;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketClientRequest extends G_TcpPacketCustomPayload {

   private RequestType e;
   private String[] f;


   public PacketClientRequest(RequestType var1, String ... var2) {
      this.e = var1;
      this.f = var2;
   }

   public void a(DataOutputStream var1) throws IOException {
      a(var1, this.e.toString());
      var1.writeBoolean(this.f != null);
      var1.writeInt(this.f.length);

      for(int var2 = 0; var2 < this.f.length; ++var2) {
         a(var1, this.f[var2]);
      }

   }

   public void a(DataInputStream var1) throws IOException {}

   public void a(G_ITcpConnectionHandler var1) throws IOException {}

   @SideOnly(Side.CLIENT)
   public static void a(RequestType var0, String ... var1) {
      PacketClientRequest var2 = new PacketClientRequest(var0, var1);
      CMClient.b(var2);
   }

   @SideOnly(Side.SERVER)
   public static void b(RequestType var0, String ... var1) {
      PacketClientRequest var2 = new PacketClientRequest(var0, var1);
      CMServer.b(var2);
   }
}
