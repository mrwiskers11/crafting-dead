package com.craftingdead.cloud.packet;

import com.craftingdead.cloud.CMClient;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import com.f3rullo14.fds.tag.FDSTagCompound;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketRequestedCloudData extends G_TcpPacketCustomPayload {

   private FDSTagCompound e = new FDSTagCompound("cloudstats");


   public void a(DataOutputStream var1) throws IOException {}

   public void a(DataInputStream var1) throws IOException {
      this.e.a((DataInput)var1);
   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {
      CMClient.a = this.e.e("usersonline");
   }
}
