package com.craftingdead.cloud.packet;


public enum RequestType {

   a("PLAYER_DATA", 0, "playerdata"),
   b("FACTION", 1, "faction"),
   c("NEWS", 2, "news"),
   d("CLOUD_DATA", 3, "clouddata");
   private String e;
   // $FF: synthetic field
   private static final RequestType[] f = new RequestType[]{a, b, c, d};


   private RequestType(String var1, int var2, String var3) {
      this.e = var3;
   }

   public String toString() {
      return this.e;
   }

}
