package com.craftingdead.cloud.packet;

import com.craftingdead.CraftingDead;
import com.craftingdead.client.ClientNotification;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketNotificationFromCloud extends G_TcpPacketCustomPayload {

   private String e;
   private String f;


   public void a(DataOutputStream var1) throws IOException {}

   public void a(DataInputStream var1) throws IOException {
      this.e = a((DataInput)var1);
      this.f = a((DataInput)var1);
   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {
      ClientNotification var2 = (new ClientNotification(this.f)).a(this.e);
      CraftingDead.l().d().a(var2);
   }
}
