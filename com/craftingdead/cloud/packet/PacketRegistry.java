package com.craftingdead.cloud.packet;

import com.craftingdead.cloud.CMClient;
import com.craftingdead.cloud.CMServer;
import com.craftingdead.cloud.packet.PacketClientRequest;
import com.craftingdead.cloud.packet.PacketNotificationFromCloud;
import com.craftingdead.cloud.packet.PacketRequestedCloudData;
import com.craftingdead.cloud.packet.PacketRequestedNews;
import com.craftingdead.cloud.packet.PacketRequestedPlayerData;
import com.craftingdead.cloud.packet.serverpacket.PacketPlayerDataEditValue;
import com.craftingdead.cloud.packet.serverpacket.PacketServerNotification;
import com.craftingdead.cloud.packet.serverpacket.PacketServerOfficial;
import com.craftingdead.cloud.packet.serverpacket.PacketServerOfficialAccepted;
import com.craftingdead.faction.packet.PacketFactionCommand;
import com.craftingdead.faction.packet.PacketFactionData;
import com.f3rullo14.cloud.G_TcpPacket;
import cpw.mods.fml.common.FMLCommonHandler;

public class PacketRegistry {

   public static void a() {
      G_TcpPacket.a(10, PacketClientRequest.class);
      G_TcpPacket.a(11, PacketRequestedPlayerData.class);
      G_TcpPacket.a(12, PacketRequestedNews.class);
      G_TcpPacket.a(13, PacketNotificationFromCloud.class);
      G_TcpPacket.a(19, PacketServerNotification.class);
      G_TcpPacket.a(20, PacketRequestedCloudData.class);
      G_TcpPacket.a(24, PacketPlayerDataEditValue.class);
      G_TcpPacket.a(29, PacketServerOfficial.class);
      G_TcpPacket.a(30, PacketServerOfficialAccepted.class);
      G_TcpPacket.a(35, PacketFactionCommand.class);
      G_TcpPacket.a(36, PacketFactionData.class);
   }

   public static void a(G_TcpPacket var0) {
      if(FMLCommonHandler.instance().getSide().isClient()) {
         CMClient.b(var0);
      } else {
         CMServer.b(var0);
      }

   }
}
