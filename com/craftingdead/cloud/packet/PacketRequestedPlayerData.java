package com.craftingdead.cloud.packet;

import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.player.PlayerDataHandler;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import com.f3rullo14.fds.tag.FDSTagCompound;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketRequestedPlayerData extends G_TcpPacketCustomPayload {

   private String e;
   private FDSTagCompound f = new FDSTagCompound("playerdata");


   public void a(DataOutputStream var1) throws IOException {}

   public void a(DataInputStream var1) throws IOException {
      this.e = a((DataInput)var1);
      this.f.a((DataInput)var1);
   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {
      CDPlayer var2 = PlayerDataHandler.a(this.e);
      var2.b = this.f.e("kills");
      var2.c = this.f.e("zkills");
      var2.d = this.f.e("deaths");
      var2.a = this.f.g("online");
      var2.e = this.f.e("karma");
      var2.f = this.f.f("mood");
      var2.g = this.f.f("faction");
      var2.h = this.f.f("factionInvite");
      if(var2.g.length() <= 0) {
         var2.g = null;
      }

   }
}
