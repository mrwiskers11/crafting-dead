package com.craftingdead.cloud.packet.serverpacket;

import com.craftingdead.server.ServerProxy;
import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketServerOfficial extends G_TcpPacketCustomPayload {

   private ServerProxy e;


   public PacketServerOfficial(ServerProxy var1) {
      this.e = var1;
   }

   public void a(DataOutputStream var1) throws IOException {
      var1.writeBoolean(this.e.l);
      a(var1, this.e.n);
   }

   public void a(DataInputStream var1) throws IOException {}

   public void a(G_ITcpConnectionHandler var1) throws IOException {}
}
