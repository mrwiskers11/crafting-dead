package com.craftingdead.cloud.packet.serverpacket;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketPlayerDataEditValue extends G_TcpPacketCustomPayload {

   private String e;
   private String f;
   private int g;


   public PacketPlayerDataEditValue(String var1, String var2, int var3) {
      this.e = var1;
      this.f = var2;
      this.g = var3;
   }

   public void a(DataOutputStream var1) throws IOException {
      a(var1, this.e);
      a(var1, this.f);
      var1.writeInt(this.g);
   }

   public void a(DataInputStream var1) throws IOException {}

   public void a(G_ITcpConnectionHandler var1) throws IOException {}
}
