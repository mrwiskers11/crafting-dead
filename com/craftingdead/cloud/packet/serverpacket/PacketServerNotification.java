package com.craftingdead.cloud.packet.serverpacket;

import com.f3rullo14.cloud.G_ITcpConnectionHandler;
import com.f3rullo14.cloud.G_TcpPacketCustomPayload;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.minecraft.server.MinecraftServer;

public class PacketServerNotification extends G_TcpPacketCustomPayload {

   private String e;


   public void a(DataOutputStream var1) throws IOException {}

   public void a(DataInputStream var1) throws IOException {
      this.e = a((DataInput)var1);
   }

   public void a(G_ITcpConnectionHandler var1) throws IOException {
      MinecraftServer.getServer().logInfo("Cloud: " + this.e);
   }
}
