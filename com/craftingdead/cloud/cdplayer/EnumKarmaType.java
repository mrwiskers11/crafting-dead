package com.craftingdead.cloud.cdplayer;

import net.minecraft.util.EnumChatFormatting;

public enum EnumKarmaType {

   a("HERO_1", 0, 1000, true, EnumChatFormatting.BLUE + "" + EnumChatFormatting.BOLD + "Hero I"),
   b("HERO_2", 1, 2500, true, EnumChatFormatting.BLUE + "" + EnumChatFormatting.BOLD + "Hero II"),
   c("HERO_3", 2, 4000, true, EnumChatFormatting.BLUE + "" + EnumChatFormatting.BOLD + "Hero III"),
   d("HERO_4", 3, 7000, true, EnumChatFormatting.BLUE + "" + EnumChatFormatting.BOLD + "Hero IV"),
   e("BANDIT_1", 4, -1000, false, EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + "Bandit I"),
   f("BANDIT_2", 5, -2500, false, EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + "Bandit II"),
   g("BANDIT_3", 6, -4000, false, EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + "Bandit III"),
   h("BANDIT_4", 7, -7000, false, EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + "Bandit IV");
   public static final int i = 8000;
   public static final int j = -8000;
   private int m = 0;
   public boolean k = true;
   public String l = "";
   // $FF: synthetic field
   private static final EnumKarmaType[] n = new EnumKarmaType[]{a, b, c, d, e, f, g, h};


   private EnumKarmaType(String var1, int var2, int var3, boolean var4, String var5) {
      this.m = var3;
      this.k = var4;
      this.l = var5;
   }

   public static EnumKarmaType a(int var0) {
      EnumKarmaType var1 = null;
      EnumKarmaType[] var2 = values();
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         EnumKarmaType var5 = var2[var4];
         if(var0 > 0) {
            if(var5.m > 0 && var5.m <= var0) {
               var1 = var5;
            }
         } else if(var5.m < 0 && var5.m >= var0) {
            var1 = var5;
         }
      }

      return var1;
   }

   public String toString() {
      return this.l;
   }

}
