package com.craftingdead.cloud.cdplayer;

import com.craftingdead.cloud.cdplayer.EnumKarmaType;
import com.craftingdead.mojang.MojangAPI;

public class CDPlayer {

   private String i;
   private String j;
   public boolean a = false;
   public int b = 0;
   public int c = 0;
   public int d = 0;
   public int e = 0;
   public String f = "I dont has a status yet...";
   public String g;
   public String h;


   public CDPlayer(String var1) {
      this.i = var1;
      this.j = MojangAPI.getUUID(i, true).toString();
   }

   public EnumKarmaType a() {
      return EnumKarmaType.a(this.e);
   }

   public String b() {
      return this.i;
   }

   public String c() {
      if(this.j == null || this.j.length() <= 5) {
         this.j = MojangAPI.getUUID(this.i, true).toString();
      }

      return this.j;
   }

   public int d() {
      return this.e;
   }

   public String toString() {
      return "[Cloud Player Data] {username=" + this.i + "}";
   }
}
