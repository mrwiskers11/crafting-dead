package com.craftingdead.player;

import com.craftingdead.damage.DamageSourceThirst;
import com.craftingdead.event.EventThirst;
import com.craftingdead.item.EnumBackpackSize;
import com.craftingdead.item.ItemBackpack;
import com.craftingdead.item.ItemClothing;
import com.craftingdead.player.PlayerData;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

public class WaterLevels {

   private int a = '\u8ca0';
   private int b = '\u8ca0';
   private PlayerData c;
   private int d = 0;


   public WaterLevels(PlayerData var1) {
      this.c = var1;
   }

   public void a(EntityPlayer var1) {
      if(!var1.capabilities.isCreativeMode) {
         if(this.a > 0) {
            this.b(1);
            ItemStack var2 = this.c.d().a("backpack");
            if(var2 != null) {
               if(var2.getItem() instanceof ItemBackpack) {
                  ItemBackpack var3 = (ItemBackpack)var2.getItem();
                  if(var3.e() == EnumBackpackSize.d || var3.e() == EnumBackpackSize.e) {
                     this.b(2);
                  }
               }

               ItemStack var6 = this.c.d().a("clothing");
               if(var6 != null && var6.getItem() instanceof ItemClothing) {
                  ItemClothing var4 = (ItemClothing)var6.getItem();
                  if(var4.i() > 1) {
                     this.b(2);
                  }
               }
            }

            if(var1.isSprinting()) {
               this.b(1);
            }
         }

         if(this.a == 1) {
            EventThirst var5 = new EventThirst(this.c.a);
            MinecraftForge.EVENT_BUS.post(var5);
         }

         if(this.a == 0 && this.d++ >= 120) {
            var1.attackEntityFrom(new DamageSourceThirst(), 1.0F);
            this.d = 0;
         }
      }

   }

   public void a(int var1) {
      this.a = var1;
   }

   public void b(int var1) {
      this.a -= var1;
      if(this.a < 0) {
         this.a = 0;
      }

   }

   public int a() {
      return this.a;
   }

   public int b() {
      int var1 = (int)(20.0D * this.d());
      return var1;
   }

   public void c(int var1) {
      int var2 = this.b / 20;
      this.d(var1 * var2);
   }

   public boolean c() {
      return this.a < this.b - 20;
   }

   public void d(int var1) {
      this.a += var1;
      if(this.a > this.b) {
         this.a = this.b;
      }

   }

   public double d() {
      return (double)this.a / (double)this.b;
   }

   public int e() {
      return this.b;
   }
}
