package com.craftingdead.player;

import com.craftingdead.block.BlockManager;
import com.craftingdead.block.tileentity.TileEntityBaseCenter;
import com.craftingdead.inventory.InventoryCDA;
import com.craftingdead.player.WaterLevels;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;

public class PlayerData {

   public String a;
   public boolean b = false;
   public boolean c = false;
   public boolean d = false;
   public float e = 0.1F;
   public float f = 0.1F;
   public float g = 5.0F;
   public int h = 0;
   public int i = 0;
   public int j = 0;
   public int k = 0;
   public boolean l = false;
   public int m = 0;
   public int n = 0;
   public int o = 0;
   public int p = 0;
   public int q = 0;
   public boolean r = false;
   public boolean s = false;
   public boolean t = false;
   public float u = 0.0F;
   public int v = 0;
   public double w = 0.0D;
   public int x = 0;
   private InventoryCDA U = new InventoryCDA();
   private WaterLevels V;
   public int y = 0;
   public int z;
   public int A;
   public int B;
   public boolean C = true;
   public boolean D = false;
   public boolean E = false;
   public boolean F = true;
   public boolean G = true;
   public boolean H = true;
   public boolean I = true;
   public static final int J = 100;
   public int K = 0;
   public boolean L = false;
   public boolean M = true;
   public int N = 0;
   public int O = 600;
   public int P = 0;
   public boolean Q = false;
   public int R = 0;
   public static final int S = 200;
   public int T = 0;


   public PlayerData(String var1) {
      this.a = var1;
      this.V = new WaterLevels(this);
   }

   public void a(NBTTagCompound var1) {
      this.U.b(var1.getTagList("cdainv"));
      this.h = var1.getInteger("pkills");
      this.i = var1.getInteger("zkills");
      this.j = var1.getInteger("days");
      this.k = var1.getInteger("timealive");
      this.V.a(var1.hasKey("water")?var1.getInteger("water"):this.V.e());
      this.z = var1.getInteger("basex");
      this.A = var1.getInteger("basey");
      this.B = var1.getInteger("basez");
      this.Q = var1.getBoolean("handcuff");
      this.R = var1.getInteger("handdam");
   }

   public void b(NBTTagCompound var1) {
      NBTTagList var2 = new NBTTagList();
      this.U.a(var2);
      var1.setTag("cdainv", var2);
      var1.setInteger("pkills", this.h);
      var1.setInteger("zkills", this.i);
      var1.setInteger("days", this.j);
      var1.setInteger("timealive", this.k);
      var1.setInteger("water", this.V.a());
      var1.setInteger("basex", this.z);
      var1.setInteger("basey", this.A);
      var1.setInteger("basez", this.B);
      var1.setBoolean("handcuff", this.Q);
      var1.setInteger("handdam", this.R);
   }

   public void a() {
      this.V = new WaterLevels(this);
      this.c = false;
      this.k = 0;
      this.j = 0;
      this.i = 0;
      this.h = 0;
      this.K = 0;
      this.Q = false;
      this.R = 0;
   }

   public boolean b() {
      return this.N > 0;
   }

   public void c() {
      this.U = new InventoryCDA();
   }

   public InventoryCDA d() {
      return this.U;
   }

   public WaterLevels e() {
      return this.V;
   }

   public boolean a(EntityPlayer var1) {
      World var2 = var1.worldObj;
      if(this.z == 0 && this.A == 0 && this.B == 0) {
         return false;
      } else {
         if(!var2.isRemote && var2.getBlockId(this.z, this.A, this.B) == BlockManager.N.blockID) {
            TileEntityBaseCenter var3 = (TileEntityBaseCenter)var2.getBlockTileEntity(this.z, this.A, this.B);
            if(var3 != null && var3.f() != null && var3.f().equals(this.a)) {
               return true;
            }
         }

         return false;
      }
   }

   public void a(int var1, int var2, int var3) {
      this.z = var1;
      this.A = var2;
      this.B = var3;
   }

   public void f() {
      this.z = this.A = this.B = 0;
   }

   public void a(float var1) {
      this.e += var1;
      if(this.e > this.g) {
         this.e = this.g;
      }

   }

   public void b(float var1) {
      this.u += var1;
      if(this.u > 2.0F) {
         this.u = 2.0F;
      }

   }

   public int g() {
      return (int)(this.u * 60.0F);
   }
}
