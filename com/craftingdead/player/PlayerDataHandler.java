package com.craftingdead.player;

import com.craftingdead.cloud.cdplayer.CDPlayer;
import com.craftingdead.cloud.packet.PacketClientRequest;
import com.craftingdead.cloud.packet.RequestType;
import com.craftingdead.player.PlayerData;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;

public class PlayerDataHandler {

   private static Map b = new HashMap();
   public static Map a = new HashMap();
   private static int c = 10000;
   private static int d = 10000;


   @SideOnly(Side.CLIENT)
   public static void a(Minecraft var0) {
      if(var0.theWorld != null && !var0.isSingleplayer() && c++ >= 100) {
         WorldClient var1 = var0.theWorld;
         if(var1 != null) {
            ArrayList var2 = (ArrayList)var1.playerEntities;

            for(int var3 = 0; var3 < var2.size(); ++var3) {
               EntityPlayer var4 = (EntityPlayer)var2.get(var3);
               if(!var4.username.equalsIgnoreCase(var0.getSession().getUsername())) {
                  PacketClientRequest.a(RequestType.a, new String[]{var4.username});
               }
            }
         }

         c = 0;
      }

   }

   @SideOnly(Side.SERVER)
   public static void a() {
      if(d++ > 80) {
         if(FMLCommonHandler.instance().getSide() == Side.SERVER) {
            ArrayList var0 = new ArrayList();
            ArrayList var1 = new ArrayList(Arrays.asList(MinecraftServer.getServer().getConfigurationManager().getAllUsernames()));
            Iterator var2 = var1.iterator();

            while(var2.hasNext()) {
               String var3 = (String)var2.next();
               PacketClientRequest.b(RequestType.a, new String[]{var3});
               CDPlayer var4 = a(var3);
               if(var4.g != null && var4.g.length() > 0 && !var0.contains(var4.g)) {
                  PacketClientRequest.b(RequestType.b, new String[]{var4.g});
                  var0.add(var4.g);
               }
            }
         }

         d = 0;
      }

   }

   public static CDPlayer a(String var0) {
      if(!a.containsKey(var0)) {
         a.put(var0, new CDPlayer(var0));
      }

      return (CDPlayer)a.get(var0);
   }

   public static PlayerData b() {
      Minecraft var0 = Minecraft.getMinecraft();
      return b(var0.getSession().getUsername());
   }

   public static PlayerData a(EntityPlayer var0) {
      return var0 == null?null:b(var0.username);
   }

   public static PlayerData b(String var0) {
      if(!b.containsKey(var0)) {
         b.put(var0, new PlayerData(var0));
      }

      return (PlayerData)b.get(var0);
   }

   public static void b(EntityPlayer var0) {
      if(var0 != null) {
         c(var0.username);
      }
   }

   public static void c(String var0) {
      if(b.containsKey(var0)) {
         b.remove(var0);
      }

   }

}
