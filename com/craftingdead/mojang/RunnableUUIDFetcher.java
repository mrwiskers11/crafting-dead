package com.craftingdead.mojang;

import com.craftingdead.mojang.MojangAPI;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RunnableUUIDFetcher implements Runnable {

   private List pendingUsernames;
   private Map uuidCache;


   public RunnableUUIDFetcher(List var1, Map var2) {
      this.pendingUsernames = new ArrayList(var1);
      this.uuidCache = var2;
   }

   public void run() {
      try {
         String var1 = MojangAPI.GSON.toJson(this.pendingUsernames);
         JsonArray var2 = (JsonArray)MojangAPI.JSON_PARSER.parse(new InputStreamReader(MojangAPI.postJson(new URL("https://api.mojang.com/profiles/minecraft"), var1.getBytes())));
         Iterator var3 = var2.iterator();

         while(var3.hasNext()) {
            Object var4 = var3.next();

            try {
               JsonObject var5 = (JsonObject)var4;
               String var6 = var5.get("id").getAsString();
               String var7 = var5.get("name").getAsString();
               this.uuidCache.put(var7, MojangAPI.parseUUID(var6));
            } catch (Exception var8) {
               var8.printStackTrace();
            }
         }
      } catch (IOException var9) {
         var9.printStackTrace();
      }

   }
}
