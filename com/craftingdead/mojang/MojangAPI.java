package com.craftingdead.mojang;

import com.craftingdead.mojang.RunnableUUIDFetcher;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MojangAPI {

   public static final String UUID_URL = "https://api.mojang.com/profiles/minecraft";
   public static final String PROFILE_URL = "https://sessionserver.mojang.com/session/minecraft/profile/";
   public static final JsonParser JSON_PARSER = new JsonParser();
   public static final Gson GSON = new Gson();
   private static ExecutorService executorService = Executors.newCachedThreadPool();
   private static Map uuidCache = new HashMap();
   private static ArrayList pendingUsernames = new ArrayList();
   private static int tickThreadSpawning = 0;


   public static void downloadPendingUUIDs() {
      ++tickThreadSpawning;
      if(pendingUsernames.size() > 0 && tickThreadSpawning >= 20) {
         executorService.execute(new RunnableUUIDFetcher(pendingUsernames, uuidCache));
         pendingUsernames.clear();
         tickThreadSpawning = 0;
      }

      if(tickThreadSpawning >= 100) {
         tickThreadSpawning = 0;
      }

   }

   public static UUID getUUID(String var0, boolean var1) {
      if(uuidCache.containsKey(var0)) {
         return (UUID)uuidCache.get(var0);
      } else if(var1) {
         ArrayList var2 = new ArrayList();
         var2.add(var0);
         (new RunnableUUIDFetcher(var2, uuidCache)).run();
         return (UUID)uuidCache.get(var0);
      } else {
         if(!pendingUsernames.contains(var0)) {
            pendingUsernames.add(var0);
         }

         return null;
      }
   }

   public static InputStream postJson(URL var0, byte[] var1) throws IOException {
      HttpURLConnection var2 = (HttpURLConnection)var0.openConnection();
      var2.setRequestMethod("POST");
      var2.setRequestProperty("Content-Type", "application/json");
      var2.setUseCaches(false);
      var2.setDoOutput(true);
      OutputStream var3 = var2.getOutputStream();
      Throwable var4 = null;

      try {
         var3.write(var1);
         var3.flush();
      } catch (Throwable var13) {
         var4 = var13;
         throw var13;
      } finally {
         if(var3 != null) {
            if(var4 != null) {
               try {
                  var3.close();
               } catch (Throwable var12) {
                  var4.addSuppressed(var12);
               }
            } else {
               var3.close();
            }
         }

      }

      return var2.getInputStream();
   }

   public static String fixImageUrl(String var0) {
      String var1 = var0;
      String[] var2;
      String[] var3;
      String var4;
      UUID var5;
      if(var0.startsWith("http://skins.minecraft.net/MinecraftSkins/")) {
         try {
            var2 = var0.split("/");
            var3 = var2[var2.length - 1].split("\\.");
            var4 = var3[0];
            var5 = getUUID(var4, true);
            if(var5 == null) {
               return var1;
            }

            var1 = MojangAPI.PlayerResource.SKIN_URL.getUrl(var5);
            var1 = var1.isEmpty()?var0:var1;
         } catch (Exception var7) {
            var7.printStackTrace();
            var1 = var0;
         }
      }

      if(var0.startsWith("http://skins.minecraft.net/MinecraftCloaks/")) {
         try {
            var2 = var0.split("/");
            var3 = var2[var2.length - 1].split("\\.");
            var4 = var3[0];
            var5 = getUUID(var4, false);
            if(var5 == null) {
               return var1;
            }

            var1 = MojangAPI.PlayerResource.CAPE_URL.getUrl(var5);
            var1 = var1.isEmpty()?var0:var1;
         } catch (Exception var6) {
            var6.printStackTrace();
            var1 = var0;
         }
      }

      return var1;
   }

   public static UUID parseUUID(String var0) {
      return UUID.fromString(var0.substring(0, 8) + "-" + var0.substring(8, 12) + "-" + var0.substring(12, 16) + "-" + var0.substring(16, 20) + "-" + var0.substring(20, 32));
   }


   public static enum PlayerResource {

      SKIN_URL("SKIN_URL", 0, "https://crafatar.com/skins/%s.png"),
      CAPE_URL("CAPE_URL", 1, "https://crafatar.com/capes/%s.png"),
      AVATAR_URL("AVATAR_URL", 2, "https://crafatar.com/avatars/%s.png");
      private String url;
      // $FF: synthetic field
      private static final MojangAPI.PlayerResource[] $VALUES = new MojangAPI.PlayerResource[]{SKIN_URL, CAPE_URL, AVATAR_URL};


      private PlayerResource(String var1, int var2, String var3) {
         this.url = var3;
      }

      public String getUrl(UUID var1) {
         return String.format(this.url, new Object[]{var1});
      }

   }
}
