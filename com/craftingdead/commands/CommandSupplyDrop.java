package com.craftingdead.commands;

import com.craftingdead.CraftingDead;
import com.craftingdead.entity.SupplyDropManager;
import com.craftingdead.utils.BlockLocation;

import java.util.List;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CommandSupplyDrop extends CommandBase {

   public String getCommandName() {
      return "supplydrop";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "Supply Drop commands [spawn, add, clear, killall, help] (EX: [/supplydrop spawn <USERNAME> <LOOT> <TYPE>] [/supplydrop spawn F3RULLO14 1 military])";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var2.length >= 1) {
         String var3 = var2[0];
         SupplyDropManager var4 = CraftingDead.f.h().a();
         if(var1.getEntityWorld().isRemote) {
            return;
         }

         EntityPlayerMP var10;
         if(var3.equals("spawn")) {
            if(var2.length == 4) {
               String var5 = var2[1];
               EntityPlayerMP var6 = getPlayer(var1, var5);
               int var7 = Integer.parseInt(var2[2]);
               String var8 = var2[3];
               if(var6 != null) {
                  BlockLocation var9 = new BlockLocation((int)var6.posX, (int)var6.posY, (int)var6.posZ);
                  var4.a(var1.getEntityWorld(), var9, var8, var7);
                  var6.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Spawning " + var8.toUpperCase() + " Supply Drop!"));
                  return;
               }
            }

            if(var1 instanceof EntityPlayerMP) {
               var10 = (EntityPlayerMP)var1;
               BlockLocation var11 = new BlockLocation((int)var10.posX, (int)var10.posY, (int)var10.posZ);
               var4.a(var1.getEntityWorld(), var11, "military", 1);
               var10.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Spawning MILITARY Supply Drop!"));
               return;
            }
         }

         if(var1 instanceof EntityPlayerMP) {
            var10 = (EntityPlayerMP)var1;
            if(var3.equals("add")) {
               var4.a(new BlockLocation((int)var10.posX, (int)var10.posY, (int)var10.posZ));
               var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Added Supply Drop Location."));
            }

            if(var3.equals("clear")) {
               var4.a();
               var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Cleared Supply Drop Locations."));
            }

            if(var3.equals("killall")) {
               var4.b(var10.worldObj);
               var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Removed all spawned Supply Drops."));
            }

            if(var3.equals("help")) {
               var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + this.getCommandUsage(var1)));
            }
         }
      } else {
         var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Invalid Arguments"));
      }

   }

   public List addTabCompletionOptions(ICommandSender var1, String[] var2) {
      return var2.length == 2?getListOfStringsMatchingLastWord(var2, this.d()):null;
   }

   protected String[] d() {
      return MinecraftServer.getServer().getAllUsernames();
   }

   public int compareTo(Object var1) {
      return 0;
   }
}
