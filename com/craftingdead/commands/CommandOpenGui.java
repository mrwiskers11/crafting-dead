package com.craftingdead.commands;

import com.craftingdead.client.gui.CraftingDeadGui;
import com.craftingdead.network.packets.CDAPacketOpenGUI;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

public class CommandOpenGui extends CommandBase {

   public String getCommandName() {
      return "menu";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.gui";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      String var3 = var2[0];
      String var4 = var2[1];
      EntityPlayerMP var5 = MinecraftServer.getServer().getConfigurationManager().getPlayerForUsername(var4);
      byte var7 = -1;
      switch(var3.hashCode()) {
      case -1224522114:
         if(var3.equals("guiSettings")) {
            var7 = 0;
         }
         break;
      case 2147005490:
         if(var3.equals("guiFactions")) {
            var7 = 1;
         }
      }

      switch(var7) {
      case 0:
         PacketDispatcher.sendPacketToPlayer(CDAPacketOpenGUI.a(CraftingDeadGui.h), (Player)var5);
         break;
      case 1:
         PacketDispatcher.sendPacketToPlayer(CDAPacketOpenGUI.a(), (Player)var5);
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
