package com.craftingdead.commands;

import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CommandAdminBase extends CommandBase {

   public String getCommandName() {
      return "baseadmin";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.admin.base.op";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var2.length >= 1) {
         if(var1.getEntityWorld().isRemote) {
            return;
         }

         String var3 = var2[0];
         if(var3.equals("find")) {
            if(var2.length == 2) {
               String var4 = var2[1];
               EntityPlayerMP var5 = getPlayer(var1, var4);
               PlayerData var6 = PlayerDataHandler.a((EntityPlayer)var5);
               if(var6.a((EntityPlayer)var5) && (var6.z != 0 || var6.A != 0 || var6.B != 0)) {
                  var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + var4 + "\'s base is located at x " + var6.z + " z " + var6.B));
               } else {
                  var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + var4 + "doesn\'t have a base!"));
               }
            } else if(var2.length == 1) {
               var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Please enter another argument featuring the player\'s name."));
            }
         }
      } else {
         var1.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Invalid Arguments"));
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
