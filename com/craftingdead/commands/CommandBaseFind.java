package com.craftingdead.commands;

import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CommandBaseFind extends CommandBase {

   public String getCommandName() {
      return "basefind";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.base.find";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var1 instanceof EntityPlayer) {
         EntityPlayer var3 = (EntityPlayer)var1;
         if(FMLCommonHandler.instance().getSide() == Side.SERVER || !var3.worldObj.isRemote) {
            PlayerData var4 = PlayerDataHandler.a(var3);
            if(var4.a(var3) && (var4.z != 0 || var4.A != 0 || var4.B != 0)) {
               var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Your base center is located at: x " + var4.z + " z " + var4.B));
            } else {
               var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You don\'t have a base!"));
            }
         }
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
