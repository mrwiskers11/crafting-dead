package com.craftingdead.commands;

import com.craftingdead.item.blueprint.ItemBlueprintBaseCenter;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

public class CommandBaseCenter extends CommandBase {

   public String getCommandName() {
      return "cdbasecenter";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.base.place";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var1 instanceof EntityPlayerMP) {
         EntityPlayerMP var3 = (EntityPlayerMP)var1;
         ItemStack var4 = var3.getCurrentEquippedItem();
         if(var4 != null && var4.getItem() instanceof ItemBlueprintBaseCenter) {
            ItemBlueprintBaseCenter var5 = (ItemBlueprintBaseCenter)var4.getItem();
            var5.a(var3, var4);
         }
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
