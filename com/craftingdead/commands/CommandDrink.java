package com.craftingdead.commands;

import com.craftingdead.event.EventThirstQuenched;
import com.craftingdead.player.PlayerData;
import com.craftingdead.player.PlayerDataHandler;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;

public class CommandDrink extends CommandBase {

   public String getCommandName() {
      return "drink";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.player.drink";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var1 instanceof EntityPlayerMP) {
         EntityPlayerMP var3 = (EntityPlayerMP)var1;
         PlayerData var4 = PlayerDataHandler.a((EntityPlayer)var3);
         ItemStack var5 = var3.getCurrentEquippedItem();
         if(var4.e().c()) {
            EventThirstQuenched var6 = new EventThirstQuenched(var3, var5, var4.e().b(), 20);
            MinecraftForge.EVENT_BUS.post(var6);
            var4.e().c(20);
            var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.GREEN + "Thirst quenched"));
         } else {
            var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "No thirst to quench"));
         }
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
