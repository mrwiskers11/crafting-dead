package com.craftingdead.commands;

import com.craftingdead.commands.CommandAdminBase;
import com.craftingdead.commands.CommandBaseCenter;
import com.craftingdead.commands.CommandBaseFind;
import com.craftingdead.commands.CommandDrink;
import com.craftingdead.commands.CommandOpenGui;
import com.craftingdead.commands.CommandRSpawn;
import com.craftingdead.commands.CommandRSpawnAdd;
import com.craftingdead.commands.CommandRSpawnClear;
import com.craftingdead.commands.CommandSupplyDrop;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

public class CommandManager {

   public void a(FMLServerStartingEvent var1) {
      var1.registerServerCommand(new CommandBaseCenter());
      var1.registerServerCommand(new CommandRSpawn());
      var1.registerServerCommand(new CommandRSpawnAdd());
      var1.registerServerCommand(new CommandRSpawnClear());
      var1.registerServerCommand(new CommandBaseFind());
      var1.registerServerCommand(new CommandSupplyDrop());
      var1.registerServerCommand(new CommandAdminBase());
      var1.registerServerCommand(new CommandDrink());
      var1.registerServerCommand(new CommandOpenGui());
   }
}
