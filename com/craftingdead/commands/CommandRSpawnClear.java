package com.craftingdead.commands;

import com.craftingdead.CraftingDead;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CommandRSpawnClear extends CommandBase {

   public String getCommandName() {
      return "rspawnclear";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.randomspawn.clear";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var1 instanceof EntityPlayerMP && FMLCommonHandler.instance().getSide() == Side.SERVER) {
         EntityPlayerMP var3 = (EntityPlayerMP)var1;
         CraftingDead.f.g().a();
         var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Removed all Random Spawns"));
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
