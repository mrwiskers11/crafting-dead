package com.craftingdead.commands;

import com.craftingdead.CraftingDead;
import com.craftingdead.utils.BlockLocation;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CommandRSpawn extends CommandBase {

   public String getCommandName() {
      return "rspawn";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.randomspawn";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var1 instanceof EntityPlayerMP && FMLCommonHandler.instance().getSide() == Side.SERVER) {
         EntityPlayerMP var3 = (EntityPlayerMP)var1;
         BlockLocation var4 = CraftingDead.f.g().b();
         if(var4 == null) {
            var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "No random spawn locations set, please consult world owner"));
            return;
         }

         var3.setPositionAndUpdate((double)var4.b, (double)var4.c, (double)var4.d);
         var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "You have been teleported, Good Luck..."));
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
