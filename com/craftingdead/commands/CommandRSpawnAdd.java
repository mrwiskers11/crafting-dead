package com.craftingdead.commands;

import com.craftingdead.CraftingDead;
import com.craftingdead.utils.BlockLocation;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.util.EnumChatFormatting;

public class CommandRSpawnAdd extends CommandBase {

   public String getCommandName() {
      return "rspawnadd";
   }

   public String getCommandUsage(ICommandSender var1) {
      return "cda.commands.randomspawn.add";
   }

   public void processCommand(ICommandSender var1, String[] var2) {
      if(var1 instanceof EntityPlayerMP && FMLCommonHandler.instance().getSide() == Side.SERVER) {
         EntityPlayerMP var3 = (EntityPlayerMP)var1;
         int var4 = (int)var3.posX;
         int var5 = (int)var3.posY;
         int var6 = (int)var3.posZ;
         CraftingDead.f.g().a(new BlockLocation(var4, var5, var6));
         var3.sendChatToPlayer(ChatMessageComponent.createFromText(EnumChatFormatting.RED + "Random Spawn Added"));
      }

   }

   public int compareTo(Object var1) {
      return 0;
   }
}
